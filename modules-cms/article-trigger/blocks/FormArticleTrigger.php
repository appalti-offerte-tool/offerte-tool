<?php

class ArticleTrigger_Block_FormArticleTrigger extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Customization');
    }

    protected function _toHtml()
    {}
}