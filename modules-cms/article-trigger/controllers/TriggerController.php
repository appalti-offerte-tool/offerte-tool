<?php

final class ArticleTrigger_TriggerController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var ArticleTrigger_Service_Trigger
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_service = new ArticleTrigger_Service_Trigger();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'guest';
    }

    public function comboBoxAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');

        $this->view->response = $this->_service->fetchAllWithResponse();
    }
}