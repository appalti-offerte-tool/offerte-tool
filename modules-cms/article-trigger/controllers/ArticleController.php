<?php

class ArticleTrigger_ArticleController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var ArticleTrigger_Service_Article
     */
    protected $_service;

    /**
     * @var Article_Model_DbTable_ArticleRow
     */
    protected $_articleRow;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $articleService = new Article_Service_Article();
        $this->_articleRow = $articleService->find($this->_getParam('articleId'));

        $this->_service = new \ArticleTrigger_Service_Article($this->_articleRow);

        $this->_helper->contextSwitch()
            ->addActionContext('index', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch', 'json')
            ->initContext();

        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'rating:list';
    }

    public function indexAction()
    {
        // prepare layout
    }

    public function fetchAllAction()
    {
        $response = $this->_service->fetchAllWithResponse($this->_getAllParams());

        $articleRow = $this->_articleRow;
        $response->setRowsetCallback(function(\ArticleTrigger_Model_ArticleTriggerRel $row) use ($articleRow) {

            $triggerRow = $row->getTriggerRow();
            $o = $triggerRow->toArray();

            $entityRow = $row->toEntity($articleRow, $triggerRow);
            if (null !== $entityRow) {
                $o['relation'] = $entityRow->getName();
                $o['entityId'] = $row->entityId;
            }

            return $o;
        });

        $this->view->assign($response->toArray());
    }

    public function fetchAction()
    {
        $articleTriggerRow = $this->_service->find($this->_getParam('triggerId'));
        $this->view->row = $articleTriggerRow->toArray();
        $this->view->success = true;
    }

    public function createAction()
    {
        try {
            $articleTriggerRow = $this->_service->create($this->_getAllParams());
            $this->view->triggerId = $articleTriggerRow->getId();
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        try {
            $articleTriggerRow = $this->_service->update($this->_getParam('triggerId'), $this->_getAllParams());
            $this->view->triggerId = $articleTriggerRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->view->success = $this->_service->delete($this->_getParam('triggerId'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}