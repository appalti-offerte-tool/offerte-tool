<?php

final class ArticleTrigger_TriggerFeedbackController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    public function init()
    {
        $this->_helper->contextSwitch()
             ->addActionContext('send', 'json')
             ->initContext();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'guest';
    }

    public function sendAction()
    {
        $feedback = new ArticleTrigger_Misc_Email_Feedback();

        try {
            $feedback->notificate($this->_getAllParams());
            $this->_helper->information('Message has been send successfully', null, E_USER_NOTICE);
            $this->view->success = true;
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}