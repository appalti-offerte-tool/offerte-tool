<?php

final class ArticleTrigger_TriggerRatingController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    public function init()
    {
        $this->_helper->contextSwitch()
             ->addActionContext('fetch', 'json')
             ->addActionContext('vote', 'json')
             ->initContext();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'guest';
    }

    public function fetchAction()
    {
        $triggerService = new ArticleTrigger_Service_Trigger();
        $triggerRatingRow = $triggerService->fetchByLuid('rating');

        $articleService = new Article_Service_Article();
        $articleRow = $articleService->find($this->_getParam('articleId'));

        $entityId = $triggerRatingRow->toEntityId($articleRow);

        $ratingService = new Rating_Service_Rating();
        $ratingRow = $ratingService->find($entityId);

        $this->view->row = $ratingRow->toArray();
        $this->view->options = $ratingRow->getRatingOptionsRowset()->toArray();
        $this->view->success = true;
    }

    public function voteAction()
    {
        $articleService = new Article_Service_Article();
        $articleRow = $articleService->find($this->_getParam('articleId'));

        $ratingService = new Rating_Service_Rating();
        $ratingRow = $ratingService->find($this->_getParam('ratingId'));

        $ratingVoteService = new Rating_Service_RatingVote($ratingRow);

        $data = array(
            'entityId' => $articleRow->getId()
        );

        try {
            $ratingOptions = $this->_getParam('ro');
            $ratingVoteRow = $ratingVoteService->create($data, is_array($ratingOptions) ? $ratingOptions : array());
            $this->_helper->information('Your vote has been added', null, E_USER_NOTICE);
            $this->view->success = true;
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}