<?php

final class ArticleTrigger_Misc_Email_Feedback extends \Configuration_Service_ConfigurationAbstract
    implements \Notification_Instance_NotifiableInterface
{
    protected $_identity = 'article-trigger.feedback';

    protected $_fields = array(
        'receipt',
        'subject', 'body'
    );

    protected $_mui = true;

    protected $_keywords = array(
        'message'      => 'E-mail body',
        'name'         => 'Sender name',
        'timestamp'    => 'Timestamp',
        'articleId'    => 'Article ID',
        'article'      => 'Article title',
        'rawlink'      => 'Article raw link',
        'link'         => 'Article HTML link',
    );

    protected $_serialized = array('receipt');

    public function __construct()
    {
        parent::__construct();

        $defaultFrom = Zend_Mail::getDefaultFrom();
        if (is_array($defaultFrom)) {
            $this->_fields['receipt']['default'] = $this->_toValue($defaultFrom, 'receipt');
        }
    }

    /**
     * (non-PHPdoc)
     * @see Notification_Instance_NotifiableInterface::notificate()
     */
    public function notificate(array $params)
    {
        $f = new OSDN_Filter_Input(array(), array(
            'message'      => array('presence' => 'required', 'allowEmpty' => false),
            'articleId'    => array('id', 'presence' => 'required', 'allowEmpty' => false)
        ), $params);
        $f->validate(true);

        $params = $f->getData();

        $dt = new \DateTime();
        $params['timestamp'] = $dt->format('Y-m-d H:i:s');

        $articleService = new Article_Service_Article();
        $articleRow = $articleService->find($f->articleId);

        $params['article'] = $articleRow->getTitle();

        $link = $articleRow->toLink();
        $params['rawLink'] = $link;

        $params['link'] = sprintf('<a href="%s">%s</a>', $link, $articleRow->getTitle());

        $instance = $this->toInstance();

        $mail = new Zend_Mail('UTF-8');
        $mail->setFrom($f->email, $f->name);

        $receipt = $instance->getReceipt();
        $mail->addTo($receipt['email'], $receipt['name']);

        $caption = $instance->getSubject($params);
        $mail->setSubject($caption);

        $notification = $instance->getBody($params);
        $mail->setBodyHtml($notification);

        try {
            $result = $mail->send();

            $notificationServiceService = new Notification_Service_Service();
            $notificationServiceRow = $notificationServiceService->fetchByLuid('article-feedback');

            $log = new Notification_Service_Log();
            $log->create(array(
                'caption'                => $caption,
                'notification'           => $notification,
                'notificationServiceId'  => $notificationServiceRow->getId(),
                'notificationKindId'     => $notificationServiceRow->getNotificationKind()->getId(),
                'notificatedAccountId'   => Zend_Auth::getInstance()->getIdentity()->getId()
            ));

        } catch(\Zend_Mail_Transport_Exception $e) {
            throw new OSDN_Exception('Unable to send. Transport error.', null, $e);
        } catch(\Exception $e) {
            throw new OSDN_Exception('Unable to send message', null, $e);
        }

        return (boolean) $result;
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::update()
     */
    public function update(array $params)
    {
        /**
         * @todo Add validation
         */
        return parent::update($params);
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_toValue()
     */
    protected function _toValue($value, $field)
    {
        if (in_array($field, $this->_serialized)) {
            return serialize($value);
        }

        return parent::_toValue($value, $field);
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_fromValue()
     */
    protected function _fromValue($value, $field)
    {
        if (in_array($field, $this->_serialized)) {
            return unserialize($value);
        }

        return parent::_fromValue($value, $field);
    }
}