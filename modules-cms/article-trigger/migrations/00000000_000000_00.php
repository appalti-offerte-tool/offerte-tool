<?php

class ArticleTrigger_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('articleTrigger');
        $this->createColumn('articleTrigger', 'name', self::TYPE_VARCHAR, 100, null, true);
        $this->createColumn('articleTrigger', 'luid', self::TYPE_VARCHAR, 100, null, true);
        $this->createColumn('articleTrigger', 'description', self::TYPE_VARCHAR, 255, null, false);

        $this->createTable('articleTriggerRel');
        $this->createColumn('articleTriggerRel', 'articleId', self::TYPE_INT, 11, null, true);
        $this->createColumn('articleTriggerRel', 'articleTriggerId', self::TYPE_INT, 11, null, true);
        $this->createUniqueIndexes('articleTriggerRel', array('articleId', 'articleTriggerId'), 'UX_articleId');
        $this->createForeignKey('articleTriggerRel', array('articleId'), 'article', array('id'), 'FK_articleId');
        $this->createIndex('articleTriggerRel', array('articleTriggerId'), 'IX_articleTriggerId');
        $this->createForeignKey('articleTriggerRel', array('articleTriggerId'), 'articleTrigger', array('id'), 'FK_articleTriggerId');

        $keys = array('name', 'luid', 'description');
        foreach(array(
            array('Feeback', 'feedback', 'Provide easy feedback option for article. The message has been sent to email, which admin set in configuration file.'),
            array('Rating', 'rating', 'User will have posibility to set rating for articles')
        ) as $o) {
            $this->_dbAdapter->insert('articleTrigger', array_combine($keys, $o));
        }

        $notificationServiceKind = new Notification_Service_Kind();
        $notificationServiceKindRow = $notificationServiceKind->fetchByLuid('email');

        $this->_dbAdapter->insert('notificationService', array(
            'name'                  => 'Article feedback',
            'luid'                  => 'article-feedback',
            'description'           => 'Allow to make feedback to article',
            'module'                => 'article-trigger',
            'notificationKindId'    => $notificationServiceKindRow->getId()
        ));
    }

    public function down()
    {
        $this->dropTable('articleTriggerRel');
        $this->dropTable('articleTrigger');
    }
}