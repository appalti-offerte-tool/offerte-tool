<?php

class ArticleTrigger_Migration_20120424_100404_40 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createColumn('articleTriggerRel', 'entityId', self::TYPE_INT, 11, null, false);
    }

    public function down()
    {
        $this->dropColumn('articleTriggerRel', 'entityId');
    }
}