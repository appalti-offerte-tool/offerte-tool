<?php

class ArticleTrigger_Service_ArticleTrigger extends OSDN_Application_Service_Dbable
{
    /**
     * @param array $params
     * @return OSDN_Db_Response
     */
    public function fetchAllByArticleId($articleId)
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('atr' => 'articleTriggerRel'),
                array()
            )
            ->join(
                array('at' => 'articleTrigger'),
                'atr.articleTriggerId = at.id',
                array('id', 'luid', 'name')
            )
            ->where('atr.articleId = ?', $articleId, Zend_Db::INT_TYPE);

        return $this->getDecorator('response')->decorate($select);
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \ArticleTrigger_Model_Trigger
     */
    public function find($id, $throwException = true)
    {
        $triggerRow = $this->_table->findOne($id);
        if (null === $triggerRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find article #' . $id);
        }

        return $triggerRow;
    }

    /**
     * @param string $luid
     * @param boolean $throwException
     *
     * @return \ArticleTrigger_Model_Trigger
     */
    public function fetchByLuid($luid, $throwException = true)
    {
        $triggerRow = $this->_table->fetchRow(array(
            'luid = ?' => (string) $luid
        ));

        if (null === $triggerRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find trigger luid: #' . ($luid ?: 'none'));
        }

        return $triggerRow;
    }
}