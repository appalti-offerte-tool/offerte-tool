<?php

class ArticleTrigger_Service_Trigger extends OSDN_Application_Service_Dbable
{
    /**
     * @var Article_Model_DbTable_Article
     */
    protected $_table;

    protected function _init()
    {
        $this->_table = new \ArticleTrigger_Model_DbTable_Trigger();

        parent::_init();
    }

    /**
     * @param array $params
     * @return OSDN_Db_Response
     */
    public function fetchAllWithResponse(array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('a' => $this->_table->getTableName()),
                $this->_table->getAllowedColumns(\OSDN_Db_Table_Abstract::OMIT_FETCH_ROW)
            );

        return $this->getDecorator('response')->decorate($select);
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \ArticleTrigger_Model_Trigger
     */
    public function find($id, $throwException = true)
    {
        $triggerRow = $this->_table->findOne($id);
        if (null === $triggerRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find article #' . $id);
        }

        return $triggerRow;
    }

    /**
     * @param string $luid
     * @param boolean $throwException
     *
     * @return \ArticleTrigger_Model_Trigger
     */
    public function fetchByLuid($luid, $throwException = true)
    {
        $triggerRow = $this->_table->fetchRow(array(
            'luid = ?' => (string) $luid
        ));

        if (null === $triggerRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find trigger luid: #' . ($luid ?: 'none'));
        }

        return $triggerRow;
    }
}