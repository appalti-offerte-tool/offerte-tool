<?php

class ArticleTrigger_Service_Article extends OSDN_Application_Service_Dbable
{
    /**
     * @var ArticleTrigger_Model_DbTable_ArticleTriggerRel
     */
    protected $_table;

    /**
     * @var Article_Model_DbTable_ArticleRow
     */
    protected $_articleRow;

    /**
     * Initialize the constuctor
     * @param Article_Model_DbTable_ArticleRow $articleRow
     */
    public function __construct(Article_Model_DbTable_ArticleRow $articleRow)
    {
        $this->_articleRow = $articleRow;
        $this->_table = new \ArticleTrigger_Model_DbTable_ArticleTriggerRel();

        parent::__construct();
    }

    /**
     * @param array $params
     * @return OSDN_Db_Response
     */
    public function fetchAllWithResponse(array $params = array())
    {
        $select = $this->_table->select()
            ->where('articleId = ?', $this->_articleRow->getId(), Zend_Db::INT_TYPE);

        return $this->getDecorator('response')->decorate($select, $this->_table);
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \ArticleTrigger_Model_Trigger
     */
    public function find($id, $throwException = true)
    {
        $triggerRow = $this->_table->findOne($id);
        if (null === $triggerRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find article #' . $id);
        }

        return $triggerRow;
    }

    /**
     * Create the new article trigger row
     *
     * @param array $data
     * @return ArticleTrigger_Model_DbTable_ArticleTriggerRel
     */
    public function create(array $data)
    {
        $this->_attachValidationRules('default', array(
            'articleTriggerId'  => array('id', 'presence' => 'required', 'allowEmpty' => false),
            'entityId'          => array('allowEmpty' => true)
        ));
        $f = $this->_validate($data);

        $data = $f->getData();
        $data['articleId'] = $this->_articleRow->getId();

        $articleRatingRelRow = $this->_table->createRow($data);
        $articleRatingRelRow->save();

        return $articleRatingRelRow;
    }
}