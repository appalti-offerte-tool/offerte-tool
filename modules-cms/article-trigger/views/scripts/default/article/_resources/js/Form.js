Ext.define('Module.Application.Model.Error', {
    extend: 'Ext.data.Model',
    fields: [
        'message', 'type', 'field',
        {name: 'id', mapping: 'field'},
        {name: 'msg', mapping: 'message'}
    ]
});

Ext.define('Module.ArticleTrigger.Model.ArticleTrigger', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name', 'description',
        'entityId', 'relation', 'articleTriggerId'
    ]
});

Ext.define('Module.ArticleTrigger.Article.Form', {
    extend: 'Ext.form.Panel',

    bodyPadding: 5,

    wnd: null,
    ratingOptionId: null,
    articleId: null,

    model: 'Module.ArticleTrigger.Model.ArticleTrigger',
    atCombo: null,

    fileStockImage: null,

    initComponent: function() {

        this.atCombo = new Module.ArticleTrigger.Trigger.ComboBox({
            fieldLabel: lang('Article type'),
            anchor: '100%',
            name: 'articleTriggerId'
        });

        this.initialConfig.trackResetOnLoad = true;
        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.ArticleTrigger.Model.ArticleTrigger',
            type: 'json',
            root: 'row'
        });

        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
            model: 'Module.Application.Model.Error',
            type: 'json',
            root: 'messages',
            successProperty: 'success'
        });

        this.items = [this.atCombo];

        this.callParent();

        this.addEvents('complete');

        this.atCombo.on('select', function(combo, records) {
            var me = this;
            var record = records.pop();
            me.form.getFields().each(function(ct, i) {
                if (i == 0) {
                    return;
                }

                me.remove(ct);
            });
            me.doLayout();

            switch(record.get('luid')) {
                case 'rating':
                    Application.require([
                        'rating/./combo-box'
                    ], function() {
                        var combo = new Module.Rating.ComboBox({
                            fieldLabel: lang('Rating'),
                            anchor: '100%',
                            name: 'entityId'
                        });

                        me.add(combo);
                        me.doLayout();
                    }, this);

                    break;
            }
        }, this);
    },

    doLoad: function() {

        if (!this.ratingOptionId) {
            return;
        }
    },

    onSubmit: function() {

        if (!this.form.isValid()) {
            return false;
        }

        var action = this.ratingOptionId ? 'update' : 'create';
        var o = {
            articleId: this.articleId
        };
        if (this.ratingOptionId) {
            o.ratingOptionId = this.ratingOptionId;
        }

        this.form.submit({
            url: link('article-trigger', 'article', action, {format: 'json'}),
            params: o,
            success: function(options, action) {

                var decResponse = Ext.decode(action.response.responseText);
                Application.notificate(decResponse.messages);

                if (decResponse.success) {
                    this.fireEvent('complete', this, decResponse.ratingOptionId);
                    this.wnd && this.wnd.close();
                }
            },
            scope: this
        });
    },

    showInWindow: function() {
        this.border = false;
        var w = this.wnd = new Ext.Window({
            title: this.ratingOptionId ? lang('Update rating') : lang('Create rating'),
            resizable: false,
            layout: 'fit',
            iconCls: 'm-rating-icon-16',
            width: 450,
            height: 150,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Save'),
                handler: this.onSubmit,
                scope: this
            }, {
                text: lang('Close'),
                handler: function() {
                    w.close();
                    this.wnd = null;
                },
                scope: this
            }]
        });

        w.show();
        return w;
    }
});