Ext.define('Module.ArticleTrigger.Model.Article', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name', 'description',
        'entityId', 'relation'
    ]
});

Ext.define('Module.ArticleTrigger.Article.List', {
    extend: 'Ext.ux.grid.GridPanel',

    iconCls: 'm-article-trigger-icon-16',

    modeReadOnly: false,
    articleId: null,

    features: [{
        ftype: 'filters'
    }],

    plugins: [{
        ptype: 'rowexpander',

        /**
         * @FIXME Can cause a problem when multiple instances are created
         */
        pluginId: 'rowexpander',
        rowBodyTpl: ['<p style="font-style: italic;">{description}</p>']
    }],

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.ArticleTrigger.Model.Article',
            proxy: {
                type: 'ajax',
                url: link('article-trigger', 'article', 'fetch-all', {format: 'json'}),
                extraParams: {
                    articleId: this.articleId
                },
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.columns = [{
            header: lang('Name'),
            dataIndex: 'name',
            width: 150,
            renderer: function(v) {
                return '<b>' + v + '</b>';
            }
        }, {
            header: lang('Relation'),
            dataIndex: 'relation',
            flex: 1
        }];

        if (false === this.modeReadOnly) {
            this.columns.push({
                xtype: 'actioncolumn',
                header: lang('Actions'),
                width: 50,
                fixed: true,
                items: [{
                    text: lang('Delete'),
                    iconCls: 'icon-delete-16 icon-16',
                    handler: this.onDeleteArticleTrigger,
                    scope: this
                }]
            });
        }

        this.tbar = [{
            text: lang('Create'),
            iconCls: 'icon-create-16',
            qtip: lang('Create new ArticleTrigger'),
            handler: this.onCreateArticleTrigger,
            scope: this
        }, '-'];

        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 2
        })].concat(this.plugins);

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent();

        if (false === this.modeReadOnly) {
            this.getView().on('itemdblclick', function(w, record) {
                this.onEditArticleTrigger(this, record);
            }, this);
        }

        this.getStore().on('load', function(s) {
            var plugin = this.getPlugin('rowexpander');
            s.each(function(record, i) {
                plugin.toggleRow(i);
            });
        }, this);
    },

    onCreateArticleTrigger: function() {
        Application.require([
            'article-trigger/article/form'
        ], function() {
            var f = new Module.ArticleTrigger.Article.Form({
                articleId: this.articleId
            });

            f.on('complete', function(form, triggerId) {
                this.setLastInsertedId(triggerId);
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditArticleTrigger: function(g, record) {
        Application.require([
            'article-trigger/article/form'
        ], function() {
            var f = new Module.ArticleTrigger.Article.Form({
                triggerId: record.get('id')
            });
            f.doLoad();
            f.on('complete', function(form, triggerId) {
                this.setLastInsertedId(triggerId);
                g.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onDeleteArticleTrigger: function(g, rowIndex) {

        var record = g.getStore().getAt(rowIndex);

        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
            if (b != 'yes') {
                return;
            }

            Ext.Ajax.request({
                url: link('article-trigger', 'article', 'delete', {format: 'json'}),
                method: 'POST',
                params: {
                    triggerId: record.get('id')
                },
                success: function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);

                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope: this
            });
        }, this);
    },

    setArticleId: function(articleId, forceReload) {

        this.articleId = articleId;

        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.articleId = articleId;
        if (true === forceReload) {
            this.getStore().load();
        }

        return this;
    }
});