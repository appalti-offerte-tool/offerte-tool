<?php

class ArticleTrigger_Model_ArticleTriggerRel extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    /**
     * @var ArticleTrigger_Model_Trigger
     */
    protected $_triggerRow;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArticleTrigger_Model_Trigger
     */
    public function getTriggerRow()
    {
        if (null === $this->_triggerRow) {
            $this->_triggerRow = $this->findParentRow('ArticleTrigger_Model_DbTable_Trigger', 'ArticleTrigger');
        }

        return $this->_triggerRow;
    }

    public function toEntity(Article_Model_DbTable_ArticleRow $articleRow, ArticleTrigger_Model_Trigger $triggerRow = null)
    {
        if (empty($this->entityId)) {
            return null;
        }

        if (null !== $triggerRow && null === $this->_triggerRow) {
            $this->_triggerRow = $triggerRow;
        }
        $triggerRow = $this->getTriggerRow();

        $luid = $triggerRow->luid;
        $entityServiceCls = ucfirst($luid) . '_Service_' . ucfirst($luid);
        if (!class_exists($entityServiceCls)) {
            return null;
        }

        $entityServiceObj = new $entityServiceCls();
        return $entityServiceObj->find($this->entityId, false);
    }
}