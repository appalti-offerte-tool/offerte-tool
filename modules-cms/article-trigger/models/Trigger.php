<?php

class ArticleTrigger_Model_Trigger extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    public function toEntityId(Article_Model_DbTable_ArticleRow $articleRow)
    {
        $select = $this->select()->where('articleId = ?', $articleRow->getId(), Zend_Db::INT_TYPE);

        $rowset = $this->findDependentRowset('ArticleTrigger_Model_DbTable_ArticleTriggerRel', 'ArticleTrigger', $select);
        if ($rowset->count() == 0) {
            return null;
        }

        if ($rowset->count() > 1) {
            throw new OSDN_Exception('Data integrity corrupted');
        }

        return $rowset->current()->entityId;
    }
}