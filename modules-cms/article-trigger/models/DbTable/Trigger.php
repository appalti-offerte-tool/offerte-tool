<?php

class ArticleTrigger_Model_DbTable_Trigger extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'articleTrigger';

    protected $_rowClass = '\\ArticleTrigger_Model_Trigger';

    protected $_nullableFields = array(
        'description'
    );

    protected $_omitColumns = array(
        'description'    => self::OMIT_FETCH_ALL
    );
}