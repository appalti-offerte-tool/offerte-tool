<?php

class ArticleTrigger_Model_DbTable_ArticleTriggerRel extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'articleTriggerRel';

    protected $_rowClass = '\\ArticleTrigger_Model_ArticleTriggerRel';

    protected $_nullableFields = array(
        'description'
    );

    protected $_omitColumns = array(
        'description'    => self::OMIT_FETCH_ALL
    );

    protected $_referenceMap    = array(
        'ArticleTrigger' => array(
            'columns'       => 'articleTriggerId',
            'refTableClass' => 'ArticleTrigger_Model_DbTable_Trigger',
            'refColumns'    => 'id'
        ),
        'Article'    => array(
            'columns'       => 'articleId',
            'refTableClass' => 'Article_Model_DbTable_Article',
            'refColumns'    => 'id'
        )
    );
}