<?php

class ArticleCategory_Model_DbTable_Category extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'articleCategory';

    protected $_rowClass = '\\ArticleCategory_Model_DbTable_CategoryRow';

    protected $_referenceMap    = array(
        'Parent' => array(
            'columns'       => 'parentId',
            'refTableClass' => __CLASS__,
            'refColumns'    => 'id'
        )
    );

    protected $_nullableFields = array(
        'parentId', 'luid', 'template',
        'modifiedAccountId', 'modifiedDatetime'
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        if (empty($data['createdDatetime'])) {
            $dt = new \DateTime();
            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        $data['createdAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        return parent::insert($data);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
        if (empty($data['modifiedDatetime'])) {
            $dt = new \DateTime();
            $data['modifiedDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        $data['modifiedAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        return parent::update($data, $where);
    }
}