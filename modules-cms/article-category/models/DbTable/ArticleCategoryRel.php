<?php

class ArticleCategory_Model_DbTable_ArticleCategoryRel extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'articleCategoryRel';

    protected $_referenceMap    = array(
        'Article' => array(
            'columns'       => 'articleId',
            'refTableClass' => 'Article_Model_DbTable_Article',
            'refColumns'    => 'id'
        ),
        'Category' => array(
            'columns'       => 'categoryId',
            'refTableClass' => 'ArticleCategory_Model_DbTable_Category',
            'refColumns'    => 'id'
        )
    );
}