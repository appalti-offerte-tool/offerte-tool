<?php

class ArticleCategory_Model_DbTable_CategoryRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    public function getId()
    {
        return $this->id;
    }

    public function getParentRow()
    {
        return $this->findParentRow('ArticleCategory_Model_DbTable_Category', 'Parent');
    }

    public function refreshModifiedDateTime(\DateTime $dt = null)
    {
        if (null === $dt) {
            $dt = new \DateTime();
        }

        $this->modifiedDatetime = $dt->format('Y-m-d H:i:s');
        return $this;
    }

    public function getChildren()
    {
        return $this->findDependentRowset('ArticleCategory_Model_DbTable_Category', 'Parent');
    }

    public function walkRecursiveOnNestedChildrenInDepth(Closure $callbackFn, $depth = false)
    {
        if (is_int($depth)) {
            $depth -= 1;
        }

        foreach($this->getChildren() as $categoryRow) {

            $callbackFn($categoryRow);
            if (false === $depth || $depth >= 0) {
                $categoryRow->walkRecursiveOnNestedChildrenInDepth($callbackFn, $depth);
            }
        }
    }
}