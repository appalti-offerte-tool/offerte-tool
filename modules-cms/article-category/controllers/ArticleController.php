<?php

class ArticleCategory_ArticleController extends Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var ArticleCategory_Service_Category
     */
    protected $_service;

    public function init()
    {
        $this->_service = new \ArticleCategory_Service_Category();

        $this->_helper->contextSwitch()
            ->addActionContext('fetch-all-for-tree-assignment', 'json')
            ->addActionContext('connect', 'json')
            ->addActionContext('disconnect', 'json')
            ->initContext();

        parent::init();
    }

    public function getResourceId()
    {
        return 'article-category';
    }

    public function fetchAllForTreeAssignmentAction()
    {
        $response = $this->_service->fetchAllByParentIdWithAssignment($this->_getParam('articleId'), $this->_getParam('node'), $this->_getAllParams());
        $response->setRowsetCallback(function($row) {
            $o = array(
                'id'        => $row['id'],
                'text'      => $row['name'],
                'isActive'  => $row['isActive'],
                'iconCls'   => 'edit-icon-16',
                'childrens' => $row['childrens']
            );

            $o['checked'] = (boolean) $row['assigned'];
            $o['leaf'] = 0 == $row['childrens'];

            return $o;
        });

        $this->view->assign($response->getRowset());
    }

    public function connectAction()
    {
        $service = new ArticleCategory_Service_ArticleCategory();

        try {
            $service->connectArticleWithCategory($this->_getParam('articleId'), $this->_getParam('categoryId'));
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function disconnectAction()
    {
        $service = new ArticleCategory_Service_ArticleCategory();

        try {
            $service->deleteArticleCategoryConnection($this->_getParam('articleId'), $this->_getParam('categoryId'));
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}