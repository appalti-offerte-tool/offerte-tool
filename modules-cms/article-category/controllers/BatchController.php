<?php

final class ArticleCategory_BatchController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    public function init()
    {
        $this->_helper->contextSwitch()
            ->addActionContext('fetch-all-categories-with-article-title', 'json')
            ->initContext();

        parent::init();
    }

    public function getResourceId()
    {
        return 'guest';
    }

    public function fetchAllCategoriesWithArticleTitleAction()
    {
        $category = new ArticleCategory_Service_Category();
        $categoryRow = $category->findByLuid($this->_getParam('luid'), false);

        if (null === $categoryRow) {
            $this->_helper->information('Unable to find article with luid: ' . $this->_getParam('luid'));
            return;
        }

        $ac = new ArticleCategory_Service_ArticleCategory();
        $response = $ac->fetchAllByCategoryWithArticleTitle($categoryRow);
        $this->view->assign($response->toArray());
    }
}