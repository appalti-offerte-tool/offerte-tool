<?php

class ArticleCategory_IndexController extends Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var ArticleCategory_Service_Category
     */
    protected $_service;

    public function init()
    {
        $this->_service = new \ArticleCategory_Service_Category();

        $this->_helper->contextSwitch()
            ->addActionContext('fetch-all-for-tree', 'json')
            ->addActionContext('connect-article-with-category', 'json')
            ->addActionContext('delete-article-category-connection', 'json')

            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('move', 'json')
            ->addActionContext('fetch', 'json')
            ->initContext();

        parent::init();
    }

    public function getResourceId()
    {
        return 'article-category';
    }

    public function indexAction()
    {
        // prepare layout
    }

    public function fetchAllForTreeAction()
    {
        $response = $this->_service->fetchAllByParentId($this->_getParam('node'), $this->_getAllParams());
        $response->setRowsetCallback(function($row) {
            return array(
                'id'            => $row['id'],
                'text'          => $row['name'],
                'isActive'      => $row['isActive'],
                'iconCls'       => 'edit-icon-16',
                'isProtected'   => (int) $row['isProtected']
            );
        });

        $this->view->assign($response->getRowset());
    }

    public function fetchAction()
    {
        $row = $this->_service->find($this->_getParam('id'));

        $this->view->row = $row->toArray();
        $this->view->success = true;
    }

    public function createAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $articleCategoryRow = $this->_service->create($this->_getAllParams());
            $this->view->articleCategoryId = $articleCategoryRow->getId();
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $articleCategoryRow = $this->_service->update($this->_getParam('id'), $this->_getAllParams());
            $this->view->articleCategoryId = $articleCategoryRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $articleCategoryRow = $this->_service->delete($this->_getParam('id'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function moveAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $articleCategoryRow = $this->_service->move($this->_getParam('id'), $this->_getParam('parentId'));
            $this->view->articleCategoryId = $articleCategoryRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function connectArticleWithCategoryAction()
    {
        $service = new ArticleCategory_Service_ArticleCategory();

        try {
            $service->connectArticleWithCategory($this->_getParam('articleId'), $this->_getParam('categoryId'));
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteArticleCategoryConnectionAction()
    {
        $service = new ArticleCategory_Service_ArticleCategory();

        try {
            $service->deleteArticleCategoryConnection($this->_getParam('articleId'), $this->_getParam('categoryId'));
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}