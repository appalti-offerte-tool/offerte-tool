<?php

final class ArticleCategory_Block_FormCategoryTree extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Category');
    }
}