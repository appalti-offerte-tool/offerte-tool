<?php

class ArticleCategory_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('articleCategory');
        $this->createColumn('articleCategory', 'name', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('articleCategory', 'parentId', self::TYPE_INT, 11, null, false);
        $this->createColumn('articleCategory', 'luid', self::TYPE_VARCHAR, 255, null, false);
        $this->createColumn('articleCategory', 'template', self::TYPE_VARCHAR, 255, null, false);
        $this->createColumn('articleCategory', 'isActive', self::TYPE_INT, 1, 1, true);
        $this->createIndex('articleCategory', array('parentId'), 'IX_parentId');
        $this->createForeignKey('articleCategory', array('parentId'), 'articleCategory', array('id'), 'FK_parentId');

        $this->createTable('articleCategoryRel');
        $this->createColumn('articleCategoryRel', 'articleId', self::TYPE_INT, 11, null, true);
        $this->createColumn('articleCategoryRel', 'categoryId', self::TYPE_INT, 11, null, true);
        $this->createUniqueIndexes('articleCategoryRel', array('articleId', 'categoryId'), 'UX_articleId');
        $this->createForeignKey('articleCategoryRel', array('articleId'), 'article', array('id'), 'FK_articleId');
        $this->createIndex('articleCategoryRel', array('categoryId'), 'IX_categoryId');
        $this->createForeignKey('articleCategoryRel', array('categoryId'), 'articleCategory', array('id'), 'FK_categoryId');
    }

    public function down()
    {
        $this->dropTable('articleCategoryRel');
        $this->dropTable('articleCategory');
    }
}