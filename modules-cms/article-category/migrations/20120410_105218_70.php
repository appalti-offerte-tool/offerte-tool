<?php

class ArticleCategory_Migration_20120410_105218_70 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createColumn('articleCategory', 'isProtected', self::TYPE_INT, 1, '0', true);
    }

    public function down()
    {
        $this->dropColumn('articleCategory', 'isProtected');
    }
}