<?php

class ArticleCategory_Migration_20111102_165245_56 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createColumn('articleCategory', 'createdAccountId', self::TYPE_INT, 11, null, true);
        $this->getDbAdapter()->update('articleCategory', array(
            'createdAccountId' => 1
        ));
        $this->createIndex('articleCategory', array('createdAccountId'), 'IX_createdAccountId');
        $this->createForeignKey('articleCategory', array('createdAccountId'), 'account', array('id'), 'FK_createdAccountId');
        $this->createColumn('articleCategory', 'createdDatetime', self::TYPE_DATETIME, null, null, true);

        $dt = new DateTime();
        $this->getDbAdapter()->update('articleCategory', array(
            'createdDatetime' => $dt->format('Y-m-d H:i:s')
        ));

        $this->createColumn('articleCategory', 'modifiedAccountId', self::TYPE_INT, 11, null, false);
        $this->createIndex('articleCategory', array('modifiedAccountId'), 'IX_modifiedAccountId');
        $this->createForeignKey('articleCategory', array('modifiedAccountId'), 'account', array('id'), 'FK_modifiedAccountId');
        $this->createColumn('articleCategory', 'modifiedDatetime', self::TYPE_DATETIME, null, null, false);
    }

    public function down()
    {
        $this->dropForeignKey('articleCategory', array('FK_modifiedAccountId'));
        $this->dropIndex('articleCategory', 'IX_modifiedAccountId');
        $this->dropColumn('articleCategory', 'modifiedAccountId');
        $this->dropColumn('articleCategory', 'modifiedDatetime');

        $this->dropForeignKey('articleCategory', array('FK_createdAccountId'));
        $this->dropIndex('articleCategory', 'IX_createdAccountId');
        $this->dropColumn('articleCategory', 'createdDatetime');
    }
}