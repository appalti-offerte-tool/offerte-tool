Ext.define('Module.ArticleCategory.Block.FormCategoryTree', {

    extend: 'Module.ArticleCategory.Tree',

    onLoadExpandAll: true,

    showCheckbox: true,

    initComponent: function() {

        this.callParent();

        this.on({
            checkchange: this.onCheckChange,
            scope: this
        });
    },

    onCheckChange: function(node, checked) {

        var action = checked ? 'connect' : 'disconnect';
        Ext.Ajax.request({
            url: link('article-category', 'article', action, {format: 'json'}),
            params: {
                articleId: this.articleId,
                categoryId: node.data.id
            },
            success: function(response, options) {
                var decResponse = Ext.decode(response.responseText);
                if (!decResponse.success) {
                    Application.notificate(decResponse.messages);
                    node.checked = !checked;
                }
            },
            scope: this
        });
    },

    setArticleId: function(articleId, forceReload) {

        this.articleId = articleId;
        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.articleId = articleId;

        if (true === forceReload) {
            this.doLoad();
        }
    }
});