Ext.define('Module.ArticleCategory.Model.ArticleCategory', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name', 'parentId', 'luid', 'template',
        {name: 'isProtected', type: 'boolean'},
        {name: 'isActive', type: 'boolean'}
    ]
});

Ext.define('Module.ArticleCategory.Form', {
    extend: 'Ext.form.Panel',
    bodyPadding: 5,

    articleCategoryId: null,
    parentId: null,

    trackResetOnLoad: true,
    waitMsgTarget: true,

    initComponent: function() {

        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.ArticleCategory.Model.ArticleCategory',
            type: 'json',
            root: 'row'
        });

        this.items = [{
            xtype: 'textfield',
            fieldLabel: lang('Name') + '<span class="asterisk">*</span>',
            name: 'name',
            allowBlank: false,
            anchor: '100%'
        }, {
            xtype: 'textfield',
            fieldLabel: lang('LUID') + '<span class="asterisk">*</span>',
            name: 'luid',
            allowBlank: false,
            anchor: '100%'
        }, {
            xtype: 'textfield',
            fieldLabel: lang('Template'),
            name: 'template',
            anchor: '100%'
        }, {
            xtype: 'checkbox',
            name: 'isProtected',
            fieldLabel: lang('Protected'),
            inputValue: '1',
            uncheckedValue: '0'
        }, {
            xtype: 'checkbox',
            name: 'isActive',
            fieldLabel: lang('Active'),
            inputValue: '1',
            uncheckedValue: '0'
        }];

        this.callParent();

        this.addEvents(
            /**
             * Fires when account is created
             *
             * @param {Module.ArticleCategory.Form}
             */
            'completed'
        );
    },

    doLoad: function() {

        if (!this.articleCategoryId) {
            return;
        }

        var params = Ext.applyIf({
            id: this.articleCategoryId,
            format: 'json'
        }, this.invokeArgs || {});

        this.form.load({
            url: link('article-category', 'index', 'fetch', params),
            waitMsg: Ext.LoadMask.prototype.msg,
            scope: this
        });
    },

    onSubmit: function(panel, w) {

        if (true !== this.form.isValid()) {
            return;
        }

        var params = Ext.apply({}, this.invokeArgs || {});
//        params.parentId = this.parentId || 0;
        params.parentId = 3;

        var action = this.articleCategoryId ? 'update' : 'create';
        if (this.articleCategoryId) {
            params['id'] = this.articleCategoryId;
        }

        this.form.submit({
            url: link('article-category', 'index', action, {format: 'json'}),
            method: 'post',
            params: params,
            waitMsg: Ext.LoadMask.prototype.msg,
            success: function(form, action) {
                var decResponse = Ext.decode(action.response.responseText);
                Application.notificate(decResponse.messages);
                if (action.result.success) {
                    this.fireEvent('completed', decResponse.articleCategoryId, action.response);
                    w.close();
                }
            },
            scope: this
        });
    },

    showInWindow: function() {

        var w = new Ext.Window({
            title:  this.articleCategoryId ? lang('Update article category') : lang('Create article category'),
            iconCls: 'm-article-category-icon-16',
            resizable: false,
            width: 415,
            modal: true,
            border: false,
            items: [this],
            buttons: [{
                iconCls: 'icon-submit-16',
                text: lang('Submit'),
                handler: function() {
                    this.onSubmit(this, w);
                },
                scope: this
            }, {
                text: lang('Cancel'),
                handler: function() {
                    w.close();
                }
            }],
            scope: this
        });

        w.show();
        this.doLoad();

        return w;
    }
});