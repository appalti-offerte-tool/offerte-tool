Ext.define('Module.ArticleCategory.Tree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.module.article-category.tree',

    iconCls: 'm-article-category-icon-16',

    dataUrl: null,
    animate: false,
    enableExpandAllTool: false,
    onLoadExpandAll: false,

    rootNode: null,
    permissions: true,
    readOnly: false,
    showCheckbox: false,

    initComponent: function() {

        this.viewConfig = Ext.apply(this.viewConfig || {}, {
            plugins: {
                ptype: 'treeviewdragdrop'
            }
        });

        this.tools = [];
        if (true === this.enableExpandAllTool) {
            this.tools.push({
                type: 'expand',
                handler: function() {
                    this.expandAll();
                },
                scope: this
            });
        }

        var fields = [
            'id', 'text', 'leaf', 'expanded',
            {name: 'isProtected', type: 'boolean'}
        ];
        if (this.showCheckbox) {
            fields.push({name: 'checked', type: 'boolean'});
        }

        this.store = Ext.create('Ext.data.TreeStore', {
            clearOnLoad: false,
            proxy: {
                type: 'ajax',
                url: this.dataUrl || link('article-category', 'index', 'fetch-all-for-tree', {format: 'json'})
            },
            root: this.rootNode || {
                text: lang('All categories'),
//                id: '0',
                id: '3',
                expanded: false
            },

            fields: fields
        });

        if (!this.readOnly && this.permissions) {
            this.tbar = [{
                text: lang('Create'),
                iconCls: 'icon-create-16',
                handler: function() {
                    this.onCreateCategory(null);
                },
                scope: this
            }];
        }

        this.bbar = ['->', {
            iconCls: 'x-tbar-loading',
            handler: function() {
                this.doLoad();
            },
            scope: this
        }];

        this.callParent();

        this.on({
            itemcontextmenu: this.onContextMenu,
            scope: this
        });

        this.getView().on('drop', this.onTreeViewDragDrop, this);
    },

    doLoad: function(o) {

        var node = ((o && o.node) || this.getRootNode());
        if (true !== this.getStore().clearOnLoad) {
            node.removeAll();
        }

        this.getStore().load(Ext.apply(o || {}, {
            node: node,
            callback: function() {
                if (node == this.getRootNode()) {
                    this.onLoadExpandAll ? this.expandAll() : node.expand();
                }
            },
            scope: this
        }));

        return this;
    },

    onCreateCategory: function(pRecord) {

        Application.require([
            'article-category/./form'
        ], function() {
            var f = new Module.ArticleCategory.Form({
                parentId: pRecord ? pRecord.get('id') : null
            });
            f.on('completed', function(articleCategoryId) {
                this.doLoad({
                    node: pRecord || this.getRootNode()
                });
            }, this);
            f.showInWindow();
        }, this);
    },

    onUpdateCategory: function(record) {
        Application.require([
            'article-category/./form'
        ], function() {
            var f = new Module.ArticleCategory.Form({
                parentId: record.get('parentId'),
                articleCategoryId: record.get('id')
            });

            f.on('completed', function(articleCategoryId) {
                this.doLoad({
                    node: record.parentNode || this.getRootNode()
                });
            }, this);

            f.showInWindow();
        }, this);
    },

    onDeleteCategory: function(record) {

        Ext.Msg.confirm(lang('Confirmation'), lang('The category "{0}" will be removed', record.get('text')), function(b) {

            Ext.Ajax.request({
                url: link('article-category', 'index', 'delete', {format: 'json'}),
                params: {
                    id: record.get('id')
                },
                success: function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);

                    this.doLoad({
                        node: record.parentNode || this.getRootNode()
                    });
                },
                scope: this
            });
        }, this);

    },

    onTreeViewDragDrop: function(node, data, oRecord, position, eOpts) {

        this.view.loadMask.show();

        var pRecord = null;
        switch(position) {
            case 'after':
            case 'before':
                pRecord = oRecord.parentNode;
                break;

            case 'append':
            default:
                pRecord = oRecord;

        }
        Ext.Ajax.request({
            url: link('article-category', 'index', 'move', {format: 'json'}),
            params: {
                parentId: pRecord.get('id'),
                id: data.records[0].get('id')
            },
            success: function(response, options) {
                var decResponse = Ext.decode(response.responseText);
                if (!decResponse.success) {
                    Application.notificate(decResponse.messages);
                    this.getStore().doLoad();
                }
            },
            callback: function() {
                this.view.loadMask.hide();
            },
            scope: this
        });
    },

    onContextMenu: function(w, record, item, index, e) {

        e.stopEvent();

        var isRoot = record.get('id') == this.getRootNode().get('id');
        var isProtected = !!record.get('isProtected');
        var items = [];
//        if (!this.readOnly && this.permissions && !isProtected) {
//            items.push({
//                text: lang('Create') + (isRoot ? '' : (' on ' + record.get('text'))),
//                iconCls: 'icon-create-16',
//                handler: function() {
//                    this.onCreateCategory(record);
//                },
//                scope: this
//            });
//        }

        if (!isRoot && !this.readOnly && this.permissions) {
            items.push({
                text: lang('Edit') + ': "' + record.get('text') + '"',
                iconCls: 'icon-update-16',
                handler: function() {
                    this.onUpdateCategory(record);
                },
                scope: this
            });
        }

        if (!this.readOnly && this.permissions && !isProtected) {
            items.push({
                text: lang('Delete'),
                iconCls: 'icon-delete-16',
                handler: function() {
                    this.onDeleteCategory(record);
                },
                scope: this
            });
        }

        if (items.length > 0) {
            items.push('-');
        }

        items.push({
            text: lang('Refresh'),
            iconCls: 'icon-refresh-16',
            handler: function() {
                this.doLoad({
                    node: record
                });
            },
            scope: this
        });

        var m = new Ext.menu.Menu({
            items: items
        });

        m.showAt(e.getXY());
    },

    showInWindow: function(configuration) {

        var w = new Ext.Window(Ext.apply({
            title: lang('Article categories'),
            iconCls: 'm-article-category-icon-16',
            resizable: false,
            width: 415,
            height: 450,
            layout: 'fit',
            modal: true,
            border: false,
            items: [this],
            buttons: [{
                text: lang('Cancel'),
                handler: function() {
                    w.close();
                }
            }],
            scope: this
        }, configuration || {}));

        w.show();
        this.doLoad();

        return w;
    }
});