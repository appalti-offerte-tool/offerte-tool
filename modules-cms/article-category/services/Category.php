<?php

class ArticleCategory_Service_Category extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Article_Model_DbTable_Category
     */
    protected $_table;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new \ArticleCategory_Model_DbTable_Category();

        $this->_attachValidationRules('default', array(
            'name'      => array(array('StringLength', 1, 255), 'presence' => 'required', 'allowEmpty' => false),
            'parentId'  => array(array('id', true), 'presence' => 'required', 'allowEmpty' => true),
            'luid'      => array(array('StringLength', 1, 255), 'presence' => 'required', 'allowEmpty' => false),
            'isActive'  => array('presence' => 'required', 'allowEmpty' => true)
        ));

        parent::_init();
    }

    public function fetchAllByParentId($parentId = null, array $params = array())
    {
        $select = $this->_table->select(true);
        $select->order('name', 'ASC');

        if (empty($parentId)) {
            $select->where('parentId IS NULL');
        } else {
            $select->where('parentId = ?', $parentId, Zend_Db::INT_TYPE);
        }


        return $this->getDecorator('response')->decorate($select);
    }

    public function fetchAllByParentIdWithAssignment($articleId, $parentId = null, array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(
                array('c' => $this->_table->getTableName())
            );
        $select->order('name', 'ASC');

        if (empty($parentId)) {
            $select->where('parentId IS NULL');
        } else {
            $select->where('parentId = ?', $parentId, Zend_Db::INT_TYPE);
        }

        $detectChildrenSelect = $this->_table->select()->from($this->_table->getTableName(), array())->columns('COUNT(*)');

        $select->joinLeft(
            array('acr' => 'articleCategoryRel'),
            $this->getAdapter()->quoteInto('acr.categoryId = c.id AND acr.articleId = ?', $articleId, Zend_Db::INT_TYPE),
            array(
                'assigned'     => new \Zend_Db_Expr('CASE WHEN COUNT(acr.id) > 0 THEN 1 ELSE 0 END'),
                'childrens'    => new \Zend_Db_Expr('(' . $detectChildrenSelect->where('parentId = c.id') . ')')
            )
        );
        $select->group('c.id');

        return $this->getDecorator('response')->decorate($select);
    }

    /**
     * Find category row
     *
     * @param int $id
     * @param OSDN_Exception $throwException
     *
     * @return ArticleCategory_Model_DbTable_CategoryRow
     */
    public function find($id, $throwException = true)
    {
        $row = $this->_table->findOne($id);
        if (null === $row && true === $throwException) {
            throw new OSDN_Exception('Unable to find #' . $id);
        }

        return $row;
    }

    /**
     * @param string $luid
     * @param boolean $throwException
     *
     * @return \ArticleCategory_Model_DbTable_CategoryRow
     */
    public function findByLuid($luid, $throwException = true)
    {
        $articleCategoryRow = $this->_table->fetchRow(array('luid = ?' => $luid));
        if (null === $articleCategoryRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find category luid: ' . $luid);
        }

        return $articleCategoryRow;
    }

    public function create(array $data)
    {
        $f = $this->_validate($data);

        $articleCategoryRow = $this->_table->createRow($f->getData());

        $this->getAdapter()->beginTransaction();
        try {

            $articleCategoryRow->save();

            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $articleCategoryRow;
    }

    public function update($id, array $data)
    {
        $articleCategoryRow = $this->find($id);
        $f = $this->_validate($data);

        $this->getAdapter()->beginTransaction();
        try {
            $articleCategoryRow->setFromArray($f->getData());
            $articleCategoryRow->save();

            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $articleCategoryRow;
    }

    public function move($id, $parentId)
    {
        $articleCategoryRow = $this->find($id);
        $articleCategoryRow->parentId = $parentId;

        $articleCategoryRow->save();

        return $articleCategoryRow;
    }

    public function delete($id)
    {
        $articleCategoryRow = $this->find($id);

        try {
            $articleCategoryRow->delete();
        } catch(\Exception $e) {
            throw new OSDN_Exception('Unable to delete category. Some relations are present.');
        }

        return true;
    }
}