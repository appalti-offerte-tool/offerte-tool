<?php

class ArticleCategory_Service_ArticleCategory extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \ArticleCategory_Model_DbTable_Category
     */
    protected $_table;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new \ArticleCategory_Model_DbTable_Category();

        parent::_init();
    }

    public function fetchAllByCategoryWithArticleTitle(\ArticleCategory_Model_DbTable_CategoryRow $categoryRow, array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(
                array('ac' => $this->_table->getTableName()),
                array('id', 'name')
            )
            ->where('ac.isActive = 1')
            ->where('(ac.parentId = ?', $categoryRow->getId(), Zend_Db::INT_TYPE)
            ->orWhere('ac.id = ?)', $categoryRow->getId(), Zend_Db::INT_TYPE);

        $response = $this->getDecorator('response')->decorate($select);
        $response->setScope($this);
        $response->setRowsetCallback(function($row, $rowset, $scope) {

            $select = $scope->getAdapter()->select()
                ->from(
                    array('a' => 'article'),
                    array('id', 'title', 'createdDatetime', 'modifiedDatetime')
                )
                ->joinLeft(
                    array('acr' => 'articleCategoryRel'),
                    'a.id = acr.articleId',
                    array()
                )
                ->where('a.isActive = 1')
                ->where('acr.categoryId = ?', $row['id'], Zend_Db::INT_TYPE);

            $row['articles'] = $select->query()->fetchAll();

            return $row;
        });

        return $response;
    }


    public function fetchAllByCategory(\ArticleCategory_Model_DbTable_CategoryRow $categoryRow, array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(
                array('ac' => $this->_table->getTableName()),
                array('id')
            )
            ->where('ac.isActive = 1')
            ->where('(ac.parentId = ?', $categoryRow->getId(), Zend_Db::INT_TYPE)
            ->orWhere('ac.id = ?)', $categoryRow->getId(), Zend_Db::INT_TYPE);

        $response = $this->getDecorator('response')->decorate($select);
        $response->setScope($this);
        $response->setRowsetCallback(function($row) {
            return $row['id'];
        });
        $categoriesIds = $response->getRowset();
        unset($select);

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('a' => 'article')
            )
            ->join(
                array('acr' => 'articleCategoryRel'),
                'a.id = acr.articleId',
                array()
            )
            ->where('acr.categoryId IN(?)', $categoriesIds)
            ->where('a.isActive = 1');

        $table = new Article_Model_DbTable_Article();
        $response = $this->getDecorator('response')->decorate($select);
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });

        return $response;
    }

    public function connectArticleWithCategory($articleId, $categoryId)
    {
        $table = new \ArticleCategory_Model_DbTable_ArticleCategoryRel($this->getAdapter());

        try {
            $table->insert(array(
                'articleId'  => (int) $articleId,
                'categoryId' => (int) $categoryId
            ));
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to connect article to category');
        }

        return true;
    }

    public function deleteArticleCategoryConnection($articleId, $categoryId)
    {
        $table = new \ArticleCategory_Model_DbTable_ArticleCategoryRel($this->getAdapter());

        try {
            $affectedRows = $table->deleteQuote(array(
                'articleId = ?'  => (int) $articleId,
                'categoryId = ?' => (int) $categoryId
            ));
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to delete connection article with category');
        }

        return false !== $affectedRows;
    }

    public function deleteAllArticleConnections(\Article_Model_DbTable_ArticleRow $articleRow)
    {
        $dt = new \DateTime();
        foreach($articleRow->getCategoryRowset() as $categoryRow) {
            $categoryRow->refreshModifiedDateTime($dt)->save();
        }

        $acTable = new ArticleCategory_Model_DbTable_ArticleCategoryRel();
        $acTable->deleteQuote(array(
            'articleId = ?' => $articleRow->getId()
        ));

        return true;
    }
}