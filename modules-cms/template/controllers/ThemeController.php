<?php

final class Template_ThemeController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'guest';
    }

    public function comboBoxAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');
    }
}