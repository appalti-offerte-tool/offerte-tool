<?php

class Article_ArticleFileStockController extends \Zend_Controller_Action implements \FileStock_Instance_EntityFileStockControllerInterface
{
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'article:form';
    }

    public function init()
    {
        $this->_service = new \Article_Service_ArticleFileStock($this->getEntityType());

        $this->_helper->contextSwitch()
           ->addActionContext('fetch-all', 'json')
           ->addActionContext('fetch', 'json')
           ->addActionContext('create', 'json')
           ->addActionContext('update', 'json')
           ->addActionContext('delete', 'json')
           ->addActionContext('set-default-thumbnail', 'json')
           ->initContext();
    }

    public function getEntityType()
    {
        return 'ARTICLE';
    }

    public function downloadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $fileStockRow = $this->_service->find($this->_getParam('articleId'), $this->_getParam('id'));

        $fileStockService = new FileStock_Service_FileStock();
        $fileStockService->download($fileStockRow, $this->getResponse(), true);
    }

    public function fetchAllAction()
    {
        try {
            $response = $this->_service->fetchAllFileStockByArticleIdWithResponse($this->_getParam('articleId'));
            $this->view->assign($response->toArray());
            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAction()
    {
        $fileStockRow = $this->_service->find($this->_getParam('articleId'), $this->_getParam('fileStockId'));

        $this->view->data = $fileStockRow->toArray();
        $this->view->success = true;
    }

    public function createAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');

        try {

            list($articleRow, $fileStockRow) = $this->_service->create($this->_getAllParams());

            $this->view->success = true;
            $this->view->entityId = $articleRow->getId();
            $this->view->fileStockId = $fileStockRow->getId();

            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);

        } catch(OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');

        try {

            $this->_service->update($this->_getAllParams());

            $this->view->success = true;
            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);

        } catch(OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->_service->delete($this->_getParam('articleId'), $this->_getParam('fileStockId'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }

    public function setDefaultThumbnailAction()
    {
        try {
            $this->_service->setDefaultThumbnail($this->_getParam('articleId'), $this->_getParam('fileStockId'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }
}