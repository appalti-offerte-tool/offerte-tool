<?php

class Article_MainController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Article_Service_Article
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_service = new \Article_Service_Article();

        $this->_helper->contextSwitch()
            ->addActionContext('index', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch-all-by-parent-luid', 'json')
            ->setContext(
                'iphone', array(
                    'suffix'    => 'iphone',
                    'headers'    => array(
                        'Content-type' => 'text/html;charset=utf-8'
                    )
                )
            )
            ->addActionContext('view', 'iphone')
            ->initContext();

        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'article:list';
    }

    public function indexAction()
    {
        // prepare layout
    }

    public function fetchAllAction()
    {
        $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
        $this->view->assign($response->toArray());
    }

    public function createAction()
    {
        try {
            $articleRow = $this->_service->create($this->_getAllParams());
            $this->view->articleId = $articleRow->getId();
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        try {
            $articleRow = $this->_service->update($this->_getParam('articleId'), $this->_getAllParams());
            $this->view->articleId = $articleRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->view->success = $this->_service->delete($this->_getParam('id'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}