<?php

class Article_PageController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Article_Service_Article
     */
    protected $_service;

    public function init()
    {
        $this->_service = new \Article_Service_Article();

        $isIphoneLayout = false;
        if (
            false !== stripos($_SERVER['HTTP_USER_AGENT'], 'iphone') ||
            false !== stripos($_SERVER['HTTP_USER_AGENT'], 'ipad')
        ) {
            if (false == $this->_getParam('format', false)) {
                $this->getRequest()->setParam('format', 'iphone');
            }

            $isIphoneLayout = true;
        }

        $this->_helper->contextSwitch()
            ->addContext('iphone', array(
                'suffix'    => 'iphone'
            ))
            ->addActionContext('index', 'iphone')
            ->addActionContext('id', 'iphone')
            ->initContext();

        parent::init();

        /**
         * @FIXME
         * Check why the layout configuration does not work before "context" creation
         */
        if (true === $isIphoneLayout) {
            $this->_helper->layout->setLayout('iphone');
        }
    }

    public function getResourceId()
    {
        return 'guest';
    }

    public function indexAction()
    {
        $luid = $this->_getParam('luid');

        $articleRow = $this->_service->fetchByLuid($luid);
        $this->_doRenderArticle($articleRow);
    }

    public function idAction()
    {
        $id = $this->_getParam('id');
        $articleRow = $this->_service->find($id);

        $this->_doRenderArticle($articleRow);
    }

    protected function _doRenderArticle($articleRow)
    {
        $articleFileStock = new Article_Service_ArticleFileStock();
        $articleFileStockRowset = $articleFileStock->fetchAllFileStockByArticleIdWithResponse($articleRow->getId());
        foreach($articleFileStockRowset->getRowset() as $afsRow) {
            if($afsRow['isThumbnail']) {
                $this->view->thumb = $afsRow['link'];
                break;
            }
        }

//        $categoryRow = $articleRow->getCategoryRow();
//
//        while(null === ($template = $categoryRow->template)) {
//
//            if (null === ($parentRow = $categoryRow->getParentRow())) {
//                break;
//            }
//            $categoryRow = $parentRow;
//
//            if (null !== $categoryRow->template) {
//                $template = $categoryRow->template;
//            }
//        }

        $this->view->articleRow = $articleRow;

//        $this->_helper->layout->setLayout('iphone');

        /**
         * @FIXME
         *
         * The action and controller name should be identified via "context" configuration
         */
        $viewRenderer = $this->_helper->viewRenderer;
        $viewRenderer->setScriptAction('view');
        $this->getRequest()->setControllerName('index');
    }
}