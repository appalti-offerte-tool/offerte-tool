<?php

class Article_IndexController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Article_Service_Article
     */
    protected $_service;

    public function init()
    {
        $this->_service = new \Article_Service_Article();

        if (Zend_Auth::getInstance()->getIdentity()->isGuest()) {
            $this->_helper->layout->setLayout('guest');
        }

        $this->_helper->contextSwitch()
            ->addActionContext('index', 'json')
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch-all-by-category', 'json')
            ->addActionContext('fetch-all-by-parent-luid', 'json')
            ->setContext(
                'iphone', array(
                    'suffix'    => 'iphone',
                    'headers'    => array(
                        'Content-type' => 'text/html;charset=utf-8'
                    )
                )
            )
            ->addActionContext('view', 'iphone')
            ->initContext();

        parent::init();
    }

    public function getResourceId()
    {
        return 'guest';
    }

    public function indexAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(3);

        $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
        $table = new Article_Model_DbTable_Article();
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });
        $this->view->response = $response;

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }

    /**
     * @deprecated
     */
    public function fetchAllByParentLuidAction()
    {
        $response = $this->_service->fetchAllByParentLuid($this->_getParam('luid'), $this->_getAllParams());
        $this->view->assign($response->toArray());
    }

    /**
     * @deprecated
     */
    public function fetchAllByCategoryAction()
    {
        $response = $this->_service->fetchAllOverviewByCategoryId($this->_getParam('categoryId'), $this->_getAllParams());

        $articleFileStock = new Article_Service_ArticleFileStock();
        $response->setRowsetCallback(function($row) use ($articleFileStock) {

            unset($row['content']);

            $row['overview'] = str_replace(array('<br/>', '<br>'), "\n", $row['overview']);
            $row['overview'] = strip_tags($row['overview']);
            $row['overview'] = html_entity_decode($row['overview'], ENT_QUOTES, 'UTF-8');

            $fileStockRawRow = $articleFileStock->fetchFileStockThumbnailRowByArticle($row['id']);
            if (isset($fileStockRawRow['link'])) {
                $row['thumbnail'] = $fileStockRawRow['link'];
            }

            return $row;
        });

        $this->view->assign($response->toArray());
    }

    public function viewAction()
    {
        $articleRow = $this->_service->find($this->_getParam('id'));

        $articleFileStock = new Article_Service_ArticleFileStock();
        $articleFileStockRowset = $articleFileStock->fetchAllFileStockByArticleIdWithResponse($articleRow->getId());
        foreach($articleFileStockRowset->getRowset() as $afsRow) {
            if ($afsRow['isDefaultThumbnail']) {
                $this->view->thumb = $afsRow['link'];
                break;
            }
        }

        $this->view->articleRow = $articleRow;
    }
}