<?php

class Article_Service_ArticleFileStock extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \Article_Model_DbTable_Article
     */
    protected $_table;

    /**
     * @var \FileStock_Service_FileStock
     */
    protected $_fileStock;

    public function _init()
    {
        $this->_table = new \Article_Model_DbTable_Article($this->getAdapter());
        $this->_fileStock = new \FileStock_Service_FileStock(array(
            'baseFilePath'         => APPLICATION_PATH . '/data/public-article-images',
            'useCustomExtension'   => false,
            'identity'             => 'article'
        ));
    }

    /**
     * Retrieve the filestock rows by article
     *
     * @param int $articleId
     * @return Zend_Db_Table_Rowset_Abstract
     * @throws OSDN_Exception
     */
    public function fetchAllFileStockByArticleIdWithResponse($articleId)
    {
        $articleRow = $this->_table->findOne($articleId);
        if (empty($articleRow)) {
            throw new \OSDN_Exception('Unable to find #' . $articleId);
        }

        $fileStockTable = new \FileStock_Model_DbTable_FileStock($this->getAdapter());

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('df'    => 'articleFileStockRel'),
                array()
            )
            ->join(
                array('f'    => 'fileStock'),
                'f.id = df.fileStockId',
                $fileStockTable->getAllowedColumns()
            )
            ->join(
                array('dfThumbnail' => 'articleFileStockRel'),
                'dfThumbnail.parentId = df.id AND dfThumbnail.isThumbnail = 1',
                array(
                    'thumbnailFileStockId' => 'fileStockId',
                    'isThumbnail',
                    'isDefaultThumbnail'
                )
            )
            ->where('df.articleId = ?', $articleRow->getId(), Zend_Db::INT_TYPE)
            ->where('df.parentId IS NULL');

        $response = $this->getDecorator('response')->decorate($select);


        $storage = $this->_fileStock->factory();

        $response->setScope($this);
        $response->setRowsetCallback(function($row, $rowset, $scope) use ($storage, $articleRow) {

            $thumbnailFileStockRow = $scope->find($articleRow->getId(), $row['thumbnailFileStockId']);
            $row['thumbnailLink'] = $storage->toFilePathHost($thumbnailFileStockRow);

            $fileStockRow = $scope->find($articleRow->getId(), $row['id']);
            $row['link'] = $storage->toFilePathHost($fileStockRow);

            return $row;
        });

        return $response;
    }

    public function fetchFileStockThumbnailRowByArticle($articleId)
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('df'    => 'articleFileStockRel'),
                array()
            )
            ->join(
                array('f'    => 'fileStock'),
                'f.id = df.fileStockId',
                array('id')
            )
            ->where('df.articleId = ?', $articleId, Zend_Db::INT_TYPE)
            ->where('isThumbnail = 1')
            ->limit(1);

        $fileStockId = $select->query()->fetchColumn(0);
        if (empty($fileStockId)) {
            return null;
        }

        if (null == ($fileStockRow = $this->_fileStock->find($fileStockId))) {
            return null;
        }

        $row = $fileStockRow->toArray();

        $storage = $this->_fileStock->factory();
        $row['link'] = $storage->toFilePathHost($fileStockRow);

        return $row;
    }

    public function find($articleId, $fileStockId)
    {
        /**
         * @todo
         * Implement verification on assignment article to filestock
         */
        return $this->_fileStock->find($fileStockId);
    }

    public function create(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {
        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }

        if (null === ($articleRow = $this->_table->findOne($data['articleId']))) {
            $articleRow = $this->_table->createRow(array('title' => uniqid('Untitled ')));
            $articleRow->save();
        }

        $articleFileStockRelation = new Article_Model_DbTable_ArticleFileStockRel();
        $articleFileStockRelationRow = null;
        $fileStockRow = $this->_fileStock->create($transfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
            use ($articleRow, $articleFileStockRelation, & $articleFileStockRelationRow) {

            $articleFileStockRelationRow = $articleFileStockRelation->createRow(array(
                'fileStockId'           => $fileStockRow->getId(),
                'articleId'             => $articleRow->getId()
            ));
            $articleFileStockRelationRow->save();
        });

        $rawFileTransfer = new FileStock_Service_Transfer_Raw();

        $storage = $this->_fileStock->factory();
        $link = $storage->toFilePath($fileStockRow, true);

        $rawFileTransfer->addFile($link);

        /**
         * @FIXME
         */
        require_once APPLICATION_PATH . '/modules/file-stock/views/scripts/filters/File/Duplicate.php';
        $rawFileTransfer->addFilter(new FileStock_View_Filter_File_Duplicate($link . '.tmp', null, true));

        $isf = new Zend_Filter_ImageSize();
        $isf->setHeight(100);
        $isf->setWidth(100);
        $isf->setThumnailDirectory(dirname($link));
        $rawFileTransfer->addFilter($isf);

        $fileStockThumRow = $this->_fileStock->create($rawFileTransfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
            use ($articleRow, $articleFileStockRelationRow, $articleFileStockRelation) {

            $hasPredefinedThumbnail = $articleFileStockRelation->count(array(
                'articleId = ?' => $articleRow->getId(),
                'isThumbnail = 1'
            )) > 0;

            $articleRow->getTable()->getAdapter()->insert('articleFileStockRel', array(
                'fileStockId'            => $fileStockRow->getId(),
                'articleId'         => $articleRow->getId(),
                'isThumbnail'       => 1,
                'isDefaultThumbnail'=> ! $hasPredefinedThumbnail,
                'parentId'          => $articleFileStockRelationRow->id
            ));
        });

        return array($articleRow, $fileStockRow);
    }

    public function update(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {
        $articleRow = $this->_table->findOne($data['articleId']);
        if (empty($articleRow)) {
            throw new OSDN_Exception('Unable to find article #' . $data['articleId']);
        }

        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }

        $this->getAdapter()->beginTransaction();
        try {
            $fileStockRow = $this->_fileStock->update($transfer, $data);

            $articleFileStockRelation = new Article_Model_DbTable_ArticleFileStockRel();
            $articleFileStockRelationRow = $articleFileStockRelation->fetchRow(array(
                'articleId = ?'    => $articleRow->getId(),
                'fileStockId = ?'       => $fileStockRow->getId(),
                'parentId IS NULL'
            ));

            if (null !== $articleFileStockRelationRow) {

                foreach($articleFileStockRelationRow->getChildrenRowset() as $row) {
                    $this->_fileStock->delete($row->fileStockId, function($fileStockRow) use ($row) {
                        $row->delete();
                    });
                }
            }

            $rawFileTransfer = new FileStock_Service_Transfer_Raw();

            $storage = $this->_fileStock->factory();
            $link = $storage->toFilePath($fileStockRow, true);

            $rawFileTransfer->addFile($link);

            /**
             * @FIXME
             */
            require_once APPLICATION_PATH . '/modules/file-stock/views/scripts/filters/File/Duplicate.php';
            $rawFileTransfer->addFilter(new FileStock_View_Filter_File_Duplicate($link . '.tmp', null, true));

            $isf = new Zend_Filter_ImageSize();
            $isf->setHeight(100);
            $isf->setWidth(100);
            $isf->setThumnailDirectory(dirname($link));
            $rawFileTransfer->addFilter($isf);

            $fileStockThumRow = $this->_fileStock->create($rawFileTransfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow) use ($articleRow, $articleFileStockRelationRow) {
                $articleRow->getTable()->getAdapter()->insert('articleFileStockRel', array(
                    'fileStockId'       => $fileStockRow->getId(),
                    'articleId'    => $articleRow->getId(),
                    'isThumbnail'  => 1,
                    'parentId'     => $articleFileStockRelationRow->id
                ));
            });


            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $fileStockRow;
    }

    public function delete($articleId, $fileStockId)
    {
        $articleRow = $this->_table->findOne($articleId);
        if (empty($articleRow)) {
            throw new OSDN_Exception('Unable to find article #' . $articleId);
        }

        $this->getAdapter()->beginTransaction();
        try {

            $articleFileStockRelation = new \Article_Model_DbTable_ArticleFileStockRel();
            $scope = $this->_fileStock;

            $callbackFn = function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
                use ($articleRow, $articleFileStockRelation, $scope, & $callbackFn)
            {
                $articleFileStockRelationRow = $articleFileStockRelation->fetchRow(array(
                    'articleId = ?'    => $articleRow->getId(),
                    'fileStockId = ?'       => $fileStockRow->getId()
                ));

                if (null !== $articleFileStockRelationRow) {

                    foreach($articleFileStockRelationRow->getChildrenRowset() as $row) {
                        $scope->delete($row->fileStockId, $callbackFn);
                    }

                    $articleFileStockRelationRow->delete();
                }
            };

            $result = $this->_fileStock->delete($fileStockId, $callbackFn);
            $this->getAdapter()->commit();

        } catch (\Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        }

        return (boolean) $result;
    }

    public function deleteByArticle(Article_Model_DbTable_ArticleRow $articleRow)
    {
        $articleFileStockRelation = new \Article_Model_DbTable_ArticleFileStockRel();
        $rowset = $articleFileStockRelation->fetchAll(array(
            'articleId = ?'    => $articleRow->getId()
        ));

        foreach($rowset as $row) {
            $this->delete($articleRow->getId(), $row->fileStockId);
        }

        return true;
    }

    public function setDefaultThumbnail($articleId, $fileStockId)
    {
        $articleFileStockRelation = new \Article_Model_DbTable_ArticleFileStockRel();

        $this->getAdapter()->beginTransaction();
        try {

            $articleFileStockRelation->updateQuote(array(
                'isDefaultThumbnail' => 0
            ), array(
                'articleId = ?' => (int) $articleId,
                'isThumbnail = 1'
            ));

            $articleFileStockRelationRow = $articleFileStockRelation->fetchRow(array(
                'articleId = ?' => (int) $articleId,
                'fileStockId = ?'    => (int) $fileStockId
            ));

            if (null == $articleFileStockRelationRow) {
                throw new OSDN_Exception('Unable to find row');
            }

            $affectedRows = $articleFileStockRelation->updateQuote(array(
                'isDefaultThumbnail' => 1
            ), array(
                'articleId = ?' => (int) $articleId,
                'parentId = ?'  => (int) $articleFileStockRelationRow->id,
                'isThumbnail = 1'
            ));

            if (0 == $affectedRows) {
                throw new \OSDN_Exception('Unable to change default thumbnail');
            }

            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }
    }
}