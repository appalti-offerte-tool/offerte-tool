<?php

class Article_Service_Article extends OSDN_Application_Service_Dbable
{
    /**
     * @var Article_Model_DbTable_Article
     */
    protected $_table;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new \Article_Model_DbTable_Article();

        $this->_attachValidationRules('default', array(
            'title'     => array(array('StringLength', 1, 255), 'presence' => 'required', 'allowEmpty' => false),
            'luid'      => array(array('StringLength', 1, 255), 'presence' => 'required', 'allowEmpty' => false),
            'overview'  => array('presence' => 'required', 'allowEmpty' => true)
        ));

        parent::_init();
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \Article_Model_DbTable_ArticleRow
     */
    public function find($id, $throwException = true)
    {
        $articleRow = $this->_table->findOne($id);
        if (null === $articleRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find article #' . $id);
        }

        return $articleRow;
    }

    /**
     * @param string $luid
     * @param boolean $throwException
     *
     * @return \Article_Model_DbTable_ArticleRow
     */
    public function fetchByLuid($luid, $throwException = true)
    {
        $articleRow = $this->_table->fetchRow(array(
            'luid = ?' => (string) $luid
        ));

        if (null === $articleRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find article luid: #' . ($luid ?: 'none'));
        }

        return $articleRow;
    }

    /**
     * Create the new article row
     *
     * @param array $data
     * @return Article_Model_DbTable_ArticleRow
     */
    public function create(array $data)
    {
        $f = $this->_validate($data);

        $articleRow = $this->_table->createRow($f->getData());

        $this->getAdapter()->beginTransaction();
        try {

            $articleRow->save();

            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $articleRow;
    }

    /**
     * @param array $params
     * @return OSDN_Db_Response
     */
    public function fetchAllWithResponse(array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('a' => $this->_table->getTableName()),
                $this->_table->getAllowedColumns(\OSDN_Db_Table_Abstract::OMIT_FETCH_ROW)
            );

        if (!empty($params['categoryId']) || !empty($params['luid'])) {
            $select->joinLeft(
                array('acr' => 'articleCategoryRel'),
                'acr.articleId = a.id',
                array(
                    'categoryId'
                )
            );

            if (!empty($params['categoryId'])) {
                $select->where('acr.categoryId = ?', $params['categoryId'], Zend_Db::INT_TYPE);
            }

            if (!empty($params['luid'])) {
                $select->joinLeft(
                    array('ac' => 'articleCategory'),
                    'ac.id = acr.categoryId',
                    array()
                );
                $select->where('ac.luid = ?', (string) $params['luid']);
            }
        }

        $select->order('createdDatetime DESC');
        $fields = array(
            'a.title'    => 'title',
            'a.isActive' => 'isActive'
        );

        $this->_initDbFilter($select, $this->_table, $fields)->parse($params);
        return $this->getDecorator('response')->decorate($select);
    }

    public function fetchAllModifiedDatetimeByLuidCollection($luids)
    {
        if (!is_array($luids)) {
            $luids = array($luids);
        }

        $select = $this->getAdapter()->select()
            ->from($this->_table->getTableName(), array('luid', 'modifiedDatetime'))
            ->where('modifiedDatetime IS NOT NULL')
            ->where('luid IN(?)', $luids);

        $query = $select->query();
        $output = array();
        while(false != ($row = $query->fetch())) {
            $output[$row['luid']] = $row['modifiedDatetime'];
        }

        return $output;
    }

    public function fetchAllByParentLuid($luid, array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('a' => $this->_table->getTableName()),
                $this->_table->getAllowedColumns(\OSDN_Db_Table_Abstract::OMIT_FETCH_ROW)
            )
            ->join(
                array('acr' => 'articleCategoryRel'),
                'acr.articleId = a.id',
                array(
                    'categoryId'
                )
            )
            ->join(
                array('ac' => 'articleCategory'),
                'ac.id = acr.categoryId',
                array()
            )
            ->where('ac.parentId IN(?)', new Zend_Db_Expr(
                $this->getAdapter()->quoteInto('SELECT id from articleCategory where luid= ?', $luid))
            )
            ->order('a.createdDatetime DESC')
            ->where('a.isActive = 1')
            ->where('ac.isActive = 1')
            ->group('a.id');

        if (!empty($params['categoryId'])) {
            $select->where('acr.categoryId = ?', $params['categoryId'], Zend_Db::INT_TYPE);
        }

        $this->_initDbFilter($select, $this->_table)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);

        $articleFileStock = new \Article_Service_ArticleFileStock();
        $response->setRowsetCallback(function($row) use ($articleFileStock) {
             $fileStockRawRow = $articleFileStock->fetchFileStockThumbnailRowByArticle($row['id']);
             if (isset($fileStockRawRow['link'])) {
                 $row['thumbnail'] = $fileStockRawRow['link'];
             }

             return $row;
        });

        return $response;
    }

    public function fetchAllOverviewByCategoryId($categoryId,array $params = array(), $active = null)
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('a' => $this->_table->getTableName()),
                $this->_table->getAllowedColumns(\OSDN_Db_Table_Abstract::OMIT_FETCH_ROW)
            )
            ->join(
                array('ac' => 'articleCategoryRel'),
                'ac.articleId = a.id',
                array()
            )
            ->where('ac.categoryId = ?', $categoryId, Zend_Db::INT_TYPE)
            ->where ('a.isActive <> ?', $active);
        $this->_initDbFilter($select, $this->_table)->parse($params);
        return $this->getDecorator('response')->decorate($select);
    }

    public function fetchAllModifiedDatetimeByParentLuid($luid)
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('a' => $this->_table->getTableName()),
                array()
            )
            ->join(
                array('acr' => 'articleCategoryRel'),
                'acr.articleId = a.id',
                array()
            )
            ->join(
                array('ac' => 'articleCategory'),
                'ac.id = acr.categoryId',
                array('luid')
            )
            ->where('ac.parentId IN(?)', new Zend_Db_Expr(
                $this->getAdapter()->quoteInto('SELECT id from articleCategory where luid= ?', $luid))
            )
            ->where('a.isActive = 1')
            ->where('ac.isActive = 1')
            ->columns(array(
                'modifiedDatetime' => new Zend_Db_Expr(
                    'MAX(
                        CASE
                            WHEN ac.modifiedDatetime IS NULL THEN a.modifiedDatetime
                            WHEN ac.modifiedDatetime > a.modifiedDatetime THEN ac.modifiedDatetime
                        ELSE
                            a.modifiedDatetime
                        END
                     )'
                )
            ))
            ->group('ac.id');

        $query = $select->query();
        $output = array();
        while(false != ($row = $query->fetch())) {
            $output[$row['luid']] = $row['modifiedDatetime'];
        }

        return $output;
    }

    public function fetchAllModifiedDatetimeByLuid($luid)
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('a' => $this->_table->getTableName()),
                array()
            )
            ->join(
                array('acr' => 'articleCategoryRel'),
                'acr.articleId = a.id',
                array()
            )
            ->join(
                array('ac' => 'articleCategory'),
                'ac.id = acr.categoryId',
                array()
            )
            ->where('(ac.luid = ?', $luid)
            ->orWhere('ac.parentId IN(?))', new Zend_Db_Expr(
                $this->getAdapter()->quoteInto('SELECT id from articleCategory where luid= ?', $luid))
            )
            ->where('a.isActive = 1')
            ->where('ac.isActive = 1')
            ->columns(array(
                'modifiedDatetime' => new Zend_Db_Expr(
                    'MAX(
                        CASE
                            WHEN ac.modifiedDatetime IS NULL THEN a.modifiedDatetime
                            WHEN ac.modifiedDatetime > a.modifiedDatetime THEN ac.modifiedDatetime
                        ELSE
                            a.modifiedDatetime
                        END
                     )'
                )
            ));

        return $select->query()->fetchColumn();
    }

    public function update($id, array $data)
    {
        $articleRow = $this->find($id);
        $f = $this->_validate($data);

        $this->getAdapter()->beginTransaction();
        try {
            $articleRow->setFromArray($f->getData());
            $articleRow->save();

            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $articleRow;
    }

    public function getArticleCount($isActive = null)
    {
        $clause = array();
        if (null !== $isActive) {
            $clause['isActive = ?'] = (int) $isActive;
        }

        return $this->_table->count($clause);
    }

    /**
     * @todo should be done via event manager
     *
     * @FIXME Add main global transation for that
     */
    public function delete($id)
    {
        $articleRow = $this->find($id);

        /**
         * Delete attached file-stock items
         */
        $articleFileStock = new \Article_Service_ArticleFileStock();
        $fileStockRawRow = $articleFileStock->deleteByArticle($articleRow);

        /**
         * Delete connected categories relations
         */
        $articleCategory = new ArticleCategory_Service_ArticleCategory();
        $articleCategory->deleteAllArticleConnections($articleRow);

        return false !== $articleRow->delete();
    }
}