Ext.define('Module.Article.Model.Article', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'title', 'luid',
        'overview',
        'createdAccountId', {name: 'createdDatetime', type: 'date', dateFormat: 'Y-m-d H:i:s'},
        'modifiedAccountId', {name: 'modifiedDatetime', type: 'date', dateFormat: 'Y-m-d H:i:s'},
        {name: 'holdOnDatetime', type: 'date', dateFormat: 'Y-m-d H:i:s'},
        'isActive'
    ]
});