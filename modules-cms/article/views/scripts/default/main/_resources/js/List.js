Ext.define('Module.Article.List', {
    extend: 'Ext.ux.grid.GridPanel',
    alias: 'widget.module.article.list',

    categoryId: null,

    filterRequestParam: null,

    features: [{
        ftype: 'filters'
    }],

    iconCls: 'm-article-icon-16',

    modeReadOnly: false,

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.Article.Model.Article',
            proxy: {
                type: 'ajax',
                url: link('article', 'main', 'fetch-all', {format: 'json'}),
                extraParams: {
                    categoryId: this.categoryId
                },
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.columns = [{
            header: lang('Title'),
            dataIndex: 'title',
            flex: 1
        }, {
            header: lang('Active'),
            dataIndex: 'isActive'
        }];

        if (false === this.modeReadOnly) {
            this.columns.push({
                xtype: 'actioncolumn',
                header: lang('Actions'),
                width: 50,
                fixed: true,
                items: [{
                    tooltip: lang('Edit'),
                    iconCls: 'icon-edit-16 icon-16',
                    handler: function(g, rowIndex) {
                        this.onEditArticle(g, g.getStore().getAt(rowIndex));
                    },
                    scope: this
                }, {
                    text: lang('Delete'),
                    iconCls: 'icon-delete-16 icon-16',
                    handler: this.onDeleteArticle,
                    scope: this
                }]
            });
        }

        this.tbar = [{
            text: lang('Create'),
            iconCls: 'icon-create-16',
            qtip: lang('Create new Article'),
            handler: this.onCreateArticle,
            scope: this
        }, '-'];

        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 2
        })];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent();

        if (false === this.modeReadOnly) {
            this.getView().on('itemdblclick', function(w, record) {
                this.onEditArticle(this, record);
            }, this);
        }
    },

    onCreateArticle: function() {
        Application.require([
            'article/article-form/.'
        ], function() {
            var f = new Module.Article.Form({
                categoryId: this.categoryId
            });

            f.on('completed', function(articleId) {
                this.setLastInsertedId(articleId);
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditArticle: function(g, record) {
        Application.require([
            'article/article-form/.'
        ], function() {
            var f = new Module.Article.Form({
                articleId: record.get('id')
            });
            f.on('completed', function(articleId) {
                this.setLastInsertedId(articleId);
                g.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onAfterEdit: function(data, g) {

        var params = {articleId: data.record.get('id')};
        var action = null;

        switch(data.field) {
            case 'active':
                action = link('article', 'main', data.record.get('active') ? 'activate' : 'deactivate', {format: 'json'});
                break;

            default:
                action = link('article', 'main', 'update-field', {format: 'json'});
                params.field = data.field;
                params.value = data.value;
                break;
        }

        Ext.Ajax.request({
            url: action,
            params: params,
            callback: function(options, success, response) {
                var decResponse = Ext.decode(response.responseText);
                if (decResponse && decResponse.success) {
                    data.record.commit();
                    Application.notificate(decResponse.messages || lang('Updated successfully'));
                    return;
                }

                data.record.reject();
                Application.notificate(decResponse.messages || lang('Unable to updated article'));
            },
            scope: this
        });
    },

    onDeleteArticle: function(g, rowIndex) {

        var record = g.getStore().getAt(rowIndex);

        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
            if (b != 'yes') {
                return;
            }

            Ext.Ajax.request({
                url: link('article', 'main', 'delete', {format: 'json'}),
                method: 'POST',
                params: {
                    id: record.get('id')
                },
                success: function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);

                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope: this
            });
        }, this);
    },

    setCategoryId: function(categoryId, forceReload) {

        this.categoryId = categoryId;
        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.categoryId = categoryId;
        if (true === forceReload) {
            this.getStore().load();
        }
        return this;
    }
});