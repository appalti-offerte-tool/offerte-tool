Ext.define('Module.Article.Form', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.module.article.form',
    layout: 'fit',

    form: null,
    articleId: null,

    wnd: null,

    initComponent: function() {

        var params = {};
        if (this.articleId) {
            params['articleId'] = this.articleId;
        }

        this.blocks = new Module.Application.BlockTabPanel({
            activeTab: 0,
            border: false,
            invokeArgs: {
                articleId: this.articleId
            }
        });

        this.items = [this.blocks];

        this.callParent(arguments);

        this.addEvents(
            /**
             * Fires when article is created
             *
             * @param {Module.Article.Form}
             */
            'completed'
        );

        this.blocks.on('tabchange', function(tb, b) {
            b && b.reload();
            b && b.doLayout();
        });

        params.keywords = 'form';
        params.module = 'article';
        this.on('render', function() {

            var blocks = this.blocks;
            this.blocks.reload(params);

        }, this);
    },

    onSubmit: function(panel, w) {

        var params = Ext.apply({}, this.invokeArgs || {});

        if (this.articleId) {
            params['articleId'] = this.articleId;
            var url = link('article', 'main', 'update', {format: 'json'});
        } else {
            var url = link('article', 'main', 'create', {format: 'json'});
        }
        var isValid = true;

        this.blocks.items.each(function(block) {

            if (!block.isValid()) {
                isValid = false;
                return isValid = false;
            }

            Ext.applyIf(params, block.toValues());
        });

        if (true !== isValid) {
            return;
        }

        var action = this.articleId ? 'update' : 'create';
        this.el.mask(lang('Saving...'), 'x-mask-loading');
        Ext.Ajax.request({
            url: link('article', 'main', action, {format: 'json'}),
            method: 'POST',
            params: Ext.ux.OSDN.encode(params, true),
            success: function(response, options) {

                this.el.unmask();

                var responseObj = Ext.decode(response.responseText);
                Application.notificate(responseObj.messages);

                if (true === responseObj.success) {
                    this.fireEvent('completed', responseObj.articleId);
                    this.wnd && this.wnd.close();
                    return;
                }
            },
            scope: this
        });
    },

    showInWindow: function() {

        this.border = false;
        var w = this.wnd = new Ext.Window({
            title: this.articleId ? lang('Update article') : lang('Create article'),
            resizable: false,
            layout: 'fit',
            iconCls: 'm-article-icon-16',
            width: 775,
            height: 550,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Save'),
                handler: this.onSubmit,
                scope: this
            }, {
                text: lang('Close'),
                handler: function() {
                    w.close();
                    this.wnd = null;
                },
                scope: this
            }]
        });

        w.show();
        return w;
    },

    destroy: function() {

        Ext.destroy(this.blocks);

        this.callParent(arguments);
    }
});