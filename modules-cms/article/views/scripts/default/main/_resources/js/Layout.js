Ext.define('Module.Article.Layout', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.article.layout',

    alist: null,
    actree: null,

    layout: 'border',

    modeReadOnly: false,

    initComponent: function() {

        this.alist = new Module.Article.List({
            region: 'center',
            title: lang('Article'),
            autoLoadRecords: true,
            modeReadOnly: this.modeReadOnly,
            stateful: false,
            stateId: false,
            split: true
        });

        this.actree = new Module.ArticleCategory.Tree({
            title: lang('Categories'),
            enableExpandAllTool: true,
            stateful: false,
            stateId: false,
            region: 'west',
            split: true,
            width: 250
        });

        this.items = [this.alist, this.actree];

        this.callParent();

        this.actree.on('itemclick', function(w, record) {
            this.alist.setCategoryId(record.get('id'), true);
        }, this);

        this.alist.on('itemdblclick', function(g, record) {
            if (false !== this.fireEvent('articleselected', this, record)) {
                this.wnd && this.wnd.close();
            }
        }, this);
    },

    doLoad: function() {
        this.actree.doLoad();
    },

    showInWindow: function() {

        this.border = false;
        var w = this.wnd = new Ext.Window({
            title: lang('Article'),
            iconCls: 'm-article-icon-16',
            resizable: false,
            layout: 'fit',
            width: 775,
            height: 550,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Close'),
                handler: function() {
                    w.close();
                    this.wnd = null;
                },
                scope: this
            }]
        });

        w.show();
        return w;
    }
});