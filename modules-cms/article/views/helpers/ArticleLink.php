<?php

class Article_View_Helper_ArticleLink extends \Zend_View_Helper_Abstract
{
    public function articleLink(\Article_Model_DbTable_ArticleRow $articleRow)
    {
        return $this->view->baseUrl($articleRow->toLink());
    }
}