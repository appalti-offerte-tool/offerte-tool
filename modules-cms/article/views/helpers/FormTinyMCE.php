<?php

final class Article_View_Helper_FormTinyMCE extends Zend_View_Helper_Abstract
{
    protected $_id;

    public function formTinyMCE($name, $value = null, $attribs = null)
    {
        if (empty($attribs['id'])) {
            $attribs['id'] = uniqid('tinymce-');
        }
        $this->_id = $attribs['id'];

        return $this->view->formTextarea($name, $value, $attribs);
    }
}