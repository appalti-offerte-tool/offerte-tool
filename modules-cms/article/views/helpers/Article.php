<?php

class Article_View_Helper_Article extends \Zend_View_Helper_Abstract
{
    protected $_articleRow;

    public function article(\Article_Model_DbTable_ArticleRow $articleRow)
    {
        $this->_articleRow = $articleRow;

        return $this;
    }

    public function __toString()
    {
        $this->view->articleRow = $this->_articleRow;
        try {
            return $this->view->render('article.phtml');
        } catch(\Exception $e) {
            return 'Unable to render view: "article"';
        }
    }

    public function setView(Zend_View_Interface $view)
    {
        parent::setView(clone $view);

        $this->view->setScriptPath(null);
        $this->view->addScriptPath(__DIR__ . '/default');

        return $this;
    }
}

//        $categoryRow = $articleRow->getCategoryRow();
//
//        while(null === ($template = $categoryRow->template)) {
//
//            if (null === ($parentRow = $categoryRow->getParentRow())) {
//                break;
//            }
//            $categoryRow = $parentRow;
//
//            if (null !== $categoryRow->template) {
//                $template = $categoryRow->template;
//            }
//        }
