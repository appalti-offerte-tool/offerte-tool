Ext.define('Module.Application.Model.Error', {
    extend: 'Ext.data.Model',
    fields: [
        'message', 'type', 'field',
        {name: 'id', mapping: 'field'},
        {name: 'msg', mapping: 'message'}
    ]
});

Ext.define('Module.Article.Block.FormInformation', {
    extend: 'Ext.form.Panel',
    alias: 'widget.article.block.form-information',
    model: 'Module.Article.Model.Article',

    bodyPadding: 5,

    initComponent: function() {

        this.initialConfig.trackResetOnLoad = true;
        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
            model: 'Module.Application.Model.Error',
            type: 'json',
            root: 'messages',
            successProperty: 'success'
        });

        this.reader  = this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.Article.Model.Article',
            record: 'row',
            successProperty: 'success'
        });

        this.items = [{
            xtype: 'triggerfield',
            fieldLabel: lang('Title'),
            name: 'title',
            anchor: '100%',
            onTriggerClick: Ext.bind(this.onTitleChangeRefreshLuid, this),
            triggerCls: 'article-luid-link'
        }, {
            xtype: 'textfield',
            fieldLabel: lang('Luid'),
            name: 'luid',
            anchor: '100%'
        }, {
            xtype: 'htmleditor',
            fieldLabel: lang('Quick overview'),
            name: 'overview',
            anchor: '100% 50%',
            allowBlank: true
        }, {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            items: [{
                xtype: 'checkbox',
                fieldLabel: lang('Active'),
                name: 'isActive',
                inputValue: '1',
                uncheckedValue: '0',
                checkedValue: '1'
            }, {
                xtype: 'datefield',
                fieldLabel: lang('Activate on'),
                margin: '0 0 0 50',
                labelWidth: 70,
                name: 'holdOnDatetime',
                format: 'd-m-Y',
                submitFormat: 'Y-m-d',
                width: 175,
                value: Ext.Date.add(new Date(), Ext.Date.DAY, 1)
            }]
        }];

        this.callParent(arguments);

        this.form.findField('isActive').on('change', function(cb, isChecked) {
            this.form.findField('holdOnDatetime').setDisabled(isChecked);
        }, this);
    },

    setAutoLuidGeneration: function(flag) {
        var title = this.form.findField('title');
        title[flag ? 'on' : 'un']('change', this.onTitleChangeRefreshLuid, this);
    },

    onTitleChangeRefreshLuid: function() {

        var bf = this.form;
        var value = bf.findField('title').getValue();

        var L = {
            'А':'A','а':'a','Б':'B','б':'b','В':'V','в':'v','Г':'G','г':'g',
            'Д':'D','д':'d','Е':'E','е':'e','Ё':'Yo','ё':'yo','Ж':'Zh','ж':'zh',
            'З':'Z','з':'z','И':'I','и':'i','Й':'Y','й':'y','К':'K','к':'k',
            'Л':'L','л':'l','М':'M','м':'m','Н':'N','н':'n','О':'O','о':'o',
            'П':'P','п':'p','Р':'R','р':'r','С':'S','с':'s','Т':'T','т':'t',
            'У':'U','у':'u','Ф':'F','ф':'f','Х':'Kh','х':'kh','Ц':'Ts','ц':'ts',
            'Ч':'Ch','ч':'ch','Ш':'Sh','ш':'sh','Щ':'Sch','щ':'sch','Ъ':'"','ъ':'"',
            'Ы':'Y','ы':'y','Ь':"'",'ь':"'",'Э':'E','э':'e','Ю':'Yu','ю':'yu',
            'Я':'Ya','я':'ya', ' ': '-'
        };

        var r = '';
        var k;

        for (k in L) {
            r += k;
        }

        value = value.replace(new RegExp('[' + r + ']', 'g'), function(a){
            return a in L ? L[a] : '-';
        });
        value = value.replace(new RegExp('[^\\w\\d_\\.-]', 'g'), '-');

        bf.findField('luid').setValue(value);
    }
});