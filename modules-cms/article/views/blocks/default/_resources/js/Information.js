Ext.define('Module.Article.Block.Information', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.article.block.information',

    articleId: null,

    themeCombo: null,

    initComponent: function() {

        this.themeCombo = Ext.widget('module.template.theme.combo-box', {
            value: ''
        });

        this.tbar = [lang('Template') + ':', this.themeCombo, '->', {
            text: lang('Refresh'),
            iconCls: 'x-tbar-loading',
            handler: this.doLoad,
            scope: this
        }];

        this.loader = {
            loadMask: true
        };
        this.callParent(arguments);

        this.themeCombo.on('select', this.onChangeTheme, this);
    },

    doLoad: function() {

        var o = {id: this.articleId};
        var value = this.themeCombo.getValue();
        if (value) {
            o.theme = value;
        }

        this.getLoader().load({
            url: link('article', 'index', 'view', o),
            scope: this
        });
    },

    onChangeTheme: function(combo) {
        this.doLoad();
    },

    setArticleId: function(id, forceReload) {
        this.articleId = id;

        if (true === forceReload) {
            this.doLoad();
        }
    }
});