Ext.define('Module.Article.Block.MenuKindSingle', {
    extend: 'Ext.form.Panel',
    alias: 'widget.article.block.menu-kind-single',

    bodyPadding: 5,

    menuId: null,

    initComponent: function() {

        this.initialConfig.trackResetOnLoad = true;
        this.items = [{
            xtype: 'triggerfield',
            fieldLabel: lang('Article'),
            name: 'articleName',
            anchor: '100%',
            triggerCls: 'article-choose',
            editable: false,
            onTriggerClick: Ext.bind(this.onTriggerClick, this)
        }, {
            xtype: 'hiddenfield',
            name: 'luid'
        }];

        this.callParent(arguments);
    },

    onTriggerClick: function() {
        Application.require([
            'article/./layout'
        ], function() {
            var layout = new Module.Article.Layout({
                modeReadOnly: true
            });
            layout.on('articleselected', this.onChooseArticle, this);

            layout.showInWindow();

        }, this);
    },

    onChooseArticle: function(alp, record) {
        this.form.findField('articleName').setValue(record.get('title'));
        this.form.findField('luid').setValue(record.get('id'));

        this.doUpdateArticleBinding();
    },

    doUpdateArticleBinding: function() {
        if (!this.form.isValid()) {
            return;
        }

        this.form.submit({
            url: link('menu', 'bind', 'update', {format: 'json'}),
            params: {
                menuId: this.menuId
            },
            success: function(response, options) {
                Application.notificate(options.result);
            },
            failure: function() {
                Application.notificate(lang('Unable to update'));
            },
            scope: this
        });
    }
});