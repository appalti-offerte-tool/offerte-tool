<?php

class Article_Model_DbTable_ArticleFileStockRelRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    public function getId()
    {
        return $this->id;
    }

    public function getFileStockRow()
    {
        return $this->findParentRow('FileStock_Model_DbTable_FileStock', 'FileStock');
    }

    public function getArticleRow()
    {
        return $this->findParentRow('Article_Model_DbTable_Article', 'Article');
    }

    public function getParentRow()
    {
        return $this->findParentRow('Article_Model_DbTable_ArticleFileStockRel', 'Parent');
    }

    public function getChildrenRowset()
    {
        return $this->findDependentRowset('Article_Model_DbTable_ArticleFileStockRel', 'Parent');
    }
}