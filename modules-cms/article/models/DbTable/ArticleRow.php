<?php

class Article_Model_DbTable_ArticleRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function toLink()
    {
        $route = '/article';
        $luid = $this->luid;

        $output = '';
        if (!empty($luid)) {
            $output = sprintf('%s/page/%s.html', $route, $luid);
        } else {
            $output = sprintf('%s/%d', $route, $this->getId());
        }

        return $output;
    }

    public function getCategoryRowset()
    {
        return $this->findManyToManyRowset(
            'ArticleCategory_Model_DbTable_Category',
            'ArticleCategory_Model_DbTable_ArticleCategoryRel'
        );
    }
}