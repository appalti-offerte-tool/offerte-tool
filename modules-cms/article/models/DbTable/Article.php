<?php

class Article_Model_DbTable_Article extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'article';

    protected $_rowClass = '\\Article_Model_DbTable_ArticleRow';

    protected $_nullableFields = array(
        'modifiedAccountId', 'modifiedDatetime',
        'holdOnDatetime'
    );

    protected $_omitColumns = array(
        'content'    => self::OMIT_FETCH_ALL
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        if (empty($data['createdDatetime'])) {
            $dt = new \DateTime();
            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        $data['createdAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        return parent::insert($data);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
        if (empty($data['modifiedDatetime'])) {
            $dt = new \DateTime();
            $data['modifiedDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        $data['modifiedAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        return parent::update($data, $where);
    }
}