<?php

class Article_Model_DbTable_ArticleFileStockRel extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'articleFileStockRel';

    protected $_nullableFields = array(
        'isThumbnail', 'parentId'
    );

    protected $_referenceMap    = array(
        'FileStock' => array(
            'columns'       => 'fileStockId',
            'refTableClass' => 'FileStock_Model_DbTable_FileStock',
            'refColumns'    => 'id'
        ),
        'Parent'    => array(
            'columns'       => 'parentId',
            'refTableClass' => __CLASS__,
            'refColumns'    => 'id'
        ),
        'Article'    => array(
            'columns'       => 'articleId',
            'refTableClass' => 'Article_Model_DbTable_Article',
            'refColumns'    => 'id'
        )
    );

    protected $_rowClass = '\\Article_Model_DbTable_ArticleFileStockRelRow';
}