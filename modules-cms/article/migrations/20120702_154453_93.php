<?php

class Article_Migration_20120702_154453_93 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createColumn('article', 'holdOnDatetime', self::TYPE_DATETIME, null, null, false);

        $cronSchedule = new CronSchedule_Service_Configuration();
        $cronSchedule->create(array(
            'periodicity'  => \CronSchedule_Model_DbTable_Configuration::PERIODICITY_MULTIPLE,
            'name'         => 'Move hold articles to active',
            'module'       => 'article',
            'path'         => 'bin/hold2active.php',
            'isActive'     => 0
        ));
    }

    /**
     * (non-PHPdoc)
     * @see Core_Migration_Abstract::down()
     */
    public function down()
    {
        $this->dropColumn('article', 'holdOnDatetime');

        /**
         * @FIXME Add some uniq identity for cron schedule job
         * Add remove method code
         */
    }
}