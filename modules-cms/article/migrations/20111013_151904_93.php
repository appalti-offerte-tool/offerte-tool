<?php

class Article_Migration_20111013_151904_93 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createColumn('articleFileStockRel', 'isDefaultThumbnail', self::TYPE_INT, 1, '0', true, false);
    }

    public function down()
    {
        $this->dropColumn('articleFileStockRel', 'isDefaultThumbnail');
    }
}