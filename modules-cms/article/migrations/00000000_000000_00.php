<?php

class Article_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('article');
        $this->createColumn('article', 'title', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('article', 'luid', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('article', 'overview', self::TYPE_TEXT, null, null, true);
        $this->createColumn('article', 'content', self::TYPE_TEXT, null, null, true);
        $this->createColumn('article', 'createdAccountId', self::TYPE_INT, 11, null, true);
        $this->createColumn('article', 'createdDatetime', self::TYPE_DATETIME, null, null, true);
        $this->createColumn('article', 'modifiedAccountId', self::TYPE_INT, 11, null, false);
        $this->createColumn('article', 'modifiedDatetime', self::TYPE_DATETIME, null, null, false);
        $this->createColumn('article', 'isActive', self::TYPE_INT, 1, 1, true);
        $this->createIndex('article', array('luid'), 'IX_luid');
        $this->createIndex('article', array('createdAccountId'), 'IX_createdAccountId');
        $this->createForeignKey('article', array('createdAccountId'), 'account', array('id'), 'FK_createdAccountId');
        $this->createIndex('article', array('modifiedAccountId'), 'IX_modifiedAccountId');
        $this->createForeignKey('article', array('modifiedAccountId'), 'account', array('id'), 'FK_modifiedAccountId');

        $this->createTable('articleFileStockRel');
        $this->createColumn('articleFileStockRel', 'articleId', self::TYPE_INT, 11, null, true);
        $this->createColumn('articleFileStockRel', 'fileStockId', self::TYPE_INT, 11, null, true);
        $this->createColumn('articleFileStockRel', 'isThumbnail', self::TYPE_INT, 1, null, true);
        $this->createColumn('articleFileStockRel', 'parentId', self::TYPE_INT, 1, null, false);
        $this->createUniqueIndexes('articleFileStockRel', array('articleId', 'fileStockId'), 'UX_articleId');
        $this->createForeignKey('articleFileStockRel', array('articleId'), 'article', array('id'), 'FK_articleId');
        $this->createIndex('articleFileStockRel', array('fileStockId'), 'IX_fileStockId');
        $this->createForeignKey('articleFileStockRel', array('fileStockId'), 'fileStock', array('id'), 'FK_fileStockId');
        $this->createIndex('articleFileStockRel', array('parentId'), 'IX_parentId');
        $this->createForeignKey('articleFileStockRel', array('parentId'), 'articleFileStockRel', array('id'), 'FK_parentId');
    }

    public function down()
    {
        $this->dropTable('articleFileStockRel');
        $this->dropTable('article');
    }
}