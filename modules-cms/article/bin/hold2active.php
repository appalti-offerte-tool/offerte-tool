<?php

define('SILENT_MODE', true);

/**
 * @var Zend_Application
 */
$application = require __DIR__ . '/../../../index.php';

/**
 * Here we create the bootstrap for get
 * easy access for all configured caches
 *
 * @var Zend_Application_Bootstrap_Bootstrap
 */
$application->bootstrap();

$bootstrap = $application->getBootstrap();

set_time_limit(0);

$dt = new \DateTime();
$table = new \Article_Model_DbTable_Article();

$table->getAdapter()->beginTransaction();
try {
    $table->updateQuote(array(
        'isActive'            => 1,
        'modifiedDatetime'    => $dt->format('Y-m-d H:i:s'),
        'modifiedAccountId'   => Zend_Auth::getInstance()->getIdentity()->getId(),
        'holdOnDatetime'      => new Zend_Db_Expr('NULL')
    ), array(
        'isActive != 1',
        'holdOnDatetime IS NOT NULL',
        'holdOnDatetime <= ?' => $dt->format('Y-m-d H:i:s')
    ));

    $table->getAdapter()->commit();

} catch(Exception $e) {
    $table->getAdapter()->rollBack();

    throw $e;
}