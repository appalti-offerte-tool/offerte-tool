<?php

class Article_Bootstrap extends Zend_Application_Module_Bootstrap
{
    /**
     * @todo Implement caching
     */
    protected function _initModuleConfig()
    {
        $config = new Zend_Config_Ini(__DIR__ . '/configs/module.ini');
        $this->getApplication()->getBootstrap()->setOptions($config->toArray());
    }
}