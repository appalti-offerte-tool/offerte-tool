<?php

final class Article_Block_Information extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Information');
    }

    protected function _toHtml()
    {
        if ('iphone' == $this->_getParam('format')) {
            $layout = Zend_Controller_Action_HelperBroker::getStaticHelper('layout');
            $layout->setLayout('iphone');
        }
        $articleId = $this->_getParam('id');

        $this->view->articleId = $articleId;
        $service = new Article_Service_Article();
        $this->view->articleRow = $service->find($articleId);

        /**
         * @FIXME
         */
        $articleFileStock = new Article_Service_ArticleFileStock();
        $this->view->articleFileStockRowset = $articleFileStock->fetchAllFileStockByArticleIdWithResponse($articleId);
    }
}