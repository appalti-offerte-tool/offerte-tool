<?php

final class Article_Block_FormContent extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Content');
    }

    protected function _toHtml()
    {
        $articleId = $this->_getParam('articleId', false);

        if (!empty($articleId)) {
            $this->view->articleId = $articleId;
            $service = new \Article_Service_Article();
            $this->view->articleRow = $service->find($articleId);
        }
    }
}