<?php

class Article_Block_MenuKindSingle extends \Application_Block_Abstract
{
    /**
     * (non-PHPdoc)
     * @see Application_Block_Abstract::_toTitle()
     */
    protected function _toTitle()
    {
        return $this->view->translate('Article');
    }

    protected function _toHtml()
    {
        $menuId = (int) $this->_getParam('menuId');
        $this->view->menuId = $menuId;

        $service = new Menu_Service_Menu();
        $menuRow = $service->find($menuId);

        $this->view->menuRow = $menuRow;

        $bind = new Menu_Service_Bind($menuRow, $this->view);
        $this->view->articleRow = $bind->toBindInstance();
    }
}