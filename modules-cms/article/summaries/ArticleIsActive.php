<?php

class Article_Summary_ArticleIsActive extends Application_Instance_Summary
{
    public function __construct()
    {
        parent::__construct();

        $this->_setHref('article', 'main', 'index', array('f' => 1));
    }

    protected function _bind()
    {
        $this->_title = $this->getTranslator()->translate('Article active');

        $article = new Article_Service_Article();
        $this->_value = $article->getArticleCount(true);
    }
}