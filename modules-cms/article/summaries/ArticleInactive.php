<?php

class Article_Summary_ArticleInactive extends \Application_Instance_Summary
{
    public function __construct()
    {
        parent::__construct();

        $this->_setHref('article', 'main', 'index', array('f' => 0));
    }

    protected function _bind()
    {
        $this->_title = $this->getTranslator()->translate('Article inactive');

        $article = new Article_Service_Article();
        $this->_value = $article->getArticleCount(false);
    }
}