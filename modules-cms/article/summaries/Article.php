<?php

class Article_Summary_Article extends Application_Instance_Summary
{
    public function __construct()
    {
        parent::__construct();

        $this->_setHref('article', 'main');
    }

    protected function _bind()
    {
        $this->_title = $this->getTranslator()->translate('Article');

        $article = new Article_Service_Article();
        $this->_value = $article->getArticleCount();
    }
}