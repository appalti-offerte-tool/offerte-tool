<?php

class Menu_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('menu');
        $this->createColumn('menu', 'name', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('menu', 'luid', self::TYPE_VARCHAR, 255, null, false);

        $this->createTable('menuItem');
        $this->createColumn('menuItem', 'menuId', self::TYPE_INT, 11, null, true);
        $this->createIndex('menuItem', array('menuId'), 'IX_menuId');
        $this->createForeignKey('menuItem', array('menuId'), 'menu', array('id'), 'FK_menuId');

        $this->createColumn('menuItem', 'name', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('menuItem', 'kind', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('menuItem', 'link', self::TYPE_VARCHAR, 255, null, false);
        $this->createColumn('menuItem', 'parentId', self::TYPE_INT, 11, null, false);
        $this->createColumn('menuItem', 'isActive', self::TYPE_INT, 1, 1, true);
        $this->createIndex('menuItem', array('parentId'), 'IX_parentId');
        $this->createForeignKey('menuItem', array('parentId'), 'menuItem', array('id'), 'FK_parentId');
    }

    public function down()
    {
        $this->dropTable('menu');
    }
}


//public function up()
//{
//    $this->createColumn('menu', 'roleId', self::TYPE_INT, 11, null, true);
//    $this->createIndex('menu', array('roleId'), 'IX_roleId');
//    $this->createForeignKey('menu', array('roleId'), 'aclRole', array('id'), 'FK_roleId');
//    $this->createColumn('menu', 'position', self::TYPE_INT, 11, null, true);
//}
//
//public function down()
//{
//    $this->dropForeignKey('menu', 'FK_roleId');
//    $this->dropIndex('menu', 'IX_roleId');
//    $this->dropColumn('menu', 'roleId');
//}