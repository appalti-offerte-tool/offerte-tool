<?php

class Menu_Model_DbTable_Menu extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'menu';

    protected $_rowClass = '\\Menu_Model_Menu';
}