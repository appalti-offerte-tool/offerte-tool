<?php

class Menu_Model_DbTable_MenuItem extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'menuItem';

    protected $_rowClass = '\\Menu_Model_MenuItem';

    protected $_nullableFields = array(
        'parentId',
        'modifiedAccountId', 'modifiedDatetime'
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
//        if (empty($data['createdDatetime'])) {
//            $dt = new \DateTime();
//            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
//        }
//
//        $data['createdAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        return parent::insert($data);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
//        if (empty($data['modifiedDatetime'])) {
//            $dt = new \DateTime();
//            $data['modifiedDatetime'] = $dt->format('Y-m-d H:i:s');
//        }
//
//        $data['modifiedAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        return parent::update($data, $where);
    }
}