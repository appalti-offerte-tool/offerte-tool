<?php

class Menu_View_Helper_MainMenu extends Zend_View_Helper_Abstract
{
    public function MainMenu()
    {
        $service = new Menu_Service_Menu();
        $this->view->response = $service->fetchAllByParentId();
        return $this->view->render('main-menu.phtml');
    }

    /**
     * (non-PHPdoc)
     * @see Zend_View_Helper_Abstract::setView()
     */
    public function setView(Zend_View_Interface $view)
    {
        parent::setView(clone $view);
        $this->view->addScriptPath(__DIR__ . '/default');

        return $this;
    }
}