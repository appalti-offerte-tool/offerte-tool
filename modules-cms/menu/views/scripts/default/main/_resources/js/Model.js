Ext.define('Module.Menu.Model.Menu', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name', 'luid'
    ]
});