Ext.define('Module.Menu.Navigation.List', {
    extend: 'Ext.ux.grid.GridPanel',
    alias: 'widget.module.article.list',

    features: [{
        ftype: 'groupingsummary',
        groupHeaderTpl: '{name}',
        hideGroupedHeader: true,
        remoteRoot: 'summaryData'
    }],

    iconCls: 'm-menu-icon-16',
    menuId: null,

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.Menu.Model.Navigation',
            proxy: {
                type: 'ajax',
                url: link('menu', 'navigation', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            },
            groupField: 'mtitle',
            remoteSort: true,
            remoteGroup: true
        });

        this.columns = [{
            header: lang('Title'),
            dataIndex: 'text',
            flex: 1
        }, {
            hidden: true,
            hideable: false,
            dataIndex: 'mtitle'
        }];

        var checkColumn = new Ext.ux.CheckColumn({
            header:lang ('Archive'),
            dataIndex: 'isActive',
            width:50
        });
        checkColumn.on('checkchange', function (cc, rowIndex, isChecked) {
            var record = this.getStore().getAt(rowIndex);
            this.onCreateMenuNavigation(record, isChecked);
        }, this);

        this.columns.push(checkColumn);

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent();

        this.getView().on('itemdblclick', function(w, record) {
            this.onEditMenuNavigation(this, record);
        }, this);
    },

    onCreateMenuNavigation: function(record, isChecked) {
        console.log('onCreateMenuNavigation: ', record, isChecked);

        Ext.Ajax.request({
            url: link('menu', 'menu-item', 'create', {format: 'json'}),
            params: {
                name: record.get('text'),
                kind: 'navigation',
                link: link(record.get('module'), record.get('controller') || 'index', record.get('action') || 'index'),
                menuId: this.menuId
            }
        });
    },

    doLoad: function() {
        this.getStore().load();
        return this;
    },

    showInWindow: function(configuration) {

        var w = new Ext.Window(Ext.apply({
            title: lang('Menu navigation'),
            iconCls: 'm-menu-icon-16',
            resizable: false,
            width: 415,
            height: 450,
            layout: 'fit',
            modal: true,
            border: false,
            items: [this],
            buttons: [{
                text: lang('Cancel'),
                handler: function() {
                    w.close();
                }
            }],
            scope: this
        }, configuration || {}));

        w.show();
        return w;
    }
});