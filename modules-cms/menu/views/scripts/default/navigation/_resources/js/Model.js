Ext.define('Module.Menu.Model.Navigation', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'text', 'isActive',
        'module', 'controller', 'action',
        'mname', 'mtitle'
    ]
});