Ext.define('Module.Menu.Form', {
    extend: 'Ext.form.Panel',
    bodyPadding: 5,

    menuId: null,
    parentId: null,

    trackResetOnLoad: true,
    waitMsgTarget: true,

    initComponent: function() {

        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.Menu.Model.Menu',
            type: 'json',
            root: 'row'
        });

        this.items = [{
            xtype: 'textfield',
            fieldLabel: lang('Name') + '<span class="asterisk">*</span>',
            name: 'name',
            allowBlank: false,
            anchor: '100%'
        }, {
            xtype: 'module.menu.kind.kind-trigger-field',
            name: 'kind',
            fieldLabel: lang('Kind'),
            anchor: '100%'
        }, {
            xtype: 'checkbox',
            name: 'isActive',
            fieldLabel: lang('Active'),
            inputValue: '1'
        }];

        this.callParent();

        this.addEvents(
            /**
             * Fires when account is created
             *
             * @param {Module.Menu.Form}
             */
            'completed'
        );
    },

    doLoad: function() {

        if (!this.menuId) {
            return;
        }

        var params = Ext.applyIf({
            id: this.menuId,
            format: 'json'
        }, this.invokeArgs || {});

        this.form.load({
            url: link('menu', 'main', 'fetch', params),
            waitMsg: Ext.LoadMask.prototype.msg,
            success: function(response, result, type) {
                Application.notificate(result);
            },
            scope: this
        });
    },

    onSubmit: function(panel, w) {

        if (true !== this.form.isValid()) {
            return;
        }

        var params = Ext.apply({}, this.invokeArgs || {});
        params.parentId = this.parentId || 0;

        if (this.menuId) {
            params['id'] = this.menuId;
        }

        var acField = this.form.findField('isActive');
        if (!acField.checked) {
            params['isActive'] = '0';
        }

        var action = this.menuId ? 'update' : 'create';
        this.form.submit({
            url: link('menu', 'main', action, {format: 'json'}),
            method: 'post',
            params: params,
            waitMsg: Ext.LoadMask.prototype.msg,
            success: function(form, action) {
                var responseObj = Ext.decode(action.response.responseText);
                Application.notificate(action);
                if (action.result.success) {
                    this.fireEvent('completed', responseObj.menuId, action.response);
                    w.close();
                }
            },
            scope: this
        });
    },

    showInWindow: function() {

        var w = new Ext.Window({
            title:  this.menuId ? lang('Update menu item') : lang('Create menu item'),
            iconCls: 'm-menu-icon-16',
            resizable: false,
            width: 415,
            modal: true,
            border: false,
            items: [this],
            buttons: [{
                iconCls: 'icon-submit-16',
                text: lang('Submit'),
                handler: function() {
                    this.onSubmit(this, w);
                },
                scope: this
            }, {
                text: lang('Cancel'),
                handler: function() {
                    w.close();
                }
            }],
            scope: this
        });

        w.show();
        this.doLoad();

        return w;
    }
});