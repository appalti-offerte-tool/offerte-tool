Ext.define('Module.Menu.Model.MenuItem', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name', 'parentId', 'kind',
        'menuId',
        {name: 'isActive', type: 'boolean'}
    ]
});