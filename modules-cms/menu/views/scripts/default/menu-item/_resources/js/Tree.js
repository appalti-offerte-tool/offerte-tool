Ext.define('Module.MenuItem.Tree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.module.menu.tree',

    iconCls: 'm-menu-icon-16',

    dataUrl: null,
    animate: false,

    onLoadExpandAll: false,
    rootVisible: false,

    menuId: null,

    initComponent: function() {

        this.viewConfig = Ext.apply(this.viewConfig || {}, {
            plugins: {
                ptype: 'treeviewdragdrop'
            }
        });

        this.tools = [];
        if (true === this.enableExpandAllTool) {
            this.tools.push({
                type: 'expand',
                handler: function() {
                    this.expandAll();
                },
                scope: this
            });
        }

        this.store = Ext.create('Ext.data.TreeStore', {
            clearOnLoad: false,
            proxy: {
                type: 'ajax',
                url: this.dataUrl || link('menu', 'menu-item', 'fetch-all-by-parent-id', {format: 'json'})
            },
            root: {
                id: '0',
                expanded: false
            },
            fields: [
                'id', 'name', 'leaf', 'expanded', 'kind',
                {name: 'isActive', type: 'boolean'}
            ]
        });

        this.columns = [{
            xtype: 'treecolumn',
            text: lang('Name'),
            flex: 2,
            sortable: true,
            dataIndex: 'name'
        }, {
            text: lang('Kind'),
            dataIndex: 'kind'
        }, {
            xtype: 'booleancolumn',
            text: lang('Active'),
            dataIndex: 'isActive',
            align: 'center'
        }
        /*, {
            xtype: 'actioncolumn',
            text: lang('Actions'),
            width: 50,
            align: 'center',
            items: [{
                iconCls: 'icon-edit-16 icon-16',
                tooltip: lang('Edit'),
                handler: function(dw, rowIndex) {
                    console.log(arguments);
                    this.onUpdateMenuItemItem(this.getStore().getAt(rowIndex));
                },
                scope: this
            }, {
                iconCls: 'icon-delete-16 icon-16',
                tooltip: lang('Delete'),
                handler: function(dw, rowIndex) {
                    this.onDeleteMenuItemItem(this.getStore().getAt(rowIndex));
                },
                scope: this
            }]
        }*/];

        this.tbar = [{
            text: lang('Create'),
            iconCls: 'icon-create-16',
            handler: function() {
                this.onCreateMenuItem(null);
            },
            scope: this
        }, '-', {
            text: lang('Attach'),
            iconCls: 'icon-create-16',
            handler: this.onAttachMenuItem,
            scope: this
        }, '->', lang('Menu') + ':'];
        var menu = new Module.Menu.Main.ComboBox();
        menu.on('select', function(cb, record) {
            this.menuId = cb.getValue();

            var p = this.getStore().getProxy();
            p.extraParams = p.extraParams || {};
            p.extraParams.menuId = this.menuId;

            this.doLoad();
        }, this);

        this.tbar.push(menu);

        this.bbar = ['->', {
            iconCls: 'x-tbar-loading',
            text: lang('Refresh'),
            handler: function() {
                this.doLoad();
            },
            scope: this
        }];

        this.callParent();

        this.on({
            itemcontextmenu: this.onContextMenuItem,
            scope: this
        });

        this.getView().on('drop', this.onTreeViewDragDrop, this);
    },

    doLoad: function(o) {

        var node = ((o && o.node) || this.getRootNode());
        if (true !== this.getStore().clearOnLoad) {
            node.removeAll();
        }

        console.log(this.menuId);
        this.getStore().load(Ext.apply(o || {}, {
            node: node,
            params: {
                menuId: this.menuId
            },
            callback: function() {
                if (node == this.getRootNode()) {
                    this.onLoadExpandAll ? this.expandAll() : node.expand();
                }
            },
            scope: this
        }));

        return this;
    },

    onCreateMenuItem: function(pRecord) {

        Application.require([
            'menu/./form'
        ], function() {
            var f = new Module.MenuItem.Form({
                parentId: pRecord ? pRecord.get('id') : null
            });
            f.on('completed', function(menuId) {
                this.doLoad({
                    node: pRecord || this.getRootNode()
                });
            }, this);
            f.showInWindow();
        }, this);
    },

    onAttachMenuItem: function() {
        Application.require([
            'menu/navigation/list'
        ], function() {
            var list = new Module.Menu.Navigation.List({
                menuId: this.menuId
            });
            list.showInWindow();
            list.doLoad();
        }, this);
    },

    onUpdateMenuItem: function(record) {
        Application.require([
            'menu/./form'
        ], function() {
            var f = new Module.MenuItem.Form({
                parentId: record.get('parentId'),
                menuId: record.get('id')
            });

            f.on('completed', function(menuId) {
                this.doLoad({
                    node: record.parentNode || this.getRootNode()
                });
            }, this);

            f.showInWindow();
        }, this);
    },

    onDeleteMenuItem: function(record) {

        Ext.Msg.confirm(lang('Confirmation'), lang('The category "{0}" will be removed', record.get('name')), function(b) {

            Ext.Ajax.request({
                url: link('menu', 'main', 'delete', {format: 'json'}),
                params: {
                    id: record.get('id')
                },
                success: function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    if (!decResponse.success) {
                        return Application.notificate(decResponse.messages);
                    }

                    this.doLoad({
                        node: record
                    });
                },
                scope: this
            });
        }, this);

    },

    onTreeViewDragDrop: function(node, data, oRecord, position, eOpts) {

        this.view.loadMask.show();

        var pRecord = null;
        switch(position) {
            case 'after':
            case 'before':
                pRecord = oRecord.parentNode;
                break;

            case 'append':
            default:
                pRecord = oRecord;

        }
        Ext.Ajax.request({
            url: link('menu', 'main', 'move', {format: 'json'}),
            params: {
                parentId: pRecord.get('id'),
                id: data.records[0].get('id')
            },
            success: function(response, options) {
                var decResponse = Ext.decode(response.responseText);
                if (!decResponse.success) {
                    Application.notificate(decResponse.messages);
                    this.doLoad();
                }
            },
            callback: function() {
                this.view.loadMask.hide();
            },
            scope: this
        });
    },

    onContextMenuItem: function(w, record, item, index, e) {

        e.stopEvent();

        var isRoot = record.get('id') == this.getRootNode().get('id');
        var items = [{
            text: lang('Create') + (isRoot ? '' : (' on ' + record.get('name'))),
            iconCls: 'icon-create-16',
            handler: function() {
                this.onCreateMenuItem(record);
            },
            scope: this
        }];

        if (!isRoot) {
            items.push({
                text: lang('Edit') + ': "' + record.get('name') + '"',
                iconCls: 'icon-update-16',
                handler: function() {
                    this.onUpdateMenuItem(record);
                },
                scope: this
            });
        }

        items.push({
            text: lang('Delete'),
            iconCls: 'icon-delete-16',
            handler: function() {
                this.onDeleteMenuItem(record);
            },
            scope: this
        });

        items.push('-');
        items.push({
            text: lang('Refresh'),
            iconCls: 'icon-refresh-16',
            handler: function() {
                this.doLoad({
                    node: record
                });
            },
            scope: this
        });

        var m = new Ext.menu.Menu({
            items: items
        });

        m.showAt(e.getXY());
    }
});