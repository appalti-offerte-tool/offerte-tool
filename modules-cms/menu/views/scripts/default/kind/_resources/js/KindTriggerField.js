Ext.define('Module.Menu.Kind.KindTriggerField', {
    extend: 'Ext.form.field.Trigger',
    alias: 'widget.module.menu.kind.kind-trigger-field',

    initComponent: function() {
        this.callParent();
    },

    onTriggerClick: function() {

        var ksp = new Module.Menu.Kind.KindSelectionPanel();
        ksp.showInWindow();

        ksp.on('pickupkind', function(p, o) {
            this.setValue(o.module + ':' + o.kind);
        }, this);
    }
});