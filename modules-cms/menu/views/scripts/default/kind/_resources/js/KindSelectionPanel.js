Ext.define('Module.Menu.Kind.KindSelectionPanel', {
    extend: 'Ext.panel.Panel',

    wnd: null,

    bodyPadding: 5,

    module: null,
    kind: null,

    initComponent: function() {

        this.autoLoad = {
            url: link('menu', 'kind', 'index'),
            callback: this.onCompleteContentLoading,
            scope: this
        };

        this.callParent();

        this.addEvents('pickupkind');
    },

    onCompleteContentLoading: function() {

        this.mon(this.getEl(), 'click', function(e, dom) {

            e.stopEvent();

            /**
             * @FIXME Should be changed to more modern methods
             */
            var element = Ext.get(dom);
            this.module = element.getAttribute('data-module');
            this.kind = element.getAttribute('data-kind');

            if (false !== this.fireEvent('pickupkind', this, {
                module: this.module,
                kind: this.kind,
                caption: element.up('h3')
            })) {
                this.wnd.close();
            }

        }, this, {
            delegate: 'a'
        });
    },

    showInWindow: function() {

        var w = this.wnd = new Ext.Window({
            title:  lang('Menu kind'),
            iconCls: 'm-menu-icon-16',
            resizable: false,
            width: 415,
            modal: true,
            autoScroll: true,
            border: false,
            items: [this],
            buttons: [{
                text: lang('Cancel'),
                handler: function() {
                    w.close();
                }
            }],
            scope: this
        });

        w.show();

        return w;
    }
});