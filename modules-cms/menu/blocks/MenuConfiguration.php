<?php

class Menu_Block_Configuration extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        $this->view->translate('List');
    }

    protected function _toHtml()
    {
        $acService = new ArticleCategory_Service_Category();
        $categoryRow = $acService->findByLuid($this->_getParam('categoryLuid'));

//        $articleService = new Article_Service_Article();
//        $response = $articleService->fetchAllByParentLuid($this->_getParam('categoryLuid'));

        $articleCategoryService = new ArticleCategory_Service_ArticleCategory();
        $response = $articleCategoryService->fetchAllByCategory($categoryRow);

        $this->view->response = $response;
    }
}