<?php

class Menu_Service_Menu extends OSDN_Application_Service_Dbable
{
    /**
     * @var Menu_Model_DbTable_Menu
     */
    protected $_table;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new \Menu_Model_DbTable_Menu();

        $this->_attachValidationRules('default', array(
            'name'      => array(array('StringLength', 1, 255), 'presence' => 'required', 'allowEmpty' => false),
            'luid'      => array(array('StringLength', 1, 255), 'presence' => 'required', 'allowEmpty' => false)
        ));

        parent::_init();
    }

    public function fetchAllWithResponse(array $params = array())
    {
        $select = $this->_table->select(true);
        return $this->getDecorator('response')->decorate($select, $this->_table);
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \Menu_Model_DbTable_MenuRow
     */
    public function find($id, $throwException = true)
    {
        $menuRow = $this->_table->findOne($id);
        if (null === $menuRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find menu #' . $id);
        }

        return $menuRow;
    }

    /**
     * @param string $luid
     * @param boolean $throwException
     *
     * @return \Menu_Model_DbTable_MenuRow
     */
    public function fetchByLuid($luid, $throwException = true)
    {
        $menuRow = $this->_table->fetchRow(array(
            'luid = ?' => (string) $luid
        ));

        if (null === $menuRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find menu luid: #' . ($luid ?: 'none'));
        }

        return $menuRow;
    }
}