<?php

class Menu_Service_Navigation
{
    /**
     * Contain the collection of menu items
     *
     * @var array
     */
    protected $_menu = array();

    /**
     * @var \Module_Resource_Modulesmanager
     */
    protected $_im;

    public function __construct()
    {
        /**
         * @FIXME Make independent module
         */
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $this->_im = $bootstrap->getResource('ModulesManager');
    }

    public function fetchAllWithModuleInformation()
    {
        $toMenu = function(array $menu) use (& $toMenu/*, $toComparisionFn, $toResetPosition*/) {

            $o = array();
            if (isset($menu['_attributes'])) {
                $o = $menu['_attributes'];
                unset($menu['_attributes']);
            } else {
//                $o['text'] = 'Untitled';
//                $o['position'] = 100;
            }

            if (!empty($o)) {
                $o = array($o);
            }

            foreach($menu as $k => $value) {
                if (false !== ($mi = $toMenu($value))) {
                    $o = array_merge($o, $mi);
                }
            }

//            if (empty($o['children']) && empty($o['handler']) && empty($o['href'])) {
//                return false;
//            }

//            if (!empty($o['children'])) {
//                usort($o['children'], $toComparisionFn);
//                $toResetPosition($o['children']);
//            }

            return $o;
        };

        $result = array();
        foreach($this->_im->fetchAll() as $information) {
            $fMenu = $information->getRoot() . '/configs/menu.xml';
            if (!file_exists($fMenu)) {
                continue;
            }

            $xMenu = simplexml_load_file($fMenu);
            foreach($xMenu->xpath('navigation/item') as $xMenuItem) {
                $this->_toMenu($xMenuItem);

//                $result[$information->getName()] = array(
//                    'information' => $information,
//                    'menu'        => array()
//                );
//
//                $iResult  = & $result[$information->getName()];

                foreach($this->_menu as $m) {
                    if (false !== ($mResult = $toMenu($m))) {
//                        $iResult['menu'] = array_merge($iResult['menu'], $mResult);

                        $result = array_merge($result, $mResult);
                    }
                }
            }

            $this->_menu = array();
        }
        return $result;
    }

    public function fetchAll()
    {
        $aMenu = array();
        foreach($this->_im->fetchAll() as $information) {
            $fMenu = $information->getRoot() . '/configs/menu.xml';
            if (!file_exists($fMenu)) {
                continue;
            }

            $xMenu = simplexml_load_file($fMenu);
            foreach($xMenu->xpath('navigation/item') as $xMenuItem) {
                $this->_toMenu($xMenuItem);
            }
        }

        $toComparisionFn = function($a, $b) {
            if ($a['position'] == $b['position']) {
                return 0;
            }
            return ($a['position'] < $b['position']) ? -1 : 1;
        };

        $toResetPosition = function(array & $mi) {
            foreach($mi as $k => $o) {
                unset($mi[$k]['position']);
            }

            return $mi;
        };

        $toMenu = function(array $menu) use (& $toMenu, $toComparisionFn, $toResetPosition) {

            $o = array();
            if (isset($menu['_attributes'])) {
                $o = $menu['_attributes'];
                unset($menu['_attributes']);
            } else {
                $o['text'] = 'Untitled';
                $o['position'] = 100;
            }

            foreach($menu as $k => $value) {
                if (false !== ($mi = $toMenu($value))) {
                    $o['children'][] = $mi;
                }
            }

//            if (empty($o['children']) && empty($o['handler']) && empty($o['href'])) {
//                return false;
//            }

            if (!empty($o['children'])) {
                usort($o['children'], $toComparisionFn);
                $toResetPosition($o['children']);
            }
            return $o;
        };

        $result = array();
        foreach($this->_menu as $m) {
            if (false !== ($mResult = $toMenu($m))) {
                $result[] = $mResult;
            }
        }

        usort($result, $toComparisionFn);
        $toResetPosition($result);

        return $result;
    }

    protected function _toMenu(SimpleXMLElement $xItem, array $aParent = null)
    {
        $m = & $this->_menu;

        $path = array_filter(explode('/', null === $aParent ? trim($xItem['path']) : $aParent['_attributes']['path']));

        foreach($path as $pName) {
            if (!isset($m[$pName])) {
                $m[$pName] = array();
            }

            $m = & $m[$pName];
        }

        $name = trim($xItem['name']);
        $m[$name]['_attributes']['name'] = $name;

        $m = & $m[$name];
        $a = & $m['_attributes'];
        $a['position'] = (int) $xItem['position'];

        foreach(array(
            'text', 'href', 'target',
            'position', 'class', 'type'
        ) as $o) {
            if (isset($xItem->$o)) {
                $a[$o] = trim($xItem->$o);
            }
        }

        foreach(array('module', 'controller', 'action') as $o) {
            if (isset($xItem->mvc->$o)) {
                $a[$o] = trim($xItem->mvc->$o);
            }
        }

        $a['params'] = (array) $xItem->mvc->params;
        foreach($a['params'] as $pk => $pv) {
            if ('null' == $pv) {
                $a['params'][$pk] = null;
            }
        }

        $a['path'] = '/' . join('/', $path);
        if (!empty($path)) {
            $a['path'] .= '/';
        }

        $a['path'] .= $a['name'];

        if (! isset($xItem->children)) {
            return;
        }

        foreach($xItem->children->xpath('item') as $xItemChild) {
            $this->_toMenu($xItemChild, $m);
        }
    }
}