<?php

class Menu_Service_MenuItem extends OSDN_Application_Service_Dbable
{
    /**
     * @var Menu_Model_DbTable_Menu
     */
    protected $_table;

    protected $_menuRow;

    public function __construct(\Menu_Model_Menu $menuRow)
    {
        $this->_menuRow = $menuRow;

        parent::__construct();

        $this->_table = new \Menu_Model_DbTable_MenuItem();

        $this->_attachValidationRules('default', array(
            'name'      => array(array('StringLength', 1, 255), 'presence' => 'required', 'allowEmpty' => false)
        ));
    }

    /**
     * Create the new menu row
     *
     * @param array $data
     * @return Menu_Model_DbTable_MenuRow
     */
    public function create(array $data)
    {
        $f = $this->_validate($data);

        $menuRow = $this->_table->createRow($f->getData());

        $this->getAdapter()->beginTransaction();
        try {

            $menuRow->save();

            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $menuRow;
    }

    /**
     * @param array $params
     * @return OSDN_Db_Response
     */
    public function fetchAllWithResponse(array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('a' => $this->_table->getTableName()),
                $this->_table->getAllowedColumns(\OSDN_Db_Table_Abstract::OMIT_FETCH_ROW)
            );

        if (!empty($params['categoryId']) || !empty($params['luid'])) {
            $select->joinLeft(
                array('acr' => 'menuCategoryRel'),
                'acr.menuId = a.id',
                array(
                    'categoryId'
                )
            );

            if (!empty($params['categoryId'])) {
                $select->where('acr.categoryId = ?', $params['categoryId'], Zend_Db::INT_TYPE);
            }

            if (!empty($params['luid'])) {
                $select->joinLeft(
                    array('ac' => 'menuCategory'),
                    'ac.id = acr.categoryId',
                    array()
                );
                $select->where('ac.luid = ?', (string) $params['luid']);
            }
        }

        $select->where('menuId = ?', $this->_menuRow->getId());
        $select->order('createdDatetime DESC');
        $fields = array(
            'a.title'    => 'title',
            'a.isActive' => 'isActive'
        );

        $this->_initDbFilter($select, $this->_table, $fields)->parse($params);
        return $this->getDecorator('response')->decorate($select);
    }

    public function fetchAllByParentId($parentId = null, array $params = array())
    {
        $select = $this->_table->select(true);
        $select->order('name', 'ASC');

        if (empty($parentId)) {
            $select->where('parentId IS NULL');
        } else {
            $select->where('parentId = ?', $parentId, Zend_Db::INT_TYPE);
        }

        return $this->getDecorator('response')->decorate($select);
    }

    public function update($id, array $data)
    {
        $menuRow = $this->find($id);
        $f = $this->_validate($data);

        $this->getAdapter()->beginTransaction();
        try {
            $menuRow->setFromArray($f->getData());
            $menuRow->save();

            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $menuRow;
    }

    /**
     * @return \Menu_Model_Menu
     */
    public function move($id, $parentId)
    {
        $menuRow = $this->find($id);
        $menuRow->parentId = $parentId;

        $menuRow->save();

        return $menuRow;
    }

    /**
     * @todo should be done via event manager
     *
     * @FIXME Add main global transation for that
     */
    public function delete($id)
    {
        $menuRow = $this->find($id);

        /**
         * Delete attached file-stock items
         */
        $menuFileStock = new \Menu_Service_MenuFileStock();
        $fileStockRawRow = $menuFileStock->deleteByMenu($menuRow);

        /**
         * Delete connected categories relations
         */
        $menuCategory = new MenuCategory_Service_MenuCategory();
        $menuCategory->deleteAllMenuConnections($menuRow);

        return false !== $menuRow->delete();
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \Menu_Model_DbTable_MenuRow
     */
    public function find($id, $throwException = true)
    {
        $menuRow = $this->_table->findOne($id);
        if (null === $menuRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find menu #' . $id);
        }

        return $menuRow;
    }
}