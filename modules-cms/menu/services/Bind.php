<?php

class Menu_Service_Bind extends \OSDN_Application_Service_Abstract
{
    protected $_menu;
    protected $_view;

    public function __construct(\Menu_Model_Menu $menu, Zend_View $view)
    {
        parent::__construct();

        $this->_menu = $menu;
        $this->_view = $view;
    }

    protected function _init()
    {
//        $this->_attachValidationRules('default', array(
//            'name'      => array(array('StringLength', 1, 255), 'presence' => 'required', 'allowEmpty' => false),
//            'isActive'  => array('boolean', 'presence' => 'required', 'allowEmpty' => true)
//        ));

        parent::_init();
    }

    /**
     * @return \OSDN_Application_Model_Interface|null
     */
    public function toBindInstance()
    {
        if (null === $this->_menu->luid) {
            return null;
        }

        $as = new Article_Service_Article();
        return $as->find($this->_menu->luid);
    }

    /**
     * @FIXME
     *
     * @return \OSDN_Application_Model_Interface
     */
    protected function _toBindInstance($data)
    {
        $as = new Article_Service_Article();
        return $as->find($data['luid']);
    }

    public function connect(array $data)
    {
        $instance = $this->_toBindInstance($data);

        $this->_menu->luid = $instance->getId();
        $this->_menu->link = $this->_view->modules('article')->articleLink($instance);
        $this->_menu->save();

        return $this->_menu;
    }
}