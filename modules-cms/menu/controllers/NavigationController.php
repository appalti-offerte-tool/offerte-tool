<?php

class Menu_NavigationController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Menu_Service_Navigation
     */
    protected $_service;

    public function init()
    {
        $this->_service = new \Menu_Service_Navigation();

        $this->_helper->contextSwitch()
            ->addActionContext('fetch-all', 'json')
            ->initContext();

        parent::init();
    }

    public function getResourceId()
    {
        return 'menu:list';
    }

    public function indexAction()
    {
        // prepare layout
    }

    public function fetchAllAction()
    {
        $rowsetWithModuleInformation = $this->_service->fetchAllWithModuleInformation();

        $output = array();
        foreach($rowsetWithModuleInformation as $module) {
            foreach($module['menu'] as $menu) {

                $menu['mtitle'] = $module['information']->getTitle();
                $menu['mname'] = $module['information']->getName();

                $output[] = $menu;
            }
        }

        $this->view->rowset = $output;
        $this->view->total = count($output);
        $this->view->success = true;
    }
}