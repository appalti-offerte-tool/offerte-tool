<?php

class Menu_MainController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Menu_Service_Menu
     */
    protected $_service;

    public function init()
    {
        $this->_service = new \Menu_Service_Menu();

        parent::init();
    }

    public function getResourceId()
    {
        return 'menu:list';
    }

    public function indexAction()
    {}

    public function comboBoxAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');

        $response = $this->_service->fetchAllWithResponse();
        $this->view->assign($response->toArray());
    }
}