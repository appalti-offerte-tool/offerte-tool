<?php

class Menu_BindController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Menu_Service_Menu
     */
    protected $_service;

    public function init()
    {
        $this->_service = new Menu_Service_Menu();

        $this->_helper->contextSwitch()
            ->addActionContext('index', 'json')
            ->addActionContext('fetch', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('move', 'json')
            ->addActionContext('fetch-all-by-parent-id', 'json')
            ->initContext();

        parent::init();
    }

    public function getResourceId()
    {
        return 'menu:list';
    }

    public function indexAction()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        $mm = $bootstrap->getResource('ModulesManager');

        $this->view->rowset = $mm->fetchAll();
    }

    public function updateAction()
    {
        try {
            $menu = $this->_service->find($this->_getParam('menuId'));

            $bind = new \Menu_Service_Bind($menu, $this->view);
            $bind->connect($this->_getAllParams());

            $this->view->success = true;
            $this->_helper->information('Updated successfully.', array(), E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}
