<?php

class Menu_KindController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Menu_Service_Kind
     */
    protected $_service;

    public function init()
    {
//        $this->_service = new \Menu_Service_Kind();

        $this->_helper->contextSwitch()
            ->addActionContext('index', 'json')
            ->addActionContext('fetch', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('move', 'json')
            ->addActionContext('fetch-all-by-parent-id', 'json')
            ->initContext();

        parent::init();
    }

    public function getResourceId()
    {
        return 'menu:list';
    }

    public function indexAction()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        $mm = $bootstrap->getResource('ModulesManager');

        $this->view->rowset = $mm->fetchAll();
    }
}
