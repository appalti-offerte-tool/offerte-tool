<?php

class Menu_MenuItemController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Menu_Service_Menu
     */
    protected $_service;

    public function init()
    {
        $menu = new Menu_Service_Menu();
        $menuRow = $menu->find($this->_getParam('menuId'));

        $this->_service = new \Menu_Service_MenuItem($menuRow);

        $this->_helper->contextSwitch()
            ->addActionContext('index', 'json')
            ->addActionContext('fetch', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('move', 'json')
            ->addActionContext('fetch-all-by-parent-id', 'json')
            ->initContext();

        parent::init();
    }

    public function getResourceId()
    {
        return 'menu:list';
    }

    public function indexAction()
    {
        // prepare layout
    }

    public function fetchAllByParentIdAction()
    {
        $response = $this->_service->fetchAllByParentId($this->_getParam('node'), $this->_getAllParams());
        $response->setRowsetCallback(function($row) {
            return $row;
        });

        $this->view->assign($response->getRowset());
    }

    public function fetchAction()
    {
        $row = $this->_service->find($this->_getParam('id'));

        $this->view->row = $row->toArray();
        $this->view->success = true;
    }

    public function createAction()
    {
        try {
            $menuRow = $this->_service->create($this->_getAllParams());
            $this->view->menuId = $menuRow->getId();
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $menuRow = $this->_service->update($this->_getParam('id'), $this->_getAllParams());
            $this->view->menuId = $menuRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $this->view->success = $this->_service->delete($this->_getParam('id'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function moveAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $menuRow = $this->_service->move($this->_getParam('id'), $this->_getParam('parentId'));
            $this->view->menuId = $menuRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function comboBoxAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');
    }
}