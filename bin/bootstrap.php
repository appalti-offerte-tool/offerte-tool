<?php

define('SILENT_MODE', true);
$application = require_once __DIR__ . '/../index.php';
if (! $application instanceof Zend_Application) {
    throw new Zend_Application_Exception('The "$application" should be instance of Zend_Application');
}

$isModuleRequest = isset($_SERVER['argv'][3]) && 0 === strcasecmp('module', $_SERVER['argv'][3]);

try {

    $application->bootstrap('dbable');
    if (! $isModuleRequest) {
        $application->bootstrap('modulees');
    }

    $application->bootstrap('frontController');
    $application->bootstrap('auth');

    foreach (new DirectoryIterator(APPLICATION_PATH) as $di) {
        if ($di->isDot() || !$di->isDir() || substr($di->getBasename(), 0, 1) == '.') {
            continue;
        }

        if (preg_match('/^modules/', $di->getBasename())) {
            Zend_Controller_Front::getInstance()->addModuleDirectory($di->getPathname());
        }
    }

} catch (Exception $exception) {
    echo 'Error: ' . $exception->getMessage();
    exit(1);
}