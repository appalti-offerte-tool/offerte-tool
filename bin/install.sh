#!/bin/sh
#############################################################################
# Installation
#
# Small installation script
#
#############################################################################

# Binary
chmod a+x ./zf.sh
chmod a+x ./zfc.sh

# Run migration
./zfc.sh up migration module
./zfc.sh up migration i18n
./zfc.sh up migration cron-schedule
./zfc.sh up migration ddbm
./zfc.sh up migration account
./zfc.sh up migration default
./zfc.sh up migration application
./zfc.sh up migration configuration
./zfc.sh up migration notification
./zfc.sh up migration comment
./zfc.sh up migration file-stock
./zfc.sh up migration document
./zfc.sh up migration menu
./zfc.sh up migration