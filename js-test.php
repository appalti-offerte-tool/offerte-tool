<?php

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Javascript test</title>
    </head>
    <body id="body">
        <h1>Resultaat:</h1>
        <noscript>
            Javascript is uitgeschakeld.
        </noscript>
        <script type="text/javascript">
            window.onload = function(){
                var p = document.createElement('p');
                p.innerHTML = 'Javascript werkt.';

                var aText = document.createTextNode('Klik hier om uw browser te bepalen.');
                var a = document.createElement('a');
                a.href = 'http://whichbrowser.net/';
                a.appendChild(aText);

                var body = document.getElementById('body');
                body.appendChild(p);
                body.appendChild(a);
            };
        </script>

    </body>
</html>