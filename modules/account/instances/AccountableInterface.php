<?php

interface Account_Instance_AccountableInterface
{
    /**
     * Set accountId
     *
     * @param int $accountId
     */
    function setAccountId($accountId);

    /**
     * Retrieve account
     *
     */
    function getAccountId();
}