<?php

class Account_Model_DbTable_AccountRow extends \Zend_Db_Table_Row_Abstract implements \Account_Model_AccountInterface
{
    /**
     * @var Zend_Acl
     */
    protected $_acl;

    protected $_roleRow;

    protected $_companyRow;

    protected $_contactPersonRow;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    public function setActive($flag)
    {
        if (true === (boolean)$flag) {
            $this->isActive = 1;
        } else {
            $this->isActive = 0;
            $this->username =  $this->username . '_inactive_' . time();
        }

        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see Account_Model_AccountInterface::getRoleId()
     */
    public function getRoleId()
    {
        return (int) $this->roleId;
    }

    public function getRoleLuid()
    {
        return $this->getRole()->luid;
    }

    /**
     * Retrieve role row
     *
     * @return Account_Model_DbTable_AclRoleRow
     */
    public function getRole()
    {
        if (null === $this->_roleRow) {
            $this->_roleRow = $this->findParentRow('Account_Model_DbTable_AclRole', 'Role');
        }

        return $this->_roleRow;
    }

    /**
     * @FIXME Hardcoded role luid
     */
    public function isProposalBuilder()
    {
        return 0 === strcasecmp('proposalBuilder', $this->getRole()->luid);
    }

    /**
     * @FIXME Hardcoded role luid
     */

    public function isCompanyOwner()
    {
        return 0 === strcasecmp('companyOwner', $this->getRole()->luid);
    }

    /**
     * @FIXME Hardcoded role luid
     */
    public function isManager()
    {
        return 0 === strcasecmp('manager', $this->getRole()->luid);
    }

    /**
     * @FIXME Hardcoded role luid
     */
    public function isTextWriter()
    {
        return 0 === strcasecmp('textWriter', $this->getRole()->luid);
    }

    /**
     * (non-PHPdoc)
     * @see Account_Model_AccountInterface::getRoleName()
     */
    public function getRoleName()
    {
        return $this->getRole()->name;
    }

    /**
     * (non-PHPdoc)
     * @see Account_Model_AccountInterface::getFullname()
     */
    public function getFullname()
    {
        return $this->fullname ?
                        $this->fullname :
                        sprintf('%s%s%s', $this->firstname,  $this->prefix ? ' ' . $this->prefix . ' ' : ' ', $this->lastname);
    }

    public function getLastnameWithSalutation()
    {
        return sprintf('%s %s', 'man' === $this->sex ? 'heer' : 'mevrouw', $this->lastname);
    }

    public function getSalutation()
    {
        return 'man' === $this->sex ? 'heer' : 'mevrouw';
    }

    /**
     * (non-PHPdoc)
     * @see Account_Model_AccountInterface::getUsername()
     */
    public function getUsername()
    {
        return $this->username;
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getLanguage()
    {
        return !empty($this->language) ? $this->language : 'nl';
    }

    public function toArrayWithOmit($mode = OSDN_Db_Table_Abstract::OMIT_FETCH_ROW)
    {
        $o = $this->toArray();

        return $this->getTable()->filterRow($o, $mode);
    }

    public function getParentRow()
    {
        return $this->findParentRow('Account_Model_DbTable_Account', 'Parent');
    }

    public function isChildOf(Account_Model_AccountInterface $accountRow)
    {
        return !empty($this->parentId) && $this->parentId == $accountRow->getId();
    }

    /**
     * Retrieve companyRow for current account
     *
     * @param bool $throwException Throw exception when row is null
     *
     * @return Company_Model_CompanyRow
     * @throws OSDN_Exception
     */
    public function getCompanyRow($throwException = true)
    {
        if (null === $this->_companyRow) {
            if (empty($this->parentId)) {
                $rowset = $this->findDependentRowset('Company_Model_DbTable_Company', 'Account');
            } else {
                $parentAccount = $this->getParentRow();
                $rowset = $parentAccount->findDependentRowset('Company_Model_DbTable_Company', 'Account');
            }

            if ($rowset->count() == 1) {
                $this->_companyRow = $rowset->current();
            } elseif ($rowset->count() > 1) {
                throw new OSDN_Exception('Database intergrity corrupted.');
            }

            if (null === $this->_companyRow) {
                if (true === $throwException) {
                    throw new OSDN_Exception('Unable to find company');
                }
            } elseif(!$this->_companyRow->isRoot()) {
                throw new OSDN_Exception('Only main companies allowed');
            }
        }

        return $this->_companyRow;
    }

    public function getContactPersonRow($throwException = true)
    {
        if (null === $this->_contactPersonRow) {
            $select = $this->select()->where('isActive = 1');
            $rowset = $this->findDependentRowset('Company_Model_DbTable_ContactPerson', 'Account', $select);
            if ($rowset->count() == 1) {
                $this->_contactPersonRow = $rowset->current();
            } elseif ($rowset->count() > 1) {
                throw new OSDN_Exception('Database integrity corrupted.');
            }

            if (null === $this->_contactPersonRow) {

                $companyRow = $this->getCompanyRow($throwException);

                if (null !== $companyRow) {
                    $this->_contactPersonRow = $companyRow->getCompanyContactPersonRow();
                }

                if (null === $this->_contactPersonRow && true === $throwException) {
                    throw new OSDN_Exception('Unable to find contact person');
                }
            }
        }

        return $this->_contactPersonRow;
    }

    public function getAccountRelatedContactPersonRow($throwException = true)
    {
        if (null === $this->_contactPersonRow) {
            $select = $this->select()->where('isActive = 1');
            $rowset = $this->findDependentRowset('Company_Model_DbTable_ContactPerson', 'Account', $select);
            if ($rowset->count() == 1) {
                $this->_contactPersonRow = $rowset->current();
            } elseif ($rowset->count() > 1) {
                throw new OSDN_Exception('Database integrity corrupted.');
            }
        }

        if (null === $this->_contactPersonRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find contact person');
        }

        return $this->_contactPersonRow;
    }

    /**
     * (non-PHPdoc)
     * @see Account_Model_AccountInterface::hasAcl()
     */
    public function hasAcl()
    {
        return null !== $this->_acl;
    }

    /**
     * (non-PHPdoc)
     * @see Account_Model_AccountInterface::getAcl()
     */
    public function getAcl()
    {
        return $this->_acl;
    }

    /**
     * (non-PHPdoc)
     * @see Account_Model_AccountInterface::setAcl()
     */
    public function setAcl(Zend_Acl $acl)
    {
        $this->_acl = $acl;
        return $this;
    }

    public function isAdmin()
    {
        return (0 === strcasecmp('admin', $this->getRoleName()));
    }

    public function isGuest()
    {
        return 0 === strcasecmp('guest', $this->getRoleName());
    }

    public function isBeheerder()
    {
        return ($this->admin || ($this->roleId == 6));
    }

    /**
     * Retrieve row with guest account
     *
     * @throws OSDN_Exception
     * @return \Account_Model_DbTable_AccountRow
     */
    public static function initWithGuest()
    {
        $table = new Account_Model_DbTable_Account();

        /**
         * @FIXME
         */
        $accountRow = $table->findOne(2);
        if (null === $accountRow || 2 !== $accountRow->getRoleId()) {
            throw new OSDN_Exception('Unable to find guest account');
        }

        return $accountRow;
    }

    /**
     * @var Account_Model_DbTable_Account
     */
    private static $__table;

    /**
     * (non-PHPdoc)
     * @see Zend_Db_Table_Row_Abstract::__wakeup()
     */
    public function __wakeup()
    {
        parent::__wakeup();

        if (null === self::$__table) {
            self::$__table = new Account_Model_DbTable_Account();
        }

        $this->_table = self::$__table;
        $this->_connected = true;
    }

    public function acl($pResource, $sResource = null, $privilege = null)
    {
        $helper = new Account_View_Helper_Acl();
        return $helper->acl($pResource, $sResource, $privilege);
    }

    public function getAccountCode()
    {
        return $this->_table->getAccountCode($this);
    }
}