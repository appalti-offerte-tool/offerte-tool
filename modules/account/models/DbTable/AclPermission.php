<?php

class Account_Model_DbTable_AclPermission extends OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'aclPermission';

    protected $_rowClass = 'Account_Model_DbTable_AclPermissionRow';
}