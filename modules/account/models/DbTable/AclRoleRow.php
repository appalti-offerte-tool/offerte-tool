<?php

class Account_Model_DbTable_AclRoleRow extends \Zend_Db_Table_Row_Abstract
    implements \Zend_Acl_Role_Interface, \OSDN_Application_Model_Interface
{
    /**
     * The predefined role list
     */
    const ROLE_ADMIN = 1;
    const ROLE_GUEST = 2;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Role_Interface::getRoleId()
     */
    public function getRoleId()
    {
        return $this->id;
    }

    /**
     * We consider that when role id is less then 10
     * the role is using for some internal stuff
     *
     * @return boolean
     */
    public function isProtected()
    {
        return $this->id < 10;
    }
}