<?php

class Account_Model_DbTable_Account extends OSDN_Db_Table_Abstract
{
    /**
     * Additional salt to encript the password hash
     *
     * @FIXME Should be replaced with value from configuration
     */
    const SALT = '6b32916a39bfe06f0aac6c6fd4ff12ae';

    protected $_primary = 'id';

    protected $_name    = 'account';

    protected $_rowClass = 'Account_Model_DbTable_AccountRow';

    protected $_omitColumns = array(
        'password'          => self::OMIT_FETCH_ROW,
        'state'             => self::OMIT_FETCH_ALL,
        'luid'              => self::OMIT_FETCH_ALL,
        'luidRelationId'    => self::OMIT_FETCH_ALL
    );

    protected $_personalInformationColumns = array(
        'username'  => true,
        'email'     => true
    );

    protected $_nullableFields  = array(
        'hash', 'firstname', 'prefix', 'title', 'sex',
        'department', 'function',
        'language'
    );

    protected $_referenceMap    = array(
        'Role'    => array(
            'columns'       => 'roleId',
            'refTableClass' => 'Account_Model_DbTable_AclRole',
            'refColumns'    => 'id'
        ),
        'Parent'    => array(
            'columns'       => 'parentId',
            'refTableClass' => 'Account_Model_DbTable_Account',
            'refColumns'    => 'id'
        )
    );

    protected function _toFullname($data)
    {
        $o = '';
        foreach(array('firstname', 'prefix', 'lastname') as $field) {
            if (isset($data[$field])) {
                $o .= $data[$field] . ' ';
            }
        }

        return trim($o);
    }


    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        $data['password'] = md5($data['password'] . self::SALT);
        $data['fullname'] = $this->_toFullname($data);

        return parent::insert($data);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
        if (!empty($data['password'])) {
            $data['password'] = md5($data['password'] . self::SALT);
        }

        $data['fullname'] = $this->_toFullname($data);

        return parent::update($data, $where);
    }

    public function getAccountCode(Account_Model_DbTable_AccountRow $accountRow)
    {
        if ($accountRow->getId())
        {
            $select = $this->select()
                                ->from($this->getTableName(), array('hash' => 'SHA1(CONCAT(username,password,hash))'))
                                ->where('id = ?', $accountRow->getId())
            ;
            if (($row = $this->fetchRow($select)) != false)
            {
                return $row->hash;
            }
        }
        return '';
    }

    public function getByAccountCode($account_code)
    {
        $select = $this->select()
                        ->where('SHA1(CONCAT(username,password,hash)) = ?', $account_code)
        ;
        return $this->fetchRow($select);

    }
}