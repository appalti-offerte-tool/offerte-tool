<?php

class Account_Model_DbTable_AclRole extends OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'aclRole';

    protected $_rowClass = 'Account_Model_DbTable_AclRoleRow';

    /**
     * copies a role to a new role using given data ( including permissions )
     *
     * @param Account_Model_DbTable_AclRoleRow $fromRole role to copy from
     * @param array $data optional data to fill copy with, default empty array
     * @param bool $copyPermissions optional flag for copying permissions too
     * @return Account_Model_DbTable_AclRoleRow $toRole
     * @throws Exception
     */
    public function copyRole(Account_Model_DbTable_AclRoleRow $fromRole, array $data = array(), $copyPermissions = false)
    {
        $copyPermissions = (bool)!empty($copyPermissions);
        $toRole = $this->createRow($fromRole->toArray());
        $toRole->setFromArray($data);
        $toRole->id = null;
        $toRole->save();
        if ($copyPermissions && !$this->copyPermissions($fromRole, $toRole))
        {
            throw new Exception("Couldn't copy permissions");
        }
        return $toRole;
    }

    /**
     * copies permissions from 1th given role to 2th givene role
     *
     * @param Account_Model_DbTable_AclRoleRow $fromRole permissions are copied from this role
     * @param Account_Model_DbTable_AclRoleRow $toRole permissions are copied to this role
     * @return bool true on success, true on false
     */
    public function copyPermissions(Account_Model_DbTable_AclRoleRow $fromRole, Account_Model_DbTable_AclRoleRow $toRole)
    {
        $_permissions = new Account_Model_DbTable_AclPermission();
        $query = "
        INSERT INTO `".$_permissions->getTableName()."`
        SELECT
            0 AS id,
            ".(int)$toRole->getId()." AS roleId,
            resource,
            privilege
        FROM `".$_permissions->getTableName()."`
        WHERE roleId = ".(int)$fromRole->getId()."
        ;";
        if ($this->getAdapter()->query($query))
        {
            return true;
        }
        return false;
    }
}