<?php

interface Account_Model_AccountInterface extends \OSDN_Application_Model_Interface
{
    public function getUsername();

    public function getFullname();

    public function getRoleId();

    public function getRoleName();

    public function hasAcl();

    public function getAcl();

    public function setAcl(Zend_Acl $acl);
}