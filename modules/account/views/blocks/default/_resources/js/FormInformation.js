Ext.define('Module.Application.Model.Error', {
    extend: 'Ext.data.Model',
    fields: [
        'message', 'type', 'field',
        {name: 'id', mapping: 'field'},
        {name: 'msg', mapping: 'message'}
    ]
});

Ext.define('Module.Account.Block.FormInformation', {
    extend: 'Ext.form.Panel',
    alias: 'widget.account.block.form-information',

    bodyPadding: 5,

    roleId: null,
    roleName: null,
    roleLuid: null,

    accountId: null,
    trackResetOnLoad: true,
    waitMsgTarget: true,

    passwordCt: null,

    wnd: null,

    initComponent: function() {

        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.Account.Model.Account',
            type: 'json',
            root: 'row'
        });

        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
            model: 'Module.Application.Model.Error',
            type: 'json',
            root: 'messages',
            successProperty: 'success'
        });



        this.items = [{
            xtype: 'textfield',
            fieldLabel: lang('E-mail') + '<span class="asterisk">*</span>',
            name: 'username',
            allowBlank: false,
            anchor: '100%'
        }, {
            xtype: 'textfield',
            fieldLabel: lang('First name'),
            name: 'firstname',
            allowBlank: true,
            anchor: '100%'
        }];
        var item = [
         this.accountCombo = new Module.Account.Acl.ComboBox( {
                fieldLabel: lang('Role'),
                name: 'roleId',
                hiddenName: 'roleId',
                allowBlank: false,
                anchor: '100%'

            })];

        var itemsSecond = [
         this.companyCombo = new Module.Company.ComboBox.Company({
            fieldLabel: lang('Company'),
            name: 'parentId',
            hiddenName: 'parentId',
            allowBlank: false,
            anchor: '100%'
        }),{
            xtype: 'textfield',
            fieldLabel: lang('Prefix'),
            name: 'prefix',
            allowBlank: true,
            anchor: '100%'
        }, {
            xtype: 'textfield',
            fieldLabel: lang('Last name'),
            name: 'lastname',
            allowBlank: false,
            anchor: '100%'
        },{
            xtype: 'combobox',
            fieldLabel: lang('Title'),
            store: [['Dhr.', 'Dhr.'], ['Mevr.','Mevr.']],
            queryMode: 'local',
            triggerAction: 'all',
            editable: false,
            name: 'title',
            hiddenName: 'title',
            forceSelection: false,
            valueField: 'field1',
            displayField: 'field2',
            allowBlank: true,
            anchor: '100%'
        }, {
            xtype: 'combobox',
            fieldLabel: lang('Sex'),
            store: [['man', 'Man'], ['vrouw', 'Vrouw']],
            queryMode: 'local',
            triggerAction: 'all',
            editable: false,
            name: 'sex',
            hiddenName: 'sex',
            forceSelection: false,
            valueField: 'field1',
            displayField: 'field2',
            allowBlank: true,
            anchor: '100%'
        }, {
            xtype: 'textfield',
            fieldLabel: lang('Department'),
            name: 'department',
            allowBlank: true,
            anchor: '100%'
        }, {
            xtype: 'textfield',
            fieldLabel: lang('Function'),
            name: 'function',
            allowBlank: true,
            anchor: '100%'
        }, {
            xtype: 'textfield',
            fieldLabel: lang('Credits'),
            name: 'credits',
            allowBlank: false,
            anchor: '100%',
            value: '0'
        }, {
            xtype: 'textfield',
            fieldLabel: lang('Email'),
            name: 'email',
            anchor: '100%',
            vtype: 'email',
            allowBlank: false
        }, {
            xtype: 'textfield',
            fieldLabel: lang('Phone'),
            name: 'phone',
            anchor: '100%'
        }, {
            xtype: 'module.i18n.language.combo-box',
            fieldLabel: lang('Language'),
            name: 'language',
            hiddenName: 'language',
            anchor: '100%',
            value: 'nl'
        }, {
            xtype: 'checkbox',
            name: 'isActive',
            fieldLabel: lang('Active'),
            inputValue: '1',
            uncheckedValue: '0',
            checked: true
        }, this.passwordCt = new Ext.form.FieldSet({
            title: lang('Password'),
            checkboxToggle: this.accountId > 0,
            checkboxName: 'passwordCheckbox',
            collapsed: this.accountId > 0,
            items: [{
                xtype: 'textfield',
                type: 'password',
                name: 'password',
                fieldLabel: lang('Password') + '<span class="asterisk">*</span>',
                allowBlank: false,
                labelWidth: 150,
                anchor: '100%'
            }, {
                xtype: 'textfield',
                type: 'password',
                name: 'confirmPassword',
                fieldLabel: lang('Confirm password') + '<span class="asterisk">*</span>',
                allowBlank: false,
                labelWidth: 150,
                anchor: '100%'
            }]
        })];
        if (this.roleLuid != null) {
            this.items = this.items.concat(itemsSecond);
                if ((this.roleLuid == 'textWriter') || (this.roleLuid == 'manager') || (this.roleLuid == 'proposalBuilder'))
                {
                    this.companyCombo.setDisabled(false);

                } else {
                    this.companyCombo.setDisabled(true);
                }

        } else {
            this.items = this.items.concat(item);
            this.items = this.items.concat(itemsSecond);

        }


        this.callParent();

        this.addEvents(
            /**
             * Fires when account is created
             *
             * @param {Module.Account.Form}
             */
            'completed'
        );

        /**
         * @fixme
         * http://www.sencha.com/forum/showthread.php?129144-FieldSet-expand-and-collapse-events
         * Waiting for Ext version 4.1
         */
//        this.passwordCt.on({
//            beforeexpand: this.onTogglePasswordCt,
//            beforecollapse: this.onTogglePasswordCt,
//            scope: this
//        });

        /**
         * @fixme Kill me in 4.1
         */
        this.passwordCt.setExpanded = Ext.Function.createSequence(this.passwordCt.setExpanded, this.onTogglePasswordCt, this);
        this.onTogglePasswordCt();


        this.accountCombo.on('change', function(t){
            var me = this
            var l = t.displayTplData[0]['luid'];

            if ((l == 'textWriter') ||( l=='manager' )||( l == 'proposalBuilder'))
            {
               this.companyCombo.setDisabled(false);
               this.companyCombo.setVisible(true);
            } else {
               this.companyCombo.setVisible(false);
               this.companyCombo.setDisabled(true);
            }
        },this);



    },

    roleCase: function()
    {
//        console.log(this);
//        console.log(this.form.findField('firstname'));
//         this.form.findField('password').setDisabled(this.passwordCt.collapsed);
    },

    onTogglePasswordCt: function() {
        this.form.findField('password').setDisabled(this.passwordCt.collapsed);
        this.form.findField('confirmPassword').setDisabled(this.passwordCt.collapsed);
        this.form.isValid();
    },

    doLoad: function() {

        if (!this.accountId) {
            return;
        }

        var params = Ext.applyIf({
            id: this.accountId,
            format: 'json'
        }, this.invokeArgs || {});

        this.form.load({
            url: link('account', 'main', 'fetch', params),
            waitMsg: Ext.LoadMask.prototype.msg,
            success: function(response, result, type) {
                Application.notificate(result);
                if (result.success) {
                    this.onTogglePasswordCt();
                }
            },
            scope: this
        });
    },

    onSubmit: function(panel, w) {

        if (true !== this.form.isValid()) {
            return;
        }
        var params = Ext.apply({}, this.invokeArgs || {});
        if (this.accountId) {
            params['id'] = this.accountId;
            var url = link('account', 'main', 'update', {format: 'json'});
        } else {
            params['roleId'] = this.roleId;
            var url = link('account', 'main', 'create', {format: 'json'});
        }

        var acField = this.form.findField('isActive');
        if (!acField.checked) {
            params['isActive'] = '0';
        }

        this.form.submit({
            url: url,
            method: 'post',
            params: params,
            waitMsg: Ext.LoadMask.prototype.msg,
            success: function(form, action) {

                var responseObj = Ext.decode(action.response.responseText);
                Application.notificate(action);

                if (action.result.success) {
                    this.fireEvent('completed', responseObj.accountId, action.response);
                    this.wnd && this.wnd.close();
                }
            },
            scope: this
        });
    },

    showInWindow: function() {

        var w = this.wnd = new Ext.Window({
            title:  this.accountId ? lang('Update account') : lang('Create account with role "{0}"', this.roleName),
            iconCls: 'osdn-acl-role-icon-16x16',
            resizable: false,
            width: 415,
            modal: true,
            border: false,
            items: [this],
            buttons: [{
                iconCls: 'icon-submit-16',
                text: lang('Submit'),
                handler: function() {
                    this.onSubmit(this, w);
                },
                scope: this
            }, {
                text: lang('Cancel'),
                handler: function() {
                    w.close();
                }
            }],
            scope: this
        });

        w.show();
        this.doLoad();

        return w;
    }
});