<?php

class Account_View_Helper_Acl extends Zend_View_Helper_Abstract
{
    /**
     * @var Account_Model_AccountInterface
     */
    protected $_account;

    public function __construct()
    {
        $this->_account = Zend_Auth::getInstance()->getIdentity();
        try {
            $this->_company = $this->_account->getCompanyRow();
        } catch(Exception $e) {
            $this->_company = null;
        }
    }

    /**
     * Assert permission
     *
     * @param string $pResource
     * @param string $sResource    [OPTIONAL]
     * @param string $privilege    [OPTIONAL]
     *
     * @return boolean
     */
    public function acl($pResource, $sResource = null, $privilege = null)
    {
        $request = Zend_Controller_Front::getInstance()
                                            ->getRequest();

        $resource = $pResource;
        if (null !== $sResource) {
            $resource .= ':' . $sResource;
        }

        $acl    = $this->_account->getAcl();
        $cAcl   = isset($this->_company)? $this->_company->getAcl():null;
//echo '<pre>'.print_r($acl->getRoles(),1).'</pre>';
if ($request->getParam('acl') == 'show') {
    echo $resource.': '.(int)$cAcl->has($resource).'-'.(int)$acl->has($resource).'<br/>';
}
        if (!$acl->has($resource) || ($cAcl && !$cAcl->has($resource))) {
            return null;
        }
        $allowed = false;
        foreach($acl->getRoles() as $roleId)
        {
            $allowed |= $acl->isAllowed($roleId, $resource, $privilege ?: 'default');
        }
if ($request->getParam('acl') == 'show') {
    echo $resource.':'.$privilege.' : '.$allowed.'<br/>';
}
        return $allowed;
    }
}