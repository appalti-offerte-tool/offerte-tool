<?php

class Account_View_Helper_Account extends Zend_View_Helper_Abstract
{
    public function account($name, $value = null, $attribs = array())
    {
        $options = array();
        $attribs['data-xtype'] = 'module.account.helper.account';

        if (!empty($value)) {
            $service = new Account_Service_Account();
            if (null != ($account = $service->find($value))) {
                $options[$account->id] = $account->fullname;
            }
        }

        if (isset($attribs['data-cfg']) && is_array($attribs['data-cfg'])) {
            $attribs['data-cfg'] = Zend_Json::encode($attribs['data-cfg']);
        }
        
        return $this->view->formSelect($name, $value, $attribs, $options);
    }
}