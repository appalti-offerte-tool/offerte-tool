<?php

class Account_View_Helper_Authentication extends Zend_View_Helper_Abstract
{
    public function authentication()
    {
        return $this;
    }

    public function __toString()
    {
        /**
         * @FIXME
         */
        $version = OSDN_Version::getInstance(APPLICATION_PATH . '/configs/version.xml');

        $this->view->releaseDate = $version->getDate();
        $this->view->releaseVersion = $version->getVersion();

        return $this->view->render('authentication.phtml');
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->view = clone $view;
        $this->view->setScriptPath(__DIR__ . '/default');

        return $this;
    }
}