Ext.ns('Module.Account.Helper');

Ext.define('Module.Account.Helper.Account', {

    extend: 'Ext.form.ComboBox',

    alias: 'widget.module.account.helper.account',

    permissions: true,

    queryMode: 'remote',

    lazyInit: false,

    editable: true,

    minChars: 1,

    trackResetOnLoad: true,

    valueField: 'id',

    displayField: 'username',

    filteringFields: 'username',

    filteringMode: 'remote',

    filteringFree: true,

    listConfig: {
        cls: ' x-osdn-combo-plainlist',
        width: 400
    },

    preloadRecords: false,

    allowBlank: false,

    initComponent: function() {

        this.columns = [{
            title: lang('Name'),
            width: 360,
            value: '{[fm.ellipsis(values.username, 40)]}'
        }, {
            width: 40,
            cls: 'icon-16 icon-info-16  x-combo-list-tooltip-icon'
        }];


//        this.listClass += ' x-osdn-combo-plainlist';
        this.store = new Ext.data.Store({
            proxy: {
                type: 'ajax',
                url: link('account', 'main', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset',
                    totalProperty: 'total'
                }
            },
            fields: [
                {name:'id', type:'int'},
                'username'
            ],
            pageSize: 15
        });

        this.callParent(arguments);

//        this.on('expand', this.onInitTooltip, this, {single: true});
    }
//    ,

//    onInitTooltip: function() {
//
//        var tip = new Ext.ToolTip({
//            target: this.view.getEl(),
//            delegate: '.x-combo-list-tooltip-icon',
//            renderTo: Ext.getBody(),
//            width: 400,
//            maxHeight: 600,
//            mouseOffset: [20, -200],
//            autoHide: false,
//            trackMouse: true
//        });
//
//        tip.on('beforeshow', function(tip) {
//
//            var rowIndex = this.view.getSelectedIndexes()[0];
//            var record = this.getStore().getAt(rowIndex);
//
//            tip.load(link('employee', 'information', 'index', {id: record.get('id')}, 'block'));
//
//        }, this);
//    }

});

