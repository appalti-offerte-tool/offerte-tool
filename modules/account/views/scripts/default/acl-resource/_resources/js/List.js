Ext.define('Module.Account.Model.Resource', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name', {name: 'isAllowed', type: 'boolean'}
    ],
});

Ext.define('Module.Account.AclResource.List', {

    extend: 'Ext.ux.grid.GridPanel',
    alias: 'module.account.acl-resource.list',

    roleId: null,

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.Account.Model.Resource',
            proxy: {
                type: 'ajax',
                url: link('account', 'acl-resource', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            },
            remoteSort: {sort: 'name', dir: 'asc'}
        });

        var checkColumn = new Ext.ux.CheckColumn({
            header: lang('Allowed'),
            dataIndex: 'isAllowed',
            width: 50
        });


        this.columns = [{
            header: lang('Name'),
            flex: 1,
            dataIndex: 'name',
            sortable : true
        }, checkColumn];

        this.callParent(arguments);

        checkColumn.on('checkchange', function(cc, rowIndex, isChecked) {
            var record = this.getStore().getAt(rowIndex);
            this.onChangeResourceState(record.get('name'), !! isChecked, record);
        }, this);

        this.bbar = ['->', {
            text: lang('Refresh'),
            iconCls: 'x-tbar-loading',
            handler: function() {
                this.getStore().load();
            },
            scope: this
        }];
    },

    onChangeResourceState: function(name, value, record) {
        Ext.Ajax.request({
            url: link('account', 'acl-resource', true === value ? 'allow' : 'deny', {format: 'json'}),
            params: {
                name: name,
                roleId: this.roleId
            },
            success: function(response) {
                var decResponse = Ext.decode(response.responseText);
                Application.notificate(decResponse.messages);
                if (decResponse.success) {
                    record.commit();
                } else {
                    record.reject();
                }
            }
        });
    },

    setRoleId: function(id) {
        this.roleId = id;
        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.roleId = id;

        return this;
    }
});