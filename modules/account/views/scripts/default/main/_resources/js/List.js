Ext.define('Module.Account.List', {

    extend: 'Ext.ux.grid.GridPanel',
    alias: 'module.account.list',

    stateful: true,
    stateId: 'module.account.list',

    roleId: null,
    roleName: null,
    roleLuid:null,
    action: null,
    disableEdit: true,
    initComponent: function() {

        if (this.roleId == null) {
            this.action = 'fetch-all-owners'
        }

        this.store = new Ext.data.Store(
            {
            model: 'Module.Account.Model.Account',
            proxy: {
                type: 'ajax',
                url: link('account', 'main',this.action || 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset',
                    totalProperty: 'total'
                }
            },
            simpleSortMode: true
        });

        this.columns = [{
            header: lang('Name'),
            dataIndex: 'fullname',
            width: 200,
            flex: 1
        }, {
            header: lang('Login'),
            dataIndex: 'username',
            width: 120
        }, {
            header: lang('E-mail'),
            dataIndex: 'email',
            width: 150
        }, {
            header: lang('Mobile'),
            dataIndex: 'phone',
            width: 150,
            flex: 2
        }, {
            header: lang('Language'),
            dataIndex: 'language',
            width: 100
        }, {
            header: lang('Active'),
            dataIndex: 'isActive',
            renderer: function(value){
                if (value ==null) {
                    return '';
                } else {
                    if (value == 1 ) {
                        return 'Yes';
                    } else {
                        return 'No';
                    }
                }
            },
            width: 50
        }];

        var comItem = [{
            header: lang('Is company active'),
            dataIndex: 'isActiveCompany',
            scope: this,
            renderer: function(value){
                if (value ==null) {
                    return '';
                } else {
                    if (value == 1 ) {
                        return 'Yes';
                    } else {
                        return 'No';
                    }
                }
            },
            width: 50
        }];

        var resItems = [{
            xtype: 'actioncolumn',
            width: 60,
            header: lang('Actions'),
            fixed: true,
            items:  [{
                tooltip: lang('Edit'),
                iconCls: 'icon-edit-16 icon-16',
                handler: this.onEditAccount,
                scope: this
            }, {
                tooltip: lang('Change password'),
                iconCls: 'icon-16',
                icon: '/modules/account/views/scripts/default/main/_resources/images/change-password.png',
                handler: this.onChangePassword,
                scope: this
            }, {
                tooltip: lang(this.roleId == null ? 'Deactivate company' : 'Delete'),
                iconCls: 'icon-delete-16 icon-16',
                handler: this.onDeleteAccount,
                scope: this
            }]
        }];


        if (this.roleId == null) {
            this.columns = this.columns.concat(comItem);
        }
        this.columns = this.columns.concat(resItems);


        this.features = [{
            ftype: 'filters'
        }];

        this.plugins = [Ext.create('Ext.ux.grid.Search', {
            minChars: 2,
            align: 2,
            disableIndexes:['isActive', 'isActiveCompany', 'language'],
            stringFree: true
        })];

        this.createAccountBtn = new Ext.Button({
            text: lang('Create'),
            iconCls: 'icon-create-16',
            qtip: lang('Create new account'),
            handler: this.onCreateAccount,
//            disabled: (this.roleId > 0) ? false : true,
            scope: this
        });

        this.tbar = [this.createAccountBtn, '-'];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent(arguments);

        this.on({
            afteredit: this.onAfterEdit,
//            itemdblclick: function(g, record, html, rowIndex) {
//                this.onEditAccount(g, rowIndex);
//            },
            scope: this
        });
    },

    onCreateAccount: function() {
        Application.require([
            'account/account-form/form'
        ], function() {
            var f = new Module.Account.Form({
                roleId: this.roleId,
                roleName: this.roleName,
                roleLuid: this.roleLuid
            });
            f.on('completed', function(accountId) {
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditAccount: function(g, rowIndex) {
        var record = g.getStore().getAt(rowIndex);
        if (record.raw.isActiveCompany == 1 || record.raw.isActiveCompany == null ) {
            Application.require([
                'account/account-form/form'
            ], function() {
                var f = new Module.Account.Form({
                    accountId: record.get('id'),
                    roleLuid: this.roleLuid

                });
                f.on('completed', function(clientId) {
                    g.getStore().load();
                }, this);
                f.showInWindow();
            }, this);
        } else {
            Application.notificate( lang('Current company is inactive! Please activate company!'));
        }
    },

    onAfterEdit: function(data, g) {

        var params = {accountId: data.record.get('id')};
        var action = null;

        switch(data.field) {
            case 'isActive':
                action = link('account', 'main', data.record.get('isActive') ? 'activate' : 'deactivate', {format: 'json'});
                break;

            default:
                action = link('account', 'main', 'update-field', {format: 'json'});
                params.field = data.field;
                params.value = data.value;
                break;
        }

        Ext.Ajax.request({
            url: action,
            params: params,
            callback: function(options, success, response) {
                var decResponse = Ext.decode(response.responseText);
                if (decResponse && decResponse.success) {
                    data.record.commit();
                    Application.notificate(decResponse.messages || lang('Updated successfully'));
                    return;
                }

                data.record.reject();
                Application.notificate(decResponse.messages || lang('Unable to updated account'));
            },
            scope: this
        });
    },

    setRole: function(roleId, roleName,roleLuid) {

        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.roleId = roleId;
        p.extraParams.roleLuid = roleLuid;

        this.roleId = roleId;
        this.roleName = roleName;
        this.roleLuid = roleLuid;

        this.getStore().load();

        this.createAccountBtn.setDisabled((this.roleId > 0) ? false : true);

        return this;
    },

    getRoleId: function() {
        return this.roleId;
    },


    setDisabled : function(g, rowIndex) {

        console.log(rowIndex);
//        var record = g.getStore().getAt(rowIndex);
//        if (record.get('isActiveCompany') == 0) {
//            return false
//        } else {
//            return true;
//        }
//        return true;
    },

    onDeleteAccount: function(g, rowIndex) {

        var record = g.getStore().getAt(rowIndex);

        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
            if (b != 'yes') {
                return;
            }

            var url = 'delete';
            if (this.roleId == null) {
                url = 'deactivate-company';
            }

            Ext.Ajax.request({
                url: link('account', 'main', url, {format: 'json'}),
                params: {
                    id: record.get('id')
                },
                callback: function(options, success, response) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);
                    if (decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope: this
            });
        }, this);
    },

    onChangePassword: function(gw, rowIndex) {

        var record = gw.getStore().getAt(rowIndex);
        if (record.raw.isActiveCompany == 1 || record.raw.isActiveCompany == null ) {
            Application.require([
                'account/./change-password-form'
            ], function() {

                var f = new Module.Account.ChangePasswordForm({
                    accountId: record.get('id')
                });

                f.showInWindow();
            });
        }
        if (record.raw.isActiveCompany == 0) {
            Application.notificate( lang('Current company is inactive! Please activate company!'));
        }
    }
});