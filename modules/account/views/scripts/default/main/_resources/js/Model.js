Ext.define('Module.Account.Model.Account', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'parentId',
        'username', 'fullname', 'firstname', 'prefix', 'lastname',
        'title', 'sex',
        'email', 'department', 'function', 'photo',
        'credits',
        'phone', 'language',
        'companyId', 'companyName','isActiveCompany',
        {name: 'isActive', type: 'boolean'}
    ]
});