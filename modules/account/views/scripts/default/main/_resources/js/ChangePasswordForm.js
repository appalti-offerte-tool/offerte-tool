Ext.define('Module.Account.ChangePasswordForm', {
    extend: 'Ext.form.Panel',
    labelAlign: 'top',
    bodyPadding: 5,
    border: false,

    accountId: null,

    /**
     * @param {Ext.Window}
     */
    wnd: null,

    initComponent: function() {

        this.items = [{
            fieldLabel: lang('Enter new password'),
            xtype: 'textfield',
            name: 'password',
            anchor: '100%',
            labelWidth: 150,
            maskRe: /^\w$/,
            regex: /^\w{3,50}$/,
            regexText: lang('Value can contain only {0} characters.', 'a-z 0-9 _'),
            maxLength: 50,
            minLength: 3
        }];

        this.callParent();
    },

    onChangePassword: function() {

        this.getForm().submit({
            url: link('account', 'main', 'update', {format: 'json'}),
            params: {
                id: this.accountId
            },
            success: function(f, action) {
                Application.notificate(action);
                if (true == action.result.success) {
                    this.wnd && this.wnd.close();
                }
            },
            scope: this
        });
    },

    showInWindow: function() {

        var w = new Ext.Window({
            width: 350,
            title: lang('Change password'),
            items: [this],
            buttons: [{
                text: lang('Change'),
                handler: this.onChangePassword,
                scope: this
            }, {
                text: lang('Cancel'),
                handler: function() {
                    w.close();
                }
            }],
            scope: this
        });

        this.wnd = w;

        w.show();
        w.center();
        return w;
    }
});