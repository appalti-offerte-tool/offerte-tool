Ext.define('Module.Account.Form', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.module.account.form',
    layout: 'fit',

    form: null,
    accountId: null,

    roleId: null,
    roleName: null,
    roleLuid: null,
    wnd: null,

    initComponent: function() {

        var params = {};
        if (this.accountId) {
            params['accountId'] = this.accountId;
        }
        this.blocks = new Module.Application.BlockTabPanel({
            activeTab: 0,
            border: false,
            invokeArgs: {
                accountId: this.accountId,
                roleId: this.roleId,
                roleLuid: this.roleLuid
            }
        });

        this.items = [this.blocks];

        this.callParent(arguments);

        this.addEvents(
            /**
             * Fires when account is created
             *
             * @param {Module.Account.Form}
             */
            'completed'
        );

        this.blocks.on('tabchange', function(tb, b) {
            b && b.reload();
            b && b.doLayout();
        });

        params.keywords = 'form';
        params.module = 'account';
        this.on('render', function() {
            this.blocks.reload(params);
        }, this);
    },

    onSubmitAccount: function(panel, w) {

        var params = Ext.apply({}, this.invokeArgs || {});
        if (this.accountId) {
            params['accountId'] = this.accountId;
        }

        var isValid = true;
        this.blocks.items.each(function(block) {

            if (!block.isValid()) {
                isValid = false;
                return isValid = false;
            }

            Ext.applyIf(params, block.toValues());
        });

        if (this.roleId!=null){
            params.roleId = this.roleId;
        }
        if (true !== isValid) {
            return;
        }
        var action = this.accountId ? 'update' : 'create';
        this.el.mask(lang('Saving...'), 'x-mask-loading');
        Ext.Ajax.request({
            url: link('account', 'main', action, {format: 'json'}),
            method: 'POST',
            params: Ext.ux.OSDN.encode(params, true),
            success: function(response, options) {

                this.el.unmask();

                var responseObj = Ext.decode(response.responseText);
                Application.notificate(responseObj.messages);

                if (true === responseObj.success) {
                    this.fireEvent('completed', responseObj.accountId);
                    this.wnd && this.wnd.close();
                    return;
                }
            },
            scope: this
        });
    },

    showInWindow: function() {

        this.border = false;
        var w = this.wnd = new Ext.Window({
            title:  this.accountId ? lang('Update account') : lang('Create account'),
            iconCls: 'm-account-icon-16',
            resizable: false,
            layout: 'fit',
            width: 500,
            height: 600,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Save'),
                handler: this.onSubmitAccount,
                scope: this
            }, {
                text: lang('Close'),
                handler: function() {
                    w.close();
                    this.wnd = null;
                },
                scope: this
            }]
        });

        w.show();
        return w;
    },

    destroy: function() {

        Ext.destroy(this.blocks);

        this.callParent(arguments);
    }
});