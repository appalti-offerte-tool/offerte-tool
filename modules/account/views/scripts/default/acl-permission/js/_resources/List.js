Ext.define('Module.Account.AclPermission.List', {

    extend: 'Ext.panel.Panel',
    alias: 'module.account.acl-permission.list',

    stateful: true,
    stateId: 'module.account.acl-permission.list',

    cls: 'm-account-acl-permission-list',

    roleId: null,
    resourceId: null,

    autoScroll: true,

    initComponent: function() {

        this.bbar = ['->', {
            text: lang('Refresh'),
            iconCls: 'x-tbar-loading',
            handler: this.reload,
            scope: this
        }];

        this.callParent(arguments);
    },

    setRoleId: function(id) {
        this.roleId = id;
        return this;
    },

    setResource: function(resource) {
        this.resource = resource;
        return this;
    },

    reload: function() {

        if (!this.rendered) {
            this.on('render', this.reload, this);
            return;
        }

        this.body.mask(Ext.LoadMask.prototype.msg);
        this.body.getLoader().load({
            url: link('account', 'acl-permission', 'fetch-all-by-role-and-resource'),
            params: {
                roleId: this.roleId,
                resource: this.resource
            },
            callback: function() {
                this.body.unmask();
                this.onInitAclPermissionLayout();
            },
            scripts: true,
            scope: this
        });
    },

    onInitAclPermissionLayout: function() {

        Ext.select('.' + this.cls + ' .ext-form-checkboxable').on('click', function(e, checkbox) {

            var params = {
                roleId: this.roleId,
                resource: this.resource
            };

            var allow = checkbox.checked;
            params[checkbox.name] = + allow;
            Ext.Ajax.request({
                url: link('account', 'acl-permission', allow ? 'allow' : 'deny', {format: 'json'}),
                params: params,
                success: function(response) {
                    var decResponse = Ext.decode(response.responseText);

                    Application.notificate(decResponse.messages);
                    if (!decResponse.success) {
                        checkbox.checked = !allow;
                    }
                },
                scope: this
            });

        }, this);
    }
});