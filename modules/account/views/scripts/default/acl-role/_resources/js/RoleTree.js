/**
 * The first serious bug I've found on 4.0.7 version
 *
 * @link http://www.sencha.com/forum/showthread.php?151211-Reloading-TreeStore-adds-all-records-to-store-getRemovedRecords
 */

Ext.define('Module.Account.Acl.RoleTree',  {
    extend: 'Ext.tree.Panel',
    alias: 'module.account.acl.role-tree',

    title: lang('Roles'),

    rootVisible: true,

    iconCls: 'osdn-acl-role-icon-16x16',

    nodeIconCls: '',

    initComponent: function() {

        this.nodeIconCls = this.nodeIconCls || this.iconCls;

        this.store = Ext.create('Ext.data.TreeStore', {
            clearOnLoad: false,
            proxy: {
                type: 'ajax',
                url: link('account', 'acl-role', 'fetch-all', {format: 'json'})
            },
            root: {
                text: lang('All roles'),
                id: '0',
                expanded: true,
                iconCls: this.nodeIconCls
            },
            fields: [
                'id', 'text', 'leaf', 'expanded',
                'cls','luid'
            ]
        });

        this.tools = [{
            id: 'gear',
            qtip: lang('Flush cache'),
            handler: this.onFlushAuthCache,
            scope: this
        }, {
            id: 'plus',
            qtip: lang('Add'),
            handler: function() {
//                this.createProcess(this.getRootNode());
            },
            scope: this
        }, {
            id: 'refresh',
            qtip: lang('Refresh'),
            handler: function() {
                this.doLoad();
            },
            scope: this
        }];

        this.callParent(arguments);

//        this.on('nodedragover', this.onNodeDragOverRoles, this);
    },

    onContextMenu: function(node, e) {

//        this.fireEvent('itemclick', this,);
        e.stopEvent();

        var menu = new Ext.menu.Menu({items: []});
        if (node == this.getRootNode()) {
            menu.add({
                text: lang('Create'),
                iconCls: 'icon-create-16',
                handler: function() {
                    this.createProcess(node);
                },
                scope: this
            });
        } else {
            menu.add({
                text: lang('Quick edit'),
                iconCls: 'icon-edit-16',
                handler: function() {
                    this.treeEditor.triggerEdit(node);
                },
                scope: this
            });

            menu.add({
                text: lang('Delete'),
                iconCls: 'icon-delete-16',
                handler: function() {
                    Ext.Msg.confirm(lang('Confirmation'), lang('The role "{0}" will be deleted', node.text), function(b) {
                        b == 'yes' && this.beforeRemove(node);
                    }, this);
                },
                scope: this
            });
        }
        menu.showAt(e.getXY());
    },

    createProcess: function(node) {
        var text = this.generateNodeName(this.getRootNode());
        Ext.Ajax.request({
            url: link('account', 'acl-role', 'create', {format: 'json', name: text}),
            success: function(response) {
                var decResponse = Ext.decode(response.responseText);
                if (decResponse && decResponse.success && decResponse.id > 0) {
                    var newNode = new Ext.tree.TreeNode({
                        text: text,
                        id: decResponse.id,
                        iconCls: this.nodeIconCls
                    });
                    node.expand();
                    node.appendChild(newNode);
                    this.treeEditor.triggerEdit(newNode);
                    return;
                }

                Application.notificate(decResponse.messages || lang('Unable to create role'));
            },
            scope: this
        });
    },

    renameProcess: function(editor, node, value, startValue) {
        Ext.Ajax.request({
            url: link('account', 'acl-role', 'rename', {format: 'json'}),
            params: {
                node: node.id,
                text: value
            },
            success: function(response) {
                var decResponse = Ext.decode(response.responseText);

                if (!decResponse.success) {
                    this.revertRename(node, startValue);
                    Application.notificate(decResponse.messages || lang('Rename role failed.'));
                    return;
                }

                Application.notificate(decResponse.messages || lang('Updated successfully.'));
            },
            scope: this
        });
    },

    removeProcess: function(node, force) {
        Ext.Ajax.request({
            url: link('account', 'acl-role', 'delete', {format: 'json'}),
            params: {
                id: node.id,
                force: +!!force
            },
            success: function(response) {
                var decResponse = Ext.decode(response.responseText);
                if (decResponse.success) {
                    this.removeNode(node);
                    Application.notificate(decResponse.messages || lang('Deleted successfully.'));
                    return;
                }

                if (decResponse.force) {
                    Ext.Msg.confirm(
                        lang('Confirmation'),
                        decResponse.messages[0].message + '<br/>' + lang('Do you really want to continue?'),
                        function(btn) {
                            if ('yes' != btn) {
                                return;
                            }

                            this.removeProcess(node, true);
                        },
                        this
                    );

                    return;
                }

                Application.notificate(decResponse.messages || lang('Delete role failed.'));
            },
            scope: this
        });
    },

    onNodeDragOverRoles: function(e) {
        var nodeId = parseInt(e.target.id, 10);
        if (nodeId === 0) {
            e.cancel = true;
            return;
        }
    },

    onFlushAuthCache: function() {
        Ext.Ajax.request({
            url: link('account', 'acl-role', 'flush-cache', {format: 'json'}),
            success: function(response, options) {
                var decResponse = Ext.decode(response.responseText);
                Application.notificate(decResponse.messages);
            }
        });
    },

    doLoad: function(o) {
        if (true !== this.getStore().clearOnLoad) {
            ((o && o.node) || this.getRootNode()).removeAll();
        }

        this.getStore().load(o);
        return this;
    }
});