<?php

class Account_Migration_20120127_093810_45 extends Core_Migration_Abstract
{
    public function up()
    {
        /**
         * @FIXME
         */
        $this->getDbAdapter()->query('CREATE TABLE accountDdbmData (`id` int(11) UNSIGNED NOT NULL, PRIMARY KEY (`id`)) Engine=InnoDB');
    }

    public function down()
    {
        $this->dropTable('accountDdbmData');
    }
}