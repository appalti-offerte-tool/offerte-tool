<?php

class Account_Migration_20120425_100918_99 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('accountActivityAuthLog');
        $this->createColumn('accountActivityAuthLog', 'accountId', self::TYPE_INT, 11, null, true);
        $this->createIndex('accountActivityAuthLog', array('accountId'), 'IX_accountId');
        $this->createForeignKey('accountActivityAuthLog', array('accountId'), 'account', array('id'), 'FK_accountId');
        $this->createColumn('accountActivityAuthLog', 'createdDatetime', self::TYPE_DATETIME);


        $this->createColumn('account', 'parentId', self::TYPE_INT, 11);
        $this->createIndex('account', array('parentId'), 'IX_parentId');
        $this->createForeignKey('account', array('parentId'), 'account', array('id'), 'FK_parentId');

        $this->createColumn('account', 'firstname', self::TYPE_VARCHAR, 30);
        $this->createColumn('account', 'prefix', self::TYPE_VARCHAR, 20);
        $this->createColumn('account', 'lastname', self::TYPE_VARCHAR, 40);
        $this->createColumn('account', 'title', self::TYPE_ENUM, array('Dhr.', 'Mevr.'), null, false);
        $this->createColumn('account', 'sex', self::TYPE_ENUM, array('man', 'vrouw'), null, false);
        $this->createColumn('account', 'department', self::TYPE_VARCHAR, 50);
        $this->createColumn('account', 'function', self::TYPE_VARCHAR, 50);
        $this->createColumn('account', 'photo', self::TYPE_VARCHAR, 255);
        $this->createColumn('account', 'credits', self::TYPE_FLOAT, 9);
    }

    public function down()
    {
        $this->dropTable('accountActivityAuthLog');

        $this->dropForeignKey('account', 'FK_parentId');
        $this->dropIndex('account', 'IX_parentId');
        $this->dropColumn('account', 'parentId');

        $this->dropColumn('account', 'firstname');
        $this->dropColumn('account', 'prefix');
        $this->dropColumn('account', 'lastname');
        $this->dropColumn('account', 'title');
        $this->dropColumn('account', 'sex');
        $this->dropColumn('account', 'department');
        $this->dropColumn('account', 'function');
        $this->dropColumn('account', 'photo');
        $this->dropColumn('account', 'credits');
    }
}