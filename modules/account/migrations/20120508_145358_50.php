<?php

class Account_Migration_20120508_145358_50 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('accountActivityLog');
        $this->createColumn('accountActivityLog', 'accountId', self::TYPE_INT, 11, null, true);
        $this->createIndex('accountActivityLog', array('accountId'), 'IX_accountId');
        $this->createForeignKey('accountActivityLog', array('accountId'), 'account', array('id'), 'FK_accountId');
        $this->createColumn('accountActivityLog', 'createdDatetime', self::TYPE_DATETIME, null, null, true);
        $this->createColumn('accountActivityLog', 'description', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('accountActivityLog', 'params', self::TYPE_TEXT, null, null, false);
    }

    public function down()
    {
        $this->createTable('accountActivityLog');
    }
}