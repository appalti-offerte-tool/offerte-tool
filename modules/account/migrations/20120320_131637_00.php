<?php

class Account_Migration_20120320_131637_00 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createColumn('aclRole', 'luid', self::TYPE_VARCHAR, 100, null, false);
        $this->createUniqueIndexes('aclRole', array('luid'), 'UX_luid');
    }

    public function down()
    {
        $this->dropUniqueIndexes('aclRole', 'UX_luid');
        $this->dropColumn('aclRole', 'luid');
    }
}

