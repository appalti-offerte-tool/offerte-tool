<?php

class Account_Migration_20120228_133217_48 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createUniqueIndexes('account', array('username'), 'UX_username');
    }

    public function down()
    {
        $this->dropUniqueIndexes('account', 'UX_username');
    }
}