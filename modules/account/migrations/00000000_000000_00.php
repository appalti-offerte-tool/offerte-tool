<?php

class Account_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    const ROLE_ADMIN = 1;
    const ROLE_GUEST = 2;

    const ACCOUNT_ADMIN = 1;
    const ACCOUNT_GUEST = 2;

    public function up()
    {
        $this->createTable('aclRole');
        $this->createColumn('aclRole', 'name', Core_Migration_Abstract::TYPE_VARCHAR, 50, null, true);
        $this->createColumn('aclRole', 'parentId', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createUniqueIndexes('aclRole', array('name'), 'name');
        $this->createIndex('aclRole', array('parentId'), 'parentId');
        $this->createForeignKey('aclRole', array('parentId'), 'aclRole', array('id'), 'FK_roleId');
        $this->insert('aclRole', array('id' => self::ROLE_ADMIN, 'name' => 'admin', 'parentId' => null));
        $this->insert('aclRole', array('id' => self::ROLE_GUEST, 'name' => 'guest', 'parentId' => null));

        $this->createTable('aclPermission');
        $this->createColumn('aclPermission', 'roleId', Core_Migration_Abstract::TYPE_INT, 11, null, true);
        $this->createColumn('aclPermission', 'resource', Core_Migration_Abstract::TYPE_VARCHAR, 50, null, true);
        $this->createColumn('aclPermission', 'privilege', Core_Migration_Abstract::TYPE_VARCHAR, 50, null, false);
        $this->createIndex('aclPermission', array('roleId'), 'roleId');
        $this->createForeignKey('aclPermission', array('roleId'), 'aclRole', array('id'), 'FK_roleId');

        foreach(array(
            array('roleId' => self::ROLE_ADMIN, 'resource' => 'account', 'privilege' => null),
            array('roleId' => self::ROLE_ADMIN, 'resource' => 'account:account', 'privilege' => null),
            array('roleId' => self::ROLE_ADMIN, 'resource' => 'account:profile', 'privilege' => null),
            array('roleId' => self::ROLE_ADMIN, 'resource' => 'account:acl', 'privilege' => null),
            array('roleId' => self::ROLE_ADMIN, 'resource' => 'account:acl', 'privilege' => 'role'),
            array('roleId' => self::ROLE_ADMIN, 'resource' => 'account:acl', 'privilege' => 'resource'),
            array('roleId' => self::ROLE_ADMIN, 'resource' => 'account:acl', 'privilege' => 'permission'),

            /**
             * Default resources of module "Module"
             * There are a litle circle dependences :)
             */
            array('roleId' => self::ROLE_ADMIN, 'resource' => 'module:module', 'privilege' => null),
            array('roleId' => self::ROLE_ADMIN, 'resource' => 'module', 'privilege' => null),
            array('roleId' => self::ROLE_ADMIN, 'resource' => 'module:configuration', 'privilege' => null)
        ) as $o) {
            $this->insert('aclPermission', $o);
        }

        $this->createTable('account');
        $this->createColumn('account', 'username', Core_Migration_Abstract::TYPE_VARCHAR, 50, null, true);
        $this->createColumn('account', 'password', Core_Migration_Abstract::TYPE_VARCHAR, 50, null, false);
        $this->createColumn('account', 'hash', Core_Migration_Abstract::TYPE_VARCHAR, 32, null, false);
        $this->createColumn('account', 'email', Core_Migration_Abstract::TYPE_VARCHAR, 50, null, false);
        $this->createColumn('account', 'phone', Core_Migration_Abstract::TYPE_VARCHAR, 50, null, false);
        $this->createColumn('account', 'language', Core_Migration_Abstract::TYPE_VARCHAR, 2, null, true);
        $this->createColumn('account', 'roleId', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createColumn('account', 'fullname', Core_Migration_Abstract::TYPE_VARCHAR, 100, null, false);
        $this->createColumn('account', 'luid', Core_Migration_Abstract::TYPE_VARCHAR, 50, null, false);
        $this->createColumn('account', 'luidRelationId', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createColumn('account', 'isActive', Core_Migration_Abstract::TYPE_INT, 1, null, true);
        $this->createIndex('account', 'roleId');
        $this->createForeignKey('account', array('roleId'), 'aclRole', array('id'), 'FK_roleId');

        $accountService = new \Account_Service_Account();
        foreach(array(
            array(
                'id'            => self::ACCOUNT_ADMIN,
                'username'      => 'admin',
                'password'      => 'test',
                'hash'          => null,
                'email'         => 'yaroslav@osdn.cv.ua',
                'phone'         => null,
                'language'      => 'nl',
                'roleId'        => self::ROLE_ADMIN,
                'fullname'      => 'Administrator',
                'luid'          => null,
                'luidRelationId'=> null,
                'isActive'      => 1
            ), array(
                'id'            => self::ACCOUNT_GUEST,
                'username'      => 'guest',
                'password'      => 'test1',
                'hash'          => null,
                'email'         => 'yaroslav@osdn.cv.ua',
                'phone'         => null,
                'language'      => 'nl',
                'roleId'        => self::ROLE_GUEST,
                'fullname'      => 'Guest',
                'luid'          => null,
                'luidRelationId'=> null,
                'isActive'      => 1
            )
        ) as $o) {
            $accountService->create($o);
        }
    }

    public function down()
    {
        $this->dropTable('account');
        $this->dropTable('aclPermission');
        $this->dropTable('aclRole');
    }
}
