<?php

final class Account_Misc_Email_CreateAccount extends Configuration_Service_ConfigurationAbstract
    implements Notification_Instance_NotifiableInterface
{
    protected $_identity = 'account.create-account';

    protected $_fields = array(
        'subject' => array(
            'default'    => array(
                'en' => 'Registration confirmed',
                'nl' => 'Registratie bevestiging'
            )
        ),
        'body'      => array(
            'default'    => array(
                'en' => 'Dear {username}, <br/>Thanks for your registration.',
                'nl' => 'Geachte {username}, <br/>Bedankt voor uw registratie.'
            )
        )
    );


    protected $_mui = true;

    protected $_keywords = array(
        'username'  => 'Username',
        'fullname'  => 'Full name',
        'password'  => 'Password',
        'email'     => 'E-mail address'
    );

    protected $_serialized = array('receipt');

    public function __construct()
    {
        parent::__construct();

        $defaultFrom = Zend_Mail::getDefaultFrom();
        if (is_array($defaultFrom)) {
            $this->_fields['receipt']['default'] = $this->_toValue($defaultFrom, 'receipt');
        }
    }

    /**
     * (non-PHPdoc)
     * @see Notification_Instance_NotifiableInterface::notificate()
     */
    public function notificate(array $params)
    {
        $validate = new OSDN_Validate_Id();
        if (empty($params['id']) || !$validate->isValid($params['id'])) {
            throw new OSDN_Exception('Input credentials are invalid');
        }

        $account = new Account_Model_DbTable_Account(
            OSDN_Application_Service_Dbable::getDefaultDbAdapter()
        );

        $accountRow = $account->findOne($params['id']);

        if (null === $accountRow) {
            throw new OSDN_Exception('Unable to find account');
        }

        $params = array(
            'username'    => $accountRow->getUsername(),
            'fullname'    => $accountRow->getFullname(),
            'password'    => $params['password'],
            'email'       => $accountRow->email
        );

        $instance = $this->toInstance();
        $mail = new Zend_Mail('UTF-8');

        $receipt = $instance->getReceipt();
        $mail->setFrom($receipt['email'], $receipt['name']);

        $mail->addTo($accountRow->email, $accountRow->getFullname());
        $mail->setSubject($instance->getSubject($params));
        $mail->setBodyHtml($instance->getBody($params));

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $bootstrap->bootstrap('mail');
        $transport = $bootstrap->getResource('mail');

        $mail->send($transport);

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_toValue()
     */
    protected function _toValue($value, $field)
    {
        if (in_array($field, $this->_serialized)) {
            return serialize($value);
        }

        return parent::_toValue($value, $field);
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_fromValue()
     */
    protected function _fromValue($value, $field)
    {
        if (in_array($field, $this->_serialized)) {
            return unserialize($value);
        }

        return parent::_fromValue($value, $field);
    }
}