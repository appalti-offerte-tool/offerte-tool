<?php

final class Account_Misc_Email_LostPassword extends Configuration_Service_ConfigurationAbstract
    implements Notification_Instance_NotifiableInterface
{
    protected $_identity = 'account.lost-password';

    protected $_fields = array(
        'subject' => array(
            'default'    => array(
                'en' => 'Lost password notification',
                'nl' => 'Wachtwoord vergeten kennisgeving'
            )
        ),
        'body'      => array(
            'default'    => array(
                'en' => 'Lost password notification.',
                'nl' => 'Wachtwoord vergeten kennisgeving.'
            )
        )
    );

    protected $_mui = true;

    protected $_keywords = array(
        'username'  => 'Username',
        'fullname'  => 'Full name',
        'lastname'  => 'Last name',
        'salutation'  => 'Salutation',
        'ip'        => 'IP address',
        'email'     => 'E-mail address',
        'link'      => 'Link'
    );

    protected $_serialized = array('receipt');

    public function __construct()
    {
        parent::__construct();

        $defaultFrom = Zend_Mail::getDefaultFrom();
        if (is_array($defaultFrom)) {
            $this->_fields['receipt']['default'] = $this->_toValue($defaultFrom, 'receipt');
        }
    }

    /**
     * (non-PHPdoc)
     * @see Notification_Instance_NotifiableInterface::notificate()
     */
    public function notificate(array $params)
    {
        if (empty($params['username'])) {
            throw new OSDN_Exception('Gebruikersnaam ongeldig.');
        }

        $account = new Account_Model_DbTable_Account(
            OSDN_Application_Service_Dbable::getDefaultDbAdapter()
        );
        $accountRow = $account->fetchRow(array(
            'username = ?'    => $params['username'],
        ));

        if (null === $accountRow) {
            throw new OSDN_Exception('Niet in staat om rekening te houden te vinden met opgegeven gebruikersnaam.');
        }

        $hash = md5(uniqid() . $params['username']);

        $accountRow->hash = $hash;
        $accountRow->save();

        $request = Zend_Controller_Front::getInstance()->getRequest();

        $link = sprintf(
            '%s://%s%s%s',
            $request->getScheme(),
            $request->getHttpHost(),
            $request->getBaseUrl(),
            '/account/password/authenticate/hash/' . $hash
        );

        $params = array(
            'username'    => $accountRow->getUsername(),
            'fullname'    => $accountRow->getFullname(),
            'lastname'    => $accountRow->lastname,
            'salutation'  => $accountRow->getSalutation(),
            'link'        => sprintf('<a href="%s">%s</a>', $link, $link),
            'ip'          => $_SERVER['REMOTE_ADDR'],
            'email'       => $accountRow->email
        );

        $instance = $this->toInstance();

        $mail = new Zend_Mail('UTF-8');
        $receipt = $instance->getReceipt();
        $mail->setFrom($receipt['email'], $receipt['name']);

        $mail->addTo($accountRow->email, $accountRow->getFullname());
        $mail->setSubject($instance->getSubject($params));
        $mail->setBodyHtml($instance->getBody($params));

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $bootstrap->bootstrap('mail');
        $transport = $bootstrap->getResource('mail');

        $mail->send($transport);

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_toValue()
     */
    protected function _toValue($value, $field)
    {
        if (in_array($field, $this->_serialized)) {
            return serialize($value);
        }

        return parent::_toValue($value, $field);
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_fromValue()
     */
    protected function _fromValue($value, $field)
    {
        if (in_array($field, $this->_serialized)) {
            return unserialize($value);
        }

        return parent::_fromValue($value, $field);
    }

}