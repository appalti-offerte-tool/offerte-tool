<?php

class Account_Block_AccountList extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Accounts');
    }
}