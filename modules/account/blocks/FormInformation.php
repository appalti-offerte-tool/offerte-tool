<?php

final class Account_Block_FormInformation extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Information');
    }

    protected function _toHtml()
    {
        $accountId = $this->_getParam('accountId', false);

        if (!empty($accountId)) {
            $this->view->accountId = $accountId;

            $service = new \Account_Service_Account();
            $this->view->accountRow = $service->find($accountId);
        }
    }
}