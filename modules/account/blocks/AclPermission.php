<?php

class Account_Block_AclPermission extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Permissions');
    }
}