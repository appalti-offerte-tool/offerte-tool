<?php

class Account_ProfileController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    public function init()
    {
        parent::init();

        $this->_helper->layout->setLayout('administration');
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'account:profile';
    }

    public function indexAction()
    {

    }
}