<?php

class Account_CreateController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**@var Account_Service_Account $_accounts */
    protected $_accounts;

    /**@var Account_Model_DbTable_AccountRow $this->accountRow */
    protected $accountRow;

    public function init()
    {
        parent::init();
        $this->_accounts    = new Account_Service_Account();
        $this->accountRow   = $this->_accounts->createRow();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'guest';
    }

    public function noLayoutAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_forward('index');
    }

    public function indexAction()
    {
        $this->_forward('create');
    }

    /**
     * create account form
     */
    public function createAction()
    {
        $accountRow = $this->_accounts->createRow();
        if ($this->getRequest()->isPost())
        {
            try {
                // create account
                $accountRow = $this->_accounts->createRow($this->_getAllParams());
                $this->accountRow = $accountRow;
                $params = array_diff_key($this->_getAllParams(),array('module'=>'','controller'=>'','action'=>'', 'return'=>''));
                $accountRow->signupData = json_encode($params);

                $accountRow->username   = $accountRow->email;
                if ($this->_accounts->checkAccountExists($accountRow->username))
                {
                    throw new Exception('Emailadres bestaat al.');
                }
                $gender = array(
                    'man'   => 'Dhr.',
                    'vrouw' => 'Mevr.',
                );
                $accountRow->password   = $password = $this->_accounts->generatePassword(12);
                $accountRow->hash       = md5(rand(0, 9999999999999999));
                $accountRow->language   = 'nl';
                $accountRow->roleId     = 6; // classic role 'Beheerder'
                $accountRow->isActive   = true;
                $accountRow->title      = $gender[ $accountRow->sex ];
                $accountRow->admin      = 0;
                $accountData = $accountRow->toArray();
                $accountData['confirmPassword'] = $accountRow->password;

                if (($account = $this->_accounts->create($accountData)) != false)
                {
                    // updated account
                    $account->admin             = 1;
                    $account->isActive          = false;
                    $account->createdAccountId  = $account->getId();
                    $account->save();

                    // send email to prospect
                    // - create emailbody
                    $view = new Zend_View();
                    $view->assign('accountRow', $account);
                    $view->addScriptPath(dirname(__FILE__).'/../views/scripts/appalti/');
                    $body = $view->render('create/email/created.phtml');

                    // - send email
                    $mail = new Zend_Mail();
                    $mail->setSubject('Offertemaken.com account aanvraag');
                    $mail->addTo($account->email, $account->fullname);
                    $mail->setBodyHtml($body);
                    $mail->setBodyText(html_entity_decode(strip_tags($body)));
                    $mail->send();

                    // send email to backoffice
                    // - create emailbody
                    $view = new Zend_View();
                    $view->assign('accountRow', $account);
                    $view->addScriptPath(dirname(__FILE__).'/../views/scripts/appalti/');
                    $body = $view->render('create/email/created2admin.phtml');

                    // - send email
                    $mail = new Zend_Mail();
                    $mail->setSubject('Offertemaken.com account aanvraag');
                    $mail->addTo('nhofs@appalti.nl', 'Admin Offertetool');
                    $mail->setBodyHtml($body);
                    $mail->send();

                    /** @todo save contact in salesforce */
                    // save contact in salesforce

                    /** @todo start mailplus campagne */
                    // start mailplus campagne

                    // informeer aanvrager
                    $this->_helper->information('Uw account is aangemaakt.', true, E_USER_NOTICE);
                    $this->_forward('created','create','account', array('accountRow' => $accountRow));
                }
            } catch(Exception $e) {
                $this->_helper->information($e->getMessage());
                $this->_forward('failed');
            }
        }

        // pass models
        $this->view->accountRow = $this->accountRow;
        $this->view->package = $this->_getParam('package', '');
    }

    /**
     * created account info (email was sent)
     */
    public function createdAction()
    {
    }

    /**
     * create account failed (username already exists?)
     */
    public function failedAction()
    {
    }

    /**
     * created account confirm from email
     */
    public function confirmAction()
    {
        $account_code = $this->_getParam('account');
        $accountRow = $this->_accounts->getByAccountCode($account_code);
        if (!$accountRow)
        {
            // on failure -> /account/create/confirm-failed
            $this->_redirect('/account/create/confirm-failed');
        }

        // activate account
        $accountRow->isActive = 1;
        $accountRow->save();

        // prepare view
        $this->view->accountRow = $accountRow;
    }

    /**
     * created account confirm from email failed
     */
    public function confirmFailedAction()
    {

    }

    /**
     * created account not wanted
     */
    public function reportAction()
    {

    }

    public function updateAction()
    {
        try {
            // only post allowed
            if (!$this->getRequest()->isPost())
            {
                throw new Exception();
            }

            // spam bot detection
            if ($this->_getParam('email'))
            {
                throw new Exception();
            }

            // get accountRow
            $accountRow = $this->_accounts->find($this->_getParam('id',0));
            if (!$accountRow)
            {
                throw new Exception('Uw account kon niet gevonden worden.');
            }

            // validate password
            $data = array(
                'password'          => $this->_getParam('password'),
                'confirmPassword'   => $this->_getParam('confirmPassword'),
            );
            if (!($data['password'] === $data['confirmPassword']))
            {
                throw new Exception('U dient tweemaal hetzelfde wachtwoord in te geven.');
            }
            // save account
            if ($this->_accounts->updatePassword($accountRow->getId(), $data['password']))
            {
                $service = new Account_Service_Authenticate();
                $service->logout();
                $service->authenticate($accountRow->username, $data['password']);
            }
        } catch(Exception $e) {
            $this->_helper->information($e->getMessage());
        }
        $this->_redirect('/ap-default');
    }

    public function testAction()
    {
        die();
        $accountRow = $this->_accounts->createRow($this->_getAllParams());
        if ($this->getRequest()->isPost())
        {
            if (($id = (int)$this->_getParam('id', 0)) != false)
            {
                $accountRow = $this->_accounts->find($id);
            }
        }
        $this->view->accountRow = $accountRow;
   }

   public function emailAction()
   {
        die();
//       $accountRow = $this->accountRow;
//       $accountRow = Zend_Auth::getInstance()->getIdentity();
       $_accounts = new Account_Model_DbTable_Account();
       $accountRow = $_accounts->fetchRow('id = 337');
        // send email to prospect
        // - create emailbody
        $view = new Zend_View();
        $view->assign('accountRow', $accountRow);
        $view->addScriptPath(dirname(__FILE__).'/../views/scripts/appalti/');
        $body = $view->render('create/email/created2admin.phtml');
        die($body);
   }

}
