<?php

class Account_AclResourceController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var Account_Service_AclPermission
     */
    protected $_service;

    public function init()
    {
        parent::init();

        $this->_helper->ajaxContext()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('allow', 'json')
            ->addActionContext('deny', 'json')
            ->initContext();

        $bootstrap = $this->getInvokeArg('bootstrap');
        $im = $bootstrap->getResource('ModulesManager');

        $this->_service = new Account_Service_AclPermission($im);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return $this->getRequest()->getModuleName() . ':acl';
    }

    public function fetchAllAction()
    {
        $rowset = $this->_service->fetchAllResourcesByRole($this->_getParam('roleId'));
        $this->view->rowset = $rowset;
        $this->view->total = count($rowset);
    }

    public function allowAction()
    {
        try {
            $permissionId = $this->_service->allow($this->_getParam('roleId'), $this->_getParam('name'));
            $this->view->success = (boolean) $permissionId;
            $this->view->id = (int) $permissionId;
            $this->_helper->information('Allowed successfully.', array(), E_USER_NOTICE);
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function denyAction()
    {
        try {
            $this->view->success = $this->_service->deny($this->_getParam('roleId'), $this->_getParam('name'));
            $this->_helper->information('Deny successfully.', array(), E_USER_NOTICE);
            $this->view->success = true;
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}