<?php

class Account_PasswordController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        if (in_array($this->getRequest()->getActionName(), array('authenticate', 'lost-password'))) {
            return 'guest';
        }

        return 'account:profile';
    }

    public function indexAction()
    {}

    public function lostPasswordAction()
    {
        $this->_helper->layout->disableLayout();
        if (!$this->getRequest()->isPost()) {
            if ($this->view->refresh = $this->_getParam('refresh')) {
                $this->_helper->information('Password Message Sent', array(), E_USER_NOTICE);
            }
            return;
        }

        $lostPassword = new Account_Misc_Email_LostPassword();
        try {
            $this->view->success = (boolean) $lostPassword->notificate($this->_getAllParams());
            $this->_helper->information('Er is u een  e-mail gestuurd.<br/>Volg de instructies.', array(), E_USER_NOTICE);
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch(\Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessage());
        }
    }

    public function authenticateAction()
    {
        $authenticate = new Account_Service_Authenticate();
        try {
            $auth = $authenticate->authenticateByHash($this->_getParam('hash'));
            $this->_helper->redirector->gotoSimpleAndExit('update-password', 'password', 'account');
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessage());
        } catch(\Exception $e) {
            $this->_helper->information('Niet in staat om in te loggen');
        }

        $this->_helper->redirector->gotoSimpleAndExit('lost-password', 'password', 'account');
    }


    public function updatePasswordAction()
    {
        if (!$this->getRequest()->isPost()) {
            if ($this->view->refresh = $this->_getParam('refresh')) {
                $this->_helper->information('Wachtwoord is met succes bijgewerkt.', array(), E_USER_NOTICE);
            }
            return;
        }

        $service = new Account_Service_Account();

        try {
            $account = Zend_Auth::getInstance()->getIdentity();
            $affectedRows = $service->updatePassword($account->getId(), $this->_getParam('password'));
            $this->view->success = true;
            $this->view->refresh = 5;
            $this->_helper->information(array('Wachtwoord is met succes bijgewerkt.<br/>U wordt doorgestuurd naar de pagina in te loggen in 5 seconden.'), true, E_USER_NOTICE);
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch (\Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessage());
        }
    }

    public function checkAction()
    {
        $password = $this->_getParam('oldPassword');
        if (empty($password)) {
            die('false');
        }

        try {
            $authenticate = new Account_Service_Authenticate();
            $account = Zend_Auth::getInstance()->getIdentity();
            $result = $authenticate->checkCredentials($account->username, $password);
            die($result->isValid() ? 'true' : 'false');
        } catch (\Exception $e) {
            die('false');
        }
    }
}