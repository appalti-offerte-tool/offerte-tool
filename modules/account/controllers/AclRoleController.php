<?php

class Account_AclRoleController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var Account_Service_AclRole
     */
    protected $_service;

    public function init()
    {
        parent::init();

        $this->_helper->ajaxContext()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('rename', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('flush-cache', 'json')
            ->initContext();

        $this->_service = new Account_Service_AclRole();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return $this->getRequest()->getModuleName() . ':acl';
    }

    public function indexAction()
    {}

    public function comboBoxAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');
        $response = $this->_service->fetchAll();
        $this->view->rowset = $response->toArray();
    }

    public function fetchAllAction()
    {
        $node = $this->_getParam('node', null);
        if ($node == 'root') {
            $node = 0;
        }

        $output = array();
        foreach($this->_service->fetchAllByParentId($node, true) as $roleRow)
        {
            $node = array(
                'id'        => $roleRow->id,
                'text'      => $roleRow->name,
				'luid'      => $roleRow->luid,
                'expanded'  => false,
//                'children'  => array(),
//                'leaf'      => true
            );

            if ($roleRow->isProtected()) {
                $node['cls'] = 'osdn-acl-role-protected';
            }

            $output[] = $node;
        }

        $this->view->assign($output);
    }

    public function createAction()
    {
        try {
             $this->view->id = $this->_service->create($this->_getParam('name'));
             $this->view->success = true;
             $this->_helper->information('Completed successfully.', true, E_USER_NOTICE);
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }

    public function renameAction()
    {
        try {
            $this->view->success = $this->_service->rename($this->_getParam('node'), $this->_getParam('text'));
             $this->_helper->information('Completed successfully.', true, E_USER_NOTICE);
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }

    public function deleteAction()
    {
        $isForce = 1 == $this->_getParam('force', false);
        try {
            $this->_service->delete($this->_getParam('id'), $isForce);
            $this->_helper->information('Completed successfully.', true, E_USER_NOTICE);
            $this->view->success = true;
        } catch (Account_Service_AclRoleException $e) {
            if ($e->getCode() == Account_Service_AclRoleException::HAS_CONNECTED_ACCOUNTS_IN_ROLE) {
                $this->view->force = true;
            }

            $this->_helper->information($e->getMessages());
            $this->view->success = false;

        } catch (OSDN_Exception $e) {

            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }

//    public function updateRoleAction()
//    {
//        $roles = new OSDN_Acl_Roles();
//        $response = $roles->update($this->_getParam('id'), $this->_getAllParams());
//        $success = $response->isSuccess();
//        $this->view->rowset = array();
//
//        $this->view->success = $success;
//        if (!$success) {
//            $this->_collectErrors($response);
//        }
//    }
    public function flushCacheAction()
    {
        if (Account_Service_Authenticate::hasCache()) {
            $cache = Account_Service_Authenticate::getCache();
            $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(Account_Service_Authenticate::CACHE_TAG));
        }

        $this->view->success = true;
        $this->_helper->information('Cache has been successfully flushed', array(), E_USER_NOTICE);
    }
}