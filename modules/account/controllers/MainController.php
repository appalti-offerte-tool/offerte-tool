<?php

class Account_MainController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{

    /**
     * @var  Account_Service_Account
     */

    protected $_service;


    protected function _fetchAllByRoleId($roleId)
    {
        $i18nLanguage = new I18n_Service_LanguageConfiguration();
        $languageRowset = $i18nLanguage->fetchAllValuePairs();

        $response = $this->_service->fetchAllByRoleWithResponse($roleId, $this->_getAllParams());
        $action =  $this->_getParam('action');
        $response->setRowsetCallback(function($row) use ($languageRowset,$action) {

            $lcode = $row['language'];
            $array = array(
                'id'        => $row['id'],
                'parentId'  => $row['parentId'],
                'username'  => $row['username'],
                'fullname'  => $row['fullname'],
                'email'     => $row['email'],
                'phone'     => $row['phone'],
                'language'  => isset($languageRowset[$lcode]) ? $languageRowset[$lcode] : '',
                'isActive'  => $row['isActive'],
            );

            if ($action == 'fetch-all-owners') {
                $array['isActiveCompany'] = $row['isActiveCompany'];
            }
            return $array;
        });

        return $response;
    }

    public function init()
    {
        parent::init();

        $this->_helper->ajaxContext()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch-all-owners', 'json')
            ->addActionContext('fetch-all-administrators', 'json')
            ->addActionContext('fetch', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('update-field', 'json')
            ->addActionContext('activate', 'json')
            ->addActionContext('deactivate', 'json')
            ->addActionContext('deactivate-company', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('check-username-account-free', 'json')
            ->initContext();

        $this->_service = new Account_Service_Account();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'account';
    }

    public function indexAction()
    {
        // list
    }

    public function attachFormAction()
    {
        $this->_helper->layout->disableLayout();

        if ($this->_getParam('id')) {
            $account = $this->_service->find($this->_getParam('id'));

            if (empty($account)) {
                throw new OSDN_Exception('Unable to find account');
            }

            $this->view->account = $account;
        }
    }

    public function fetchAllComboAction()
    {

        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');
        $service = new Account_Service_AclRole();
        $response = $service->fetchAll();
        $this->view->rowset = $response->toArray();
    }

    public function fetchAction()
    {
        $account = $this->_service->find($this->_getParam('id'));
        if (empty($account)) {
            throw new OSDN_Exception('Unable to find account');
        }

        $this->view->row = $account->toArray();
        $this->view->success = true;
    }



    public function createAction()
    {
        try {
            $params = $this->_getAllParams();


            $accountRow = $this->_service->create($params);
            $params['id'] = $accountRow->getId();
            $this->view->accountId = $accountRow->getId();
            $this->_helper->information(
                array('Account has been successfully created.'),
                true, E_USER_NOTICE
            );

            $createNotify = new Account_Misc_Email_CreateAccount();
            $this->view->success = (boolean) $createNotify->notificate($params);
            $this->_helper->information('Email was sent successfully', array(), E_USER_NOTICE);

        } catch (OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch (\Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessage());
        }
    }

    public function updateAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $params = $this->_getAllParams();
        if (!isset($params['parentId'])) {
            $params['parentId'] = null;
        }
        try {
            $account = $this->_service->find($this->_getParam('accountId'));
            if (null === $account) {
                throw new OSDN_Exception('Unable to find account');
            }

            $affectedRows = $this->_service->update($account->id, $params);

            $this->view->success = true;
            $this->_helper->information(
                'Account has been successfully updated.',
                true,
                E_USER_NOTICE
            );


            $this->view->success = true;

        } catch (OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $this->_service->delete($this->_getParam('id'));

            $this->view->success = true;
            $this->_helper->information('Account has been successfully updated.', true, E_USER_NOTICE);

        } catch (\OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }


    public function deactivateCompanyAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {

            $account = $this->_service->find($this->_getParam('id'));
            $companyRow = $account->getCompanyRow(false);

            if ($companyRow != null)
            {
                $companyService = new Company_Service_Company();
                $data = $companyRow->toArray();
                $data['isActive'] == 0 ? $data['isActive'] = 1 : $data['isActive'] =0;
                $companyService->update($companyRow, $data);
                $this->view->success = true;
                $this->_helper->information('Account has been successfully updated.', true, E_USER_NOTICE);
            } else {
                $this->_service->delete($this->_getParam('id'));
                $this->view->success = true;
                $this->_helper->information('Account has been successfully updated.', true, E_USER_NOTICE);
            }
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateFieldAction()
    {
        if (!$this->getRequest()->isPost()
            || !$this->_getParam('accountId')
            || !$this->_getParam('field')
            || !$this->_getParam('value')) {
            return;
        }

        try {
            $account = $this->_service->find($this->_getParam('accountId'));
            if (null === $account) {
                throw new OSDN_Exception('Unable to find account');
            }

            $params = array(
                'id'    =>  $this->_getParam('accountId'),
                $this->_getParam('field') => $this->_getParam('value')
            );

            $affectedRows = $this->_service->update($account->id, $params);

            $this->view->success = true;
            $this->_helper->information(
                array('Account has been successfully updated.'),
                true,
                E_USER_NOTICE
            );

            $this->view->success = true;

        } catch (OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAllAction()
    {
        $response = $this->_fetchAllByRoleId($this->_getParam('roleId', null));
        $this->view->assign($response->toArray());
    }


    public function fetchAllOwnersAction()
    {
        $aclService = new Account_Service_AclRole();
        $aclRow = $aclService->fetchByLuid('companyOwner', true);

        $response = $this->_fetchAllByRoleId($aclRow->getId());
        $this->view->assign($response->toArray());
    }

    public function fetchAllAdministratorsAction()
    {
        $aclService = new Account_Service_AclRole();
        $aclRow = $aclService->fetchByLuid('admin', true);

        $response = $this->_fetchAllByRoleId($aclRow->getId());
        $this->view->assign($response->toArray());
    }


    public function accountMetadataAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');

        $md = new Ddbm_Instance_Metadata('account');
        $this->view->metadata = $md->getMetadata();
    }

    public function activateAction()
    {
        $this->view->success = $this->_service->activate($this->_getParam('accountId'));
    }

    public function deactivateAction()
    {
        $this->view->success = $this->_service->deactivate($this->_getParam('accountId'));
    }

    public function checkUsernameAccountFreeAction()
    {
        if ($this->_service->checkAccountExists($this->_getParam('username'))) {
            die('false');
        } else {
            die('true');
        }

    }

}