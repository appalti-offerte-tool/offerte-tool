<?php

class Account_AuthenticateController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'guest';
    }

    public function indexAction()
    {
        if ($this->_getParam('page') === 'admin') {
            if (Zend_Auth::getInstance()->getIdentity()->isAdmin()) {
                $this->_redirect('/company/main');
                die();
            }
        }

        $this->getResponse()->setHttpResponseCode(405);
        $this->getResponse()->setRawHeader($_SERVER['SERVER_PROTOCOL'] . ' 405 Permission denied');

        $service = new Account_Service_Authenticate();
        $service->logout();

        $this->view->return = $this->_getParam('return');
        $this->_helper->layout->disableLayout(true);
    }

    public function idealAction()
    {

        $this->getResponse()->setHttpResponseCode(405);
        $this->getResponse()->setRawHeader($_SERVER['SERVER_PROTOCOL'] . ' 405 Permission denied');

        $service = new Account_Service_Authenticate();
        $service->logout();
    }

    /**
     * User authentification
     *
     * if user go to this action
     * then he wants to login on another entity
     * then we destroy current session
     * and create new if authentification will be success
     */
    public function loginAction()
    {
        $service = new Account_Service_Authenticate();
        $service->logout();

        $ideal = $this->_getParam('ideal');

        if (!empty($ideal)) {
            $ideal = 'ideal';
        }

        try {
            $service->authenticate($this->_getParam('username'), $this->_getParam('password'));
            $this->_doRedirect($ideal);
        } catch (OSDN_Exception $e) {
            $this->_helper->information($e->getMessages(), true, E_USER_ERROR);
            $this->_redirect('/account/authenticate/' . $ideal);
        }
    }

    /**
     * Destroy account session and redirect to login page
     *
     */
    public function logoutAction()
    {
        $service = new Account_Service_Authenticate();
        $service->logout();

        $this->_redirect('/ap-default');
    }

    protected function _doRedirect($mainpage = '')
    {
        $bo = $this->getInvokeArg('bootstrap')->getOption('bootstrap');
        if (empty($mainpage)) {
            $mainpage = !empty($bo['mainpage']) ? $bo['mainpage'] : '/';
        }

        $this->_redirect($this->_getParam('return') ?: $mainpage);
    }
}