<?php

class Account_AclPermissionController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var Account_Service_AclPermission
     */
    protected $_service;

    public function init()
    {
        $this->_helper->ajaxContext()
            ->addActionContext('allow', 'json')
            ->addActionContext('deny', 'json')
            ->initContext();

        parent::init();

        $bootstrap = $this->getInvokeArg('bootstrap');
        $im = $bootstrap->getResource('ModulesManager');
        $this->_service = new Account_Service_AclPermission($im);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return $this->getRequest()->getModuleName() . ':acl';
    }

    public function fetchAllByRoleAndResourceAction()
    {
        $this->_helper->layout->disableLayout(true);

        $this->view->rowset = $this->_service->fetchAllByRoleAndResource(
            $this->_getParam('roleId'),
            $this->_getParam('resource')
        );
    }

    public function allowAction()
    {
        try {
            $this->view->success = $this->_service->allow(
                $this->_getParam('roleId'),
                $this->_getParam('resource'),
                $this->_getParam('permission')
            );

            $this->_helper->information('Completed successfully.', array(), E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function denyAction()
    {
        try {
            $this->view->success = $this->_service->deny(
                $this->_getParam('roleId'),
                $this->_getParam('resource'),
                $this->_getParam('permission')
            );

            $this->_helper->information('Completed successfully.', array(), E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }
}