<?php

/**
 * Helper for check the permission on controller action
 *
 */
class Account_Controller_Helper_Acl extends Zend_Controller_Action_Helper_Abstract
{
    protected $_role;

    public function preDispatch()
    {
        $ac = $this->getActionController();
        if ( ! $ac instanceof Zend_Acl_Resource_Interface)
        {
            $bootstrap = $this->getActionController()->getInvokeArg('bootstrap');
            if (null != $bootstrap->hasResource('log')) {
                $log = $bootstrap->getResource('log');
                $message = sprintf('The controller "%s" must implement Zend_Acl_Resource_Interface', get_class($ac));
                $log->log($message, Zend_Log::WARN);
            }
            return;
        }

        $resource = $ac->getResourceId();
        $isAllowed = (0 === strcasecmp('guest', $resource));
        if (!$isAllowed)
        {
            $accountRow = Zend_Auth::getInstance()->getIdentity();
            $permissions = explode(':', $resource);
            switch(count($permissions))
            {
                case 1:
                    $isAllowed = $accountRow->acl($permissions[0]);
                    break;
                case 2:
                    $isAllowed = $accountRow->acl($permissions[0], $permissions[1]);
                    break;
                case 3:
                    $isAllowed = $accountRow->acl($permissions[0], $permissions[1], $permissions[2]);
                    break;
            }
        }
        if (!$isAllowed) {
            $this->deny('You do not have permissions');
        }
    }

    public function getRole()
    {
        if (null === $this->_role)
        {
            $account = Zend_Auth::getInstance()->getIdentity();
            $this->_role = new Zend_Acl_Role($account->getRoleId());
        }
        return $this->_role;
    }

    public function isAllowedPrivilege($privilege, $resource = null)
    {
        if (null === $resource)
        {
            $resource = $this->getActionController()->getResourceId();
        }
        /**
         * @var Account_Model_AccountInterface
         */
        $account = Zend_Auth::getInstance()->getIdentity();
        $acl = $account->getAcl();

        if ( ! $acl->has($resource)) {
            return $this->deny(sprintf('The resource "%s" was not found in the registry', $resource));
        }
        return $acl->isAllowed($this->getRole(), $resource, $privilege ?: 'default');
    }

    public function isAllowed($resources)
    {
        if ( ! is_array($resources))
        {
            $resources = array($resources);
        }
        /**
         * @var Account_Model_AccountInterface
         */
        $account = Zend_Auth::getInstance()->getIdentity();
        $acl = $account->getAcl();

        $isAllowed = false;
        foreach($resources as $resource)
        {
            $privilege = null;
            if (3 === count($aResource = explode(':', $resource))) {
                $resource = $aResource[0] . ':' . $aResource[1];
                $privilege = $aResource[2];
            }

            if ( ! $acl->has($resource)) {
                return $this->deny(sprintf('The resource "%s" was not found in the registry', $resource));
            }

            if ($acl->isAllowed($this->getRole(), $resource, $privilege ?: 'default')) {
                $isAllowed = true;
                break;
            }
        }
        return $isAllowed;
    }

    public function assert($resources)
    {
        if (!$this->isAllowed($resources))
        {
            $this->deny();
        }
        return true;
    }

    public function direct($resources, $autoRedirectToDenyAction = true)
    {
        $isAllowed = false;
        foreach((array) $resources as $resource)
        {
            if ($this->isAllowed($resource))
            {
                $isAllowed = true;
                break;
            }
        }

        if (true !== $isAllowed && false !== $autoRedirectToDenyAction)
        {
            $this->deny();
        }
        return $isAllowed;
    }

    public function deny($message = null)
    {
        $params = array();
        if (null !== $message)
        {
            $params['message'] = $message;
        }
        $params['return'] = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        $this->getResponse()->setRedirect(sprintf('/default/error/denied/message/%s?return=%s', $params['message'], $params['return']), 302);
    }
}