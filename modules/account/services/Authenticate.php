<?php

class Account_Service_Authenticate extends OSDN_Application_Service_Dbable
{
    const CACHE_TAG = 'ACCOUNT_ACL_PERMISSION';

    /**
     * Internal cache
     *
     * @var Zend_Cache_Core
     * @access private
     */
    private static $_cache = null;

    /**
     * Internal value to remember if cache supports tags
     *
     * @var boolean
     */
    private static $_cacheTags = false;

    /**
     * Internal option, cache disabled
     *
     * @var     boolean
     * @access  private
     */
    private static $_cacheDisabled = false;

    /**
     * Authenticate account by username and password
     *
     * @param string $username
     * @param string $password
     *
     * @return Zend_Auth
     */
    public function authenticate($username, $password)
    {
        $this->_attachValidationRules('default', array(
            'username'    => array('presence' => 'required', 'allowEmpty' => false),
            'password'    => array('presence' => 'required', 'allowEmpty' => false)
        ));

        $this->_attachFilteringRules('default', array(
            'username'      => 'StringTrim',
            'password'      => 'StringTrim'
        ));

        $this->_validate(array(
            'username'    => $username,
            'password'    => $password
        ));

        $auth = Zend_Auth::getInstance();

        $table = new Account_Model_DbTable_Account($this->getAdapter());
        $authAdapter = new Zend_Auth_Adapter_DbTable($this->getAdapter());
        $authAdapter->setTableName($table->getTableNameWithSchema());
        $authAdapter->setIdentityColumn('username');
        $authAdapter->setCredentialColumn('password');
        $authAdapter->setIdentity($username);
        $authAdapter->setCredential(md5($password . Account_Model_DbTable_Account::SALT));

        try {
            $result = $authAdapter->authenticate();
        } catch (Zend_Auth_Adapter_Exception $e) {
            throw new OSDN_Exception($e->getMessage());
        }

        if (!$result->isValid()) {
            $e = new OSDN_Exception();
            $e->assign($result->getMessages());
            throw $e;
        }

        $row = $authAdapter->getResultRowObject('id');
        $accountRow = $table->findOne($row->id);

        if (empty($accountRow->isActive)) {
            $this->logout();
            throw new OSDN_Exception('Account with supplied login and password is disabled.');
        }

        if (!$accountRow->isAdmin() && !$accountRow->isTextWriter()) {
            $companyRow = $accountRow->getCompanyRow(false);

            if (empty($companyRow) && !$accountRow->isCompanyOwner()) {
                throw new OSDN_Exception('Unable to find company details');
            }

            if (!empty($companyRow)) {
                $activationExpired = false;

                if (null !== $companyRow->activeTillDate) {
                    $dt = new DateTime();
                    $today = $dt->format('Y-m-d');
                    $activationExpired = \strtotime($companyRow->activeTillDate) < \strtotime($today);
                }

                if (!$companyRow->isActive || $activationExpired) {
                    throw new OSDN_Exception('Company is deactivated by Appalti administration. Please contact.');
                }
            }

            if (!$accountRow->isCompanyOwner() && !$companyRow->isMembershipValid()) {
                throw new OSDN_Exception('Membership expired. Please contact you company owner.');
            }
        }

        if (!empty($accountRow->authhash)) {

            /**
             * @FIXME Unfortunately cannot be done via native Zend_Session object
             * so, we are using navive php functions
             */
            session_write_close();

            $sessionId = session_id();
            session_id($accountRow->authhash);
            session_start();

//            if (Zend_Auth::getInstance()->hasIdentity()) {
//                session_write_close();
//                session_id($sessionId);
//                session_start();
//
//                throw new OSDN_Exception('Dit account is momenteel elders ingelogd, of is open on een andere browser of tab.<br /> Meerdere logins zijn niet mogelijk.');
//            }

            $this->logout();

            session_regenerate_id(true);
        }

        $accountRow->authhash = session_id();
        $accountRow->save();

        $auth->getStorage()->write($accountRow);
        return $auth;
    }

    /**
     * Check if supplied credentials are valid
     *
     * @param $username
     * @param $password
     * @return Zend_Auth_Result
     * @throws OSDN_Exception
     */
    public function checkCredentials($username, $password)
    {
        $this->_attachValidationRules('default', array(
            'username' => array('presence' => 'required', 'allowEmpty' => false),
            'password' => array('presence' => 'required', 'allowEmpty' => false)
        ));

        $this->_attachFilteringRules('default', array(
            'username' => 'StringTrim',
            'password' => 'StringTrim'
        ));

        $this->_validate(array(
            'username' => $username,
            'password' => $password
        ));

        $table = new Account_Model_DbTable_Account($this->getAdapter());
        $authAdapter = new Zend_Auth_Adapter_DbTable($this->getAdapter());
        $authAdapter->setTableName($table->getTableNameWithSchema());
        $authAdapter->setIdentityColumn('username');
        $authAdapter->setCredentialColumn('password');

        $authAdapter->setIdentity($username);
        $authAdapter->setCredential(md5($password . Account_Model_DbTable_Account::SALT));

        try {
            return $authAdapter->authenticate();
        } catch (Zend_Auth_Adapter_Exception $e) {
            throw new OSDN_Exception($e->getMessage());
        }
    }

    /**
     * Authenticate account by hash
     *
     * @param string $hash
     *
     * @return Zend_Auth
     */
    public function authenticateByHash($hash)
    {
        $this->logout();

        $auth = Zend_Auth::getInstance();

        $validate = new Zend_Validate_StringLength(array('min' => 32, 'max' => 32));
        if (!$validate->isValid($hash)) {
            throw new OSDN_Exception(sprintf('The hash "%s" is not valid', $hash));
        }

        $table = new Account_Model_DbTable_Account($this->getAdapter());

        /**
         * @todo Add hit blocking by ip address or login
         */
        $accountRow = $table->fetchRow(array(
            'hash = ?'    => $hash
        ));

        if (null === $accountRow) {
            throw new OSDN_Exception('Gebruikersnaam is onjuist');
        }

        $accountRow->hash = null;
        $accountRow->save();

        $auth->getStorage()->write($accountRow);

        return $auth;
    }

    public function assertGuestAccount()
    {
        $account = Zend_Auth::getInstance()->getIdentity();
        if (empty($account) || !$account->isGuest()) {
            $account = Account_Model_DbTable_AccountRow::initWithGuest();
            $this->initAclObject($account);
        }
    }

    public function initAclObject(Account_Model_AccountInterface $account)
    {
        $acl = null;
        $cacheId = self::CACHE_TAG . $account->getRoleId();

//        if (self::hasCache()) {
//            $acl = self::getCache()->load($cacheId);
//        }

        if ( ! $acl instanceof Zend_Acl)
        {
            $acl = new Zend_Acl();
            $adminRole = null;
            if ($account->admin)
            {
                try {
                    $companyRow = $account->getCompanyRow();
                    $roleSrv = new Account_Service_AclRole();
                    $adminRole = $roleSrv->findCompanyRoleById($companyRow->getId());
                    $acl->addRole($adminRole);
                    $acl->deny($adminRole);
                } catch(Exception $e) {

                }
            }

            $role = new Zend_Acl_Role($account->getRoleId());
            $acl->addRole($role, $adminRole);
            $acl->deny($role);

            $tablePermission = new Account_Model_DbTable_AclPermission($this->getAdapter());
            foreach($acl->getRoles() as $roleId)
            {
                $permissionRowset = $tablePermission->fetchAll(array(
                    'roleId = ?'    => $roleId,
                ), array('resource ASC', 'privilege ASC'));

                foreach($permissionRowset as $permissionRow) {
                    $resource = $permissionRow->resource;
                    if (!$acl->has($resource)) {
                        if (false === stripos($resource, ':')) {
                            $parentResource = new Zend_Acl_Resource($resource);
                            $acl->addResource($parentResource);
                            $acl->deny($role, $parentResource);
                            $acl->allow($role, $parentResource, 'default');
                            continue;
                        }

                        list($parentResource, ) = explode(':', $resource);
                        if (!$acl->has($parentResource)) {
                            $acl->addResource($parentResource);
                        }

                        $acl->addResource($resource);
                        $acl->deny($role, $resource);
                        $acl->allow($role, $resource, 'default');
                    }

                    if (null !== $permissionRow->privilege) {
                        $acl->allow($role, $resource, $permissionRow->privilege);
                    }
                }
            }
            if (self::hasCache()) {
                self::getCache()->save($acl, $cacheId, self::getCacheRoleTags($account->getRoleId(), true), 86400);
            }
        }
        $account->setAcl($acl);
        if (!Zend_Controller_Action_HelperBroker::hasHelper('acl')) {
            Zend_Controller_Action_HelperBroker::addHelper(new Account_Controller_Helper_Acl());
        }

        Module_Instance_Information::setOnAclCheckPermissionCallbackFn(function($resource, $privilege) use ($acl, $account) {
            if ( ! $acl->has($resource)) {
                return false;
            }
            return $acl->isAllowed($account->getRoleId(), $resource, $privilege ?: 'default');
        });

        return $acl;
    }

    /**
     * @todo
     *
     *  PHP bug: http://bugs.php.net/bug.php?id=29419&edit=1
     *  current PHP version:
     *      PHP Version 5.2.6
     *      May 2 2008 18:01:20
     */
    public function logout()
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            try {
                $accountRow = Zend_Auth::getInstance()->getIdentity();
                $accountRow->setFromArray(array(
                    'authhash' => new \Zend_Db_Expr('NULL')
                ));
                // destroy namespaced session
                $uniqueSessionName = 'Proposal_Service_Wizard_' . $accountRow->getId();
                Zend_Session::namespaceUnset($uniqueSessionName);

                $accountRow->save();
            } catch (\Exception $e) {
                throw new \OSDN_Exception('Authenticate hash has not been cleared.');
            }

            Zend_Auth::getInstance()->clearIdentity();
        }
    }

    /**
     * Returns the set cache
     *
     * @return Zend_Cache_Core The set cache
     */
    public static function getCache()
    {
        return self::$_cache;
    }

    /**
     * Returns true when a cache is set
     *
     * @return boolean
     */
    public static function hasCache()
    {
        return null !== self::$_cache;
    }

    /**
     * Removes any set cache
     *
     * @return void
     */
    public static function removeCache()
    {
        self::$_cache = null;
    }

    public static function getCacheRoleTags($roleId, $withCommonTag = false)
    {
        if (self::_getTagSupportForCache()) {
            $tags = array(self::CACHE_TAG . $roleId);
            if (false !== $withCommonTag) {
                $tags[] = self::CACHE_TAG;
            }

            return $tags;
        }

        return array(self::CACHE_TAG);
    }

    /**
     * Disables the cache
     *
     * @param boolean $flag
     */
    public static function disableCache($flag)
    {
        self::$_cacheDisabled = (boolean) $flag;
    }

    /**
     * Set a cache
     *
     * @param Zend_Cache_Core $cache A cache frontend
     */
    public static function setCache(Zend_Cache_Core $cache)
    {
        self::$_cache = $cache;
        self::_getTagSupportForCache();
    }

    /**
     * Internal method to check if the given cache supports tags
     *
     * @param Zend_Cache $cache
     */
    private static function _getTagSupportForCache()
    {
        $backend = self::$_cache->getBackend();
        if ($backend instanceof Zend_Cache_Backend_ExtendedInterface) {
            $cacheOptions = $backend->getCapabilities();
            self::$_cacheTags = $cacheOptions['tags'];
        } else {
            self::$_cacheTags = false;
        }

        return self::$_cacheTags;
    }
}