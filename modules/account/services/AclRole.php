<?php

class Account_Service_AclRole extends OSDN_Application_Service_Dbable
{
    /**
     * The role table instance
     *
     * @var Account_Model_DbTable_AclRole
     */
    protected $_table;

    public function _init()
    {
        $this->_attachValidationRules('insert', array(
            'name'    => array(array('StringLength', 1, 50), 'allowEmpty' => false, 'presence' => 'required')
        ));

        $this->_attachValidationRules('update', array(
            'id'      => array('id', 'presence' => 'required'),
            'name'    => array(array('StringLength', 1, 50), 'allowEmpty' => false, 'presence' => 'required')
        ));

        $this->_table = new Account_Model_DbTable_AclRole($this->getAdapter());

        parent::_init();
    }

    public function fetchAll()
    {
        return $this->_table->fetchAll();
    }

    /**
     * retrieve (child)roles
     *
     * @param int $parentId id of parent role (optional; if not given retrieves root roles)
     * @param bool $activeComapniesOnly flag for retrieving only active companies if true
     * @return Zend_DbTable_Rowset of Account_Model_AclRole
     */
    public function fetchAllByParentId($parentId = null, $activeCompaniesOnly = false)
    {
        if (empty($parentId))
        {
            $select = $this->_table
                            ->select()
                            ->where('parentId IS NULL')
                            ->order('id ASC')
            ;
        }
        else
        {
            $select = $this->_table
                            ->select()
                            ->from(array('ar'=>$this->_table->getTableName()))
                            ->where('ar.parentId = ?', $parentId)
                            ->order('ar.name ASC')
            ;
            if ($activeCompaniesOnly)
            {
                $select->setIntegrityCheck(false)
                        ->joinLeft(array('c'=>'company'), 'c.id = ar.companyId', array())
                        ->where('c.isActive = 1')
                ;
            }
        }
        $roleRowset = $this->_table->fetchAll($select);
        return $roleRowset;
    }

    /**
     * Retrieve the role by id
     *
     * @return Account_Model_DbTable_AclRoleRow
     */
    public function find($id)
    {
        return $this->_table->findOne($id);
    }

    /**
     * Retrieve AclRole row by liud
     * @param $luid
     * @param bool $throwException
     *
     * @return null|Zend_Db_Table_Row_Abstract
     * @throws OSDN_Exception
     */
    public function fetchByLuid($luid, $throwException = true)
    {
        $row = $this->_table->fetchRow(array('luid = ?' => $luid));
        if (null === $row && true === $throwException) {
            throw new OSDN_Exception('Unable to find row luid: ' . $luid);
        }

        return $row;
    }

    public function fetchByLuids(array $luids)
    {
        return $this->_table->fetchAll(array('luid IN (?)' => $luids));
    }

    public function create($name)
    {
        $this->_validateField('name', $name, 'insert', true);

        $aclRoleRow = $this->_table->createRow();
        $aclRoleRow->name = $name;
        return $aclRoleRow->save();
    }

    public function createUserRoleRow(array $data = array())
    {
        // copy from default gebruiker
        $fromRole = $this->_table->fetchRow('id = 5');
        return $this->_table->copyRole($fromRole, $data, true);
    }

    //
    public function findCompanyRoleById($companyId)
    {
        $select = $this->_table->select()
                                ->where('companyId = ?', $companyId)
                                ->order('id ASC')
        ;
        return $this->_table->fetchRow($select);
    }

    public function findChildRolesByCompanyId($companyId)
    {
        $companyLocationsSelect = $this->_table->select()
                                                ->distinct()
                                                ->from('aclRole', 'id')
                                                ->where('companyId = ?', $companyId);
        $select = $this->_table->select()
                                ->where('parentId IN ?', $companyLocationsSelect)
                                ->order('name ASC')
        ;
        return $this->_table->fetchAll($select);
    }

    /**
     * Rename role
     *
     * @param int $id       The role id
     * @param string $name  New role name
     */
    public function rename($id, $name)
    {
        $this->_validateField('name', $name, 'update', true);

        $aclRoleRow = $this->_table->findOne($id);
        if (null == $aclRoleRow) {
            throw new OSDN_Exception('Unable to find role #' . $id);
        }

        $this->_isProtectedRole($aclRoleRow);

        $aclRoleRow->name = $name;
        $aclRoleRow->save();
        return true;
    }

    public function delete($id, $force = false)
    {
        $adapter = $this->_table->getAdapter();
        try {
            $adapter->beginTransaction();

            $aclRoleRow = $this->_table->findOne($id);
            if (null == $aclRoleRow) {
                throw new OSDN_Exception('Unable to find role #' . $id);
            }
            $this->_isProtectedRole($aclRoleRow);
            if (true !== $force) {
                $account = new Account_Service_Account();
                if (false !== $account->hasConnectedAccountsInRole($aclRoleRow)) {
                    throw Account_Service_AclRoleException::hasConnectedAccountsInRole($aclRoleRow->name);
                }
            }
            $aclRoleRow->delete();

            $adapter->commit();
            return true;
        } catch (Exception $e) {
            $adapter->rollBack();
            throw $e;
        }
    }

    /**
     * The roles with id low then 10 is protected
     *
     * @param int $roleId
     * @param boolean $throwException
     */
    protected function _isProtectedRole(Account_Model_DbTable_AclRoleRow $roleRow, $throwException = true)
    {
        if ($roleRow->isProtected()) {

            if (true === $throwException) {
                throw new OSDN_Exception(sprintf('The role %d is protected and not allowed for manipulations', $rowRow->name));
            }

            return true;
        }

        return false;
    }

    public function saveRole(array $data)
    {
        $row = null;
        if ((int)$data['id'])
        {
            $row = $this->_table->updateByPk($data, $data['id']);
        }
        else
        {
            $row = $this->_table->insert($data);
        }
        return $row;
    }
}