<?php

/**
 * General class for manipulate accounts
 *
 * @category        OSDN
 * @package     OSDN_Accounts
 * @version     $Id: Abstract.php 1262 2011-06-02 11:21:18Z yaroslav $
 */
abstract class OSDN_Accounts_Abstract
{
    /**
     * Return model table object
     *
     * @return OSDN_Accounts_Table_Abstract
     */
    abstract protected function _model();

    /**
     * Create new account
     *
     * @param array $data
     */
    abstract public function create(array $data);

    abstract public function isExists($login);

    abstract public function update($id, array $data);

    public function unlock($id)
    {
        $response = new OSDN_Response();
        $validate = new OSDN_Validate_Id();

        if (!$validate->isValid($id)) {
            $response->addStatus(new OSDN_Response_Status_Storage(
                OSDN_Response_Status_Storage::INPUT_PARAMS_INCORRECT, 'id'));
            return $response;
        }

        $rows = $this->_model()->updateByPk(array(
            'locked' => 0
        ), $id);

        if (false === $rows || 1 !== $rows) {
            $status = OSDN_Response_Status_Storage::DATABASE_ERROR;
        } else {
            $status = OSDN_Response_Status_Storage::OK;
        }

        $response->addStatus(new OSDN_Response_Status_Storage($status));
        return $response;
    }


    public function delete($id)
    {
        $response = new OSDN_Response();
        $validate = new OSDN_Validate_Id();

        if (!$validate->isValid($id)) {
            $response->addStatus(new OSDN_Response_Status_Storage(
                OSDN_Response_Status_Storage::INPUT_PARAMS_INCORRECT, 'id'));
            return $response;
        }

        if (true !== $this->_onBeforeDelete($id, $response)) {
            return $response;
        }

        try {
            $affectedRows = $this->_model()->deleteByPk($id);
            $status = OSDN_Response_Status_Storage::retrieveAffectedRowStatus($affectedRows);
        } catch (Exception $e) {

            $rows = $this->_model()->updateByPk(array(
                'locked' => 1
            ), $id);

            if (false === $rows || 1 !== $rows) {
                $status = OSDN_Response_Status_Storage::DATABASE_ERROR;
                if (OSDN_DEBUG) {
                    throw $e;
                }
            } else {
                $status = OSDN_Response_Status_Storage::OK;
            }
        }

        $response->addStatus(new OSDN_Response_Status_Storage($status));
        return $response;
    }



    protected function _onBeforeDelete($id, OSDN_Response $response)
    {
        return true;
    }

    /**
     * Fetch account information by id
     *
     * @param int $accountId    The account id
     * @return OSDN_Response
     * <code> array(
     *     'rowset' => array|null
     * )
     * </code>
     */
    public function fetchAccount($accountId)
    {
        $response = new OSDN_Response();
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($accountId)) {
            $response->addStatus(new OSDN_Accounts_Status(OSDN_Accounts_Status::INPUT_PARAMS_INCORRECT, 'account_id'));
            return $response;
        }

        $row = $this->_model()->findOne($accountId);
        $response->addStatus(new OSDN_Accounts_Status(OSDN_Accounts_Status::OK));
        $response->setRow(!is_null($row) ? $row->toArray() : array());
        return $response;
    }

    /**
     * @see _fetchAll()
     *
     * @return OSDN_Response
     */
    public function fetchAll(array $params = array(), array $filterIds = array())
    {
        if (!empty($filterIds)) {
            $filterWhere = array('id IN(?)' => $filterIds);
        } else {
            $filterWhere = array();
        }
        return $this->_fetchAll($filterWhere, $params);
    }

    public function fetchAllByIds(array $ids)
    {
        return $this->_fetchAll(array('id IN(?)' => $ids), array());
    }

    /**
     * Fetches all rows
     *
     * @param array $params
     * The param examples<code>
     *      sort    => 'name'
     *      dir     => 'ASC'
     *      limit   => 20
     *      start   => 1
     *      ...
     *      filter[0][data][type]   string
     *      filter[0][data][value]  1
     *      filter[0][field]        alias
     * </code>
     * @param array $where      The array of where clauses<code>
     *  array(
     *      array('name = ?' => test),
     *      array('company_id = ?' => 1)
     *  );</code>
     *
     * @return OSDN_Response
     * Details of contain data <code>
     *      rows array          The modules collection
     *      total int           The total count of rows
     * </code>
     */
    protected function _fetchAll($where, array $params = array())
    {
        $response = new OSDN_Response();
        
        $select = $this->_model()->getAdapter()->select();
        $select->from(array('a' => $this->_model()->getTableName()));

        $fields = array(
            'login',
            'email',
            'fullname',
            'id' => 'value'
        );
        
        if (!empty($where)) {
            foreach ($where as $key => $value) {
                $select->where($key, $value);
            }
        }

        $plugin = new OSDN_Db_Plugin_Select($this->_model(), $select, $fields);
        $plugin->setSqlCalcFoundRows(true);
        $plugin->setStrategy(OSDN_Db_Plugin_Select::STRATEGY_COMBO);
        $plugin->parse($params);
        
        $status = null;
        try {
            $query = $select->query();
            $rowset = array();
            while(false != ($row = $query->fetch())) {
                $rowset[] = $this->_model()->filterRow($row);
            }

            $response->setRowset($rowset);
            $response->total = $plugin->getTotalCountSql();
            $status = OSDN_Acl_Status::OK;
        } catch (Exception $e) {
            if (OSDN_DEBUG) {
                throw $e;
            }

            $status = OSDN_Acl_Status::DATABASE_ERROR;
        }

        $response->addStatus(new OSDN_Acl_Status($status));
        return $response;
    }

    public function updatePersonalInformation($id, array $data)
    {
        $informationColumns = $this->_model()->getPersonalInformationColumns();
        $data = array_intersect_key($data, $informationColumns);
        $response = new OSDN_Response();
        $validate = $this->_validateField('id');
        if (!$validate->isValid($id)) {
            $response->addStatus(new OSDN_Response_Status_Storage(
                OSDN_Response_Status_Storage::INPUT_PARAMS_INCORRECT, 'id'));
            return $response;
        }

        $f = new OSDN_Filter_Input(array(), $this->_validate($data, $informationColumns), $data);

        $response->addInputStatus($f);
        if ($response->hasNotSuccess()) {
            return $response;
        }
        
        $affectedRows = $this->_model()->updateByPk($f->getEscaped(), $id);

        $response->addStatus(new OSDN_Response_Status_Storage(
            OSDN_Response_Status_Storage::retrieveAffectedRowStatus($affectedRows)));
        return $response;
    }

    /**
     * Update account field by name
     *
     * @param int $id           The account id
     * @param string $field     Available field in database
     * @param mixed $value      Field value
     * @return OSDN_Response
     */
    public function updateByField($id, $field, $value)
    {
        $response = new OSDN_Response();
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($id)) {
            $response->addStatus(new OSDN_Response_Status_Storage(
                OSDN_Response_Status_Storage::INPUT_PARAMS_INCORRECT, 'id'));
            return $response;
        }

        $fieldValidate = null;
        $field = strtolower($field);

        if (false === ($fieldValidate = $this->_validateField($field))) {
            $response->addStatus(new OSDN_Response_Status_Storage(
                OSDN_Response_Status_Storage::INPUT_PARAMS_INCORRECT, $field));
            return $response;
        }

        $model = $this->_model();

        if (!$this->_model()->hasColumn($field)) {
            $response->addStatus(new OSDN_Response_Status_Storage(
                OSDN_Response_Status_Storage::INPUT_PARAMS_INCORRECT, $field));
            return $response;
        }

        if ($fieldValidate instanceof Zend_Validate_Interface) {
            if (!$fieldValidate->isValid($value)) {
                $response->addStatus(new OSDN_Response_Status_Storage(
                    OSDN_Response_Status_Storage::INPUT_PARAMS_INCORRECT, $field));
                return $response;
            }
        }
        $affectedRows = false;

        // try to set method catcher before common method
        $method = '_set' . ucfirst($field) . 'Account';
        if (method_exists($this, $method) && is_callable(array($this, $method))) {
            $affectedRows = call_user_func_array(array($this, $method), array($id));
        } elseif (!$this->_model()->isAllowedField($field)) {
            $response->addStatus(new OSDN_Response_Status_Storage(
                OSDN_Response_Status_Storage::INPUT_PARAMS_INCORRECT, $field));
            return $response;
        } else {
            $affectedRows = $this->_model()->updateByPk(array(
                $field  => $value
            ), $id);
        }

        $response->addStatus(new OSDN_Response_Status_Storage(
            OSDN_Response_Status_Storage::retrieveAffectedRowStatus($affectedRows)));
        return $response;
    }

    protected function _toFilterInputValidate($field)
    {
        $validate = $this->_validateField($field);

        if ($validate instanceof Zend_Validate_Interface) {
            return $validate;
        }

        return array();
    }

//    protected function _fieldValidateDefinition($field)
//    {
//        $validate = array();
//        switch($field) {
//            case 'id':
//                $validate[] = new OSDN_Validate_Id();
//                break;
//
//            case 'password':
//                $fieldValidate = new Zend_Validate_Regex('~\w{3,50}~');
//                $fieldValidate->setMessage("'%value%' does not a valid login name", Zend_Validate_Regex::NOT_MATCH);
//                $validate[] = $fieldValidate;
//
//                break;
//
//            case 'login':
//                $fieldValidate = new Zend_Validate_Regex('~\w{3,50}~');
//                $fieldValidate->setMessage("'%value%' does not a valid login name", Zend_Validate_Regex::NOT_MATCH);
//                $validate[] = $fieldValidate;
//
//                break;
//
//            case 'email':
//                $fieldValidate = new Zend_Validate_EmailAddress();
//                $validate[] = $fieldValidate;
//                $validate['presence'] = 'required';
//                break;
//        }
//
//        $v = array(
//            'id', 'presence' => 'required'
//        );
//    }

    /**
     *
     * @param string $field     The input field name
     * @return boolean|null
     */
    protected function _validateField($field)
    {
        $fieldValidate = null;

        switch ($field) {
            case 'id':
                $fieldValidate = new OSDN_Validate_Id();
                break;

            case 'login':
                $fieldValidate = new Zend_Validate_Regex('~[a-z0-9_-]{3,50}~i');
                $fieldValidate->setMessage("'%value%' does not a valid login name. Only 'a-z 0-9 - _' are allowed",
                    Zend_Validate_Regex::NOT_MATCH);
                break;

            case 'password':
                $fieldValidate = new Zend_Validate_Regex('~[a-z0-9_-]{3,50}~i');
                $fieldValidate->setMessage("'%value%' does not a valid password. Only 'a-z 0-9 - _' are allowed",
                    Zend_Validate_Regex::NOT_MATCH);
                break;

            case 'fullname':
                $fieldValidate = new Zend_Validate_NotEmpty();
                break;

            case 'email':
                $fieldValidate = new Zend_Validate_EmailAddress();
                break;

            case 'active':
            case 'radius':
            case 'mailable':
                $fieldValidate = new OSDN_Validate_Boolean();
                break;

            case 'locale':
                $fieldValidate = new Zend_Validate_InArray(array('en', 'nl'));
                break;
            case 'contract_hour_per_week':
                $fieldValidate = new Zend_Validate_Int();
                break;
            case 'notify_not_filled_in_hours':
                $fieldValidate = new Zend_Validate_InArray(array('0', '1'));
                break;
            // @FIXME
            case 'state':
            case 'phone':
                break;
            
            default:
                $fieldValidate = false;
        }

        return $fieldValidate;
    }

    protected function _validate(array $data, array $columns = array())
    {
        $validators = array();

        foreach($data as $field => $value) {
            $validate = array();
            if (true === $columns[$field]) {
                $validate['presence'] = 'required';
            } else {
                $validate['allowEmpty'] = true;
            }

            $v = $this->_toFilterInputValidate($field);
            if (!empty($v)) {
                $validate[] = $v;
            }

            $validators[$field] = $validate;
        }

        return $validators;
    }

    /**
     * Set anonymous account
     *
     * @param int $id           The account id
     * @return int              The affected rows
     */
    protected function _setAnonymousAccount($id)
    {
        // if anonymous is present = return
        $anonymousPresent = $this->_model()->count(array('anonymous = ?' => 1));
        if ($anonymousPresent) {
            return false;
        }

        $this->_model()->updateQuote(
            array('anonymous'   => new Zend_Db_Expr('NULL')),
            array('anonymous ?' => new Zend_Db_Expr('IS NOT NULL'))
        );

        $affecteRows = $this->_model()->updateByPk(array(
            'anonymous'  => 1
        ), $id);
        return $affecteRows;
    }

    /**
     * Fetch anonymous account
     *
     * @return OSDN_Response
     * <code>
     * array(
     *      'row' => array
     * )</code>
     */
    public function fetchAnonymousAccount()
    {
        $rowset = $this->_model()->fetchRow(array(
            'anonymous = ?' => 1
        ));

        $response = new OSDN_Response();
        $status = null;

        if (!is_null($rowset)) {
            $response->row = $rowset->toArray();
            $status = OSDN_Acl_Status::OK;
        } else {
            $status = OSDN_Acl_Status::FAILURE;
        }

        $response->addStatus(new OSDN_Acl_Status($status));
        return $response;
    }

    /**
     * Change account password
     *
     * @param int $id           The account id
     * @param string $password  The account password
     * @return OSDN_Response
     */
    public function changePassword($id, $password)
    {
        $f = new OSDN_Filter_Input(array(
            'id'    => 'int'
        ), array(
            'id'        => array('id', 'presense'   => 'required'),
            'password'  => array('password', 'presense' => 'required')
        ), array(
            'id'        => $id,
            'password'  => $password
        ));

        $response = new OSDN_Response();
        $response->addInputStatus($f);
        if ($response->hasNotSuccess()) {
            return $response;
        }

        $affectedRow = $this->_model()->updateByPk(array(
            'password'  => md5($password),
        ), $id);

        $response->addStatus(new OSDN_Accounts_Status(OSDN_Accounts_Status::retrieveAffectedRowStatus($affectedRow)));
        return $response;
    }

    /**
     * Change password
     *
     * @param int $id       The account id
     * @param array $data   contains old password and new one (old_password, new_password1, new_password2)
     * @return OSDN_Response
     * <data>
     * array(
     *  affectedRows: int
     * )
     * </data>
     */
    public function chPassword($id, array $data)
    {
        $response = new OSDN_Response();
        $data['id'] = $id;

        $f = new OSDN_Filter_Input(array(
            '*'     => array('StringTrim')
        ), array(
            'old_password'  => array('password', 'presense' => 'required'),
            'new_password1'  => array('password', 'presense' => 'required'),
            'new_password2'  => array('password', 'presense' => 'required')
        ), $data);

        $response->addInputStatus($f);
        if ($response->hasNotSuccess()) {
            return $response;
        }

        $password = $this->_model()->fetchPassword($id);

        if ($password !== md5($f->old_password)) {
            return $response->addStatus(new OSDN_Accounts_Status(OSDN_Accounts_Status::WRONG_PASSWORD, 'old_password'));
        }

        if ($f->new_password1 !== $f->new_password2) {
            return $response->addStatus(new OSDN_Accounts_Status(OSDN_Accounts_Status::UNCORRECT_NEW_PASSWORD, 'new_password2'));
        }

        $affectedRows = $this->_model()->updateByPk(array(
            'password' => md5($f->new_password1)
        ), $id);

        $response->addStatus(new OSDN_Accounts_Status(OSDN_Accounts_Status::retrieveAffectedRowStatus($affectedRows)));
        $response->affectedRows = $affectedRows;
        return $response;
    }
}