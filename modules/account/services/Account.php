<?php

class Account_Service_Account extends OSDN_Application_Service_Dbable
{
    /**
     * @var Account_Model_DbTable_Account
     */
    protected $_table;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new Account_Model_DbTable_Account($this->getAdapter());

        parent::_init();
    }

    public function fetchAllByRoleWithResponse($roleId = null, array $params = array())
    {

        $select = $this->_table->getAdapter()->select()
            ->from(array('a' => $this->_table->getTableName()));



        if ($params['action'] = 'fetch-all-owners') {
            $select->joinLeft(
                array('com'=> 'company'),
                'a.id = com.accountId',
                array('isActiveCompany' => 'isActive')
             );
        }
        if (!empty($roleId)) {
            $select->where('a.roleId = ?', $roleId, Zend_Db::INT_TYPE);
        }

        if (!empty($params)) {

            $fields = array(
            'a.username'       => 'username',
            'a.fullname'       => 'fullname',
            'a.phone'          => 'phone',
            'a.email'          => 'email'
        );

            $this->_initDbFilter($select, $this->_table, $fields)->parse($params);
        }

        return $this->getDecorator('response')->decorate($select);
    }

    /**
     * return Writer row
     */

    public function fetchAllWriters(array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(array('r'=> 'aclRole'))
            ->join(array('ac' => 'account'),
            'r.id = ac.roleId',
            array('*')
        )
            ->where("r.luid = 'textWriter'");

        $fields = array(
            'ac.fullname'       => 'fullname',
            'ac.phone'          => 'phone',
            'ac.email'          => 'email',
            'ac.department'     => 'department'
        );

        $params['filter'] = $this->getFilterParams();

        $this->_initDbFilter($select, $this->_table, $fields)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        $writer = new SupportTicket_Service_SupportTicketModerator();
        $response->setRowsetCallback(function($row) use ($writer)
        {
            $response = $writer->fetchAllAssigned(array('writerId'=> $row['id']));
            $response = $response->getRowset();
            $row['count'] = count($response);
            $response = $writer->fetchAssignedByStatus(array('writerId'=> $row['id']), 'close');
            $response = $response->getRowset();
            $row['close'] = count($response);
            return $row;
        });
        return $response;
    }

    public function activate($id)
    {
        if (null == ($accountRow = $this->_table->findOne($id))) {
            throw new OSDN_Exception('Unable to find account #' . $id);
        }

        $accountRow->setActive(true);
        return (boolean) $accountRow->save();
    }

    public function deactivate($id)
    {
        if (null == ($accountRow = $this->_table->findOne($id))) {
            throw new OSDN_Exception('Unable to find account #' . $id);
        }

        $accountRow->setActive(false);
        $adapter = $this->_table->getAdapter();
        $adapter->beginTransaction();
        try {
            $accountRow->save();
            $contactPersonRow = $accountRow->getContactPersonRow(false);
            if ($contactPersonRow) {
                $contactPersonRow->setFromArray(array(
                    'isActive' => 0
                ));
                $contactPersonRow->save();
            }
            $adapter->commit();
        } catch (\Exception $e) {
            $adapter->rollBack();
            throw new OSDN_Exception('Error deactivating account #' . $id);
        }

        return true;
    }

    public function hasConnectedAccountsInRole(Account_Model_DbTable_AclRoleRow $aclRoleRow)
    {
        return $this->_table->count(array(
            'roleId = ?'    => $aclRoleRow->id
        ), 1) > 0;
    }

    /**
     * @return \Account_Model_DbTable_AccountRow
     */
    public function find($id, $throwException = true)
    {
        $accountRow = $this->_table->findOne($id);
        if (null === $accountRow && true === $throwException) {
            throw new \OSDN_Exception('Unable to find row #' . $id);
        }

        return $accountRow;
    }

    /**
     * @return \Account_Model_DbTable_AccountRow
     */
    public function fetchByUsername($username, $throwException = true)
    {
        $accountRow = $this->_table->fetchRow(array(
            'username = ?'    => $username
        ));

        if (null === $accountRow && true === $throwException) {
            throw new \OSDN_Exception('Unable to find row: username=' . $username);
        }

        return $accountRow;
    }

    /**
     * @return \Account_Model_DbTable_AccountRow
     */
    public function createRow(array $data = array())
    {
        return $this->_table->createRow($data);
    }

    /**
     * @return \Account_Model_DbTable_AccountRow
     */
    public function create(array $data)
    {
        $this->_attachValidationRules('default', array(
            'username'              => array('allowEmpty' => false, 'presence' => 'required'),
            'lastname'              => array('allowEmpty' => false, 'presence' => 'required'),
            'password'              => array(array('PasswordConfirmation', $data, 'confirmPassword'), 'allowEmpty' => false, 'presence' => 'required'),
            'roleId'                => array('id', 'allowEmpty' => false, 'presence' => 'required'),
            'email'                 => array('EmailAddress', 'presence' => 'required', 'allowEmpty' => false),
            'isActive'              => array('boolean', 'allowEmpty' => false, 'presence' => 'required')
        ));
        $data['createdAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();
        $f = $this->_validate($data);
        $accountRow = $this->_table->createRow($f->getData());
        $accountRow->save();

        return $accountRow;
    }

    public function update($id, array $data)
    {
        $this->_attachValidationRules('update', array(
            'username'              => array('allowEmpty' => false),
            'lastname'              => array('allowEmpty' => false),
            'password'              => array(array('PasswordConfirmation', $data, 'confirmPassword'), 'allowEmpty' => false),
            'roleId'                => array('id', 'allowEmpty' => false),
            'email'                 => array('EmailAddress', 'allowEmpty' => false),
            'isActive'              => array('boolean', 'allowEmpty' => false)
        ));

        $f = $this->_validate($data, 'update');

        $this->getAdapter()->beginTransaction();
        try {

            $affectedRows = $this->_table->updateByPk($f->getData(), $id);
            $this->getAdapter()->commit();

        } catch(OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        }

        return true;
    }

    public function updateFields(array $params = array())
    {
        try {
            $this->_table->updateByPk($params, $params['writerId']);
            return true;
        } catch (OSDN_Exception $e) {
            throw $e;
        }
    }

    public function delete($id)
    {
        $accountRow = $this->find($id);
        try {
            $accountRow->delete();
        } catch (OSDN_Exception $e) {
            throw new OSDN_Exception('Some relations do not allow to do this operation!');
        }
    }


    public function updatePassword($id, $password)
    {
        if (empty($password)) {
            throw new OSDN_Exception('Password is empty!');
        }

        $affectedRows = $this->_table->updateByPk(array('password' => $password), $id);

        if (false === $affectedRows) {
            throw new OSDN_Exception('Update account failure.');
        }

        return $affectedRows;
    }

    public function getAccountCount($isActive = null)
    {
        $clause = array();
        if (null !== $isActive) {
            $clause['isActive = ?'] = (int) $isActive;
        }

        return $this->_table->count($clause);
    }

    public function getChildAccounts($accountId)
    {
        return $this->_table->fetchAll(array(
            'parentId = ?'    => $accountId
        ));
    }

    /**
     * @param string $username
     * @return bool true if username exists, false in all other cases
     */
    public function checkAccountExists($username)
    {
        if (empty($username)) {
            return false;
        }
        $accountRow = $this->fetchByUsername($username, false);
        if ($accountRow) {
            return true;
        }
        return false;
    }

    /**
     * @tutorial http://www.laughing-buddha.net/php/password
     * @param int $length length of generated password
     * @return string password
     */
    function generatePassword ($length = 8)
    {
        // start with a blank password
        $password = "";

        // define possible characters - any character in this string can be
        // picked for use in the password, so if you want to put vowels back in
        // or add special characters such as exclamation marks, this is where
        // you should do it
        $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ!@#$%&*?!@#$%&*?";

        // we refer to the length of $possible a few times, so let's grab it now
        $maxlength = strlen($possible);

        // check for length overflow and truncate if necessary
        if ($length > $maxlength) {
            $length = $maxlength;
        }

        // set up a counter for how many characters are in the password so far
        $i = 0;

        // add random characters to $password until $length is reached
        while ($i < $length) {

            // pick a random character from the possible ones
            $char = substr($possible, mt_rand(0, $maxlength-1), 1);

            // have we already used this character in $password?
            if (!strstr($password, $char)) {
                // no, so it's OK to add it onto the end of whatever we've already got...
                $password .= $char;
                // ... and increase the counter by one
                $i++;
            }
        }
        // done!
        return $password;
    }


    /**
     * Returns the accountRow that belongs to the given accountstring
     * @param string $account sha1 of concatenated string of username and md5ed password
     * return Account_Model_DbTable_AccountRow
     */
    public function getByAccountCode($hash)
    {
        return $this->_table->getByAccountCode($hash);
    }

}