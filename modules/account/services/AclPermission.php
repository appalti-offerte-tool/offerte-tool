<?php

class Account_Service_AclPermission extends OSDN_Application_Service_Dbable
{
    /**
     * The permission table instance
     *
     * @var Account_Model_DbTable_AclPermission
     */
    protected $_table;

    /**
     * @var Module_Instance_Manager
     */
    protected $_im;

    public function __construct(Module_Instance_Manager $im)
    {
        $this->_im = $im;
        parent::__construct();
    }

    public function _init()
    {
        $this->_table = new Account_Model_DbTable_AclPermission($this->getAdapter());

        parent::_init();
    }

    public function fetchAllResourcesByRole($roleId)
    {
        $output = array();
        $select = $this->_table->select();

        $isAllowedResources = array();

        $i = 0;
        $k = 10;

        $rowset = $this->_im->fetchAll();
        while(false != ($slice = array_slice($rowset, $i, $k))) {
            $i += $k;
            $names = array();
            foreach($slice as $information) {
                $names[] = $information->getName();
            }

            $select->reset(Zend_Db_Select::WHERE);
            $select->where('roleId = ?', $roleId, Zend_Db::INT_TYPE);
            $select->where($select->getAdapter()->quoteIdentifier('resource') . ' IN(?)', $names);
            foreach($select->query()->fetchAll() as $permissionRow) {
                $isAllowedResources[] = $permissionRow['resource'];
            }
        }

        foreach($rowset as $information) {
            $informationRow = $information->toArray();
            $informationRow['isAllowed'] = in_array($information->getName(), $isAllowedResources);
            $output[] = $informationRow;
        }

        usort($output, function($a, $b) {
            return strcasecmp($a['name'], $b['name']);
        });

        return $output;
    }

    public function fetchAllByParentResource($parentResourceName)
    {
        if (null == ($information = $this->_im->fetch($parentResourceName))) {
            throw new OSDN_Exception('Unable to find resource: ' . $parentResourceName);
        }

        return $information->getAcl();
    }

    public function fetchAllByRoleAndResource($roleId, $parentResource)
    {
        $aclResourceRowset = $this->fetchAllByParentResource($parentResource);

        $select = $this->_table->select();
        $select->order(array('resource ASC', 'privilege ASC'));

        foreach($aclResourceRowset as & $aclResourceRow) {

            $subResourceName = sprintf('%s:%s', $parentResource, $aclResourceRow['name']);
            $select->reset(Zend_Db_Select::WHERE);

            $select->where($select->getAdapter()->quoteIdentifier('roleId') . ' = ?', $roleId, Zend_Db::INT_TYPE);

            $select->where('(' . $select->getAdapter()->quoteIdentifier('resource') . ' = ?', $subResourceName);
            $select->orWhere($select->getAdapter()->quoteIdentifier('resource') . ' = ?)', $parentResource);

            foreach($select->query()->fetchAll() as $permissionRow) {

                $aclResourceRow['isAllowed'] = 0 === strcasecmp($subResourceName, $permissionRow['resource']);

                foreach($aclResourceRow['privileges'] as & $aclResourceRowPrivilege) {

                    if (
                        !empty($permissionRow['privilege'])
                        && 0 === strcasecmp($aclResourceRowPrivilege['name'], $permissionRow['privilege'])
                    ) {
                        $aclResourceRowPrivilege['isAllowed'] = true;
                    }
                }
            }
        }

        usort($aclResourceRowset, function($a, $b) {
            return strcasecmp($a['name'], $b['name']);
        });

        return $aclResourceRowset;
    }

    public function allow($roleId, $resource, array $privileges = array())
    {
        if (empty($privileges)) {
            $permissionRow = $this->_table->createRow();
            $permissionRow->roleId = $roleId;
            $permissionRow->resource = $resource;
            $result = (boolean) $permissionRow->save();

            $this->_flushCacheByRoleId($roleId);
            return $result;
        }

        foreach($privileges as $childResource => $childResourceValue) {

            if (is_numeric($childResourceValue)) {

                $this->_table->insert(array(
                    'roleId'    => $roleId,
                    'resource'  => sprintf('%s:%s', $resource, $childResource)
                ));

            } else {
                foreach($childResourceValue as $privilege => $pValue) {
                    if (1 != (int) $pValue) {
                        // something wrong
                    }

                    $this->_table->insert(array(
                        'roleId'    => $roleId,
                        'resource'  => sprintf('%s:%s', $resource, $childResource),
                        'privilege' => $privilege
                    ));
                }
            }
        }

        $this->_flushCacheByRoleId($roleId);
        return true;
    }

    public function deny($roleId, $resource, array $privileges = array())
    {
        if (empty($privileges)) {
            $result = (boolean) $this->_table->deleteQuote(array(
                'roleId = ?'      => $roleId,
                'resource LIKE ?' => $resource . '%'
            ));

            $this->_flushCacheByRoleId($roleId);
            return $result;
        }

        foreach($privileges as $childResource => $childResourceValue) {

            if (is_numeric($childResourceValue)) {
                $this->_table->deleteQuote(array(
                    'roleId = ?'    => $roleId,
                    'resource = ?'  => sprintf('%s:%s', $resource, $childResource)
                ));
            } else {
                foreach($childResourceValue as $privilege => $pValue) {
                    if (0 != (int) $pValue) {
                        // something wrong
                    }

                    $this->_table->delete(array(
                        'roleId = ?'    => $roleId,
                        'resource = ?'  => sprintf('%s:%s', $resource, $childResource),
                        'privilege = ?' => $privilege
                    ));
                }
            }
        }

        $this->_flushCacheByRoleId($roleId);
        return true;
    }

    protected function _flushCacheByRoleId($roleId)
    {
        if (!Account_Service_Authenticate::hasCache()) {
            return false;
        }

        $cache = Account_Service_Authenticate::getCache();
        $tags = Account_Service_Authenticate::getCacheRoleTags($roleId);
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, $tags);
    }
}