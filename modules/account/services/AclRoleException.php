<?php

class Account_Service_AclRoleException extends OSDN_Exception
{
    const HAS_CONNECTED_ACCOUNTS_IN_ROLE = 1;

    public static function hasConnectedAccountsInRole($name)
    {
        $message = new OSDN_Exception_Message(
            'Some accounts are still connected to role: "%s"',
            array($name)
        );
        $message->markAsTranslated();
        return new self($message, self::HAS_CONNECTED_ACCOUNTS_IN_ROLE);
    }
}