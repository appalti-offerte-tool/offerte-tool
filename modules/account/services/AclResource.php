<?php

class Account_Service_AclResource extends OSDN_Application_Service_Dbable
{
    /**
     * The permission table instance
     *
     * @var Account_Model_DbTable_AclPermission
     */
    protected $_table;

    public function __construct(Module_Instance_Manager $im)
    {
        parent::__construct();
    }

    public function _init()
    {
        $this->_table = new Account_Model_DbTable_AclPermission($this->getAdapter());

        parent::_init();
    }

    public function fetchAllByParentId($parentId = null)
    {
        $clause = array();
        if (empty($parentId)) {
            $clause['parentId ?'] = new Zend_Db_Expr('IS NULL');
        } else {
            $clause['parentId = ?'] = $parentId;
        }

        return $this->_table->fetchAll($clause);
    }
}