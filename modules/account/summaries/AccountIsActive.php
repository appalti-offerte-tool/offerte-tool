<?php

class Account_Summary_AccountIsActive extends Application_Instance_Summary
{
    public function __construct()
    {
        parent::__construct();

        $this->_setHref('account', 'acl-role', 'index', array('f' => 1));
    }

    protected function _bind()
    {
        $this->_title = $this->getTranslator()->translate('Account active');

        $account = new Account_Service_Account();
        $this->_value = $account->getAccountCount(true);
    }
}