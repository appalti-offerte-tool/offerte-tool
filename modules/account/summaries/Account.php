<?php

class Account_Summary_Account extends Application_Instance_Summary
{
    public function __construct()
    {
        parent::__construct();

        $this->_setHref('account', 'acl-role');
    }

    protected function _bind()
    {
        $this->_title = $this->getTranslator()->translate('Account');

        $account = new Account_Service_Account();
        $this->_value = $account->getAccountCount();
    }
}