<?php

class Account_Summary_AccountInactive extends \Application_Instance_Summary
{
    public function __construct()
    {
        parent::__construct();

        $this->_setHref('account', 'acl-role', 'index', array('f' => 0));
    }

    protected function _bind()
    {
        $this->_title = $this->getTranslator()->translate('Account inactive');

        $account = new Account_Service_Account();
        $this->_value = $account->getAccountCount(false);
    }
}