<?php

final class Ddbm_Block_Preview extends Application_Block_Abstract
{
    public function _toTitle()
    {
        return $this->view->translate('Preview');
    }
}