<?php

final class Ddbm_Block_FieldListMetadata extends Application_Block_Abstract
{
    public function _toTitle()
    {
        return $this->view->translate('Dynamic fields');
    }
}