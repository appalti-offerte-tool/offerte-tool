<?php

class Ddbm_IndexController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    public function init()
    {
        parent::init();

        $this->_helper->ajaxContext()
            ->addActionContext('fetch-all', 'json')
            ->initContext();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'ddbm';
    }

    public function indexAction()
    {
        // load index template
    }

    public function fetchAllAction()
    {
        $bootstrap = $this->getFrontController()->getParam('bootstrap');
        $bootstrap->bootstrap('ModulesManager');
        $mm = $bootstrap->getResource('ModulesManager');

        $rowset = array();
        foreach($mm->fetchAll() as $row) {


            $o = array(
                'module'    => $row->getName(),
                'title'     => $row->getTitle()
            );

            foreach($row->getDb() as $tbInformation) {

                if (false === $tbInformation['ddbm']) {
                    continue;
                }

                $ddbm = $o;
                $ddbm['name'] = $tbInformation['ddbm'];
                $rowset[] = $ddbm;
            }
        }

        $this->view->rowset = $rowset;
        $this->view->total = count($rowset);
    }
}