<?php

class Ddbm_FieldController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Ddbm_Service_Field
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();

        $this->_helper->ajaxContext()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('update-field', 'json')
            ->addActionContext('delete', 'json')
            ->initContext();

        $mm = $this->getInvokeArg('bootstrap')->getResource('ModulesManager');
        $information = $mm->fetch($this->_getParam('moduleId'));
        if (null === $information) {
            throw new \OSDN_Exception('Unable to find module: ' . $this->_getParam('moduleId'));
        }

        $ddbmTableObj = null;
        $ddbmId = $this->_getParam('ddbmId');
        foreach($information->getDb() as $tb) {
            if (0 !== strcasecmp($tb['ddbm'], $ddbmId)) {
                continue;
            }

            if (!empty($tb['ddbmTableCls'])) {
                $ddbmTableCls = $tb['ddbmTableCls'];
            } else {
                $ddbmTableCls = $information->getNameCamelCase() . '_Model_DbTable_' . ucfirst($tb['ddbm']);
            }

            $ddbmTableObj = new $ddbmTableCls(\OSDN_Db_Table_Abstract::getDefaultAdapter());
            break;
        }

        if (
            null === $ddbmTableObj ||
            ! $ddbmTableObj instanceof \Ddbm_Model_DbTable_Abstract
        ) {
            throw new \OSDN_Exception(sprintf(
                'The ddbm table object should be instance of "Ddbm_Model_DbTable_Abstract" instead of "%s"',
                is_object($ddbmTableObj) ? get_class($ddbmTableObj) : var_export($ddbmTableObj, true)
            ));
        }

        $this->_service = new Ddbm_Service_Field($ddbmTableObj);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'ddbm:metadata';
    }

    public function indexAction()
    {
        // load index template
    }

    public function fetchAllAction()
    {
        $response = $this->_service->fetchAll($this->_getAllParams());
        $this->view->assign($response->toArray());
    }

    public function fetchAction()
    {
        $row = $this->_service->find($this->_getParam('id'));
        $this->view->row = $row->toArray();
        $this->view->success = true;
    }

    public function createAction()
    {
        try {
            $fieldRow = $this->_service->create($this->_getAllParams());
            $this->view->fieldId = $fieldRow->getId();

            $this->_helper->information(
                array('Field has been successfully created.'),
                true, E_USER_NOTICE
            );
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $fieldRow = $this->_service->update($this->_getParam('fieldId'), $this->_getAllParams());
            $this->view->success = true;
            $this->view->fieldId = $fieldRow->getId();
            $this->_helper->information(
                'Field has been successfully updated.',
                true,
                E_USER_NOTICE
            );

        } catch (OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateFieldAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $affectedRows = $this->_service->update($this->_getParam('fieldId'), array(
                $this->_getParam('field') => $this->_getParam('value'),
                'ddbmId'     => $this->_getParam('ddbmId'),
                'moduleId'   => $this->_getParam('moduleId')
            ));

            $this->view->success = true;
            $this->_helper->information(array('Updated successfully'), true, E_USER_NOTICE);

        } catch (OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $this->_service->delete($this->_getParam('id'));

            $this->view->success = true;
            $this->_helper->information('Field has been successfully updated.', true, E_USER_NOTICE);

        } catch (\OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }
}