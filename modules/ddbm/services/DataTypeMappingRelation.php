<?php

class Ddbm_Service_DataTypeMappingRelation extends \OSDN_Application_Service_Dbable
{
    const TYPE_INT          = 'INT';
    const TYPE_BIGINT       = 'BIGINT';

    const TYPE_FLOAT        = 'FLOAT';

    CONST TYPE_TEXT         = 'TEXT';
    CONST TYPE_LONGTEXT     = 'LONGTEXT';

    CONST TYPE_VARCHAR      = 'VARCHAR';
    CONST TYPE_ENUM         = 'ENUM';

    CONST TYPE_DATE         = 'DATE';
    CONST TYPE_DATETIME     = 'DATETIME';
    CONST TYPE_TIME         = 'TIME';
    CONST TYPE_TIMESTAMP    = 'TIMESTAMP';

    /**
     * @var Module_Instance_Manager
     */
    protected $_mm;

    public function __construct(\Module_Instance_Manager $mm)
    {
        $this->_mm = $mm;

        parent::__construct();
    }

    public static function toArrayDbDataType($inLowerCase = false)
    {
        $o = array(
            self::TYPE_INT,
            self::TYPE_BIGINT,
            self::TYPE_FLOAT,
            self::TYPE_TEXT,
            self::TYPE_LONGTEXT,
            self::TYPE_VARCHAR,
            self::TYPE_ENUM,
            self::TYPE_DATE,
            self::TYPE_DATETIME,
            self::TYPE_TIME,
            self::TYPE_TIMESTAMP
        );

        if (true === $inLowerCase) {
            $o = array_map('strtolower', $o);
        }

        return $o;
    }

    /**
     * @todo
     */
    public function fetchAllAcceptedDataTypeForComponent($componentId)
    {

    }
}