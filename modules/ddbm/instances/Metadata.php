<?php

final class Ddbm_Instance_Metadata
{
    protected $_kind;

    protected $_table;

    public function __construct($kind)
    {
        $this->_kind = $kind;

        $this->_table = new Ddbm_Model_DbTable_Metadata();
    }

    public function getMetadata()
    {
        $rowset = $this->_table->fetchAll(array(
            'kind = ?' => $this->_kind
        ));

        return $rowset;
    }
}