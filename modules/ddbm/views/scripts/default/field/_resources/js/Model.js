Ext.define('Module.Ddbm.Model.Field', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name', 'alias', 'caption', 'dataType', 'mapping',
        {name: 'isEditable', type: 'boolean'},
        {name: 'isVisible', type: 'boolean'},
        {name: 'isVisibleInList', type: 'boolean'},
        {name: 'isRequired', type: 'boolean'},
        {name: 'isSearchable', type: 'boolean'}
    ]
});