Ext.define('Module.Application.Model.Error', {
    extend: 'Ext.data.Model',
    fields: [
        'message', 'type', 'field',
        {name: 'id', mapping: 'field'},
        {name: 'msg', mapping: 'message'}
    ]
});

Ext.define('Module.Ddbm.Field.Form', {
    extend: 'Ext.form.Panel',
    alias: 'widget.ddbm.field.form',

    bodyPadding: 5,

    ddbmId: null,
    moduleId: null,
    fieldId: null,

    trackResetOnLoad: true,
    waitMsgTarget: true,

    wnd: null,

    initComponent: function() {

        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.Ddbm.Model.Field',
            type: 'json',
            root: 'row'
        });

        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
            model: 'Module.Application.Model.Error',
            type: 'json',
            root: 'messages',
            successProperty: 'success'
        });

        this.items = [{
            xtype: 'textfield',
            fieldLabel: lang('Name') + '<span class="asterisk">*</span>',
            name: 'alias',
            allowBlank: false,
            anchor: '100%'
        }, {
            xtype: 'textfield',
            fieldLabel: lang('Caption') + '<span class="asterisk">*</span>',
            name: 'caption',
            allowBlank: false,
            anchor: '100%'
        }, {
            xtype: 'checkboxgroup',
            fieldLabel: lang('Options'),
            columns: 2,
            vertical: true,
            items: [{
                boxLabel: lang('Visible'),
                name: 'isVisible',
                inputValue: 1,
                uncheckedValue: 0
            }, {
                boxLabel: lang('Editable'),
                name: 'isEditable',
                inputValue: 1,
                uncheckedValue: 0
            }, {
                boxLabel: lang('Required'),
                name: 'isRequired',
                inputValue: 1,
                uncheckedValue: 0
            }, {
                boxLabel: lang('Visible in list'),
                name: 'isVisibleInList',
                inputValue: 1,
                uncheckedValue: 0
            }, {
                boxLabel: lang('Searchable'),
                name: 'isSearchable',
                inputValue: 1,
                ncheckedValue: 0
            }]
        }];

        this.callParent();

        this.addEvents(
            /**
             * Fires when account is created
             *
             * @param {Module.Ddbm.Form}
             */
            'completed'
        );
    },

    doLoad: function() {

        if (!this.fieldId) {
            return;
        }

        this.form.load({
            url: link('ddbm', 'field', 'fetch', {format: 'json'}),
            method: 'get',
            params: {
                id: this.fieldId,
                moduleId: this.moduleId,
                ddbmId: this.ddbmId
            },
            waitMsg: Ext.LoadMask.prototype.msg,
            success: function(response, result, type) {
                Application.notificate(result);
            },
            scope: this
        });
    },

    onSubmitField: function(panel, w) {

        if (true !== this.form.isValid()) {
            return;
        }

        var params = Ext.apply({
            ddbmId: this.ddbmId,
            moduleId: this.moduleId,
        }, this.invokeArgs || {});

        var action = '';
        if (this.fieldId) {
            params['fieldId'] = this.fieldId;
            action = 'update';
        } else {
            action = 'create';
        }

        this.form.submit({
            url: link('ddbm', 'field', action, {format: 'json'}),
            method: 'post',
            params: params,
            waitMsg: Ext.LoadMask.prototype.msg,
            success: function(form, action) {

                var responseObj = Ext.decode(action.response.responseText);
                Application.notificate(action);

                if (action.result.success) {
                    this.fireEvent('completed', this, responseObj.fieldId, action.response);
                    this.wnd && this.wnd.close();
                }
            },
            scope: this
        });
    },

    showInWindow: function() {

        var w = this.wnd = new Ext.Window({
            title:  this.fieldId ? lang('Update field') : lang('Create field'),
            iconCls: 'm-ddbm-icon-16',
            resizable: false,
            width: 415,
            modal: true,
            border: false,
            items: [this],
            buttons: [{
                iconCls: 'icon-submit-16',
                text: lang('Submit'),
                handler: function() {
                    this.onSubmitField(this, w);
                },
                scope: this
            }, {
                text: lang('Cancel'),
                handler: function() {
                    w.close();
                }
            }],
            scope: this
        });

        w.show();
        this.doLoad();

        return w;
    }
});