Ext.define('Module.Ddbm.Field.List', {

    ddbmId: null,
    moduleId: null,

    extend: 'Ext.grid.Panel',
    alias: 'module.ddbm.field.list',

    stateful: true,
    stateId: 'module.ddbm.field.list',

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.Ddbm.Model.Field',
            proxy: {
                type: 'ajax',
                url: link('ddbm', 'field', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset',
                    totalProperty: 'total'
                },
                extraParams: {
                    ddbmId: this.ddbmId,
                    moduleId: this.moduleId
                }
            },
            simpleSortMode: true
        });

        this.columns = [{
            header: lang('Name'),
            dataIndex: 'name',
            width: 45
        }, {
            header: lang('Alias'),
            dataIndex: 'alias',
            width: 100
        }, {
            header: lang('Caption'),
            dataIndex: 'caption',
            flex: 1
        }, {
            header: lang('Data type'),
            dataIndex: 'dataType',
            width: 65
        }, {
            header: lang('Mapping'),
            dataIndex: 'mapping',
            width: 75
        }];

        Ext.each([
            {n: 'Visible', di: 'isVisible'},
            {n: 'Visible in list', di: 'isVisibleInList', w: 80},
            {n: 'Editable', di: 'isEditable'},
            {n: 'Required', di: 'isRequired', w: 60},
            {n: 'Searchable', di: 'isSearchable', w: 75}
        ], function(i) {

            this.columns.push({
                xtype: 'checkcolumn',
                text: lang(i.n),
                dataIndex: i.di,
                width: i.w || 50,
                listeners: {
                    checkchange: function(cc, rowIndex, checked, record) {
                        this.onEditFieldProp({
                            field: i.di,
                            value: + checked,
                            record: record,
                            grid: this
                        }, this);
                    },
                    scope: this
                }
            });

        }, this);

        this.columns.push({
            xtype: 'actioncolumn',
            width: 50,
            align: 'center',
            header: lang('Actions'),
            fixed: true,
            items: [{
                tooltip: lang('Edit'),
                iconCls: 'icon-edit-16 icon-16',
                handler: this.onEditField,
                scope: this
            }, {
                tooltip: lang('Delete'),
                iconCls: 'icon-delete-16 icon-16',
                handler: this.onDeleteField,
                scope: this
            }]
        });

        this.features = [{
            ftype: 'filters'
        }];

        this.plugins = [Ext.create('Ext.ux.grid.Search', {
            minChars: 2,
            align: 2,
            stringFree: true
        })];

        this.tbar = [{
            text: lang('Create'),
            iconCls: 'icon-create-16',
            handler: this.onCreateField,
            scope: this
        }, '-'];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent(arguments);

        this.on({
            afteredit: this.onEditFieldProp,
            itemdblclick: function(g, record, html, rowIndex) {
                this.onEditField(g, rowIndex);
            },
            scope: this
        });
    },

    onCreateField: function() {
        Application.require([
            'ddbm/field/form'
        ], function() {
            var f = new Module.Ddbm.Field.Form({
                ddbmId: this.ddbmId,
                moduleId: this.moduleId
            });

            f.on('completed', function(accountId) {
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditField: function(g, rowIndex) {
        var record = g.getStore().getAt(rowIndex);

        Application.require([
            'ddbm/field/form'
        ], function() {
            var f = new Module.Ddbm.Field.Form({
                ddbmId: this.ddbmId,
                moduleId: this.moduleId,
                fieldId: record.get('id')
            });

            f.on('completed', function(form, fieldId) {
                g.getStore().load();
            }, this);

            f.showInWindow();
        }, this);
    },

    onEditFieldProp: function(data, g) {

        var params = {
            ddbmId: this.ddbmId,
            moduleId: this.moduleId,
            fieldId: data.record.get('id')
        };

        var action = null;
        switch(data.field) {

            default:
                action = link('ddbm', 'field', 'update-field', {format: 'json'});
                params.field = data.field;
                params.value = data.value;
                break;
        }

        Ext.Ajax.request({
            url: action,
            params: params,
            callback: function(options, success, response) {
                var decResponse = Ext.decode(response.responseText);
                if (decResponse && decResponse.success) {
                    data.record.commit();
                    Application.notificate(decResponse.messages || lang('Updated successfully'));
                    return;
                }

                data.record.reject();
                Application.notificate(decResponse.messages || lang('Unable to update'));
            },
            scope: this
        });
    },

    setDdbmCredentials: function(ddbmId, moduleId) {

        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.ddbmId = ddbmId;
        this.ddbmId = ddbmId;

        p.extraParams.moduleId = moduleId;
        this.moduleId = moduleId;

        this.getStore().load();

        return this;
    },

    onDeleteField: function(g, rowIndex) {

        var record = g.getStore().getAt(rowIndex);

        Ext.Msg.show({
            icon: Ext.Msg.WARNING,
            fn: function(b) {
                b == 'yes' && this.onDeleteFieldCallbackFn(record);
            },
            modal: true,
            title: 'Delete field confirmation dialogue',
            msg: '<b>The field "' + record.get('caption') + '" can contain the data. <br/>' +
                'All data will be complitely lost without restore posibility. <br/>' +
                'Do you really want to continue?</b>',
            width: 400,
            buttons: Ext.Msg.YESNO,
            scope: this
        });
    },

    onDeleteFieldCallbackFn: function(record) {
        Ext.Ajax.request({
            url: link('ddbm', 'field', 'delete', {format: 'json'}),
            params: {
                id: record.get('id'),
                ddbmId: this.ddbmId,
                moduleId: this.moduleId
            },
            callback: function(options, success, response) {
                var decResponse = Ext.decode(response.responseText);
                Application.notificate(decResponse.messages);
                if (decResponse.success) {
                    this.getStore().load();
                }
            },
            scope: this
        });
    }
});