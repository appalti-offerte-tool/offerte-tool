Ext.define('Module.Ddbm.Model.Ddbm', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name', 'title', 'module',
        'cfields'
    ],
});

Ext.define('Module.Ddbm.List', {
    extend: 'Ext.ux.grid.GridPanel',
    alias: 'module.ddbm.list',

    stateful: false,

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.Ddbm.Model.Ddbm',
            groupField: 'module',
            sorters: ['module'],
            proxy: {
                type: 'ajax',
                url: link('ddbm', 'index', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.features = [{
            ftype: 'grouping',
            groupHeaderTpl: lang('Module') + ': {name}'
        }, {
            ftype: 'filters'
        }];

        this.columns = [{
            header: lang('Name'),
            flex: 1,
            dataIndex: 'name',
            sortable : true
        }, {
            header: lang('Module'),
            hidden: true,
            hideable: false,
            dataIndex: 'module'
        }, {
            header: lang('Fields count'),
            dataIndex: 'cfields',
            align: 'center'
        }];

        this.plugins = [Ext.create('Ext.ux.grid.Search', {
            minChars: 2,
            align: 0,
            stringFree: true
        })];

        this.tbar = [];

        this.bbar = ['->', {
            iconCls: 'x-tbar-loading',
            handler: function() {
                this.getStore().load();
            },
            scope: this
        }];

        this.callParent(arguments);
    }
});