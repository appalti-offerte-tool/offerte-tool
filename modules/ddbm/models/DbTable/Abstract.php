<?php

abstract class Ddbm_Model_DbTable_Abstract extends OSDN_Db_Table_Abstract
{
    /**
     * The sequence is always the related row
     *
     * @var boolean
     */
    protected $_sequence = false;

    /**
     * For this kind of structure we use by default the
     * primary key of related table
     *
     * @var boolean
     */
    protected $_clearPkOnInsert = false;

    /**
     * Retrive ddbm kind identity
     * To this identity will be assigned all fields definitions
     *
     * @return string
     */
    abstract public function getKind();
}