<?php

class Ddbm_Model_DbTable_Metadata extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'ddbmMetadata';

    protected $_rowClass = 'Ddbm_Model_Metadata';

    protected $_serializableColumns = array(
        'filters', 'validators', 'format'
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
        unset($data['name'], $data['kind']);
        return parent::update($data, $where);
    }
}