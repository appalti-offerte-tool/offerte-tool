<?php

class Ddbm_Model_DdbmData extends Zend_Db_Table_Row_Abstract implements OSDN_Application_Model_Interface
{
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return  $this->id;
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Db_Table_Row_Abstract::toArray()
     */
    public function toArray()
    {
        $output = array();
        foreach($this->_data as $columnName => $value) {
            $output[$this->toAliasFieldName($columnName)] = $value;
        }

        return $output;
    }

    public function toArrayWithOmit($mode = OSDN_Db_Table_Abstract::OMIT_FETCH_ROW)
    {
        $o = $this->toArray();

        return $this->getTable()->filterRow($o, $mode);
    }

    public function toRawFieldName($alias)
    {
        /**
         * @FIXME Make some optimization (caching)
         */
        $md = new Ddbm_Instance_Metadata('account');
        $metadata = $md->getMetadata();
        foreach($metadata as $md) {
            if (0 === strcasecmp($alias, $md->alias)) {
                return $md->name;
            }
        }

        return $alias;
    }

    public function toAliasFieldName($raw)
    {
        /**
         * @FIXME Make some optimization (caching)
         */
        $md = new Ddbm_Instance_Metadata('account');
        $metadata = $md->getMetadata();
        foreach($metadata as $md) {
            if (0 === strcasecmp($raw, $md->name)) {
                return $md->alias;
            }
        }

        return $raw;
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Db_Table_Row_Abstract::_transformColumn()
     */
//    protected function _transformColumn($columnName)
//    {
//        $columnName = parent::_transformColumn($columnName);
//
//        $md = new Ddbm_Instance_Metadata('account');
//        $metadata = $md->getMetadata();
//        foreach($metadata as $md) {
//            if (0 === strcasecmp($columnName, $md->name)) {
//                return $md->alias;
//            }
//        }
//
//        return $columnName;
//    }

    /**
     * (non-PHPdoc)
     * @see Zend_Db_Table_Row_Abstract::setFromArray()
     */
    public function setFromArray(array $data)
    {
        foreach ($data as $columnName => $value) {

//            if (!$this->getTable()->hasColumn()) {
//
//            }

            $this->__set($columnName, $value);
        }

        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Db_Table_Row_Abstract::__get()
     */
    public function __get($columnName)
    {
        $columnName = $this->toRawFieldName($columnName);

        return parent::__get($columnName);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Db_Table_Row_Abstract::__set()
     */
    public function __set($columnName, $value)
    {
        $columnName = $this->toRawFieldName($columnName);

        return parent::__set($columnName, $value);
    }
}