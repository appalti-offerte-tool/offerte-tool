<?php

class Module_Instance_Manager
{
    /**
     * Contain instance of self
     *
     * @var ModuleManager
     */
    private static $_instance;

    protected $_modules = array();

    protected $_moduleDirectories;

    /**
     * Internal cache for ldml values
     *
     * @var Zend_Cache_Core
     * @access private
     */
    private static $_cache = null;

    /**
     * Create new instance of object
     * Rememeber instance in static var for prevent double creation of object
     *
     * @throws OSDN_Exception
     * @param $moduleDirectories
     * @return void
     */
    public function __construct($moduleDirectories)
    {
        if (null !== self::$_instance) {
            throw new OSDN_Exception('Manager can exists only with one instance');
        }

        if (is_array($moduleDirectories)) {
            $this->_moduleDirectories = $moduleDirectories;
        } else if (is_string($moduleDirectories)) {
            $this->_moduleDirectories = array($moduleDirectories);
        } else {
            throw new OSDN_Exception('Directory path is not valid.');
        }

        self::$_instance = $this;
    }

    /**
     * Retrieve instance of manager
     * Throws an exception if instance is not initialized.
     *
     * @throws OSDN_Exception
     * @return Module_Service_ModuleManager
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            throw new OSDN_Exception('Manager is not initialized.');
        }

        return self::$_instance;
    }

    public function walkAndRegisterAccesibleModules()
    {
        require_once APPLICATION_PATH . '/modules/module/instances/Information.php';

        if (null !== self::$_cache) {
            if (Module_Instance_Information::hasCache()) {
                $cache = Module_Instance_Information::getCache();
                $cache->clean();
            }

            self::$_cache->remove(__CLASS__);
        }

        $service = new Module_Service_Module();
        $moduleRowset = array();
        foreach($service->fetchAll() as $moduleRow) {
            $moduleRowset[$moduleRow->name] = $moduleRow->isEnabled();
        }

        $service = new Module_Service_Module();
        foreach($this->_moduleDirectories as $md) {

            /**
             * @FIXME Make more smart existing module detection
             */
            if (!is_dir($md)) {
                continue;
            }

            foreach(new DirectoryIterator($md) as $module) {

                $moduleName = $module->getFilename();
                if ($module->isDot() || '.' == $moduleName[0]) {
                    continue;
                }

                if (array_key_exists($moduleName, $moduleRowset)) {
                    $this->_modules[$moduleName] = new Module_Instance_Information($module->getPathname());
                    continue;
                }

                $information = new Module_Instance_Information($module->getPathname());
                $service->create($information);
                $this->_modules[$information->getName()] = $information;
            }
        }

        return $this->_modules;
    }

    public function fetchAll()
    {
        if (empty($this->_modules)) {

            if (false !== ($hasCache = null !== self::$_cache)) {
                if (false === ($modules = self::$_cache->load(__CLASS__))) {
                    $modules = $this->walkAndRegisterAccesibleModules();
                    self::$_cache->save($modules, __CLASS__, array(), 86400);
                }

                $this->_modules = $modules;

            } else {
                $this->_modules = $this->walkAndRegisterAccesibleModules();
            }
        }

        return $this->_modules;
    }

    /**
     * Fetch module information
     *
     * @param string $module
     * @return Module_Instance_Information|null
     */
    public function fetch($module)
    {
        foreach($this->fetchAll() as $information) {
            if (0 === strcasecmp($module, $information->getName())) {
                return $information;
            }
        }

        return null;
    }

    /**
     * Returns the set cache
     *
     * @return Zend_Cache_Core The set cache
     */
    public static function getCache()
    {
        return self::$_cache;
    }

    /**
     * Set a cache for Module_Instance_Information
     *
     * @param Zend_Cache_Core $cache A cache frontend
     */
    public static function setCache(Zend_Cache_Core $cache)
    {
        self::$_cache = $cache;
    }
}