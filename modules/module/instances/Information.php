<?php

/**
 * Module information object
 * Define module status in the system
 *
 * @version $Id: Information.php 2132 2012-06-15 09:56:57Z yaroslav $
 */
class Module_Instance_Information extends \Zend_Application_Module_Bootstrap
{
    /**
     * Contain base module path
     *
     * @var string
     */
    protected $_path;

    /**
     * Contain raw module name
     *
     * @var string
     */
    protected $_name;

    /**
     * Contain formatted module name
     *
     * @var string
     */
    protected $_nameCamelCase;

    /**
     * Contain actual module version
     *
     * @var string
     */
    protected $_version = '0';

    /**
     * Module title
     *
     * @var string
     */
    protected $_title = 'Untitied module';

    /**
     * Description
     *
     * @var text
     */
    protected $_description = '';

    /**
     * Define true if module or module configuration
     * is missing in filesystem
     *
     * @var boolean
     */
    protected $_isMissing = true;

    /**
     * Define true if module is protected
     * Protected means that module is core or default
     * and without this module system does not work properly
     *
     * @var boolean
     */
    protected $_isProtected = true;

    /**
     * Define if module is enabled
     *
     * @var boolean
     */
    protected $_isEnabled = false;

    /**
     * Contain list of dependences of other modules
     *
     * @var array
     */
    protected $_dependences = array();

    /**
     * Contain list of available blocks
     *
     * @var array
     */
    protected $_blocks = array(

        /**
         * Blocks which are described internally
         * and related "TO" other modules
         * @var array
         */
        'internals'    => array(),

        /**
         * Blocks which are described in "OTHER" modules
         * and related "FROM" other modules to this module
         */
        'relations'    => array()
    );

    /**
     * Contain list of available summaries
     *
     * @var array
     */
    protected $_summary;

    protected $_acl = array();

    protected $_db = array();

    protected $_menu = array();

    /**
     * Internal cache
     *
     * @var Zend_Cache_Core
     * @access private
     */
    private static $_cache = null;

    /**
     * Internal value to remember if cache supports tags
     *
     * @var boolean
     */
    private static $_cacheTags = false;

    /**
     * Internal option, cache disabled
     *
     * @var     boolean
     * @access  private
     */
    private static $_cacheDisabled = false;

    private static $_aclCheckPermissionCallbackFn = null;

    /**
     * Initialize module information
     * Read configuration and fill into object properties
     *
     * @param $path string      Path to root module directory
     *
     * @throws OSDN_Exception
     */
    public function __construct($path)
    {
        if (false === ($path = realpath($path))) {
            throw new OSDN_Exception(sprintf('The following path does not exists: ', (string) $path));
        }

        $this->_name = strtolower(basename($path));
        $this->_path = $path;

        $this->__wakeup();

        /**
         * Read module configuration...
         */
        $this->_read();
    }

    /**
     * Using with cache implementation
     * When executed serialize method like
     * serialize($information) the method sleep return
     * the list of properties which will be stored in cache
     *
     * @return array
     */
    public function __sleep()
    {
        return array(
            '_path', '_name', '_nameCamelCase',
            '_version', '_title', '_description',
            '_isMissing', '_isProtected', '_isEnabled',
            '_dependences',
            '_blocks',
            '_summary',
            '_acl', '_db', '_options', '_moduleName',
            '_menu'
        );
    }

    public function __wakeup()
    {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $this->setApplication($bootstrap->getApplication());

        $mBootstrapFilename = $this->getRoot() . '/Bootstrap.php';
        if (file_exists($mBootstrapFilename)) {
            require_once $mBootstrapFilename;
            $mBootstrapCls = $this->getNameCamelCase() . '_Bootstrap';
            $mBootstrap = new $mBootstrapCls($bootstrap->getApplication());

            $mBootstrap->bootstrap();
        }
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getNameCamelCase()
    {
        if (null === $this->_nameCamelCase) {
            $dashToCamelCase = new Zend_Filter_Word_DashToCamelCase();
            $this->_nameCamelCase = $dashToCamelCase->filter($this->getName());
        }

        return $this->_nameCamelCase;
    }

    public function isEnabled()
    {
        return $this->_isEnabled;
    }

    public function isMissing()
    {
        return $this->_isMissing;
    }

    public function isProtected()
    {
        return $this->_isProtected;
    }

    public function getTitle()
    {
        return $this->_title;
    }

    public function getVersion()
    {
        return $this->_version;
    }

    /**
     * @return text
     */
    public function getDescription()
    {
        return $this->_description;
    }

    public function getDependences()
    {
        return $this->_dependences;
    }

    public function getRoot()
    {
        return $this->_path;
    }

    protected function _getInternalBlockDefinition($name)
    {
        if (!array_key_exists($name, $this->_blocks['internals'])) {
            return null;
        }

        return $this->_blocks['internals'][$name];
    }

    public function getBlock($name, array $args = array(), array $o = array(), Closure $permissionFn = null)
    {
        if (null === ($block = $this->_getInternalBlockDefinition($name))) {
            return null;
        }

        $resource = null;
        $privilege = null;
        if (!empty($o['acl']) && is_array($o['acl'])) {
            if (2 == count($o['acl'])) {
                list($resource, $privilege) = $o['acl'];
            } else {
                list($resource, ) = $o['acl'];
            }
        }

        if (null !== $permissionFn) {
            if (true !== call_user_func($permissionFn, $resource, $privilege ?: 'default')) {
                return null;
            }
        } else {
            if (null === self::$_aclCheckPermissionCallbackFn) {
                throw new OSDN_Exception('Unable to find permission callback');
            }

            if (true !== call_user_func(self::$_aclCheckPermissionCallbackFn, $resource, $privilege ?: 'default')) {
                return null;
            }
        }

        $dashToCamelCase = new Zend_Filter_Word_DashToCamelCase();
        $blockCls = $dashToCamelCase->filter($block['module']);

        $blockCls .= '_Block_';

        /**
         * @todo Make optimization
         */

        $nameCamelCase = $dashToCamelCase->filter($name);
        $blockCls .= $nameCamelCase;

        /**
         * @todo Implement logger for production
         */
        if (!class_exists($blockCls)) {
            throw new OSDN_Exception(sprintf('Unable to find block "%s"', $blockCls));
        }

        $information = $this;
        if (0 !== strcasecmp($block['module'], $this->getName())) {
            $mm = Module_Instance_Manager::getInstance();
            $information = $mm->fetch($block['module']);
        }

        $blockObj = new $blockCls($information, array(
            'params' => $args
        ) + $o);

        if (! $blockObj instanceof Application_Block_Abstract) {
            throw new OSDN_Exception(sprintf(
                'The "%s" must be instance of Application_Block_Abstract class.', $blockCls
            ));
        }

        return $blockObj;
    }

    public function getBlocks($module, array $keywords, array $args = array())
    {
        $output = array();
        foreach($this->_blocks['internals'] as $blockName => $block) {

            $vRelation = false;
            foreach($block['relations'] as $relation) {
                if (0 === strcasecmp($module, $relation['module'])) {
                    $vRelation = $relation;
                    break;
                }
            }

            if (false === $vRelation) {
                continue;
            }

            $vKeyword = false;
            foreach($vRelation['keywords'] as $keyword) {
                if (in_array($keyword['name'], $keywords)) {
                    $vKeyword = $keyword;
                    break;
                }
            }

            if (false === $vKeyword) {
                continue;
            }

            $vMappings = $vKeyword['mappings'];


            /**
             * @FIXME
             *
             * Change block name
             */
            $bName = $block['parent'] ?: $block['name'];
            if (null !== ($blockObj = $this->getBlock($bName, $args, array(

                'title'     => $vRelation['title'],
                'parent'    => $block['parent'],
                'mappings'  => $vMappings,
                'priority'  => $vRelation['priority'],
                'acl'       => $vRelation['acl']
            )))) {
                $output[] = $blockObj;
            }
        }

        if (0 !== strcasecmp($module, $this->getName())) {
            return $output;
        }

        $mm = Module_Instance_Manager::getInstance();
        foreach($this->_blocks['relations'] as $nBlock => $relation) {

            $information = $mm->fetch($relation['module']);

            if (null !== ($blockObj = $information->getBlock($nBlock, $args, array(
                'mappings'    => $relation['mappings'],
                'priority'    => $relation['priority'],
                'title'       => $relation['title'],
                'acl'         => $relation['acl']
            )))) {
                $output[] = $blockObj;
            }
        }

        return $output;
    }

    /**
     * @return Module_Instance_Summary
     */
    public function getSummary()
    {
        if (null === $this->_summary) {
            $this->_summary = new Module_Instance_Summary($this);

            /**
             * @fixme
             */
            $this->_summary->setAcl(Zend_Auth::getInstance()->getIdentity()->getAcl());
        }

        return $this->_summary;
    }

    public function getAcl()
    {
        return $this->_acl;
    }

    public function getDb()
    {
        return $this->_db;
    }

    public function getMenu()
    {
        return $this->_menu;
    }

    protected function _read()
    {
        $f = $this->_path . '/configs/module.xml';
        if (true === ($this->_isMissing = !file_exists($f))) {
            return;
        }

        /**
         * @todo make default cache folder
         */
        if (! self::hasCache() && ! self::$_cacheDisabled) {
//            self::$_cache = Zend_Cache::factory(
//                'Core',
//                'File',
//                array('automatic_serialization' => true),
//                array()
//            );
        }

        $cacheInformationId = 'Module_' . strtr($this->getName(), array('-' => '_'));
        if (
            self::hasCache()
            && false !== ($result = self::getCache()->load($cacheInformationId))
        ) {
            foreach($result as $field => $value) {
                $this->{$field} = $value;
            }

            return $this;
        }

        $this->_nameCamelCase = $this->getNameCamelCase();

        /**
         * @todo Implement DTD or XMLSchema and
         *       make validation of xml
         */
        $xml = simplexml_load_file($f);

        if (!isset($xml['name']) || (string) $xml['name'] != $this->getName()) {
            throw new OSDN_Exception(sprintf(
                'Module name "%s" does not match with configuration name "%s"',
                $this->getName(), (string) $xml['name']
            ));
        }

        if (isset($xml['version'])) {
            $this->_version = (string) $xml['version'];
        }

        /**
         * @todo Remove hardcoded values
         */
        $this->_isProtected = 0 === stripos($this->getName(), 'core-')
            || 0 === strcasecmp($this->getName(), 'default');

        /**
         * Run throuth xml and fill object props
         */
        foreach($xml as $prop => $value) {

            switch($prop) {
                case 'title':
                    $this->_title = trim((string) $value);
                    break;

                case 'description':
                    $this->_description = trim((string) $value);
                    break;

                case 'dependences':
                    $this->_dependences = $this->_toDependences($value);
                    break;

                case 'database':
                    break;

                case 'acl':
                    $this->_acl = $this->_toAcl($value);
                    break;

                case 'db':
                    $this->_db = $this->_toDb($value);
                    break;

                case 'menu':
                    $this->_menu = $this->_toMenu($value);
                    break;

                default:
                    throw new OSDN_Exception(sprintf('Unknown option "%s" in module "%s"', $prop, $this->getName()));
            }
        }

        if (file_exists($this->_path . '/configs/block.xml')) {
            list($internals, $relations) = $this->_toBlocks(simplexml_load_file($this->_path . '/configs/block.xml'));
            $this->_blocks['internals'] = $internals;
            $this->_blocks['relations'] = $relations;
            unset($internals, $relations);
        }


        $service = new Module_Service_Module();
        $moduleRow = $service->findOneByName($this->getName());

        if (empty($moduleRow)) {
            $this->_isMissing = true;
            return $this;
        }

        $this->_isEnabled = (boolean) $moduleRow->isEnabled();

        $fcIni = $this->_path . '/configs/application.ini';
        if (file_exists($fcIni)) {
            $ini = new Zend_Config_Ini($fcIni, $this->getApplication()->getEnvironment());
            $this->setOptions($ini->toArray());
        }

        if (self::hasCache()) {
            $o = array();
            foreach($this->__sleep() as $field) {
                $o[$field] = $this->{$field};
            }

            self::getCache()->save($o, $cacheInformationId);
        }

        return $this;
    }

    protected function _toDependences(SimpleXMLElement $element)
    {
        $output = array();
        foreach($element as $module) {
            $output[] = array(
                'name'      => trim((string) $module['name']),
                'version'   => trim((string) $module['version'])
            );
        }

        return $output;
    }

    protected function _toBlocks(SimpleXMLElement $element)
    {
        $outputInternalBlocks = array();

        $blocks = null;
        if (count($element->xpath('internals/block')) > 0) {
            $blocks = $element->internals->block;
        } elseif(count($element->xpath('block'))) {
            $blocks = $element->block;
        }

        if (null !== $blocks) {
            foreach($blocks as $xBlock) {

                $blockModuleName = trim((string) $xBlock['module']) ?: $this->getName();
                $aBlock = array(
                    'name'      => trim((string) $xBlock['name']),
                    'parent'    => trim((string) $xBlock['parent']) ?: false,
                    'priority'  => (int) $xBlock['priority'],
                    'module'    => $blockModuleName,
                    'relations' => array(),
                    'resources' => array()
                );

                if (count($xBlock->xpath('relations/relation')) > 0) {

                    foreach($xBlock->relations->relation as $xRelation) {

                        $acl = array();
                        $aclResource = trim((string) $xRelation['acl']);
                        if (!empty($aclResource)) {

                            $aclResourceBreaks = explode(':', $aclResource);
                            if (3 == count($aclResource)) {
                                $acl[] = array_pop($aclResourceBreaks);
                                $acl[] = join(':', $aclResourceBreaks);
                            } else {
                                $acl[] = $aclResource;
                            }

                        } else {
                            $acl[] = $blockModuleName;
                        }

                        $aRelation = array(
                            'module'    => trim((string) $xRelation['module']),
                            'priority'  => (int) $xRelation['priority'],
                            'keywords'  => array(),
                            'title'     => trim((string) $xRelation->title),
                            'acl'       => $acl
                        );

                        if (0 == count($xRelation->xpath('keywords/keyword'))) {
                            continue;
                        }

                        foreach($xRelation->keywords->keyword as $xKeyword) {

                            $aKeyword = array(
                                'name'      => trim((string) $xKeyword['name']),
                                'mappings'  => array()
                            );

                            if (0 !== count($xKeyword->xpath('mappings/mapping'))) {
                                foreach($xKeyword->mappings->mapping as $xMapping) {
                                    $kFrom = trim((string) $xMapping['from']);
                                    $aKeyword['mappings'][] = array(
                                        'from'  => $kFrom,
                                        'to'    => trim((string) $xMapping['to']) ?: $kFrom,
                                        'value' => trim((string) $xMapping['value'])
                                    );
                                }
                            }

                            $aRelation['keywords'][] = $aKeyword;
                        }

                        $aBlock['relations'][] = $aRelation;
                    }
                }

                $outputInternalBlocks[$aBlock['name']] = $aBlock;
            }
        }

        $outputRelationsBlocks = array();

        return array($outputInternalBlocks, $outputRelationsBlocks);
    }

    protected function _toAcl(SimpleXMLElement $element)
    {
        $output = array();
        foreach($element as $xResource) {
            $oResource = array(
                'name'        => trim((string) $xResource['name']),
                'title'       => trim((string) $xResource->title),
                'description' => trim((string) $xResource->description),
                'privileges'  => array()
            );

            if (count($xResource->privileges->privilege) > 0) {
                 foreach($xResource->privileges->privilege as $xPrivilege) {
                    $oResource['privileges'][] = array(
                        'name'        => trim((string) $xPrivilege['name']),
                        'title'       => trim((string) $xPrivilege->title),
                        'description' => trim((string) $xPrivilege->description)
                    );
                }
            }

            $output[] = $oResource;
        }

        return $output;
    }

    protected function _toDb(SimpleXMLElement $element)
    {
        $output = array();
        foreach($element->table as $xTable) {
            $oTable = array(
                'name'            => trim((string) $xTable['name']),
                'ddbm'            => trim((string) $xTable['ddbm']) ?: false,
                'ddbmTableCls'    => trim((string) $xTable['ddbmTableCls']) ?: null,
            );

            $output[] = $oTable;
        }

        return $output;
    }

    /**
     * Process the kind for article connection
     *
     * @param SimpleXMLElement $element
     * @return array
     */
    protected function _toMenu(SimpleXMLElement $element)
    {
        $output = array();
        foreach($element->kind as $xKind) {
            $oKind = array(
                'name'            => trim((string) $xKind['name']),
                'title'           => trim((string) $xKind->title),
                'description'     => trim((string) $xKind->description) ?: null,
            );

            $output[] = $oKind;
        }

        return $output;
    }

    public function toArray()
    {
        return array(
            'name'        => $this->getName(),
            'title'       => $this->getTitle(),
            'description' => $this->getDescription(),
            'version'     => $this->getVersion(),
            'missing'     => $this->isMissing(),
            'enabled'     => $this->isEnabled(),
            'protected'   => $this->isProtected()
        );
    }

    /**
     * Returns the set cache
     *
     * @return Zend_Cache_Core The set cache
     */
    public static function getCache()
    {
        return self::$_cache;
    }

    /**
     * Returns true when a cache is set
     *
     * @return boolean
     */
    public static function hasCache()
    {
        return null !== self::$_cache;
    }

    /**
     * Removes any set cache
     *
     * @return void
     */
    public static function removeCache()
    {
        self::$_cache = null;
    }

    /**
     * Disables the cache
     *
     * @param boolean $flag
     */
    public static function disableCache($flag)
    {
        self::$_cacheDisabled = (boolean) $flag;
    }

    /**
     * Set a cache
     *
     * @param Zend_Cache_Core $cache A cache frontend
     */
    public static function setCache(Zend_Cache_Core $cache)
    {
        self::$_cache = $cache;
        self::_getTagSupportForCache();
    }

    /**
     * Internal method to check if the given cache supports tags
     *
     * @param Zend_Cache $cache
     */
    private static function _getTagSupportForCache()
    {
        $backend = self::$_cache->getBackend();
        if ($backend instanceof Zend_Cache_Backend_ExtendedInterface) {
            $cacheOptions = $backend->getCapabilities();
            self::$_cacheTags = $cacheOptions['tags'];
        } else {
            self::$_cacheTags = false;
        }

        return self::$_cacheTags;
    }

    public static function setOnAclCheckPermissionCallbackFn(Closure $fn)
    {
        if (null !== self::$_aclCheckPermissionCallbackFn) {
            throw new Exception('Callback is already set');
        }

        self::$_aclCheckPermissionCallbackFn = $fn;
    }
}