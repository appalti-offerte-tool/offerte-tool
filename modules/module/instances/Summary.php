<?php

final class Module_Instance_Summary
{
    /**
     * Information instance
     *
     * @var Module_Instance_Information
     */
    protected $_information;

    /**
     * Contain parsed summary configs
     *
     * @var array
     */
    protected $_configuration = array();

    /**
     * @var Zend_Acl
     */
    protected $_acl;

    /**
     * Initialize summary
     *
     * Parse configuration file
     * Initialize summary objects
     *
     * @param Module_Instance_Information $information
     */
    public function __construct(Module_Instance_Information $information)
    {
        $this->_information = $information;

        $filename = $this->_information->getRoot() . '/configs/summary.xml';
        if ( !file_exists($filename) || !Zend_Loader::isReadable($filename)) {
            return;
        }

        $xml = simplexml_load_file($filename);

        if (count($xml->xpath('groups/group')) > 0) {
            foreach($xml->groups->group as $xGroup) {

                $id = (string) $xGroup['id'];
                if (isset($this->_configuration[$id])) {
                    throw new OSDN_Exception(sprintf('The group "%s" is already exists in collection', $id));
                }

                $this->_configuration[$id] = array(
                    'id'      => $id,
                    'name'    => trim((string) $xGroup['name']) ?: null,
                    'acl'     => trim((string) $xGroup['acl']) ?: null,
                    'title'   => trim((string) $xGroup->title),
                    'item'    => null,
                    'items'   => array()
                );

                if (count($xGroup->xpath('items/item')) > 0) {
                    foreach($xGroup->items->item as $xItem) {
                        $this->_configuration[$id]['items'][] = $this->_toXItem($xItem);
                    }
                }
            }
        }

        if (count($xml->xpath('items/item'))) {

            $i = 0;
            foreach($xml->items->item as $xItem) {

                $this->_configuration[$i] = array(
                    'id'      => trim((string) $xItem['group']),
                    'acl'     => trim((string) $xItem['acl']),
                    'items'   => array()
                );
                $this->_configuration[$i]['items'][] = $this->_toXItem($xItem);

                $i++;
            }
        }
    }

    protected function _toXItem(SimpleXMLElement $xItem)
    {
        $o = array(
            'name'   => trim((string) $xItem['name']),
            'acl'    => trim((string) $xItem['acl']) ?: null,
            'title'  => trim((string) $xItem->title)
        );

        return $o;
    }

    public function setInformation(Module_Instance_Information $information)
    {
        $this->_information = $information;
        return $this;
    }

    public function setAcl(Zend_Acl $acl)
    {
        $this->_acl = $acl;
        return $this;
    }

    public function __sleep()
    {
        return array('_configuration');
    }

    protected function _toObject(array $item)
    {
        $dashToCamelCase = new Zend_Filter_Word_DashToCamelCase();

        $summaryCls = $this->_information->getNameCamelCase() . '_Summary_';
        $summaryCls .= $dashToCamelCase->filter($item['name']);

        $summaryObj = new $summaryCls($this);

        return $summaryObj;
    }

    /**
     * @todo Make optimization and rebuild resource parsing
     */
    protected function _isAllowed($aclResource)
    {
        $resource = null;
        $privilege = null;

        $aclResourceBreaks = explode(':', $aclResource);
        if (3 == count($aclResourceBreaks)) {
            $privilege = array_pop($aclResourceBreaks);
            $resource = join(':', $aclResourceBreaks);
        } else {
            $resource = $aclResource;
        }

        if (!$this->_acl->has($resource)) {
            return false;
        }

        $role = Zend_Auth::getInstance()->getIdentity()->getRoleId();
        return $this->_acl->isAllowed($role, $resource, $privilege ?: 'default');

    }

    public function toArray()
    {
        $gOutput = array();

        foreach($this->_configuration as $gId => $gOptions) {

            if (empty($gOptions['acl']) || !$this->_isAllowed($gOptions['acl'])) {
                continue;
            }

            if (!empty($gOptions['name'])) {

                $gOptions['item'] = $this->_toObject(array(
                    'name'   => $gOptions['name'],
                    'acl'    => $gOptions['acl']
                ));
            }
            $gOutput[$gId] = $gOptions;

            foreach($gOptions['items'] as $k => $item) {
                if (empty($item['acl']) || !$this->_isAllowed($item['acl'])) {
                    unset($gOptions['items'][$k]);
                    continue;
                }

                $gOutput[$gId]['items'][$k] = $this->_toObject($item);
            }
        }

        return $gOutput;
    }
}