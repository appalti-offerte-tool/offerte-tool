<?php

class Module_Model_Installation extends OSDN_Application_Service_Abstract
{
    /**
     * @var Module_Instance_Information
     */
    protected $_information;

    protected $_model;

    public function __construct(Module_Instance_Information $information)
    {
        $this->_information = $information;

        $this->_read();
    }

    public function run()
    {
        if ( ! $this->isInstalled()) {
            $this->_install();
        }

        $installedVersion = $this->getInstalledVersion();
        if ($this->_information->getVersion() > $installedVersion) {
            $this->_update($installedVersion);
        }
    }

    public function getInstalledVersion()
    {
        $version = 0;

        $row = $this->_model->fetchByName($this->_moduleName);
        if (!empty($row)) {
            $version = (float) $version;
        }

        return $version;
    }

    protected function _install()
    {
        $this->_model->create($this->_information->toArray());
    }

    protected function _update($currentVersion)
    {
        $files = array();
        foreach(new DirectoryIterator(
            $this->_information->getRoot() . DIRECTORY_SEPARATOR . 'installation'
        ) as $d) {

            if ($d->isDot() || $d->isDir()) {
                continue;
            }

            $filename = $d->getFilename();
            if (!preg_match('~\.(php|sql)$~', $filename, $matches)) {
                continue;
            }

            $extension = $matches[1];

            $files[] = array(
                'name'      => $filename,
                'extension' => $extension
            );
        }

        usort($files, function($a, $b) {

            $f1 = (float) $a['name'];
            $f2 = (float) $b['name'];

            if ($f1 < $f2) {
                return -1;
            }

            if ($f1 > $f2) {
                return 1;
            }

            return 0;
        });

        var_dump($files);
    }

    /**
     * @todo think about delete method
     */
    protected function _delete()
    {}

    protected function _read()
    {
        $xml = simplexml_load_file($this->_information->getRoot() . '/configs/database.xml');
        var_dump($xml);
    }
}