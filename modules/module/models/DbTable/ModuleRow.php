<?php

class Module_Model_DbTable_ModuleRow extends Zend_Db_Table_Row_Abstract
{
    public function isEnabled()
    {
        return (boolean) $this->enabled;
    }

    public function isProtected()
    {
        return (boolean) $this->protected;
    }

    public function setProtected($flag)
    {
        $this->protected = (int) $flag;
    }

    public function setEnabled($flag)
    {
        $this->enabled = (int) $flag;
    }
}