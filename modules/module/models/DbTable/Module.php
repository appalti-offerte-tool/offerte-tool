<?php

class Module_Model_DbTable_Module extends OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name    = 'module';

    protected $_rowClass = 'Module_Model_DbTable_ModuleRow';
}