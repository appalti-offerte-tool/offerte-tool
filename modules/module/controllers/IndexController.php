<?php

class Module_IndexController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var Module_Instance_Manager
     */
    protected $_moduleManager;

    /**
     * @var Module_Service_Module
     */
    protected $_service;

    public function init()
    {
        parent::init();

        $bootstrap = $this->getFrontController()->getParam('bootstrap');
        $bootstrap->bootstrap('ModulesManager');
        $this->_moduleManager = $bootstrap->getResource('ModulesManager');

        $this->_service = new Module_Service_Module();

        $this->_helper->ajaxContext()
            ->addActionContext('index', 'json')
            ->addActionContext('fetch-all', 'json')
            ->initContext();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'module';
    }

    public function indexAction()
    {

    }

    public function fetchAllAction()
    {
        $rowset = array();
        foreach($this->_moduleManager->fetchAll() as $row) {
            $rowset[] = $row->toArray();
        }

        $this->view->rowset = $rowset;
        $this->view->total = count($rowset);

//        $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
//        $response->setRowsetCallback(function($row) {
//            var_dump(get_class($row));
//            return $row->toArray();
//        });
//
//        $this->view->assign($response->toArray());
    }

    public function enableAction()
    {
        $name = $this->_getParam('name');

        try {
            $this->_service->toggleEnabledState($name, true);
            $this->_moduleManager->walkAndRegisterAccesibleModules();
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }

        $this->_helper->redirector->gotoSimpleAndExit('index');
    }

    public function disableAction()
    {
        $name = $this->_getParam('name');

        try {
            $result = $this->_service->toggleEnabledState($name, false);
            $this->_moduleManager->walkAndRegisterAccesibleModules();
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }

        $this->_helper->redirector->gotoSimpleAndExit('index');
    }

    public function findAndRegisterModulesAction()
    {
        try {
            $this->_moduleManager->walkAndRegisterAccesibleModules();
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }

        $this->_helper->redirector->gotoSimpleAndExit('index');
    }

    public function installAction()
    {
        /**
         * @todo change to param
         * @var unknown_type
         */
        $name = $this->_getParam('name', 'default');

//        $installationModel = new CoreModule_Model_Installation();
//        $installationModel->run();
    }

    public function deleteAction()
    {
        $name = $this->_getParam('name');

        try {
            $this->view->success = (boolean) $this->_service->delete($name);
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }

        $this->_helper->redirector->gotoSimpleAndExit('index');
    }
}