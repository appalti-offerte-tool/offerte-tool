<?php

class Module_SummaryController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{

    private $_dashToCamelCase;

    private function _getSummaryClassInstance()
    {
        $summaryInstance = $this->_getParam('si');
        $summaryModule = $this->_getParam('sm');

        if (!empty($summaryInstance) && !empty($summaryModule)) {
            $summaryCls = $this->_dashToCamelCase->filter($summaryModule) . '_Summary_' . $this->_dashToCamelCase->filter($summaryInstance);
        }

        return !empty($summaryCls) ? new $summaryCls() : null;
    }

    public function init()
    {
        parent::init();

        $this->_dashToCamelCase = new Zend_Filter_Word_DashToCamelCase();

        $this->_helper->ajaxContext()
            ->addActionContext('get', 'json')
            ->addActionContext('disable', 'json')
            ->initContext();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'guest';
    }

    public function getAction()
    {
        $summary = $this->_getSummaryClassInstance();

        if (!empty($summary)) {
            $result = array(
                'success'   => true,
                'href'      => $summary->getHref(),
                'title'     => $summary->getTitle(),
                'value'     => $summary->getValue(),
                'module'    => $summary->getModuleName(),
                'instance'  => $summary->getName(),
            );
        } else {
            $result = array(
                'success'   => false,
                'error'     => 'Invalid summary class name'
            );
        }

        $this->view->assign($result);
    }

    public function disableAction()
    {

    }
}