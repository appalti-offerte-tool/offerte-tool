<?php

final class Module_ConfigurationController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * Module information
     *
     * @var Module_Instance_Information
     */
    protected $_information;

    public function init()
    {
        parent::init();

        $this->_helper->layout->setLayout('administration');

        $mm = $this->getInvokeArg('bootstrap')->getResource('ModulesManager');
        if (null === ($this->_information = $mm->fetch($this->_getParam('m')))) {
            throw new OSDN_Exception('Unable to find module');
        }

        $this->view->headTitle($this->_information->getTitle())->append(' (' . $this->_information->getVersion() . ')');
    }

    public function getResourceId()
    {
        return 'module:configuration';
    }

    public function indexAction()
    {
        $this->view->module = $this->_information;
    }

    public function emailTemplateAction()
    {
        $this->view->module = $this->_information;
        $this->view->ptemplate = $this->_getParam('ptemplate');
    }
}