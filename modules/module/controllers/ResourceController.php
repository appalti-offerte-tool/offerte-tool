<?php

class Module_ResourceController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    public function init()
    {
        $this->_helper->layout->disableLayout(true);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'guest';
    }

    public function indexAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/javascript');
        $this->getResponse()->setHeader('Cache-Control', 'public, no-cache');
    }
}