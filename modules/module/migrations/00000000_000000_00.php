<?php

class Module_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('module');

        $this->createColumn('module', 'name', Core_Migration_Abstract::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('module', 'version', Core_Migration_Abstract::TYPE_VARCHAR, 10, null, true);
        $this->createColumn('module', 'title', Core_Migration_Abstract::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('module', 'description', Core_Migration_Abstract::TYPE_TEXT, 255, null, false);
        $this->createColumn('module', 'protected', Core_Migration_Abstract::TYPE_INT, 1, null, true);
        $this->createColumn('module', 'enabled', Core_Migration_Abstract::TYPE_INT, 1, null, true);
    }

    public function down()
    {
        $this->dropTable('module');
    }
}