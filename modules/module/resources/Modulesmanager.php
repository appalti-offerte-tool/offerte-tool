<?php

class Module_Resource_Modulesmanager extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        $bootstrap = $this->getBootstrap();
        $bootstrap->bootstrap('FrontController');
        $fc = $bootstrap->getResource('FrontController');
        $fc->setParam('bootstrap', $bootstrap);

        $autoloader = $bootstrap->getApplication()->getAutoloader();
        $attachAdditionalResourceToAutoloaderFn = function($root, $name, $nameCamelCase) use ($fc, $autoloader) {

            $fc->addControllerDirectory(
                $root . DIRECTORY_SEPARATOR . $fc->getModuleControllerDirectoryName(),
                $name
            );

            $ma = new Zend_Application_Module_Autoloader(array(
                'namespace' => $nameCamelCase,
                'basePath'  => $root
            ));

            $ma->addResourceType('controllers-plugins', 'controllers/plugins', 'Controller_Plugin');
            $ma->addResourceType('controllers-helpers', 'controllers/helpers', 'Controller_Helper');
            $ma->addResourceType('repository', 'models/repositories', 'Model_Repository');
            $ma->addResourceType('behaviours', 'models/behaviours', 'Model_Behaviour');
            $ma->addResourceType('block', 'blocks', 'Block');
            $ma->addResourceType('summary', 'summaries', 'Summary');
            $ma->addResourceType('instance', 'instances', 'Instance');
            $ma->addResourceType('miscs', 'misc', 'Misc');
            $ma->addResourceType('misc-emails', 'misc/emails', 'Misc_Email');
            $ma->addResourceType('misc-validators', 'misc/validators', 'Misc_Validator');
            $ma->addResourceType('misc-navigations', 'misc/navigations', 'Misc_Navigation');

            $autoloader->pushAutoloader($ma);
        };

        $attachAdditionalResourceToAutoloaderFn(
            APPLICATION_PATH . '/modules/module',
            'module',
            'Module'
        );

        $attachAdditionalResourceToAutoloaderFn(
            APPLICATION_PATH . '/modules/application',
            'application',
            'Application'
        );

        $fileCache = null;
        if ($bootstrap instanceof Zend_Application_Bootstrap_ResourceBootstrapper
            && $bootstrap->hasPluginResource('CacheManager')
        ) {
            $cacheManager = $bootstrap->bootstrap('CacheManager')->getResource('CacheManager');
            if (null !== $cacheManager && $cacheManager->hasCache('filecache')) {
                $fileCache = $cacheManager->getCache('filecache');
            }
        }

        $options = $this->getOptions();
        $moduleDirectories = array();
        if (is_array($options['paths'])) {
            $moduleDirectories = $options['paths'];
        }

        $mm = new Module_Instance_Manager($moduleDirectories);
        if (null !== $fileCache) {
            $mm->setCache($fileCache);
        }

        /**
         * @todo Make custom autoloader for module
         *      It will be more faster
         */
        foreach($mm->fetchAll() as $information) {
            $attachAdditionalResourceToAutoloaderFn(
                $information->getRoot(),
                $information->getName(),
                $information->getNameCamelCase()
            );
        }

        return $mm;
    }
}