<?php

class Module_Instance_SummaryTest extends PHPUnit_Framework_TestCase
{
    protected $_path;

    public function setUp()
    {
        $this->_path = APPLICATION_PATH . '/modules-components/worklocation/';
        Module_Instance_Information::disableCache(true);
        Module_Instance_Information::removeCache();
    }

    public function testInitBlocks()
    {
        $information = new Module_Instance_Information($this->_path);

        $summary = $information->getSummary();

        var_dump($summary->toArray());
    }
}