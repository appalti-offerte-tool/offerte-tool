<?php

class Module_Instance_InformationTest extends PHPUnit_Framework_TestCase
{
    protected $_path;

    public function setUp()
    {
        $this->_path = __DIR__ . '/../_files/core-module';
        Module_Instance_Information::disableCache(true);
        Module_Instance_Information::removeCache();
    }

    public function testCreateInformationWithThrowingException()
    {
        try {
            new Module_Instance_Information(__DIR__ . '/../_files/does-not-existing-module');
        } catch(OSDN_Exception $e) {
            $this->assertRegExp('/^The following path does not exists:/', $e->getMessage());
        }

        try {
            new Module_Instance_Information(__DIR__ . '/../_files/unknown-module');
        } catch(OSDN_Exception $e) {
            $this->assertRegExp('/^Module name ".+" does not match with configuration name ".+"/', $e->getMessage());
        }
    }

    public function testCacheSetUp()
    {
        Module_Instance_Information::disableCache(false);
        $this->assertFalse(Module_Instance_Information::hasCache());

        $cache = Zend_Cache::factory(
            'Core',
            'File',
            array('automatic_serialization' => true),
            array()
        );

        Module_Instance_Information::setCache($cache);
        $this->assertTrue(Module_Instance_Information::hasCache());
        $this->assertEquals(Module_Instance_Information::getCache(), $cache);

        Module_Instance_Information::removeCache();
        $this->assertFalse(Module_Instance_Information::hasCache());
    }

    public function testCacheSaveOrLoad()
    {
        $information = new Module_Instance_Information($this->_path);
    }

    public function testFullfillmentOfInformationWithSimpleProps()
    {
        $information = new Module_Instance_Information($this->_path);
        $this->assertTrue($information->isProtected());
        $this->assertTrue($information->isEnabled());
        $this->assertFalse($information->isMissing());

        $this->assertEquals($information->getName(), 'core-module');
        $this->assertEquals($information->getNameCamelCase(), 'CoreModule');
        $this->assertEquals($information->getTitle(), 'Information test');
        $this->assertEquals($information->getDescription(), 'Information description');
        $this->assertEquals($information->getVersion(), '0.1');

        $this->assertEquals($information->getDependences(), array(
            array(
                'name'      => 'module1',
                'version'   => '0.1'
            ),
            array(
                'name'      => 'module2',
                'version'   => '0.2'
            )
        ));

        require_once __DIR__ . '/../_files/core-module/blocks/Block1.php';
        require_once __DIR__ . '/../_files/core-module/blocks/Block3.php';

//        $relatedBlocks = $information->getRelatedBlocks('default', 'list', array('id' => 1));
//        $this->assertEquals(2, count($relatedBlocks));
//
//        $this->assertEquals(array(
//            new CoreModule_Block_Block1(array('id' => 1), $information),
//            new CoreModule_Block_Block3(array('id' => 1), $information),
//        ), $relatedBlocks);
    }
}