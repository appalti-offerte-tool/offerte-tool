<?php

class CoreModule_Block_Block3 extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return 'block3';
    }
    
    protected function _toHtml()
    {
        return 'block3 html';
    }
}