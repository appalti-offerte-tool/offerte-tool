<?php

class CoreModule_Block_Block1 extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return 'block1';
    }
    
    protected function _toHtml()
    {
        return 'block1 html';
    }
}