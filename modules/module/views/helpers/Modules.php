<?php

class Module_View_Helper_Modules extends Zend_View_Helper_Abstract
{
    protected $_module;

    public function modules($module = "")
    {
        $this->_module = $module;
        return $this;
    }

    public function getRoot()
    {
        $moduleDirectoryAbsolute = Zend_Controller_Front::getInstance()->getModuleDirectory($this->_module);

        if (empty ($moduleDirectoryAbsolute)) {
            return "";
        }

        if (false === ($path = realpath($moduleDirectoryAbsolute))) {
            return "";
        }

        return $path;
    }

    public function getRootWww()
    {
        $path = strstr($this->getRoot(), $this->_module, true);
        if (empty($path)) {
            return "";
        }

        $parentModuleDirectory = basename($path);
        return '/' . $parentModuleDirectory . '/' . $this->_module;
    }

    public function getResourcesPath($theme = 'default')
    {
        return $this->getRootWww() . '/views/scripts/' . $theme . '/_resources';
    }

    public function sandbox(Closure $callback)
    {
        $result = null;

        /**
         * View object must be very clean
         * Because it may render different template
         */
        $view = clone $this->view;

        /**
         * @todo
         * @var Make some optimization
         */
        $dashToCamelCase = new Zend_Filter_Word_DashToCamelCase();
        $moduleNameCamelCase = $dashToCamelCase->filter($this->_module);

        $isPrefixLoaded = false;
        if (false !== ($path = realpath($this->getRoot() . '/views/helpers'))) {
            $prefix = $moduleNameCamelCase . '_View_Helper_';
            if (false === ($isPrefixLoaded = $view->getPluginLoader('helper')->getPaths($prefix))) {
                $view->addHelperPath($path, $prefix);
            }
        }

        /**
         * @todo Do we need other themes here?
         */
        $view->addScriptPath($this->getRoot() . '/views/scripts/default');
        $view->addScriptPath($this->getRoot() . '/views/blocks/default');

        $result = call_user_func($callback, $view);

        if (false !== $path && false !== $isPrefixLoaded) {
            $view->getPluginLoader('helper')->clearPaths($prefix);
        }

        unset($view);
        return $result;
    }

    public function partial($name, $model = null)
    {
        return $this->sandbox(function($view) use ($name, $model) {

            if (is_object($model)) {
                $view->assign(get_object_vars($model));
            } elseif (is_array($model)) {
                $view->assign($model);
            }

            return $view->render($name);
        });
    }

    public function helper($name, array $args = array())
    {
        return $this->sandbox(function($view) use ($name, $args) {
            return call_user_func_array(array($view, $name), $args);
        });
    }

    public function __call($method, array $args = array())
    {
        return $this->helper($method, $args);
    }
}