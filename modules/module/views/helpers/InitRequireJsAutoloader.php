<?php

class Module_View_Helper_InitRequireJsAutoloader extends Zend_View_Helper_Abstract
{
    public function initRequireJsAutoloader()
    {
        return $this;
    }

    public function collectResourcePaths()
    {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $cm = $bootstrap->getResource('Cachemanager');
        $filecache = $cm->getCache('filecache');
        OSDN_View_Bind::setCache($filecache);

        $cacheBindViewModuleId = 'bind_view_module_cache';
        if (false === ($includes = $filecache->load($cacheBindViewModuleId))) {

            $output = array();
            $mm = $bootstrap->getResource('ModulesManager');
            foreach($mm->fetchAll() as $information) {
                $resourceFile = $information->getRoot() . '/configs/resource.xml';
                if (!file_exists($resourceFile)) {
                    continue;
                }

                $bind = new OSDN_View_Bind($resourceFile);
                $output[] = $bind->toArray();

            }

            $includes = call_user_func_array('array_merge_recursive', $output);
            $filecache->save($includes, $cacheBindViewModuleId, array('osdn_view_bind'), 3600);

        }

        return $includes;
    }

    public function collectResourcePathsInJson()
    {
        return Zend_Json::encode($this->collectResourcePaths());
    }

    public function __toString()
    {
        return <<<HTML
<script type="text/javascript" src="/public/js/library/LAB.min.js"></script>
<script type="text/javascript" src="/module/resource/index"></script>
HTML;
    }
}