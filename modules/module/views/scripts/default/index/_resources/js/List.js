Ext.define('Module.Module.Model.Module', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name', 'title', 'description', 'version',
        {name: 'protected', type: 'boolean'},
        {name: 'enabled', type: 'boolean'},
        {name: 'missing', type: 'boolean'}
    ],
});

Ext.define('Module.Module.List', {
    extend: 'Ext.ux.grid.GridPanel',
    alias: 'module.module.list',

    stateful: false,

    cls: 'm-module',

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.Module.Model.Module',
            proxy: {
                type: 'ajax',
                url: link('module', 'index', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            },
            remoteSort: {
                sort: 'name',
                dir: 'asc'
            }
        });

        this.columns = [{
            header: lang('Title'),
            width: 200,
            dataIndex: 'title',
            sortable : true
        }, {
            header: lang('Raw name'),
            width: 100,
            dataIndex: 'name'
        }, {
            header: lang('Version'),
            width: 50,
            dataIndex: 'version',
        }, {
            header: lang('Description'),
            dataIndex: 'description',
            flex: 1
        }, {
            header: lang('Protected'),
            xtype: 'actioncolumn',
            dataIndex: 'protected',
            align: 'center',
            width: 60,
            items: [{
                getClass: function(v, md, record) {
                    return 'icon-16 protected-' + (record.get('protected') ? 'on' : 'off');
                },
                scope: this
            }]
        }, {
            header: lang('Enabled'),
            xtype: 'actioncolumn',
            dataIndex: 'enabled',
            align: 'center',
            renderer: function(v,  md, record) {
                if (record.get('missing')) {
                    md.style += md.style + 'background: orange;';
                }
                return "";
            },
            width: 60,
            items: [{
                getClass: function(v, md, record) {
                    return 'icon-16 ' + (record.get('enabled') ? 'active' : 'inactive');
                },
                handler: this.onToggleEnabledState,
                scope: this
            }]

        }, {
            xtype: 'actioncolumn',
            width: 60,
            header: lang('Actions'),
            fixed: true,
            items: [{
                tooltip: lang('Edit'),
                getClass: function(v) {
                    return 'icon-edit-16 icon-16';
                },
                handler: this.onEditAccount,
                scope: this
            }, {
                tooltip: lang('Delete'),
                iconCls: 'icon-delete-16 icon-16',
                handler: this.onDeleteAccount,
                scope: this
            }]
        }];

        this.features = [{
            ftype: 'filters'
        }];

        this.plugins = [Ext.create('Ext.ux.grid.Search', {
            minChars: 2,
            align: 0,
            stringFree: true
        })];

        this.tbar = ['-', {
            text: lang('Find and register new modules'),
            href: link('module', 'index', 'find-and-register-modules'),
            target: '_self'
        }];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent(arguments);
    }
});