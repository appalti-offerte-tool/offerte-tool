<?php

class Module_Service_Module extends OSDN_Application_Service_Dbable
{
    /**
     * @var Module_Model_DbTable_Module
     */
    protected $_table;

    protected function _init()
    {
        $this->_table = new Module_Model_DbTable_Module($this->getAdapter());

        parent::_init();

        $this->_attachValidationRules('default', array(
            'name'          => array(
                array('StringLength', 2, 30),
                array('Regex', '/^[a-z][a-z-]+[a-z]$/'),
                'presence'  => 'required',
                'messages'  => array(
                    0 => 'Name is not correct, must be minimum 2 characters',
                    1 => 'Name is not correct "%value%" (can contain only alnum)'
                 )
            ),
            /**
             * @todo Add version validator
             */
            'version'       => array('presence' => 'required'),
            'title'         => array(array('StringLength', 1, 30), 'presence' => 'required'),
            'description'   => array('allowEmpty' => true)
        ));
    }

    /**
     * Retrieve rowset collection
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function fetchAll()
    {
        return $this->_table->fetchAll(array(), array('name ASC'));
    }

    /**
     * @return OSDN_Db_Response_Abstract
     */
    public function fetchAllWithResponse(array $params = array())
    {
        $statement = $this->_table->select(true);
        if (!empty($params)) {
            $this->_initDbFilter($statement, $this->_table)->parse($params);
        }

        $response = $this->getDecorator('response')->decorate($statement, $this->_table);
        return $response;
    }

    public function findOneByName($name)
    {
        return $this->_table->fetchRow(array('name = ?' => $name));
    }

    public function create(Module_Instance_Information $information)
    {
        $moduleRow = $this->_table->createRow();
        $moduleRow->name = $information->getName();
        $moduleRow->title = $information->getTitle();
        $moduleRow->description = $information->getDescription();
        $moduleRow->version = $information->getVersion();
        $moduleRow->setProtected(false);
        $moduleRow->setEnabled(true);

        return $moduleRow->save();
    }

    public function update($id, array $data)
    {
        $this->_attachValidationRules('update', array(
            'id'    => array('id', 'presence' => 'required')
        ), 'default');

        $data['id'] = $id;
        $f = $this->_validate($data, 'update');

        throw new Exception('not implemented');
        $affectedRows = $this->_table->updateByPk($data, $id);
        if (false === $affectedRows) {
            $this->_throw(sprintf('Unable to update module "%s"'));
        }

        return $affectedRows;
    }

    public function toggleEnabledState($name, $isEnabled)
    {
        $this->_validateField('name', $name);

        $affectedRows = $this->_table->updateQuote(array(
            'enabled'   => (int) $isEnabled
        ), array(
            'name = ?'  => $name
        ));

        if (false === $affectedRows) {
            throw new OSDN_Exception('Unable to toggle module state');
        }

        return true;
    }

    public function delete($name)
    {
        $affectedRows = $this->_table->deleteQuote(array(
            'name = ?'    => $name
        ));

        if (false === $affectedRows) {
            throw new OSDN_Exception('Unable to delete module');
        }

        return $affectedRows;
    }
}