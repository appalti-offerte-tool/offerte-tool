<?php

class Module_Service_SummaryConfiguration extends Configuration_Service_ConfigurationAbstract
{
    protected $_identity = 'summary.configuration';

    protected $_fields = array('disabled');
}