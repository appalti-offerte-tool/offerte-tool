<?php

class Configuration_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('configuration');

        $this->createColumn('configuration', 'param', Core_Migration_Abstract::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('configuration', 'en', Core_Migration_Abstract::TYPE_TEXT, null, null, true);
        $this->createColumn('configuration', 'nl', Core_Migration_Abstract::TYPE_TEXT, null, null, false);
        $this->createColumn('configuration', 'accountId', Core_Migration_Abstract::TYPE_INT, 11, null, false);

        $this->createUniqueIndexes('configuration', array('param'), 'UX_param');
        $this->createIndex('configuration', array('accountId'), 'IX_accountId');
        $this->createForeignKey('configuration', array('accountId'), 'account', array('id'), 'FK_accountId');

        foreach(array(
            array('roleId' => 1, 'resource' => 'configuration', 'privilege' => null),
        ) as $o) {
            $this->insert('aclPermission', $o);
        }
    }

    public function down()
    {
        $this->getDbAdapter()->delete('aclPermission', array(
            'roleId = 1',
            "(resource = 'configuration' OR resource LIKE 'configuration:%')"
        ));

        $this->dropTable('configuration');
    }
}