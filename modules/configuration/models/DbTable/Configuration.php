<?php

class Configuration_Model_DbTable_Configuration extends OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'configuration';
}