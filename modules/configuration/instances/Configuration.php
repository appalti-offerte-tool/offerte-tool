<?php

/**
 * Simple configuration instance
 * Allow simply retrieving configuration according to
 * language and account
 *
 * @version     $Id: Configuration.php 1889 2012-03-28 15:38:54Z yaroslav $
 */
class Configuration_Instance_Configuration
{
    /**
     * Configuration object
     *
     * @var OSDN_Configuration_Abstract
     */
    protected $_configuration;

    /**
     * Indicate if configuration is already loaded
     *
     * @var boolean
     */
    protected $_loaded = false;

    /**
     * Loaded configuration
     *
     * @var array
     */
    protected $_rowset = array();

    protected $_locale;

    /**
     * The constructor
     *
     * @param OSDN_Configuration_Abstract $configuration    Configuration object
     * @return void
     */
    public function __construct(Configuration_Service_ConfigurationInterface $configuration)
    {
        $this->_configuration = $configuration;
    }

    /**
     * Assemble text
     *
     * @param object $text
     * @param object $params
     * @return string
     */
    public function assemble($text, array $params)
    {
        if (!is_string($text)) {
            return "";
        }

        $matches = array();
        if (!preg_match_all('/\{[^}]+\}/i', $text, $matches)) {
            return $text;
        }

        foreach($matches[0] as $m) {
            $text = str_replace($m, strip_tags($m), $text);
        }

        $keywords = $this->_configuration->getRawKeywords();

        foreach($keywords as $keyword => $properties) {
            $e = array_key_exists($keyword, $params);
            if (is_array($properties) && isset($properties['convert']) && is_callable($properties['convert'])) {
                $value = call_user_func_array($properties['convert'], array($e ? $params[$keyword] : null, $params));
            } elseif ($e) {
                $value = $params[$keyword];
            } else {
                continue;
            }

            $text = str_replace('{' . $keyword . '}', $value, $text);
        }

        return $text;
    }

    /**
     * Allow configuration for multiple accounts
     *
     * @param boolean $flag
     * @return boolean
     */
    public function setAccountable($flag)
    {
        $this->_configuration->setAccountable($flag);
        return $this;
    }

    /**
     * Catch method before execution
     *
     * @param string $method
     * @param array $args
     *
     * @return string
     * @throws Exception
     */
    public function __call($method, $args)
    {
        $method = strtolower($method);
        if (!preg_match('/^get/i', $method)) {
            throw new Exception('You trying call undefined method "' . $method . '"');
        }

        $field = substr($method, 3);

        $locale = null;
        if (isset($args[1]) && is_string($args[1])) {
            $locale = $args[1];
        }

        $text = $this->get($field, $locale);
        if (null === $text) {
            return $text;
        }

        if (count($args) && is_array($args[0])) {
            $text = $this->assemble($text, $args[0]);
        }

        return $text;
    }

    /**
     * Retrieve configuration property
     * Support language locale
     *
     * @param string $field                 The configuration property
     * @param string $locale [optional]     Language locale, can be `en`, `nl`, etc.
     * @return
     */
    public function get($field, $locale = null)
    {
        if (!$this->_configuration->isAllowedField($field)) {
            return null;
        }

        if (true !== $this->_loaded) {
            $this->_rowset = $this->_configuration->getRowset();
            $this->_loaded = true;
        }

        $text = "";
        if ($this->_configuration->isAllowedMui()) {

            if (empty($locale)) {
                $locale = $this->_locale;
            }

            if (empty($locale)) {
                $locale = OSDN_Language::getDefaultLocale();
            }

            if (isset($this->_rowset[$field][$locale])) {
                $text = $this->_rowset[$field][$locale];
            }

            if ('en' != $locale && empty($text) && !empty($this->_rowset[$field]['en'])) {
                $text = $this->_rowset[$field]['en'];
            }

            return $text;
        }

        if (isset($this->_rowset[$field])) {
            $text = $this->_rowset[$field];
        }

        return $text;
    }

    /**
     * Catch property before retrieving
     * Try to retrive configuration
     * Locale and params are not supported
     *
     * @param string $field      The property name
     * @return string|null
     */
    public function __get($field)
    {
        return $this->get($field);
    }

    /**
     * Set default locale
     *
     * @param string $locale
     * @return OSDN_Configuration_Instance
     */
    public function setLocale($locale)
    {
        $this->_locale = $locale;
        return $this;
    }
}