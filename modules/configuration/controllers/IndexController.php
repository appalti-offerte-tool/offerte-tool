<?php

class Configuration_IndexController extends Zend_Controller_Action
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
             ->addActionContext('fetch', 'json')
             ->addActionContext('update', 'json')
             ->initContext();
    }

    public function fetchAction()
    {
        $module = 'order';
        $configurationCls = 'customer-confirmation';

        $dashToCamelCase = new Zend_Filter_Word_DashToCamelCase();
        $confirmationCls = $dashToCamelCase->filter($module) . '_Misc_Email_' . $dashToCamelCase->filter($configurationCls);
        if (!class_exists($confirmationCls)) {
            return $this->_helper->information('Unable to find configuration class');
        }

        $confirmation = new $confirmationCls();
        $confirmation->setAccountable(false);
        $keywords = $confirmation->getKeywords();

        $this->view->keywords = $keywords;
        $this->view->total = count($keywords);

        $this->view->rowset = array($confirmation->fetch());
        $this->view->success = true;
    }

    public function updateAction()
    {
        /**
         * @FIXME
         */
        $confirmation = new Order_Misc_Email_CustomerConfirmation();
        $confirmation->setAccountable(false);

//        $wrap = $this->_getParam('__wrap');
        $params = $this->_getAllParams();

//        if (!empty($wrap) && isset($params[$wrap]) && is_array($params[$wrap])) {
//            $params = $params[$wrap];
//        }

        $result = $confirmation->update($params);
        $this->view->success = (boolean) $result;
    }

//    order.customer-confirmation
}