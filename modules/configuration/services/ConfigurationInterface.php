<?php

interface Configuration_Service_ConfigurationInterface
{
    public function setAccountable($flag);

    public function getDefaultFieldValue($field, $initiatedValue = null);

    public function fetch();

    public function getRowset();

    public function getKeywords();

    public function getRawKeywords();
}