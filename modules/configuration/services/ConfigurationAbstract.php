<?php

/**
 * Abstract configuration object
 * Allow save, retrieve configurations with multilanguage and multiaccounts
 *
 * @version     $Id: ConfigurationAbstract.php 1889 2012-03-28 15:38:54Z yaroslav $
 */
abstract class Configuration_Service_ConfigurationAbstract implements \Configuration_Service_ConfigurationInterface
{
    /**
     * Default value retrieve mode
     * It will be replaced with default value from configuration
     *
     * ISSET	When value is not present
     * EMPTY	When value is empty (like "", "0", 0, etc)
     *
     * @var int
     */
    const FIELD_DVRM_ISSET = 1;
    const FIELD_DVRM_EMPTY = 2;

    /**
     * Configuration identity prefix
     *
     * @var string
     */
    protected $_identity;

    /**
     * Replecable keywords collection
     *
     * @var array
     */
    protected $_keywords = array();

    /**
     * Allowed fields collection
     *
     * @var array
     */
    protected $_fields = array();

    /**
     * Apply default values to the fields on isset or empty
     *
     * @var int
     */
    protected $_fieldsDefaultValueRetrieveMode = self::FIELD_DVRM_EMPTY;

    /**
     * Default model
     *
     * @var Configuration_Model_DbTable_Configuration
     */
    protected $_table;

    /**
     * Allow support multilanguage
     *
     * @var boolean
     */
    protected $_mui = false;

    /**
     * Allow support multiaccounts
     *
     * @var boolean
     */
    protected $_accountable = false;

    /**
     * Allow retrieve default value if not present for account
     * Working only $_accountable is true
     *
     * @var boolean
     */
    protected $_defaultable = false;

    /**
     * Account id
     *
     * @var int
     */
    protected $_accountId = null;

    /**
     * Translation object
     * @var Zend_Translate
     */
    protected $_translator;

    /**
     * Default translation object for all validate objects
     * @var Zend_Translate
     */
    protected static $_defaultTranslator;

    /**
     * Is translation disabled?
     * @var Boolean
     */
    protected $_translatorDisabled = false;

    private $__instance;

    private $__rowsetCached = null;


    /**
     * The constructor
     *
     * @param string $identity
     * @return void
     */
    public function __construct()
    {
        if (empty($this->_identity) || !is_string($this->_identity)) {
            throw new Exception('Identity must be string. The type "' . gettype($this->_identity) . '" found.');
        }

        $translation = null;
        if (!empty($this->_keywords) && null !== ($translation = $this->getTranslator())) {
            foreach($this->_keywords as $key => & $value) {
                if (!is_array($value)) {
                    $value = $translation->translate($value);
                } elseif (isset($value['caption'])) {
                    $value['caption'] = $translation->translate($value['caption']);
                }
            }
        }

        $this->_table = new Configuration_Model_DbTable_Configuration(
            OSDN_Application_Service_Dbable::getDefaultDbAdapter()
        );

        if ($this->_accountable) {
            if (!Zend_Auth::getInstance()->hasIdentity()) {
                throw new OSDN_Exception('Account is missed');
            }

            $this->_accountId = Zend_Auth::getInstance()->getIdentity()->getId();
        }

        $this->init();
    }

    /**
     * Extra functionality can be implemented
     * Executed after constructor created
     *
     * @return void
     */
    protected function init()
    {}

    /**
     * Set flag for allowing configuration for different accounts
     *
     * @param boolean $flag
     * @return OSDN_Configuration_Abstract
     */
    public function setAccountable($flag)
    {
        $this->_accountable = (boolean) $flag;
        return $this;
    }

    /**
     * Retrieve default value from fields model
     *
     * @param string $field
     * @return mixed|null if not found
     */
    public function getDefaultFieldValue($field, $initiatedValue = null)
    {
        if (
            ($this->_fieldsDefaultValueRetrieveMode === self::FIELD_DVRM_ISSET && !isset($initiatedValue)) ||
            ($this->_fieldsDefaultValueRetrieveMode === self::FIELD_DVRM_EMPTY && empty($initiatedValue))
        ) {
            $value = null;
            if (isset($this->_fields[$field]['default'])) {
                $value = $this->_fromValue($this->_fields[$field]['default'], $field);
            }

            return $value;
        }

        return $initiatedValue;
    }

    /**
     * Retrieve rowset of configuration according to identity
     * Format into string props with languages
     * <code>
     *      myprop[en] = testen
     *      myprop[nl] = testnl
     * </code>
     *
     * @return array
     */
    public function fetch()
    {
        $rowset = $this->getRowset();
        $output = array();

        if ($this->_mui) {
            foreach($rowset as $key => $value) {
                $output[$key . '[en]'] = $value['en'];
                $output[$key . '[nl]'] = $value['nl'];
            }

            $rowset = $output;
            unset($output);
        }

        return $rowset;
    }

    /**
     * Retrieve rowset of configuration according to identity
     *
     * @return array
     */
    public function getRowset()
    {
        if (null !== $this->__rowsetCached) {
            return $this->__rowsetCached;
        }

        $ids = array();
        foreach($this->_fields as $field => $value) {
            if (is_int($field)) {
                $field = $value;
            }

            $ids[] = $this->toIdentity($field);
        }

        $select = $this->_table->select()
            ->where('param IN(?)', $ids);

        if ($this->_accountable) {
            $select->where('accountId = ?', $this->_accountId);
        } else {
            $select->where('accountId IS NULL');
        }

        try {
            $query = $select->query();
        } catch(exception $e) {
            return array();
        }

        $rowset = array();
        while($row = $query->fetch()) {
            $rowset[$this->fromIdentity($row['param'])] = $row;
        }

        $defaultable = false;
        $rowsetDefault = array();

        $output = array();
        foreach($this->_fields as $field => $value) {

            if (is_int($field)) {
                $field = $value;
            }

            $en = isset($rowset[$field]['en']) ? $this->_fromValue($rowset[$field]['en'], $field) : null;
            $nl = isset($rowset[$field]['nl']) ? $this->_fromValue($rowset[$field]['nl'], $field) : null;

            if ($this->_accountable && $this->_defaultable) {

                if (
                    true !== $defaultable && (
                        !isset($en) ||
                        ($this->_mui && !isset($nl))
                    )
                ) {
                    $rowsetDefault = $this->_fetchDefaultConfiguration($ids);
                    $defaultable = true;
                }

                if (!isset($en) && isset($rowsetDefault[$field]['en'])) {
                    $en = $this->_fromValue($rowsetDefault[$field]['en'], $field);
                }

                if ($this->_mui && !isset($nl) && isset($rowsetDefault[$field]['nl'])) {
                    $nl = $this->_fromValue($rowsetDefault[$field]['nl'], $field);
                }
            }

            $en = $this->getDefaultFieldValue($field, $en);

            if ($this->_mui) {

                $nl = $this->getDefaultFieldValue($field, $value);

                $output[$field]['en'] = $en;
                $output[$field]['nl'] = $nl;
            } else {
                $output[$field] = $en;
            }
        }

        return $this->__rowsetCached = $output;
    }

    /**
     * Retrieve keywords collection
     *
     * @return array
     */
    public function getKeywords()
    {
        $keywords = array();
        foreach($this->_keywords as $key => $caption) {

            if (is_array($caption)) {
                if (isset($caption['caption'])) {
                    $caption = $caption['caption'];
                } else {
                    $caption = 'Property description is undefined';
                }
            }

            $keywords[] = array(
                'key'       => $key,
                'caption'   => $caption
            );
        }

        return $keywords;
    }

    /**
     * Retrieve keywords without convertion
     *
     * @return array
     */
    public function getRawKeywords()
    {
        return $this->_keywords;
    }

    /**
     * Update configuration
     *
     * @param array $params
     * @return boolean
     */
    public function update(array $params)
    {
        $fields = array();

        foreach($params as $field => $value) {
            if ($this->isAllowedField($field)) {
                if ($this->_mui && is_array($value)) {
                    foreach($value as $language => $v) {
                        if (in_array($language, array('en', 'nl'))) {
                            $fields[$field][$language] = $this->_toValue($v, $field);
                        }
                    }
                } else {
                    $fields[$field]['en'] = $this->_toValue($value, $field);
                }
            }
        }

        $result = array();
        if (!empty($fields)) {
            foreach($fields as $field => $data) {

                $f = $this->toIdentity($field);

                $clause = array('param = ?' => $f);
                if ($this->_accountable) {
                    $clause['accountId = ?'] = $this->_accountId;
                } else {
                    $clause['accountId IS NULL'] = null;
                }

                $count = $this->_table->count($clause, 1);

                if ($count > 0) {
                    $result[] = false !== $this->_table->update($data, $clause);
                } else {
                    $data['param'] = $f;

                    if ($this->_accountable) {
                        $data['accountId'] = $this->_accountId;
                    }

                    $result[] = (boolean) $this->_table->insert($data);
                }
            }
        }

        return !in_array(false, $result);
    }

    public function setParam($field, $value, $language = 'en')
    {
        $result = $this->update(array(
            $field => $this->_mui ? array($language => $value) : $value
        ));

        return $result;
    }

    /**
     * Add identity prefix to param
     *
     * @param string $param
     * @return string       The assembled identity to param
     */
    public function toIdentity($param)
    {
        return $this->_identity . '.' . $param;
    }

    /**
     * Strip identity prefix from param
     *
     * @param string $param
     * @return string       The striped param
     */
    public function fromIdentity($param)
    {
        return str_replace($this->_identity . '.', '', $param);
    }

    /**
     * Retrieve default configuration
     *
     * @param array $ids
     * @return array
     */
    protected function _fetchDefaultConfiguration(array $ids)
    {
        $clause = array(
            'param IN(?)' => $ids,
            'accountId IS NULL'
        );

        $rowset = $this->_table->fetchAll($clause);

        $output = array();
        foreach($rowset as $row) {
            $output[$this->fromIdentity($row['param'])] = array(
                'en'    => $row['en'],
                'nl'    => $row['nl']
            );
        }

        return $output;
    }

    /**
     * Check if field present in collection
     *
     * @param string $field
     * @return boolean
     */
    public function isAllowedField($field)
    {
        return is_string($field) && (
            in_array($field, $this->_fields) || array_key_exists($field, $this->_fields)
        );
    }

    /**
     * Check if allowed multilanguage
     *
     * @return boolean
     */
    public function isAllowedMui()
    {
        return $this->_mui;
    }

    /**
     * Set up convertable function to keyword
     *
     * @param string $property
     * @param mixed $fn     The callable function
     * @return
     */
    public function setKeywordConvertFn($property, $fn)
    {
        if (!is_callable($fn)) {
            return $this;
        }

        if (array_key_exists($property, $this->_keywords)) {

            $value = $this->_keywords[$property];
            if (is_string($value)) {
                $this->_keywords[$property] = array('caption' => $value);
                $value = $this->_keywords[$property];
            }

            $this->_keywords[$property]['convert'] = $fn;
        }

        return $this;
    }

    /**
     * @return Configuration_Instance_Configuration
     */
    public function toInstance()
    {
        if (null === $this->__instance) {
            $this->__instance = new Configuration_Instance_Configuration($this);
        }

        return $this->__instance;
    }

    /**
     * Convert value to specific format
     */
    protected function _toValue($value, $field)
    {
        return $value;
    }

    /**
     * Convert value from specific format
     */
    protected function _fromValue($value, $field)
    {
        return $value;
    }

    /**
     * Set translation object
     *
     * @param  Zend_Translate|Zend_Translate_Adapter|null $translator
     * @return Zend_Validate_Abstract
     */
    public function setTranslator($translator = null)
    {
        if ((null === $translator) || ($translator instanceof Zend_Translate_Adapter)) {
            $this->_translator = $translator;
        } elseif ($translator instanceof Zend_Translate) {
            $this->_translator = $translator->getAdapter();
        } else {
            require_once 'Zend/Validate/Exception.php';
            throw new Zend_Validate_Exception('Invalid translator specified');
        }
        return $this;
    }

    /**
     * Return translation object
     *
     * @return Zend_Translate_Adapter|null
     */
    public function getTranslator()
    {
        if ($this->translatorIsDisabled()) {
            return null;
        }

        if (null === $this->_translator) {
            return self::getDefaultTranslator();
        }

        return $this->_translator;
    }

    /**
     * Does this validator have its own specific translator?
     *
     * @return bool
     */
    public function hasTranslator()
    {
        return (bool)$this->_translator;
    }

    /**
     * Set default translation object for all validate objects
     *
     * @param  Zend_Translate|Zend_Translate_Adapter|null $translator
     * @return void
     */
    public static function setDefaultTranslator($translator = null)
    {
        if ((null === $translator) || ($translator instanceof Zend_Translate_Adapter)) {
            self::$_defaultTranslator = $translator;
        } elseif ($translator instanceof Zend_Translate) {
            self::$_defaultTranslator = $translator->getAdapter();
        } else {
            require_once 'Zend/Validate/Exception.php';
            throw new Zend_Validate_Exception('Invalid translator specified');
        }
    }

    /**
     * Get default translation object for all validate objects
     *
     * @return Zend_Translate_Adapter|null
     */
    public static function getDefaultTranslator()
    {
        if (null === self::$_defaultTranslator) {
            require_once 'Zend/Registry.php';
            if (Zend_Registry::isRegistered('Zend_Translate')) {
                $translator = Zend_Registry::get('Zend_Translate');
                if ($translator instanceof Zend_Translate_Adapter) {
                    return $translator;
                } elseif ($translator instanceof Zend_Translate) {
                    return $translator->getAdapter();
                }
            }
        }

        return self::$_defaultTranslator;
    }

    /**
     * Is there a default translation object set?
     *
     * @return boolean
     */
    public static function hasDefaultTranslator()
    {
        return (bool)self::$_defaultTranslator;
    }

    /**
     * Indicate whether or not translation should be disabled
     *
     * @param  bool $flag
     * @return Zend_Validate_Abstract
     */
    public function setDisableTranslator($flag)
    {
        $this->_translatorDisabled = (bool) $flag;
        return $this;
    }

    /**
     * Is translation disabled?
     *
     * @return bool
     */
    public function translatorIsDisabled()
    {
        return $this->_translatorDisabled;
    }
}