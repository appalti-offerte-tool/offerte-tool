Ext.define('Module.Configuration.Layout', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.module.configuration.layout',
    layout: 'border',

    defaults: {
        split: true,
        border: false
    },

    initComponent: function() {

        this.tree = new Module.Configuration.Tree({
            region: 'west',
            width: 200,
            cls: 'x-module-border-right'
        });

        this.collection = new Ext.Panel({
            region: 'center',
            cls: 'x-module-border-left',
            items: [],
            layout: 'card',
            defauts: {
                border: false
            },
            layoutConfig: {
                deferredRender: true,
                layoutOnTabChange: true
            }
        });

        this.items = [this.tree, this.collection];
        this.callParent(arguments);

        this.tree.getSelectionModel().on('selectionchange', this.doActivateLayout, this);

        this.tree.on('beforerefresh', function() {
            this.collection.removeAll();
        }, this);
    },

    showInWindow: function() {

        var w = new Ext.Window({
            title: lang('Configuration manager'),
            iconCls: 'module-configuration-icon-16x16',
            layout: 'fit',
            border: false,
            height: 550,
            width: 750,
            modal: true,
            items: [this]
        });

        w.show.apply(w, arguments);
        return w;
    },

    doActivateLayout: function(sm, node) {

        if (Module.empty(node)) {
            return;
        }

        var exists = false;
        var p = null;
        var xtype = node.attributes.xtype;

        this.collection.items.each(function(i) {
            if (i.id == xtype) {
                exists = true;
                p = i;
                return true;
            }
        });

        if (true !== exists) {

            p = this.collection.add({
                border: false,
                xtype: xtype,
                pid: node.id
            });
        }

        this.collection.getLayout().setActiveItem(p);
        if (p instanceof Ext.util.Observable) {
            p.fireEvent('activate', p);
        }
    }
});