Ext.define('Module.Configuration.Tree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.module.configuration.tree',

    rootVisible: false,

    autoScroll: true,

    initComponent: function() {

        this.root = new Ext.tree.AsyncTreeNode({
            id: '0'
        });

        this.loader = new Ext.tree.TreeLoader({
            url: link('admin', 'configuration', 'items'),
            baseAttrs: {
                leaf: true
            }
        });

        this.bbar = new Ext.Toolbar(['->', {
            qtip: lang('Refresh'),
            text: lang('Refresh'),
            iconCls: 'Module-refresh-icon-16x16',
            handler: this.doRefresh,
            scope: this
        }]);

        this.callParent(arguments);

        this.addEvents('beforerefresh');

        this.getSelectionModel().on('beforeselect', function(sm, newnode, oldnode) {
            if (!newnode.isLeaf()) {
                return false;
            }
        });
    },

    doRefresh: function() {

        if (false === this.fireEvent('beforerefresh', this)) {
            return;
        }

        this.getEl().mask(lang('Loading...'), 'x-mask-loading');
        this.getRootNode().reload(function() {
            this.getEl().unmask();
        }, this);
    }
});