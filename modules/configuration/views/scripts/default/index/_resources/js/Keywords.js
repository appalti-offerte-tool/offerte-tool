Ext.define('Module.Configuration.Model.Keywords', {
    extend: 'Ext.data.Model',
    fields: [ 'key', 'caption']
});

Ext.define('Module.Configuration.Keywords', {
    extend: 'Ext.ux.grid.GridPanel',

    autoScroll: true,

    monitorResize: true,

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.Configuration.Model.Keywords',
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'keywords',
                    totalProperty: 'total'
                }
            },
            simpleSortMode: true
        });

        this.columns = [{
            header: lang('Keyword'),
            dataIndex: 'key',
            width: 100,
            menuDisabled: true,
            sortable: true
        }, {
            header: lang('Caption'),
            dataIndex: 'caption',
            flex: 1,
            menuDisabled: true,
            sortable: true
        }];

        this.callParent();
    }
});