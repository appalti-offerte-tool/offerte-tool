<?php

class Default_ErrorController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    public function init()
    {
        parent::init();

        $this->_helper->layout->setLayout('error');

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
        }
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'guest';
    }

    public function deniedAction()
    {
        $this->getResponse()->setHttpResponseCode(405);
        $this->getResponse()->setRawHeader($_SERVER['SERVER_PROTOCOL'] . ' 405 Permission denied');
        $this->view->message = $this->_getParam('message');
//        $this->getResponse()->setRedirect(sprintf('/account/authenticate/index?return=%s', $this->_getParam('return'), 302));
    }

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');

        switch($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                $this->getResponse()->setRawHeader($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
                $this->view->message = 'Page not found';
                break;

            default:

                $this->getResponse()->setRawHeader($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal server error');
                $this->view->message = 'Internal server error';
                break;
        }

        if (0 !== strcasecmp('production', APPLICATION_ENV)) {
            if ($this->getRequest()->isXmlHttpRequest()) {
                $this->_helper->viewRenderer->setNoRender(true);
                $this->view->success = false;
                $this->view->message = $errors->exception->__toString();
            } else {
                $this->view->errors = $errors;
            }
        }
    }
}
