<?php

class Default_IndexController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'guest';
    }

    public function indexAction()
    {
        $account = Zend_Auth::getInstance()->getIdentity();

        $this->_helper->layout->setLayout('guest');
    }

    /**
     * @FIXME
     *
     * Move to other controller?
     */
    public function northSummaryAction()
    {
        $this->_helper->layout->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->northSummary();
    }
}