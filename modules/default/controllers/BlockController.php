<?php

class Default_BlockController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
             ->addActionContext('fetch-all', 'json')
             ->initContext();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'guest';
    }

    public function preDispatch()
    {
        $module = $this->_getParam('__module');
        $block = $this->_getParam('__block');
        $action = $this->_getParam('__action');

        $this->_setParam('__action', null);
        $this->_setParam('__block', null);
        $this->_setParam('__module', null);

        $this->getRequest()->setModuleName($module);
        $this->getRequest()->setControllerName($block);
        $this->getRequest()->setActionName($action);

    }

    public function loadAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        /**
         * Change route to default...
         */
//        Zend_Controller_Front::getInstance()->getRouter()

        $module = $this->getRequest()->getModuleName();

        $dashToCamelCase = new Zend_Filter_Word_DashToCamelCase();
        $moduleNameCamelCase = $dashToCamelCase->filter($module);

        $path = Zend_Controller_Front::getInstance()->getModuleDirectory($module);
        $this->view->addHelperPath($path . '/views/helpers', ucFirst($moduleNameCamelCase) . '_View_Helper_');
        $this->view->addScriptPath($path . '/views/scripts/default');

        $block = $this->getRequest()->getControllerName();

        $blockCls = ucFirst($moduleNameCamelCase)
            . '_Block_' . ucFirst($dashToCamelCase->filter($block));

        $bootstrap = $this->getFrontController()->getParam('bootstrap');
        $bootstrap->bootstrap('ModulesManager');
        $mm = $bootstrap->getResource('ModulesManager');

        $information = $mm->fetch($module);

        $blockObj = new $blockCls($information, array(
            'params' => $this->_getAllParams()
        ));
        $blockObj->setView($this->view);
        $blockObj->setInvokeController($this);

        $action = $this->getRequest()->getActionName();
        $actionMethod = $dashToCamelCase->filter($action) . 'Action';
        if (!method_exists($blockObj, $actionMethod)) {
            throw new OSDN_Exception(sprintf('Unable to run action "%s::%s"', $blockCls, $actionMethod));
        }

        $blockObj->$actionMethod();
    }

    public function fetchAllAction()
    {
        $this->getRequest()->setModuleName($this->getRequest()->getPost('module'));
        $keywords = $this->_getParam('keywords');

        $output = array();
        $blocks = $this->view->blocks($keywords);
        foreach($blocks->toArray() as $block) {
            $output[] = $block->toArray();
        }

        $this->view->rowset = $output;
        $this->view->total = count($output);
        $this->view->success = true;
    }
}