<?php

class Default_Controller_Plugin_Bootstrap extends Zend_Controller_Plugin_Abstract
{
    protected $_isDefaultTheme = true;

    protected $_view;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Plugin_Abstract::routeStartup()
     */
    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
        if ($request->isXmlHttpRequest()) {
            ini_set('html_errors', 'Off');

            Zend_Controller_Action_HelperBroker::getStaticHelper('layout')->disableLayout();
        }

        $f = Zend_Controller_Front::getInstance();
        $router = $f->getRouter();

        $router->addRoute(
            'block',
            new Zend_Controller_Router_Route('block/:module/:controller/:action/*', array(
                'module'        => $f->getDefaultModule(),
                'controller'    => $f->getDefaultControllerName(),
                'action'        => $f->getDefaultAction()
            ))
        );

        $router->addRoute(
            'admin',
            new Zend_Controller_Router_Route('admin', array(
                'module'        => 'account',
                'controller'    => 'authenticate',
                'action'        => 'index',
                'page'          => 'admin'
            ))
        );
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Plugin_Abstract::preDispatch()
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $f = Zend_Controller_Front::getInstance();
        $bootstrap = $f->getParam('bootstrap');

        $f = Zend_Controller_Front::getInstance();
        $router = $f->getRouter();

        $routeName = $router->getCurrentRouteName();

        if (0 === strcasecmp($routeName, 'block')) {
            $request->setParam('__module', $request->getModuleName());
            $request->setParam('__block', $request->getControllerName());
            $request->setParam('__action', $request->getActionName());

            $request->setModuleName('default');
            $request->setControllerName('block');
            $request->setActionName('load');
        }

        $bootstrap->bootstrap('view');

        /**
         * @var Zend_Application_Resource_View
         */
        $viewResource = $bootstrap->getPluginResource('view');
        $viewOptions = $viewResource->getOptions();

        /**
         * Theme support
         * When custom theme is selected we should care that default theme
         * is also using when template is not found in custom theme
         */
        if (!$this->_isDefaultTheme = empty($viewOptions['theme']) || 0 === strcasecmp('default', $viewOptions['theme'])) {

            $layout = Zend_Layout::getMvcInstance();

            /**
             * Trick which allow to using the default theme too
             */
            $layout->setLayoutPath(APPLICATION_PATH . '/layouts/scripts/' . $viewOptions['theme']);
        }

        /**
         * @var Zend_View
         */
        $view = $this->_view = $bootstrap->getResource('view');
        $view->setHelperPath('OSDN/View/Helper', 'OSDN_View_Helper_');

        /**
         * @todo change to more quick solution
         */
        $dashToCamelCase = new Zend_Filter_Word_DashToCamelCase();
        $modules = array('default');
        if ('default' != $request->getModuleName()) {
            $modules[] = $request->getModuleName();
        }

        $view->setScriptPath(null);


        foreach($modules as $module) {
            $path = $f->getModuleDirectory($module);
            $camelCaseModuleName = $dashToCamelCase->filter($module);

            $view->addScriptPath($path . '/views/scripts/default');

            $view->addHelperPath($f->getModuleDirectory($module) . '/views/helpers', $camelCaseModuleName . '_View_Helper_');

            if (!$this->_isDefaultTheme) {
                $view->addScriptPath($path . '/views/scripts/' . $viewOptions['theme']);
            }
        }

        /**
         * Add automatic path for default module and core module
         *
         * @FIXME !!!!!!
         */

        $view->addHelperPath($f->getModuleDirectory('account') . '/views/helpers', 'Account_View_Helper_');

        $view->addHelperPath($f->getModuleDirectory('application') . '/views/helpers', 'Application_View_Helper_');
        $view->addHelperPath($f->getModuleDirectory('module') . '/views/helpers', 'Module_View_Helper_');

        $view->addHelperPath(APPLICATION_PATH . '/layouts/helpers/', 'Layout_View_Helper_');

        /**
         * Init lazy module bootstrap
         */
        parent::preDispatch($request);
    }

    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
        if (!$this->_isDefaultTheme) {
            $layout = Zend_Layout::getMvcInstance();
            $layout->getView()->setScriptPath(APPLICATION_PATH . '/layouts/scripts/default');
        }

//        foreach(array('ContextSwitch', 'AjaxContext') as $hName) {
//
//            if (! Zend_Controller_Action_HelperBroker::hasHelper($hName)) {
//                continue;
//            }
//
//            $cs = Zend_Controller_Action_HelperBroker::getExistingHelper($hName);
//            if ('json' == $cs->getCurrentContext()) {
//                if (method_exists($this->_view, 'getVars')) {
//                    foreach($this->_view->getVars() as $k => $v) {
//                        if (is_object($v) && $v instanceof OSDN_Application_Model_Interface) {
//                            $this->_view->$k = $v->toArray();
//                        }
//                    }
//                }
//            }
//        }
    }
}