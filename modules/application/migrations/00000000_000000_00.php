<?php

class Application_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        foreach(array(
            array('roleId' => 1, 'resource' => 'application', 'privilege' => null),
            array('roleId' => 1, 'resource' => 'application:core', 'privilege' => null),
        ) as $o) {
            $this->insert('aclPermission', $o);
        }
    }

    public function down()
    {
        $this->getDbAdapter()->delete('aclPermission', array(
            'roleId = 1',
            "(resource = 'application' OR resource LIKE 'application:%')"
        ));
    }
}