<?php

abstract class Application_Block_Abstract
{
    /**
     * @var Zend_View_Interface
     */
    public $view;

    protected $_viewSuffix = '';

    protected $_id;

    protected $_name;

    protected $_parent;

    protected $_title;

    protected $_icon;

    protected $_priority = 0;

    protected $_mappings = array();

    protected $_invokeArgs = array();

    protected $_doForceInitReload = false;

    protected $_hasInterfacePackage = false;

    /**
     * Contain information about module
     *
     * @var Module_Instance_Information
     */
    protected $_information;

    private $_invokeController;

    public function __construct(Module_Instance_Information $information, array $options = array())
    {
        foreach($options as $oKey => $oValue) {
            $oKey = strtolower($oKey);
            switch($oKey) {
                case 'title':
                case 'parent':
                    $this->{'_' . $oKey} = $oValue;
                    break;

                case 'params':
                    $this->_invokeArgs = (array) $oValue;
                    break;

                case 'mappings':
                    $this->_mappings = (array) $oValue;
                    break;

                case 'priority':
                    $this->_priority = (int) $oValue;
            }
        }

        $this->_information = $information;
        $this->init();
    }

    public function init()
    {}

    public function getFilterParams()
    {
        $namespace = OSDN_Controller_Plugin_ViewFilter::toId($this->getId());
        $session = new Zend_Session_Namespace($namespace);

        return !empty($session->filter) ? $session->filter : array();
    }

    public function getInvokeArgs()
    {
        return $this->_invokeArgs;
    }

    public function setInvokeController(Zend_Controller_Action $controller)
    {
        $this->_invokeController = $controller;
        return $this;
    }

    public function getMappings()
    {
        return $this->_mappings;
    }

    public function getPriority()
    {
        return $this->_priority;
    }

    public function getIcon()
    {
        return !empty($this->_icon) ? $this->_icon : '';
    }

    public function setIcon($icon)
    {
        $this->_icon = $icon;
    }


    public function isForceInitReload()
    {
        return (boolean) $this->_doForceInitReload;
    }

    public function indexAction()
    {
        echo $this->toHtml();
    }

    abstract protected function _toTitle();

    protected function _toHtml()
    {}

    public function toTitle()
    {
        if (!empty($this->_title)) {
            return $this->_title;
        }

        return $this->_title = $this->_toTitle();
    }

    public function toHtml()
    {
        $this->view->blockId = $this->getId();

        try {
            $html = $this->_toHtml();
            if (false === $html) {
                return '';
            } elseif (!is_string($html)) {
                return $this->view->render($this->_toTemplateName());
            }

        } catch(OSDN_Exception $e) {
            $o = array();

            foreach($e->getMessages() as $message) {
                $o[] = $message->getMessage();
            }

            return '<ul><li>' . join('</li><li>', $o) . '</li></ul>';
        } catch(Exception $e) {
            return $e;
        }

        return $html;
    }

    public function getModuleName()
    {
        return $this->_information->getName();
    }

    public function getId()
    {
        if (empty($this->_id)) {
            $this->_id = $this->_getParam('_bid');
        }

        if (empty($this->_id)) {
            $this->_id = sprintf('%s:%s:%s', $this->getModuleName(), $this->getName(), uniqid());
        }

        return $this->_id;
    }

    /**
     * @FIXME
     */
    public function getName()
    {
        $clsSuffix = substr(strrchr(get_class($this), "_"), 1);
        $camelCaseToDash = new Zend_Filter_Word_CamelCaseToDash();
        $template = $camelCaseToDash->filter($clsSuffix);

        return strtolower($template);
    }

    public function getPackage()
    {
        if (true !== $this->_hasInterfacePackage) {
            return false;
        }

        return sprintf('module:%s:%s', $this->getModuleName(), $this->getName());
    }

    public function _toTemplateName()
    {
        $name = $this->getName();
        if (!empty($this->_viewSuffix)) {
            $name .= '.' . $this->_viewSuffix;
        }

        return $name . '.phtml';
    }

    public function __toString()
    {
        return $this->toHtml();
    }

    public function toArray()
    {
        return array(
            'id'        => $this->getId(),
            'title'     => $this->toTitle(),
            'module'    => $this->getModuleName(),
            'block'     => $this->getName(),
            'icon'      => $this->getIcon(),
            'package'   => $this->getPackage(),
            'loaded'    => false,
            'mappings'  => $this->getMappings(),
            'force'     => $this->isForceInitReload()
        );
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;

        $basePath = $this->_information->getRoot() . '/views';
        $classPrefix = $this->_information->getNameCamelCase() . '_View';
        $this->view->addBasePath($basePath, $classPrefix);

        $this->view->addScriptPath($basePath . '/scripts/default');
        $this->view->addScriptPath($basePath . '/blocks/default');
    }

    /**
     * @return Zend_Controller_Request_Abstract
     */
    public function getRequest()
    {
        return Zend_Controller_Front::getInstance()->getRequest();
    }

    protected function _getParam($name, $default = null)
    {
        if (array_key_exists($name, $this->_invokeArgs)) {
            return $this->_invokeArgs[$name];
        }

        return $default;
    }

    /**
     * Redirect method call directly to invoke controller
     *
     * @param string $method
     * @param array $args
     */
    public function __call($method, array $args = array())
    {
        if (null === $this->_invokeController) {
            throw new OSDN_Exception('Unable to execute methods. Invoke controller is not defined.');
        }

        return call_user_func_array(array($this->_invokeController, $method), $args);
    }
}