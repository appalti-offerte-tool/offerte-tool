<?php

class Application_UpdateController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /** @var OSDN_Db_Adapter_Pdo_Mysql $db */
    protected $db;

    public function init()
    {
        parent::init();
        $this->_helper->layout->disableLayout();
    }

    public function getResourceId()
    {
        return 'application:core';
    }

    public function indexAction()
    {

    }

    public function latestAction()
    {
        die('not implemented.');
        $this->_forward('fase1-commit');
    }

    public function fase1CommitAction()
    {
        // init
        $_companies = new Company_Model_DbTable_Company();

        // svn switched/merged to branch fase1

        /**
         * @todo apply db changes /docs/fase1.commit.sql
         */
        $path_sql_file = APPLICATION_PATH .'/docs/fase1.commit.sql';
        if (file_exists($path_sql_file))
        {
            $front = Zend_Controller_Front::getInstance();
            $bootstrap = $front->getParam("bootstrap");
            $bootstrap->bootstrap('db');
            $config = $bootstrap->getResource('db')->getConfig();
            $runCommand = "mysql"
                        .(isset($config["host"])? " -h ".$config["host"]:"")
                        .(isset($config["port"])? " -P ".$config["port"]:"")
                        .(isset($config["username"])? " -u ".$config["username"]:"")
                        .(isset($config["password"])? " -p ".$config["password"]:"")
                        ." ".$config["dbname"]." < ".$path_sql_file;
//            echo $runCommand;
//            if (system($runCommand) == false)
//            {
//              $this->_helper->information("Couldn't apply sqlfile: '".$path_sql_file."'");
//              return;
//            }
//            // do what you need to do after import sql file
//            // eg archive file so it isn't available anymore
        }



        /**
         * @todo call /module; klik 'Find and register new modules'
         */



        /**
         * convert library from proposalblocks per company
         */
        $select = $_companies
                        ->select()
                        ->from(array('c'=>'company'))

                        // get total of companies that need to be processed
                        ->columns(array('total'=>'COUNT(*)'))

                        ->joinLeft(array('l'=>'library'), 'l.companyId = c.id', array())
                        ->where('l.id IS NULL')
                        ->where('c.parentId IS NULL') // remove company relations
                        ->where('c.motherId IS NULL') // remove company extra locations
                        ->order('c.id ASC')
        ;
        $companyRow = $_companies->fetchRow($select);
        if($companyRow && $companyRow->getId())
        {
            $this->view->action = 'collect library';
            $this->view->companyRow = $companyRow;
            $this->view->libraryRow = null;
            if ($companyRow->total != $this->_getParam('total', $companyRow->total))
            { // total didn't decrease; something's wrong
                $this->_helper->information("Couldn't create library.");
                return;
            }

            // create library from proposalblocks
            if (($libraryRow = $this->_createLibrary($companyRow)) != false)
            {
                $this->_helper->information('library created.', true, E_USER_NOTICE);
                $this->view->libraryRow = $libraryRow;
                $this->_redirect($this->view->url(array('total'=>$companyRow->total-1)));
            }
            return;
        }
        if ($this->_getParam('total'))
        {
            // total is set; unset total
            $this->_redirect($this->view->url(array('total'=>null)));
        }



        /**
         * convert theme from customtemplates per company
         */
        $selectNot = $_companies
                        ->select()
                        ->from(array('c'=>'company'), array('id'))
                        ->join(array('pct'=>'proposalCustomTemplate'), 'pct.companyId = c.id', array())
                        ->join(array('pt'=>'proposalTheme'), 'pt.companyId = c.id AND pt.useCustomTemplate = pct.id', array())
                        ->group(array('c.id'))
        ;
        $select = $_companies->select()
                                ->from(array('c'=>'company'))

                                // get total of companies that need to be processed
                                ->columns(array('total'=>'COUNT(*)'))

                                ->where('id NOT IN (?)', $selectNot)
                                ->where('c.parentId IS NULL') // remove company relations
                                ->where('c.motherId IS NULL') // remove company extra locations
                                ->order('c.id ASC')
        ;
        $companyRow = $_companies->fetchRow($select);
        if($companyRow && $companyRow->getId())
        {
            $this->view->action = 'collect theme';
            $this->view->companyRow = $companyRow;
            $this->view->themeRow = null;
            if ($companyRow->total != $this->_getParam('total', $companyRow->total))
            { // total didn't decrease; something's wrong
                $this->_helper->information("Couldn't update theme.");
                return;
            }
            // create theme from proposalblocks
            if (($themeRow = $this->_updateTheme($companyRow)) != false)
            {
                $this->_redirect($this->view->url(array('total'=>$companyRow->total-1)));
                $this->view->themeRow = $themeRow;
                $this->_helper->information('Theme updated.', true, E_USER_NOTICE);
            }
            return;
        }
        if ($this->_getParam('total'))
        {
            // total is set; unset total
            $this->_redirect($this->view->url(array('total'=>null)));
        }



        /**
         * convert roles
         */
        $_roles = new Account_Model_DbTable_AclRole();

        // find company Appalti by username 'nhofs@appalti.nl'
        $select = $_companies->select()
                                ->setIntegrityCheck(false)
                                ->from(array('c'=>'company'))
                                ->columns(array(
                                    'role_id'           =>'ar.id',
                                    'role_companyId'    =>'ar.companyId',
                                    'role_name'         =>'ar.name',
                                    'role_parentId'     =>'ar.parentId',
                                    'role_luid'         =>'ar.luid',
                                ))
                                ->join(array('cp'=>'contactPerson'), 'cp.companyId = c.id', array())
                                ->join(array('a'=>'account'), 'a.id = cp.accountId', array())
                                ->joinLeft(array('ar'=>'aclRole'), ' ar.companyId = c.id', array())
                                ->where('c.parentId IS NULL') // remove company relations
                                ->where('c.motherId IS NULL') // remove company extra locations
                                ->where('a.username = ?', 'nhofs@appalti.nl')
        ;
        $this->view->companyRowset = $companyRowset = $_companies->fetchAll($select);
        if (count($companyRowset) != 1)
        {
            $this->_helper->information('Too many companies found.');
            return;
        }
        $this->view->companyRow = $companyRow = $companyRowset->current();
        // find company role
        if (($appaltiRoleRow = $_roles->fetchRow('id = '.$companyRow->role_id)) == false)
        { // company not converted
            $this->_helper->information('Appalti role niet gevonden.');

            // modify company roles
            if (!$this->_modifyCompanyRoles($companyRow))
            {
                $this->_helper->information('Modifying Appalti failed.');
                return;
            }
            // find company role
            $select = $_roles->select()
                                ->where('companyId = ?', $companyRow->getId())
            ;
            if (($appaltiRoleRow = $_roles->fetchRow($companyRow->role_id)) == false)
            { // conversion went wrong
                $this->_helper->information('Appalti role not found.');
                return;
            }

            // set all rights for company role
            // - copy application admin permissions to Appalti company role
            $select = $_roles->select()->where('id = ?', 1);
            if (($adminRole = $_roles->fetchRow($select)) == false)
            {
                 throw new Exception("Couldn't find admin role.");
            }
            $_roles->copyPermissions($adminRole, $appaltiRoleRow);

            // next company
            $this->_redirect($this->view->url());
        }
        $this->view->companyRow = null;
        $this->view->companyRowset = null;


        // find companies that still need conversion
        $select = $_companies->select()
                                ->setIntegrityCheck(false)
                                ->from(array('c'=>'company'))
                                ->columns(array(
                                    'role_id'           =>'ar.id',
                                    'role_companyId'    =>'ar.companyId',
                                    'role_name'         =>'ar.name',
                                    'role_parentId'     =>'ar.parentId',
                                    'role_luid'         =>'ar.luid',
                                ))
                                ->join(array('cp'=>'contactPerson'), 'cp.companyId = c.id', array())
                                ->join(array('a'=>'account'), 'a.id = cp.accountId', array())
                                ->joinLeft(array('ar'=>'aclRole'), ' ar.companyId = c.id', array())
                                ->where('c.parentId IS NULL') // remove company relations
                                ->where('c.motherId IS NULL') // remove company extra locations
                                ->where('ar.id IS NULL')
                                ->group(array('c.id'))
        ;
        // per company
        if(($companyRow = $_companies->fetchAll($select)->current()))
        {
//            $this->_helper->information('Company gevonden.', true, E_USER_NOTICE);
            // - modify company
            if (!$this->_modifyCompanyRoles($companyRow, $appaltiRoleRow))
            {
                $this->_helper->information("Modifying company failed.");
                return;
            }
            // next company
            $this->_redirect($this->view->url());
        }

        // get companies that don't have a theme available
        // per company
        $select = $_companies->select()
                                ->from(array('c'=>$_companies->getTableName()), array('*'))
                                ->joinLeft(array('p'=>'proposalCustomTemplate'), 'p.companyId = c.id', array())
                                ->joinLeft(array('lp'=>'locationProposalcustomtemplate'), 'lp.locationId = c.id', array())
                                ->where('c.parentId IS NULL')
                                ->where('c.motherId IS NULL')
                                ->where('lp.locationId IS NULL')
        ;
        if (($companyRow = $_companies->fetchRow($select)) != false)
        {
            if (!$this->linkThemeLibrary($companyRow))
            {
                return;
            }
            $this->_redirect($this->view->url());
        }

        // get companies that have proposals without location or theme
        $select = $_companies
                        ->select()
                        ->from(array('c'=>'company'))
                        ->join(array('p'=>'proposal'),'p.companyId = c.id',array())
                        ->where('p.locationId = 0')
                        ->orWhere('p.proposalCustomTemplateId = 0')
                        ->order('id ASC')
        ;
        // per company
        if (($companyRow = $_companies->fetchRow($select)) != false)
        {
            if (!$this->updateProposals($companyRow))
            {
                $this->_helper->information('Updating company proposals failed');
                return;
            }
            $this->_redirect($this->view->url());
        }

        // update ready
        $this->_helper->information('Update ready.', true, E_USER_NOTICE);
    }

    public function updateProposals(Company_Model_CompanyRow $companyRow)
    {
        $this->view->companyRow = $companyRow;
        try {
            // start transaction
            $companyRow->getTable()->getAdapter()->beginTransaction();

            // - get company proposals without location or theme
            // - set company proposal location and theme from select
            $_proposals = new Proposal_Model_DbTable_Proposal();
            $select = $_proposals
                            ->select()
                            ->setIntegrityCheck(false)
                            ->from(array('p'=>'proposal'))
                            ->join(array('pt'=>'proposalTheme'),'pt.proposalId = p.id',array())
                            ->columns(array(
                                'locationId'=>'p.companyId',
                                'proposalCustomTemplateId'=>'pt.useCustomTemplate',
                            ))
                            ->where('p.companyId = ?', $companyRow->getId() )
                            ->where('(p.locationId = 0 OR p.proposalCustomTemplateId = 0)')
            ;
            foreach($_proposals->fetchAll($select) as $proposalRow)
            {
                $this->view->proposalRow = $proposalRow;
                // $proposalRow->save(); doesn't work
                // because of integrity check and/or join;
                // this row can't be saved: Error: 'This row has been marked read-only'
                // so reload row from proposalRow
                $newProposalRow = $_proposals->fetchRow('id = '.(int)$proposalRow->getId());
                $newProposalRow->locationId                 = $proposalRow->locationId;
                $newProposalRow->proposalCustomTemplateId   = $proposalRow->proposalCustomTemplateId;
                $newProposalRow->save();
                $this->view->newProposalRow = $newProposalRow;
            }

            // commit transaction
            $companyRow->getTable()->getAdapter()->commit();
            return $companyRow;

        } catch(Exception $e) {
            $this->_helper->information($e->getMessage());
            $companyRow->getTable()->getAdapter()->rollBack();
        }
    }

    public function linkThemeLibrary(Company_Model_CompanyRow $companyRow)
    {
        $this->view->companyRow = $companyRow;
        try {
            //init
            $_librariesThemes           = new Company_Model_DbTable_LibraryProposalCustomTemplate();
            $_locationThemes            = new Company_Model_DbTable_LocationProposalCustomTemplate();
            $_contactpersons            = new Company_Model_DbTable_ContactPerson();
            $_contactpersonLocations    = new Company_Model_DbTable_ContactPersonLocation();
            $_contactpersonThemes       = new Company_Model_DbTable_ContactPersonProposalCustomTemplate();

            // start transaction
            $companyRow->getTable()->getAdapter()->beginTransaction();

            // link standard library to standard theme
            // - get company theme
            if (($themeRow = $companyRow->findDependentRowset('Proposal_Model_DbTable_CustomTemplate', 'Company')->current()) == false)
            {
                throw new Exception('Could not get company theme.');
            }
            $this->view->themeRow = $themeRow;
            // - get company library
            if (($libraryRow = $companyRow->findDependentRowset('Company_Model_DbTable_Library')->current()) == false)
            {
                throw new Exception('Could not get company library.');
            }
            $this->view->libraryRow = $libraryRow;

            // - link library to theme
            $themeLibraryRow = $_librariesThemes->createRow(array(
                'libraryId'                 => $libraryRow->getId(),
                'proposalcustomtemplateId'  => $themeRow->getId(),
            ));
            if (!$themeLibraryRow->save())
            {
                throw new Exception('Could not link library to theme.');
            }
            // make standard theme available to company
            $locationThemeRow = $_locationThemes->createRow(array(
                'locationId' => $companyRow->getId(),
                'proposalcustomtemplateId'  => $themeRow->getId(),
            ));
            if (!$locationThemeRow->save())
            {
                throw new Exception('Could not link theme to company.');
            }

            // get company users
            $contactpersonRowset = $companyRow->findDependentRowset('Company_Model_DbTable_ContactPerson', 'Company');
            foreach($contactpersonRowset as $contactpersonRow)
            {
                // make company available to user
                $contactpersonLocationRow = $_contactpersonLocations->createRow(array(
                    'contactpersonId'   => $contactpersonRow->getId(),
                    'locationId'        => $companyRow->getId(),
                ));
                if (!$contactpersonLocationRow->save())
                {
                    throw new Exception('Could not link contactperson to company.');
                }

                // make standard theme available to user
                $contactpersonThemeRow = $_contactpersonThemes->createRow(array(
                    'contactpersonId'           => $contactpersonRow->getId(),
                    'proposalcustomtemplateId'  => $themeRow->getId(),
                ));
                if (!$contactpersonThemeRow->save())
                {
                    throw new Exception('Could not link contactperson to theme.');
                }
            }

            // commit transaction
            $companyRow->getTable()->getAdapter()->commit();
            return $companyRow;
        } catch(Exception $e) {
            $this->_helper->information($e->getMessage());
            $companyRow->getTable()->getAdapter()->rollBack();
        }
    }

    /**
     *  modify company (parent company)
     *
     * @param Company_Model_CompanyRow $companyRow
     * @param Account_Model_DbTable_AclRoleRow $parentRoleRow
     * @return mixed Company_Model_CompanyRow on success, false on failure
     */
    protected function _modifyCompanyRoles(Company_Model_CompanyRow $companyRow, Account_Model_DbTable_AclRoleRow $parentRoleRow = null)
    {
        // init
        $_roles     = new Account_Model_DbTable_AclRole();
        $_accounts  = new Account_Model_DbTable_Account();
        $this->view->companyRow = $companyRow;

        // process
        try {
            // start transaction
            $_roles->getAdapter()->beginTransaction();

            // find company role
            $select = $_roles->select()->where('companyId = ?', $companyRow->getId());
            $companyRolesRowset = $_roles->fetchAll($select);
            if (count($companyRolesRowset)> 1)
            { // multiple company roles
                throw new Exception('Multiple company roles found.');
            }
            if (count($companyRolesRowset)== 1)
            { // company role exists
                $this->_helper->information('company role found.', true, E_USER_NOTICE);

                // get roleRow
                $roleRow = $companyRolesRowset->current();
                $this->_helper->information('got role.', true, E_USER_NOTICE);

                // set columns and save
                $roleRow->parentId = $parentRoleRow->getId();
                if (!$roleRow->save())
                {
                    throw new Exception("Couldn't save role.");
                }
                $this->_helper->information('updated role.', true, E_USER_NOTICE);

                // copy permissions from beheerder role
                $select = $_roles->select()->where('id = ?', 6);
                $beheerderRoleRow = $_roles->fetchRow($select);
                if (!$_roles->copyPermissions($beheerderRoleRow, $roleRow))
                {
                    throw new Exception("Couldn't copy permissions.");
                }
                $this->_helper->information('copied permissions.', true, E_USER_NOTICE);

                // ready
                return $companyRow;
            }
            // no company role found

            // create company role from beheerder role
            // -set Appalti roleId as parentId
            // - set company_id
            // - set company_name as role_name
            // - save company role
            // - copy rights from beheerder role
            $select = $_roles->select()->where('id = ?', 6);
            $beheerderRoleRow = $_roles->fetchRow($select);
            $data = array(
                'id'        => null,
                'companyId' => $companyRow->getId(),
                'name'      => $companyRow->name,
                'parentId'  => $parentRoleRow->getId(),
                'luid'      => null,
            );
            $companyAdminRoleRow = $_roles->copyRole($beheerderRoleRow, $data, true);
            $this->_helper->information('created company admin role for admins.', true, E_USER_NOTICE);

            // create company gebruiker role from gebruiker role
            // - set rights from gebruiker role
            $select = $_roles->select()->where('id = ?', 5);
            $userRoleRow = $_roles->fetchRow($select);
            $data = array(
                'id'        => null,
                'companyId' => $companyRow->getId(),
                'name'      => $userRoleRow->name,
                'parentId'  => $companyAdminRoleRow->getId(),
                'luid'      => null,
            );
            $companyUserRoleRow = $_roles->copyRole($userRoleRow, $data, true);
            $this->_helper->information('created company user role for managers and users.', true, E_USER_NOTICE);

            // find beheerders of company;
            // - set company beheerder flag
            // - set company gebruiker role
            $select = $_accounts->select()
                                ->from(array('a'=>'account'), array('*'))
                                ->join(array('cp'=>'contactPerson'), 'cp.accountId = a.id', array())
                                ->join(array('c'=>'company'), 'c.id = cp.companyId', array())
                                ->where('c.id = ?', $companyRow->getId())
                                ->where('a.roleId = ?', 6)
            ;
            $adminRowset = $_accounts->fetchAll($select);
            foreach($adminRowset as $adminRow)
            {
                $adminRow->roleId = $companyUserRoleRow->getId();
                $adminRow->admin = 1;
                $adminRow->save();
            }
            $this->_helper->information('replace admin role for company admin role.', true, E_USER_NOTICE);

            // find managers of company
            // - set company gebruiker role
            $select = $_accounts->select()
                                ->from(array('a'=>'account'), array('*'))
                                ->join(array('cp'=>'contactPerson'), 'cp.accountId = a.id', array())
                                ->join(array('c'=>'company'), 'c.id = cp.companyId', array())
                                ->where('c.id = ?', $companyRow->getId())
                                ->where('a.roleId = ?', 3)
            ;
            $managersRowset = $_accounts->fetchAll($select);
            foreach($managersRowset as $userRow)
            {
                $userRow->roleId = $companyUserRoleRow->getId();
                $userRow->save();
            }
            $this->_helper->information('replaced manager role for company user role', true, E_USER_NOTICE);

            // find gebruikers of company
            // - set company gebruiker role
            $select = $_accounts->select()
                                ->from(array('a'=>'account'), array('*'))
                                ->join(array('cp'=>'contactPerson'), 'cp.accountId = a.id', array())
                                ->join(array('c'=>'company'), 'c.id = cp.companyId', array())
                                ->where('c.id = ?', $companyRow->getId())
                                ->where('a.roleId = ?', 5)
            ;
            $usersRowset = $_accounts->fetchAll($select);
            foreach($usersRowset as $userRow)
            {
                $userRow->roleId = $companyUserRoleRow->getId();
                $userRow->save();
            }



            // apply changes
            $_roles->getAdapter()->commit();
            return $companyRow;
        } catch (Exception $e) {
            $_roles->getAdapter()->rollback();
            $this->_helper->information($e->getMessage());
        }
        return false;
    }

    protected function _createLibrary(Company_Model_CompanyRow $companyRow)
    {
        // current clients do already have proposalblocks
        // find these blocks and gather them in a new 'default' library
        try {
            $_libraries = new Company_Service_Library($companyRow);
            $_libraries->getAdapter()
                                ->beginTransaction();

            // create new library
            $data = array(
                'id'            => 0,
                'companyId'     => $companyRow->getId(),
                'name'          => 'Standaard',
                'description'   => 'Uw standaard bibliotheek',
            );
            if(!$libraryRow = $_libraries->create($data))
            {
                throw new Exception('Could not create new library.');
            }
            $this->view->libraryRow = $libraryRow;

            // add existing proposalblocks to newly created library
            $proposalblockSrv = new ProposalBlock_Service_ProposalBlock($companyRow);
            $proposalBlocks = $proposalblockSrv->fetchAll(null, $companyRow);
            foreach($proposalBlocks as $proposalBlock)
            {
                $this->view->proposalblock = $proposalBlock;
                if(!$_libraries->addProposalBlock($libraryRow, $proposalBlock))
                {
                    throw new Exception("Could not add proposalblock");
                }
            }

            // add existing company subcategories to newly created library
            $categorySrv = new ProposalBlock_Service_Category($companyRow);
            $categories = $categorySrv->fetchAll()->getRowset();
            while($category = $categories->current())
            {
                $category->libraryId = $libraryRow->getId();
                if (!$category->save())
                {
                    throw new Exception("Could not add category to library.");
                }
                $categories->next();
            }

            // commit changes
            $_libraries->getAdapter()->commit();
        } catch(Exception $e) {
            $_libraries->getAdapter()->rollback();
            $this->_helper->information($e->getMessage());
            return false;
        }
        return $libraryRow;
    }

    public function _updateTheme(Company_Model_CompanyRow $companyRow)
    {
        /*
         * If only 1 theme exists, this may be the first visit
         * if first visit
         *     get company proposal themes
         *     assign each to company custom template
         */
        $_themes = new Proposal_Service_CustomTemplate($companyRow);
        $themes = $_themes->fetchAll();
        $this->view->themes = $themes;

        if (count($themes) == 0)
        {
            $this->_helper->information('No themes');
            try {
                $_themes->getAdapter()->beginTransaction();

                // create theme
                $themeRow = $_themes->createRow();
                $themeRow->companyId = $companyRow->getId();
                $themeRow->name = 'Standaard';
                $themeRow->active = 1;
                if (!$themeRow->save())
                {
                    throw Exception("Couldn't create theme.");
                }

                // add settings
                $_proposalThemes = new Proposal_Model_DbTable_ProposalTheme();
                $themeSettings = $_proposalThemes->createRow(array(
                    'companyId' => $themeRow->companyId
                ));
                if (!$themeSettings->save())
                {
                    throw Exception("Couldn't create theme settings.");
                }

                // add templates
                $_templates = new Proposal_Model_DbTable_CustomTemplateSettings();

                // - add template 'common'
                $themeTemplateRowCommon = $_templates->createRow(array(
                    'proposalCustomTemplateId' => $themeRow->getId(),
                    'scope' => 'common',
                ));
                if (!$themeTemplateRowCommon->save())
                {
                    throw Exception("Couldn't create theme template 'common'.");
                }
                // - add template 'introduction'
                $themeTemplateRowIntro = $_templates->createRow(array(
                    'proposalCustomTemplateId' => $themeRow->getId(),
                    'scope' => 'introduction',
                ));
                if (!$themeTemplateRowIntro->save())
                {
                    throw Exception("Couldn't create theme template 'introduction'.");
                }

                // - add template 'content'
                $themeTemplateRowContent = $_templates->createRow(array(
                    'proposalCustomTemplateId' => $themeRow->getId(),
                    'scope' => 'content',
                ));
                if (!$themeTemplateRowContent->save())
                {
                    throw Exception("Couldn't create theme template 'content'.");
                }
                // - add template 'attachm'
                $themeTemplateRowAttach = $_templates->createRow(array(
                    'proposalCustomTemplateId' => $themeRow->getId(),
                    'scope' => 'attachment',
                ));
                if (!$themeTemplateRowAttach->save())
                {
                    throw Exception("Couldn't create theme template 'attachm'.");
                }

                // commit changes
                $_themes->getAdapter()->commit();
                $this->_helper->information('Theme created', true, E_USER_NOTICE);
                return $themeRow;

            } catch (Exception $e) {
                $_themes->getAdapter()->rollback();
                $this->_helper->information($e->getMessage());
            }
        }
        if (count($themes) > 1)
        {
            $this->_helper->information('Too many themes');
            return;
        }
        try {
//die('stop');
            // get custom template
            $customTemplateRow = $themes->current();

            // get custom template proposal themes
            $_proposalThemes = new Proposal_Model_DbTable_ProposalTheme();
            $_proposalThemes->getAdapter()
                                ->beginTransaction();

            $select = $_proposalThemes->select()
                                        ->where('companyId = ?', $customTemplateRow->companyId)
                                        ->where('proposalId IS NULL')
                                        ->where('useCustomTemplate = ? ', $customTemplateRow->getId())
            ;
            $proposalThemeRowset = $_proposalThemes->fetchAll($select);
            if (count($proposalThemeRowset))
            {
                return $customTemplateRow;
            }

            // get proposal theme
            $select = $_proposalThemes->select()
                                            ->where('companyId = ?', $customTemplateRow->companyId)
            // all templates are selected
            // selects theme templates      ->where('proposalId IS NULL')
            // selects proposal templates   ->where('proposalId IS NOT NULL')
            ;
            $proposalThemeRowset = $_proposalThemes->fetchAll($select);
            foreach($proposalThemeRowset as $proposalThemeRow)
            {
                // save as custom template proposal theme
                $proposalThemeRow->useCustomTemplate = $customTemplateRow->getId();
                $proposalThemeRow->save();
            }

            // copy backgrounds
            // - create theme map
            $defaultPath = APPLICATION_PATH .'/modules-appalti/company/data/templates';
            $oldPathname = sprintf($defaultPath.'/%d/backgrounds', $companyRow->getId());
            $newPathname = sprintf($defaultPath.'/%d/%d/backgrounds', $companyRow->getId(), $proposalThemeRow->getId());
            if (!is_dir($newPathname) && !mkdir($newPathname, 0777, true))
            {
                throw new Exception('Could not create dir for theme.');
            }
            // - copy existing backgrounds to new map
            if (is_dir($oldPathname))
            {
                foreach( new DirectoryIterator($oldPathname) as $fileInfo)
                {
                    if ($fileInfo->isDot())
                    {
                        continue;
                    }
                    if (!$fileInfo->isFile())
                    {
                        throw new Exception("File '".$fileInfo->getPath()."/".$fileInfo->getFilename()."' is not a file.");
                    }
                    if (!copy($fileInfo->getPathname(), $newPathname.'/'.$fileInfo->getBasename()))
                    {
                        throw new Exception("Could not copy file '".$fileInfo->getPath()."/".$fileInfo->getFilename()."' from theme to '".$newPathname.'/'.$fileInfo->getBasename()."'.");
                    }
                }
            }

            // commit changes
            $_proposalThemes->getAdapter()->commit();
            return $customTemplateRow;
        } catch(Exception $e) {
            $_proposalThemes->getAdapter()->rollback();
            $this->_helper->information($e->getMessage());
        }
    }

    public function fase1RollbackAction()
    {
        $this->_helper->information('Not implemented.', true, E_USER_NOTICE);
        // apply db changes /docs/fase1/ fase1.rollback.sql
        // svn switch to trunk / revert to last revision before merge
    }

    public function companiesAction()
    {
        $_companies = new Company_Model_DbTable_Company();
        $select = $_companies
                        ->select()
                        ->from(array('c'=>'company'))

                        // get total of companies that need to be processed
                        ->columns(array('total'=>'COUNT(*)'))

                        // get total of regions for this company
//                        ->joinLeft(array('cr'=>'companyRegion'), 'cr.companyId = c.id', array())
//                        ->columns(array('count_regions'=> 'COUNT(cr.regionId)'))

                        // get total of libraries for this company
//                        ->joinLeft(array('l'=>'library'), 'l.companyId = c.id', array())
//                        ->columns(array('count_libraries'=> 'COUNT(l.id)'))

                        // get total of themes for this company
//                        ->joinLeft(array('pct'=>'proposalCustomTemplate'), 'pct.companyId = c.id', array())
//                        ->columns(array('count_themes'=> 'COUNT(*)'))

                        // get total of proposals for this company
                        ->joinLeft(array('p'=>'proposal'), 'p.companyId = c.id', array())
//                        ->joinLeft(array('pt'=>'proposalTheme'), 'pt.companyId = c.id AND proposalId IS NOT NULL', array())
                        ->columns(array('count_proposals'=> 'COUNT(*)'))

                        ->group(array('c.id'))
        ;
//die($select);
        $companiesRowset = $_companies->fetchAll($select);
        $this->companies = array();
        foreach($companiesRowset as $companyRow)
        {
            $company = $companyRow->toArray();
            $company['childs'] = array();
            $company['locations'] = array();
            $this->companies[ $company['id'] ] = $company;
        }
        foreach($companiesRowset as $companyRow)
        {
            $company = $companyRow->toArray();
            if ($company['parentId'])
            {
                $this->companies[ $company['parentId']]['childs'][] = $company['id'];
            }
            if ($company['motherId'])
            {
                $this->companies[ $company['motherId']]['locations'][] = $company['id'];
            }
        }
        $this->view->companies = $this->companies;
    }

    public function correctThemesAction()
    {
        $_companies = new Company_Model_DbTable_Company();
        $select = $_companies
                        ->select()
                        ->setIntegrityCheck(false)
                        ->from(array('c'=>'company'), array('id'=>'c.id', 'name' =>'c.name'))
                        ->join(array('pct' => 'proposalCustomTemplate'),'pct.companyId = c.id',array('themeId'=>'pct.id'))
                        ->join(array('pt' => 'proposalTheme'),'pt.useCustomTemplate = pct.id AND pt.proposalId IS NULL',array('styleId'=>'pt.id'))
                        ->where('c.parentId IS NULL')
                        ->where('c.motherId IS NULL')
//                        ->where('c.isActive = 1')
//                        ->limit(10)
        ;
        $companies = $_companies->fetchAll($select);
        $defaultPath = APPLICATION_PATH .'/modules-appalti/company/data/templates';
echo '<ul>';
        foreach($companies as $company)
        {
            // copy backgrounds
            // - create theme map
            $defaultPath = APPLICATION_PATH .'/modules-appalti/company/data/templates';
            $oldPathname = sprintf($defaultPath.'/%d/backgrounds', $company->id);
            $newPathname = sprintf($defaultPath.'/%d/%d/backgrounds', $company->id, $company->themeId);
echo '  <li>'.$company->name.'('.$company->id.'): ';
echo $oldPathname.' - ';
echo $newPathname.'<br/>';
//continue;
            if (!is_dir($newPathname) && !mkdir($newPathname, 0777, true))
            {
                throw new Exception('Could not create dir for theme.');
            }
            // - copy existing backgrounds to new map
            if (is_dir($oldPathname))
            {
echo '- old path found<br/>';
                foreach( new DirectoryIterator($oldPathname) as $fileInfo)
                {
                    if ($fileInfo->isDot())
                    {
                        continue;
                    }
echo '- '.$fileInfo->getPathname().' => '.$newPathname.'/'.$fileInfo->getBasename().'<br>';
                    if (!$fileInfo->isFile())
                    {
                        throw new Exception("File '".$fileInfo->getPath()."/".$fileInfo->getFilename()."' is not a file.");
                    }
                    if (!copy($fileInfo->getPathname(), $newPathname.'/'.$fileInfo->getBasename()))
                    {
                        throw new Exception("Could not copy file '".$fileInfo->getPath()."/".$fileInfo->getFilename()."' from theme to '".$newPathname.'/'.$fileInfo->getBasename()."'.");
                    }
                }
            }
        }
        die('klaar');
    }

}