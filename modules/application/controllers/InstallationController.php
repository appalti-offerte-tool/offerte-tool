<?php

class Application_InstallationController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    public function init()
    {
        parent::init();

        $this->_helper->layout->disableLayout(true);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'application';
    }

    public function assertPermissionsAction()
    {
        $this->view->permissions = array(
            '/data/cache'           => 775,
            '/data/cache-public'    => 775,
            '/data/sessions'        => 775,
            '/data/logs'            => 775,
            '/data/proxies'         => 775,
            '/data/tmp'             => 775
        );
    }
}