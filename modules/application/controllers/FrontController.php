<?php

class Application_FrontController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    public function getResourceId()
    {
        return 'guest';
    }

    public function indexAction()
    {}
}