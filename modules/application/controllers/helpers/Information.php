<?php

class Application_Controller_Helper_Information extends Zend_Controller_Action_Helper_Abstract
{
    protected $_callbacks = array();

    public function __construct()
    {
        foreach(array('ContextSwitch', 'AjaxContext') as $hName) {

            if (! Zend_Controller_Action_HelperBroker::hasHelper($hName)) {
                continue;
            }

            $cs = Zend_Controller_Action_HelperBroker::getExistingHelper($hName);

            $trigger = constant(get_class($cs) . '::TRIGGER_POST');
            foreach(array_keys($cs->getContexts()) as $context) {

                $callback = $cs->getCallback($context, $trigger);
                if (!empty($callback)) {
                    $this->_callbacks[] = array(get_class($cs), $callback);
                }

                $cs->setCallback(
                    $context,
                    $trigger,
                    array($this, 'onContextSwitchTriggerPostCallback')
                );
            }
        }
    }

    /**
     * Remember information in session
     *
     * @param $message
     */
    public function setInformation($message, $isTranslated = false, $type = null)
    {
        if (!is_array($message)) {
            $message = array($message);
        }

        /**
         * We expecting here a one message with arguments
         */
        if (isset($message[0]) && is_string($message[0]) && isset($message[1]) && is_array($message[1])) {
            $message = array(new OSDN_Exception_Message($message[0], $message[1]));
        }

        $fm = $this->getActionController()->getHelper('FlashMessenger');
        $fm->setNamespace('message-information');

        foreach($message as $m) {

            if (is_string($m)) {
                $m = new OSDN_Exception_Message($m);
            } elseif (! $m instanceof OSDN_Exception_Message) {
                throw new OSDN_Exception('Unable to add information');
            }

            if ($isTranslated) {
                $m->markAsTranslated();
            }

            if (null !== $type) {
                $m->setType($type);
            }

            $fm->addMessage($m);
        }

        return $this;
    }

    /**
     * Strategy pattern: proxy to setInformation()
     *
     * @return OSDN_Controller_Action_Helper_Information
     */
    public function direct()
    {
        return call_user_func_array(array($this, 'setInformation'), func_get_args());
    }

    public function onContextSwitchTriggerPostCallback()
    {
        $f = Zend_Controller_Front::getInstance();

        $fm = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
        $fm->setNamespace('message-information');

        if ($fm->hasMessages() || $fm->hasCurrentMessages()) {
            $view = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer')->view;

            $messages = $fm->getCurrentMessages();
            if (!$this->getRequest()->isXmlHttpRequest()) {
                $messages = array_merge($messages, $fm->getMessages());
            }

            $fm->clearMessages();
            $fm->clearCurrentMessages();

            $output = array();
            foreach($messages as $message) {
                if (in_array($message->getType(), array(E_USER_ERROR, E_USER_WARNING))) {
                    $view->success = false;
                }

                $output[] = $message->toArray();
            }
        }

        if (!empty($output)) {
            $view->messages = $output;
        }

        foreach(array('ContextSwitch', 'AjaxContext') as $hName) {

            if (! Zend_Controller_Action_HelperBroker::hasHelper($hName)) {
                continue;
            }

            $cs = Zend_Controller_Action_HelperBroker::getExistingHelper($hName);

            if ('json' == $cs->getCurrentContext()) {
                $cs->postJsonContext();
            }
        }
    }
}