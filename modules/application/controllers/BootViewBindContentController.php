<?php

require_once APPLICATION_PATH . '/library/jsmin-php/jsmin-1.1.1.php';

class Application_BootViewBindContentController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout(true);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'guest';
    }

    public function javascriptAction()
    {
        $layout = $this->_getParam('layout');
        $this->getResponse()->setHeader('Content-type', 'text/javascript');

        $path = APPLICATION_PATH . '/layouts/scripts/default/' . $layout . '/_resources/resource.xml';

        set_time_limit(3600);

        $bind = new OSDN_View_Bind($path);
        $includes = $bind->getIncludes(OSDN_View_Bind_Js::TYPE);

        $output = array();
        $baseUrl = $this->view->baseUrl();

        foreach($includes as $include) {

            if (!$include->isCacheable()) {
                continue;
            }

            $include->setBaseUrl($baseUrl);
            $jsContent = $include->toString();

            if ($include->isMinifiable()) {
                $jsContent = JSMin::minify($jsContent);
            }

            $output[] = $jsContent;
        }

        $content = join("\n", $output);

        $version = OSDN_Version::getInstance()->getVersion();

        $token = APPLICATION_PATH . '/data/public-bind-cache/bind-' . $layout . '.js';
        file_put_contents($token, $content);

        echo $content;
    }

    public function stylesheetAction()
    {
        $layout = $this->_getParam('layout');

        $this->getResponse()->setHeader('Content-type', 'text/css');
        $bind = new OSDN_View_Bind(APPLICATION_PATH . '/layouts/scripts/default/' . $layout . '/_resources/resource.xml');
        $includes = $bind->getIncludes(OSDN_View_Bind_Css::TYPE);

        $output = array();

        foreach($includes as $include) {
            if ($include->isConditional()) {
                continue;
            }
            $include->setBaseUrl('');
            $include->setRootDirectory(APPLICATION_PATH);

            $cssInclude = $include->toString();

            if ($include->isMinifiable()) {
                $cssInclude = trim($cssInclude);
                $cssInclude = str_replace("\r\n", "\n", $cssInclude);
                $search = array("/\/\*[^!][\d\D]*?\*\/|\t+/","/\s+/", "/\}\s+/");
                $replace = array(null," ", "}\n");
                $cssInclude = preg_replace($search, $replace, $cssInclude);
                $search = array("/;[\s+]/","/[\s+];/","/\s+\{\\s+/", "/\\:\s+\\#/", "/,\s+/i", "/\\:\s+\\\'/i","/\\:\s+([0-9]+|[A-F]+)/i","/\{\\s+/","/;}/");
                $replace = array(";",";","{", ":#", ",", ":\'", ":$1","{","}");
                $cssInclude = preg_replace($search, $replace, $cssInclude);
                $cssInclude = str_replace("\n", null, $cssInclude);
            }

            $output[] = $cssInclude;
        }

        $content = join("\n", $output);

        $version = OSDN_Version::getInstance()->getVersion();
        $token = APPLICATION_PATH . '/data/public-bind-cache/bind-' . $layout . '.css';
        file_put_contents($token, $content);

        echo $content;
    }
}