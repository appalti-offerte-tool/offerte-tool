<?php

class Application_InformationController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    public function init()
    {
        parent::init();

        $this->_helper->layout->setLayout('administration');
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'application:core';
    }

    public function indexAction()
    {}

    public function phpInfoAction()
    {
        $this->_helper->layout->disableLayout();
    }
}