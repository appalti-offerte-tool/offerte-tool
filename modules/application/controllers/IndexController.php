<?php

class Application_IndexController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    public function getResourceId()
    {
        return 'application:core';
    }

    public function indexAction()
    {}
}