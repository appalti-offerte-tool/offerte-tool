<?php

/**
 * Calculate summary, save/retrieve from cache object
 *
 * @category OSDN
 * @package OSDN_Application
 * @subpackage OSDN_Application_Module
 */
abstract class Application_Instance_Summary
{
    protected $_name;

    protected $_module;

    protected $_title;

    protected $_value;

    protected $_href;

    /**
     * Is this summary disabled
     *
     * @var boolean
     */
    protected $_disabled;

    /**
     * @var Zend_Translate
     */
    protected $_translator;

    /**
     * Default translator
     *
     * @var Zend_Translate
     */
    protected static $_defaultTranslator;

    /**
     * Is class already initialized with value?
     *
     * @var boolean
     */
    private $_isInitialized = false;

    /**
     * Internal cache
     *
     * @var Zend_Cache_Core
     * @access private
     */
    private static $_cache = null;

    /**
     * Internal value to remember if cache supports tags
     *
     * @var boolean
     */
    private static $_cacheTags = false;

    /**
     * Internal option, cache disabled
     *
     * @var     boolean
     * @access  private
     */
    private static $_cacheDisabled = false;

    /**
     * Initialize data
     */
    public function __construct()
    {
        $cls = explode('_', get_class($this));

        $camelCaseToDash = new Zend_Filter_Word_CamelCaseToDash();
        $this->_module = $camelCaseToDash->filter($cls[0]);
        $this->_name = $camelCaseToDash->filter($cls[2]);
    }

    abstract protected function _bind();

    protected function _setHref($module, $controller = null, $action = null, array $params = array())
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $this->_href = $router->assemble(array(
            'module'    => $module,
            'controller'=> $controller,
            'action'    => $action
        ) + $params, null, true);
        return $this;
    }

    private function _initValue()
    {
        if ($this->_isInitialized) {
            return $this;
        }

        $cacheId = '_' . md5(get_class($this));
        if (
            self::hasCache()
            && false !== ($result = self::getCache()->load($cacheId))
        ) {
            foreach($result as $field => $value) {
                $this->$field = $value;
            }

            $this->_isInitialized = true;
            return $this;
        }

        $this->_bind();

        if (self::hasCache()) {
            $o = array();
            foreach($this->__sleep() as $field) {
                $o[$field] = $this->$field;
            }

            self::getCache()->save($o, $cacheId);
        }

        $this->_isInitialized = true;
        return $this;
    }

    public function getName()
    {
        $this->_initValue();
        return $this->_name;
    }

    public function getTitle()
    {
        $this->_initValue();
        return $this->_title;
    }

    public function getValue()
    {
        $this->_initValue();
        return $this->_value;
    }

    public function getHref()
    {
        $this->_initValue();
        return $this->_href;
    }

    public static function setDefaultTranslator(Zend_Translate $translator)
    {
        self::$_defaultTranslator = $translator;
    }

    public function setDisabled($flag)
    {
        $this->_disabled = (boolean) $flag;
    }

    public function isDisabled()
    {
        return $this->_disabled;
    }

    public function getModuleName()
    {
        $this->_initValue();
        return $this->_module;
    }

    /**
     * Get the default translator
     *
     * @return Zend_Translate
     */
    public static function getDefaultTranslator()
    {
        return self::$_defaultTranslator;
    }

    public function setTranslator(Zend_Translate $translator)
    {
        $this->_translator = $translator;
    }

    public function isActive()
    {
        $f = Zend_Controller_Front::getInstance();

        /**
         * @todo
         *
         * In the system can be more that one route
         * Add additional checking
         */
        $route = $f->getRouter()->getCurrentRoute();

        if (! $result = $route->match($this->getHref())) {
            return false;
        }

        $diff = array_diff_assoc($result, $f->getRequest()->getParams());
        return empty($diff);
    }

    /**
     * Get the translator
     *
     * @throws OSDN_Exception When no translator can be found
     * @return Zend_Translate
     */
    public function getTranslator()
    {
        if ($this->_translator !== null) {
            return $this->_translator;
        } if (null !== ($translator = self::getDefaultTranslator())) {
            return $translator;
        } else {
            try {
                $translator = Zend_Registry::get('Zend_Translate');
            } catch (Zend_Exception $e) {
                $translator = null;
            }

            if ($translator instanceof Zend_Translate) {
                $this->_translator = $translator;
                return $translator;
            }
        }

        throw new OSDN_Exception('Could not find a translator');
    }

    public function __sleep()
    {
        return array('_name', '_title', '_value', '_module', '_href');
    }

    /**
     * Returns the set cache
     *
     * @return Zend_Cache_Core The set cache
     */
    public static function getCache()
    {
        return self::$_cache;
    }

    /**
     * Returns true when a cache is set
     *
     * @return boolean
     */
    public static function hasCache()
    {
        return null !== self::$_cache;
    }

    /**
     * Removes any set cache
     *
     * @return void
     */
    public static function removeCache()
    {
        self::$_cache = null;
    }

    /**
     * Disables the cache
     *
     * @param boolean $flag
     */
    public static function disableCache($flag)
    {
        self::$_cacheDisabled = (boolean) $flag;
    }

    /**
     * Set a cache
     *
     * @param Zend_Cache_Core $cache A cache frontend
     */
    public static function setCache(Zend_Cache_Core $cache)
    {
        self::$_cache = $cache;
        self::_getTagSupportForCache();
    }

    /**
     * Internal method to check if the given cache supports tags
     *
     * @param Zend_Cache $cache
     */
    private static function _getTagSupportForCache()
    {
        $backend = self::$_cache->getBackend();
        if ($backend instanceof Zend_Cache_Backend_ExtendedInterface) {
            $cacheOptions = $backend->getCapabilities();
            self::$_cacheTags = $cacheOptions['tags'];
        } else {
            self::$_cacheTags = false;
        }

        return self::$_cacheTags;
    }
}