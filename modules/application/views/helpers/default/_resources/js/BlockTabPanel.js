/**
 * @FIXME conflict with layout
 */
//Ext.History.hashCode = function(str) {
//    if (str.length == 0) return;
//    var hash = 0, char;
//    for (i = 0; i < str.length; i++) {
//        char = str.charCodeAt(i);
//        hash = 31 * hash + char;
//        hash = hash & hash;
//    }
//    return hash;
//};
//
//Ext.History.init();

Ext.ns('Module.Application');

Ext.define('Module.Application.BlockTabPanel', {
    extend: 'Ext.tab.Panel', /*Ext.ux.panel.DDTabPanel, */

    enableTabScroll: true,

//    inPopup: false,

    keyword: 'common',

//    pActionable: null,

    invokeArgs: null,

    autoLoadFirstBlock: true,
    bodyCls: 'x-docked-noborder-top',

    initComponent: function() {

        this.defaults = Ext.applyIf(this.defaults || {}, {
            extend: 'Module.Application.Block',
            autoScroll: true,
            xtab: true
        });


        /**
         * @FIXME Conflict with order tabs in order process
         */
//        this.hash = Ext.History.hashCode(location.pathname),
//
//        this.regExp = new RegExp(this.hash + '/tab\\/(\\d)', 'i');
//
//        this.activeTab = this.getActiveFromHash();
//
//        this.listeners = {
//            afterrender: function() {
//                this.inPopup = this.getEl().parent('.x-window');
//                this.setActiveTab(this.getActiveFromHash());
//                if (!this.inPopup) {
//                    Ext.History.on('change', function(token) {
//                        this.setActiveTab(this.getActiveFromHash(token));
//                    }, this);
//                }
//            },
//            tabchange: function(tabPnl, pnl) {
//
//                if (!this.inPopup) {
//                    var currentId = pnl.getItemId(),
//                        activeTab = this.getActiveFromHash(),
//                        hash = location.hash;
//
//                    if (currentId) {
//                        tabPnl.items.each(function(item, idx) {
//                            if (item.getItemId() == currentId) {
//                                activeTab = idx;
//                            }
//                        });
//                    }
//
//                    if (hash || activeTab > 0) {
//                        var hashVal = this.hash + '/tab/' + activeTab;
//                        location.hash = hash ? hash.replace(this.regExp, hashVal) : hashVal;
//
//                        var oldToken = Ext.History.getToken();
//                        if (oldToken === null || oldToken.search(hashVal) === -1) {
//                            Ext.History.add(hashVal);
//                        }
//                    }
//                }
//
//            },
//            scope: this
//        }

//        this.pActionable = new Ext.ux.plugins.TabPanel.Actionable({
//            keyword: this.keyword
//        });
//
//        this.plugins = [this.pActionable];

        this.callParent(arguments);

        this.on('afterrender', function(me, o) {
            var tabs = this.el.query('div.x-tab');
            for(var i = 0, len = tabs.length; i < len; i++){
                var tab = tabs[i];
                var title = tab.getAttribute('title');
                tab.removeAttribute('title');

                me.add({
                    xtype: 'module.application.block',
                    title: title,
                    contentEl: tab
                });
            }
        });

        this.addEvents(
            /**
             * Fires when some block change invoke args
             * for example, when upload file but entity for that is not jet created
             * The list is not completed. It means that not all params changed.
             *
             * @param {Module.Application.BlockTabPanel}
             * @param {Object} The list of changed invoke args
             * @return false Will stop futher execution
             */
            'backgroundinvokeargschanged'
        );
    },

//    getActiveFromHash: function(token) {
//        var active = 0;
//
//        if (this.inPopup) {
//            return active;
//        }
//
//        var hash = token ? token : location.hash;
//        if (hash) {
//            hash = token ? hash : hash.split('#')[1];
//            var match = hash.match(this.regExp);
//            if (match) {
//                match = match[1];
//                active = match ? parseInt(match) : active;
//            }
//        }
//
//        return active;
//    },

    add: function(c) {

        if (!c.id) {
            var el = Ext.get(c.contentEl);
            c.id = el.getAttribute('data-id');
        }

        return Module.Application.BlockTabPanel.superclass.add.apply(this, arguments);
    },

    reload: function(params, callbackFn) {

        Ext.Ajax.request({
            url: link('default', 'block', 'fetch-all', {format: 'json'}),
            params: Ext.ux.OSDN.encode(params, true),
            success: function(response, options) {
                var decResponse = Ext.decode(response.responseText);

                var redundant = [];
                var aBlocks = [];
                this.items.each(function(tab) {
                    redundant.push(tab);
                });

//                this.beginUpdate();
                Ext.each(decResponse.rowset, function(row) {

                    var hasBlock = false;
                    this.items.each(function(block) {

                        if (block.module == row.module && block.name == row.block) {

                            aBlocks.push(block);

                            var iBlock = redundant.indexOf(block);
                            if (-1 != iBlock) {
                                redundant.splice(iBlock, 1);
                            }

                            if (false == this.fireEvent('beforeunhide', this, block)) {
                                block.tab.hide();
                            } else {
                                if (!block.tab.isVisible()) {
                                    block.tab.show();
                                }
                            }

                            hasBlock = true;
                            return false;
                        }
                    }, this);

                    if (hasBlock) {
                        return;
                    }

                    var b = this.add({
                        id: row.id,
                        xtype: 'module.application.block',
                        iconCls: row.icon,
                        autoScroll: true,
                        module: row.module,
                        title: row.title,
                        hash: row.hash,
                        name: row.block,
                        mappings: row.mappings,
                        invokeArgs: this.invokeArgs || null
                    });

                    aBlocks.push(b);

                }, this);

//                this.endUpdate();
                this.fireEvent('loadblocks', this, aBlocks);

                Ext.each(redundant, function(tab) {
                    tab.tab.hide();
                    tab.hide();
                }, this);

                var aBlock = this.getActiveTab();
                if (null != aBlock && !aBlock.tab.isVisible()) {
                    this.setActiveTab(null);
                    aBlock.hide();
                }

                if (Ext.isFunction(callbackFn)) {
                    callbackFn(this);
                }
                this.fireEvent('blockrefresh', this);

                if ( ! this.getActiveTab()) {
                    this.items.each(function(b) {
                        if (b.tab.isVisible()) {
                            this.setActiveTab(b);
                            return false;
                        }
                    }, this);
                }

                if (true === this.autoLoadFirstBlock) {
                    var b = this.getActiveTab();
                    b && b.reload();
                    b && b.doLayout();
                }
            },
            scope: this
        });
    },

    setKeyword: function(keyword) {
        this.keyword = keyword;
        this.pActionable.setKeyword(this.keyword);
        return this;
    },

    fireChangedInvokeArgs: function(ia) {
        if (false === this.fireEvent('backgroundinvokeargschanged', this, ia)) {
            return false;
        }

        this.items.each(function(block) {
            block.onBackgroundRefreshInvokeArgs(ia);
        });
    }
});