Ext.define('Module.Application.Block',  {
    extend: 'Ext.panel.Panel',
    alias: 'widget.module.application.block',

    module: 'default',

    name: '',

    /**
     * @param {Object}
     */
    invokeArgs: null,

    /**
     * @param {Object}
     */
    baseParams: null,

    layout: 'fit',

    xtab: false,

    autoLoadPkg: null,

    mappings: null,

    transaction: null,

    cls: 'x-osdn-block-obj',

    isLoaded: false,

    isForceInitReload: false,

    initComponent: function() {

        this.baseParams = {};

        if (Ext.isEmpty(this.name)) {
            this.on('afterrender', function() {
                var xtab = this.getEl().down('.x-tab');
                this.module = xtab.getAttribute('data-module');
                this.name = xtab.getAttribute('data-block');
                this.mappings = Ext.decode(xtab.getAttribute('data-mappings'));

                if (this.mappings.length == 0) {
                    this.mappings.push({
                        from: 'id',
                        to: 'id'
                    });
                }
                this.autoLoadPkg = xtab.getAttribute('data-package') || false;
                this.isLoaded = !! parseInt(xtab.getAttribute('data-loaded'), 10);
                this.isForceInitReload = !! parseInt(xtab.getAttribute('data-force'), 10);
//                this.setIconClass(xtab.getAttribute('data-icon') || '');

            }, this);
        } else {
            this.setInvokeArgs(this.invokeArgs || {});
        }

        this.callParent(arguments);
    },

    setInvokeArgs: function(args) {
        this.invokeArgs = {};
        for(var i = 0; i < this.mappings.length; i++) {
            if (!this.mappings[i].from && this.mappings[i].value) {
                this.invokeArgs[this.mappings[i].to] = this.mappings[i].value;
            } else {
                this.invokeArgs[this.mappings[i].to] = args[this.mappings[i].from];
            }
        }

        return this;
    },

    reload: function(callback, force) {

        /**
         * Prevent loading when invoke args is not defined jet
         */
        if (null == this.invokeArgs && true !== force) {
            if (!this.isForceInitReload) {
                return;
            } else {
                this.isForceInitReload = false;
            }
        }

        var hasChanges = false;
        var oc = function(o) {
            var c = 0;
            for(var i in o) {
                if (o.hasOwnProperty(i)) {
                    c++;
                }
            }
            return c;
        };

        if (false === (hasChanges = !(oc(this.invokeArgs) == oc(this.baseParams)))) {
            for(var i in this.invokeArgs) {
                if (
                    !this.baseParams.hasOwnProperty(i)
                    || this.baseParams[i] !== this.invokeArgs[i]
                ) {
                    hasChanges = true;
                    break;
                }
            }
        }

        if (false === hasChanges && true !== force && this.isLoaded) {
            return this;
        }

        this.baseParams = this.invokeArgs;
        if (false === this.fireEvent('beforeload', this, this.baseParams, callback)) {
            return this;
        }

        if (this.hasTransaction()) {
//            Ext.Ajax.abort(this.transaction.id);
            return this;
        }

        this.getEl().mask(Ext.LoadMask.prototype.msg, 'x-mask-loading');

        if (!Ext.isFunction(callback)) {
            callback = Ext.emptyFn;
        }

        this.transaction = Ext.Ajax.request({
            url: link(this.module, this.name, null, {_bid: this.id}, 'block'),
            params: this.baseParams,
            method: 'GET',
            callback: function(options, success, response) {
                this.getEl().unmask();

                if (false !== this.fireEvent('beforeupdate', this, response.responseText, callback)) {
                    this.update(response.responseText, true);
                }

                this.transaction = null;
                this.isLoaded = true;
                this.fireEvent('load', this);
            },
            scope: this
        });
    },

    getBaseParam: function(param, value) {
        if (this.baseParams.hasOwnProperty(param)) {
            return this.baseParams[param];
        }

        return value;
    },

    hasTransaction: function() {
        return null !== this.transaction;
    },

    isValid: function() {
        return true;
    },

    /**
     * Abstract method
     * Used by childrens for retrieve data from blocks
     *
     * @returns {Object}
     */
    toValues: function() {
        return {};
    },

    onBackgroundRefreshInvokeArgs: Ext.emptyFn
});