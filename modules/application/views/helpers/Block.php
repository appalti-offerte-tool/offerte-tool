<?php

class Application_View_Helper_Block extends Zend_View_Helper_Abstract
{
    protected $_block = false;

    public function block($name, array $args = array(), $module = null, $acl = null)
    {
        $this->_block = false;

        if (empty($args['_bid'])) {
            $args['_bid'] = \base64_encode(\Zend_Json::encode(array($name, $args, $module)));
        }

        $f = Zend_Controller_Front::getInstance();
        if (null === $module) {
            $module = $f->getRequest()->getModuleName();
        }

        $mm = $f->getParam('bootstrap')->getResource('ModulesManager');
        $information = $mm->fetch($module);

        $o = array();
        $permissionFn = null;
        if (null !== $acl) {
            if (is_string($acl)) {
                $o['acl'] = array($acl);
            } elseif (is_bool($acl)) {
                $permissionFn = function() use ($acl) {
                    return $acl;
                };
            } elseif (is_callable($acl)) {
                $permissionFn = $acl;
            }
        }

        if (null == ($block = $information->getBlock($name, $args, $o, $permissionFn))) {
            return $this;
        }

        $block->setView(clone $this->view);
        $this->_block = $block;

        return $this;
    }

    public function getBlockObj()
    {
        return $this->_block;
    }

    public function toString()
    {
        if (false === $this->_block) {
            return 'Unable to initialize block';

        }

        $this->view->block = $this->getBlockObj();

        return $this->view->render('block.phtml');
    }

    public function __toString()
    {
        try {
            $output = $this->toString();
        } catch(Exception $e) {
            $output = sprintf('%s::%s - %s', __CLASS__, __METHOD__, $e->getMessage());
        }

        if (!is_string($output)) {
            $output = sprintf('Unable to render block "%s"', $this->_name);
        }

        return $output;
    }

    public function setView(Zend_View_Interface $view)
    {
        parent::setView($view);

        $basePath = __DIR__;
        $this->view->addScriptPath($basePath . '/default');

        return $this;
    }
}