<?php

class Application_View_Helper_Blocks
{
    /**
     * @var Zend_View
     */
    protected $_view;

    /**
     * List of allowed keywords
     *
     * @var array
     */
    protected $_keywords = array();

    protected $_invokeArgs = array();

    protected $_blocks = array();

    protected $_template;

    /**
     * Init blocks collection
     *
     * @param string|array $keywords    List of keywords
     * @param array $params             List of arguments, will be initialized with block
     * @param string $view              Default layout view
     * @param boolean $instance         If true new instance of helper will be created
     *                                  Very handy for nested helpers
     *
     * @throws InvalidArgumentException
     */
    public function blocks($keywords, array $params = array(), $view = 'tabs', $instance = false)
    {
        if (true === $instance) {
            $b = new self();
            $b->setView($this->_view);
            return $b->blocks($keywords, $params, $view);
        }

        if (is_string($keywords)) {
            $keywords = array($keywords);
        } elseif (!is_array($keywords)) {
            throw new InvalidArgumentException(sprintf('Keywords with type "%s" is not allowed.', gettype($keywords)));
        }

        $this->_keywords = $keywords;
        $this->_invokeArgs = $params;
        $this->_template = $view;

        $this->_postInit();

        return $this;
    }

    protected function _postInit()
    {
        $f = Zend_Controller_Front::getInstance();
        $mm = $f->getParam('bootstrap')->getResource('ModulesManager');

        $module = $f->getRequest()->getModuleName();

        $modules = $mm->fetchAll();
        foreach($modules as $moduleName => $information) {

            $blocks = $information->getBlocks($module, $this->_keywords, $this->_invokeArgs);
            if (!empty($blocks)) {
                $this->_blocks = array_merge($this->_blocks, $blocks);
            }
        }

        /**
         * Initialize clone view in each block
         */
        foreach($this->_blocks as $bk => $block) {

            $view = clone $this->_view;
            $block->setView($view);

            $title = $block->toTitle();
            if (empty($title)) {
                unset($this->_blocks[$bk]);
                continue;
            }
        }

        usort($this->_blocks, function(Application_Block_Abstract $x1, Application_Block_Abstract $x2) {
            $p1 = $x1->getPriority();
            $p2 = $x2->getPriority();

            if ($p1 == $p2) {
                return strcasecmp($x1->toTitle(), $x2->toTitle());
            }

            return $p1 > $p2 ? -1 : 1;
        });

        return $this;
    }

    public function toArray()
    {
        return $this->_blocks;
    }

    public function toString()
    {
        /**
         * @FIXME
         *
         * Needs to render like anonymous
         */
        if (!is_string($this->_template)) {

            ob_start();

            foreach($this->_blocks as $block) {
                echo $block->toHtml();
            }
            return ob_get_clean();
        }

        $this->_view->addScriptPath(__DIR__ . '/default/blocks-skins');
        $this->_view->blocks = $this->_blocks;

        return $this->_view->render($this->_template . '.phtml');
    }

    public function __toString()
    {
        try {
            $output = $this->toString();
        } catch(Exception $e) {
            $output = sprintf('%s::%s - %s', __CLASS__, __METHOD__, $e->getMessage());
        }

        if (!is_string($output)) {
            $output = sprintf('Unable to render block "%s"', $this->_name);
        }

        return $output;
    }

    public function allowOutputBlockHtml($flag)
    {
        $this->_view->allowOutputBlockHtml = (boolean) $flag;
        return $this;
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->_view = $view;
    }
}