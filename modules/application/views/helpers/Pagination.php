<?php

class Application_View_Helper_Pagination
{
    /**
     * View instance
     *
     * @var Zend_View_Instance
     */
    public $view = null;

    protected $_pagination;

    protected $_style = 'basic';

    protected $_skinPath = 'pagination';

    protected $_theme = 'default';

    public function __construct()
    {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $viewResource = $bootstrap->getPluginResource('view');
        $viewOptions = $viewResource->getOptions();

        $this->_theme = 'default';
        if (!empty($viewOptions['theme'])) {
            $this->_theme = $viewOptions['theme'];
        }
    }

    /**
     * Sets the view instance.
     *
     * @param  Zend_View_Interface $view View instance
     * @return Zend_View_Helper_PaginationControl
     */
    public function setView(Zend_View_Interface $view)
    {
        $view = clone $view;

        $path = __DIR__ . DIRECTORY_SEPARATOR . '%s/pagination' . DIRECTORY_SEPARATOR . 'skins';

        $view->setBasePath(__DIR__);
        $view->addScriptPath(sprintf($path, 'default'));
        if ('default' != $this->_theme) {
            $view->addScriptPath(sprintf($path, $this->_theme));
        }

        $this->view = $view;
        return $this;
    }

    public function pagination(OSDN_Pagination $pagination = null, array $options = array())
    {
        if (is_null($pagination) && $this->view->pagination instanceof OSDN_Pagination) {
            $this->_pagination = $this->view->pagination;
        } else {
            $this->_pagination = $pagination;
        }

        if (is_null($this->_pagination)) {
            throw new Exception('The pagination object cannot be empty');
        }

        $this->setOptions($options);

        return $this;
    }

    public function setOptions(array $options = array())
    {
        foreach($options as $option => $value) {
            $method = 'set' . ucfirst($option);
            if (method_exists($this, $method)) {
                call_user_func_array(array($this, $method), array($value));
            }
        }
    }

    public function setStyle($style)
    {
        $style = basename($style);
        $themes = array('default');
        if ('default' != $this->_theme) {
            $themes[] = $this->_theme;
        }

        $matched = false;
        foreach($themes as $theme) {
            if (false !== realpath(sprintf(__DIR__ . '/%s/pagination/skins/%s/index.phtml', $theme, $style))) {
                $matched = true;
                break;
            }
        }

        if (false === $matched) {
            throw new Exception(sprintf('Pagination skin "%s" is not found', $style));
        }
        $this->_style = $style;
        return $this;
    }

    public function setSkinPath($path)
    {
        $public = realpath(__DIR__ . '/default/pagination/skins/');
        if (false !== ($tpath = realpath($public . '/' . $path))) {
            $path = $tpath;
            unset($tpath);
        } else {
            $path = realpath($path);
        }

        if (false === $path) {
            $path = func_get_arg(0);
            throw new Exception(sprintf('The path to skins "%s" does not exist in filesystem', $path));
        }

        $result = str_replace($public, '', $path, $count);
        if (empty($result) || $count !== 1) {
            throw new Exception(sprintf('The path "%s" must be above public directory "%s"', $path, $public));
        }


        $this->_skinPath = $path;
        return $this;
    }

    public function getPagesCount()
    {
        return $this->_pagination->getPagesCount();
    }

    public function render()
    {
        $this->view->pagination = $this->_pagination;
        $this->view->plugin = $this;

        return $this->view->render($this->_style . '/index.phtml');
    }

    public function isFirstPage()
    {
        return 1 == $this->_pagination->getCurrentPage();
    }

    public function isLastPage()
    {
        return $this->_pagination->getCurrentPage() == $this->_pagination->getPagesCount();
    }

    public function __toString()
    {
        try {
            return $this->render();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

    }
}