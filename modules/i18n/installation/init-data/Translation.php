<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

class Translation extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");



    }

    public function down(Schema $schema)
    {
        $this->addSql($this->platform->getTruncateTableSQL('translation'));
    }
}
