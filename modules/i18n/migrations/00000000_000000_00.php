<?php

class I18n_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('translation');
        $this->createColumn('translation', 'caption', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('translation', 'en', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('translation', 'nl', self::TYPE_VARCHAR, 255, null, false);

//        foreach(array(
//            array('roleId' => 1, 'resource' => 'i18n', 'privilege' => null),
//            array('roleId' => 1, 'resource' => 'i18n:translation', 'privilege' => null),
//            array('roleId' => 1, 'resource' => 'i18n:country', 'privilege' => null),
//            array('roleId' => 1, 'resource' => 'i18n:language', 'privilege' => null)
//        ) as $o) {
//            $this->insert('aclPermission', $o);
//        }
    }

    public function down()
    {
//        $this->getDbAdapter()->delete('aclPermission', array(
//            'roleId = 1',
//            "(resource = 'i18n' OR resource LIKE 'i18n:%')"
//        ));

        $this->dropTable('translation');
    }
}