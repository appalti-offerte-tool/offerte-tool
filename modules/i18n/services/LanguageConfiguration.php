<?php

class I18n_Service_LanguageConfiguration extends Configuration_Service_ConfigurationAbstract
{
    protected $_identity = 'i18n.language';

    protected $_fields = array(
        'list' => array('default' => 'nl')
    );

    public function fetchCompleteList($locale = null)
    {
        $response = Zend_Locale_Data::getList('en', 'language', 2);

        $instance = $this->toInstance();
        $list = $instance->getList();

        $rowset = array();
        foreach ($response as $key => $value) {
            $rowset[] = array(
                'code' => $key,
                'name'     => $value,
                'isUsed'   => in_array($key, $list)
            );
        }

        return $rowset;
    }

    public function fetchAll($locale = null)
    {
        $rowset = array();
        foreach($this->fetchAllValuePairs($locale) as $key => $value) {
            $rowset[] = array(
                'code'    => $key,
                'name'    => $value
            );
        }

        return $rowset;
    }

    public function fetchAllValuePairs($locale = null)
    {
        $response = Zend_Locale_Data::getList('en', 'language', 2);

        $instance = $this->toInstance();
        $list = $instance->getList();

        foreach ($response as $key => $value) {
            if (!in_array($key, $list)) {
                unset($response[$key]);
            }
        }

        return $response;
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_toValue()
     */
    public function _toValue($value, $field)
    {
        $value = array_unique($value);
        return join(',', $value);
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_fromValue()
     */
    public function _fromValue($value, $field)
    {
        return explode(',', $value);
    }

    public function attach($code)
    {
        $rowset = $this->getRowset();
        if (!isset($rowset['list']) || ! is_array($rowset['list'])) {
            throw new OSDN_Exception('Configuration data is corrupted');
        }

        $rowset['list'][] = $code;
        return $this->update($rowset);
    }

    public function detach($code)
    {
        $rowset = $this->getRowset();
        if (!isset($rowset['list']) || ! is_array($rowset['list'])) {
            throw new OSDN_Exception('Configuration data is corrupted');
        }

        $si = array_search($code, $rowset['list']);
        if (-1 !== $si) {
            unset($rowset['list'][$si]);
        }

        return $this->update($rowset);
    }
}