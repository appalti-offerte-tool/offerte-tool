<?php

class I18n_Service_Translation extends OSDN_Application_Service_Dbable
{
    /**
     * Table object
     *
     * @var I18n_Model_DbTable_Translation
     */
    protected $_table;

    /**
     * The constructor
     *
     */
    protected function _init()
    {
        parent::_init();

        $this->_table = new I18n_Model_DbTable_Translation($this->getAdapter());
    }

    /**
     * Retrieve all traslations collection
     *
     */
    public function fetchAllWithResponse(array $params = array())
    {
        $select = $this->_table->select(true);
        $this->_initDbFilter($select, $this->_table)->parse($params);

        return $this->getDecorator('response')->decorate($select);
    }

    /**
     * Retrieve translations by locale
     *
     * @param string $locale
     * @return OSDN_Response
     */
    public function fetchAllByLocale($locale)
    {
        $language = false !== strpos($locale, '_') ? strstr($locale, '_', true) : $locale;

        if (!$this->_table->hasColumn($language)) {
            $language = 'en';
            /**
             * @FIXME Add logging
             */
//            throw new OSDN_Exception(sprintf('Unknown locale "%s"', $language));
        }

        $columns = array('caption');
        if ('en' == $locale) {
            $columns['translation'] = 'en';
        } else {
            $lColumn = $this->_table->getAdapter()->quoteIdentifier($language);
            $enColumn = $this->_table->getAdapter()->quoteIdentifier('en');
            $columns['translation'] = new Zend_Db_Expr(
                "CASE WHEN $lColumn IS NULL OR $lColumn = '' THEN $enColumn ELSE $lColumn END"
            );
        }

        $select = $this->_table->select(true)->columns($columns);
        return $this->getDecorator('response')->decorate($select);
    }

    /**
     * Create new captions
     * @param object $captions
     * @return
     */
    public function createNewCaptions(array $captions)
    {
        $output = array();

        foreach($captions as $caption) {
            $normalized = $this->_normalizeCaption($caption);
            $row = $this->_table->fetchRow(array(
                'caption = ?'   => $normalized
            ));

            if (empty($row)) {
                $output[] = (boolean) $this->_table->insert(array(
                    'caption'   => $normalized,
                    'en'        => $caption
                ));
            }
        }

        return empty($output) ? true : in_array(false, $output);
    }

    /**
     * Update translation caption
     *
     * @param int $id           The translation id
     * @param string $locale    Used locale (nl, en, ...)
     * @param string $value     The translation caption
     * @return boolean
     */
    public function update($id, $locale, $value)
    {
        $result = $this->_table->updateByPk(array(
            $locale => $value
        ), $id);

        return false !== $result;
    }

    public function updateRow($id, array $data)
    {
        foreach($data as $key => $value) {
            if (!in_array($key, array('en', 'nl'))) {
                unset($data[$key]);
            }
        }

        if (empty($data)) {
            return false;
        }

        return false !== $this->_table->updateByPk($data, $id);
    }

    /**
     * Normalize translation caption to 255 characters
     *
     * @param string $alias
     * @return string       The normalized caption
     */
    protected function _normalizeCaption($alias)
    {
        return md5($alias);
    }

    public function delete($id)
    {
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($id)) {
            return false;
        }

        return false !== $this->_table->deleteByPk($id);
    }

    public function fillEmpty()
    {
        $rows = $this->_table->fetchAll();
        foreach ($rows as $row) {
            if (empty($row->nl)) {
                $row->setFromArray(array(
                    'nl' =>  $row->en
                ));
                $row->save();
            }
        }
    }
}