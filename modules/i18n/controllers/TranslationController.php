<?php

class I18n_TranslationController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    public function init()
    {
        parent::init();

        $this->_helper->ajaxContext()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('update-row', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('autocreate', 'json')
            ->addActionContext('flush-translation-cache', 'json')
            ->initContext();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        $actions = array('init-frontend-translation');
        return in_array($this->getRequest()->getActionName(), $actions) ? 'guest' : 'i18n:translation';
    }

    public function indexAction()
    {
        $this->_helper->layout->setLayout('administration');
    }

    /**
     * Initialize frontend translation manager
     * Access to this action is free
     *
     */
    public function initFrontendTranslationAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/javascript');
        $this->getResponse()->setHeader('Cache-Control', 'public, no-cache');

        $this->view->clearVars();
        $this->_helper->layout->disableLayout();

        $translation = Zend_Registry::get('Zend_Translate');


        $messages = $translation->getMessages();


        array_walk($messages, function($v, $k) {
            $v = str_replace(array("\r", "\n"), '', nl2br($v));
            $k = str_replace(array("\r", "\n"), '', nl2br($k));
        });

        $this->view->messages = $messages;

        /**
         * @FIXME Theme problem
         */
        $this->view->addScriptPath(__DIR__ . '/../views/scripts/default');
    }

    /**
     * Allow autocreate new captions
     *
     */
    public function autocreateAction()
    {
        $service = new I18n_Service_Translation();

        try {
            $result = $service->createNewCaptions(array($this->_getParam('alias')));
            $this->view->success = (boolean) $result;

            $this->_helper->information('Translation has been created', array(), E_USER_NOTICE);
        } catch(Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAllAction()
    {
        $service = new I18n_Service_Translation();
        $response = $service->fetchAllWithResponse($this->_getAllParams());
        $this->view->assign($response->toArray());
    }

    public function updateAction()
    {
        $service = new I18n_Service_Translation();
        try {
            $result = $service->update(
                $this->_getParam('id'),
                $this->_getParam('locale'),
                $this->_getParam('value')
            );
            $this->view->success = (boolean) $result;

            $this->_helper->information('Translation has been updated', array(), E_USER_NOTICE);
        } catch(Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateRowAction()
    {
        $service = new I18n_Service_Translation();
        try {
            $result = $service->updateRow(
                $this->_getParam('id'),
                $this->_getAllParams()
            );
            $this->view->success = (boolean) $result;

            $this->_helper->information('Translation has been updated', array(), E_USER_NOTICE);
        } catch(Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        $service = new I18n_Service_Translation();
        try {
            $result = $service->delete($this->_getParam('id'));
            $this->view->success = (boolean) $result;

            $this->_helper->information('Translation has been deleted', array(), E_USER_NOTICE);
        } catch(Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function flushTranslationCacheAction()
    {
        Zend_Translate::clearCache();

        $this->view->success = true;
    }

    public function copyAction()
    {
        $service = new I18n_Service_Translation();
        try {
            $service->fillEmpty();;
            $this->_helper->information('All translations copied', null, E_USER_NOTICE);
        } catch (\Exception $e) {
            $this->_helper->information($e->getMessages());
        }

        $this->_redirect('/');
    }

}