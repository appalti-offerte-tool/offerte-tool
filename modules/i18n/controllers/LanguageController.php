<?php

class I18n_LanguageController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'i18n:language';
    }

    public function comboBoxAction()
    {
        $service = new I18n_Service_LanguageConfiguration();
        $this->view->rowset = $service->fetchAll();

        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');
    }
}