<?php

class I18n_LanguageConfigurationController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    protected $_service;

    public function init()
    {
        $this->_helper->layout->setLayout('administration');

        $this->_service = new I18n_Service_LanguageConfiguration();

        $this->_helper->ajaxContext()
            ->addActionContext('attach', 'json')
            ->addActionContext('detach', 'json')
            ->addActionContext('fetch-complete-list', 'json')
            ->initContext();

        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'i18n:language';
    }

    public function indexAction()
    {

    }

    public function fetchCompleteListAction()
    {
        $languages = $this->_service->fetchCompleteList();
        $this->view->rowset = $languages;
        $this->view->total = count($languages);
    }

    public function attachAction()
    {
        $this->view->success = (boolean) $this->_service->attach($this->_getParam('code'));
    }

    public function detachAction()
    {
        $this->view->success = (boolean) $this->_service->detach($this->_getParam('code'));
    }
}