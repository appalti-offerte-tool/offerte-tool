Ext.ns('Module.I18n.CountryConfiguration.CountryConfiguration');

Module.I18n.CountryConfiguration.List = Ext.extend(Ext.ux.grid.GridPanel, {

    stateful: true,

    controller: 'country-configuration',

    initComponent: function() {

        this.stateId = 'module.i18n.' + this.controller + '.list';

        var mn = 'Module.I18n.Model.' + this.controller;
        Ext.define(mn, {
            extend: 'Ext.data.Model',
            fields: [
                'name', 'code',
                {name: 'isUsed', type: 'boolean'}
            ]
        });

        this.store = new Ext.data.JsonStore({
            model: mn,
            proxy: {
                type: 'ajax',
                url: link('i18n', this.controller, 'fetch-complete-list', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset',
                    totalProperty: 'total'
                }
            },
            simpleSortMode: true
        });

        this.columns = [{
            header: lang('Name'),
            flex: 1,
            sortable : true,
            dataIndex: 'name'
        }, {
            header: lang('Code'),
            sortable : true,
            dataIndex: 'code'
        }, {
            xtype: 'checkcolumn',
            header: lang('Used'),
            dataIndex: 'isUsed',
            width: 50,
            listeners: {
                checkchange: this.onToggleIsUsedField,
                scope: this
            }
        }];

        this.features = [{
            ftype: 'filters'
        }];

        this.plugins = [Ext.create('Ext.ux.grid.Search', {
            minChars: 2,
            align: 2,
            stringFree: true,
            mode: 'local'
        })];

        this.tbar = new Ext.Toolbar();
        this.bbar = new Ext.Toolbar([
            '->', {
            text: lang('Refresh'),
            iconCls: 'icon-refresh-16',
            qtip: lang('Refresh'),
            handler: function() {
                this.getStore().load();
            },
            scope: this
        }]);

        this.callParent();

        this.on('afteredit', function(o) {
            switch(o.field) {
                case 'isUsed':
                    o.value ? this.onAttach(o.record) : this.onDetach(o.record);
                    break;
            }
        });
    },

    onToggleIsUsedField: function(column, recordIndex, checked, record) {

        var action = checked ? 'attach' : 'detach';
        Ext.Ajax.request({
            url: link('i18n', this.controller, action, {format: 'json'}),
            params: {
                code: record.get('code')
            },
            success: function(response, options) {
                var decResponse = Ext.decode(response.responseText);
                Application.notificate(decResponse.messages);
                if (decResponse.success) {
                    record.commit();
                } else {
                    record.reject();
                }
            }
        });
    }
});