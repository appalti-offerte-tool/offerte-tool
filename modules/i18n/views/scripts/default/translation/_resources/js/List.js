Ext.define('Module.I18n.Translation.List', {

    extend: 'Ext.ux.grid.GridPanel',

    loadMask: true,
    clicksToEdit: 1,

    stateful: true,
    stateId: 'module.i18n.translation.list',

    initComponent: function() {

        this.viewConfig = {
            trackOver: true,
            stripeRows: true,
            plugins: [{
                ptype: 'preview',
                bodyField: 'caption',
                previewExpanded: false,
                pluginId: 'preview'
            }]
        };

        Ext.define('Translation', {
            extend: 'Ext.data.Model',
            fields: [
                'id', 'caption',
                'en', 'nl'
            ]
        });

        this.store = new Ext.data.Store({
            model: 'Translation',
            proxy: {
                type: 'ajax',
                url: link('i18n', 'translation', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset',
                    totalProperty: 'total'
                }
//                ,
//                simpleSortMode: true
            },
            remoteSort: true,
            sorters: {
                property: 'caption',
                direction: 'ASC'
            }
        });

        this.features = [{
            ftype: 'filters'
        }];

        var uButton = Ext.create('Ext.form.field.File', {
            buttonOnly: true,
            hideLabel: true,
            buttonText: '',
            buttonConfig: {
                iconCls: 'icon-create-16 icon-16'
            }
        });

        this.plugins = [Ext.create('Ext.ux.grid.Search', {
            minChars: 2,
            stringFree: true
        })];

        this.tbar = [
            '->', {
                text: lang('Flush translation cache'),
                iconCls: 'icon-cache-16',
                handler: this.onFlushTranslationCache,
                scope: this
            }
        ];

        this.bbar = Ext.create('Ext.PagingToolbar', {
            displayInfo: true,
            store: this.store,
            plugins: 'pagesize',
            items:[
                '-', {
                text: 'Show Preview',
                pressed: false,
                enableToggle: true,
                toggleHandler: function(btn, pressed) {
                    var preview = this.getView().getPlugin('preview');
                    preview.toggleExpanded(pressed);
                },
                scope: this
            }, '-', uButton]
        });

        this.columns = [{
            header: lang('Caption'),
            dataIndex: 'caption',
            sortable: 1,
            filter: {
                type: 'date'
            },
            flex: true,
            renderer: Ext.util.Format.htmlEncode,
            renderer: function(v) {
                return v;
            }
        }, {
            header: lang('English'),
            dataIndex: 'en',
            sortable: 1,
            filter: {
                type: 'search'
            },
            flex: 1,
//            editor: new Ext.form.TextField({
//                allowBlank: false
//            }),
            renderer: function(v) {
//                if (!OSDN.empty(v)) {
//                    v = Ext.util.Format.htmlEncode(v);
//                    v = OSDN.Msg.getQtipSpan(v, v);
//                }
                return v;
            }
        }, {
            header: lang('Dutch'),
            dataIndex: 'nl',
            sortable: true,
            filterable: 1,
            flex: 1,
            editor: new Ext.form.TextField({
                allowBlank: false
            }),
            renderer: function(v) {
//                if (!OSDN.empty(v)) {
//                    v = Ext.util.Format.htmlEncode(v);
//                    v = OSDN.Msg.getQtipSpan(v, v);
//                }
                return v;
            }
        }, {
            xtype: 'actioncolumn',
            header: lang('Actions'),
            width: 50,
            fixed: true,
            items: [{
                tooltip: lang('Edit'),
                iconCls: 'icon-edit-16 icon-16',
                handler: function(gw, rowIndex) {
                    this.onRowEdit(gw.getStore().getAt(rowIndex));
                },
                scope: this
            }, {
                tooltip: lang('Delete'),
                iconCls: 'icon-delete-16 icon-16',
                handler: function(g, rowIndex) {
                    Ext.Msg.confirm(
                        lang('Confirmation'),
                        lang('Translation "{0}" will be removed', g.getStore().getAt(rowIndex).get('caption')),
                        function(b) {
                            'yes' == b && this.onRowDelete(g, rowIndex);
                        },
                        this
                   );
                },
                scope: this
            }]

        }];

        var contextMenu = new Ext.menu.Menu({
            items: [{
                xtype: 'filefield',
                buttonOnly: true,
                hideLabel: true,
                fieldLabel: 'Upload file',
                buttonText: '',
                buttonConfig: {
                    iconCls: 'icon-create-16 icon-16'
                }
            }]
        })

        this.callParent(arguments);

        this.on('itemcontextmenu', function(view, record, item, index, e, options) {
             e.stopEvent();
             contextMenu.showAt(e.xy);
        });

        this.on('afteredit', this.onAfterEdit, this);

        this.on('itemdblclick', function(gw, record) {
            this.onRowEdit(record);
        }, this);
    },

    onAfterEdit: function(e) {

        Ext.Ajax.request({
            url: link('i18n', 'translation', 'update', {format: 'json'}),
            params: {
                id: e.record.get('id'),
                value: e.value,
                locale: e.field
            },
            callback: function(options, success, response) {
                var decResponse = Ext.decode(response.responseText);
                if (success && decResponse && decResponse.success) {
                    e.record.commit();
                } else {
                    e.record.reject();
                }

                Application.notificate(decResponse.messages);
            },
            scope: this
        });
    },

    onRowEdit: function(record) {

        var f = Ext.create('Ext.form.Panel', {
            permissions: true,
            defaultType: 'textfield',
            border: false,
            bodyPadding: 5,
            items: [{
                fieldLabel: lang('Caption'),
                name: 'caption',
                readOnly: true,
                anchor: '-24'
            }, {
                fieldLabel: lang('English'),
                xtype: 'textarea',
                name: 'en',
                height: 100,
                allowBlank: false,
                anchor: '-24'
            }, {
                fieldLabel: lang('Dutch'),
                xtype: 'textarea',
                name: 'nl',
                height: 100,
                allowBlank: true,
                anchor: '-24'
            }]
        });

        var w = new Ext.Window({
            title: lang('Edit translation'),
            modal: true,
            width: 500,
            items: [f],
            buttons: [{
                text: lang('Update'),
                iconCls: 'icon-edit-16',
                handler: function() {
                    this.onRowUpdate(f.getForm(), record, function() {
                        w.close();
                    });
                },
                scope: this
            }, {
                text: lang('Cancel'),
                iconCls: 'icon-cancel-16',
                handler: function() {
                    w.close();
                }
            }],
            scope: this
        });

        w.show(false, function() {
            w.center();
            f.getForm().loadRecord(record);
        });
    },

    onRowUpdate: function(form, record, callback) {
        form.submit({
            url: link('i18n', 'translation', 'update-row', {format: 'json'}),
            waitMsg: lang('Saving...'),
            params: {
                id: record.get('id')
            },
            success: function(options, response) {

                Application.notificate(response.result.messages);
                if (!response.result.success) {
                    return;
                }

                record.set('en', form.findField('en').getValue());
                record.set('nl', form.findField('nl').getValue());
                record.commit();

                if (Ext.isFunction(callback)) {
                    callback();
                }
            },
            error: function() {
                Ext.Msg.alert('Translation saving failed.');
            },
            scope: this
        });
    },

    onRowDelete: function(g, rowIndex, e) {

        var s = g.getStore();
        var record = s.getAt(rowIndex);

        Ext.Ajax.request({
            url: link('i18n', 'translation', 'delete', {format: 'json'}),
            params: {
                id: record.get('id')
            },
            callback: function(options, success, response) {
                var decResponse = Ext.decode(response.responseText);
                Application.notificate(decResponse.messages);

                if (success && decResponse && decResponse.success) {
                    s.remove(record);
                    return;
                }
            },
            scope: this
        });
    },

    onFlushTranslationCache: function() {
        Ext.Ajax.request({
            url: link('i18n', 'translation', 'flush-translation-cache', {format: 'json'}),
            callback: function(options, success, response) {
                var decResponse = Ext.decode(response.responseText);
                Application.notificate(decResponse.messages);
            }
        });
    }
});