Ext.define('Module.I18n.LanguageConfiguration.List', {
    extend: 'Module.I18n.CountryConfiguration.List',

    initComponent: function() {

        this.controller = 'language-configuration';

        this.callParent();
    }
});