<?php

class I18n_View_Helper_Language extends Zend_View_Helper_Abstract
{
    /**
    * Contain value/pairs customer codes
    *
    * @var $_options array
    */
    protected $_options = array();

    /**
     * The constructor
     *
     * Initialize options for combo field
     */
    public function __construct()
    {
        $this->_options['en'] = 'English';
        $this->_options['nl'] = 'Nederlands';
    }

    public function language()
    {
        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs,  $this->_options);
    }

    public function toCaption($value = null)
    {
        if (empty($value)) {
            return '';
        }
        return $this->_options[$value];
    }
}