<?php

class I18n_View_Helper_Country extends Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs titles
     *
     * @var array
     */
    protected $_options = array();

    /**
     * The constructor
     *
     * Initialize options for combo field
     */
    public function __construct()
    {
        $service = new I18n_Service_CountryConfiguration();
        $this->_options = $service->fetchAllValuePairs();

        $this->_options[""] = "";
        usort($this->_options, 'strcasecmp');
    }

    public function country($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs,  $this->_options);
    }
}