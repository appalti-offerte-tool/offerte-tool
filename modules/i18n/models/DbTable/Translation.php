<?php

final class I18n_Model_DbTable_Translation extends OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'translation';
}
