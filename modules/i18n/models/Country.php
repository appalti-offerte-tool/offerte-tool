<?php

/**
 * I18n_Model_Country
 */
final class I18n_Model_Country
{
    private static $_instances = array();

    /**
     * Country code
     *
     * @var string
     */
    private $_code;

    /**
     * Country name
     *
     * @var string
     */
    private $_name;

    private function __construct($code)
    {
        $this->_code = $code;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    public static function toInstance($code)
    {
        if (!isset(self::$_instances[$code])) {
            self::$_instances[$code] = new self($code);
        }

        return self::$_instances[$code];
    }
}