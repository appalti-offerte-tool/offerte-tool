<?php

class Comment_Model_DbTable_CommentRow extends Zend_Db_Table_Row_Abstract implements OSDN_Application_Model_Interface
{
    public function getId()
    {
        return $this->id;
    }

    public function isStatusPrivate()
    {
        return 0 === strcasecmp($this->commentTypeId, Comment_Model_DbTable_Comment::STATUS_PRIVATE);
    }

    public function isStatusPublic()
    {
        return 0 === strcasecmp($this->commentTypeId, Comment_Model_DbTable_Comment::STATUS_PUBLIC);
    }

    public function isStatusEmail()
    {
        return 0 === strcasecmp($this->commentTypeId, Comment_Model_DbTable_Comment::STATUS_EMAIL);
    }

    public function isResponseRequired()
    {
        return 0 === strcasecmp($this->notificatedAction, Comment_Model_DbTable_Comment::RESPONSE_IS_NEEDED);
    }

    public function isNotification()
    {
        return 0 === strcasecmp($this->notificatedAction, Comment_Model_DbTable_CommentRow::NOTIFICATION_ONLY);
    }

    public function hasParent()
    {
        return !empty($this->parentId);
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getBodyText()
    {
        return $this->bodyText;
    }

    public function getCreatorAccountName()
    {
        $accountRow = $this->findParentRow('Account_Model_DbTable_Account', 'Creator');
        return $accountRow->username;
    }

    public function getCreatedDatetime()
    {
        if (empty($this->createdDatetime)) {
            return '';
        }

        $createdDatetime = new DateTime($this->createdDatetime);
        return $createdDatetime->format('d-m-Y H:i:s');
    }

    public function getModifiedAccountName()
    {
        $accountRow = $this->findParentRow('Account_Model_DbTable_Account', 'Modifier');
        return null !== $accountRow ? $accountRow->username : '';
    }

    public function getModifiedDatetime()
    {
        if (!$this->modifiedDatetime) {
            return '';
        }

        $createdDatetime = new DateTime($this->modifiedDatetime);
        return $createdDatetime->format('d-m-Y H:i:s');
    }

    public function getReactionReceived()
    {
        if (!$this->reactionReceived) {
            return '';
        }

        $reactionReceived = new DateTime($this->reactionReceived);
        return $reactionReceived->format('d-m-Y H:i:s');
    }

    public function getNotificatedAccountName()
    {
        $accountRow = $this->findParentRow('Account_Model_DbTable_Account', 'Notifier');
        return null !== $accountRow ? $accountRow->username : '';
    }

    public function getNotificatedEmail()
    {
        return 'Empty email';
    }

    public function getNotificatedAction()
    {
        return $this->notificatedAction;
    }

    public function getCommentTypeId()
    {
        return $this->commentTypeId;
    }

    public function getNotificatedAccountId()
    {
        return $this->notificatedAccountId;
    }

}