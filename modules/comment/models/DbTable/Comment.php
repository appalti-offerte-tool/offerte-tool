<?php

class Comment_Model_DbTable_Comment extends OSDN_Db_Table_Abstract
{
    const RESPONSE_IS_NEEDED    = 'R';

    const NOTIFICATION_ONLY     = 'N';

    /**
     * Disable email and action
     */
    const STATUS_PRIVATE   = 'PRIVATE';

    /**
     * Disable email and action
     */
    const STATUS_PUBLIC    = 'PUBLIC';

    /**
     * Enable email and action
     */
    const STATUS_EMAIL     = 'EMAIL';

    protected $_primary = 'id';

    protected $_name = 'comment';

    protected $_rowClass = 'Comment_Model_DbTable_CommentRow';

    protected $_referenceMap = array(
        'Creator'    => array(
            'columns'       => 'creatorAccountId',
            'refTableClass' => 'Account_Model_DbTable_Account',
            'refColumns'    => 'id'
        ),
        'Modifier'   => array(
            'columns'       => 'modifiedAccountId',
            'refTableClass' => 'Account_Model_DbTable_Account',
            'refColumns'    => 'id'
        ),
        'Notifier'   => array(
            'columns'       => 'notificatedAccountId',
            'refTableClass' => 'Account_Model_DbTable_Account',
            'refColumns'    => 'id'
        ),
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        if (empty($data['createdDatetime'])) {
            $createdDateTime = new DateTime();
            $data['createdDatetime'] = $createdDateTime->format('Y-m-d H:i:s');
            unset($createdDateTime);
        }

        if (empty($data['creatorAccountId'])) {
            $data['creatorAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();
        }

        return parent::insert($data);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
        if (empty($data['modifiedDatetime'])) {
            $modifiedDateTime = new DateTime();
            $data['modifiedDatetime'] = $modifiedDateTime->format('Y-m-d H:i:s');
            unset($modifiedDateTime);
        }

        if (empty($data['modifiedAccountId'])) {
            $data['modifiedAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();
        }

        return parent::update($data, $where);
    }
}