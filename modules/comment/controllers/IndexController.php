<?php

abstract class Comment_IndexController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
    * @var Comment_Service_Comment
    */
    protected $_service;

    protected $_entityTypeId = null;

    public function init()
    {
        if (is_null($this->_entityTypeId)) {
            throw new OSDN_Exception('The entity type is not defined.');
        }

        if (null === $this->_service) {
            $this->_service = new Comment_Service_Comment();
        }

        parent::init();

        $this->_helper->contextSwitch()
             ->addActionContext('list', 'json')
             ->addActionContext('form', 'json')
             ->addActionContext('index', 'json')
             ->addActionContext('create', 'json')
             ->addActionContext('update', 'json')
             ->addActionContext('delete', 'json')
             ->addActionContext('add-reply', 'json')
             ->addActionContext('mark-readed', 'json')
             ->initContext();

    }

    public function formAction()
    {
        $id = $this->_getParam('commentId');
        if (!empty($id)) {
            $row = $this->_service->find($id);
            $this->view->data = $row->toArray();
            $this->view->success = true;
        }

        if ('json' != $this->_getParam('format')) {
            echo $this->view->modules('comment')->partial('index/form.phtml');
            $this->_helper->viewRenderer->setNoRender(true);
        } 
    }

    public function historyAction()
    {
        $parentId = $this->_getParam('parentId');
        $entityId = $this->_getParam('entityId');
        $row = $this->_service->fetchHistory($this->_entityTypeId, $entityId, $parentId);
        $this->view->history = $row;

        if ('json' != $this->_getParam('format')) {
            echo $this->view->modules('comment')->partial('index/history.phtml');
            $this->_helper->viewRenderer->setNoRender(true);
        }
    }

    public function getRepliesAction()
    {
        $id = $this->_getParam('id');
        $entityTypeId = $this->_getParam('entityTypeId');
        $entityId = $this->_getParam('entityId');

        $response = $this->_service->fetchReplies($entityTypeId, $entityId, $id);
        $rowset = $response->getRowset();

        $this->view->assign($rowset);
    }

    public function indexAction()
    {
        $id = $this->_getParam('node');
        $entityId = $this->_getParam('entityId');

        // fetch all commentary
        if ($id == 0) {
            $response = $this->_service->fetchAll($this->_entityTypeId, $entityId);
        } else {
            $response = $this->_service->fetchReplies($this->_entityTypeId, $entityId, $id);
        }

        $rowset = $response->getRowset();
        if (is_array($rowset)) {
            foreach ($rowset as & $row) {
                $row['leaf'] = !isset($row['repliesCount']) || $row['repliesCount'] == 0;
            }
        }

        $this->view->assign($rowset);
//        $this->view->rowset = $rowset;
//        $this->view->success = true;
   }

    public function createAction()
    {
        $params = $this->_getAllParams();
        $params['entityTypeId'] = $this->_entityTypeId;

        try {
            list($commentRow, $entityId) = $this->_service->create($params);

            $this->_helper->information('Comment has been created successfully', true, E_USER_NOTICE);
            $this->view->commentId = $commentRow->getId();
            $this->view->entityId = $entityId;
            $this->view->success = true;

        } catch(OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch(Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessage());
        }
    }

    public function updateAction()
    {
        $params = $this->_getAllParams();
        $params['entityTypeId'] = $this->_entityTypeId;
        $success = false;
        try {
            $success = $this->_service->update($this->_getParam('id'), $params);
            $this->_helper->information('Comment has been updated successfully', true, E_USER_NOTICE);
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        } catch(Exception $e) {
            $this->_helper->information($e->getMessage());
        }
        $this->view->success = (boolean) $success;
    }

    public function deleteAction()
    {
        $params = $this->_getAllParams();
        $params['entityTypeId'] = $this->_entityTypeId;

        $success = false;
        try {
            $success = $this->_service->delete($params);
            $this->_helper->information('Comment has been deleted successfully', true, E_USER_NOTICE);
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        } catch(Exception $e) {
            $this->_helper->information($e->getMessage());
        }

        $this->view->success = (boolean) $success;
    }

    public function fetchAction()
    {
        $commentId = $this->_getParam('commentId');
        $entityId = $this->_getParam('entityId');
        $row = $this->_service->fetchCommentByEntity($this->_entityTypeId, $entityId, $commentId);
        $this->view->row = $row;

        if ('json' != $this->_getParam('format')) {
            echo $this->view->modules('comment')->partial('index/fetch.phtml');
            $this->_helper->viewRenderer->setNoRender(true);
        }
    }

    public function addReplyAction()
    {
        $data = $this->getRequest()->getPost();
        $data['entityTypeId'] = $this->_entityTypeId;

        try {
            list($commentRow, $entityId) = $this->_service->reply($data);
            $this->_helper->information('Reply has been created successfully', true, E_USER_NOTICE);
            $this->view->success = true;
            $this->view->commentId = $commentRow->getId();
        } catch(OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch(Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessage());
        }
    }

    public function fetchParentAction()
    {
        $entityId = $this->_getParam('entity_id');
        $commentId = $this->_getParam('commentary_id');
        $response = $this->_service->fetchParentCommentary($entityId, $this->_entityTypeId, $commentId);
        if (empty ($response)) {
            return;
        }

        $this->view->parent_id = (int) $response->parent_id;
        $this->view->success = true;
    }

    /**
     * Mark script as readed
     *
     * @return void
     */
    public function markReadedAction()
    {
        $success = false;
        try {
            $entityId = $this->_getParam('entityId');
            $commentId = $this->_getParam('commentId');
            $success = $this->_service->markReaded($this->_entityTypeId, $entityId, $commentId);

            $this->_helper->information('Comment has been marked readed successfully', true, E_USER_NOTICE);
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        } catch(Exception $e) {
            $this->_helper->information($e->getMessage());
        }

        $this->view->success = (boolean) $success;
    }

}