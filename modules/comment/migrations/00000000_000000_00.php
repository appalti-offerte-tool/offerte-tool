<?php

class Comment_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('comment');
        $this->createColumn('comment', 'entityId', self::TYPE_INT, 11, null, true);
        $this->createColumn('comment', 'entityTypeId', self::TYPE_VARCHAR, 20, null, true);
        $this->createColumn('comment', 'commentTypeId', self::TYPE_VARCHAR, 10, null, true);
        $this->createColumn('comment', 'parentId', self::TYPE_INT, 11, null, false);
        $this->createColumn('comment', 'title', self::TYPE_VARCHAR, 150, null, false);
        $this->createColumn('comment', 'bodyText', self::TYPE_TEXT, null, null, false);
        $this->createColumn('comment', 'creatorAccountId', self::TYPE_INT, 11, null, true);
        $this->createColumn('comment', 'createdDatetime', self::TYPE_DATETIME, null, null, true);
        $this->createColumn('comment', 'modifiedAccountId', self::TYPE_INT, 11, null, false);
        $this->createColumn('comment', 'modifiedDatetime', self::TYPE_DATETIME, null, null, false);
        $this->createColumn('comment', 'notificatedAccountId', self::TYPE_INT, 11, null, false);
        $this->createColumn('comment', 'notificatedAction', self::TYPE_VARCHAR, 1, null, false);
        $this->createColumn('comment', 'reactionReceived', self::TYPE_DATETIME, null, null, false);

        $this->createIndex('comment', array('notificatedAccountId'), 'IX_notificatedAccountId');
        $this->createForeignKey('comment', array('notificatedAccountId'), 'account', array('id'), 'FK_notificatedAccountId');

        $this->createIndex('comment', array('modifiedAccountId'), 'IX_modifiedAccountId');
        $this->createForeignKey('comment', array('modifiedAccountId'), 'account', array('id'), 'FK_modifiedAccountId');

        $this->createIndex('comment', array('creatorAccountId'), 'IX_creatorAccountId');
        $this->createForeignKey('comment', array('creatorAccountId'), 'account', array('id'), 'FK_creatorAccountId');

        foreach(array(
            array('roleId' => 1, 'resource' => 'comment', 'privilege' => null)
        ) as $o) {
            $this->insert('aclPermission', $o);
        }
    }

    public function down()
    {
        $this->getDbAdapter()->delete('aclPermission', array(
            'roleId = 1',
            "(resource = 'comment' OR resource LIKE 'comment:%')"
        ));

        $this->dropTable('comment');
    }
}