<?php

class Comment_Service_Comment extends OSDN_Application_Service_Dbable
{
    /**
     * @var Comment_Model_DbTable_Comment
     */
    protected $_table = null;

    protected function _init()
    {
        /**
         * @fixme remove adapter
         */
        $this->_table = new Comment_Model_DbTable_Comment($this->getAdapter());

        parent::_init();

        $this->_attachValidationRules('default', array(

            'entityId'       => array('id', 'presence' => 'required'),
            'entityTypeId'   => array(array('StringLength', 1, 20), 'allowEmpty' => false, 'presence' => 'required'),
            'commentTypeId'  => array(array('StringLength', 1, 10), 'allowEmpty' => false),
            'title'          => array(array('StringLength', 0, 150), 'allowEmpty' => true),
            'parentId'       => array('id', 'allowEmpty' => true),
            'bodyText'       => array('allowEmpty' => false),
            'bodyText'       => array('allowEmpty' => false),
            'notificatedAction' => array(array('StringLength', 1, 1), 'allowEmpty' => true),
            'notificatedAccountId' => array('int', 'allowEmpty' => true)
        ));

        $this->_attachValidationRules('update', array(
            'id'     => array('id', 'presence' => 'required'),
        ), 'default');

        $this->_attachValidationRules('update-email-type', array(
           'notificatedAccountId'  => array('int', 'allowEmpty' => false, 'presence' => 'required'),
           'notificatedAction'     => array(array('StringLength', 1, 1), 'allowEmpty' => false, 'presence' => 'required'),
           'commentTypeId'         => array(array('StringLength', 1, 10), 'presence' => 'required')
        ), 'update');

        $this->_attachValidationRules('insert', array(
            'commentTypeId' => array(array('StringLength', 1, 10), 'presence' => 'required'),
        ), 'default');

        $this->_attachValidationRules('insert-email-type', array(
           'notificatedAccountId'  => array('int', 'allowEmpty' => false, 'presence' => 'required'),
           'notificatedAction'     => array(array('StringLength', 1, 1), 'allowEmpty' => false, 'presence' => 'required'),
        ), 'insert');

        $this->_attachValidationRules('reply', array(
            'parentId'     => array('id', 'presence' => 'required'),
        ), 'default');

    }

    protected function _fetchAll(array $whereClause = array(), array $params = array())
    {
        /**
         * @todo change table select to db select
         */
        $accountTable = new Account_Model_DbTable_Account($this->getAdapter());

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('c' => $this->_table->getTableNameWithSchema()),
                array('c.*', 'repliesCount' => new Zend_Db_Expr('(SELECT COUNT(*) FROM comment AS pc WHERE pc.parentId = c.id)'))
            )
            ->joinLeft(
                array('ca' => $accountTable->getTableNameWithSchema()),
                'c.creatorAccountId = ca.id',
                array('creatorAccountName' => 'ca.username')
            )
            ->joinLeft(
                array('ma' => $accountTable->getTableNameWithSchema()),
                'c.modifiedAccountId = ma.id',
                array('modifiedAccountName' => 'ma.username')
            );

        foreach ($whereClause as $cond => $value) {
            if ($value instanceof Zend_Db_Expr) {
                $select->where($value);
                continue;
            }
            $select->where($cond, $value);
        }

        $this->_initDbFilter($select, $this->_table)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        $response->setScope($this);
        $response->setRowsetCallback($this->_onNormalizeCommentRow());

        return $response;
    }

    protected function _onNormalizeCommentRow()
    {
        return function($row, $rowset, $scope) {

            $createdDatetime = new DateTime($row['createdDatetime']);
            $modifiedDatetime = new DateTime($row['modifiedDatetime']);

            return array(
                'id'              => $row['id'],
                'commentId'       => $row['id'],
                'entityId'        => $row['entityId'],
                'entityTypeId'    => $row['entityTypeId'],
                'title'           => $row['title'],
                'bodyText'        => $row['bodyText'],
                'creatorAccountId'=> $row['creatorAccountId'],
                'creatorAccountName'  => $row['creatorAccountName'],
                'modifiedAccountName' => $row['modifiedAccountName'],
                'reactionReceived'    => $row['reactionReceived'],
                'createdDatetime'     => $createdDatetime->format('Y-m-d H:i:s'),
                'modifiedDatetime'    => $modifiedDatetime->format('Y-m-d H:i:s'),
                'notificatedAccountId'=> $row['notificatedAccountId'], //OSDN_Accounts_Prototype::getId();
                'notificatedAction'=> $row['notificatedAction'],
                'repliesCount'=> $row['repliesCount']

//                'repliesCount'        => $scope->getRepliesCount($row['id'])
            );
        };
    }

    /**
     * Fetch commentary for entity type by entity id
     *
     * @param int $entityTypeId
     * @param int $entityId
     * @param array $params
     *
     * @return OSDN_Db_Response_Abstract
     */
    public function fetchAll($entityTypeId, $entityId, array $params = array())
    {
        $validate = new OSDN_Validate_Id();
        if (empty($entityTypeId) || !$validate->isValid($entityId)) {
            throw new InvalidArgumentException('Invalid credentials');
        }

        return $this->_fetchAll(array(
            'entityTypeId = ?' => $entityTypeId,
            'entityId = ?' => $entityId,
            new Zend_Db_Expr('parentId IS NULL')
        ), $params);
    }

    /**
     * Fetch all commentaries by entity type ids
     *
     * @param array $entityTypesIds     The entity types ids
     * @param array $params
     * @return OSDN_Db_Response_Abstract
     */
    public function fetchAllByEntityTypes(array $entityTypesIds, array $params = array())
    {
        if (empty($entityTypesIds)) {
            throw new InvalidArgumentException('Invalid credentials');
        }

        $validate = new OSDN_Validate_Id();
        foreach($entityTypesIds as $entityTypeId) {
            if (empty($entityTypeId)) {
                throw new InvalidArgumentException('Input parameters incorrect');
            }
        }
        return $this->_fetchAll(array(
            'entityTypeId IN (?)' => $entityTypesIds,
            new Zend_Db_Expr('parentId IS NULL')
        ), $params);
    }

    /**
     *
     * @param type $entityTypeId
     * @param type $entityId
     * @param type $commentId
     * @param array $params
     * @return OSDN_Db_Response_Abstract
     */
    public function fetchReplies($entityTypeId, $entityId, $commentId, array $params = array())
    {
        $validate = new OSDN_Validate_Id();
        if (
            empty($entityTypeId)
            || !$validate->isValid($entityId)
            || !$validate->isValid($commentId))
        {
            throw new InvalidArgumentException('Input parameters incorrect');
        }

        return $this->_fetchAll(array(
            'entityTypeId = ?' => $entityTypeId,
            'entityId = ?' => $entityId,
            'parentId = ?' => $commentId
        ), $params);
    }

    /**
     *
     * @param type $entityTypeId
     * @param type $entityId
     * @param type $commentId
     * @return row
     */
    public function fetchCommentByEntity($entityTypeId, $entityId, $commentId)
    {
        $row = $this->_table->fetchRow(array(
            'id = ?'              => $commentId,
            'entityTypeId = ?'    => $entityTypeId,
            'entityId = ?'        => $entityId
        ));

        if (empty($row)) {
            return null;
        }

        return $row;
    }

    /**
     *
     * @param type $entityTypeId
     * @param type $entityId
     * @param type $parentId
     * @return array
     */
    public function fetchHistory($entityTypeId, $entityId, $parentId)
    {
        $validate = new OSDN_Validate_Id();
        if (
            empty($entityTypeId)
            || !$validate->isValid($entityId)
            || !$validate->isValid($parentId))
        {
            throw new InvalidArgumentException('Invalid credentials');
        }

        $history = array();
        $id = $parentId;
        do {
            $commentRow = $this->_table->fetchRow(array(
                'comment.entityTypeId = ?' => $entityTypeId,
                'comment.entityId = ?' => $entityId,
                'comment.id = ?' => $id
            ));
            unset($id); // on every iteration different id
            try {
                if (null != $commentRow) {
                    if ($commentRow->hasParent()) {
                        $id = $commentRow->parentId;
                    }

                    $history[] = $commentRow;
                }

            } catch (Exception $e) {
                throw new OSDN_Exception($e->getMessage()) ;
                break;
            }

        } while (isset($id) && (int) $id);

        return array_reverse($history);
    }

    /**
     * Fetch count comments by entity and entity type
     *
     * @param int $entityTypeId     The entity type id
     * @param int $entityId         The entity id
     * @return array
     * <code> {
     *     'count' => int
     *     'unread'=> int       // the unread calculated only for related account
     * }
     * </code
     */
    public function fetchCount($entityTypeId, $entityId, $withUnreadCount = false)
    {
        $validate = new OSDN_Validate_Id();

        if (
            empty($entityTypeId)
            || !$validate->isValid($entityId))
        {
            throw new InvalidArgumentException('Invalid credentials');
        }

        $select = $this->_table->select(true)
            ->where('entityTypeId = ?', $entityTypeId)
            ->where('entityId = ?', $entityId);

        $columns = array('count' => new Zend_Db_Expr('COUNT(*)'));

        if (true === $withUnreadCount) {
            $reactionReceived = $this->_table->getAdapter()->quoteIdentifier('reactionReceived');
            $notificatedAccountId = $this->_table->getAdapter()->quoteIdentifier('notificatedAccountId');
            $unreadClause = $this->_table->getAdapter()->quoteInto(
                "SUM(CASE WHEN $reactionReceived IS NULL AND $notificatedAccountId = ? THEN 1 ELSE 0 END)",
                Zend_Auth::getInstance()->getIdentity()->getId()
            );

            $columns['unread'] = new Zend_Db_Expr($unreadClause);
        }
        $select->reset(\Zend_Db_Select::COLUMNS)->columns($columns);

        try {
            $query = $select->query();
            $response = $query->fetch();
            $response = \array_map('intval', $response);
        } catch (\Exception $e) {
            throw new OSDN_Exception($e->getMessage());
        }

        return $response;
    }

    /**
     * Add new commentary
     *
     * @param array $data
     *
     * Data may contain following attributes<pre>
     *      entityId           int     required
     *      entityTypeId       int     required
     *      commenttype_id      int     required
     *      title               string  OPTIONAL
     *      bodyText            string  OPTIONAL
     *      parentId           int     OPTIONAL
     *      account_id          int     OPTIONAL       The notificated account id
     *      action_id           int     OPTIONAL
     * </pre>
     * @return array
     */
    public function create(array $data)
    {
        return $this->_insert($data);
    }

    protected function _insert(array $data, Closure $onBeforeInsertCallbackFn = null)
    {
        try {
            $this->getAdapter()->beginTransaction();

            $entityRow = null;
            if (null !== $onBeforeInsertCallbackFn) {
                $entityRow = $onBeforeInsertCallbackFn($data);
                if (null === $entityRow || ! $entityRow instanceof OSDN_Application_Model_Interface) {
                    throw new OSDN_Exception('Unable to create comment entity row');
                }
                $data['entityId'] = $entityRow->getId();
            }

            if (isset($data['commentTypeId']) && Comment_Model_DbTable_Comment::STATUS_EMAIL === $data['commentTypeId']) {
                $f = $this->_validate($data, 'insert-email-type');
                $preparedData = $f->getData();
            } else {
                $f = $this->_validate($data, 'insert');
                $preparedData = $f->getData();
                $null = new Zend_Db_Expr("NULL");
                $preparedData['notificatedAccountId'] = $null;
                $preparedData['notificatedAction'] = $null;
            }

            $commentRow = $this->_table->createRow($preparedData);
            $commentRow->save();

            $this->getAdapter()->commit();

        } catch(\Zend_Db_Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to create comment', 0, $e);
        }

        $notificate = new Comment_Misc_Email_Notification();
        $notificate->notificate(array(
            'commentId'    => $commentRow->getId()
        ));

        return array($commentRow, $preparedData['entityId'], 'commentId' => $commentRow->getId());
    }

    /**
     *
     * @param type $id
     * @param array $data
     * @return boolean
     */
    public function update($id, array $data)
    {
        $this->_table->getAdapter()->beginTransaction();
        try {
            if (null == ($commentRow = $this->_table->findOne($id))) {
                throw new OSDN_Exception('Unable to find comment #' . $id);
            }

            if (isset($data['commentTypeId']) && Comment_Model_DbTable_Comment::STATUS_EMAIL === $data['commentTypeId']) {
                $f = $this->_validate($data, 'update-email-type');
                $commentRow->setFromArray($f->getData());
            } else {
                $f = $this->_validate($data, 'update');
                $commentRow->setFromArray($f->getData());
                $null = new Zend_Db_Expr("NULL");
                $commentRow->notificatedAccountId = $null;
                $commentRow->notificatedAction = $null;
            }

            // check if reply is present
            $count = $this->_table->count(array('parentId = ?' => $commentRow->id));
            if ($count > 0) {
                throw new OSDN_Exception('Unable to change comment. Reply is already made.');
            }
            $commentRow->save();

            $this->_table->getAdapter()->commit();
        } catch(Zend_Db_Exception $e) {
            $this->_table->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to update comment #' . $commentRow->id);
        }

        $notificate = new Comment_Misc_Email_Notification();
        $notificate->notificate(array(
            'commentId'    => $commentRow->id
        ));

        return true;
    }

    /**
     * Delete commentary by id
     *
     * @return boolean
     */
    public function delete(array $data)
    {
        $f = $this->_validate($data, 'update');
        $preparedData = $f->getData();

        $commentId = $preparedData['id'];
        $entityTypeId = $preparedData['entityTypeId'];
        $entityId = $preparedData['entityId'];

        if (false === $this->fetchCommentByEntity($entityTypeId, $entityId, $commentId)) {
            throw new OSDN_Exception('Unable to find comment.');
        }

        $affectedRows = $this->_table->deleteQuote(array(
            'id = ?'           => $commentId,
            'entityTypeId = ?' => $entityTypeId,
            'entityId = ?'     => $entityId
        ));

        return true;
    }

    /**
     * Delete comment by entity
     *
     * @param type $entityTypeId
     * @param type $entityId
     * @return boolean
     */
    public function deleteByEntity($entityTypeId, $entityId)
    {
        $validate = new OSDN_Validate_Id();
        if (
            empty($entityTypeId)
            || !$validate->isValid($entityId))
        {
            throw new InvalidArgumentException('Invalid credentials');
        }

        $affectedRows = $this->_table->deleteQuote(array(
            'entityTypeId = ?' => $entityTypeId,
            'entityId = ?'     => $entityId
        ));

        if (false === $affectedRows) {
            throw new \OSDN_Exception('Unable to delete comments');
        }

        return true;
    }

    /**
     * Create new reply by comment
     *
     * @param array $data
     * @see insert()
     */
    public function reply(array $data)
    {
        $f = $this->_validate($data, 'reply');
        $preparedData = $f->getData();

        $parentId = $preparedData['parentId'];
        $parentCommentary = $this->_table->findOne($parentId);

        if (false === ($parentCommentary = $this->_table->findOne($parentId))) {
            throw new OSDN_Exception('Unable to find parent comment.');
        }

        if (empty($preparedData['title'])) {
            $preparedData['title'] = $parentCommentary->title;
        }

        return $this->create($preparedData);
    }

    /**
     * Update reply
     *
     * @param int $id       The comment
     * @param array $data   Reply data
     * @see update()
     */
    public function updateReply($id, array $data)
    {
        $prepare = array();
        if (isset($data['bodyText'])) {
            array_push($prepare, $data['bodyText']);
        }
        return $this->update($id, $prepare);
    }

    /**
     * Set message status to private
     *
     * @param int $id           message id
     * @return boolean
     */
    public function setPrivate($id)
    {
        $validate = new OSDN_Validate_Id();

        if (!$validate->isValid($id)) {
            throw new InvalidArgumentException('Invalid credentials');
        }

        $affectedRows = $this->_table->updateByPk(array('private' => 1), $id);
        return true;
    }

    /**
     * Fetch replies for commentary
     *
     * @param int $id       The commentary id
     * @return int|false
     */
    public function getRepliesCount($id)
    {
        return $this->_table->count(array('parentId = ?' => $id));
    }

    /**
     * Mark the message readed
     *
     * @param type $entityTypeId
     * @param type $entityId
     * @param type $commentId
     * @return boolean
     */
    public function markReaded($entityTypeId, $entityId, $commentId)
    {
        $validate = new OSDN_Validate_Id();
        if (empty($entityTypeId) ||
            !$validate->isValid($entityId) ||
            !$validate->isValid($commentId))
        {
            throw new InvalidArgumentException('Invalid credentials');
        }

        $dt = new DateTime();
        $reactionReceived = $dt->format('Y-m-d H:i:s');

        $affectedRows = $this->_table->updateQuote(
            array('reactionReceived' => $reactionReceived),
            array(
                'entityTypeId = ?' => $entityTypeId,
                'entityId = ?'     => $entityId,
                'id = ?'           => $commentId,
                new Zend_Db_Expr('reactionReceived IS NULL'),
                'notificatedAccountId = ?' => 77
            )
        );
        return true;
    }

    /**
     * Fetch parent commentary id
     *
     * @param type $entityId
     * @param type $entityTypeId
     * @param type $commentId
     * @return row|int
     */
    public function fetchParentCommentary($entityId, $entityTypeId, $commentId)
    {
        $response = new OSDN_Response();
        $validate = new OSDN_Validate_Id();
        if (empty($entityTypeId) ||
            !$validate->isValid($entityId) ||
            !$validate->isValid($commentId))
        {
            throw new InvalidArgumentException('Invalid credentials');
        }

        $row = $this->_table->fetchRow(array(
            'entityTypeId = ?' => $entityTypeId,
            'entityId = ?'     => $entityId,
            'id = ?'           => $commentId
        ));

        return !is_null($row) ? $row->parentId : 0;
    }

    /**
     * Fetch comment row
     *
     * @param int $commentId     The comment id
     * @return Comment_Model_DbTable_CommentRow
     */
    public function find($commentId)
    {
        return $this->_table->fetchRow(array('id = ?' => $commentId));
    }
}
