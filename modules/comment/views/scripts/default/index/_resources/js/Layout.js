Ext.ns('Module.Comment');

Module.Comment.Layout = Ext.extend(Ext.Panel, {

    layout: 'border',

    stateful: false,

    loadUrl: null,

    information: null,

    commentId: null,

    tree: null,

    entityId: null,

    entityType: null,

    activeNode: null,

    informationTitle: null,

    initComponent: function() {

        this.tree = new Module.Comment.Tree({
            entityId: this.entityId,
            entityType: this.entityType,
            region: 'north',
            height: 200,
            split: true
        });

        this.replyButton = new Ext.Button({
            text: lang('Reply'),
            iconCls: 'icon-submit-16',
            disabled: true,
            handler: function() {
                this.tree.onAddReply({
                    commentId: this.commentId,
                    node: this.activeNode
                });
            },
            scope: this
        });

        this.informationTitle = new Ext.Toolbar.TextItem({
            text: '<b>Details</b>'
        });

        this.information = new Ext.Panel({
            region: 'center',
            deferredRender: false,
            autoScroll: true,
            split: true,
            headerStyle: 'border-width: 1px 0',
            bodyStyle: 'border-width: 1px 0 0',
            tbar: new Ext.Toolbar({
                cls: 'x-panel-header',
                items: [
                    this.informationTitle,
                    '->',
                    this.replyButton
                ]
           })
        });

        this.items = [this.tree, this.information];

        Module.Comment.Layout.superclass.initComponent.apply(this, arguments);

        this.tree.on('itemclick', this.onItemClick, this);
        this.tree.on('beforeload', function() {
            this.information.setTitle(this.information.initialConfig.title);
            if (this.information.rendered) {
                this.replyButton.disable();
            }
        }, this);

        this.tree.on('load', function() {
            this.tree.getEl().unmask();
        }, this);
    },

    onItemClick: function(node) {
        var record = node.attributes;
        this.activeNode = node;
        this.commentId = record.commentId;
        this.informationTitle.setText('<b>Details: ' + record.title + '</b>');
        this.information.load({
            url: link(this.entityType, 'comment', 'fetch'),
            method: 'post',
            params: {
                commentId: record.commentId,
                entityId: this.entityId
            },
            callback: function() {
                this.replyButton.enable();
            },
            scope: this
        });
    },

    setEntityId: function(id, reload) {
        this.entityId = id;
        this.tree.entityId = id;
        this.tree.loader.baseParams.entityId = id;
        Ext.applyIf(this.tree.loader.baseParams, this.baseParams || {});
        if (true == reload) {
            this.tree.getRootNode().reload();
            if (this.information.rendered) {
                this.information.update('');
                this.informationTitle.setText('<b>Details</b>');
            }
        }
        return this;
    },

    setBaseParams: function(params) {
        if (!Ext.isObject(this.tree.baseParams)) {
            this.tree.baseParams = {};
        }

        Ext.apply(this.tree.baseParams, params);
        return this;
    }
});