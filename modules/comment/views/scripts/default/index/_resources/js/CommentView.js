Ext.define('Module.Comment.CommentView', {

    extend: 'Ext.Panel',

    window: null,

//    border: false,

    commentId: null,

    bodyPadding: 5,

    entityId: null,

    entityType: null,

    actionUrl: null,

    layout: 'fit',

    node: null,

    baseParams: null,

    controller: null,

    initComponent: function() {

        Ext.Ajax.request({
            url: link(this.entityType, this.controller || 'comment', 'fetch', {commentId: this.commentId, entityId: this.entityId}),
            success: function (res) {
                this.update(res.responseText);
            },
            failure: function () {},
            scope: this
        });

        this.addEvents(
            /**
             * Fires when reply comment is created
             *
             * @param {Module.Comment.CommentView}
             */
            'created-reply'
        );

        this.callParent(arguments);

    },

    showInWindow: function() {
        this.window = new Ext.Window({
            layout: 'fit',
            title: this.title,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Reply'),
                iconCls: 'reply',
                handler: function() {
                    this.onAddReply({
                        commentId: this.commentId
                    });
                },
                scope: this
            }, {
                text: lang('Close'),
                handler: function() {
                    this.window.close();
                },
                scope: this
            }]
        });
        this.setTitle('');
        this.window.show.apply(this.window, arguments);
        return this.window;
    },

    onAddReply: function(cfg) {
        Application.require([
            'comment/form/.'
        ], function() {
            var mtf = new Module.Comment.Form({
                commentId: this.commentId,
                entityId: this.entityId,
                entityType: this.entityType,
                controller: this.controller,
                actionUrl: link(this.entityType, this.controller || 'comment', 'add-reply', {format: 'json'}),
                title: lang('Add new reply'),
                history: true
            });
            mtf.on('completed', function(form, entityId, commentId) {
                this.fireEvent('created-reply', commentId);
                this.window.close();
            }, this);
            mtf.showInWindow();
        }, this);
    }

});