Ext.define('Module.Comment.Tree', {

    extend: 'Ext.tree.Panel',

    alias: 'module.comment.tree',

    useArrows: true,

    animCollapse: false,
    animate: false,

    cls: 'osdn-comment-tree',

    rootVisible: false,

    entityType: null,
    entityId: null,
    controller: null,

    stateful: true,

    stateId: 'module.comment.tree',

    baseParams: null,

    resource: null,

    initComponent: function() {

        Ext.define('Module.Comment.Model.Tree', {
            extend: 'Ext.data.Model',
            fields: [
                'title', 'creatorAccountName', 'bodyText', 'repliesCount',
                'notificatedAccountId', 'reactionReceived', 'createdDatetime',
                'id', 'commentId', 'parentId', 'notificatedAction'
            ]
        });

        this.store = Ext.create('Ext.data.TreeStore', {
            clearOnLoad: false,
            model: 'Module.Comment.Model.Tree',
            proxy: {
                type: 'ajax',
                url: link(this.entityType, this.controller || 'comment', 'index', {format: 'json'}),
                extraParams: {
                    entityId: this.entityId
                },
                simpleSortMode: true
            }
        });

        this.tbar = [{
            iconCls: 'icon-add-16',
            text: lang('Add comment'),
            handler: this.onAddComment,
            scope: this
        }, '->', {
            iconCls: 'icon-refresh-16',
            text: lang('Refresh'),
            handler: function() {
                this.getRootNode().removeAll();
                this.getStore().load();
            },
            scope: this
        }];

        this.columns = [{
            xtype: 'treecolumn',
            header: lang('Title'),
            dataIndex: 'title',
            flex: 1,
            renderer: function(v, p, record) {
                var repliesCount = record.get('repliesCount');
                if (!Ext.isEmpty(repliesCount) && repliesCount > 0) {
                    p.tdAttr = (p.tdAttr || "") + " data-qtip='" + repliesCount + " " + lang('replies present') + "'";
                    return v + ' (' + repliesCount + ') ';
                }
                return v;
            }
        }, {
            header: lang('Owner'),
            xtype: 'templatecolumn',
            dataIndex: 'creatorAccountName',
            flex: 1,
            tpl: new Ext.XTemplate(
                '<tpl if="!Ext.isEmpty(creatorAccountName)">',
                '<span data-qtip="{creatorAccountName}">{creatorAccountName}</span>',
                '</tpl>',
                '<tpl if="Ext.isEmpty(creatorAccountName)">',
                    '<span data-qtip="' + lang('Anonymous') + '">' + lang('Anonymous') + '</span>',
                '</tpl>'
            )
        }, {
            header: lang('Response needed'),
            width: 150,
            renderer: function(v, p, record) {
                if (!Ext.isEmpty(record.get('notificatedAccountId'))
                    && Ext.isEmpty(record.get('reactionReceived'))
                    && record.get('notificatedAction') == 'R') {
                        return '<div class=" x-grid-checkheader-checked ">&nbsp;</div>';
                    } else {
                        return '';
                    }

            }
        }, {
            header: lang('Created'),
            xtype: 'templatecolumn',
            flex: 1,
            dataIndex: 'createdDatetime',
            tpl: new Ext.XTemplate('{createdDatetime:this.formatCreatedDatetime}', {
                formatCreatedDatetime: function(v) {
                    return Ext.Date.format(Ext.Date.parseDate(v, 'Y-m-d H:i:s'), 'd-m-Y H:i:s');
                }
            })
        }];
        
        this.on('itemcontextmenu', function(view, record, item, index, e, options) {
            e.stopEvent();
            var contextMenu = this.createContextMenu(record);

            // check if replies are present then disabled menu item
            var repliesPresent = (record.get('repliesCount') && record.get('repliesCount') > 0);
            contextMenu.items.each(function(i) {
                if (-1 != ['delete', 'edit'].indexOf(i.action)) {
                    i.setDisabled(repliesPresent);
                }
            }, this);

            contextMenu.showAt(e.getXY());
        }, this);

        this.on('itemclick', this.onItemClick, this);

        this.callParent(arguments);
    },

    createContextMenu: function(record) {
        var items = [{
            text: lang('Edit'),
            iconCls: 'icon-edit-16',
            action: 'edit',
            handler: function() {
                this.onUpdateComment({
                    commentId: record.get('commentId'),
                    node: record
                });
            },
            scope: this
        }, {
            text: lang('Delete'),
            iconCls: 'icon-delete-16',
            action: 'delete',
            handler: function() {
                Ext.Msg.confirm(
                    lang('Confirmation'),
                    lang('Do you really want to delete comment?'),
                    function(b) {
                        'yes' == b && this.onRemoveComment({
                            commentId: record.get('commentId'),
                            node: record
                        });
                    }, this
                );
            },
            scope: this
        }, {
            text: lang('Reply'),
            iconCls: 'reply',
            action: 'reply',
            handler: function() {
                this.onAddReply({
                    commentId: record.get('commentId'),
                    node: record
                });
            },
            scope: this
        }];

        var m = new Ext.menu.Menu({
            items: items,
            listeners: {
                click: function() {
                    m.hide();
                }
            }
        });

        return m;

    },
    
    onItemClick: function(view, record, item, index, e, options) {
        var p = new Module.Comment.CommentView({
            entityId: this.entityId,
            width: 450,
            height: 250,
            commentId: record.get('commentId'),
            entityType: this.entityType,
            controller: this.controller,
            title: lang('View comment')
        });
        p.on('created-reply', function(commentId) {
            this.reloadNode(record, commentId);
            this.fireEvent('completed', this.entityId);
        }, this);
        p.showInWindow();
    },

    onAddComment: function() {
        Application.require([
            'comment/form/.'
        ], function() {
            var mcf = new Module.Comment.Form({
                actionUrl: link(this.entityType, this.controller || 'comment', 'create', {format: 'json'}),
                entityId: this.entityId,
                entityType: this.entityType,
                controller: this.controller,
                title: lang('Create new comment'),
                plugins: [Ext.create('Ext.ux.plugins.panel.Window')]
            });
            mcf.on('completed', function(f, entityId, commentId) {
                this.reloadNode(this.getRootNode(), commentId);
                this.fireEvent('completed', this.entityId);
//                this.getRootNode().removeAll();
//                this.getStore().load();
            }, this);
            mcf.showInWindow();
        }, this);
    },

    onUpdateComment: function(cfg) {
        Application.require([
            'comment/form/.'
        ], function() {
            var mtf = new Module.Comment.Form({
                commentId: cfg.commentId,
                actionUrl: link(this.entityType, this.controller || 'comment', 'update', {format: 'json'}),
                entityId: this.entityId,
                entityType: this.entityType,
                controller: this.controller,
                title: lang('Update comment'),
                plugins: [Ext.create('Ext.ux.plugins.panel.Window')]
            });
            mtf.on('completed', function() {
                this.reloadNode(cfg.node, cfg.commentId);
            }, this);
            mtf.showInWindow({});
        }, this);
    },

    onAddReply: function(cfg) {
        Application.require([
            'comment/form/.'
        ], function() {
            var mtf = new Module.Comment.Form({
                commentId: cfg.commentId,
                entityId: this.entityId,
                entityType: this.entityType,
                controller: this.controller,
                actionUrl: link(this.entityType, this.controller || 'comment', 'add-reply', {format: 'json'}),
                title: lang('Add new reply'),
                history: true
            });
            mtf.on('completed', function(form, entityId, commentId) {
                this.reloadNode(cfg.node, commentId);
            }, this);
            mtf.showInWindow();
        }, this);
    },

    onRemoveComment: function(cfg) {
        Ext.Ajax.request({
            url: link(this.entityType, this.controller || 'comment', 'delete', {format: 'json'}),
            params: {
                id: cfg.commentId,
                entityId: this.entityId
            },
            success: function(response) {
                var res = Ext.decode(response.responseText);
                if (res.success === true) {
                    var reloadingNode = cfg.node;
                    if (this.getRootNode() != reloadingNode && reloadingNode.get('leaf') == true) {
                        reloadingNode = reloadingNode.parentNode
                    }
                    this.reloadNode(reloadingNode);
                    this.fireEvent('completed', this.entityId);
                }
                Application.notificate(res.messages);
            },
            failure: function() {
                Application.notificate(lang('Delete node failed. Try again.'));
            },
            scope: this
        });
    },

//    markReaded: function(node) {
//
//        var a = node.attributes;
//        if (a.reactionReceived || !a.notificatedAccountId) {
//            return;
//        }
//
//        Ext.Ajax.request({
//            url: link(this.entityType, this.controller || 'comment', 'mark-readed', {format: 'json'}),
//            params: {
//                commentId: a.commentId,
//                entityId: a.entityId
//            },
//            callback: function(options, success, response) {
//                var response = Ext.decode(response.responseText);
//                if (response.success === true) {
//                    node.attributes.reactionReceived = new Date().format('Y-m-d H:i:s');
//                    node.getUI().elNode.cells[2].innerHTML = "&nbsp;";
//                }
//            },
//            scope: this
//        });
//    },

    reloadNode: function(node, selectionId) {

        var parentNode = node;
        var id = node.get('id');

        if (this.getRootNode() != node) {
            parentNode = node.parentNode;
        }
        
        var treeStore = this.getStore();
        var view = this.getView();

        // Remove all current children if clear on load not set
        if (!treeStore.clearOnLoad) {
            parentNode.removeAll();
        }

        // Call load, refreshing our view when done...
        var viewRefresher = function() {
            view.refreshNode(parentNode.get('id'));

            if (parentNode.get('id') == id) {
                node = parentNode;
            } else {
                node = parentNode.findChild('id', id, true);
            }
            if (node) {
                if (selectionId) {
                    var selectionNode = parentNode.findChild('commentId', selectionId, true);
                    if (selectionNode) {
                        var sm = view.getSelectionModel();
                        sm.select(selectionNode);
                    }
                }
                parentNode.on('expand', function() {
                    if (!selectionId) {
                        return;
                    }
                    var selectionNode = parentNode.findChild('commentId', selectionId, true);
                    if (selectionNode) {
                        var sm = view.getSelectionModel();
                        sm.select(selectionNode);
                    }
                }, this, {single: true});
                node.expand();
            }
        };

        treeStore.load({
            node: parentNode,
            callback: viewRefresher
        });

    }

//    fetchReverse: function(id, callback, parentIds) {
//
//        if (!Ext.isArray(parentIds)) {
//            parentIds = [];
//        }
//
//        Ext.Ajax.request({
//            url: link(this.entityType, this.controller || 'comment', 'fetch-parent', {format: 'json'}),
//            params: Ext.applyIf({
//                entityId: this.entityId,
//                commentId: id
//            }, this.baseParams || {}),
//            callback: function(options, success, response) {
//                var res = OSDN.decode(response.responseText);
//                if (!success || !res.success) {
//                    return false;
//                }
//
//                var parentId = parseInt(res.parentId, 10);
//                if (isNaN(parentId)) {
//                    return false;
//                }
//
//                if (parentId > 0) {
//                    parentIds.unshift(parentId);
//                    this.fetchReverse(parentId, callback, parentIds);
//                    return;
//                }
//
//                if (0 == parentId && 'function' == typeof callback) {
//                    callback.apply(this, [parentIds]);
//                }
//            },
//            scope: this
//        });
//    },

//    expandReverse: function(id, callback) {
//        this.getEl().mask(lang('Fetching commentary...'), 'x-mask-loading');
//        this.fetchReverse(id, function(parentIds) {
//            parentIds.unshift(this.getRootNode().id);
//            this.expandPath('/' + parentIds.join('/'), null, function(oSuccess, oLastNode) {
//                var node = oLastNode.findChild('id', id);
//                if (node instanceof Ext.ux.tree.TreeGridNodeUI) {
//                    this.onItemClick(node);
//                    node.select();
//                } else {
//                    alert(lang('Commentary does not exists.'));
//                }
//
//            }.createDelegate(this));
//
//            if ('function' == typeof callback) {
//                callback(parentIds);
//            }
//            this.getEl().unmask();
//        }.createDelegate(this));
//    }

});
