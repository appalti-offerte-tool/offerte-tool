Ext.define('Module.Comment.Form', {

    extend: 'Ext.panel.Panel',

    border: false,

    commentId: null,

    entityId: null,

    actionUrl: null,

    history: false,

    autoWidth: true,

    autoHeight: true,

    commentTypeId: null,

    entityType: null,

    notificatedAccountId: null,

    notificatedAction: null,

    layout: 'border',

    baseParams: null,

    controller: null,

    hasHistory: false,

    form: null,

    historyPanel: null,

    initComponent: function() {

        this.hasHistory = this.commentId && this.history;
        
        this.form = Ext.create('Ext.form.Panel', {
            region: 'center',
            bodyPadding: 5,
            width: 400,
            height: 300,
            split: true,

            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },

            defaultType: 'textfield',
            items: [{
                fieldLabel: 'Title',
                name: 'title',
                allowBlank: false
            },{
                xtype     : 'htmleditor',
                name      : 'bodyText',
                fieldLabel: 'Text',
                anchor    : '100%'
            }, {
                xtype: 'module.comment.comment-type.combo-box',
                allowBlank: false,
                name: 'commentTypeId',
                fieldLabel: lang('Comment type')
            }, {
                xtype: 'module.account.helper.account',
                name: 'notificatedAccountId',
                fieldLabel: lang('Recipient')
            }, {
                xtype: 'module.comment.comment-action.combo-box',
                name: 'notificatedAction',
                fieldLabel: lang('Action')
            }]

        });

        this.on('render', this.doLoadForm, this);

        this.historyPanel = Ext.create('Ext.Panel', {
            region: 'west',
            width: 300,
            height: 300,
            autoScroll: true,
            html: '',
            split: true
        });

        this.items = [this.form];

        if (this.hasHistory) {
            this.items.push(this.historyPanel);
        }

        this.plugins = [Ext.create('Ext.ux.plugins.panel.Window', {
            title: this.title,
            width: this.hasHistory ? 700 : 400,
            height: 320
        })];

        this.callParent(arguments);

        this.addEvents(
            /**
             * Fires when comment is created
             *
             * @param {Module.Comment.Form}
             */
            'completed'
        );

        this.on('wndhandlerclick', this.onSubmit, this);
    },

    doLoadForm: function() {

        this.onWindowContentLoaded();

        if (!this.commentId) {
            return;
        }

        var params = {
            entityId: this.entityId,
            format: 'json'
        };

        this.form.getForm().load({
            url: link(this.entityType, this.controller || 'comment', 'form', (!this.history && this.commentId) ? {commentId: this.commentId} : {}),
            method: 'POST',
            params: params,
            success: function(options, response) {
                if ('EMAIL' === this.commentTypeId.getValue()) {
                    this.commentTypeId.setDisabled(true);
                    this.notificatedAccountId.setDisabled(true);
                    this.notificatedAction.setDisabled(true);
                } else {
                    this.notificatedAccountId.setDisabled(true);
                    this.notificatedAction.setDisabled(true);
                }
            },
            waitMsg: lang('Loading...'),
            scope: this
        });

        Ext.Ajax.request({
            url: link(this.entityType, this.controller || 'comment', 'history', {parentId: this.commentId, entityId: this.entityId}),
            success: function (res) {
                this.historyPanel.update(res.responseText);
            },
            failure: function () {},
            scope: this
        });

    },

    onWindowContentLoaded: function() {
        this.commentTypeId = this.form.form.findField('commentTypeId');
        this.notificatedAccountId = this.form.form.findField('notificatedAccountId');
        this.notificatedAction = this.form.form.findField('notificatedAction');

        if (Ext.ux.OSDN.empty(this.commentTypeId.getValue())) {
            this.commentTypeId.setDefaultValue();
        }

        if ('EMAIL' === this.commentTypeId.getValue()) {
            this.commentTypeId.setDisabled(true);
            this.notificatedAccountId.setDisabled(true);
            this.notificatedAction.setDisabled(true);
        } else {
            this.notificatedAccountId.setDisabled(true);
            this.notificatedAction.setDisabled(true);
        }

        this.commentTypeId.on('select', function() {
            if ('EMAIL' === this.commentTypeId.getValue()) {
                this.notificatedAccountId.setDisabled(false);
                this.notificatedAction.setDisabled(false);
            } else {
                this.notificatedAccountId.setDisabled(true);
                this.notificatedAction.setDisabled(true);
            }
        },this);
    },

    onSubmit: function() {

        if (!this.form.form.isValid()) {
            return;
        }

        var params = {};
        if (this.commentId) {
            if (this.history) {
                params['parentId'] = this.commentId;
            } else {
                params['id'] = this.commentId;
            }
        }
        params['entityId'] = this.entityId;

        if (this.commentTypeId.disabled) {
            params['commentTypeId'] = this.commentTypeId.getValue();
        }
        if (this.notificatedAction.disabled) {
            params['notificatedAction'] = this.notificatedAction.getValue();
        }
        if (this.notificatedAccountId.disabled) {
            params['notificatedAccountId'] = this.notificatedAccountId.getValue();
        }

        this.form.form.submit({
            url: this.actionUrl,
            method: 'post',
            params: Ext.applyIf(params, this.baseParams || {}),
            success: function(form, action) {
                var responseObj = Ext.decode(action.response.responseText);
                Application.notificate(responseObj.messages);
                if (action.result.success) {
                    this.fireEvent('completed', this, responseObj.entityId, responseObj.commentId);
                    this.window.close();
                }
            },
            scope: this
        });
    }
});