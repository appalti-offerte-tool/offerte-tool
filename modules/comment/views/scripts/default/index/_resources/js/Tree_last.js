Ext.ns('Module.Comment');

Module.Comment.TreeNodeUI = Ext.extend(Ext.ux.tree.TreeGridNodeUI, {
    onCheckboxClick: function() {},
    onCheckChange: function() {}
});

Module.Comment.Tree = Ext.extend(Ext.ux.tree.TreeGrid, {

    border: false,
    animCollapse: false,
    animate: false,

    tabCls: 'yellow-notes',
    allowRename: false,
    allowRemove: false,
    allowMenu: false,
    enableDD: false,
    allowMove: false,
    rootVisible: false,

    entityType: null,
    entityId: null,
    controller: null,

    stateful: true,

    stateId: 'module.comment.tree',

    baseParams: null,

    resource: null,

    initComponent: function() {

        this.tbar = [{
            iconCls: 'icon-add-16',
            text: lang('Add comment'),
            handler: this.onAddComment,
            scope: this
        }, '->', {
            iconCls: 'icon-refresh-16',
            text: lang('Refresh'),
            handler: function() {
                this.getRootNode().reload();
            },
            scope: this
        }];

        this.columns = [{
            header: lang('Title'),
            dataIndex: 'title',
            width: 200,
            tpl: new Ext.XTemplate(
                '{title}',
                '<tpl if="!Ext.isEmpty(repliesCount) && repliesCount &gt; 0">',
                ' <span qtip="{repliesCount} ' + lang('replies present') + '">({values.repliesCount})</span>',
                '</tpl>'
            )
        }, {
            header: lang('Owner'),
            dataIndex: 'creatorAccountName',
            width: 100,
            tpl: new Ext.XTemplate(
                '<tpl if="!Ext.isEmpty(creatorAccountName)">',
                '<span qtip="{creatorAccountName}">{creatorAccountName}</span>',
                '</tpl>',
                '<tpl if="Ext.isEmpty(creatorAccountName)">',
                    '<span qtip="' + lang('Anonymous') + '">' + lang('Anonymous') + '</span>',
                '</tpl>'
            )
        }, {
            header: lang('Response needed'),
            width: 150,
            tpl: new Ext.XTemplate(
                '<tpl if="!Ext.isEmpty(notificatedAccountId) && Ext.isEmpty(reactionReceived) && (notificatedAction == &quot;R&quot;)">',
                '<div class="x-grid3-check-col-on">&nbsp;</div>',
                '</tpl>'
            )
        }, {
            header: lang('Created'),
            width: 130,
            dataIndex: 'createdDatetime',
            tpl: new Ext.XTemplate('{createdDatetime:this.formatCreatedDatetime}', {
                formatCreatedDatetime: function(v) {
                    return Date.parseDate(v, 'Y-m-d H:i:s').format('d-m-Y H:i:s');
                }
            })
        }];

        this.loader = new Ext.ux.tree.TreeGridLoader({
            dataUrl: link(this.entityType, this.controller || 'comment', 'index', {format: 'json'}),
            baseParams: {
                entityId: this.entityId
            },
            uiProviders:{
                'col': Ext.ux.tree.TreeGridNodeUI
            },
            baseAttrs: {
                uiProvider: 'col',
                iconCls: 'comm'
            },
            scope: this
        });

        this.root = new Ext.tree.AsyncTreeNode({
            id: '0',
            iconCls: 'comm'
        });

        this.contextMenu = new Ext.menu.Menu({
            items: [{
                text: lang('Edit'),
                iconCls: 'icon-edit-16',
                action: 'edit'
            }, {
                text: lang('Delete'),
                iconCls: 'icon-delete-16',
                action: 'delete'
            }, {
                text: lang('Reply'),
                iconCls: '',
                action: 'reply'
            }]
        });
        this.contextMenu.on('itemclick', this.onContextMenuItemClick, this);

        this.on('contextmenu', function(node, e) {
            node.select();
            var c = node.getOwnerTree().contextMenu;
            c.contextNode = node;

            // check if replies are present then disabled menu item
            var repliesPresent = (node.attributes.repliesCount && node.attributes.repliesCount > 0);
            c.items.each(function(i) {
                if (-1 != ['delete', 'edit'].indexOf(i.action)) {
                    i.setDisabled(repliesPresent);
                }
            }, this);

            c.showAt(e.getXY());
        }, this);

        this.on('click', this.onItemClick, this);
        Module.Comment.Tree.superclass.initComponent.call(this);
    },

    onItemClick: function(node, e) {
        this.fireEvent('itemclick', node);
    },

    onContextMenuItemClick: function(item) {

        var node = item.parentMenu.contextNode;
        var id = node.id;

        switch (item.action) {
            case 'edit':
                this.onUpdateComment({
                    commentId: id,
                    node: node
                });
                break;

            case 'reply':
                this.onAddReply({
                    commentId: id,
                    node: node
                });
                break;

            case 'delete':
                Ext.Msg.confirm(
                    lang('Confirmation'),
                    lang('Do you really want to delete comment?'),
                    function(b) {
                        'yes' == b && this.onRemoveComment({
                            commentId: id,
                            node: node
                        });
                    }, this
                );
                break;
        }
    },

    onAddComment: function() {
        Application.require([
            'comment/form/.'
        ], function() {
            var mcf = new Module.Comment.Form({
                actionUrl: link(this.entityType, this.controller || 'comment', 'create', {format: 'json'}),
                entityId: this.entityId,
                entityType: this.entityType,
                controller: this.controller,
                title: lang('Create new comment')
            });
            mcf.on('completed', function(f, entityId) {
                this.fireEvent('completed', this.entityId);
                this.getRootNode().reload();
            }, this);
            mcf.showInWindow();
        }, this);
    },

    onUpdateComment: function(cfg) {
        Application.require([
            'comment/form/.'
        ], function() {
            var mtf = new Module.Comment.Form({
                commentId: cfg.commentId,
                actionUrl: link(this.entityType, this.controller || 'comment', 'update', {format: 'json'}),
                entityId: this.entityId,
                entityType: this.entityType,
                controller: this.controller,
                title: lang('Update comment')
            });
            mtf.on('completed', function(f, entityId) {
                var node = cfg.node;
                if (this.getRootNode() != node) {
                    node = node.parentNode;
                }
                node.reload();
            }, this);
            mtf.showInWindow();
        }, this);
    },

    onAddReply: function(cfg) {
        Application.require([
            'comment/form/.'
        ], function() {
            var mtf = new Module.Comment.Form({
                commentId: cfg.commentId,
                entityId: this.entityId,
                entityType: this.entityType,
                controller: this.controller,
                actionUrl: link(this.entityType, this.controller || 'comment', 'add-reply', {format: 'json'}),
                title: lang('Add new reply'),
                history: true
            });
            mtf.on('completed', function(f, entityId) {
                var node = cfg.node;
                if (this.getRootNode() != node) {
                    node = node.parentNode;
                }
                this.fireEvent('completed', this.entityId);
                node.reload();
            }, this);
            mtf.showInWindow();
        }, this);
    },

    onRemoveComment: function(cfg) {

        Ext.Ajax.request({
            url: link(this.entityType, this.controller || 'comment', 'delete', {format: 'json'}),
            params: {
                id: cfg.commentId,
                entityId: this.entityId
            },
            success: function(response) {
                var res = Ext.decode(response.responseText);
                if (res.success === true) {
                    var parent = cfg.node.parentNode;
                    if (this.getRootNode() != parent) {
                        parent = parent.parentNode;
                    }
                    this.fireEvent('completed', this.entityId);
                    parent.reload();
                }
                Application.notificate(res.messages);
            },
            failure: function() {
                Application.notificate(lang('Delete node failed. Try again.'));
            },
            scope: this
        });
    },

    markReaded: function(node) {

        var a = node.attributes;
        if (a.reactionReceived || !a.notificatedAccountId) {
            return;
        }

        Ext.Ajax.request({
            url: link(this.entityType, this.controller || 'comment', 'mark-readed', {format: 'json'}),
            params: {
                commentId: a.commentId,
                entityId: a.entityId
            },
            callback: function(options, success, response) {
                var response = Ext.decode(response.responseText);
                if (response.success === true) {
                    node.attributes.reactionReceived = new Date().format('Y-m-d H:i:s');
                    node.getUI().elNode.cells[2].innerHTML = "&nbsp;";
//                    node.getUI().columns[2].innerHTML = "&nbsp;";
                }
            },
            scope: this
        });
    }

//    fetchReverse: function(id, callback, parentIds) {
//
//        if (!Ext.isArray(parentIds)) {
//            parentIds = [];
//        }
//
//        Ext.Ajax.request({
//            url: link(this.entityType, this.controller || 'comment', 'fetch-parent', {format: 'json'}),
//            params: Ext.applyIf({
//                entityId: this.entityId,
//                commentId: id
//            }, this.baseParams || {}),
//            callback: function(options, success, response) {
//                var res = OSDN.decode(response.responseText);
//                if (!success || !res.success) {
//                    return false;
//                }
//
//                var parentId = parseInt(res.parentId, 10);
//                if (isNaN(parentId)) {
//                    return false;
//                }
//
//                if (parentId > 0) {
//                    parentIds.unshift(parentId);
//                    this.fetchReverse(parentId, callback, parentIds);
//                    return;
//                }
//
//                if (0 == parentId && 'function' == typeof callback) {
//                    callback.apply(this, [parentIds]);
//                }
//            },
//            scope: this
//        });
//    },

//    expandReverse: function(id, callback) {
//        this.getEl().mask(lang('Fetching commentary...'), 'x-mask-loading');
//        this.fetchReverse(id, function(parentIds) {
//            parentIds.unshift(this.getRootNode().id);
//            this.expandPath('/' + parentIds.join('/'), null, function(oSuccess, oLastNode) {
//                var node = oLastNode.findChild('id', id);
//                if (node instanceof Ext.ux.tree.TreeGridNodeUI) {
//                    this.onItemClick(node);
//                    node.select();
//                } else {
//                    alert(lang('Commentary does not exists.'));
//                }
//
//            }.createDelegate(this));
//
//            if ('function' == typeof callback) {
//                callback(parentIds);
//            }
//            this.getEl().unmask();
//        }.createDelegate(this));
//    }

});

Ext.reg('module.comment.tree', 'Module.Comment.Tree');