Ext.ns('Module.Comment');

Module.Comment.List = Ext.extend(Ext.grid.GridPanel, {

    stateId: 'module.comment.list',

    getRepliesLink: link('worklocation', 'comment', 'get-replies'),

    worklocationId: null,

    initComponent: function() {

        /**
        * The structure is:
        *
        *     "comment_id"            : int,
        *  !  "id"                    : int,
        *     "entity_id"             : int,
        *  !  "entitytype_id"         : int,
        *  !  "commenttype_id"        : int,
        *  !  "title"                 : string,
        *     "bodyText"              : string,
        *  !  "creator_account_id"    : int,
        *  !  "createddatetime"       : string (YYYY-MM-DD hh:mm:ss),
        *     "modified_account_id"   : int,
        *     "modifieddatetime"      : string (YYYY-MM-DD hh:mm:ss),
        *     "notificated_account_id": int,
        *     "notificated_action"    : ???,
        *     "reaction_received"     : int,
        *     "parent_id"             : int,
        *  !  "creator_account_name"  : string,
        *     "modified_account_name" : string,
        *  !  "replies_count"         : int,
        *     "notificated_owner"     : bool
        */

        this.store = new Ext.data.JsonStore({
            url: link('worklocation', 'comment', 'list', {format: 'json'}),
            root: 'rowset',
            totalProperty: 'total',
            remoteSort: true,
            sortInfo: {field: 'createdDatetime', direction: 'DESC'},
            fields: [
                'id', 'entityId', 'entityTypeId', 'commentTypeId', 'title',
                'creatorAccountId', 'creatorAccountName', 'repliesCount',
                {name: 'createdDatetime', type: 'date', dateFormat: 'Y-m-d H:i:s'}
            ]
        });

        this.filterPlugin = new Ext.ux.plugin.grid.Filtering([{
            type: 'list',
            dataIndex: 'commentTypeId',
            phpMode: true,
            options: [
                    {id: '1', text: lang('Private')},
                    {id: '2', text: lang('Public')},
                    {id: '3', text: lang('Email')}
            ]
        }, {
            type: 'date',  dataIndex: 'createdDatetime', dateFormat: 'Y-m-d'
        }]);

        this.plugins = [this.filterPlugin];

        this.bbar = new Ext.ux.PagingToolbar({
            store: this.store
        });

        this.columns = [{
            header: lang('Title'),
            dataIndex: 'title',
            id: this.autoExpandColumn = 'module.comment.list-title',
            sortable: true,
            renderer: function(v, m, r) {
                return parseInt(r.get('repliesCount')) > 0
                ? v + ' (' + r.get('repliesCount') + ')'
                : v;
            }
        }, {
            header: lang('Entity type'),
            dataIndex: 'entityTypeId',
            hidden: true,
            width: 60,
            sortable: true,
            renderer: function(v) {
                var text = lang('Not specified');
                switch(parseInt(v)) {
                    case 1:
                        text = lang('Company');
                        break;
                }
                return text;
            }
        }, {
            header: lang('Type'),
            dataIndex: 'commentTypeId',
            sortable: true,
            width: 40,
            renderer: function(v, m, r) {
                var text = lang('Not specified');
                switch(parseInt(v)) {
                    case 1:
                        text = lang('Private');
                        break;
                    case 2:
                        text = lang('Public');
                        break;
                    case 3:
                        text = lang('Email');
                        break;
                }
                return text;
            }
        }, {
            header: lang('Creator'),
            width: 80,
            dataIndex: 'creatorAccountName'
        }, {
            header: lang('Created'),
            width: 130,
            sortable : true,
            dataIndex: 'createdDatetime',
            xtype: 'datecolumn',
            format: 'd-m-Y H:i:s'
        }];

        Module.Comment.List.superclass.initComponent.apply(this, arguments);

        this.on('rowdblclick', this.openComment, this);

//        if (this.autoLoadData) {
//            this.on('render', function() {
//                this.ownerCt.on('show', function() {
//                    this.getStore().load();
//                }, this);
//                if (this.ownerCt.isVisible()) {
//                    this.store.load();
//                }
//            }, this);
//        }
    },

    getReplies: function(grid, rowIndex, e) {
        var record = grid.getStore().getAt(rowIndex);
        Ext.Ajax.request({
            url: this.getRepliesLink,
            params: {
                id: record.get('id'),
                entityId: record.get('entityId'),
                entityTypeId: record.get('entityTypeId')
            },
            success: function(response, options) {
            },
            //failure: this.onFailure.createDelegate(this),
            scope: this
        });
    },

    openComment: function(grid, rowIndex, e) {
        var record = grid.getStore().getAt(rowIndex);
        if (record && record.get('entityId') && record.get('entityTypeId')) {
            switch (parseInt(record.get('entityTypeId'))) {
                case 1:
                    var studentsList = OSDN.System.Layout.getTabPanel().add({
                        iconCls: 'osdn-students',
                        xtype: 'ca.students.layout',
                        id: 'ca.students.layout',
                        title: lang('Students')
                    }).studentsList;
                    var wind = new CA.Students.Edit({
                        studentId: record.get('entityId'),
                        listeners: {
                            close: function() {
                                studentsList.getStore().load();
                            }
                        }
                    });
                    break;
                case OSDN.EntityTypes.TEACHER :
                    var teachersList = OSDN.System.Layout.getTabPanel().add({
                        iconCls: 'osdn-teachers',
                        xtype: 'ca.teachers.layout',
                        id: 'ca.teachers.layout',
                        title: lang('Teachers')
                    }).teachersList;
                    var wind = new CA.Teachers.Edit({
                        teacherId: record.get('entityId'),
                        listeners: {
                            close: function() {
                                teachersList.getStore().load();
                            }
                        }
                    });
                    break;
                default:
                    break;
            }
            if (wind) {
                wind.show();
                var tabs = wind.tabPanel.findByType('osdn.comments.tree');
                if (typeof tabs[0] != undefined) {
                    var tree = tabs[0];
                    wind.tabPanel.setActiveTab(tree);
                    tree.expandReverse(record.get('id'));
                }
            }
        }
    },

    setWorklocationId: function(id, reload) {
        this.worklocationId = id;
        this.getStore().setBaseParam('worlocationId', id);

        if (true === reload) {
            this.getStore().load();
        }
    }
});

Ext.reg('module.comment.list', 'Module.Comment.List');