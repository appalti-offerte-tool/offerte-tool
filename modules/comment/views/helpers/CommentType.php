<?php

class Comment_View_Helper_CommentType extends Zend_View_Helper_Abstract
{
    /**
    * Contain value/pairs comment types
    *
    * @var $_options array
    */
    protected $_options = array();

    protected $_isTranslated = false;

    /**
     * The constructor
     *
     * Initialize options for combo field
     */
    public function __construct()
    {
        $this->_options[Comment_Model_DbTable_Comment::STATUS_PUBLIC] = 'Public note';
        $this->_options[Comment_Model_DbTable_Comment::STATUS_PRIVATE] = 'Private note';
        $this->_options[Comment_Model_DbTable_Comment::STATUS_EMAIL] = 'Email note';
    }

    public function commentType()
    {
        if (false === $this->_isTranslated) {
            foreach($this->_options as & $o) {
                $o = $this->view->translate($o);
            }

            $this->_isTranslated = true;
        }

        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs,  $this->_options);
    }

    public function toCaption($value = null)
    {
        if (empty($value)) {
            return '';
        }
        return $this->_options[$value];
    }

}