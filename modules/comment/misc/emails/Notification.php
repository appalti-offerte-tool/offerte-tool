<?php

final class Comment_Misc_Email_Notification extends Configuration_Service_ConfigurationAbstract
    implements Notification_Instance_NotifiableInterface
{
    protected $_identity = 'comment.notification';

    protected $_fields = array('subject', 'body');

    protected $_mui = true;

    protected $_keywords = array(
        'title'    => 'E-mail subject',
        'comment'  => 'Commentrary text'
    );

    /**
     * (non-PHPdoc)
     * @see Notification_Instance_NotifiableInterface::notificate()
     */
    public function notificate(array $params)
    {
        if (empty($params['commentId'])) {
            throw new OSDN_Exception('CommentId is invalid');
        }

        $commentId = (int) $params['commentId'];
        $service = new Comment_Service_Comment();
        $commentRow = $service->find($commentId);
        if (empty($commentRow)) {
            throw new OSDN_Exception('Unable to find comment: #' . $commentId);
        }

        if ( ! $commentRow->isStatusEmail()) {
            return true;
        }

        $instance = $this->toInstance();
        $credentials = array(
            'title'    => $commentRow->title,
            'comment'  => $commentRow->bodyText
        );

        $mail = new Zend_Mail('UTF-8');

        /**
         * @todo Add real email address
         */
//        $mail->addTo($email->getEmailAddress(), $worklocation->getName());
        $mail->addTo('yaroslav@osdn.cv.ua', 'Yaroslav Zenin');
        $mail->addHeader('X-IDENTITY-TEMPLATE', $this->_identity);
        $mail->setSubject($instance->getSubject($credentials));

        $body = $instance->getBody($credentials);
        if ($commentRow->isResponseRequired()) {
            $body .= '<br/ ><br />--<br /><i>' . 'Please response on this note.' . '</i>';
        }
        $mail->setBodyHtml($body);

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $bootstrap->bootstrap('mail');
        $transport = $bootstrap->getResource('mail');

        $mail->send($transport);

        return true;
    }
}