<?php

class Notification_Model_DbTable_Notification extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'notification';

    protected $_rowClass = '\\Notification_Model_Notification';

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        $data['createdAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        if (empty($data['createdDatetime'])) {
            $dt = new \DateTime();
            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        return parent::insert($data);
    }
}