<?php

class Notification_Model_DbTable_Log extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'notificationLog';

    protected $_rowClass = '\\Notification_Model_Log';

    protected $_nullableFields = array(
        'notificationId',
        'notificationLuid'
    );

    protected $_omitColumns = array(
        'notification'    => self::OMIT_FETCH_ALL
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        if (empty($data['notificatedDatetime'])) {
            $dt = new \DateTime();
            $data['notificatedDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        $data['accountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        return parent::insert($data);
    }
}