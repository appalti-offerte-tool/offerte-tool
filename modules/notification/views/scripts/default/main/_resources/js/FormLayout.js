Ext.define('Module.Notification.FormLayout', {

    extend: 'Ext.panel.Panel',
    alias: 'widget.module.notification.form',

    layout: 'border',

    form: null,
    notificationId: null,

    wnd: null,

    initComponent: function() {

        this.form = new Module.Notification.Form({
        	region: 'south',
        	split: true,
        	notificationId: this.notificationId
        });

        this.keywords = new Module.Configuration.Keywords({
        	region: 'center',
        	split: true
        });

        this.items = [this.form, this.keywords];

        this.callParent(arguments);

        this.addEvents(
            /**
             * Fires when notification is created
             *
             * @param {Module.Notification.Form}
             */
            'completed'
        );

        this.form.on('completed', function(f, notificationId) {
        	this.fireEvent('completed', this, notificationId);
        }, this);
    },

    onSubmit: function(panel, w) {
        this.form.onSubmit(this.form, this.wnd);
    },

    doLoad: function() {
        this.form.doLoad();
    },

    showInWindow: function() {

        this.border = false;
        var w = this.wnd = new Ext.Window({
            title: this.notificationId ? lang('Update notification') : lang('Create notification'),
            resizable: false,
            layout: 'fit',
            iconCls: 'm-notification-icon-16',
            width: 775,
            height: 500,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Save'),
                handler: this.onSubmit,
                scope: this
            }, {
                text: lang('Close'),
                handler: function() {
                    w.close();
                    this.wnd = null;
                },
                scope: this
            }]
        });

        w.show();
        return w;
    }
});