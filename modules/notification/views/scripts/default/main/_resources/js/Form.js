Ext.define('Module.Notification.Model.Notification', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'caption', 'message',
        {name: 'isActive', type: 'boolean'}
    ]
});

Ext.define('Module.Notification.Form', {
    extend: 'Ext.form.Panel',
    bodyPadding: 5,

    notificationId: null,

    trackResetOnLoad: true,
    waitMsgTarget: true,

    keywords: null,

    initComponent: function() {

        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.Notification.Model.Notification',
            type: 'json',
            root: 'row'
        });

        this.items = [{
            xtype: 'textfield',
            fieldLabel: lang('Caption') + '<span class="asterisk">*</span>',
            name: 'caption',
            allowBlank: false,
            labelAlign: 'top',
            anchor: '100%'
        }, {
        	xtype: 'textarea',
        	fieldLabel: lang('Text') + '<span class="asterisk">*</span>',
			name: 'message',
			anchor: '100%',
			allowBlank: false,
			labelAlign: 'top',
			height: 200
        }, {
            xtype: 'checkbox',
            name: 'isActive',
            fieldLabel: lang('Active'),
            inputValue: '1',
            uncheckedValue: '0'
        }];

        this.callParent();

        this.addEvents(
            /**
             * Fires when account is created
             *
             * @param {Module.Notification.Form}
             */
            'completed'
        );
    },

    doLoad: function() {

        if (!this.notificationId) {
            return;
        }

        this.form.load({
            url: link('notification', 'main', 'fetch', {format: 'json', notificationId: this.notificationId}),
            waitMsg: Ext.LoadMask.prototype.msg,
            failure: function() {
            	Application.notificate('Unable to load');
            },
            scope: this
        });
    },

    onSubmit: function(panel, w) {

        if (true !== this.form.isValid()) {
            return;
        }

        var params = {};
        if (this.notificationId) {
            params['notificationId'] = this.notificationId;
        }

        var action = this.notificationId ? 'update' : 'create';
        this.form.submit({
            url: link('notification', 'main', action, {format: 'json'}),
            method: 'post',
            params: params,
            waitMsg: Ext.LoadMask.prototype.msg,
            success: function(form, action) {
                var responseObj = Ext.decode(action.response.responseText);
                Application.notificate(responseObj.messages);
                if (action.result.success) {
                    this.fireEvent('completed', responseObj.notificationId, action.response);
                    w.close();
                }
            },
            scope: this
        });
    },

    showInWindow: function() {

        var w = new Ext.Window({
            title:  this.notificationId ? lang('Create notification') : lang('Update notification'),
            iconCls: 'm-notification-icon-16',
            resizable: false,
            width: 415,
            modal: true,
            border: false,
            items: [this],
            buttons: [{
                iconCls: 'icon-submit-16',
                text: lang('Submit'),
                handler: function() {
                    this.onSubmit(this, w);
                },
                scope: this
            }, {
                text: lang('Cancel'),
                handler: function() {
                    w.close();
                }
            }],
            scope: this
        });

        w.show();
        this.doLoad();

        return w;
    }
});