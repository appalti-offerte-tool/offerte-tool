Ext.define('Module.Notification.List', {
    extend: 'Ext.ux.grid.GridPanel',
    alias: 'widget.module.notification.list',

    features: [{
        ftype: 'filters'
    }],

    iconCls: 'm-notification-icon-16',

    modeReadOnly: false,

    initComponent: function() {

        Ext.define('Module.Notification.Model.Notification', {
            extend: 'Ext.data.Model',
            fields: [
            	'id', 'caption', 'message', 'isActive',
            	{name: 'createdDatetime', type: 'date', dateFormat: 'Y-m-d H:i:s'}
        	]
        });

        this.store = new Ext.data.Store({
            model: 'Module.Notification.Model.Notification',
            proxy: {
                type: 'ajax',
                url: link('notification', 'main', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.columns = [{
            header: lang('Caption'),
            dataIndex: 'caption',
            flex: 1
        }, {
        	xtype: 'datecolumn',
        	header: lang('Date'),
        	dataIndex: 'createdDatetime',
        	format: 'd-m-Y H:i:s',
        	width: 120
        }, {
            header: lang('Active'),
            dataIndex: 'isActive'
        }];

        this.columns.push({
            xtype: 'actioncolumn',
            header: lang('Actions'),
            width: 50,
            fixed: true,
            items: [{
                tooltip: lang('Edit'),
                iconCls: 'icon-edit-16 icon-16',
                handler: function(g, rowIndex) {
                    this.onEditNotification(g, g.getStore().getAt(rowIndex));
                },
                scope: this
            }, {
                text: lang('Delete'),
                iconCls: 'icon-delete-16 icon-16',
                handler: this.onDeleteNotification,
                scope: this
            }]
        });

        this.tbar = [{
            text: lang('Create'),
            iconCls: 'icon-create-16',
            qtip: lang('Create new Notification'),
            handler: this.onCreateNotification,
            scope: this
        }, '-'];

        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 2
        })];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent();

        this.getView().on('itemdblclick', function(w, record) {
            this.onEditNotification(this, record);
        }, this);
    },

    onCreateNotification: function() {
        Application.require([
            'notification/./form-layout'
        ], function() {

            var f = new Module.Notification.FormLayout();
            f.on('completed', function(notificationId) {
                this.setLastInsertedId(notificationId);
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditNotification: function(g, record) {
        Application.require([
            'notification/./form-layout'
        ], function() {

            var f = new Module.Notification.FormLayout({
            	notificationId: record.get('id')
            });
            f.on('completed', function(notificationId) {
                this.setLastInsertedId(notificationId);
                this.getStore().load();
            }, this);
            f.showInWindow();
            f.doLoad();
        }, this);
    },

    onDeleteNotification: function(g, rowIndex) {

        var record = g.getStore().getAt(rowIndex);

        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
            if (b != 'yes') {
                return;
            }

            Ext.Ajax.request({
                url: link('notification', 'main', 'delete', {format: 'json'}),
                method: 'POST',
                params: {
                    id: record.get('id')
                },
                success: function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);

                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope: this
            });
        }, this);
    },

    setCategoryId: function(categoryId, forceReload) {

        this.categoryId = categoryId;
        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.categoryId = categoryId;
        if (true === forceReload) {
            this.getStore().load();
        }
        return this;
    }
});