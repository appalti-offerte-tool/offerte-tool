Ext.define('Module.Notification.Log.List', {
    extend: 'Ext.ux.grid.GridPanel',
    alias: 'widget.module.notification.log.list',

    categoryId: null,

    features: [{
        ftype: 'filters'
    }],

    iconCls: 'm-notification-icon-16',

    modeReadOnly: false,

    initComponent: function() {

        Ext.define('Module.Notification.Model.Log', {
            extend: 'Ext.data.Model',
            fields: [
            	'id', 'module',
            	{name: 'notificatedDatetime', type: 'date', dateFormat: 'Y-m-d H:i:s'}
            ]
        });

        this.store = new Ext.data.Store({
            model: 'Module.Notification.Model.Log',
            proxy: {
                type: 'ajax',
                url: link('notification', 'log', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.columns = [{
            header: lang('Module'),
            dataIndex: 'module',
            width: 150
        }, {
            header: lang('Caption'),
            dataIndex: 'caption',
            flex: 1
        }, {
            header: lang('Date'),
            xtype: 'datecolumn',
            dataIndex: 'notificatedDatetime',
            format: 'd-m-Y H:i:s',
            width: 115
        }];

//            this.columns.push({
//                xtype: 'actioncolumn',
//                header: lang('Actions'),
//                width: 50,
//                fixed: true,
//                items: [/*{
//                    tooltip: lang('Edit'),
//                    iconCls: 'icon-edit-16 icon-16',
//                    handler: function(g, rowIndex) {
//                        this.onEditNotification.Log(g, g.getStore().getAt(rowIndex));
//                    },
//                    scope: this
//                }, {
//                    text: lang('Delete'),
//                    iconCls: 'icon-delete-16 icon-16',
//                    handler: this.onDeleteNotification.Log,
//                    scope: this
//                }*/]
//            });

        this.tbar = [/*{
            text: lang('Create'),
            iconCls: 'icon-create-16',
            qtip: lang('Create new Notification.Log'),
            handler: this.onCreateNotification,
            scope: this
        },*/ '-'];

        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 2
        })];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent();

        this.getView().on('itemdblclick', function(w, record) {
            this.onEditNotification.Log(this, record);
        }, this);
    },

    onCreateNotification: function() {
        Application.require([
            'notification.log/notification.log-form/.'
        ], function() {
            var f = new Module.Notification.Log.Form({
                categoryId: this.categoryId
            });

            f.on('completed', function(notificationLogId) {
                this.setLastInsertedId(notificationLogId);
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditNotification: function(g, record) {
        Application.require([
            'notification.log/notification.log-form/.'
        ], function() {
            var f = new Module.Notification.Log.Form({
                notificationLogId: record.get('id')
            });
            f.on('completed', function(notificationLogId) {
                this.setLastInsertedId(notificationLogId);
                g.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onAfterEdit: function(data, g) {

        var params = {notificationLogId: data.record.get('id')};
        var action = null;

        switch(data.field) {
            case 'active':
                action = link('notification.log', 'main', data.record.get('active') ? 'activate' : 'deactivate', {format: 'json'});
                break;

            default:
                action = link('notification.log', 'main', 'update-field', {format: 'json'});
                params.field = data.field;
                params.value = data.value;
                break;
        }

        Ext.Ajax.request({
            url: action,
            params: params,
            callback: function(options, success, response) {
                var decResponse = Ext.decode(response.responseText);
                if (decResponse && decResponse.success) {
                    data.record.commit();
                    Application.notificate(decResponse.messages || lang('Updated successfully'));
                    return;
                }

                data.record.reject();
                Application.notificate(decResponse.messages || lang('Unable to updated notification.log'));
            },
            scope: this
        });
    },

    onDeleteNotification: function(g, rowIndex) {

        var record = g.getStore().getAt(rowIndex);

        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
            if (b != 'yes') {
                return;
            }

            Ext.Ajax.request({
                url: link('notification.log', 'main', 'delete', {format: 'json'}),
                method: 'POST',
                params: {
                    id: record.get('id')
                },
                success: function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);

                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope: this
            });
        }, this);
    },

    setCategoryId: function(categoryId, forceReload) {

        this.categoryId = categoryId;
        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.categoryId = categoryId;
        if (true === forceReload) {
            this.getStore().load();
        }
        return this;
    }
});