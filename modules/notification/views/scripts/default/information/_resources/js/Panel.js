Ext.define('Module.Notification.Information.Panel', {
    extend: 'Ext.panel.Panel',

    messages: [],

    log: [],

    initComponent: function() {

        this.board = Ext.get(Ext.DomHelper.append(Ext.getBody(), {cls: 'message-board-wrap'}));
        this.addLogIcon();

        this.callParent();
    },

    /**
     * @param {String} message  Text of the message or an action object from Ext.Form.submit action
     * @param {String} title
     * @param {String} type Error, warning, info. Defaults to 'info'
     */
    notificate: function(message, title, type) {
        if (false === message) {
            message = lang('Unknown error occured. Operation has been rejected');
        } else if (Ext.isEmpty(message)) {
            message = lang('Operation has been completed');
        }

        var messageBox = this.addmessageBox();

        if (Ext.isObject(message) && message instanceof Ext.form.action.Action) {
            type = message.result && message.result.success ? this.message_TYPE_INFO : this.message_TYPE_ERROR;

            if (undefined !== message.failureType) {
                switch (message.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        title = lang('Client validation error(s)');
                        message = lang('Form fields may not be submitted with invalid or empty value(s).');
                        break;

                    case Ext.form.Action.CONNECT_FAILURE:
                        title = lang('Ajax-request error');
                        message = lang('Some error occurred while ajax request.');
                        break;

                    case Ext.form.Action.SERVER_INVALID:
                        title = lang('Server error');
                        message = this.getmessageFromResponse(message);
                        break;
                }
            } else {
                title = lang('Information');
                message = this.getmessageFromResponse(message);
            }

        } else if(Ext.isArray(message)) {
            message = message.pop().message;
        } else {
            type = !type ? this.message_TYPE_INFO : type;
        }

        title = title ? title : lang('Message');
        messageBox.message.update(message);

        messageBox.title.update(title);

        messageBox.messageType.set({'class': 'message-type ' + type});

        this.logmessage(message, title, type);

//        if (undefined === this.logWindow) {
//            this.logIcon.hide();
            messageBox.board.fadeIn({duration: 2000}).pause(5).ghost('t', {
                remove: true,
                callback: function() {
                    this.messages.splice(0, 1);

                    if (!this.messages.length) {
                        this.logIcon.fadeIn();
                    }
                },
                scope: this
            });
//        } else {
            this.logIcon.show();
//        }
    },

    addmessageBox: function() {
        var board = Ext.get(Ext.DomHelper.append(this.board, {cls: 'message-board'})),
            titleCnt = Ext.get(Ext.DomHelper.append(board, {cls: 'message-title'}));

        var messageBoard = {
            board: board,
            title: Ext.get(Ext.DomHelper.append(titleCnt, {tag: 'span'})),
            messageType: Ext.get(Ext.DomHelper.append(titleCnt, {cls: 'message-type'})),
            message: Ext.get(Ext.DomHelper.append(board, {cls: 'message-text'}))
        }

        this.messages.push(messageBoard);

        return messageBoard;
    },

    addLogIcon: function() {
        this.logIcon = Ext.get(Ext.DomHelper.append(Ext.getBody(), {cls: 'message-board-log-icon'}))
        .insertBefore(this.board)
        .hide()
        .on('click', this.showLog, this);
    },


    showLog: function() {
        var board = this;
        board.logWindow = new Ext.Window({
            title: lang('System messages log'),
            height: 200,
            width: 400,
            autoScroll: true,
            modal: true,
            html: this.getLog(),
            buttons: [{
                    text: lang('Clear log'),
                    handler: board.clearLog,
                    scope: board
                }, {
                    text: lang('Close'),
                    handler: function() {
                        board.logWindow.close();
                    }
                }

            ]
        });

        board.logWindow.show();
    },

    getLog: function() {
        var rows = [];
        for (var i = 0; i < this.log.length; i++) {
            rows.push(
                '<tr>',
                    '<th class="v-a-top">',
                        '<span class="message-type ' + this.log[i].type + '"></span>&nbsp;',
                        this.log[i].title + ':',
                    '</th>',
                    '<td class="v-a-top">' + this.log[i].message + '</td>',
                '</tr>'
            );
        }

        return '<table class="form-grid message-log">' + rows.join(''); + '</table>';
    },

    logmessage: function(message, title, type) {
        var currentDate = new Date(),
            currentTime = [
            currentDate.getHours() < 10 ? '0' + currentDate.getHours() : currentDate.getHours().toString(10),
            currentDate.getMinutes() < 10 ? '0' + currentDate.getMinutes() : currentDate.getMinutes().toString(10),
            currentDate.getSeconds() < 10 ? '0' + currentDate.getSeconds() : currentDate.getSeconds().toString(10)
        ].join(':');

        this.log.push({
            type: type || this.message_TYPE_INFO,
            title: (title || lang('No title')) + ' (' + currentTime +  ')',
            message: message
        });

        this.setLogIconTitle();

        if (undefined !== this.logWindow) {
            this.logWindow.update(this.getLog());
        }
    },


    setLogIconTitle: function() {
        if (null !== this.logIcon) {
            this.logIcon.set({title: this.log.length + ' ' + lang('message(s)')})
        }
    },


    clearLog: function() {
        this.log.length = 0;
        this.logIcon.ghost('t');
        this.logWindow.update('');
    },

    getmessageFromResponse: function(response) {
        var result = '';
        if (response.result && response.result.errors) {
            var messages = response.result.errors;
            for (var i = 0; i < messages.length; i++) {
                result += messages[i].message + '<br />';
            }
        }

        return result;
    }
});