Ext.define('Module.Notification.Configuration.Layout', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.module.notification.configuration.layout',

    layout: 'border',

    field: null,

    pmodule: null,

    ptemplate: null,

    initComponent: function() {

        this.list = new Module.Configuration.Keywords({
            region: 'center',
            split: true
        });

        var langs = [
            ['en', 'english', lang('English')],
            ['nl', 'dutch', lang('Dutch')]
        ];

        var fields = [];
        var tabs = [];

        for(var i = 0, len = langs.length; i < len; i++) {
            var nSubject = 'subject[' + langs[i][0] + ']';
            fields.push(nSubject);
            var nBody = 'body[' + langs[i][0] + ']';
            fields.push(nBody);

            tabs.push({
                title: langs[i][2],
                iconCls: 'm-notification-configuration-flag-' + langs[i][1] + '-16x16',
                items: [{
                    xtype: 'textarea',
                    height: 35,
                    fieldLabel: lang('Subject'),
                    name: nSubject,
                    anchor: '100%'
                }, {
                    fieldLabel: lang('Body'),
                    xtype: 'htmleditor',
                    height: 230,
                    anchor: '100%',
                    name: nBody
                }]
            });
        }

        Ext.define('Module.Configuration.Model.Configuration', {
            extend: 'Ext.data.Model',
            fields: fields
        });

        this.form = new Ext.form.Panel({
            model: 'Module.Configuration.Model.Configuration',
            region: 'south',
            split: true,
            border: false,
            height: 350,
            layout: 'fit',
            bodyPadding: 0,
            trackResetOnLoad: true,
            items: [{
                xtype: 'tabpanel',
                activeTab: 0,
                defaults: {
                    padding: '5px',
                    border: false
                },
                items: tabs
            }]
        });

        this.items = [this.list, this.form];

        this.resetBtn = new Ext.Button({
            text: lang('Reset'),
            iconCls: 'icon-reset-16',
            handler: function() {
                this.form.getForm().reset();
            },
            scope: this
        });

        this.submitBtn = new Ext.Button({
            text: lang('Update'),
            iconCls: 'icon-update-16',
            handler: this.doSave,
            scope: this
        });

        this.bbar = ['->', this.resetBtn, '-', this.submitBtn];
        this.callParent();

        this.addEvents('activate', 'afterload', 'beforesubmit');

        this.on('activate', this.doLoad, this);

        this.on('render', function() {
            this.form.form.getFields().each(function(field) {
                switch(field.getXType()) {
                    case 'htmleditor':
                        field.on('beforesync', function() {
                            this.field = field;
                        }, this);
                        break;

                    default:
                        field.on('focus', function(f) {
                            this.field = f;
                        }, this);
                }
            }, this);
        }, this);

        this.list.on('itemdblclick', function(g, record) {

            var key = record.get('key');


            if (null == this.field) {
                return;
            }

            var text = '{' + key + '}';

            console.log(this.field.getXType());

            switch(this.field.getXType()) {
                case 'htmleditor':
                    this.field.insertAtCursor(text);
                    break;

                default:
//                    this.field.insertAtPosition(text, true);
            }
        }, this);
    },

    doLoad: function() {

//        this.getEl().mask(lang('Loading...'), 'x-mask-loading');

        Ext.Ajax.request({
            url: link('notification', 'configuration', 'fetch', {format: 'json'}),
            params: {
                pmodule: this.pmodule,
                ptemplate: this.ptemplate
            },
            success: function(response, options) {
                var decResponse = Ext.decode(response.responseText);
                this.list.getStore().loadData(decResponse.keywords);

                var f = this.form.getForm();
                f.setValues(decResponse.rowset[0]);

                this.fireEvent('afterload', this, decResponse, response.responseText);
            },
            callback: function() {
//                this.getEl().unmask();
            },
            scope: this
        });
    },

    doSave: function() {

        var o = {};
        if (false === this.fireEvent('beforesubmit', this, o)) {
            return false;
        }

        this.form.getForm().submit({
            url: link('notification', 'configuration', 'update', {format: 'json'}),
            waitMsg: lang('Saving...'),
            params: Ext.apply(o, {
                pmodule: this.pmodule,
                ptemplate: this.ptemplate
            }),
            success: function() {
                var f = this.form.getForm();
                f.setValues(f.getValues());
            },
            scope: this
        });
    }
});