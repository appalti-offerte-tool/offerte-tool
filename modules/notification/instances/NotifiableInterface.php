<?php

interface Notification_Instance_NotifiableInterface
{
    /**
     * Send notification to smb
     *
     * @param array $params
     */
    function notificate(array $params);
}