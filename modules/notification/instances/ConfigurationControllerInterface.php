<?php

interface Notification_Instance_ConfigurationControllerInterface
{
    /**
     * Retrieve configuration
     */
    public function fetchAction();

    /**
     * Update configuration
     */
    public function updateAction();
}