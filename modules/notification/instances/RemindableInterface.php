<?php

interface Notification_Instance_RemindableInterface
{
    function remind();
}