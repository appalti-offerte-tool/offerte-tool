<?php

final class Notification_Misc_Email_Feedback extends Configuration_Service_ConfigurationAbstract
    implements Notification_Instance_NotifiableInterface
{
    protected $_identity = 'notification.feedback';

    protected $_fields = array(
        'receipt',
        'subject', 'body'
    );

    protected $_mui = true;

    protected $_keywords = array(
        'subject'  => 'E-mail subject',
        'message'  => 'E-mail body',
        'name'     => 'Sender name',
        'email'    => 'Sender e-mail',
        'timestamp'=> 'Timestamp'
    );

    protected $_serialized = array('receipt', 'replyto');

    public function __construct()
    {
        parent::__construct();

        $defaultFrom = Zend_Mail::getDefaultFrom();
        if (is_array($defaultFrom)) {
            $this->_fields['receipt']['default'] = $this->_toValue($defaultFrom, 'receipt');
        }
    }

    /**
     * (non-PHPdoc)
     * @see Notification_Instance_NotifiableInterface::notificate()
     */
    public function notificate(array $params)
    {
        $f = new OSDN_Filter_Input(array(), array(
            'email'    => array('EmailAddress', 'presence' => 'required', 'allowEmpty' => false),
            'name'     => array(array('StringLength', 1, 255), 'presence' => 'required', 'allowEmpty' => false),
            'subject'  => array('allowBlank' => true),
            'message'  => array('presence' => 'required', 'allowEmpty' => false)
        ), $params);
        $f->validate(true);

        $params = $f->getData();

        $dt = new \DateTime();
        $params['timestamp'] = $dt->format('Y-m-d H:i:s');

        $instance = $this->toInstance();

        $mail = new Zend_Mail('UTF-8');
        $mail->setFrom($f->email, $f->name);
        $mail->setReplyTo($f->email, $f->name);

        $receipt = $instance->getReceipt();
        $mail->addTo($receipt['email'], $receipt['name']);

        $mail->setSubject($instance->getSubject($params));
        $mail->setBodyHtml($instance->getBody($params));

        try {
            $result = $mail->send();
        } catch(\Zend_Mail_Transport_Exception $e) {
            throw new OSDN_Exception('Unable to send. Transport error.', null, $e);
        } catch(\Exception $e) {
            throw new OSDN_Exception('Unable to send message', null, $e);
        }

        return (boolean) $result;
    }

    public function update(array $params)
    {
        /**
         * @todo Add validation
         */
        return parent::update($params);
    }

    protected function _toValue($value, $field)
    {
        if (in_array($field, $this->_serialized)) {
            return serialize($value);
        }

        return parent::_toValue($value, $field);
    }

    protected function _fromValue($value, $field)
    {
        if (in_array($field, $this->_serialized)) {
            return unserialize($value);
        }

        return parent::_fromValue($value, $field);
    }
}