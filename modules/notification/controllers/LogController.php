<?php

final class Notification_LogController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    protected $_service;

    public function init()
    {
        $this->_helper->contextSwitch()
            ->addActionContext('fetch-all', 'json')
            ->initContext();

        $this->_service = new Notification_Service_Log();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'notification:log';
    }

    public function indexAction()
    {

    }

    public function fetchAllAction()
    {
        $response = $this->_service->fetchAll($this->_getAllParams());
        $this->view->assign($response->toArray());
    }
}