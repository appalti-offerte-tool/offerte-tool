<?php

class Notification_MainController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Notification_Service_Notification
     */
    protected $_service;

    public function init()
    {
        $this->_service = new \Notification_Service_Notification();

        $this->_helper->contextSwitch()
            ->addActionContext('index', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch', 'json')
            ->initContext();

        parent::init();
    }

    public function getResourceId()
    {
        return 'notification:list';
    }

    public function indexAction()
    {
        // prepare layout
    }

    public function fetchAllAction()
    {
        $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
        $this->view->assign($response->toArray());
    }

    public function fetchAction()
    {
        $row = $this->_service->find($this->_getParam('notificationId'));
        $this->view->row = $row->toArray();
        $this->view->success = true;
    }

    public function createAction()
    {
        try {
            $notificationRow = $this->_service->create($this->_getAllParams());
            $this->view->notificationId = $notificationRow->getId();
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        try {
            $notificationRow = $this->_service->update($this->_getParam('notificationId'), $this->_getAllParams());
            $this->view->notificationId = $notificationRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->view->success = $this->_service->delete($this->_getParam('id'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}