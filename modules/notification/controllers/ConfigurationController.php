<?php

class Notification_ConfigurationController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * Contain configuration instance
     *
     * @var Configuration_Instance_Configuration
     */
    protected $_configuration;

    public function init()
    {
        parent::init();

        $dashToCamelCase = new Zend_Filter_Word_DashToCamelCase();

        $configurationCls = $dashToCamelCase->filter($this->_getParam('pmodule'));
        $configurationCls .= '_Misc_Email_';
        $configurationCls .= $dashToCamelCase->filter($this->_getParam('ptemplate'));


        if (!class_exists($configurationCls)) {
            return $this->_helper->information('Unable to find configuration class');
        }

        $this->_configuration = new $configurationCls();

        $this->_helper->ajaxContext()
             ->addActionContext('fetch', 'json')
             ->addActionContext('update', 'json')
             ->initContext();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'notification:configuration';
    }

    public function fetchAction()
    {
        $keywords = $this->_configuration->getKeywords();

        $this->view->keywords = $keywords;
        $this->view->total = count($keywords);

        $this->view->rowset = array($this->_configuration->fetch());
        $this->view->success = true;
    }

    public function updateAction()
    {
        $result = $this->_configuration->update($this->_getAllParams());
        $this->view->success = (boolean) $result;
    }
}