<?php

class Notification_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        foreach(array(
            array('roleId' => 1, 'resource' => 'notification', 'privilege' => null)
        ) as $o) {
            $this->insert('aclPermission', $o);
        }
    }

    public function down()
    {
        $this->getDbAdapter()->delete('aclPermission', array(
            'roleId = 1',
            "(resource = 'notification' OR resource LIKE 'notification:%')"
        ));
    }
}