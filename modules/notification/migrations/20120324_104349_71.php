<?php

class Notification_Migration_20120324_104349_71 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('notification');
        $this->createColumn('notification', 'caption', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('notification', 'message', self::TYPE_TEXT, 255, null, true);
        $this->createColumn('notification', 'createdAccountId', self::TYPE_INT, 11, null, true);
        $this->createIndex('notification', array('createdAccountId'), 'IX_createdAccountId');
        $this->createForeignKey('notification', array('createdAccountId'), 'account', array('id'), 'FK_createdAccountId');
        $this->createColumn('notification', 'createdDatetime', self::TYPE_DATETIME, null, null, true);
        $this->createColumn('notification', 'isActive', self::TYPE_INT, 1, null, true);

        $this->createTable('notificationLog');
        $this->createColumn('notificationLog', 'module', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('notificationLog', 'caption', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('notificationLog', 'notification', self::TYPE_TEXT, null, null, true);
        $this->createColumn('notificationLog',  'notificationId', self::TYPE_INT, 11, null, false);
        $this->createIndex('notificationLog', array('notificationId'), 'IX_notificationId');
        $this->createForeignKey('notificationLog', array('notificationId'), 'notification', array('id'), 'FK_notificationId');
        $this->createColumn('notificationLog',  'luid', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('notificationLog', 'notificatedDatetime', self::TYPE_DATETIME, null, null, true);
        $this->createColumn('notificationLog', 'createdAccountId', self::TYPE_INT, 11, null, true);
        $this->createIndex('notificationLog', array('createdAccountId'), 'IX_createdAccountId');
        $this->createForeignKey('notificationLog', array('createdAccountId'), 'account', array('id'), 'FK_createdAccountId');
    }

    public function down()
    {
        $this->dropTable('notificationLog');
        $this->dropTable('notification');
    }
}