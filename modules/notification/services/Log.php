<?php

final class Notification_Service_Log extends \OSDN_Application_Service_Dbable// implements \Notification_Instance_NotifiableInterface
{
    protected $_table;

    protected function _init()
    {
        parent::_init();

        $this->_attachValidationRules('default', array(
            'notification'     => array('presence' => 'required', 'allowEmpty' => false)
        ));

        $this->_table = new Notification_Model_DbTable_Log($this->getAdapter());
    }

    public function fetchAll(array $params = array())
    {
        $select = $this->_table->select(true);

        $this->_initDbFilter($select, $this->_table)->parse($params);
        return $this->getDecorator('response')->decorate($select);
    }

	/**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \Notification_Model_Log
     */
    public function find($id, $throwException = true)
    {
        $notificationLogRow = $this->_table->findOne($id);
        if (null === $notificationLogRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find row #' . $id);
        }

        return $notificationLogRow;
    }

    /**
     * Create the new article row
     *
     * @param array $data
     * @return Article_Model_DbTable_ArticleRow
     */
    public function create(array $data)
    {
        $f = $this->_validate($data);

        $notificationLogRow = $this->_table->createRow($f->getData());

        $this->getAdapter()->beginTransaction();
        try {

            $notificationLogRow->save();
            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $notificationLogRow;
    }
}