<?php

final class Notification_Service_Configuration extends Configuration_Service_ConfigurationAbstract
{
    protected $_identity = '1';

    protected $_fields = array('subject', 'body');

    protected $_mui = true;

    protected $_keywords = array(
        'username'      => 'Username',
        'fullname'      => 'Full name',
        'phone'		    => 'Phone',
        'email'    	    => 'E-mail address',
        'iphoneduid'	=> 'Iphone IMEI'
    );

    protected function init()
    {
        parent::init();

        $this->_table = new Notification_Model_DbTable_Notification(
            OSDN_Application_Service_Dbable::getDefaultDbAdapter()
        );
    }
}