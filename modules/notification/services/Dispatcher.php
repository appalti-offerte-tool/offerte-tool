<?php

/**
 * Provide multiple distribution to managers
 *
 * @version     $Id: Dispatcher.php 1696 2011-10-18 14:47:07Z yaroslav $
 */
final class Notification_Service_Dispatcher
{
    /**
     * Contain configuration instance object
     *
     * @var OSDN_Configuration_Instance
     */
    protected $_configurationInstance;

    /**
     * Contain allowed identities
     *
     * @var array
     */
    protected $_identityCollection;

    /**
     * Primary identity
     *
     * @var string
     */
    protected $_identity;

    /**
     * Default value for identity value
     *
     * @var mixed
     */
    protected $_defaultValue;

    /**
     * The constructor
     *
     * @param OSDN_Configuration_Abstract $configuration        The configuration instance
     * @param object                      $identity             The  primary identity
     *
     * @return void
     * @throws OSDN_Exception       If identity is not found in collection
     */
    public function __construct(OSDN_Configuration_Abstract $configuration, $identity)
    {
        $this->_configurationInstance = new OSDN_Configuration_Instance($configuration);

        if (is_null($this->_identityCollection)) {
            $reflectionCls = new ReflectionClass('CA_Config');
            $this->_identityCollection = $reflectionCls->getConstants();
        }

        if (!in_array($identity, $this->_identityCollection)) {
            throw new OSDN_Exception("The identity is not defined in allowed identities.");
        }

        $accountPreferences = CA_Config::factory(CA_Config::PREFERENCES_ACCOUNT);
        $this->_identity = $accountPreferences->toIdentity($identity);
        $this->_defaultValue = (int) $accountPreferences->getDefaultFieldValue($this->_identity);
    }

    /**
     * Send a distribution to managers
     *
     * @param string $subject       An email subject
     * @param string $body          An email body
     * @return boolean
     *
     * @throws OSDN_Exception
     */
    public function doSend(array $params)
    {
        $prefix = OSDN_Db_Table_Abstract::getDefaultPrefix();
        $db = OSDN_Db_Table_Abstract::getDefaultAdapter();

        $select = $db->select()
            ->from(
                array('a' => $prefix . 'accounts'),
                array('name', 'email', 'mailable', 'locale')
            )
            ->joinLeft(
                array('c'   => $prefix . 'configuration'),
                $db->quoteInto('a.id = c.account_id && c.param = ?', $this->_identity),
                array()
            )
            ->columns(array(
                'enabled' => new Zend_Db_Expr("IF(c.en IS NULL OR c.en = '', $this->_defaultValue, c.en)")
            ));

        try {

            $query = $select->query();

            // increase time limit for sending emails
            set_time_limit(3600);
            while(false != ($row = $query->fetch())) {

                if (empty($row['enabled']) || empty($row['mailable']) || empty($row['email'])) {
                    continue;
                }

                $mail = new Zend_Mail(OSDN_MAIL_ENCODING);
                $mail->addTo($row['email'], $row['name']);
                $mail->setFrom(OSDN_MAIL_FROM_ADDRESS, OSDN_MAIL_FROM_CAPTION);
                $mail->addHeader('X-IDENTITY-TEMPLATE', $this->_identity);

                $mail->setSubject($this->_configurationInstance->getSubject($params, $row['locale']));
                $mail->setBodyHtml($this->_configurationInstance->getBody($params, $row['locale']));

                try{
                    $mail->send();
                } catch (Exception $e) {}
            }

            return true;

        } catch(Exception $e) {
            throw new OSDN_Exception($e->getMessage(), $e->getCode());
        }
    }
}