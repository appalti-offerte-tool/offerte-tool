<?php

class Notification_Service_Notification extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Notification_Model_DbTable_Notification
     */
    protected $_table;

    protected function _init()
    {
        $this->_table = new \Notification_Model_DbTable_Notification($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'caption'     => array(array('StringLength', 1, 255), 'presence' => 'required', 'allowEmpty' => false),
            'message'     => array('presence' => 'required', 'allowEmpty' => false),
            'isActive'    => array('boolean', 'presence' => 'required', 'allowEmpty' => true)
        ));

        parent::_init();
    }

    /**
     * Create the new notification row
     *
     * @param array $data
     * @return Article_Model_DbTable_ArticleRow
     */
    public function create(array $data)
    {
        $f = $this->_validate($data);

        $notificationRow = $this->_table->createRow($f->getData());

        $this->getAdapter()->beginTransaction();
        try {

            $notificationRow->save();

            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $notificationRow;
    }

    /**
     * @param array $params
     * @return OSDN_Db_Response
     */
    public function fetchAllWithResponse(array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('a' => $this->_table->getTableName()),
                $this->_table->getAllowedColumns(\OSDN_Db_Table_Abstract::OMIT_FETCH_ROW)
            );


//        $select->order('createdDatetime DESC');

        $this->_initDbFilter($select, $this->_table)->parse($params);
        return $this->getDecorator('response')->decorate($select);
    }

    public function update($id, array $data)
    {
        $notificationRow = $this->find($id);
        $f = $this->_validate($data);

        $this->getAdapter()->beginTransaction();
        try {
            $notificationRow->setFromArray($f->getData());
            $notificationRow->save();

            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $notificationRow;
    }

    public function getNotificationCount($isActive = null)
    {
        $clause = array();
        if (null !== $isActive) {
            $clause['isActive = ?'] = (int) $isActive;
        }

        return $this->_table->count($clause);
    }

    /**
     * @todo should be done via event manager
     *
     * @FIXME Add main global transation for that
     */
    public function delete($id)
    {
        $notificationRow = $this->find($id);
        return false !== $notificationRow->delete();
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \Article_Model_DbTable_ArticleRow
     */
    public function find($id, $throwException = true)
    {
        $notificationRow = $this->_table->findOne($id);
        if (null === $notificationRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find notification #' . $id);
        }

        return $notificationRow;
    }

    /**
     * @param string $luid
     * @param boolean $throwException
     *
     * @return \Article_Model_DbTable_ArticleRow
     */
    public function fetchByLuid($luid, $throwException = true)
    {
        $notificationRow = $this->_table->fetchRow(array(
            'luid = ?' => (string) $luid
        ));

        if (null === $notificationRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find notification luid: #' . ($luid ?: 'none'));
        }

        return $notificationRow;
    }
}