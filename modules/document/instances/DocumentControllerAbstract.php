<?php

abstract class Document_Instance_DocumentControllerAbstract extends \Zend_Controller_Action
{
    /**
     * Predefined ENTITY TYPE of document
     * This is strictly predefined !!!
     * All documents will be related to this ENTITY TYPE
     *
     * @var int
     */
    protected $_entityType = null;

    /**
     * @var \Document_Service_Document
     */
    protected $_service;

    public function init()
    {
        if (null  === $this->_entityType) {
            throw new \OSDN_Exception('The entity type is not defined.');
        }

        $this->_service = new Document_Service_Document($this->_entityType);

        $this->_helper->contextSwitch()
           ->addActionContext('fetch-all-general-documents', 'json')
           ->addActionContext('fetch-all-mandatory-documents', 'json')
           ->addActionContext('fetch-all-document-type-pre-printed-forms', 'json')
           ->addActionContext('fetch', 'json')

           ->addActionContext('create', 'json')
           ->addActionContext('update', 'json')
           ->addActionContext('delete', 'json')
           ->initContext();

        parent::init();

    }

    public function updateDefaultDocumentAction()
    {
        try {
            $documentId = $this->_service->updateDefaultDocument($this->_getAllParams());
            if (false == $documentId) {
                $this->view->success = false;
            }
            $this->view->documentId = $documentId;
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAllMandatoryDocumentsAction()
    {
        try {

            $response = $this->_service->fetchAllForDocTypes($this->_getAllParams());
            if (empty($response)) {
                throw new \OSDN_Exception('Not found document types');
            }
    //        foreach ($rows as $k => $v) {
    //            $rows[$k]['templates'] = Zend_Json::encode($v['templates']);
    //        }

            $this->view->assign($response->toArray());

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAllDocumentTypePrePrintedFormsAction()
    {
        try {
//            if (!empty($data['allowedDocumentTypes'])) {
//                $data['allowedDocumentTypes'] = Zend_Json::decode($data['allowedDocumentTypes']);
//            }

            $dtFileStockService = new Document_Service_DocumentTypeFileStock();
            $response = $dtFileStockService->fetchAllFileStockByEntityTypeWithResponse($this->_entityType, $this->_getAllParams());
            $this->view->assign($response->toArray());
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAllGeneralDocumentsAction()
    {
        try {
            $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
            $this->view->assign($response->toArray());
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAction()
    {
        try {
            $response = $this->_service->find($this->_getParam('id'));
            $this->view->data = $response->toArray();

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function createAction()
    {
        try {
            $documentRow = $this->_service->insert($this->_getAllParams());
            $this->view->documentId = $documentRow->getId();
            $this->view->success = true;
            $this->_helper->information('Document has been created successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        try {
            $documentRow = $this->_service->update($this->_getParam('id'), $this->_getAllParams());
            $this->_helper->information('Document has been updated successfully', true, E_USER_NOTICE);
            $this->view->documentId = $documentRow->getId();
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $response = $this->_service->delete($this->_getParam('id'));
            $this->view->success = true;
            $this->_helper->information('Document has been deleted successfully', true, E_USER_NOTICE);
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }
}