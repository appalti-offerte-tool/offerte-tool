<?php

class Document_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        foreach(array(
            array('roleId' => 1, 'resource' => 'document', 'privilege' => null),
            array('roleId' => 1, 'resource' => 'document:category', 'privilege' => null),
            array('roleId' => 1, 'resource' => 'document:document-type', 'privilege' => null),
        ) as $o) {
            $this->insert('aclPermission', $o);
        }
    }

    public function down()
    {
        $this->getDbAdapter()->delete('aclPermission', array(
            'roleId = 1',
            "(resource = 'document' OR resource LIKE 'document:%')"
        ));
    }
}