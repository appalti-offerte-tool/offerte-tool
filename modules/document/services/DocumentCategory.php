<?php

class Document_Service_DocumentCategory extends OSDN_Application_Service_Dbable
{
    /**
     * The categories table
     *
     * @var \Document_Model_DbTable_DocumentCategory
     */
    protected $_tableCategory;

    /**
     * The entity type of categories
     *
     * @var integer
     */
    protected $_entityType = null;

     /**
     * construct function of the class
     *
     */
    public function __construct($entityType)
    {
        $this->_entityType = strtoupper($entityType);
//        'entityType'        => array(array('StringLength', 1, 20), 'allowEmpty' => false, 'presence' => 'required'),

        $this->_tableCategory = new \Document_Model_DbTable_DocumentCategory($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'id'                => array('id'),
            'name'              => array(array('StringLength', 0, 100), 'allowEmpty' => false),
            'position'          => array('int', 'allowEmpty' => false),
        ));

        $this->_attachValidationRules('create', array(
            'name'          => array(array('StringLength', 0, 100), 'allowEmpty' => false, 'presence' => 'required'),
            'position'      => array('int', 'allowEmpty' => true)
        ), 'default');


        $this->_attachValidationRules('update', array(
            'id'     => array('id', 'presence' => 'required'),
        ), 'default');

    }

    /**
     * Fetch all categories
     *
     * @param array $params
     * @return array
     */
    public function fetchAll(array $params = array())
    {
        $select = $this->_tableCategory->select(true)
            ->where('entityType = ?', $this->_entityType)
            ->order('position ASC');

        $this->_initDbFilter($select, $this->_tableCategory)->parse($params);

        return $select->query()->fetchAll();
    }

    /**
     * Add new document category
     *
     * @param array $params
     * @return Document_Model_DbTable_DocumentCategory
     */
    public function create(array $params)
    {
        $params['entityType'] = $this->_entityType;
        $f = $this->_validate($params, 'create');

        $documentCategoryRow = $this->_tableCategory->createRow($f->getData());
        $documentCategoryRow->save();

        return $documentCategoryRow;
    }

    /**
     * Update document category
     *
     * @param type $id
     * @param array $params
     * @return bool
     */
    public function update($id, array $params)
    {
        $params['id'] = $id;

        $f = $this->_validate($params, 'update');
        $data = $f->getData();

        $affectedRows = $this->_tableCategory->updateQuote($data, array(
            'id = ?'            => $id,
            'entityType = ?'    => $this->_entityType
        ));

        if (false === $affectedRows) {
            throw new \OSDN_Exception('Unable to update category.');
        }
        return true;
    }

    /**
     * Delete the document category by Pk
     *
     * @param type $id
     * @return bool
     */
    public function delete($id)
    {
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($id)) {
            throw new \OSDN_Exception('Input parameter incorrect');
        }

        $affectedRows = $this->_tableCategory->deleteByPk($id);

        if (false === $affectedRows) {
            throw new \OSDN_Exception('Unable to delete category.');
        }

        return true;
    }

    /**
     * Change order
     *
     * @param type $categoryId
     * @param type $order
     * @return boolean
     */
    public function changeOrder($categoryId, $order)
    {
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($categoryId)) {
            throw new \OSDN_Exception('Input parameter incorrect');
        }
        try {
            $this->_tableCategory->updateByPk(array('position' => $order * 2 + 1), $categoryId);
            $this->_reorderCategories();
        } catch (\Exception $e) {
            throw new \OSDN_Exception($e->getMessage());
        }
        return true;
    }

    /**
     * Get category information by id
     *
     * @param type $id
     * @return row
     */
    public function get($id)
    {
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($id)) {
            throw new \OSDN_Exception('Input parameter incorrect');
        }
        try {
            $row = $this->_tableCategory->findOne($id);
            return $row->toArray();
        } catch (Exception $e) {
            throw new \OSDN_Exception($e->getMessage());
        }
    }

    /**
     * @param array $ids
     * @return array
     */
    public function fetchAllByIds(array $ids)
    {
        if (empty($ids)) {
            throw new \OSDN_Exception('Input parameter incorrect');
        }

        try{
            $rowset = $this->_tableCategory->fetchAll(array(
                'id IN(?)'       => $ids,
                'entityType = ?' => $this->_entityType
            ));
            return $rowset->toArray();
        } catch (\Exception $e) {
            throw new \OSDN_Exception($e->getMessage());
        }
    }

    protected function _reorderCategories() {
        $rowset = $this->fetchAll();
        $position = 0;
        foreach ($rowset as $row) {
            $position+=2;
            $this->_tableCategory->updateByPk(array('position' => $position), $row['id']);
        }
        return true;
    }
}