<?php

class Document_Service_DocumentTypeReplaceable extends \OSDN_Application_Service_Dbable
{
    /**
     * The entity type
     *
     * @var string
     */
    protected $_entityType = null;

    public function __construct($entityType, array $config = array())
    {
        $this->_entityType = strtoupper($entityType);

        parent::__construct();

        $this->_table = new \Document_Model_DbTable_DocumentTypeReplaceable($this->getAdapter());
        $this->_docs = new \Document_Model_DbTable_Document($this->getAdapter());
    }

    public function create($documentTypeId, $replaceableId)
    {
        $result = $this->_table->insert(array(
            'documentTypeId'  => $documentTypeId,
            'replaceableId'   => $replaceableId
        ));

        return false !== $result;
    }

    public function delete($documentTypeId, $replaceableId)
    {
        $result = $this->_table->delete(array(
            'documentTypeId = ?' => $documentTypeId,
            'replaceableId = ?'   => $replaceableId
        ));

        return $result !== false;
    }

    public function fetchAllWithResponse($documentTypeId, array $params)
    {
        $validate = new OSDN_Validate_Id();

        if (!$validate->isValid($documentTypeId)) {
            throw new \OSDN_Exception('Invalid input parameter.');
        }

        $select = $this->getAdapter()->select()
            ->from(array('dt' => 'documentType'),array(
                '*',
                'replaceable' => new Zend_Db_Expr('(CASE WHEN dtr.id IS NOT NULL THEN 1 ELSE 0 END)')
            ))
            ->joinLeft(array('dc' => 'documentCategory'),
                "dc.id = dt.documentCategoryId",
                array(
                    'categoryName' => 'name',
                    'categoryOrder' => 'position'
                )
            )
            ->joinLeft(
                array('dtr' => $this->_table->getTableName()),
                $this->getAdapter()->quoteInto('dt.id = dtr.replaceableId AND dtr.documentTypeId= ?',  $documentTypeId, Zend_Db::INT_TYPE),
                array()
            )
            ->where("dt.entityType = ? ", $this->_entityType)
            ->where("dt.id != ? ", $documentTypeId);
        if (!empty($params['itemId'])) {
            $select->where("dt.itemId = ?", intval($params['itemId']));
        } else {
            $select->where("dt.itemId is NULL ");
        }
        $select->order('dc.position ASC');

        try {
            $this->_initDbFilter($select, $this->_table, array('name' => 'dt.name'))->parse($params);
            $response = $this->getDecorator('response')->decorate($select);
        } catch (Exception $e) {
            throw new \OSDN_Exception($e->getMessage());
        }
        return $response;
    }

    /**
     * Return list of IDs which can replace this document types.
     *
     * @param $id - document type ID
     * @return array
     */
    public function getReplaceableIdsForId($documentTypeId)
    {
        $replaceableTable = new \Document_Model_DbTable_DocumentTypeReplaceable();

        $select = $replaceableTable->getAdapter()->select();
        $select->from($replaceableTable->getTableName(), 'replaceableId');
        $select->where('documentTypeId = ?', $documentTypeId);

        $outputIds = array();
        $query = $select->query();
        while(null != ($row = $query->fetch())) {
            $outputIds[] = $row['replaceableId'];
        }

        return $outputIds;
    }

    /**
     * Return all files for document type
    *
    * @param int $documentTypeId
    * @param array $fields
    *
     * @return array
     */
    public function getFiles($documentTypeId)
    {
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($documentTypeId)) {
            throw new \OSDN_Exception('Input paremeter document type id is incorrect.');
        }

        $result = array();
        $select = $this->_tableFiles->getAdapter()->select();
        $select->from(array('dtf' => $this->_tableFiles->getTableName()));
        $select->where("dtf.documentTypeId = ?", $documentTypeId);

        try {
            $rowset = $select->query()->fetchAll();

            if ($rowset) {
                foreach ($rowset as $k => $v) {
                    if (null != ($fileStockRow = $this->_file->find($v['fileStockId']))) {
                        $result[] = $fileStockRow->toArray();
                    }
                }
            }

            return $result;
        } catch (Exception $e) {
            throw new \OSDN_Exception($e->getMessage());
        }
    }
}