<?php

class Document_Service_DocumentFileStock extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \Document_Model_DbTable_Document
     */
    protected $_table;

    /**
     * @var \FileStock_Service_FileStock
     */
    protected $_fileStock;

    public function _init()
    {
        $this->_table = new \Document_Model_DbTable_Document($this->getAdapter());
        $this->_fileStock = new \FileStock_Service_FileStock(array(
            'baseFilePath' => APPLICATION_PATH . '/data/document-files'
        ));
    }

    /**
     * Retrieve the filestock rows by document
     *
     * @param int $documentId
     * @return Zend_Db_Table_Rowset_Abstract
     * @throws OSDN_Exception
     */
    public function fetchAllFileStockByDocumentIdWithResponse($documentId)
    {
        $documentRow = $this->_table->findOne($documentId);
        if (empty($documentRow)) {
            throw new \OSDN_Exception('Unable to find document #' . $documentId);
        }

        $fileStockTable = new \FileStock_Model_DbTable_FileStock($this->getAdapter());

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('df'    => 'documentFileStockRel'),
                array()
            )
            ->join(
                array('f'    => 'fileStock'),
                'f.id = df.fileStockId',
                $fileStockTable->getAllowedColumns()
            )
            ->where('df.documentId = ?', $documentRow->getId());

        return $this->getDecorator('response')->decorate($select);
    }


//    public function deleteFile($fileStockId, $documentId)
//    {
//        $service = new \FileStock_Service_FileStock();
//
//        $documentFileStockRelation = new Document_Model_DbTable_DocumentFileStockRel($this->getAdapter());
//        $result = $service->delete($fileStockId, function(FileStock_Model_DbTable_FileStockRow $fileStockRow) use ($documentFileStockRelation, $documentId) {
//
//            $documentFileStockRelation->deleteQuote(array(
//                'documentId = ?'    => $documentId,
//                'fileStockId = ?'        => $fileStockRow->getId()
//            ));
//
//        });
//
//        return (boolean) $result;
//    }

    public function find($documentId, $fileStockId)
    {
        /**
         * @todo
         * Implement verification on assignment document to filestock
         */
        return $this->_fileStock->find($fileStockId);
    }

    public function create(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {
        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }

        $documentRow = $this->_table->findOne($data['documentId']);

        $fileStockRow = $this->_fileStock->create($transfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow) use ($documentRow) {
            $documentRow->getTable()->getAdapter()->insert('documentFileStockRel', array(
                'fileStockId'    => $fileStockRow->getId(),
                'documentId'=> $documentRow->getId()
            ));
        });

        return $fileStockRow;
    }

    public function update(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {
        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }

        $this->_fileStock->update($transfer, $data);
    }

    public function delete($documentId, $fileStockId, $skipUsingInternalTransaction = false)
    {
        $documentRow = $this->_table->findOne($documentId);

        if (true !== $skipUsingInternalTransaction) {
            $result = $this->_fileStock->delete($fileStockId, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow) use ($documentRow) {
                $documentFileStockRelation = new \Document_Model_DbTable_DocumentFileStockRel();
                $documentFileStockRelation->delete(array(
                    'documentId = ?'    => $documentRow->getId(),
                    'fileStockId = ?'        => $fileStockRow->getId()
                ));
            });
        } else {
            $result = $this->_fileStock->delete($fileStockId, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow) use ($documentRow) {
                $documentFileStockRelation = new \Document_Model_DbTable_DocumentFileStockRel();
                $documentFileStockRelation->delete(array(
                    'documentId = ?'    => $documentRow->getId(),
                    'fileStockId = ?'        => $fileStockRow->getId()
                ));
            });
        }

        return (boolean) $result;
    }

    public function deleteByDocument(Document_Model_DbTable_DocumentRow $documentRow, $skipUsingInternalTransaction = false)
    {
        $documentFileStockRelation = new \Document_Model_DbTable_DocumentFileStockRel();
        $rowset = $documentFileStockRelation->fetchAll(array(
            'documentId = ?'    => $documentRow->getId()
        ));

        foreach($rowset as $row) {
            $this->delete($documentRow->getId(), $row->fileStockId, $skipUsingInternalTransaction);
        }

        return true;
    }
}