<?php

class Document_Service_Document extends OSDN_Application_Service_Dbable
{
    /**
     * @var \Document_Model_DbTable_Document
     */
    protected $_table;

    /**
     * @var \Document_Model_DbTable_DocumentType
     */
    protected $_docTypes;

    /**
     * @var \Document_Model_DbTable_DocumentFileStockRel
     */
    protected $_docFiles;

    /**
     * @var int etity type of document
     */
    protected $_entityType = null;

    /**
     * @var array - allowed files mime types
     */
    protected $_allowedDocumentTypes = array();

    public function __construct($entityType = null, array $config = array())
    {
        if (!empty($entityType)) {
            $this->_entityType = $entityType;
        }

        if (!empty($config['allowedDocumentTypes'])) {
            $this->_allowedDocumentTypes = $config['allowedDocumentTypes'];
        }

        if (isset($config['allowedComments'])) {
            $this->_allowedComments = $config['allowedComments'];
        } else {
            $this->_allowedComments = true;
        }
        $this->_table = new \Document_Model_DbTable_Document($this->getAdapter());
        $this->_docTypes = new \Document_Model_DbTable_DocumentType($this->getAdapter());
        $this->_docFiles = new \Document_Model_DbTable_DocumentFileStockRel($this->getAdapter());

        $this->_file = new \FileStock_Service_FileStock();

        parent::__construct();
    }


    protected function _init()
    {
        parent::_init();

        $this->_attachValidationRules('default', array(
            'entityId'          => array('id', 'presence' => 'required'),
            'itemId'            => array('id', 'allowEmpty' => true),
            'entityType'      => array(array('StringLength', 1, 20), 'allowEmpty' => false, 'presence' => 'required'),
            'documentTypeId'    => array('id', 'allowEmpty' => true),
            'documentSourceId'  => array('id', 'allowEmpty' => true),
            'question'          => array('int', 'allowEmpty' => true),
            'title'             => array(array('StringLength', 0, 100), 'allowEmpty' => true),
            'author'            => array(array('StringLength', 0, 100), 'allowEmpty' => true),
            'subject'           => array(array('StringLength', 0, 100), 'allowEmpty' => true),
            'keywords'          => array(array('StringLength', 0, 100), 'allowEmpty' => true),
        ));

        $this->_attachValidationRules('update', array(
            'id'     => array('id', 'presence' => 'required'),
        ), 'default');

        $this->_attachValidationRules('fetch', array(
            'entityId'            => array('id', 'allowEmpty' => false, 'presence' => 'required')
        ));
    }

    public function find($id, $throwException = true)
    {
        $documentRow = $this->_table->findOne($id);
        if (true === $throwException &&  null === $documentRow) {
            throw new \OSDN_Exception('Unable to find document');
        }

        return $documentRow;
    }

    /**
     * Fetch all document types without documents
     *
     * @param int $entityId
     */
    public function fetchAllEmptyDocumentTypesWithResponse($entityId)
    {
        $db = $this->getAdapter();
        $select = $db->select()
            ->from(
                array('dt' => $this->_docTypes->getTableName())
            )
            ->joinLeft(
                array('d' => $this->_table->getTableName()),
                $db->quoteInto('d.documentTypeId = dt.id AND d.entityId = ?', $entityId, Zend_Db::INT_TYPE),
                array()
            )
            ->where('dt.entityType = ?', $this->_entityType)
            ->where('dt.itemId IS NULL')
            ->where('d.id IS NULL')
            ->group('dt.id');

        return $this->getDecorator('response')->decorate($select);
    }

    /**
     * Return all document for doc type
     *
     * @return OSDN_Response
     */
    public function fetchAllForDocType(array $params = array())
    {
        $response = new OSDN_Response();

        $documentSoursesTable = new OSDN_Documents_Table_DocumentSourses();

        $select = $this->_table->getAdapter()->select()
            ->from(array('d' => $this->_table->getTableName()))
            ->joinLeft(array('s' => $documentSoursesTable->getTableName()),
                "d.documentSourceId = s.id", array(
                    'sourceId'   => 'id',
                    'sourceName' => 'name'
                ))
            ->where("d.entityId = ?", intval($params['entityId']))
            ->where("d.entityType = ?", $this->_entityType)
            ->where("d.documentTypeId = ?", intval($params['documentTypeId']))
            ->order('d.title ASC');
        $status = null;
        try {
            $rowset = $select->query()->fetchAll();

            if ($rowset) {
                $templates = array();
                foreach ($rowset as $k => $v) {
                    if (isset($v['id'])) {
                        $select = $this->_docFiles->getAdapter()->select()
                            ->from(array('dtf' => $this->_docFiles->getTableName()))
                            ->where("dtf.documentId = ?", $v['id']);
                        $rows = $select->query()->fetchAll();

                        if ($rows) {
                            $rowset[$k]['files'] = array();
                            foreach ($rows as $row) {
                                if (null != ($fileStockRow = $this->_file->find($row['fileStockId']))) {
                                    $rowset[$k]['files'][] = $fileStockRow->toArray();
                                }
                            }
                        }
                    }
                }
            }

            $response->setRowset($rowset);
            $status = OSDN_Documents_Status::OK;
        } catch (Exception $e) {
            if (OSDN_DEBUG) {
                throw $e;
            }
            $status = OSDN_Documents_Status::DATABASE_ERROR;
        }
        return $response->addStatus(new OSDN_Documents_Status($status));
    }

    /**
     * Return all document for each doc type
     *
     * @FIXME All validation rules must be set in this method
     * @return OSDN_Response
     *
     * @todo fix this method for fetch mandatory documents
     *
     */
    public function fetchAllForDocTypes($params)
    {
        $f = $this->_validate($params, 'fetch');
        $params = $f->getData();

        $db = $this->getAdapter();

        $documentSources = new \Document_Model_DbTable_DocumentSource($this->getAdapter());
        $categoriesTable = new \Document_Model_DbTable_DocumentCategory($this->getAdapter());

        $select = $this->_table->getAdapter()->select();

        $select->from(
            array('dt' => $this->_docTypes->getTableName()),
            array(
                'name', 'required', 'question',
                'documentTypeId' => 'id'
            )
        );

        $onWhere = $db->quoteInto('d.entityType = ?', $this->_entityType);
        if (!empty($params['tempentityType'])) {
            $onWhere = '(' . $onWhere . ' OR ' . $db->quoteInto('d.entityType = ?', $params['tempentityType']) . ')';
            $onWhere .= ' AND (d.originalId < 1 OR d.originalId IS NULL)';
        }

        $select->joinLeft(
            array('d' => $this->_table->getTableName()),
            $db->quoteInto('d.documentTypeId = dt.id and d.entityId = ? AND ' . $onWhere, $params['entityId'], Zend_Db::INT_TYPE),
            array(
                'id', 'title', 'subject', 'documentSourceId',
                'presence', 'receiveDate', 'requestDate', 'originalId',
                'tempentityType' => 'entityType'
            )
        );

        $select->joinLeft(
            array('s' => $documentSources->getTableName()),
            'd.documentSourceId = s.id',
            array(
                'sourceId'      => 'id',
                'sourceName'    => 'name',
                'isPresence'
            )
        );

        $select->joinLeft(
            array('dc' => $categoriesTable->getTableName()),
            'dc.id = dt.documentCategoryId',
            array(
                'categoryOrder' => 'position',
                'categoryName' => 'name',
                'categoryId'   => 'id'
            )
        );

        if (!empty($params['documentTypeId'])) {
            $select->where('documentTypeId = ?', $params['documentTypeId'], Zend_Db::INT_TYPE);
        }

        if (!empty($params['documentCategoryId'])) {
            $select->where('dt.categoryId = ?', $params['categoryId'], Zend_Db::INT_TYPE);
        }

        if (empty($params['itemId'])) {
            $select->where('dt.itemId is NULL');
        } else {
            $select->where('dt.itemId = ?', $params['itemId'], Zend_Db::INT_TYPE);
        }

//        if (isset($params['allowedDocumentTypes'])) {
//            if (empty($params['allowedDocumentTypes'])) {
//                $select->where('dt.id = -1');
//            } else {
//                $select->where('dt.id IN (?)', $params['allowedDocumentTypes'], Zend_Db::INT_TYPE);
//            }
//        }

        $select->where('dt.entityType = ?', $this->_entityType);
        $select->order('dc.position ASC');
        $select->order('dt.position ASC');

        $this->_initDbFilter($select, $this->_table)->parse($params);

        if ($this->_allowedComments) {
            $commentary = new \Comment_Service_Comment();
        }

        $docTypes = new \Document_Service_DocumentType($this->_entityType, $this->_allowedDocumentTypes);
        $docFiles = $this->_docFiles;
        $file = $this->_file;
        $templates = array();

        $response = $this->getDecorator('response')->decorate($select);
        $response->setRowsetCallback(function($row) use($commentary, & $templates, $docTypes, $docFiles, $file) {

            if (empty($row['id'])) {      // @todo check id
                return $row;
            }

            $replaceableIds = $docTypes->getReplaceableIdsForId($row['documentTypeId']);
            if (in_array($row['documentTypeId'], $replaceableIds) && !empty($row['receiveDate'])) {
                $row['required'] = 0;
            }

            $select = $docFiles->getAdapter()->select()
                ->from(array('dtf' => $docFiles->getTableName()))
                ->where('dtf.documentId = ?', $row['id']);

            $rows = $select->query()->fetchAll();

            if ($rows) {
                $row['files'] = array();
                foreach ($rows as $fileRow) {
                    if (null != ($fileStockRow = $file->find($fileRow['fileStockId']))) {
                        $row['files'][] = $fileStockRow->toArray();
                    }
                }
            }

            if (!isset($templates[$row['documentTypeId']])) {
                $res = $docTypes->getFiles($row['documentTypeId']);
                $templates[$row['documentTypeId']] = $res;
            }

            $row['templates'] = $templates[$row['documentTypeId']];

            if (null !== $commentary) {
                $commentaryResponse = $commentary->fetchCount(\Document_Model_DbTable_Document::ENTITY, $row['id']);
                $row['comments'] = $commentaryResponse['count'];
            }

            return $row;
        });

        return $response;
    }

    /**
    * Fetch document with full info
    *
    * @param int $id
    * @return OSDN_Response
    */
    public function fetchWithFullInfo($id)
    {
        $response = new OSDN_Response();
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($id)) {
            return $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::INPUT_PARAMS_INCORRECT, 'id'));
        }
        $documentSourses = new OSDN_Documents_Table_DocumentSourses();
        $categoriesTable = new OSDN_Documents_Table_Categories();

        $select = $this->_table->getAdapter()->select()
            ->from(array('dt' => $this->_docTypes->getTableName()), array())
            ->joinLeft(array('d' => $this->_table->getTableName()),
                'd.documentTypeId = dt.id'
            )
            ->joinLeft(array('s' => $documentSourses->getTableName()),
                'd.documentSourceId = s.id',
                array(
                    'sourceId'      => 'id',
                    'sourceName'    => 'name'
                )
            )
            ->joinLeft(array('dc' => $categoriesTable->getTableName()),
                'dc.id = dt.categoryId',
                array(
                    'categoryName' => 'name',
                    'categoryId'   => 'id'
                )
            )
            ->where('d.id = ?', $id);

        $row = $select->query()->fetch();
        $response->setRow($row);
        $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::OK));
        return $response;

    }

    /**
    * Return all document
    *
    * @param object $params
    *
    * @return response
    */
    public function fetchAllWithResponse($params)
    {
        $documentSources = new \Document_Model_DbTable_DocumentSource();

        $select = $this->_table->getAdapter()->select();
        $select->from(
            array('d' => $this->_table->getTableName()),
            array('id', 'title', 'subject', 'documentSourceId', 'presence')
        );

        $select->joinLeft(array('s' => $documentSources->getTableName()),
            'd.documentSourceId = s.id', array(
                'sourceId'   => 'id',
                'sourceName' => 'name',
                'isPresence'
            ));

        $select->where('d.documentTypeId IS NULL');
        $select->where('d.entityId = ? ', $params['entityId'], Zend_Db::INT_TYPE);
        if (!empty($params['itemId'])) {
            $select->where('d.itemId = ?', $params['itemId'], Zend_Db::INT_TYPE);
        } else {
            $select->where('d.itemId IS NULL');
        }
        if(!isset($params['entityType'])) {
            $select->where('d.entityType = ?', $this->_entityType);
        } else {
            $select->where('(d.entityType = ?', $this->_entityType);
            $select->orWhere('d.entityType = ?)', $params['entityType']);
            $select->where('d.originalId < 1 OR d.originalId IS NULL');
        }

        $this->_initDbFilter($select, $this->_table)->parse($params);

        $response = $this->getDecorator('response')->decorate($select);

        $commentary = null;
        if ($this->_allowedComments) {
            $commentary = new \Comment_Service_Comment();
        }

        $docFiles = $this->_docFiles;
        $file = $this->_file;

        $response->setRowsetCallback(function($row) use($commentary, $docFiles, $file) {

            if (empty($row['id'])) {      // @todo check id
                return $row;
            }

            $select = $docFiles->getAdapter()->select()
                ->from(array('dtf' => $docFiles->getTableName()))
                ->where('dtf.documentId = ?', $row['id']);
            $rows = $select->query()->fetchAll();

            if ($rows) {
                $row['files'] = array();
                foreach ($rows as $fileRow) {
                    if (null != ($fileStockRow = $file->find($fileRow['fileStockId']))) {
                        $row['files'][] = $fileStockRow->toArray();
                    }
                }
            }

            if (null !== $commentary) {
                $commentaryResponse = $commentary->fetchCount(\Document_Model_DbTable_Document::ENTITY, $row['id']);
                $row['comments'] = $commentaryResponse['count'];
            }

            return $row;
        });

        return $response;
    }

    /**
     * Insert/update default document
     *
     * @param array $data
     *
     * @return OSDN_Response
     */
    public function updateDefaultDocument($data)
    {
        $response = new OSDN_Response();

        $data['entityType'] = $this->_entityType;

        $validators = array(
            'entityId'                 => array('id', 'presence' => 'required'),
            'entityType'             => array('id', 'presence' => 'required'),
            'itemId'                   => array('id', 'allowEmpty' => true),
            'documentTypeId'          => array('id', 'allowEmpty' => true),
        );
        //'expired_date'

        $filterInput = new OSDN_Filter_Input(array(), $validators, $data);
        $response->addInputStatus($filterInput);

        if ($response->hasNotSuccess()) {
            return $response;
        }
        $id = false;
        if (isset($data['documentTypeId'])) {
            $documentTypeId = intval($data['documentTypeId']);

            $params = array(
                'documentTypeId = ?'  => $filterInput->documentTypeId,
                'entityType = ?'     => $this->_entityType,
                'entityId = ?'         => $filterInput->entityId
            );
            if ($filterInput->itemId) {
                $params['itemId'] = $filterInput->itemId;
            }

            $docRow = $this->_table->fetchRow($params);

            if ($docRow) {
                $id = $docRow->id;
            }
            $row = $this->_docTypes->findOne($documentTypeId);
            $data['title'] = $row->name;
        }

        $sources = new OSDN_Documents_Sources();
        $res = $sources->getDefault();
        if ($res->isError()) {
            return $res;
        }
        $resData = $res->getRow();

        if (!$id || !$docRow['documentSourceId']) {
            $data['documentSourceId'] = $resData['id'];
        }

        /**
         * insert
         */
        if ($id) {
            $docId = $this->_table->updateByPk($data, $id);
            $docId = $id;
        } else {
            $docId = $this->_table->insert($data);
        }

        $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::OK));
        $response->docId = $docId;
        return $response;
    }

    /**
     * Insert new document
     *
     * @param array $data
     *
     * @return int
     */
    public function createEmptyDocument($data)
    {
        $data['entityType'] = $this->_entityType;

        $f = $this->_validate($data);
        $data = $f->getData();

        if (isset($data['documentTypeId'])) {
            $documentRow = $this->_table->fetchRow(array(
                'documentTypeId = ?' => $data['documentTypeId'],
                'entityType = ?'     => $this->_entityType,
                'entityId = ?'       => $data['entityId']
            ));

            if (null !== $documentRow) {
                return $documentRow;
            }

            $row = $this->_docTypes->findOne($data['documentTypeId']);
            $data['title'] = !is_null($row) ? $row->name : 'Untitled';

        }

        $data['requestDate'] = new Zend_Db_Expr('NULL');

        $documentRow = $this->_table->createRow($data);
        $documentRow->save();

        return $documentRow;
    }

    /**
     * Insert new document
     *
     * @param array $data
    *
     * @return OSDN_Response
     */
    public function insert(array $data)
    {
        $data['entityType'] = $this->_entityType;

        $f = $this->_validate($data);
        $data = $f->getData();

        $documentRow = $this->_table->createRow($data);
        $documentRow->save();

        return $documentRow;
    }

    /**
     * Update document
     *
     * @param int $id
     * @param array $data
     *
     * @return documentRow
     */
    public function update($id, array $data)
    {

        $documentRow = $this->find($id);

        $data['entityType'] = $this->_entityType;
        $f = $this->_validate($data, 'update');

        $data['presence'] = '1';

        if (!empty($data['removeRequest']) && !$documentRow->presence) {
            $data['requestDate'] = new Zend_Db_Expr('NULL');
            $data['presence'] = 0;
        }

        if (!empty($data['makeRequest']) && !$documentRow->presence) {
            $requestedDateTime = new \DateTime();
            $data['requestDate'] = $requestedDateTime->format('Y-m-d H:i:s');
            $data['presence'] = 0;
        }

        $this->getAdapter()->beginTransaction();
        try {
            $documentRow->setFromArray($data);
            $documentRow->save();

            if (
                null !== ($documentSource = $documentRow->getDocumentStorageSource()) &&
                $documentSource->isPresence()
            ) {
                $documentFileStock = new \Document_Service_DocumentFileStock();
                $documentFileStock->deleteByDocument($documentRow);
            }

            $this->getAdapter()->commit();

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update document', 0, $e);
        }

        return $documentRow;
    }

    /**
     * Change Row Id
     *
     * @param int $old_id
     * @param int $new_id
     *
     * @return OSDN_Response
     */
    public function changeRowId($old_id, $new_id)
    {
        $response = new OSDN_Response();

        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($old_id)) {
            $response->addStatus(new OSDN_Documents_Status(
                OSDN_Documents_Status::INPUT_PARAMS_INCORRECT, 'old_id'));
            return $response;
        }
        if (!$validate->isValid($new_id)) {
            $response->addStatus(new OSDN_Documents_Status(
                OSDN_Documents_Status::INPUT_PARAMS_INCORRECT, 'new_id'));
            return $response;
        }


        $tableDocuments = new Zend_Db_Table(array(
            'name' => $this->_table->getTableName()
        ));

        $affectedRows = $tableDocuments->update(array(
            'id' => $new_id
        ), $tableDocuments->getAdapter()->quoteInto('id = ?', $old_id));


        $status = null;
        if ($affectedRows > 0) {
            $status = OSDN_Documents_Status::UPDATED;
        } else if ($affectedRows === 0) {
            $status = OSDN_Documents_Status::UPDATED_NO_ONE_ROWS_UPDATED;
        } else {
            $status = OSDN_Documents_Status::FAILURE;
        }

        $response->addStatus(new OSDN_Documents_Status($status));
        $response->affectedRows = $affectedRows;
        return $response;
    }


    /**
     * Delete document
     *
     * @param int $id
     * @return boolean
     */
    public function delete($id)
    {
        $documentRow = $this->find($id);

        $documentFileStock = new Document_Service_DocumentFileStock();

        $this->getAdapter()->beginTransaction();

        try {

            $documentFileStock->deleteByDocument($documentRow, true);

            $comments = new \Document_Service_DocumentComment();
            $comments->deleteByDocument($documentRow);

            $documentRow->delete();

            $this->getAdapter()->commit();

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to delete document');
        }

        return true;
    }

    /**
     * Delete document for entity id
     *
     * @param int $entityId
     * @return OSDN_Response
     */
    public function deleteAllWithEntityId($entityId)
    {
        $response = new OSDN_Response();
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($entityId)) {
            $response->addStatus(new OSDN_Documents_Status(
                OSDN_Documents_Status::INPUT_PARAMS_INCORRECT, 'entity id'));
            return $response;
        }

        $rowset = $this->_table->fetchAll(array(
            'entityId = ?'        => $entityId,
            'entityType = ?'    => $this->_entityType
        ));

        foreach($rowset as $row) {
            $res = $this->delete($row['id']);
            if ($res->isError()) {
                return $res;
            }
        }

        $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::DELETED));
        return $response;
    }

    /**
    * Delete all with set entity id and item id (for example documents conected to student and course)
    *
    * @param int $entityId
    * @param int $itemId
    * @return OSDN_Response
    */
    public function deleteAllWithEntityIdAndItemId($entityId, $itemId)
    {
        $response = new OSDN_Response();
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($entityId)) {
            $response->addStatus(new OSDN_Documents_Status(
                OSDN_Documents_Status::INPUT_PARAMS_INCORRECT, 'entity id'));
            return $response;
        }
        if (!$validate->isValid($itemId)) {
            $response->addStatus(new OSDN_Documents_Status(
                OSDN_Documents_Status::INPUT_PARAMS_INCORRECT, 'item id'));
            return $response;
        }

        $rowset = $this->_table->fetchAll(array(
            'entityId = ?'        => $entityId,
            'entityType = ?'    => $this->_entityType,
            'itemId = ?'          => $itemId
        ));

        foreach($rowset as $row) {
            $res = $this->delete($row['id']);
            if ($res->isError()) {
                return $res;
            }
        }

        $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::DELETED));
        return $response;
    }

    /**
     * Return entity type
     *
     * @return void
     */
    public function getEntityType()
    {
        return $this->_entityType;
    }

    /**
    * @param int $id
    * @param int $entityType
    * @param bool $checkIfExist
    * @return OSDN_Response
    */
    public function getTemporary($id, $entityType = null, $checkIfExist = true)
    {
        $response = new OSDN_Response();
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($id)) {
            return $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::INPUT_PARAMS_INCORRECT, 'id'));
        }
        if ($entityType != null && !$validate->isValid($id)) {
            return $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::INPUT_PARAMS_INCORRECT, 'id'));
        }

        if ($checkIfExist) {
            $row = $this->_table->select()->from($this->_table->getTableName(), array('id'))
            ->where('originalId = ?', $id)->query()->fetch();
            if ($row) {
                $response->id = $row['id'];
                $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::OK));
                return $response;
            }

            $row = $this->_table->select()->from($this->_table->getTableName(), array('id'))
            ->where('id = ?', $id)
            ->where('originalId > 0')->query()->fetch();
            if ($row) {
                $response->id = $row['id'];
                $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::OK));
                return $response;
            }

        }

        $newId = $this->_table->cloneRow($id);
        $data = array('originalId' => $id);
        if($entityType != null) {
            $data['entityType'] = $entityType;
        }
        $this->_table->updateByPk($data, $newId);
        $rows = $this->_docFiles->
        select()->from(
            $this->_docFiles->getTableName(),
            array('documentId', 'fileStockId')
        )
        ->where('documentId = ?',$id)
        ->query()
        ->fetchAll();
        foreach($rows as $k => $v) {
            $inner = $v;
            $inner['documentId'] = $newId;
            $this->_docFiles->insert($inner);
        }
        $response->id = $newId;
        $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::OK));
        return $response;
    }

    /**
    * return temporary row
    * @param int $id
    * @return OSDN_Response
    */
    public function findTemporary($id)
    {
        $response = new OSDN_Response();
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($id)) {
            return $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::INPUT_PARAMS_INCORRECT, 'id'));
        }

        $row = $this->_table->fetchRow(array('originalId = ?' => $id));

        if ($row) {
            $response->setRow($row->toArray());
        } else {
            $response->setRow($row);
        }
        $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::OK));
        return $response;
    }

    /**
     * Check if entity has missing documents
     *
     * @param int $entityId            The entity type id
     * @param int|null $itemId        The item id            (Used for double entities)
    */
    public function hasMissingDocuments($entityId, $itemId = null)
    {
        $response = new OSDN_Response();

        $prefix = OSDN_Db_Table_Abstract::getDefaultPrefix();
        $a = $this->_table->getAdapter();

        $select = $a->select()
            ->from(
                array('dt' => $prefix . 'documents_types'),
                array('id')
            )

            ->joinLeft(
                array('dt_replaceable' => $prefix . 'documents_types_replaceable'),
                'dt_replaceable.documentTypeId = dt.id',
                array()
            )

            ->joinLeft(
                array('d_replaceable'    => $prefix . 'documents'),
                join(' AND ', array(
                    'd_replaceable.documentTypeId = dt_replaceable.replaceable_id',
                    $a->quoteInto('d_replaceable.entityId = ?', $entityId),
                    $a->quoteInto('d_replaceable.entityType = ?', $this->_entityType)
                )),
                array(
                    'allowed_count'    => new Zend_Db_Expr('COUNT(d_replaceable.presence)')
                )
            )

            ->joinLeft(
                array('d' => $prefix . 'documents'),
                join(' AND ', array(
                    'd.documentTypeId = dt.id',
                    $a->quoteInto('d.entityId = ?', $entityId),
                    $a->quoteInto('d.entityType = ?', $this->_entityType)
                )),
                array()
            )

            ->where('dt.required = ?', 1)
            ->where('d.id IS NULL')
            ->where('dt.entityType = ?', $this->_entityType);

        if (empty($itemId)) {
            $select->where('dt.itemId IS NULL');
        } else {
            $select->where('dt.itemId = ?', $itemId);
        }

        $select->group('dt.id');
        $select->having('allowed_count = 0');

        $query = $select->query();
        $i = 0;
        while($query->fetch()) {
            $i++;
        }

        return $i > 0;
    }
}