<?php

class Document_Service_DocumentType extends OSDN_Application_Service_Dbable
{
    const DOCUMENT_TYPE_FORMULIER = 'FORMULIER';

    /**
     * The entity type
     *
     * @var string
     */
    protected $_entityType = null;

    public function __construct($entityType, array $config = array())
    {
        $this->_entityType = strtoupper($entityType);

        parent::__construct();

        $this->_table = new \Document_Model_DbTable_DocumentType($this->getAdapter());
        $this->_docs = new \Document_Model_DbTable_Document($this->getAdapter());
        $this->_tableFiles = new \Document_Model_DbTable_DocumentTypeFileStockRel($this->getAdapter());

        $this->_file = new FileStock_Service_FileStock();

        $this->_attachValidationRules('default', array(
            'entityType'          => array(array('StringLength', 1, 20), 'allowEmpty' => false, 'presence' => 'required'),
            'documentCategoryId'    => array('int', 'allowEmpty' => true),
            'name'                  => array(array('StringLength', 0, 100), 'allowEmpty' => true),
            'question'              => array(array('StringLength', 0, 255), 'allowEmpty' => true),
            'abbreviation'          => array(array('StringLength', 0, 100), 'allowEmpty' => true),
            'expireDateRequired'    => array('int', 'allowEmpty' => true),
            'required'              => array('int', 'allowEmpty' => true)
        ));

        $this->_attachValidationRules('update', array(
            'id'     => array('id', 'presence' => 'required'),
        ), 'default');

    }

    /**
     * Move document type on one position down
     *
     * @param int $id
     * @param int $itemId [optional]
     * @param int $categoryId [optional]
     * @return OSDN_Response
     */
    public function moveDown($id, $itemId = null, $documentCategoryId = null)
    {
        if (false === $this->normalizePosition($itemId, $documentCategoryId)) {
            return false;
        }

        if (null === ($documentTypeRow = $this->_table->findOne($id))) {
            throw new \OSDN_Exception('Unable to find document type');
        }

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('dt' => $this->_table->getTableName()),
                array(
                    'max' => new Zend_Db_Expr('MAX(' . $this->getAdapter()->quoteIdentifier('position') . ')')
                )
            )
            ->where('entityType = ?' , $this->_entityType);

        $whereClause = array();
        if (empty($itemId)) {
            $whereClause[] = array('itemId IS NULL');
        } else {
            $whereClause[] = array('itemId = ?', $itemId, Zend_Db::INT_TYPE);
        }

        if (empty($documentCategoryId)) {
            $whereClause[] = array('documentCategoryId IS NULL');
        } else {
            $whereClause[] = array('documentCategoryId = ?', $documentCategoryId, Zend_Db::INT_TYPE);
        }

        foreach($whereClause as $where) {
            call_user_func_array(array($select, 'where'), $where);
        }

        $max = (int) $select->query()->fetchColumn(0);

        if ($max == $documentTypeRow->position) {
            return true;
        }

        $whereClause[] = array('position = ?', $documentTypeRow->position + 1);

        $positionDownClause = array();
        foreach($whereClause as $where) {
            if (1 == count($where)) {
                $positionDownClause[] = current($where);
            } else {
                $positionDownClause[] = call_user_func_array(array($this->getAdapter(), 'quoteInto'), $where);
            }
        }

        try {
            $this->getAdapter()->beginTransaction();

            $this->_table->update(array(
                'position' => new Zend_Db_Expr($this->getAdapter()->quoteIdentifier('position') . ' - 1')
            ), $positionDownClause);

            $this->_table->updateQuote(array(
                'position'      => new Zend_Db_Expr($this->getAdapter()->quoteIdentifier('position') . ' + 1')
            ), array(
                'id = ?'            => $id,
                'entityType = ?'    => $this->_entityType
            ));

            $this->getAdapter()->commit();

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();

            throw $e;
        }

        return true;
    }

    /**
     * Move document type on one position up
     *
     * @param int $id
     * @param int $itemId [optional]
     * @param int $categoryId [optional]
     * @return OSDN_Response
     */
    public function moveUp($id, $itemId = null, $documentCategoryId = null)
    {
        if (false === $this->normalizePosition($itemId, $documentCategoryId)) {
            return false;
        }

        if (null === ($documentTypeRow = $this->_table->findOne($id))) {
            throw new \OSDN_Exception('Unable to find document type');
        }

        if (0 == $documentTypeRow->position) {
            return $data;
        }

        $whereClause = array(
            'entityType = ?' => $this->_entityType,
            'position = ?'   => $documentTypeRow->position - 1
        );

        if (empty($itemId)) {
            $whereClause[] = 'itemId IS NULL';
        } else {
            $whereClause[] = $this->getAdapter()->quoteInto('itemId = ?', $itemId, Zend_Db::INT_TYPE);
        }

        if (empty($documentCategoryId)) {
            $whereClause[] = 'documentCategoryId IS NULL';
        } else {
            $whereClause[] = $this->getAdapter()->quoteInto('documentCategoryId = ?', $documentCategoryId, Zend_Db::INT_TYPE);
        }

        try {
            $this->getAdapter()->beginTransaction();

            $affectedRows = $this->_table->update(array(
                'position' => new Zend_Db_Expr($this->getAdapter()->quoteIdentifier('position') . ' + 1')
            ), $whereClause);

            $affectedRows = $this->_table->updateQuote(array(
                'position' => new Zend_Db_Expr($this->getAdapter()->quoteIdentifier('position') . ' - 1' )
            ), array(
                'id = ?'         => $id,
                'entityType = ?' => $this->_entityType
            ));

            $this->getAdapter()->commit();

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();

            throw $e;
        }

        return true;
    }

    /**
     * Normalize position field
     *
     * @param int $itemId [optional]
     * @return OSDN_Response
     */
    public function normalizePosition($itemId = null, $categoryId = null)
    {
        $select = $this->_table->getAdapter()->select()
            ->from(
                array('dt' => $this->_table->getTableName()),
                array(
                    'max'     => new Zend_Db_Expr('MAX(' . $this->getAdapter()->quoteIdentifier('position') . ')'),
                    'count'   => new Zend_Db_Expr('COUNT(DISTINCT(' . $this->getAdapter()->quoteIdentifier('position') . '))')
            ))
            ->where('dt.entityType = ?', $this->_entityType);

        if (empty($itemId)) {
            $select->where('dt.itemId IS NULL');
        } else {
            $select->where('dt.itemId = ?', $itemId);
        }

        if (empty($categoryId)) {
            $select->where('dt.documentCategoryId IS NULL');
        } else {
            $select->where('dt.documentCategoryId = ?', $categoryId);
        }

        try {
            $data = $select->query()->fetch();
        } catch (\Exception $e) {
            throw new \OSDN_Exception($e->getMessage());
        }

        if ($data['count'] - 1 == $data['max'] && $data['count'] != 1) {
            return true;
        }

        $select->reset(Zend_Db_Select::COLUMNS);
        $select->columns(array(
            'dt.id'
        ));
        $select->order('position ASC');

        try {
            $rowset = $select->query()->fetchAll();
        } catch (Exception $e) {
            throw new \OSDN_Exception($e->getMessage());
        }

        $position = 0;
        foreach ($rowset as $row) {

            $this->_table->updateByPk(array(
                'position' => $position
            ), $row['id']);

            $position++;
        }

        return true;
    }

    /**
     * Retrieve the document types
     *
     * @param array $params
     * @param boolean $countable        True to allow the rows calculation
     * @return OSDN_Response
     */
    public function fetchAll(array $params = array(), $countable = false)
    {
        $response = new OSDN_Response();

        $select = $this->_table->getAdapter()->select()
            ->from(array('dt' => $this->_table->getTableName()))
            ->where('dt.entityType = ?', $this->_entityType);

        if (!empty($params['documentCategoryId'])) {
            $select->where('dt.documentCategoryId = ?', intval($params['documentCategoryId']));
        }

        $select->order('dt.name');
        if (!empty($params) || $countable) {
            $plugin = new OSDN_Db_Plugin_Select($this->_table, $select);
            $plugin->parse($params);
        }

        $status = null;
        try {
            $response->setRowset($select->query()->fetchAll());

            if ($countable) {
                $response->totalCount = $plugin->getTotalCount();
            }

            $status = OSDN_Documents_Status::OK;

        } catch (Exception $e) {
            if (OSDN_DEBUG) {
                throw $e;
            }

            $status = OSDN_Documents_Status::DATABASE_ERROR;
        }

        $response->addStatus(new OSDN_Documents_Status($status));
        return $response;
    }

    public function fetchAllWithCategories(array $categoryIds = array(), array $params = array())
    {
        $response = new OSDN_Response();

        $categoriesTable = new OSDN_Documents_Table_Categories();
        $select = $this->_table
            ->getAdapter()->select()
            ->from(
                array('dt' => $this->_table->getTableName())
            )
            ->joinLeft(array('dc' => $categoriesTable->getTableName()),
                "dc.id = dt.documentCategoryId",
                array(
                    'category_name'     => 'name',
                    'category_order'    => 'position'
                )
            );

        if (OSDN_EntityTypes::STUDENT_COURSE == $this->_entityType) {
            $select->joinLeft(array('c' => $categoriesTable->getPrefix() . 'courses'),
                "c.id = dt.itemId",
                array()
            );

            if (!empty($params['course_id'])) {
                $select->where('itemId = ?', $params['course_id']);
            }
            $select->where('c.active = ?', 1);
        }

        $select->where('dt.entityType = ?', $this->_entityType);

        if (!empty($categoryIds)) {
            $select->where('dt.documentCategoryId IN(?)', $categoryIds);
        }

        $select->order('dc.order ASC');

        $plugin = new OSDN_Db_Plugin_Select($this->_table, $select, array('name' => 'dt.name'));
        $plugin->parse($params);
        $plugin->setSqlCalcFoundRows(true);

        $status = null;
        try {
            $rowset = $select->query()->fetchAll();

            $response->setRowset($rowset);
            $response->total = $plugin->getTotalCountSql();

            $response->setSuccess(true);

        } catch (Exception $e) {

            if (OSDN_DEBUG) {
                throw $e;
            }

            $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::DATABASE_ERROR));
        }


        return $response;

    }

    /**
     * Return all documentTypes
     *
    * @param array $params
    *
     * @return OSDN_Db_Response_Abstract
     */
    public function fetchAllWithFilesWithResponse(array $params)
    {
        $categoriesTable = new \Document_Model_DbTable_DocumentCategory($this->getAdapter());

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('dt' => $this->_table->getTableName())
            )
            ->joinLeft(
                array('dc' => $categoriesTable->getTableName()),
                'dc.id = dt.documentCategoryId',
                array(
                    'categoryName' => 'name',
                    'categoryOrder' => 'position'
                )
            )
            ->where('dt.entityType = ?', $this->_entityType);

        if (!empty($params['documentCategoryId'])) {
            $select->where('dt.documentCategoryId = ?', $params['documentCategoryId'], Zend_Db::INT_TYPE);
        } elseif (isset($params['documentCategoryId']) && null === $params['documentCategoryId']) {
            $select->where('dt.documentCategoryId IS NULL');
        }

        if (!empty($params['itemId'])) {
            $select->where('dt.itemId = ?', $params['itemId'], Zend_Db::INT_TYPE);
        } else {
            $select->where('dt.itemId is NULL');
        }

        $select->order('dc.position ASC');
        $select->order('dt.position ASC');

        $this->_initDbFilter($select, $this->_table, array('name' => 'dt.name'))->parse($params);

        $response = $this->getDecorator('response')->decorate($select);

        $status = null;
        try {
            $self = $this;
            $response->setRowsetCallback(function($row) use($self) {
                $row['files'] = $self->getFiles($row['id']);
                return $row;
            });
        } catch (Exception $e) {
            throw new \OSDN_Exception($e->getMessage());
        }

        return $response;
    }

    /**
     * Return all documentTypes
     *
     * @param array $params
     *
     * @return OSDN_Response
     */
    public function fetchAllWithReplaceableWithResponse($documentTypeId, array $params)
    {

        $validate = new OSDN_Validate_Id();

        if (!$validate->isValid($documentTypeId)) {
            throw new \OSDN_Exception('Invalid input parameter.');
        }

        $categoriesTable = new \Document_Model_DbTable_DocumentCategory($this->getAdapter());
        $replaceableTable = new \Document_Model_DbTable_DocumentTypeReplaceable($this->getAdapter());
        $select = $this->_table->getAdapter()->select()
            ->from(array('dt' => $this->_table->getTableName()),array(
                '*',
                'replaceable' => new Zend_Db_Expr('(CASE WHEN dtr.id IS NOT NULL THEN 1 ELSE 0 END)')
            ))
            ->joinLeft(array('dc' => $categoriesTable->getTableName()),
                "dc.id = dt.documentCategoryId",
                array(
                    'categoryName' => 'name',
                    'categoryOrder' => 'position'
                )
            )
            ->joinLeft(array('dtr' => $replaceableTable->getTableName()),
                "dt.id = dtr.replaceableId AND dtr.documentTypeId='" . $documentTypeId . "'", array()
            )
            ->where("dt.entityType = ? ", $this->_entityType)
            ->where("dt.id != ? ", $documentTypeId);
        if (!empty($params['itemId'])) {
            $select->where("dt.itemId = ?", intval($params['itemId']));
        } else {
            $select->where("dt.itemId is NULL ");
        }
        $select->order('dc.position ASC');

        try {
            $this->_initDbFilter($select, $this->_table, array('name' => 'dt.name'))->parse($params);
            $response = $this->getDecorator('response')->decorate($select);
        } catch (Exception $e) {
            throw new \OSDN_Exception($e->getMessage());
        }
        return $response;
    }

    /**
     * Update replaceable documents
     *
     * @param int $documentTypeId
     * @param int $replaceableId
     * @param int $value
     *
     * @return OSDN_Response
     */
    public function updateReplaceable($documentTypeId, $replaceableId, $value)
    {
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($documentTypeId)
         || !$validate->isValid($replaceableId)
         || !in_array($value, array(0,1)) ) {
            throw new \OSDN_Exception('Invalid input parameter.');
        }
        try {
            $replaceableTable = new \Document_Model_DbTable_DocumentTypeReplaceable($this->getAdapter());
            if (1 == $value) {
                $result = $replaceableTable->insert(array(
                    'documentTypeId' => $documentTypeId,
                    'replaceableId'   => $replaceableId
                ));
            } else {
                $result = $replaceableTable->delete(array(
                    'documentTypeId = ?' => $documentTypeId,
                    'replaceableId = ?'   => $replaceableId
                ));
            }
        } catch (Exception $e) {
            throw new \OSDN_Exception($e->getMessage());
        }
        return true;
    }

    /**
     * Return list of IDs which can replace this document types.
     *
     * @param $id - document type ID
     * @return array
     */
    public function getReplaceableIdsForId($documentTypeId)
    {
        $replaceableTable = new \Document_Model_DbTable_DocumentTypeReplaceable();

        $select = $replaceableTable->getAdapter()->select();
        $select->from($replaceableTable->getTableName(), 'replaceableId');
        $select->where('documentTypeId = ?', $documentTypeId);

        $outputIds = array();
        $query = $select->query();
        while(null != ($row = $query->fetch())) {
            $outputIds[] = $row['replaceableId'];
        }

        return $outputIds;
    }

    /**
     * Return all files for document type
    *
    * @param int $documentTypeId
    * @param array $fields
    *
     * @return array
     */
    public function getFiles($documentTypeId)
    {
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($documentTypeId)) {
            throw new \OSDN_Exception('Input paremeter document type id is incorrect.');
        }

        $result = array();
        $select = $this->_tableFiles->getAdapter()->select();
        $select->from(array('dtf' => $this->_tableFiles->getTableName()));
        $select->where("dtf.documentTypeId = ?", $documentTypeId);

        try {
            $rowset = $select->query()->fetchAll();

            if ($rowset) {
                foreach ($rowset as $k => $v) {
                    if (null != ($fileStockRow = $this->_file->find($v['fileStockId']))) {
                        $result[] = $fileStockRow->toArray();
                    }
                }
            }

            return $result;
        } catch (Exception $e) {
            throw new \OSDN_Exception($e->getMessage());
        }
    }

    /**
     * Return document type by id
     *
     * @param int $id
     *
     */
    public function find($id, $throwExcetionOnMissing = false)
    {
        $documentTypeRow = $this->_table->fetchRow(array(
            'id = ?'            => $id,
            'entityType = ?'    => $this->_entityType
        ));

        if (true === $throwExcetionOnMissing && null === $documentTypeRow) {
            throw new \OSDN_Exception('Unable to find document type');
        }

        return $documentTypeRow;
    }

    /**
     * Insert new documentType
     *
     * @param array $data
    *
     * @return OSDN_Response
     */
    public function create(array $data)
    {
        $data['entityType'] = $this->_entityType;
        $data['position'] = 0;

        $f = $this->_validate($data);
        $data = $f->getData();

        if (empty($data['itemId'])) {
            $data['itemId'] = new \Zend_Db_Expr('NULL');
        }

        if (empty($data['documentCategoryId'])) {
            $data['documentCategoryId'] = new \Zend_Db_Expr('NULL');
        }

        $documentTypeRow = $this->_table->createRow($data);
        $documentTypeRow->save();

        return $documentTypeRow;
    }

    /**
     * Update document
     *
     * @param int $id
     * @param array $data
     */
    public function update($id, array $data)
    {
        $documentTypeRow = $this->find($id, true);

        $data['entityType'] = $this->_entityType;
        $data['id'] = $id;

        $f = $this->_validate($data, 'update');
        $data = $f->getData();

        if (empty($data['itemId'])) {
            $data['itemId'] = new \Zend_Db_Expr('NULL');
        }
        if (empty($data['documentCategoryId'])) {
            $data['documentCategoryId'] = new \Zend_Db_Expr('NULL');
        }

        try {
            $this->getAdapter()->beginTransaction();

            if (empty($data['required'])) {
                $replaceableTable = new \Document_Model_DbTable_DocumentTypeReplaceable($this->getAdapter());
                if (false === $replaceableTable->deleteQuote(array('documentTypeId = ?' => $id))) {
                    throw new \OSDN_Exception('Deleting replaceable types failed.');
                }
            }

            $documentTypeRow->setFromArray($data);
            $documentTypeRow->save();

            $this->getAdapter()->commit();

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();

            throw $e;
        }

        return $documentTypeRow;
    }

    /**
     * Delete Document type
     *
     * @param int $id
     */
    public function delete($id)
    {
        $select = $this->_tableFiles->getAdapter()->select()
            ->from(array('dtf' => $this->_tableFiles->getTableName()))
            ->where("dtf.documentTypeId = ?", $id);

        try {
            $rowset = $select->query()->fetchAll();
            try {
                $countDocs = $this->_docs->count(array(
                    'documentTypeId = ?' => $id
                ));
                if ($countDocs == 0) {
                    $affectedRows = $this->_table->deleteByPk($id);
                } elseif ($countDocs > 0) {
                    throw new \OSDN_Exception('Unable to delete document type. There are documents of this type!');
                } else {
                    throw new \OSDN_Exception('Unable to delete document type.');
                }
            } catch (Exception $e) {
                throw new \OSDN_Exception($e->getMessage());
            }
            if (false === $affectedRows) {
                throw new \OSDN_Exception('Unable to delete document type.');
            }

            if ($rowset) {
                foreach ($rowset as $k => $v) {
                    $res = $this->_file->delete($v['fileStockId']);
                    if (false === $res) {
                        throw new \OSDN_Exception('Unable to delete document type file.');
                    }
                }
            }
        } catch (Exception $e) {
            throw new \OSDN_Exception($e->getMessage());
        }

        return true;
    }

    /**
     * Return total count of documents added for current entity type
     *
     * @param object $params [optional]
     *
     * @return OSDN_Response
     */
    public function getCountOfIncludedDocuments($params = array())
    {
        $response = new OSDN_Response();

        $documentTable = new OSDN_Documents_Table_Documents();

        $select = $this->_table->getAdapter()->select();
        $select->from(array('dt' => $this->_table->getTableName()),
            array('c' => new Zend_Db_Expr('COUNT(dt.id)')));
        $select->join(array('d' => $documentTable->getTableName()),
            'dt.id = d.documentTypeId', array());

        $select->where("dt.entityType = ? ", $this->_entityType);

        if ($params['entity_id']) {
            $select->where("d.entity_id = ? ", $params['entity_id']);
        }
        if (!empty($params['where'])) {
            foreach($params['where'] as $k => $v) {
                $select->where("dt.$k = ? ", $v);
            }
        }

        $select->group('dt.entityType');

        $plugin = new OSDN_Db_Plugin_Select($this->_table, $select);
        $plugin->parse($params);

        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $status = null;
        try {
            $total = $select->query()->fetchColumn(0);
            $response->count = $total ? $total : 0;
            $status = OSDN_Documents_Status::OK;
        } catch (Exception $e) {
            if (OSDN_DEBUG) {
                throw $e;
            }
            $status = OSDN_Documents_Status::DATABASE_ERROR;
        }
        $response->addStatus(new OSDN_Documents_Status($status));
        return $response;
    }

    /**
     * Fetch documents by documents types abbreviation
     *
     * @todo move to document type
     *
     * @param string $abbreviation      The document abbreviation
     * @return OSDN_Response
     */
    public function fetchAllByAbbreviation($abbreviation, $presence = null)
    {
        $response = new OSDN_Response();

        $adapter = $this->_table->getAdapter();
        $prefix = $this->_table->getPrefix();
        $select = $adapter->select()
            ->from(array('dt' => $prefix . 'documents_types'))
            ->join(
                array('d' => $prefix . 'documents'),
                'dt.id = d.documentTypeId'
            )
            ->where('dt.abbreviation = ?', $abbreviation);

         if (!is_null($presence)) {
             $select->where('d.presence = ?', (int) $presence);
         }

         try {
             $rowset = $select->query()->fetchAll();
             $response->setRowset($rowset);
             $status = OSDN_Documents_Status::OK;
         } catch (Exception $e) {
             if (OSDN_DEBUG) {
                 throw $e;
             }
             $status = OSDN_Documents_Status::DATABASE_ERROR;
         }

         $response->addStatus(new OSDN_Documents_Status($status));
         return $response;
    }

    /**
     * Fetch documents by documents types abbreviation and entity
     *
     * @todo move to document type
     *
     * @param string $abbreviation      The document abbreviation
     * @return OSDN_Response
     */
    public function fetchAllByAbbreviationEntity($abbreviation, $entityId, $entityType = null, $presence = null)
    {
        $response = new OSDN_Response();

        if (empty($entityType)) {
            $entityType = $this->_entityType;
        }

        $adapter = $this->_table->getAdapter();
        $prefix = $this->_table->getPrefix();
        $select = $adapter->select()
            ->from(array('dt' => $prefix . 'documents_types'))
            ->join(
                array('d' => $prefix . 'documents'),
                'dt.id = d.documentTypeId'
            )
            ->where('dt.abbreviation = ?', $abbreviation)
            ->where('d.entityType = ?', $entityType)
            ->where('d.entity_id = ?', $entityId);

        if (!is_null($presence)) {
             $select->where('d.presence = ?', (int) $presence);
        }

        try {
            $rowset = $select->query()->fetchAll();
            $response->setRowset($rowset);
            $status = OSDN_Documents_Status::OK;
        } catch (Exception $e) {
            if (OSDN_DEBUG) {
                throw $e;
            }
            $status = OSDN_Documents_Status::DATABASE_ERROR;
        }

        $response->addStatus(new OSDN_Documents_Status($status));
        return $response;
    }

    /**
     * Fetch documents by documents types abbreviation and entity
     *
     * @todo move to document type
     *
     * @param string $abbreviation      The document abbreviation
     * @return OSDN_Response
     */
    public function fetchCountByAbbreviationEntity($abbreviation, $entityId, $entityType = null, $presence = null)
    {
        $response = new OSDN_Response();

        if (empty($entityType)) {
            $entityType = $this->_entityType;
        }

        $adapter = $this->_table->getAdapter();
        $prefix = $this->_table->getPrefix();
        $select = $adapter->select()
            ->from(array('dt' => $prefix . 'documents_types'), new Zend_Db_Expr('COUNT(*)'))
            ->join(
                array('d' => $prefix . 'documents'),
                'dt.id = d.documentTypeId',
                array()
            )
            ->where('dt.abbreviation = ?', $abbreviation)
            ->where('d.entityType = ?', $entityType)
            ->where('d.entity_id = ?', $entityId);

        if (!is_null($presence)) {
            $select->where('d.presence = ?', (int) $presence);
        }

         try {
             $count = $select->query()->fetchColumn();
             $response->count = $count;
             $status = OSDN_Documents_Status::OK;
         } catch (Exception $e) {
             if (OSDN_DEBUG) {
                 throw $e;
             }
             $status = OSDN_Documents_Status::DATABASE_ERROR;
         }

         $response->addStatus(new OSDN_Documents_Status($status));
         return $response;
    }

    /**
     * Fetch documents by documents types id and entity id
     *
     * @todo move to document type
     *
     * @param string $abbreviation      The document abbreviation
     * @return OSDN_Response
     */
    public function fetchAllByIdAndEntityId($documentTypeId, $entityId, $entityType = null, $presence = null)
    {
        $response = new OSDN_Response();

        if (empty($entityType)) {
            $entityType = $this->_entityType;
        }

        $adapter = $this->_table->getAdapter();
        $prefix = $this->_table->getPrefix();
        $select = $adapter->select()
            ->from(array('dt' => $prefix . 'documents_types'))
            ->join(
                array('d' => $prefix . 'documents'),
                'dt.id = d.documentTypeId'
            )
            ->where('dt.id = ?', $documentTypeId)
            ->where('d.entityType = ?', $entityType)
            ->where('d.entity_id = ?', $entityId);

        if (!is_null($presence)) {
            $select->where('d.presence = ?', (int) $presence);
        }

        try {
            $response->setRowset($select->query()->fetchAll());
            $status = OSDN_Documents_Status::OK;
        } catch (Exception $e) {
            if (OSDN_DEBUG) {
                throw $e;
            }
            $status = OSDN_Documents_Status::DATABASE_ERROR;
        }

        $response->addStatus(new OSDN_Documents_Status($status));
        return $response;
    }

    /**
     * Fetch documents by documents types id and entity id
     *
     * @todo move to document type
     *
     * @param string $abbreviation      The document abbreviation
     * @return OSDN_Response
     */
    public function fetchCountByIdAndEntityId($documentTypeId, $entityId, $entityType = null, $presence = null)
    {
        $response = new OSDN_Response();

        if (empty($entityType)) {
            $entityType = $this->_entityType;
        }

        $adapter = $this->_table->getAdapter();
        $prefix = $this->_table->getPrefix();
        $select = $adapter->select()
            ->from(array('dt' => $prefix . 'documents_types'), new Zend_Db_Expr('COUNT(*)'))
            ->join(
                array('d' => $prefix . 'documents'),
                'dt.id = d.documentTypeId',
                array()
            )
            ->where('dt.id = ?', $documentTypeId)
            ->where('d.entityType = ?', $entityType)
            ->where('d.entity_id = ?', $entityId);

        if (!is_null($presence)) {
            $select->where('d.presence = ?', (int) $presence);
        }

        try {
            $response->count = $select->query()->fetchColumn();
            $status = OSDN_Documents_Status::OK;
        } catch (Exception $e) {
            if (OSDN_DEBUG) {
                throw $e;
            }
            $status = OSDN_Documents_Status::DATABASE_ERROR;
        }

        $response->addStatus(new OSDN_Documents_Status($status));
        return $response;
    }

    /**
     * Find or inser new document type
     *
     * @param array $data
     *
     * @return OSDN_Response
     */
    public function findOrInsertNew(array $data)
    {
        $response = new OSDN_Response();

        if (empty($data['abbreviation'])) {
            $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::INPUT_PARAMS_INCORRECT, 'abbreviation'));
            return $response;
        }

        if (empty($data['itemId'])) {
            $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::INPUT_PARAMS_INCORRECT, 'itemId'));
            return $response;
        }

        $select = $this->_table->getAdapter()->select()
            ->from($this->_table->getTableName(), array('id'))
            ->where('entityType = ?', $this->_entityType)
            ->where('documentCategoryId is NULL')
            ->where('itemId = ?', $data['itemId'])
            ->where('abbreviation = ?', $data['abbreviation']);

        try {
            $query = $select->query();
        } catch (Exception $e) {
            $response->addStatus(new OSDN_Documents_Status(OSDN_Documents_Status::DATABASE_ERROR));
            return $response;
        }
        $row = $query->fetch();
        if ($row) {
            $response = $this->update($row['id'], $data);
            $response->docTypeId = $row['id'];
            return $response;
        } else {
            return $this->insert($data);
        }
    }
}