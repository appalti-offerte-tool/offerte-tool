<?php

class Document_Service_DocumentTypeFileStock extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \Document_Model_DbTable_DocumentType
     */
    protected $_table;

    /**
     * @var \FileStock_Service_FileStock
     */
    protected $_fileStock;

    public function _init()
    {
        $this->_table = new \Document_Model_DbTable_DocumentType($this->getAdapter());
        // FIXME config added for testing/ Must be $this->_fileStock = new FileStock_Service_FileStock();
        $this->_fileStock = new \FileStock_Service_FileStock(array(
            'baseFilePath' => APPLICATION_PATH . '/data/document-files'
        ));

    }

    /**
     * Retrieve the filestock rows by document
     *
     * @param int $documentId
     * @return Zend_Db_Table_Rowset_Abstract
     * @throws OSDN_Exception
     */
    public function fetchAllFileStockByDocumentTypeIdWithResponse($documentTypeId)
    {
        $documentTypeRow = $this->_table->findOne($documentTypeId);
        if (empty($documentTypeRow)) {
            throw new \OSDN_Exception('Unable to find document #' . $documentTypeId);
        }

        $fileStockTable = new \FileStock_Model_DbTable_FileStock($this->getAdapter());

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('df'    => 'documentTypeFileStockRel'),
                array()
            )
            ->join(
                array('f'    => 'fileStock'),
                'f.id = df.fileStockId',
                $fileStockTable->getAllowedColumns()
            )
            ->where('df.documentTypeId = ?', $documentTypeRow->getId());

        return $this->getDecorator('response')->decorate($select);
    }

    /**
     * Return all document type files
     *
     * @return OSDN_Response
     */
    public function fetchAllFileStockByEntityTypeWithResponse($entityType, array $params = array())
    {
        $documentTypesFilesTable = new Document_Model_DbTable_DocumentTypeFileStockRel($this->getAdapter());

        $select = $this->getAdapter()->select()
            ->from(
                array('dt' => 'documentType'),
                array(
                    'documentTypeId' => 'id',
                    'name',
                    'required'
                )
            )
            ->join(
                array('dtf' => 'documentTypeFileStockRel'),
                "dtf.documentTypeId = dt.id ",
                array('fileStockId')
            );

        if (isset($params['allowedDocumentTypes'])) {
            if (empty($params['allowedDocumentTypes'])) {
                $select->where('dt.id = ?', -1);
            } else {
                $select->where('dt.id IN (?)', $params['allowedDocumentTypes']);
            }
        }

        $select->where('dt.entityType = ?', $entityType);

        if (empty($params['itemId'])) {
            $select->where('dt.itemId is NULL');
        } else {
            $select->where('dt.itemId = ?', $params['itemId'], Zend_Db::INT_TYPE);
        }

        $this->_initDbFilter($select, $this->_table, array(
            'dt.name' => 'name'
        ))->parse($params);

        $response = $this->getDecorator('response')->decorate($select);

        $fileStock = $this->_fileStock;
        $response->setRowsetCallback(function($row) use($fileStock) {

            if (!isset($row['fileStockId'])) {
                return false;
            }

            if (null == ($fileStockRow = $fileStock->find($row['fileStockId']))) {
                throw new \OSDN_Exception('Unable fetch file.');
            }

            $data = $fileStockRow->toArray();
            $row['fileName'] = $data['name'];
            $row['description'] = $data['description'];
            $row['type'] = $data['type'];
            $row['size'] = $data['size'];

            return $row;
        });

        return $response;
    }

    public function find($documentTypeId, $fileStockId)
    {
        /**
         * @todo
         * Implement verification on assignment document to filestock
         */
        return $this->_fileStock->find($fileStockId);
    }

    public function create(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {
        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }

        $documentTypeRow = $this->_table->findOne($data['documentTypeId']);

        $fileStockRow = $this->_fileStock->create($transfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow) use ($documentTypeRow) {
            $documentTypeRow->getTable()->getAdapter()->insert('documentTypeFileStockRel', array(
                'fileStockId'        => $fileStockRow->getId(),
                'documentTypeId'=> $documentTypeRow->getId()
            ));
        });

        return $fileStockRow;
    }

    public function update(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {
        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }

        $this->_fileStock->update($transfer, $data);
    }

    public function delete($documentTypeId, $fileStockId)
    {
        $documentTypeRow = $this->_table->findOne($documentTypeId);
        $result = $this->_fileStock->delete($fileStockId, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow) use ($documentTypeRow) {
            $documentFileStockRelation = new \Zend_Db_Table('documentTypeFileStockRel');
            $documentFileStockRelation->delete(array(
                'documentId = ?'    => $documentTypeRow->getId(),
                'fileStockId = ?'        => $fileStockRow->getId()
            ));
        });

        return (boolean) $result;
    }
}