<?php

class Document_Service_DocumentComment extends \Comment_Service_Comment
{
    protected $_entityType;

    public function __construct($entityType = null)
    {
        $this->_entityType = $entityType;

        parent::__construct();
    }

    public function deleteByDocument(Document_Model_DbTable_DocumentRow $documentRow)
    {
        return $this->deleteByEntity(\Document_Model_DbTable_Document::ENTITY, $documentRow->getId());
    }

    public function _insert(array $data, Closure $onBeforeInsertCallbackFn = null)
    {
        $entityType = $this->_entityType;
        return parent::_insert($data, function($preparedData) use ($entityType, $data) {

            if (!is_array($data)) {
                throw new \OSDN_Exception('Invalid input credentials');
            }

            $documentService = new \Document_Service_Document($entityType);
            $validate = new \OSDN_Validate_Id();

            if (array_key_exists('entityId', $data)) {
                $entityId = $data['entityId'];
                if ($validate->isValid($entityId)) {
                    $documentRow = $documentService->find($entityId, false);
                    if (null === $documentRow) {
                        throw new \OSDN_Exception('Unable to find document');
                    }
                    return $documentRow;
                }

            }

            /**
             * When document are missing but we want to
             * add new comment to it
             * we create remporary document with document type $entityType
             * and entityId $data['entityEntityId'] because the entityId will be a created documentId
             * and on the future we know that only comments exists
             * for this document but no document itself
             */

            if (empty($entityType)
                    || !array_key_exists('documentTypeId', $data)
                    || !$validate->isValid($data['documentTypeId'])
                    || !array_key_exists('entityEntityId', $data)
                    || !$validate->isValid($data['entityEntityId'])) {
                    throw new \OSDN_Exception('Invalid input credentials');
            }

            $documentRow = $documentService->createEmptyDocument(array(
                'documentTypeId'  => $data['documentTypeId'],
                'entityId'        => $data['entityEntityId'],
                'entityType'      => $entityType,
                'presence'        => 0
            ));

            return $documentRow;
        });
    }
}