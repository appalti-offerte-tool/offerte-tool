<?php

class Document_Service_DocumentSource extends OSDN_Application_Service_Dbable
{
    /**
     * @var Document_Model_DbTable_DocumentSource
     */
    protected $_tableDocumentSource = null;

    /**
     * construct function of the class
     *
     */
    public function __construct()
    {
        $this->_tableDocumentSource = new \Document_Model_DbTable_DocumentSource($this->getAdapter());
    }

    /**
     * Fetch all document sources
     *
     * @param array $params
     * @return array
     */
    public function fetchAll(array $params = array())
    {
        $select = $this->_tableDocumentSource->select()->order('isDefault DESC');

        $this->_initDbFilter($select, $this->_tableDocumentSource)->parse($params);

        return $select->query()->fetchAll();
    }

    /**
     * Retrieve the document source by id
     *
     * @param int $id
     *
     * @return \Document_Model_DbTable_DocumentSourceRow
     */
    public function find($id)
    {
        $validate = new OSDN_Validate_Id();
        if (!$validate->isValid($id)) {
            throw new \OSDN_Exception('Input parameter failure');
        }

        $documentSourceRow = $this->_tableDocumentSource->findOne($id);

        if (null === $documentSourceRow) {
            throw new \OSDN_Exception('Unable to find document source');
        }

        return $documentSourceRow;
    }

    /**
     * Retrieve the default document source
     *
     * @return \Document_Model_DbTable_DocumentSourceRow
     */
    public function getDefault()
    {

        $documentSourceRow = $this->_tableDocumentSource->fetchRow(array(
            'isDefault = ?'  => 1
        ));

        if (empty($documentSourceRow)) {
            return null;
        }

        return $documentSourceRow;
    }

    /**
     * Retrieve the presence document source (like ONLINE)
     *
     * @return \Document_Model_DbTable_DocumentSourceRow
     */
    public function getPresence()
    {

        $documentSourceRow = $this->_tableDocumentSource->fetchRow(array(
            'isPresence = ?'  => 0
        ));

        if (empty($documentSourceRow)) {
            return null;
        }

        return $documentSourceRow;
    }

}