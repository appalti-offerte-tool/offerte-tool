<?php

class Document_DocumentTypeFileStockController extends Zend_Controller_Action implements \FileStock_Instance_EntityFileStockControllerInterface
{
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        // FIXME changed for testing
        return 'document:document-type';
//        return 'client:document';
    }

    public function init()
    {
        $this->_service = new \Document_Service_DocumentTypeFileStock($this->getEntityType());

        $this->_helper->contextSwitch()
           ->addActionContext('fetch-all', 'json')
           ->addActionContext('fetch', 'json')
           ->addActionContext('create', 'json')
           ->addActionContext('update', 'json')
           ->addActionContext('delete', 'json')
           ->initContext();
    }

    public function getEntityType()
    {
        return \Document_Model_DbTable_Document::ENTITY;
    }

    public function downloadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $fileStockRow = $this->_service->find($this->_getParam('documentId'), $this->_getParam('id'));

        // FIXME config added for testing/ Must be $fileStockService = new FileStock_Service_FileStock();
        $fileStockService = new FileStock_Service_FileStock(array(
            'baseFilePath' => APPLICATION_PATH . '/data/document-files'
        ));
        $fileStockService->download($fileStockRow, $this->getResponse(), true);
    }

    public function fetchAllAction()
    {
        try {
            $response = $this->_service->fetchAllFileStockByDocumentTypeIdWithResponse($this->_getParam('documentTypeId'));
            $this->view->rowset = $response->getRowset();
            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAction()
    {
        $fileStockRow = $this->_service->find($this->_getParam('documentId'), $this->_getParam('fileStockId'));

        $this->view->data = $fileStockRow->toArray();
        $this->view->success = true;
    }

    public function createAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');

        try {

            $this->_service->create($this->_getAllParams());

            $this->view->success = true;
            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);

        } catch(OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');

        try {

            $this->_service->update($this->_getAllParams());

            $this->view->success = true;
            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);

        } catch(OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->_service->delete($this->_getParam('documentTypeId'), $this->_getParam('fileStockId'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }
}