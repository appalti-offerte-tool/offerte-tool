<?php

final class Document_DocumentTypeReplaceableController extends \Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    protected $_service;

    public function init()
    {
        $this->_service = new Document_Service_DocumentTypeReplaceable($this->_getParam('etype'));

        $this->_helper->contextSwitch()
           ->addActionContext('fetch-all', 'json')
           ->addActionContext('get-replaceable-list', 'json')
           ->addActionContext('update-replaceable', 'json')
           ->addActionContext('files-list', 'json')
           ->addActionContext('load-file', 'json')
           ->addActionContext('update', 'json')
           ->addActionContext('create', 'json')
           ->addActionContext('update-file', 'json')
           ->addActionContext('delete', 'json')
           ->addActionContext('move-up', 'json')
           ->addActionContext('move-down', 'json')
           ->addActionContext('create-category', 'json')
           ->addActionContext('rename-category', 'json')
           ->addActionContext('delete-category', 'json')
           ->addActionContext('change-category-order', 'json')
           ->initContext();

        parent::init();
    }

    public function getResourceId()
    {
        return 'document:document-type';
    }

    public function indexAction()
    {}

    public function fetchAllAction()
    {
        try {
            $response = $this->_service->fetchAllWithResponse($this->_getParam('documentTypeId'), $this->_getAllParams());
            $this->view->assign($response->toArray());
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function createAction()
    {
        try {
            $result = $this->_service->create(
                $this->_getParam('documentTypeId'),
                $this->_getParam('replaceableId')
            );

            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }


    public function deleteAction()
    {
        try {
            $result = $this->_service->delete(
                $this->_getParam('documentTypeId'),
                $this->_getParam('replaceableId')
            );

            $this->view->success = $result;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }
}