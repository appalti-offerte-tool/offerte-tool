<?php

final class Document_DocumentCategoryController extends \Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    protected $_service;

    public function init()
    {
        $this->_helper->layout->setLayout('administration');

        $this->_service = new \Document_Service_DocumentCategory($this->_getParam('etype'));

        $this->_helper->ajaxContext()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('rename', 'json')
            ->addActionContext('delete', 'json')
            ->initContext();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'document:category';
    }

    public function indexAction()
    {}

    public function fetchAllAction()
    {
        $node = $this->_getParam('node');
        if ($node > 0) {
            return;
        }

        $response = $this->_service->fetchAll();
        if (!is_array($response)) {
            $this->view->success = false;
            $this->_helper->information('Not found categories');
            return;
        }

        foreach ($response as & $row) {
            $row['text'] = $row['name'];
            $row['children'] = array();
            $row['expanded'] = true;
        }
        $this->view->assign($response);
    }

    public function createAction()
    {
        try {
            $documentCategoryRow = $this->_service->create(array(
                'name'     => $this->_getParam('name')
            ));

            $this->view->id = $documentCategoryRow->getId();
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function renameAction()
    {
        try {
            $response = $this->_service->update($this->_getParam('node'), array(
                'name' => $this->_getParam('text')
            ));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $response = $this->_service->delete($this->_getParam('node'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }
}