<?php

$commentModuleDirectory = Zend_Controller_Front::getInstance()->getControllerDirectory('comment');

require_once $commentModuleDirectory . '/IndexController.php';

class Document_CommentController extends Comment_IndexController implements \Zend_Acl_Resource_Interface
{
    protected $_entityTypeId = Document_Model_DbTable_Document::ENTITY;

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return $this->getRequest()->getModuleName();
    }
}