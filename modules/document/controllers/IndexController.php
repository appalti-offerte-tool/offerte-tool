<?php
// FIXME created for testing

class Document_IndexController extends \Document_Instance_DocumentControllerAbstract implements Zend_Acl_Resource_Interface
{
    protected $_service;

    public function init()
    {

        $this->_entityType = 'document';

        $this->_service = new Document_Service_Document();

        parent::init();

    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'document';
    }

    public function indexAction()
    {
        $this->_helper->layout->setLayout('administration');
    }

}