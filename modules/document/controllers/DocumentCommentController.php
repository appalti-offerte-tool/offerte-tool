<?php
// FIXME created for testing
require_once ('./modules/comment/controllers/IndexController.php');

class Document_DocumentCommentController extends Comment_IndexController implements Zend_Acl_Resource_Interface
{
    protected $_service;

    public function init()
    {

        $this->_entityTypeId = 'document';

        parent::init();

        $this->_service = new \Document_Service_DocumentComment($this->_entityTypeId);

    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'document';
    }


}