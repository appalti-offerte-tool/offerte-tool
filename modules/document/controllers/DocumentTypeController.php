<?php

final class Document_DocumentTypeController extends \Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    protected $_service;

    public function init()
    {
        $this->_service = new Document_Service_DocumentType($this->_getParam('etype'));

        $this->_helper->layout->setLayout('administration');

        /**
         * @todo hardcoded for migration. Should be inherited
         */
        $this->_entityTypeId = 'document';

        $this->_helper->contextSwitch()
           ->addActionContext('fetch-all', 'json')
           ->addActionContext('get-replaceable-list', 'json')
           ->addActionContext('update-replaceable', 'json')
           ->addActionContext('files-list', 'json')
           ->addActionContext('load-file', 'json')
           ->addActionContext('update', 'json')
           ->addActionContext('form', 'json')
           ->addActionContext('create', 'json')
           ->addActionContext('update-file', 'json')
           ->addActionContext('delete', 'json')
           ->addActionContext('move-up', 'json')
           ->addActionContext('move-down', 'json')
           ->addActionContext('create-category', 'json')
           ->addActionContext('rename-category', 'json')
           ->addActionContext('delete-category', 'json')
           ->addActionContext('change-category-order', 'json')
           ->initContext();

        parent::init();
    }

    public function getResourceId()
    {
        return 'document:document-type';
    }

    public function indexAction()
    {}

    public function fetchAllAction()
    {
        try {
            $response = $this->_service->fetchAllWithFilesWithResponse($this->_getAllParams());
            $this->view->assign($response->toArray());
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function formAction()
    {
//        $this->_helper->layout->disableLayout();

        try {
            $documentTypeId = $this->_getParam('id', null);
            if (null != $documentTypeId) {
                $documentType = $this->_service->find($documentTypeId);
                $this->view->data = $documentType->toArray();
            }
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }


    }

    public function createAction()
    {
        try {
            $documentTypeRow = $this->_service->create($this->_getAllParams());
            $this->view->documentTypeId = $documentTypeRow->getId();
            $this->_helper->information('Document type has been created successfully', true, E_USER_NOTICE);
            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        try {
            $documentTypeRow = $this->_service->update($this->_getParam('id'), $this->_getAllParams());
            $this->view->documentTypeId = $documentTypeRow->getId();
            $this->_helper->information('Document type has been updated successfully', true, E_USER_NOTICE);
            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function moveUpAction()
    {
        try {
            $result = $this->_service->moveUp(
                $this->_getParam('id'),
                $this->_getParam('itemId', null),
                $this->_getParam('documentCategoryId', null)
            );

            $this->view->success = $result;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function moveDownAction()
    {
        try {
            $result = $this->_service->moveDown(
                $this->_getParam('id'),
                $this->_getParam('itemId', null),
                $this->_getParam('documentCategoryId', null)
            );

            $this->view->success = $result;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function changeCategoryOrderAction()
    {
        try {
            $categories = new \Document_Service_DocumentCategory($this->_entityTypeId);
            $response = $categories->changeOrder($this->_getParam('categoryId'), $this->_getParam('position'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function downloadAction()
    {
        $this->disableRender(true);

        $response = $this->_service->getFileWithContent($this->_getParam('id'));

        if ($response->isError()) {
            return;
        }
        $data = $response->getRow();

        header('Content-disposition: attachment; filename="' . $data['name'] . '"');

        header('Content-Type: ' . $data['type']);

        header('Content-Transfer-Encoding: binary');

        header('Content-Length: '. $data['size']);

        header('Pragma: no-cache');

        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

        header('Expires: 0');

        echo $data['filecontent'];

        exit;
    }

    public function loadAction()
    {
        try {
            $row = $this->_service->get($this->_getParam('id'));
            $this->view->data = $row->getRowset();
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }




    public function deleteAction()
    {
        try {
            $response = $this->_service->delete($this->_getParam('id'));
            $this->view->success = true;
            $this->_helper->information('Document type has been deleted successfully', true, E_USER_NOTICE);
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }


    public function filesListAction()
    {
        try {
            $data = $this->_getAllParams();
            $response = $this->_service->getFiles($data['documentTypeId']);
            $this->view->data = $response;
            $this->view->total = isset($response->total)? $response->total : 0;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function loadFileAction()
    {
        try {
            $data = $this->_getAllParams();
            $response = $this->_service->getFileInfo($data['fileStockId']);
            $this->view->data = $response;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateFileAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');

        $data = $this->_getAllParams();
        $data['file'] = array();

        try {
            if (!empty($_FILES['RemoteFile']) && $_FILES['RemoteFile']['name']) {
                $data['file'] = $_FILES['RemoteFile'];
            }
            $data['file']['description'] = $data['description'];
            if (!isset($data['fileStockId'])) {
                $response = $this->_service->insertFile($data);
            } else {
                $response = $this->_service->updateFile($data);
            }
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteFileAction()
    {
        try {
            $data = $this->_getAllParams();
            $response = $this->_service->deleteFile($data['fileStockId']);
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateReplaceableAction()
    {
        try {
            $response = $this->_service->updateReplaceable(
                $this->_getParam('documentTypeId'),
                $this->_getParam('replaceableId'),
                $this->_getParam('value')
            );
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

}