<?php

final class Document_DocumentSourceController extends \Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    protected $_service;

    public function init()
    {
        $this->_service = new \Document_Service_DocumentSource();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'document';
    }

    public function comboBoxAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');

        $this->view->rowset = $this->_service->fetchAll($this->_getAllParams());
    }
}