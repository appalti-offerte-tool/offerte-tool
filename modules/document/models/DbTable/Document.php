<?php

class Document_Model_DbTable_Document extends OSDN_Db_Table_Abstract
{
    const ENTITY = 'DOCUMENT';

    protected $_primary = 'id';

    protected $_name = 'document';

    protected $_rowClass = '\\Document_Model_DbTable_DocumentRow';

    protected $_nullableFields = array(
        'expireDate', 'documentTypeId', 'itemId'
    );

    protected $_referenceMap    = array(
        'DocumentSource'    => array(
            'columns'       => 'documentSourceId',
            'refTableClass' => 'Document_Model_DbTable_DocumentSource',
            'refColumns'    => 'id'
        )
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        $dt = new \DateTime();
        $nowDateTime = $dt->format('Y-m-d H:i:s');

        $data['createDate'] = $nowDateTime;
        $data['createAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        $requested = false;
        if (isset($data['presence']) && $data['presence'] == 0 && empty($data['requestDate'])) {
            $data['requestDate'] = $nowDateTime;
            $requested = true;
        } elseif (
            !empty($data['requestDate'])
            &&  0 != strcasecmp((string) $data['requestDate'], 'null')
        ) {
            $requested = true;
        }

        $result = parent::insert($data);

        if (true === $requested) {
//            $notification = new OSDN_Documents_Request_Notification();
//            $notification->notificate($data);
        }

        return $result;
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::updateByPk()
     */
    public function updateByPk(array $data, $id)
    {
        $row = $this->findOne($id);
        if ($row) {
            $row = $row->toArray();
        }

        $requested = false;
        if ($row['presence'] == 0) {

            if (empty($data['requestDate'])) {

                if (empty($data['receiveDate'])) {
                    $dt = new \DateTime();
                    $nowDateTime = $dt->format('Y-m-d H:i:s');
                    $data['receiveDate'] = $nowDateTime;
                }

                $data['presence'] = '1';
            } elseif (0 != strcasecmp((string) $data['requestDate'], 'null')) {
                $requested = true;
            }
        }

        $result = parent::updateByPk($data, $id);

        if (true === $requested) {
//            $notification = new OSDN_Documents_Request_Notification();
//            $notification->notificate($data);
        }

        return $result;
    }

    public function reasign($documentId, $entityTypeId, $entityId, $newEntityTypeId, $newEntityId, $itemId = null)
    {
        $data = array(
            'entityTypeId'        => $newEntityTypeId,
            'entityId'            => $newEntityId,
        );

        $data['itemId'] = null !== $itemId ? $itemId : new Zend_Db_Expr('NULL');

        $result = $this->updateQuote($data, array(
            'id = ?'            => $documentId,
            'entityId = ?'     => $entityId,
            'entityTypeId = ?'    => $entityTypeId
        ));

        return $result;
    }
}