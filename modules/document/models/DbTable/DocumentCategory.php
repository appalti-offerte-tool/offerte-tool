<?php

class Document_Model_DbTable_DocumentCategory extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'documentCategory';

    protected $_rowClass = '\\Document_Model_DbTable_DocumentCategoryRow';
}