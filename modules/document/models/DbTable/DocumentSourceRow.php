<?php

final class Document_Model_DbTable_DocumentSourceRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    public function getId()
    {
        return $this->id;
    }

    public function isPresence()
    {
        return $this->isPresence;
    }
}