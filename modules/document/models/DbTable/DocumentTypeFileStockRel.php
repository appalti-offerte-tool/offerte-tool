<?php

class Document_Model_DbTable_DocumentTypeFileStockRel extends OSDN_Db_Table_Abstract
{
    /**
     * Table name
     *
     * @var string
     */
    protected $_name = 'documentTypeFileStockRel';
}