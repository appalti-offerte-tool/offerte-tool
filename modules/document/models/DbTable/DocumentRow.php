<?php

class Document_Model_DbTable_DocumentRow extends Zend_Db_Table_Row_Abstract implements OSDN_Application_Model_Interface
{
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Db_Table_Row_Abstract::toArray()
     */
    public function toArray()
    {
        $data = parent::toArray();

        foreach(array(
            'createDate', 'modifyDate',
            'expireDate', 'receiveDate', 'requestDate'
        ) as $field) {
            if (!empty($data[$field])) {
                $dateTime = new \DateTime($data[$field]);
                $data[$field] = $dateTime->format('Y-m-d H:i:s');
            }
        }

        return $data;
    }

    public function getDocumentStorageSource()
    {
        return $this->findParentRow('Document_Model_DbTable_DocumentSource', 'DocumentSource');
    }
}