<?php

class Document_Model_DbTable_DocumentFileStockRel extends OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'documentFileStockRel';
}