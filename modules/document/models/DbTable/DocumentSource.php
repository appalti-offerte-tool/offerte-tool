<?php
class Document_Model_DbTable_DocumentSource extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'documentSource';

    protected $_rowClass = '\\Document_Model_DbTable_DocumentSourceRow';
}