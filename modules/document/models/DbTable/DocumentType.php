<?php

class Document_Model_DbTable_DocumentType extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'documentType';

    protected $_rowClass = '\\Document_Model_DbTable_DocumentTypeRow';
}