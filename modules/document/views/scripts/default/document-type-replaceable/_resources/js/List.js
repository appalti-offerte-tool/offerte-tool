Ext.define('Module.Document.DocumentTypeReplaceable.List', {

    extend: 'Ext.ux.grid.GridPanel',

    alias: 'module.document.document-type-replaceable.list',

    entityType: null,

    documentTypeId: null,

    itemId: null,

    cls: 'm-document',

    window: null,

    initComponent: function() {

        Ext.define('Module.Document.DocumentTypeReplaceable.Model.List', {
            extend: 'Ext.data.Model',
            fields: [
                {name: 'id'},
                {name: 'name'},
                {name: 'abbreviation'},
                {name: 'required'},
                {name: 'replaceable'},
                {name: 'files'},
                {name: 'categoryId'},
                {name: 'categoryName'},
                {name: 'categoryOrder'}
            ]
        });

        this.store = new Ext.data.Store({
            model: 'Module.Document.DocumentTypeReplaceable.Model.List',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: link('document', 'document-type-replaceable', 'fetch-all', {format: 'json', etype: this.entityType}),
                extraParams: {
                    documentTypeId: this.documentTypeId,
                    itemId: this.itemId
                },
                reader: {
                    type: 'json',
                    root: 'rowset',
                    totalProperty: 'total'
                },
                simpleSortMode: true
            },
            sorters: {
                field: 'name',
                direction: 'ASC'
            },
            remoteSort: false,
            groupField: 'categoryOrder'
        });

        this.columns = [{
            header: lang('Name'),
            sortable: false,
            menuDisabled: true,
            dataIndex: 'name',
            flex: 1
        }, {
            header: lang('Abbreviation'),
            dataIndex: 'abbreviation',
            sortable: false,
            menuDisabled: true,
            width: 80
        }, {
            header: lang('Required'),
            dataIndex: 'required',
            sortable: false,
            menuDisabled: true,
            width: 70,
            renderer: function(value, metadata) {
                if (value == 1) {
                    metadata.tdCls = metadata.tdCls + 'x-osdn-grid-cell-checked';
                    metadata.tdAttr = (metadata.tdAttr || "") + " data-qtip='" + lang('Yes') + "'";
                } else {
                    metadata.tdAttr = (metadata.tdAttr || "") + " data-qtip='" + lang('No') + "'";
                }
            }
        }, {
            xtype: 'checkcolumn',
            text: 'Replaceable',
            dataIndex: 'replaceable',
            width: 55,
            listeners: {
                checkchange: this.onCheckReplaceable,
                scope: this
            }
        }];


        if (!this.itemId) {
            var groupingFeature = Ext.create('Ext.ux.grid.feature.GroupingOverride',{
                enableGroupingMenu: false,
                startCollapsed: false,
                scrollOffset: 22,
                groupHeaderTpl: '<span data-qtip="{[values.rs[0].get("categoryName") != null ? values.rs[0].get("categoryName") : "' + lang('Without category') + '"]}" '
                    + ' style="white-space: nowrap; overflow: hidden; display: block;">'
                    + '{[values.rs[0].get("categoryName") != null ? values.rs[0].get("categoryName") : "' + lang('Without category') + '"]}'
                    + '</span>'
            });
        }

        this.features = [groupingFeature];

        this.callParent(arguments);

        this.on('afteredit', this.onCheckReplaceable, this);
    },

    onCheckReplaceable: function(column, recordIndex, checked, record) {

        var action = Ext.ux.OSDN.empty(checked) ? 'delete' : 'create';
        Ext.Ajax.request({
            url: link('document', 'document-type-replaceable', action, {format: 'json', etype: this.entityType}),
            params: {
                documentTypeId: this.documentTypeId,
                replaceableId: record.get('id')
            },
            callback: function(options, success, response) {
                if (true === success) {
                    var res = Ext.decode(response.responseText);
                    if (true === res.success) {
                        record.commit();
                    } else {
                        record.reject();
                    }
                }
            },
            scope: this
        });
    }

//    ,showInWindow: function() {
//        this.window = new Ext.Window({
//            layout: 'fit',
//            title: this.title,
//            width: 400,
//            autoHeight: true,
//            border: false,
//            modal: true,
//            items: [this],
//            buttons: [{
//                text: lang('Close'),
//                iconCls: 'icon-cancel-16',
//                handler: function() {
//                    this.window.close();
//                },
//                scope: this
//            }]
//        });
//        this.setTitle('');
//        this.window.show.apply(this.window, arguments);
//        return this.window;
//    }
});
