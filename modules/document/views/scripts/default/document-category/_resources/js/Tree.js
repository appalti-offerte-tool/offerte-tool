//  Bug on Ext.grid.plugin.CellEditing for Tree
//  http://www.sencha.com/forum/showthread.php?136671-OPEN-EXTJSIV-2539-4.0.2-TreeGrid-CellEditing-broken
//  Should be fixed in 4.1.0
//
//
Ext.define('Module.Document.DocumentCategory.Tree', {

    extend: 'Ext.tree.Panel',

    alias: 'module.document.document-category.tree',

    title: lang('Document categories'),

    untitledNodeName: 'untitled-mandatory',

    useArrows: true,

    rootVisible: true,

    animCollapse: false,
    animate: false,

    draggable: true,

    /**
     * @todo fix hardcoded module name
     */
    entityType: null,

    iconCls: 'osdn-document-mandatory-icon-16x16',

    nodeIconCls: '',

    treeEditor: null,

    initComponent: function() {

        this.viewConfig = {
            plugins: {ptype: 'treeviewdragdrop'}
        };

        this.store = Ext.create('Ext.data.TreeStore', {
            clearOnLoad: false,
            proxy: {
                actionMethods: {
                    read: 'POST',
                    update: 'POST'
                },
                type: 'ajax',
                url: link('document', 'document-category', 'fetch-all', {format: 'json'}),
                extraParams: {
                    etype: this.entityType
                },
                simpleSortMode: true
            },
            root: {
                text: lang('All categories'),
                id: '0',
                iconCls: this.nodeIconCls,
                expanded: true
            },
            fields: ['id', 'text', 'leaf', 'expanded']
        });

        this.tools = [{
            type:'plus',
            tooltip: 'Add',
            handler: function() {
                this.createProcess(this.getRootNode());
            },
            scope: this
        }, {
            type:'refresh',
            tooltip: 'Refresh',
            handler: function() {
                this.getRootNode().removeAll();
                this.getStore().load();
            },
            scope: this
        }];

        this.treeEditor = Ext.create('Ext.ux.TreeCellEditing', {
            clicksToEdit: 0
        });

        this.columns = [{
            xtype: 'treecolumn',
            dataIndex: 'text',
            id: 'text',
            flex: 1,
            editor: {
                type: 'textfield',
                allowBlank: false
            }
        }]

        this.plugins = [this.treeEditor];

        this.callParent(arguments);

        this.on({
            itemcontextmenu: this.onContextMenu,
            nodedragover: this.onNodeDragOver,
            edit: this.renameProcess,
            scope: this
        });

        this.getStore().on('beforemove', this.onBeforeMoveCoreNodeExists, this);

    },

    onContextMenu: function(view, node, item, index, e) {

        this.fireEvent('click', node, e);
        e.stopEvent();

        var menu = new Ext.menu.Menu({items: []});
        if (node == this.getRootNode()) {
            menu.add({
                text: lang('Create'),
                iconCls: 'icon-create-16',
                handler: function() {
                    this.createProcess(node);
                },
                scope: this
            });
        } else {
            menu.add({
                text: lang('Quick edit'),
                iconCls: 'icon-edit-16',
                handler: function() {
                      this.treeEditor.startEdit(node, this.headerCt.getComponent('text'));
                },
                scope: this
            });

            menu.add({
                text: lang('Delete'),
                iconCls: 'icon-delete-16',
                handler: function() {
                    Ext.Msg.confirm(lang('Confirmation'), lang('The category "{0}" will be deleted', node.text), function(b) {
                        b == 'yes' && this.removeProcess(node);
                    }, this);
                },
                scope: this
            });
        }
        menu.showAt(e.getXY());
    },

    createProcess: function(node) {
        var parentNode = this.getRootNode();
        var text = this.untitledNodeName;
        var i = 1;
        do {
            text = this.untitledNodeName + '_' + i;
        } while (parentNode.findChild('text', text, true) && i++ < 20000);
        Ext.Ajax.request({
            method: 'post',
            url: link('document', 'document-category', 'create', {format: 'json', name: text, etype: this.entityType}),
            success: function(response) {
                var decResponse = Ext.decode(response.responseText);
                if (decResponse && decResponse.success && decResponse.id > 0) {
                    this.getStore().on('load', function() {
                        var newNode = parentNode.findChild('id', decResponse.id, true)
                        this.treeEditor.startEdit(newNode, this.headerCt.getComponent('text'));
                    }, this, {single: true})
                    this.getRootNode().removeAll();
                    this.getStore().load();
                    return;
                }

//                Application.notificate(decResponse.messages || lang('Unable to create category'));
            },
            scope: this
        });
    },

    renameProcess: function(editor, e) {

        var startValue = e.originalValue;
        var value = e.value;
        var node = e.record;

        if (startValue === value) {
            e.record.reject();
            return;
        }

        Ext.Ajax.request({
            url: link('document', 'document-category', 'rename', {format: 'json', etype: this.entityType}),
            params: {
                node: node.get('id'),
                text: value
            },
            success: function(response) {
                var decResponse = Ext.decode(response.responseText);

                if (!decResponse.success) {
                    e.record.reject();
                    Application.notificate(decResponse.messages || lang('Rename category failed.'));
                    return;
                }
                e.record.commit();
                Application.notificate(decResponse.messages || lang('Updated successfully.'));
            },
            failure: function() {
                e.record.reject();
            },
            scope: this
        });
    },

    removeProcess: function(node, force) {
        Ext.Ajax.request({
            url: link('document', 'document-category', 'delete', {format: 'json', etype: this.entityType}),
            params: {
                node: node.get('id'),
                force: +!!force
            },
            success: function(response) {
                var decResponse = Ext.decode(response.responseText);
                if (decResponse.success) {
                    node.parentNode.removeChild(node);
                    Application.notificate(decResponse.messages || lang('Deleted successfully.'));
                    return;
                }

                if (decResponse.force) {
                    Ext.Msg.confirm(
                        lang('Confirmation'),
                        decResponse.messages[0].message + '<br/>' + lang('Do you really want to continue?'),
                        function(btn) {
                            if ('yes' != btn) {
                                return;
                            }

                            this.removeProcess(node, true);
                        },
                        this
                    );

                    return;
                }

                Application.notificate(decResponse.messages || lang('Delete category failed.'));
            },
            scope: this
        });
    },

    onNodeDragOver: function(e) {
        var nodeId = parseInt(e.target.id, 10);
        if (nodeId === 0) {
            e.cancel = true;
            return;
        }
    },

    onBeforeMoveCoreNodeExists: function(node, oldParent, newParent, index) {

        if (oldParent != newParent) {
            return false;
        }

        Ext.Ajax.request({
            url: link('document', 'document-type', 'change-category-order', {format: 'json', etype: this.entityType}),
            params: {
                categoryId: node.get('id'),
                position: index
            },
            success: function() {
//                this.getRootNode().removeAll();
//                this.getStore().load();
            },
            scope: this
        });

        return true;
    }
});
