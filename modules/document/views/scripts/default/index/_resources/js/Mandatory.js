Ext.define('Module.Document.Mandatory', {

    extend: 'Module.Document.DocumentAbstract',

    stateful: true,

    stateId: 'module.document.mandatory',

    alias: 'module.document.mandatory',

    iconCls: 'osdn-documents',

    itemId: null,
    tmpEntityTypeId: null,

    allowedDocumentTypes: null,

    resource: false,

    groupResult: true,

    isAddAllowed: true,

    isUpdateAllowed: true,

    isDeleteAllowed: true,

    allowComments: true,

    orderConvF: function() {return true;},

    onBeforeInitComponent: function() {

        this.viewConfig = {
            getRowClass: this.onGetRowClassOverload
        };

        this.tools = [{
            type: 'list',
            tooltip: lang('Show all document forms'),
            handler: this.onShowDocumentForms,
            scope: this
        }];

        this.orderConvF = function(v, rec) {
            var val = (rec.get('categoryOrder') ? parseInt(rec.get('categoryOrder')) : 0) * 1000000 + (rec.get('position') ? parseInt(rec.get('position')) : 0);
            return val;
        };

        Ext.define('Module.Document.Model.Mandatory', {
            extend: 'Ext.data.Model',
            fields: [
                'id', 'name', 'required', 'expireDate', 'presence', 'question', 'categoryId', 'categoryName',
                {name: 'categoryOrder', type: 'int'},
                {name: 'position', type: 'int'},
                'title', 'sourceId', 'sourceName', 'subject', 'documentTypeId', 'fileStockId', 'files',
                {name: 'requestDate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
                {name: 'receiveDate', type: 'date', dateFormat: 'Y-m-d H:i:s'},
                'name', 'type', 'abbreviation', 'templates', 'size', 'comments', 'isPresence',
                {
                    name: 'orderField',
                    mapping: 'position',
                    convert: this.orderConvF,
                    type: 'int',
                    scope: this
                }
            ]
        });

        this.store = new Ext.data.Store({
            model: 'Module.Document.Model.Mandatory',
            proxy: {
                actionMethods: {
                    read: 'POST',
                    update: 'POST'
                },
                type: 'ajax',
                url: link(this.entityType, this.controller, 'fetch-all-mandatory-documents', {format: 'json', etype: this.entityType}),
                extraParams:{
                    entityId: this.entityId
                },
                simpleSortMode: true,
                reader: {
                    type: 'json',
                    root: 'rowset',
                    totalProperty: 'total'
                }
            },
            remoteSort: false,
            sorters: {
                field: 'orderField',
                direction: 'ASC'
            },
            groupField: 'categoryOrder',
            listeners: {
                load: function(store) {
                    store.sort('orderField', 'ASC');
                },
                scope: this
            }
        });

        this.features = [{
            ftype: 'filters'
        }];

        if (this.groupResult) {
            var groupingFeature = Ext.create('Ext.ux.grid.feature.GroupingOverride',{
                groupHeaderTpl: '<span data-qtip="{[values.rs[0].get("categoryName") != null ? values.rs[0].get("categoryName") : "' + lang('Without category') + '"]}" '
                    + ' style="white-space: nowrap; overflow: hidden; display: block;">'
                    + '{[values.rs[0].get("categoryName") != null ? values.rs[0].get("categoryName") : "' + lang('Without category') + '"]}'
                    + '</span>'
            });
            this.features.push(groupingFeature);
        } 

        var getTemplatesQtip = function(record) {

            var templates = null;
            if (record.get('templates')) {
                templates = record.get('templates');
            }

            if (templates && templates.length > 0) {
                var text = [];
                if (templates.length == 1) {
                    text.push(['<b>', lang('Available form'), ':</b>'].join(''));
                } else {
                    text.push(['<b>', lang('Available forms'), ':</b>'].join(''));
                }

                var i = 1;
                Ext.each(templates, function(template) {
                    text.push([i++, ' : ', template.description || template.name,' ( ', Ext.util.Format.fileSize(template.size), ' )'].join(''));
                }, this);
                return text.join('<br/>');
            }

            return '';
        };

        this.columns = [new Ext.grid.RowNumberer(), {
            header: lang('Title'),
            dataIndex: 'title',
            flex: 1,
            sortable: false,
            filterable: 1,
            menuDisabled: true,
            renderer: function(value, metadata, record) {
                var templates = record.get('templates'), qtip;
                var name = record.get('name'), qtip;
                value  = value || name;

                if (record.get('files') && record.get('files').length > 0) {
                    qtip = this.initQtipTemplateFiles(record);
                    value = '<span data-qtip="' + qtip + '" class="osdn-icon-attached-file">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>' + value;
                }

                if (templates.length > 0) {
                    qtip = getTemplatesQtip(record);
                    value = '<span data-qtip="' + qtip + '" class="osdn-templates">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>' + value;
                }

                if (record.get('required') == 1) {
                    value = ['<div style="color:red;" >', value , '</div>'].join('');
                }
                return value;
            },
            scope: this
        }, {
            header: lang('Location'),
            dataIndex: 'sourceName',
            menuDisabled: true,
            sortable: false,
            width: 150,
            renderer: function(value, metadata, record, rowIndex, colIndex, store) {
                var html = value;
                if ('0' === record.get('isPresence') && (!record.get('files') || record.get('files').length == 0)) {
                    html += ' ( <span style="color:red;">' + lang('missed files') + '</span> )';
                }
                return html;
            }
        }, {
            header: lang('Subject'),
            dataIndex: 'subject',
            menuDisabled: true,
            sortable: false,
            hidden: true
        }, {
            header: lang('Requested date'),
            dataIndex: 'requestDate',
            width: 120,
            sortable: false,
            menuDisabled: true,
            renderer: function(v) {
                if (v) {
                return Ext.Date.format(v, 'd-m-Y H:i');
                }
                return '';
            }
        }, {
            header: lang('Received date'),
            dataIndex: 'receiveDate',
            width: 120,
            sortable: false,
            menuDisabled: true,
            renderer: function(v, m) {
                return v ? Ext.Date.format(v, 'd-m-Y H:i') : '';
            }
        }, {
            header: lang('Comments'),
            dataIndex: 'comments',
            width: 60,
            menuDisabled: true,
            align: 'center',
            renderer: function(value, metadata, record, rowIndex,  colIndex, store) {
                metadata.tdCls = 'm-comment-list';
                metadata.tdAttr = 'data-qtip="' + lang('Add comment') + '"';
                return value;
            }
        }, {
            header: 'R',
            tooltip: lang('Make request'),
            sortable: false,
            dataIndex: 'presence',
            hideable: false,
            width: 25,
            fixed: true,
            menuDisabled: true,
            renderer: function(value, metadata, record, rowIndex, colIndex, store) {
                if (record.get('presence') === null || (!record.get('requestDate') && record.get('presence') == '0')) {
                    metadata.tdAttr = " style='cursor:pointer;' data-qtip = '" + lang('Make request') + "'";
                    metadata.tdCls = 'osdn-document-add-request';
                } else if (record.get('presence') == '0') {
                    metadata.tdAttr = " style='cursor:pointer;' data-qtip = '" + lang('Remove request') + "'";
                    metadata.tdCls = 'osdn-document-delete-request';
                } else if (record.get('presence') == '1'
                     && record.get('requestDate')
                     && record.get('receiveDate')
                ) {
                    metadata.tdAttr = "data-qtip = '" + lang('Requested document was added') + "'";
                    metadata.tdCls = 'osdn-document-requested-added';
                }
            }
        }];

        this.on('itemcontextmenu', function(view, record, item, index, e, options) {
             e.stopEvent();
             var contextMenu = this.createContextMenu(record);
             contextMenu.showAt(e.xy);
        });

    },

    createContextMenu: function(record) {
        var items = [];
        var subitems = [];

        var docName = record.get('name');

//        if (acl.isView('document', 'comment')) {
            items.push({
                text: lang('Comments'),
                iconCls: 'osdn-comments',
                handler: function() {
                    this.onCommentClick(record);
                },
                scope: this
            });
            items.push("-");
//        }

        if (this.isAddAllowed) {

            if (record.get('presence') != 1) {
                items.push({
                    text: lang('Add {0} document', docName),
                    iconCls: 'osdn-document-add',
                    handler: function() {
                        if (record.get('id')) {
                            this.onEditDocument(record);
                        } else {
                            this.onCreateDocument(record);
                        }
                    },
                    scope: this
                });
            }

            if (record.get('abbreviation') == 'FORMULIER') {
                items.push({
                    text: lang('Generate {0} form', docName),
                    iconCls: 'osdn-icon-documents-16x16',
                    handler: function() {
                        this.generateFormulier(record);
                    },
                    scope: this
                });
            }

            if (Ext.ux.OSDN.empty(record.get('id'))) {
                items.push("-");
            }

        }

        if (this.isUpdateAllowed) {

            if (record.get('presence') != 1) {
                items.push({
                    text: lang('Make request of document `{0}` ', docName),
                    iconCls: 'osdn-document-add-request',
                    handler: function() {
                        this.doMakeRequest(record);
                    },
                    scope: this
                });
            }
    
        }

        if (this.isUpdateAllowed) {
            if (record.get('id') && record.get('presence') == 1) {
                items.push({
                    text: lang('Edit'),
                    iconCls: 'osdn-document-edit',
                    handler: function() {
                        this.onEditDocument(record);
                    },
                    scope: this
                });
            }
        }

        if (this.isDeleteAllowed) {

            if (record.get('id') && record.get('presence') == 1) {
                items.push({
                    text: lang('Delete'),
                    iconCls: 'osdn-document-delete',
                    handler: function() {
                        this.onDeleteDocument(record);
                    },
                    scope: this
                });
            }
        }

        if (this.isUpdateAllowed) {
            if (record.get('id')
                && record.get('presence') == 1
                && record.get('isPresence') == 0) {

                items.push({
                    text: lang('Upload file'),
                    iconCls: 'icon-update-16',
                    handler: function() {
                        this.onUploadFile(record);
                    },
                    scope: this
                });
            }
        }

        if (record.get('files')) {
            var files = record.get('files');
            if (files.length == 1) {
                var text = ['"', files[0]['name'],'" ( ', Ext.util.Format.fileSize(files[0]['size']), ' )'].join('');
                items.push({
                    text: lang('Download {0}', text),
                    qtip: text,
                    iconCls: 'osdn-icon-attached-file',
                    handler: function() {
                        this.onDownloadDocument.call(this, files[0]['id']);
                    },
                    scope:this
                });
            } else if (files.length > 1) {
                subitems = [];
                Ext.each(files, function(file) {
                    var text = lang('Download {0}', ['"', file['name'], '" ( ', Ext.util.Format.fileSize(file['size']), ' )'].join(''));
                    subitems.push({
                        text: text,
                        qtip: text,
                        iconCls: 'osdn-icon-attached-file',
                        handler: function() {
                            this.onDownloadDocument.call(this, file['id']);
                        },
                        scope:this
                    });
                }, this);

                items.push({
                    text: lang('Files'),
                    iconCls: 'osdn-icon-attached-file',
                    menu: {
                        items: subitems
                    }
                });
            }
        }


        var templates = record.get('templates');
        if (templates.length != 0 && 0 != items.length) {
            items.push('-');
        }

        if (templates.length > 0) {
            subitems = [];
            
            Ext.each(templates, function(v) {

                if (v['description']) {
                    var text = v['description'] + ' ( ' + v['name'] + ' - ' + Ext.util.Format.fileSize(v['size']) + ' )';
                } else {
                    var text = v['name'] + ' ( ' + Ext.util.Format.fileSize(v['size']) + ' )';
                }

                subitems.push({
                    text: text,
                    qtip: text,
                    iconCls: 'osdn-icon-attached-file',
                    handler: function() {
                        this.onDownloadDocument(v['id']);
                    },
                    scope: this
                });
            }, this);

            items.push({
                text: lang('Forms'),
                iconCls: 'osdn-templates',
                menu: {
                    items: subitems
                }
            });

        }

        if (this.isUpdateAllowed) {
            if (record.get('requestDate') && record.get('presence') != 1) {
                items.push("-");

                items.push({
                    text: lang('Delete request of document `{0}` ', record.get('name')),
                    iconCls: 'osdn-document-delete-request',
                    handler: function() {
                        this.onDeleteDocument(record);
                    },
                    scope: this
                });
            }
        }

        var m = new Ext.menu.Menu({
            items: items,
            listeners: {
                click: function() {
                    m.hide();
                }
            }
        });

        return m;
    },

    onAfterInitComponent: function() {
        this.on({
            render:  function() {
                this.doLoad();
            },
            itemdblclick: function(g, rowIndex) {
                var record = g.getStore().getAt(rowIndex);
                if (record.get('id')) {
                    if (g.isUpdateAllowed) {
                        this.onEditDocument.apply(this, arguments);
                    }
                } else {
                    if (g.isAddAllowed) {
                        this.onCreateDocument.apply(this, arguments);
                    }
                }
            },

            cellclick: function(grid, cell, columnIndex, record , node , rowIndex , evt) {

                switch(grid.getHeaderCt().getHeaderAtIndex(columnIndex).dataIndex) {
                    case 'presence':
                        if (record.get('presence') != 1) {
                            this.doMakeRequest(record, !!record.get('requestDate'));
                        }
                        break;

                    case 'comments':
                        this.onCommentClick(record);
                        break;
                }
            },
            scope: this
        });

        this.addEvents('beforeupload', 'afterupload');
    },

    doMakeRequest: function(record, remove) {

        Ext.Msg.confirm(lang('Question'), lang('Are you sure? '), function(b) {
            if ('yes' != b) {
                return;
            }

            var id = record.get('id');
            record.set('presence', 0);

            Ext.Ajax.request({
                url: link(this.entityType, this.controller, id ? 'update' : 'create', this.getWrappedInBaseParams({}, true)),
                params: {
                    id: id,
                    title:  record.get('name'),
                    documentTypeId: record.get('documentTypeId'),
                    entityId: this.entityId,
                    itemId: this.itemId,
                    presence: 0,
                    removeRequest: remove? 1: 0,
                    makeRequest: !remove? 1: 0
                },
                success: function(response) {
                    var r = Ext.decode(response.responseText);
                    if (r.success) {
                        record.commit();
                        this.setLastInsertedId(r.documentId);
                        this.doLoad();
                    } else {
                        record.reject();
                    }
                },
                scope: this
            });

        }, this);
    },

    onCreateDocument: function(record) {

        Application.require([
            'document/form/.'
        ], function() {

            var f = new Module.Document.Form({
                entityType: this.entityType,
                controller: this.controller,
                accountFullname: this.accountFullname,
                allowComments: this.allowComments,
                entityId: this.entityId,
                itemId: this.itemId,
                tmpEntityTypeId: this.tmpEntityTypeId,
                permissions: this.isAddAllowed,
                disabledTitle: record.get('name'),
                question: record.get('question'),
                expireDate: record.get('expireDate') == 1,
                hiddenFields: this.hiddenFields,
                documentTypeId: record.get('documentTypeId')
            });

            f.on('completed', function(documentId) {
                this.setLastInsertedId(documentId);
                this.doLoad();
            }, this);

            var w = f.showInWindow();

        }, this);
    },

    onEditDocument: function(record) {

        Application.require([
            'document/form/.'
        ], function() {

            var f = new Module.Document.Form({
                entityType: this.entityType,
                controller: this.controller,
                accountFullname: this.accountFullname,
                allowComments: this.allowComments,
                allowedFormats: this.allowedFormats,
                documentId: record.get('id'),
                entityId: this.entityId,
                itemId: this.itemId,
                tmpEntityTypeId: this.tmpEntityTypeId,
                permissions: this.isUpdateAllowed,

                disabledTitle: record.get('name'),
                question: record.get('question'),
                expireDate: record.get('expireDate') == 1,
                hiddenFields: this.hiddenFields,
                documentTypeId: record.get('documentTypeId')
            });

            f.on('completed', function(documentId) {
                this.setLastInsertedId(documentId);
                this.getStore().load();
            }, this);

            var w = f.showInWindow();

        }, this);
    },

    doLoad: function(p) {
        if (p && p.entityId) {
            this.entityId = p.entityId;
        }
        if (p && p.itemId) {
            this.itemId = p.itemId;
        }
        if (this.entityId) {
            var params = {
                entityId:  this.entityId
            };
            if (this.itemId) {
                params.itemId = this.itemId;
            }
            if (this.allowedDocumentTypes !== null) {
                params.allowedDocumentTypes = Ext.encode(this.allowedDocumentTypes);
            }

            this.getStore().load({
                params: params
            });
        }
    },

    generateFormulier: function() {
        new OSDN.Report.Handler({
            module: 'student',
            controller: 'report',
            report: 'aanmeldings-formulier',
            params: OSDN.encode({
                courseId: this.itemId,
                courseInstanceId: this.ci_id || null,
                studentIds: [this.entityId]
            }, true),
            allowedFormats: {
                rtf: false,
                pdf: /VTU|EB/i.test(OSDN.COMPANY_NAME),
                xls: (/VTU|EB/i.test(OSDN.COMPANY_NAME) == false),
                html: (/VTU|EB/i.test(OSDN.COMPANY_NAME) == false)
            }
        });
    },

    onGetRowClassOverload: function(record, rowIndex, rowParams, store) {
        if (record.get('presence') == '0' && record.get('requestDate')) {
            return 'osdn-document-requested';
        } else if (record.get('presence') == '1'
             && record.get('requestDate')
             && record.get('receiveDate')
        ) {
            return 'osdn-document-received';
        }
    },

    onShowDocumentForms: function() {

        Application.require([
            'document/document-type/pre-printed-forms-list'
        ], function() {

            var f = new Module.Document.DocumentType.PrePrintedFormsList({
                autoLoadRecords: true,
                autoLoadRecordsCallbackFn: 'doLoad',
                width: 500,
                height: 300,
                entityId: this.entityId,
                itemId: this.itemId,
                controller: this.controller,
                entityType: this.entityType,
                plugins: [Ext.create('Ext.ux.plugins.panel.Window')],
                title: lang('Document forms')
            });

            f.showInWindow({
                submitBtnCaption: false
            });

        }, this);
    }
});
