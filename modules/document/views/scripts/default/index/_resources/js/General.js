Ext.define('Module.Document.General', {

    extend: 'Module.Document.DocumentAbstract',

    stateful: true,

    stateId: 'module.document.general',

    controller: null,

    entityId: null,
    entityType: null,

    itemId: 1,

    tmpEntityTypeId: null,

    accountFullname: 'Anonymous',

    resource: false,

    iconCls: 'osdn-documents',

    allowedFormats: '',

    hiddenListFields: null,

    allowComments: true,

    isDeleteAllowed: true,

    isAddAllowed: true,

    isUpdateAllowed: true,

    onBeforeInitComponent: function() {

        Ext.define('Module.Document.Model.General', {
            extend: 'Ext.data.Model',
            fields: [
                'id', 'title', 'sourceName', 'sourceId', 'subject',
                'typeId', 'fileStockId', 'files', 'name', 'comments', 'type', 'size', 'presence','isPresence'
            ]
        });

        this.store = new Ext.data.Store({
            model: 'Module.Document.Model.General',
            autoLoad: 1,
            proxy: {
                type: 'ajax',
                url: link(this.entityType, this.controller, 'fetch-all-general-documents', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset',
                    totalProperty: 'total'
                },
                extraParams: {
                    entityId: this.entityId,
                    itemId: this.itemId
                },
                simpleSortMode: true
            },
            remoteSort: true,
            sorters: {
                property: 'title',
                direction: 'ASC'
            }
        });

        this.tools = [{
            type: 'plus',
            tooltip: lang('Add'),
            handler: this.onCreateDocument,
            scope: this
        }];

        this.tbar = ['->'];

        this.columns = [new Ext.grid.RowNumberer(), {
            header: lang('Title'),
            dataIndex: 'title',
            filterable: 1,
            flex: 1,
            width: 150,
            renderer: function(value, metadata, record, rowIndex,  colIndex, store) {
                var qtip = '', text = '';
                if (record.get('files') && record.get('files').length > 0) {
                    qtip = ' data-qtip="' + this.initQtipTemplateFiles(record) + '"';
                    return '<div><span  class="osdn-icon-attached-file" ' + qtip + '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> ' + (value || '&nbsp;') + ' </div>';
                } else {
                    return value || '&nbsp;';
                }
            },
            scope: this
        }, {
            header: lang('Location'),
            dataIndex: 'sourceName',
            flex: 1,
            width: 160,
            renderer: function(value, metadata, record, rowIndex, colIndex, store) {
                var html = value;
                if ('0' === record.get('isPresence') && (!record.get('files') || record.get('files').length == 0)) {
                    html += ' ( <span style="color:red;">' + lang('missed files') + '</span> )';
                }
                return html;
            }

        }, {
            flex: 1,
            header: lang('Subject'),
            hidden: this.hiddenListFields && this.hiddenListFields.subject ? true : false,
            dataIndex: 'subject'
        }];

        if (this.allowComments) {
            this.columns.push({
                header: lang('Comments'),
                dataIndex: 'comments',
                width: 60,
                align: 'center',
                renderer: function(value, metadata, record, rowIndex,  colIndex, store, view) {
                    metadata.tdCls = 'm-comment-list';
                    metadata.tdAttr = (metadata.tdAttr || '') + ' data-qtip="' + lang('Comments list') + '" ';
                    return value;
                }
            });
        }

        this.on('itemcontextmenu', function(view, record, item, index, e, options) {
             e.stopEvent();
             var contextMenu = this.createContextMenu(record);
             contextMenu.showAt(e.xy);
        });

        this.features = [{
            ftype: 'filters'
        }];

        this.plugins = [Ext.create('Ext.ux.grid.Search', {
            minChars: 2,
            stringFree: true
        })];

    },

    createContextMenu: function(record) {
        var items = [];

        if (this.allowComments) {
            items.push({
                text: lang('Comments'),
                iconCls: 'osdn-comments',
                handler: function() {
                    this.onCommentClick(record);
                },
                scope: this
            });
            if (this.isAddAllowed) {
                items.push("-");
            }
        }

        if (this.isUpdateAllowed) {
            items.push({
                text: lang('Edit'),
                iconCls: 'osdn-document-edit',
                handler: function() {
                    this.onEditDocument(record);
                },
                scope: this
            });
        }
        if (this.isDeleteAllowed) {
            items.push({
                text: lang('Delete'),
                iconCls: 'osdn-document-delete',
                handler: function() {
                    this.onDeleteDocument(record);
                },
                scope: this
            });
        }

        if (this.isUpdateAllowed) {

            if (record.get('id') && record.get('presence') == 1 && record.get('isPresence') == 0) {
                items.push({
                    text: lang('Upload file'),
                    iconCls: 'icon-update-16',
                    handler: function() {
                        this.onUploadFile(record);
                    },
                    scope: this
                });

                items.push({
                    xtype: 'module.files-stock.upload-button1',
                    iconCls: 'icon-create-16',
                    maxHeight: 28,
                    mode: 'menu',
                    uploadUrl: link('document', 'document-file-stock', 'create', {format: 'json'}),
                    parameters: function() {
                        return {
                            documentId: record.get('id'),
                            description: ''
                        }
                    }
                });
            }
        }

        if (record.get('files')) {

            var files = record.get('files');

            if (files.length == 1) {
                var text = ['"', files[0]['name'], '" ( ', Ext.util.Format.fileSize(files[0]['size']), ' )'].join('');
                items.push({
                    text: lang('Download {0}', Ext.util.Format.fileSize(files[0]['size'])),
                    qtip: text,
                    iconCls: 'osdn-icon-attached-file',
                    handler: function() {
                        this.onDownloadDocument(files[0]['id']);
                    },
                    scope: this
                });
            } else {
                var subitems = [];
                Ext.each(files, function(file) {
                    var text = lang('Download {0}', ['"', file['name'], '" ( ', Ext.util.Format.fileSize(file['size']), ' )'].join(''));
                    subitems.push({
                        text: text,
                        qtip: text,
                        iconCls: 'osdn-icon-attached-file',
                        handler: function() {
                            this.onDownloadDocument(file['id']);
                        },
                        scope: this
                    });
                }, this);

                items.push({
                    text: lang('Files'),
                    iconCls: 'osdn-icon-attached-file',
                    menu: {
                        items: subitems
                    }
                });
            }
        }

        var m = new Ext.menu.Menu({
            items: items,
            listeners: {
                click: function() {
                    m.hide();
                }
            }
        });

        return m;

    },

    onAfterInitComponent: function() {

        this.on('itemdblclick', function(view, record, item, index, e) {
            if (record.get('id')) {
                this.isUpdateAllowed && this.onEditDocument(record);
            } else {
                this.isAddAllowed && this.onCreateDocument(record);
            }
        }, this);

        this.on('cellclick', function(grid, cell, columnIndex, record , node , rowIndex , evt) {

            var dataIndex = grid.getHeaderCt().getHeaderAtIndex(columnIndex).dataIndex;
            if (dataIndex == 'comments') {
                this.onCommentClick(record);
            }
        }, this);

        this.addEvents('beforeupload', 'afterupload');
    },

    onCreateDocument: function(record) {

        Application.require([
            'document/form/.'
        ], function() {

            var f = new Module.Document.Form({
                entityType: this.entityType,
                controller: this.controller,
                accountFullname: this.accountFullname,
                allowComments: this.allowComments,
                allowedFormats: this.allowedFormats,
                entityId: this.entityId,
                itemId: this.itemId,
                tmpEntityTypeId: this.tmpEntityTypeId,
                permissions: this.isAddAllowed
            });

            f.on('completed', function(documentId) {
                this.setLastInsertedId(documentId);
                this.doLoad();
            }, this);

            var w = f.showInWindow();

        }, this);
    },

    onEditDocument: function(record) {

        Application.require([
            'document/form/.'
        ], function() {

            var f = new Module.Document.Form({
                entityType: this.entityType,
                controller: this.controller,
                accountFullname: this.accountFullname,
                allowComments: this.allowComments,
                allowedFormats: this.allowedFormats,
                documentId: record.get('id'),
                entityId: this.entityId,
                itemId: this.itemId,
                tmpEntityTypeId: this.tmpEntityTypeId,
                permissions: this.isUpdateAllowed
            });

            f.on('completed', function(documentId) {
                this.setLastInsertedId(documentId);
                this.doLoad();
            }, this);

            var w = f.showInWindow();

        }, this);
    },

    doLoad: function(p) {

        if (p && p.entityId) {
            this.entityId = p.entityId;
        }
        if (p && p.itemId) {
            this.itemId = p.itemId;
        }

        if (!this.entityId) {
            return;
        }

        var params = {
            entityId: this.entityId
        };

        if (this.itemId) {
            params.itemId = this.itemId;
        }

        if (this.tmpEntityTypeId) {
            params.tmp_entity_typeId = this.tmpEntityTypeId;
        }

        this.getStore().load({
            params: params
        });
    }
});