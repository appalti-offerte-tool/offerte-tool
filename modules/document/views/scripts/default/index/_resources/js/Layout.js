Ext.define('Module.Document.Layout', {

    extend: 'Ext.panel.Panel',

    accountFullname: 'Anonymous',

    border: false,

    layout: 'anchor',

    height: 500,

    entityId: null,
    entityType: null,

    general: null,

    mandatory: null,

    initComponent: function() {

        this.general = new Module.Document.General({
            title: lang('General documents'),
            accountFullname: this.accountFullname,
            entityId: this.entityId,
            entityType: this.entityType,
            controller: 'index',
            commentsEntityType: 'DOCUMENT',
            commentsController: 'documents-commentary',
            region: 'north',
            anchor: '100% 40%',
            border: false,
            cls: 'x-osdn-border-bottom'
        });

        this.mandatory = new Module.Document.Mandatory({
            title: lang('Mandatory documents'),
            accountFullname: this.accountFullname,
            entityId: this.entityId,
            entityType: this.entityType,
            controller: 'index',
            commentsEntityType: 'DOCUMENT',
            commentsController: 'documents-commentary',
            anchor: '100% 60%',
            region: 'center',
            border: false,
            cls: 'x-osdn-border-top'
        });

        this.items = [this.general, this.mandatory];

        this.callParent(arguments);
    },

    setEntityId: function(id, reload) {
        this.entityId = id;
        this.general.setEntityId(this.entityId, reload);
        this.mandatory.setEntityId(this.entityId, reload);
        return this;
    }
});