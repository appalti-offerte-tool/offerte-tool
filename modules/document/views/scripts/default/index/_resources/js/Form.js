Ext.ns('Module.Document');

Ext.define('Module.Document.Form', {
    extend: 'Ext.form.Panel',

    autoScroll: false,
    bodyPadding: 5,

    labelWidth: 150,
    trackResetOnLoad: true,

    documentId: null,
    documentTypeId: null,

    entityType: null,
    entityId: null,
    itemId: null,

    controller: null,

    question: null,
    expireDate: false,

    permissions: false,

    sourceCombo: null,

    hiddenFields: {},

    w: null,
    header: false,

    allowComments: null,

    initComponent: function() {

        if (null === this.entityType) {
            throw 'Not defined module';
        }

        this.initialConfig = Ext.apply(this.initialConfig || {}, {
            trackResetOnLoad: true
        });

        this.items = [{
            xtype: 'textfield',
            fieldLabel: lang('Title'),
            name: 'title',
            anchor: '-1',
            allowBlank: false,
            enableKeyEvents: true
        }, {
            xtype: 'textfield',
            fieldLabel: lang('Author'),
            name: 'author',
            anchor: '-1',
            allowBlank: true,
            enableKeyEvents: true,
            value: this.accountFullname || ''
        }];

        if (!this.hiddenFields || !this.hiddenFields.expireDate) {
            this.items.push({
                xtype: 'datefield',
                fieldLabel: lang('Expired date'),
                name: 'expireDate',
                anchor: '-1',
                allowBlank: !this.expireDate
            });
        }

        this.items.push({
            xtype: 'datetimefield',
            fieldLabel: lang('Received date'),
            name: 'receiveDate',
            anchor: '-1',
            value: new Date()
        });

        this.items.push({
            xtype: 'htmleditor',
            labelAlign: 'top',
            height: 120,
            fieldLabel: lang('Subject'),
            name: 'subject',
            anchor: '-1',
            allowBlank: true
        });

        this.items.push({
            xtype: 'htmleditor',
            labelAlign: 'top',
            fieldLabel: lang('Keywords'),
            height: 120,
//            boxMaxWidth: 350,
            name: 'keywords',
            anchor: '-1',
            allowBlank: true
        });

        this.sourceCombo = new Module.Document.DocumentSource.ComboBox({
            width: 250,
            hiddenName: 'documentSourceId',
            anchor: '-1',
            name: 'documentSourceId',
            fieldLabel: lang('Document source'),
            allowBlank: false,
            resizable: false
        });
        this.sourceCombo.on('select', this.onDocumentSourceChange, this);
        this.items.push(this.sourceCombo);


        if (this.documentId && this.question) {
            this.items.push({
                xtype: 'checkbox',
                fieldLabel: this.question,
                name: 'questionResponse',
                anchor: 0,
                inputValue: 1
            });
        }

        this.reader = new Ext.data.JsonReader({
            root: 'data'
        }, [
            'id',
            'subject', 'title', 'author', 'keywords',
            {
                name: 'expireDate',
                type:'date',
                dateFormat: 'Y-m-d H:i:s'
            }, {
                name: 'receiveDate',
                type:'date',
                dateFormat: 'Y-m-d H:i:s'
            },
            'questionResponse',
            'documentSourceId',
            'fileData'
        ]);

        this.initialConfig.reader = this.reader;

        this.callParent(arguments);

        this.addEvents('document-updated', 'delete-file', 'reset-file', 'completed');

        this.getForm().on('actioncomplete', function(form, action) {

            if (action.type == 'submit') {
                return;
            }

            var ds = this.getForm().findField('documentSourceId');
            if (this.documentId && !ds.isPresenceValue(action.result.data.documentSourceId)) {
                this.getFileStockCt().expand();
            }

            form.findField('title').on('change', this.doRefreshTitle, this);

        }, this);

        this.on('render', this.doLoad, this);

        this.on('document-updated', function(scope, form, options, noClose) {

            if (options.result.documentId) {
                this.documentId = options.result.documentId;
                this.setNotification("");

                if (!this.getForm().findField('documentSourceId').isPresenceValue()) {
                    var fs = this.getFileStockCt();
                    if (fs.collapsed) {
                        fs.expand();
                        return false;
                    }
                }
            }

            this.fireEvent('completed', this.documentId);
            if (noClose) {
                this.form.setValues(this.form.getValues());
            } else {
                this.w.close();
            }

        }, this);

    },

    doRefreshTitle: function() {

        if (!this.w) {
            return this;
        }

        var title = lang(this.documentId ? 'Update document' : 'Create document');
        var titleField = this.getForm().findField('title');

        if (this.documentId) {
            title += ': "' + titleField.getValue() + '"';
        }

        this.w.setTitle(title);
    },

    doLoad: function() {

        if (!this.documentId) {
            this.doRefreshTitle();
            this.sourceCombo.setDefaultValue();
            return;
        }

        this.getForm().load({
            url: link(this.entityType, this.controller, 'fetch', {format: 'json'}),
            method: 'POST',
            params: {
                id: this.documentId,
                documentTypeId: this.documentTypeId
            },
            success: function(options, response) {

                var documentSourceId = this.getForm().findField('documentSourceId').getValue();
                if (Ext.ux.OSDN.empty(documentSourceId)) {
                    this.sourceCombo.setDefaultValue();
                }

                var author = this.getForm().findField('author').getValue();
                if (Ext.ux.OSDN.empty(author)) {
                    this.getForm().findField('author').setValue(this.accountFullname || '');
                }

                var receiveDate = this.getForm().findField('receiveDate').getValue();
                if (Ext.ux.OSDN.empty(receiveDate)) {
                    this.getForm().findField('receiveDate').setValue(new Date());
                }

                this.doRefreshTitle();

            },
            waitMsg: lang('Loading...'),
            scope: this
        });
    },

    onUpdateDocument: function(callback, skipClose) {

        if (!this.getForm().isValid()) {
            return;
        }

        var params = {
            entityId: this.entityId,
            documentTypeId: this.documentTypeId
        };

        if (this.itemId) {
            params.itemId = this.itemId;
        }

        if (this.documentId) {
            params.id = this.documentId;
        }

        var action = this.documentId ? 'update' : 'create';

        this.getForm().submit({
            url: link(this.entityType, this.controller, action, {format: 'json'}),
            params: params,
            success: function(form, options) {
                Application.notificate(options.result.messages);
                if (true !== options.result.success) {
                    Application.notificate('Unable to update');
                    return;
                }

                if ('function' === typeof callback) {
                    callback();
                }

                this.fireEvent('document-updated', this, form, options, skipClose);

            },
            failure: function() {
                Application.notificate('Unable to update');
                this.fireEvent('failure-update');
            },
            scope: this
        });
    },

    /**
     * Callback handler for comment click
     *
     * @param {Ext.util.Observable} e
     * @param {Ext.Element} el
     */
    onCommentClick: function(g, rowIndex) {
        
        Application.require([
            'comment/form/.'
        ], function() {
            var mcf = new Module.Comment.Form({
                actionUrl: link(this.entityType, 'document-comment', 'create', {format: 'json'}),
                baseParams: {
                    documentTypeId: this.documentTypeId,
                    entityEntityId: this.entityId
                },
                entityId: this.documentId,
                entityType: this.entityType,
                controller: 'document-comment',
                title: lang('Create new comment'),
                plugins: [Ext.create('Ext.ux.plugins.panel.Window')]
            });
            mcf.on('completed', function(f, entityId) {
                this.documentId = entityId;
                this.doRefreshTitle();
                this.fireEvent('completed', this.documentId);
            }, this);

            mcf.showInWindow();
        }, this);
    },

    showInWindow: function() {

        var filePanel = new Ext.Panel({
            region: 'center',
            layout: 'fit',
            split: true,
            collapsed: true,
            border: false
        });

        var tools = [];
        if (this.allowComments) {
            tools.push({
                id: 'plus',
                qtip: lang('Add comments'),
                handler: this.onCommentClick,
                scope: this
            });
        }

        this.region = 'north';
        this.split = true;

        this.w = new Ext.Window({
            width: 450,
            height: (this.documentId && this.question) ? 475 : 453,
            layout: 'border',
            padding: '5px',

            items: [this, filePanel],
            tools: tools,
            modal: true,
            border: false,
            bodyBorder: false,
            buttons: [{
                text: lang('Save'),
                iconCls: 'icon-update-16',
                handler: function() {
                    this.onUpdateDocument();
                },
                scope: this
            }, {
                text: lang('Cancel'),
                iconCls: 'icon-cancel-16',
                handler: function() {
                    this.w.close();
                },
                scope: this
            }]
        });

        filePanel.on('expand', function() {

            if (filePanel.items.length == 0) {

                var fileStock = new Module.FileStock.List({
                    title: lang('Connected files'),
                    hideHeaders: true,
                    entityType: this.entityType,
                    module: 'document',
                    controller: 'document-file-stock',
                    extraParams: {
                        documentId: this.documentId
                    }
                });

                filePanel.add(fileStock);
                fileStock.on('after-upload', this.onAfterUploadFile, this);
            }

            this.w.setHeight(this.w.getHeight() + 150);

            filePanel.doLayout();
            this.w.center();

        }, this);

        filePanel.on('collapse', function(p) {

            if (!this.isVisible()) {
                return false;
            }

            var w = p.ownerCt;
            w.setHeight(w.getHeight() - 150);
        });

        this.w.on('show', function() {
            this.w.center();
        }, this);

        this.w.show();

        return this.w;
    },

    getFileStockCt: function() {
        return this.w.items.last();
    },

    onDocumentSourceChange: function(combo, record, index) {

        var isPresence = combo.isPresenceValue();

        /**
         * On combo value change we should
         * check if document type storage is not online
         * show notification about lost files
         */
        if (isPresence) {

            this.setNotification("");

            if (!this.getFileStockCt().collapsed) {
                var count = this.getFileStockCt().items.first().getStore().getTotalCount();
                if (count > 0) {
                    Ext.Msg.confirm(lang('Question'), 'If you change storage source you will lose uploaded files. Are you sure?', function(b) {

                        if (b != 'yes') {
                            return false;
                        }

                        combo.setValue(record.get('id'));
                        this.getFileStockCt().collapse();
                        this.setNotification("");

                    }, this);

                    return false;
                }

                this.getFileStockCt().collapse();
            }

            return;
        }

        if (!this.documentId) {
            this.setNotification(lang('To upload files first press `{0}`<br/> to create document', lang('Save')));
            return;
        } else {
            this.getFileStockCt().expand();
        }
    },

    setNotification: function(text) {

        var tb = this.w.dockedItems.findBy(function(inItem) {
            return (inItem.alias == 'widget.toolbar' ) && inItem.dock == "bottom";
        })

        if (!tb) {
            return;
        }

        var info = null;
        
        text = text + 'wewewe';

        Ext.each(tb.items.items, function(inItem) {
            if (inItem.alias == 'widget.panel' && inItem.id == "infoPanel") {
                info = inItem;
            }
        })

        if (!info) {
            info = Ext.create('Ext.Panel', {
                id: 'infoPanel',
                html: text
            });
            tb.insert(0, info);
            tb.insert(1, '->');
        }

        info.update(text);
        if ('' != text) {
            info.show();
        } else {
            info.hide();
        }

        return this;
    },

    onAfterUploadFile: function() {
        var isPresence = this.getForm().findField('documentSourceId').isPresenceValue();
        var isDirty = this.getForm().findField('documentSourceId').isDirty();
        if (!isPresence && isDirty) {
            this.onUpdateDocument(null, true);
        } else {
            this.fireEvent('completed', this.documentId);
        }
    }

});