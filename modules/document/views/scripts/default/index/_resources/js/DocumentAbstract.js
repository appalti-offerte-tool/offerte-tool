Ext.define('Module.Document.DocumentAbstract', {

    extend: 'Ext.ux.grid.GridPanel',

    iconCls: 'osdn-documents',

    entityId: null,
    entityType: null,

    controller: null,

    tools: null,

    initComponent: function() {

        if (null === this.entityType) {
            throw 'Not defined module';
        }

        this.onBeforeInitComponent();

        if (!Ext.isArray(this.tools)) {
            this.tools = [];
        }

        this.tools.push({
            type: 'refresh',
            tooltip: lang('Refresh'),
            handler: function() {
                this.doLoad();
            },
            scope: this
        });

        this.onAfterInitComponent();

        this.bbar = Ext.create('Ext.PagingToolbar', {
            displayInfo: true,
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent(arguments);

    },

    onBeforeInitComponent: Ext.emptyFn,

    onAfterInitComponent: Ext.emptyfn,

    doLoad: Ext.emptyfn,

    /**
     * Callback handler for comment click
     *
     * @param {Ext.util.Observable} e
     * @param {Ext.Element} el
     */
    onCommentClick: function(record) {

        var id = record.get('id');
        var comments = record.get('comments');
        var documentTypeId = record.get('documentTypeId');

        if (Ext.ux.OSDN.empty(id) || Ext.ux.OSDN.empty(comments)) {
            this.onCreateComment(id, documentTypeId);
        } else {
            this.onListComments(id);
        }

    },

    onCreateComment: function(id, documentTypeId) {

        Application.require([
            'comment/form/.'
        ], function() {
            var mcf = new Module.Comment.Form({
                actionUrl: link(this.entityType, 'document-comment', 'create', {format: 'json'}),
                baseParams: {
                    documentTypeId: documentTypeId,
                    entityEntityId: this.entityId
                },
                entityId: id,
                entityType: this.entityType,
                controller: 'document-comment',
                title: lang('Create new comment'),
                plugins: [Ext.create('Ext.ux.plugins.panel.Window')]
            });
            mcf.on('completed', function(f, entityId) {
                this.setLastInsertedId(entityId);
                this.getStore().load();
            }, this);
            mcf.showInWindow();
        }, this);

    },

    onListComments: function(id) {

        Application.require([
            'comment/list/.'
        ], function() {

            var g = new Module.Comment.Tree({
                title: lang('Comments list'),
                width: 600,
                height: 400,
                entityId: id,
                entityType: this.entityType,
                controller: 'document-comment',
                plugins: [Ext.create('Ext.ux.plugins.panel.Window')],
                border: true
            });

            g.on('completed', function(f, entityId) {
                this.setLastInsertedId(entityId);
                this.getStore().load();
            }, this);

            g.showInWindow({
                submitBtnCaption: false
            });

        }, this);

    },

    setEntityId: function(entityId, reload) {

        var s = this.getStore();
        this.entityId = entityId;
        s.getProxy().extraParams.entityId = entityId;
        if (true == reload) {
            this.doLoad();
        }

        return this;
    },

    onDeleteDocument: function(record) {

        Ext.Msg.confirm(lang('Question'), lang('Are you sure?'), function(b) {
            if ('yes' != b) {
                return;
            }

            Ext.Ajax.request({
                url: link(this.entityType, this.controller, 'delete', this.getWrappedInBaseParams({}, true)),
                params: {
                    id: record.get('id')
                },
                success: function(response) {
                    var decResponse = Ext.decode(response.responseText);
                    if (decResponse && decResponse.success) {
                        Application.notificate(decResponse.messages);
                        this.doLoad();
                        return;
                    }

                    Application.notificate(decResponse.messages || lang('Unable to delete document'));
                },
                scope: this
            });

        }, this);
    },

    getWrappedInBaseParams: function(o, isJson) {
        var p = {};
        if (null != this.tmpEntityType) {
            p.tmpEntityType = this.tmpEntityType;
        }

        if (true === isJson) {
            p.format = 'json';
        }

        p.etype = this.entityType;
        return Ext.apply(p, o || {});
    },

    onUploadFile: function(record) {

        Application.require([
            'file-stock/./form'
        ], function() {

            var id = record.get('id');
            
            var w = new Module.FileStock.Form({
                baseParams: {
                    documentId: id
                },
                module: 'document',
                controller: 'document-file-stock',
                entityType: 'document'
            });

            w.on('update-file', function() {
                this.setLastInsertedId(id);
                this.getStore().load();
                w.close();
            }, this);

            w.show();
        }, this);

    },

    onDownloadDocument: function(id) {
        location.href = link('document', 'document-file-stock', 'download', {id: id});
    },

    initQtipTemplateFiles: function(record) {

        var files = record.get('files');
        if (!files || files.length == 0) {
            return '';
        }

        var text = [];
        if (files.length == 1) {
            text.push(['<b>', lang('Uploaded file:'), '</b>'].join(''));
        } else {
            text.push(['<b>', lang('Uploaded files:'), '</b>'].join(''));
        }

        var i = 1;
        Ext.each(files, function(file) {
            text.push([i++, ' : ', file.description || file.name,' ( ', Ext.util.Format.fileSize(file.size), ' )'].join(''));
        }, this);

        return text.join('<br/>');
    }
});