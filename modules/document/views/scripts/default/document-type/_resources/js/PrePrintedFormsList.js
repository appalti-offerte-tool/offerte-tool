Ext.define('Module.Document.DocumentType.PrePrintedFormsList', {

    extend: 'Ext.ux.grid.GridPanel',

    entityType: null,

    controller: null,

    entityId: null,

    itemId: null,

    allowedDocumentTypes: null,

    initComponent: function() {

        Ext.define('Module.Document.DocumentType.Model.PrePrintedFormsList', {
            extend: 'Ext.data.Model',
            fields: [
                {name: 'fileStockId'},
                {name: 'fileName'},
                {name: 'name'},
                {name: 'required'},
                {name: 'typeId'},
                {name: 'name'},
                {name: 'type'},
                {name: 'size'}
            ]
        });

        this.store = new Ext.data.Store({
            model: 'Module.Document.DocumentType.Model.PrePrintedFormsList',
            proxy: {
                actionMethods: {
                    read: 'POST',
                    update: 'POST'
                },
                url: link(this.entityType, this.controller ? this.controller : 'document', 'fetch-all-document-type-pre-printed-forms', {format: 'json'}),
                extraParams:{
                    entityId: this.entityId,
                    itemId: this.itemId
                },
                reader: {
                    type: 'json',
                    root: 'rowset',
                    totalProperty: 'total'
                },
                simpleSortMode: true
            },
            remoteSort: false,
            sorters: {
                field: 'name',
                direction: 'ASC'
            }
        });

        this.columns = [{
            header: lang('name'),
            dataIndex: 'name',
            filter: {
                type: 'stringfree'
            },
            flex: 1,
            renderer: function(value, metadata, record) {
                if (record.get('required') == 1) {
                    value = ['<div style="color:red;" >', value, '</div>'].join('');
                }
                return value;
            }
        }, {
            header: lang('file name'),
            dataIndex: 'fileName',
            width: 130
        }, {

            header: lang('size'),
            dataIndex: 'size',
            renderer: Ext.util.Format.fileSize,
            width: 80
        }];

        this.features = [{
            ftype: 'filters'
        }];

        this.bbar = Ext.create('Ext.PagingToolbar', {
            displayInfo: true,
            store: this.store,
            plugins: 'pagesize'
        });


        this.callParent(arguments);

        this.on('itemdblclick', function(view, record, item, index, e, options) {
            this.download(record);
        }, this);

        this.on('itemcontextmenu', function(view, record, item, index, e, options) {
             e.stopEvent();
             var contextMenu = this.createContextMenu(record);
             contextMenu.showAt(e.xy);
        });

        this.addEvents('beforeupload', 'afterupload');
    },

    createContextMenu: function(record) {

        var items = [];

        if (!record.get('fileStockId')) {
            return false;
        }

        var text = [
            record.get('name'),
            ' ( ', Ext.util.Format.fileSize(record.get('size')),
            ' )'
        ].join('');

        items.push({
            text: lang('Download ({0})', Ext.util.Format.fileSize(record.get('size'))),
            qtip: text,
            iconCls: 'osdn-icon-attached-file',
            handler: function() {
                this.download(record);
            },
            scope: this
        });

        return new Ext.menu.Menu({
            items: items,
            listeners: {
                click: function(m) {
                    m.hide();
                }
            }
        });;
    },

    download: function(record) {
        var id = record.get('fileStockId');
        if (!Ext.ux.OSDN.empty(id)) {
            location.href = link('document', 'document-file-stock', 'download', {id: id});
        }
    },

    doLoad: function(p) {

        if (p && p.entityId) {
            this.entityId = p.entityId;
        }

        if (p && p.itemId) {
            this.itemId = p.itemId;
        }

        if (this.entityId) {
            var params = {
                entityId:  this.entityId
            };

            if (this.itemId) {
                params.itemId = this.itemId;
            }

            if (this.allowedDocumentTypes !== null) {
                params.allowedDocumentTypes = Ext.encode(this.allowedDocumentTypes);
            }

            this.getStore().load({
                params: params
            });
        }
    }
});