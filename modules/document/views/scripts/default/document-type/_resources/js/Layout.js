Ext.define('Module.Document.DocumentType.Layout', {

    extend: 'Ext.panel.Panel',

    alias: 'module.document.document-type.layout',

    resource: false,

    border: false,

    layout: 'border',

    entityType: null,

    ddGroupCommon: 'unknown-dd',

    initComponent: function() {

        this.ddGroupCommon = this.entityType + '-dd';

        this.tree = new Module.Document.DocumentCategory.Tree({
            resource: this.resource,
            entityType: this.entityType,
                region: 'west',
            split: true,
            collapsible: true,
            border: false,
            cmargins: "0 5 0 0",
            cls: 'x-osdn-border-right',
            width: 220,
            ddGroup: this.ddGroupCommon
        });

        this.docTypesList = new Module.Document.DocumentType.List({
            title: '&nbsp;',
            resource: this.resource,
            entityType: this.entityType,
            region: 'center',
            split: true,
            collapsible: false,
            border: false,
            cls: 'x-osdn-border-left',
            enableDragDrop: true,
            ddGroup: this.ddGroupCommon
        });

        this.items = [
            this.tree,
            this.docTypesList
        ];

        this.callParent(arguments);

//        this.tree.on('beforedrop', function(e) {
//            if (!e.dropNode && e.data.grid && e.target.attributes.id) {
//                var list = e.data.grid;
//                var record = list.getStore().getAt(e.data.rowIndex);
//                list.changeCategory.call(list, record.get('id'), e.target.attributes.id);
//            }
//        }, this);

        this.tree.on('load', function(tree, node) {
            var sm = this.tree.getView().getSelectionModel();
            if (Ext.isEmpty(sm.getSelection())) {
                sm.select(node);
            }
        }, this);

        this.tree.getSelectionModel().on('selectionchange', function(tree, selectedRecords) {

            if (Ext.isEmpty(selectedRecords)) {
                return;
            }

            if (selectedRecords && Ext.isArray(selectedRecords)) {
                var node = selectedRecords[0];
            }

            if (node) {
                var id = node.get('id') || null;
            } else {
                var id = null;
            }

            if (node && id != 0) {
                this.docTypesList.setTitle(lang('{0}`s forms', node.get('text').ucFirst()));
            } else {
                this.docTypesList.setTitle(lang('All forms'));
            }

            this.docTypesList.load({
                documentCategoryId: id
            });

//            this.docTypesList.getView().onShowGroupsClick(null, Ext.ux.OSDN.empty(id));
        }, this);
    },

    getDocTypesList : function() {
        return this.docTypesList;
    }
});

