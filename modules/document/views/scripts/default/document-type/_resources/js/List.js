Ext.define('Module.Document.DocumentType.List', {

    extend: 'Ext.ux.grid.GridPanel',

    alias: 'widget.module.document.document-type.list',

    ctCls: 'm-document',

    controller: 'document-type',

    loadMask: true,

    resource: false,

    documentCategoryId: null,

    itemId: null,

    isAddAllowed: null,
    isUpdateAllowed: null,
    isDeleteAllowed: null,

    orderConvF: function() {return true;},

    initComponent: function() {

        this.isAddAllowed = true;
        this.isUpdateAllowed = true;
        this.isDeleteAllowed = true;

        this.tools = [{
            type:'plus',
            tooltip: 'Add form',
            handler: function() {
                this.onCreate();
            },
            scope: this
        }, {
            type:'refresh',
            tooltip: 'Refresh',
            handler: function() {
                this.getStore().load({
                    params: {
                        documentCategoryId: this.documentCategoryId
                    }
                });
            },
            scope: this
        }];

        var self = this;

        this.orderConvF = function(v, rec) {
            var val = (rec.get('categoryOrder') ? parseInt(rec.get('categoryOrder')) : 0) * 1000000 + (rec.get('position') ? parseInt(rec.get('position')) : 0);
            return val;
        };

        Ext.define('Module.Document.DocumentType.Model.List', {
            extend: 'Ext.data.Model',
            fields: [
                {name: 'id'},
                {name: 'name'},
                {name: 'files'},
                {name: 'required'},
                {name: 'abbreviation'},
                {name: 'itemId'},
                {name: 'position', type: 'int'},
                {name: 'documentCategoryId'},
                {name: 'categoryName'},
                {name: 'categoryOrder', type: 'int'},
                {
                    name: 'orderField',
                    mapping: 'position',
                    convert: this.orderConvF,
                    type: 'int',
                    scope: this
                }
            ]
        });

        this.store = new Ext.data.Store({
            model: 'Module.Document.DocumentType.Model.List',
            proxy: {
                actionMethods: {
                    read: 'POST',
                    update: 'POST'
                },
                type: 'ajax',
                url: link('document', 'document-type', 'fetch-all', {format: 'json', etype: this.entityType}),
                simpleSortMode: true,
                reader: {
                    type: 'json',
                    root: 'rowset',
                    totalProperty: 'total'
                }
            },
            remoteSort: false,
            sorters: {
                field: 'orderField',
                direction: 'ASC'
            },
            groupField: 'categoryOrder',
            listeners: {
                load: function(store) {
                    store.sort('orderField', 'ASC');
                },
                scope: this
            }
        });


        var getFilesQtip = function(record) {
            var qtip = '', text = '', files, i = 1 ;
            if (record.get('files')) {
                files = record.get('files');
            }
            if (files && files.length > 0) {
                text = [];
                if (files.length == 1) {
                    text.push(['<b>', lang('Uploaded form:'), '</b>'].join(''));
                } else {
                    text.push(['<b>', lang('Uploaded forms:'), '</b>'].join(''));
                }
                Ext.each(files, function(file) {
                    text.push([i++, ' : ', file.description,' ( ', Ext.util.Format.fileSize(file.size), ' )'].join(''));
                }, this);
                return text.join('<br/>');
            }
            return '';
        }

//        var attachPlugin = new Ext.ux.plugins.grid.ContextMenuActions({
//            autoWidth: true,
//            actionsType: 'inline',
//            header: lang('Actions'),
//            items: [{
//                text: lang('Replaceable'),
//                iconCls: 'icon-document-type-replaceable-16',
//                handler: self.onReplaceableClick.createDelegate(self)
//            }]
//        });

        this.columns = [{
            header: lang('Name'),
            sortable: false,
            menuDisabled : true,
            flex: 1,
            renderer: function(value, metadata, record, rowIndex,  colIndex, store) {
                var qtip = '',  files;
                if (record.get('files')) {
                    files = record.get('files');
                }
                if (files.length > 0) {
                    qtip = ' data-qtip="' + getFilesQtip(record) + '"';
                    return '<div><span  ' + qtip + ' class="osdn-icon-attached-file">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> ' + (value || '&nbsp;') + '</div>';
                } else {
                    return value || '&nbsp;';
                }
            },
            dataIndex: 'name'
        }, {
            header: lang('Abbreviation'),
            dataIndex: 'abbreviation',
            sortable: false,
            width: 80,
            menuDisabled: true
        }, {
            header: lang('Required'),
            dataIndex: 'required',
            sortable: false,
            width: 70,
            menuDisabled: true,
            renderer: function(value, metadata) {
                if (value == 1) {
                    metadata.tdCls = metadata.tdCls + 'x-osdn-grid-cell-checked';
                    metadata.tdAttr = (metadata.tdAttr || "") + " data-qtip='" + lang('Yes') + "'";
                } else {
                    metadata.tdAttr = (metadata.tdAttr || "") + " data-qtip='" + lang('No') + "'";
                }
            }
        }, {
            xtype: 'actioncolumn',
            sortable: false,
            header: '',
            width: 20,
            menuDisabled: true,
            fixed: 1,
            items: [{
                tooltip: lang('Move up'),
                getClass: function(v, metadata, record) {
                    if (0 == record.get('position')) {
                        return '';
                    }
                    return 'osdn-up-16x16 icon-16';
                },
                handler: function(view, rowIndex) {
                    var record = view.getStore().getAt(rowIndex);
                    this.move.call(this, record, 'up');
                },
                scope: this
            }],
            scope: this
        }, {
            xtype: 'actioncolumn',
            sortable: false,
            header: '',
            width: 20,
            menuDisabled: true,
            fixed: 1,
            items: [{
                tooltip: lang('Move down'),
                getClass: function(v, metadata, record) {
                    var max = 0, count = 0;
                    this.getStore().each(function(r) {
                        if (
                               record.get('itemId') == r.get('itemId')
                            && record.get('documentCategoryId') == r.get('documentCategoryId')
                        ) {
                            count++;
                            if (max < r.get('position')) {
                                max = r.get('position');
                            }
                        }
                    }, this);
                    if (count == 1 || (max == record.get('position') && max != 0)) {
                        return '';
                    }
                    return 'osdn-down-16x16 icon-16';
                },
                handler: function(view, rowIndex) {
                    var record = view.getStore().getAt(rowIndex);
                    this.move.call(this, record, 'down');
                },
                scope: this
            }],
            scope: this
        }, {
            xtype: 'actioncolumn',
            sortable: false,
            header: '',
            width: 20,
            menuDisabled: true,
            fixed: 1,
            items: [{
                tooltip: lang('Upload file'),
                getClass: function(v, metadata, record) {
                    if (0 == record.get('required')) {
                        return '';
                    }
                    return 'icon-document-forms-16 icon-16';
                },
                handler: function(view, rowIndex) {
                    var record = view.getStore().getAt(rowIndex);
                    this.onAttachFormClick(record);
                },
                scope: this
            }],
            scope: this
        }, {
            xtype: 'actioncolumn',
            sortable: false,
            header: '',
            width: 20,
            menuDisabled: true,
            fixed: 1,
            items: [{
                tooltip: lang('Replaceable'),
                iconCls: 'icon-document-type-replaceable-16 icon-16',
                handler: function(view, rowIndex) {
                    var record = view.getStore().getAt(rowIndex);
                    this.onReplaceableClick(record);
                },
                scope: this
            }],
            scope: this
        }];

        var groupingFeature = Ext.create('Ext.ux.grid.feature.GroupingOverride',{
            groupHeaderTpl: '<span data-qtip="{[values.rs[0].get("categoryName") != null ? values.rs[0].get("categoryName") : "' + lang('Without category') + '"]}" '
                + ' style="white-space: nowrap; overflow: hidden; display: block;">'
                + '{[values.rs[0].get("categoryName") != null ? values.rs[0].get("categoryName") : "' + lang('Without category') + '"]}'
                + '</span>'
        });

        this.features = [groupingFeature];

        this.callParent(arguments);

        this.on('itemcontextmenu', function(view, record, item, index, e, options) {
             e.stopEvent();
             var contextMenu = this.createContextMenu(record);
             contextMenu.showAt(e.xy);
        });

        this.on('itemdblclick', function(view, record, item, index, e, options) {
            if (this.isUpdateAllowed) {
                this.onUpdate(record);
            }
        }, this);

        this.getStore().on('load', this._normalize, this);

        this.addEvents('beforeupload', 'afterupload');
    },

    createContextMenu: function(record) {
        var items = [];
        var self = this;

        if (this.isUpdateAllowed) {
            items.push({
                text: lang('Edit'),
                iconCls: 'osdn-document-edit',
                handler: function() {
                    self.onUpdate(record);
                }
            });
        }

        if (this.isDeleteAllowed) {
            items.push({
                text: lang('Delete'),
                iconCls: 'osdn-document-delete',
                handler: function(g, rowIndex) {
                    Ext.Msg.show({
                        title: lang('Question'),
                        msg: lang('Are you sure?'),
                        buttons: Ext.Msg.YESNO,
                        fn: function(b) {
                            'yes' == b && this.onDeleteDocumentType(record);
                        },
                        icon: Ext.MessageBox.QUESTION,
                        scope: this
                    }, this);
                },
                scope: this
            });
        }

        if (this.isAddAllowed) {
            items.push("-");
            items.push({
                text: lang('Create'),
                iconCls: 'osdn-document-add',
                handler: function() {
                    this.onCreate(record);
                },
                scope: this
            });
        }

        var m = new Ext.menu.Menu({
            items: items,
            listeners: {
                click: function() {
                    m.hide();
                }
            }
        });

        return m;
    },

    onAttachFormClick: function(record) {

        var id = record.get('id');
        var itemId = record.get('itemId');

        Application.require([
            'file-stock/list/.'
        ], function() {

            var f = new Module.FileStock.List({
                title: lang('Document types forms'),
                width: 600,
                height: 400,
                hideHeaders: true,
                entityType: this.entityType,
                module: 'document',
                controller: 'document-type-file-stock',
                extraParams: {
                    documentTypeId: id,
                    itemId: itemId
                },
                plugins: [Ext.create('Ext.ux.plugins.panel.Window')]
            });

            f.on('completed', function() {
                this.getStore().load();
            }, this);

            f.showInWindow({
                submitBtnCaption: false
            });

        }, this);

    },

    onReplaceableClick: function(record) {
        var id = record.get('id');
        var itemId = record.get('itemId');

        Application.require([
            'document/document-type-replaceable/list'
        ], function() {

            var f = new Module.Document.DocumentTypeReplaceable.List({
                title: lang('Replaceable document types list'),
                width: 600,
                height: 400,
                documentTypeId: id,
                itemId: itemId,
                entityType: this.entityType,
                plugins: [Ext.create('Ext.ux.plugins.panel.Window')]
            });

            f.on('completed', function(id) {
                this.getStore().load();
            }, this);

            f.showInWindow({
                submitBtnCaption: false,
                cls: 'm-document',
                iconCls: 'icon-document-type-replaceable-16'
            });
        }, this);

    },

    download: function(id) {
        location.href = link('document', 'document-type', 'download', {id: id});
    },

    move: function(record, direction) {

        var action = '';
        switch (direction) {
            case 'up':
                action = 'move-up';
            break;

            case 'down':
            default:
                action = 'move-down';
            break;
        }

        var params = {
            id: record.get('id')
        };

        if (record.get('itemId')) {
            params.itemId = record.get('itemId');
        }

        if (record.get('documentCategoryId')) {
            params.documentCategoryId = record.get('documentCategoryId');
        }

        Ext.Ajax.request({
            url: link('document', 'document-type', action, {format: 'json', etype: this.entityType}),
            params: params,
            success: function(response, options) {
                if (direction == 'up') {
                    this.onMoveRowUp(record);
                } else {
                    this.onMoveRowDown(record);
                }
            },
            failure: function(response, options) {
                OSDN.Msg.error(lang('Some error occured'));
            },
            scope: this
        });
    },

    onCreate: function(record) {

        var documentCategoryId = this.documentCategoryId;
        var itemId = this.itemId;
        if (record) {
            documentCategoryId = record.get('documentCategoryId');
            itemId = record.get('itemId');
        }

        Application.require([
            'document/document-type/form',
        ], function() {
            var mcf = new Module.Document.DocumentType.Form({
                itemId: itemId,
                width: 400,
                height: 300,
                documentCategoryId: documentCategoryId,
                entityType: this.entityType,
                controller: this.controller,
                title: lang('Create new document type'),
                plugins: [Ext.create('Ext.ux.plugins.panel.Window')]
            });

            mcf.on('completed', function(id) {
                this.setLastInsertedId(id);
                this.getStore().load({
                    params: {
                        documentCategoryId: this.documentCategoryId
                    }
                });
            }, this);

            mcf.showInWindow();
        }, this);

    },

    onUpdate: function(record) {
        var id = record.get('id');
        var documentCategoryId = record.get('documentCategoryId');
        var itemId = record.get('itemId');
        var name = record.get('name');

        Application.require([
            'document/document-type/form',
        ], function() {
            var mcf = new Module.Document.DocumentType.Form({
                documentTypeId: id,
                itemId: itemId,
                width: 400,
                height: 300,
                documentCategoryId: documentCategoryId,
                entityType: this.entityType,
                controller: this.controller,
                title: lang('Edit document type {0}', name),
                plugins: [Ext.create('Ext.ux.plugins.panel.Window')]
            });

            mcf.on('completed', function(id) {
                this.setLastInsertedId(id);
                this.getStore().load({
                    params: {
                        documentCategoryId: this.documentCategoryId
                    }
                });
            }, this);

            mcf.showInWindow();
        }, this);
    },

    changeCategory: function(id, documentCategoryId) {
        var failure = function() {
            OSDN.Msg.error(lang('Saving document type failed. Try again.'));
            this.fireEvent('failureUpdate');
        };

        Ext.Ajax.request({
            url: link('document', 'document-type', 'update', {format: 'json', etype: this.entityType}),
            params: {
                id: id,
                documentCategoryId: documentCategoryId
            },
            success: function(response, options) {
                var data = Ext.decode(response.responseText);
                if (true !== data.success) {
                    failure.call(this);
                    return;
                }
                this.fireEvent('update');
                this.load();
            },
            failure: failure.createDelegate(this),
            scope: this
        });
    },

    _normalize: function() {
        var position = {};

        this.getStore().each(function(record) {
            var cId = record.get('documentCategoryId') ? record.get('documentCategoryId') : 0;
            var iId = record.get('itemId') ? record.get('itemId') : 0;
            if (!position[cId]) {
                position[cId] = {};
            }
            if (!position[cId][iId]) {
                position[cId][iId] = 0;
            }

            if (position[cId][iId] != record.get('position')) {
                record.set('position', position[cId][iId]);
            }
            position[cId][iId]++;
        });
    },

    onMoveRowUp: function(record) {
        this._normalize();
        if (0 == record.get('position')) {
            return false;
        }
        var index = this.getStore().findBy(function(r) {
            if (record.get('documentCategoryId') == r.get('documentCategoryId')
                && record.get('itemId') == r.get('itemId')
                && record.get('position') - 1 == r.get('position')) {
                    return true;
                }
        }, this);

        var orderValue = 0;
        if (index >= 0) {
            var placeRecord = this.getStore().getAt(index);
            placeRecord.set('position', placeRecord.get('position') + 1);
            orderValue = this.orderConvF(0, placeRecord);
            placeRecord.set('orderField', orderValue);
        }
        record.set('position', record.get('position') - 1);
        orderValue = this.orderConvF(0, record);
        record.set('orderField', orderValue);
        this.getStore().sort('orderField', 'ASC');
    },

    onMoveRowDown: function(record) {
        this._normalize();
        var max = 0;
        this.getStore().each(function(record) {
            if (record.get('position') > max) {
                max = record.get('position');
            }
        }, this);

        if (max == record.get('position')) {
            return false;
        }

        var index = this.getStore().findBy(function(r) {
            if (record.get('documentCategoryId') == r.get('documentCategoryId')
                && record.get('itemId') == r.get('itemId')
                && record.get('position') + 1 == r.get('position')) {
                    return true;
                }
        }, this);

        var orderValue = 0;
        if (index >= 0) {
            var placeRecord = this.getStore().getAt(index);
            placeRecord.set('position', placeRecord.get('position') - 1);
            orderValue = this.orderConvF(0, placeRecord);
            placeRecord.set('orderField', orderValue);
        }

        record.set('position', record.get('position') + 1);
        orderValue = this.orderConvF(0, record);
        record.set('orderField', orderValue);
        this.getStore().sort('orderField', 'ASC');
    },

    load: function(p) {
        Ext.apply(this, p);
        this.getStore().load({
            params: {
                documentCategoryId:    this.documentCategoryId,
                itemId:        this.itemId
            }
        });
    },

    dummyFunc: function() {
        OSDN.Msg.info('In progress...');
    },

    onDeleteDocumentType: function(record) {
        Ext.Ajax.request({
            url: link('document', 'document-type', 'delete', {format: 'json', etype: this.entityType}),
            params: {
                id: record.get('id')
            },
            success: function(response) {
                var decResponse = Ext.decode(response.responseText);
                if (decResponse && decResponse.success) {
                    Application.notificate(decResponse.messages);
                    this.load();
                    return;
                }

                Application.notificate(decResponse.messages || lang('Unable to delete document type'));
            },
            scope: this
        }, this);
    }

});
