Ext.define('Module.Document.DocumentType.Form', {

    extend: 'Ext.form.Panel',

    alias: 'widget.module.document.document-type.form',

    layout: 'anchor',
    defaults: {
        anchor: '100%'
    },

    autoHeight: true,

    window: null,

    bodyPadding: 5,

    documentTypeId: null,

    documentCategoryId: null,

    itemId: null,

    entityType: null,

    initComponent: function() {

        if (null === this.entityType) {
            throw 'Not defined module';
        }

        var o = {etype: this.entityType};
        if (this.documentTypeId) {
            o.id = this.documentTypeId;
        }

        this.items = [{
            xtype: 'textfield',
            fieldLabel: lang('Name'),
            name: 'name',
            anchor: '-1',
            allowBlank: false,
            enableKeyEvents: true
        }, {
            xtype: 'textfield',
            fieldLabel: lang('Abbreviation'),
            name: 'abbreviation',
            anchor: '-1',
            allowBlank: true,
            enableKeyEvents: true
        },{
            xtype: 'htmleditor',
            labelAlign: 'top',
            fieldLabel: lang('Question'),
            name: 'question',
            allowBlank: true
        }, {
            xtype: 'checkboxfield',
            boxLabel  : 'Expire date required',
            name      : 'expireDateRequired',
            inputValue: '1',
            anchor: '-1',
            id        : 'expireDateRequired'
        }, {
            xtype: 'checkboxfield',
            boxLabel  : 'Required',
            name      : 'required',
            inputValue: '1',
            anchor: '-1',
            id        : 'required'
        }];

        this.callParent(arguments);

        this.addEvents(
            /**
             * Fires when comment is created
             *
             * @param {Module.Document.DocumentType.Form}
             */
            'completed'
        );

        this.on('render', this.doLoad, this);

    },

    doLoad: function() {

        if (!this.documentTypeId) {
            return;
        }

        this.getForm().load({
            url: link('document', 'document-type', 'form', {format: 'json'}),
            method: 'POST',
            params: {
                id: this.documentTypeId,
                etype: this.entityType
            },
            waitMsg: lang('Loading...'),
            scope: this
        });
    },

    onSubmit: function(panel, w) {

        if (!this.form.isValid()) {
            return;
        }

        var params = {};
        var action = 'create';
        if (this.documentTypeId) {
            params['id'] = this.documentTypeId;
            action = 'update';
        }

        params['itemId'] = this.itemId;
        params['documentCategoryId'] = this.documentCategoryId;

        this.form.submit({
            url: link('document', 'document-type', action, {format: 'json', etype: this.entityType}),
            method: 'post',
            params: params,
            success: function(form, action) {
                var responseObj = Ext.decode(action.response.responseText);
                Application.notificate(responseObj.messages);

                if (action.result.success) {
                    this.fireEvent('completed', responseObj.documentTypeId);
                    this.window.close();
                }
            },
            scope: this
        });
    }

});