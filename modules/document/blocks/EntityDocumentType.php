<?php

class Document_Block_EntityDocumentType extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Entity');
    }
}