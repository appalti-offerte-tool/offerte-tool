<?php

class Document_Block_Document extends \Application_Block_Abstract
{
    protected $_icon = 'icon-comment-16';

    protected function _toTitle()
    {
        return $this->view->translate('Documents');
    }
}