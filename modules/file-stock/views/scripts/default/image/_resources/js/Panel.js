Ext.define('Module.FileStock.Image.Panel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.module.file-stock.image.panel',

    layout: 'anchor',

    module: null,
    controller: null,

    fileStockId: null,
    entityId: null,
    entityName: null,

    autoLoadRecords: false,
    onImageClickHandle: true,
    useUploadFormField: false,
    field: null,

    cachable: true,

    /**
     * @param {Ext.Img}
     */
    image: null,

    initComponent: function() {

        this.image = new Ext.Img({
            src: 'http://placehold.it/150x150',
            style: {
                display: 'block',
                margin: '0 auto',
                width: "auto",
                height: (this.height - 55) + 'px'
            }
        })

        this.items = [this.image];

        if (this.useUploadFormField) {
            this.field = new Ext.form.field.File(Ext.apply({
                emptyText: 'Choose...',
                fieldLabel: 'Photo',
                name: 'photo-path',
                buttonText: '',
                labelAlign: 'top',
                anchor: '100%',
                buttonConfig: {
                    iconCls: 'upload-icon'
                }
            }, this.field || {}));

            this.items.push(this.field);
        }

        if (true === this.autoLoadRecords) {
            this.doLoad();
        }

        this.callParent();

        if (!this.disabled && this.onImageClickHandle) {
            this.on('render', function() {
                this.getEl().on({
                    contextclick: this.onContextMenuImage,
                    click: this.onShowFileStockDialogue,
                    scope: this
                });
            }, this);
        }
    },

    setImageLink: function(link) {
        if (false === this.cacheable) {
            var filter = /([\?])+/;
            link += filter.test(link) ? '&' : '?';
            link += '_dc=' + Math.random();
        }
        this.image.setSrc(link);
        return this;
    },

    doLoad: function () {

//        Ext.Ajax.request({
//            url: link(this.module, this.controller, 'load', {format: 'json'}),
//            params: {
//                entityId: this.entityId,
//                id: this.iconId
//            },
//            success: function (response, options) {
//                var decResponse = Ext.decode(response.responseText);
//
//                if (res.success && res.rowset && res.rowset.length > 0) {
//                    var imData = res.rowset[0];
//                    this.iconId = imData.id;
//                    this.imData = imData;
//                    this.updateImage.call(this, imData);
//                } else {
//                    this.imData = {};
//                    this.updateImage.call(this);
//                }
//
//            },
//            failure: function() {
//                this.imData = {};
//            },
//            callback: function () {
//                this.getEl().unmask();
//            },
//            scope: this
//        });
    },

    onContextMenuImage: function (e) {
//        e.stopEvent();
//        var items = [];
//        if (this.mode == 'normal') {
//            items.push({
//                iconCls: 'osdn-select-image',
//                text: lang('Select image'),
//                handler: this.showImages,
//                scope: this
//            });
//        }
//
//        if (this.iconId && this.imData && this.imData.thumbexist) {
//            items.push({
//                iconCls: 'osdn-show-image',
//                text: lang('Show image'),
//                handler: this.showImage,
//                scope: this
//            });
//            if (this.mode == 'normal') {
//                items.push('-');
//                items.push({
//                    iconCls: 'delete-image',
//                    text: lang('Delete image'),
//                    handler: this.doDelete,
//                    scope: this
//                });
//            }
//        }

        var menu = new Ext.menu.Menu({
            items: [{
                text: lang('Assign new image'),
                handler: this.onShowFileStockDialogue,
                scope: this
            }]
        });

        menu.showAt(e.getXY());
    },

    onShowFileStockDialogue: function() {

        Application.require([
            'file-stock/./form'
        ], function() {

            var bp = {
                fileStockId: this.fileStockId
            };
            bp[this.entityName] = this.entityId;

            var w = new Module.FileStock.Form({
                module: this.module,
                controller: this.controller,
                baseParams: bp
            });
            w.show();

            w.on('update-file', function(btn, decResponse) {

                this.setFileStockId(decResponse.fileStockId || this.fileStockId, true);
                w.close();
            }, this);

        }, this);
    },

    setFileStockId: function(id, forceReload) {
        this.fileStockId = id;

        if (true === forceReload && this.fileStockId) {

            var o = {
                fileStockId: this.fileStockId,
                _dc: Math.random()
            };

            o[this.entityName] = this.entityId;
            var path = link(this.module, this.controller, 'view', o);

            this.image.setSrc(path);
        }

        return this;
    }
});

//
//    showImages: function () {
//        var uIO = new OSDN.Images.List({
//            loadUrl: this.loadUrl,
//            updateUrl: this.uploadUrl,
//            deleteUrl: this.deleteUrl,
//            baseParams: {
//                entityId: this.entityId,
//                width: 125,
//                height: 125
//            },
//            additionalContextItems: [{
//                iconCls: 'osdn-select-avatar',
//                text: lang('Select avatar'),
//                handler: function () {
//                    var selNodes = uIO.getView().getSelectedNodes();
//                    if (selNodes && selNodes.length > 0) {
//                        uIO.fireEvent('selected', selNodes[0], uIO);
//                    }
//                },
//                scope: uIO
//            }, '-']
//        });
//        var w = uIO.showInWindow();
//        uIO.on('selected', function (data, uIO) {
//            this.setIcon.call(this, data.id);
//            w.close();
//        }, this, {delay: 300});
//
//        uIO.on('deleted', function(data) {
//            if (data && data.id == this.iconId) {
//                this.doLoad.call(this);
//            }
//        }, this);
//    },
//
//    setIcon: function(id) {
//        Ext.Ajax.request({
//            url: this.setIconUrl,
//            params: {
//                id: id,
//                entityId: this.entityId
//            },
//            success: function (response, options) {
//                var res = Ext.decode(response.responseText);
//                if (res.success) {
//                    this.iconId = id;
//                    this.doLoad();
//                } else {
//                    OSDN.Msg.error('Error occured.');
//                }
//            },
//            failure: function() {
//                OSDN.Msg.error('Error occured.');
//            },
//            scope: this
//        });
//    },
//
//    showImage: function (imageData) {
//        imageData = this.imageData || {};
//        if (imageData && imageData.imagepath && imageData.exist) {
//            var width = parseInt(imageData.width <= 900 ? imageData.width : 900) + 12;
//            var w = new Ext.Window({
//                autoHeight: true,
//                width: width,
//                border: false,
//                resizable: false,
//                items: {
//                    xtype: 'panel',
//                    width: imageData.width <= 900 ? imageData.width : 900,
//                    height: imageData.height <= 700 ? imageData.height : 700,
//                    html: [
//                        '<img class="osdn-image" src="'
//                        + imageData.imagepath
//                        + '?rand=' + Math.random()
//                        + '" border = 0 '
//                        + '>'
//                    ].join('')
//                }
//            });
//
//            w.on('render', function() {
//                w.getEl().child('img').on('click', function() {
//                    w.close();
//                }, this);
//                w.center();
//            }, this, {delay: 200});
//
//            w.show();
//        }
//    },
//
//    doDelete: function () {
//        Ext.Msg.show({
//            title: lang('Question!'),
//            msg: lang('Are you sure?'),
//            buttons: Ext.Msg.YESNO,
//            fn: function(b){
//                if ('yes' == b) {
//                    Ext.Ajax.request({
//                        url: this.deleteUrl,
//                        params: {
//                            id: this.iconId,
//                            entityId: this.entityId,
//                            icon: 1
//                        },
//                        success: function (response, options) {
//                            var res = Ext.decode(response.responseText);
//                            if (res.success) {
//                                this.iconId = null;
//                                this.updateImage();
//                            } else {
//                                OSDN.Msg.error('Error occured.');
//                            }
//                        },
//                        failure: function() {
//                            OSDN.Msg.error('Error occured.');
//                        },
//                        scope: this
//                    });
//                }
//            },
//            scope: this
//        });
//    },
//
//
//
//    updateImage: function (imageData) {
//        var el = this.imagePanel.getEl();
//        var callback = function() {
//            if (el.child('img')) {
//                el.child('img').on('click', function() {
//                    if (this.mode == 'normal') {
//                        this.showImages();
//                    } else {
//                        this.showImage(imageData);
//                    }
//                }, this);
//                el.child('img').on('contextmenu', this.onContextMenu, this);
//            }
//        }.createDelegate(this);
//        if (imageData && imageData.thumbpath && imageData.thumbexist) {
//            this.imageData = imageData;
//            this.imagePanel.setHeight(imageData.thumbheight);
//            el.update([
//                '<img class="osdn-avatar" src="'
//                + imageData.thumbpath
//                + '?rand=' + Math.random()
//                + '" border = 0 '
//                + ' width = "'+ imageData.thumbwidth
//                + '">'
//            ].join(''), false, callback);
//        } else if (this.noAvatarPath) {
//            this.imageData = false;
//            this.imagePanel.setHeight(this.iconConfig.height);
//            el.update([
//                '<img class="osdn-noavatar" src="'
//                + this.noAvatarPath
//                + '?rand=' + Math.random()
//                + '" border = 0 '
//                + ' width = "'+ this.iconConfig.width + '"'
//                + '>'
//            ].join(''), false, callback);
//        } else {
//            el.update('', false, callback);
//        }
//    },
//
//    _parameters: function () {
//        var params  = {
//            icon: 1,
//            width: this.iconConfig.width,
//            height: this.iconConfig.height,
//            type: this.iconConfig.type,
//            entityId: this.entityId
//        };
//        if (this.iconId) {
//            params.id = this.iconId;
//        }
//        return params;
//    }
//});
