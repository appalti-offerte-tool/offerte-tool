Ext.define('Module.FileStock.Image.Layout', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.module.file-stock.image.layout',

    layout: 'border',
    border: false,
    cls: 'img-chooser-dlg',

    listView: null,
    imageView: null,

    entityName: null,
    entityId: null,
    extraParams: null,

    module: null,
    controller: null,

    enablePickerContextMenu: false,

    thumbTemplate: [
        '<tpl for=".">',
            '<div class="thumb-wrap" osdn:img-id="{id}">',
                '<div class="thumb"><img src="{thumbnailLink}" title="{name}"></div>',
                '<span>{shortName}</span>',
                '<tpl if="isDefaultThumbnail == 1"><div class="thumb-icon"></div></tpl>',
            '</div>',
        '</tpl>'
    ],

    detailsTemplate: [
        '<div class="details">',
            '<tpl for=".">',
                '<img src="{link}">',
                '<div class="details-info">',
                    '<b>Name:</b>',
                    '<span>{name}</span>',
                    '<b>Size:</b>',
                    '<span>{sizeString}</span>',
                    '<b>Last Modified:</b>',
                    '<span>{dateString}</span>',
                '</div>',
            '</tpl>',
        '</div>'
    ],

    initComponent: function() {

        this.lookup = {};
        this.defaults = {
            split: true
        };

        if (null === this.extraParams) {
            this.extraParams = {};
        }
        this.extraParams[this.entityName] = this.entityId;
        var p = Ext.clone(this.extraParams);

        Ext.define('Module.FileStock.Model.Image', {
            extend: 'Ext.data.Model',
            fields: [
                'id', 'name', 'link', 'thumbnailLink',
                {name: 'isDefaultThumbnail', type: 'boolean'},
                {name:'size', type: 'float'},
                {name:'created', type: 'date', dateFormat: 'Y-m-d H:i:s'},
                {name:'modified', type: 'date', dateFormat: 'Y-m-d H:i:s'}
            ]
        });

        this.store = new Ext.data.Store({
            model: 'Module.FileStock.Model.Image',
            proxy: {
                type: 'ajax',
                url: link(this.module, this.controller, 'fetch-all', {format: 'json'}),
                extraParams: p,
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        if (this.entityId) {
            this.getStore().getProxy().extraParams[this.entityName] = this.entityId;
            this.getStore().load();
        }

        this.dw = Ext.widget('dataview', {
            tpl: this.getTpl('thumbTemplate'),
            singleSelect: true,
            trackOver: true,
            overItemCls: 'x-view-over',
            itemSelector: 'div.thumb-wrap',
            emptyText : '<div style="padding:10px;">No images found</div>',
            store: this.getStore(),
            prepareData: Ext.bind(this.onPrepareViewData, this)
        });

        this.imageView = new Ext.Panel({
            region: 'east',
            width: 250,
            autoScroll: true
        });

        this.listView = new Ext.Panel({
            cls: 'img-chooser-view',
            autoScroll: true,
            region: 'center',
            tbar: [{
                text: lang('Create'),
                handler: this.onBeforeCreateImage,
                iconCls: 'icon-create-16',
                scope: this
            }],
            bbar: [{
                text: lang('Refresh'),
                iconCls: 'x-tbar-loading',
                handler: function() {
                    this.getStore().load();
                },
                scope: this
            }],
            items: [this.dw]
        });

        this.items = [this.listView, this.imageView];

        this.callParent(arguments);

        this.store.on('load', function() {
            this.dw.select(0);
        }, this);

        this.dw.on({
            loadexception: this.onLoadException,
            beforeselect: function(view){
                return view.store.getRange().length > 0;
            },
            itemcontextmenu: this.onItemContextMenu,
            itemdblclick: this.onItemDblClick,
            scope: this
        });
        this.dw.on('selectionchange', this.onChangeImageSelection, this, {buffer:100});

        this.addEvents('choose-image');
    },

    onChangeImageSelection: function(dwm, records, eOpts) {

        var de = this.imageView.body;

        if (records.length > 0) {
            var record = records[0];
            var data = this.lookup[record.get('id')];

            de.hide();
            this.getTpl('detailsTemplate').overwrite(de, data);
            de.slideIn('l', {stopFx:true,duration: 0.1});
        } else {
            de.update('');
        }
    },

    onBeforeCreateImage: function() {
        Application.require([
            'file-stock/./form'
        ], function() {

            if (null === this.extraParams) {
                this.extraParams = {};
            }
            this.extraParams[this.entityName] = this.entityId;
            var p = Ext.clone(this.extraParams);

            var w = new Module.FileStock.Form({
                baseParams: p,
                module: this.module,
                controller: this.controller
            });

            w.on('update-file', function(form, decResponse) {
                this.setEntityId(decResponse.entityId, true);
                w.close();
            }, this);

            w.show();
        }, this);
    },

    onUpdateImage: function(record) {

        Application.require([
            'file-stock/./form'
            ], function() {

                if (null === this.extraParams) {
                    this.extraParams = {};
                }
                this.extraParams[this.entityName] = this.entityId;
                var p = Ext.clone(this.extraParams);

                var w = new Module.FileStock.Form({
                    baseParams: p,
                    fileStockId: record.get('id'),
                    module: this.module,
                    controller: this.controller
                });

                w.on('update-file', function(form, decResponse) {
                    w.close();
                }, this);

                w.show();
            }, this);
    },

    setEntityId: function(entityId, forceReload) {
        this.entityId = entityId;
        this.getStore().getProxy().extraParams[this.entityName] = this.entityId;
        if (true === forceReload) {
            this.getStore().load();
        }

        this.fireEvent('entity-change', this, entityId);
    },

    onLoadException : function(v,o){
        this.dw.getEl().update('<div style="padding:10px;">Error loading images.</div>');
    },

    getStore: function() {
        return this.store;
    },

    onDeleteImage: function(record) {

        var params = this.baseParams || {};
        params[this.entityName] = this.entityId;
        params.fileStockId = record.get('id');

        Ext.Ajax.request({
            url: link(this.module, this.controller, 'delete', {format: 'json'}),
            params: params,
            success: function(response, options) {
                var decResponse = Ext.decode(response.responseText);
                if (!decResponse.success) {
                    Application.notificate(decResponse.messages);
                    return;
                }

                this.getStore().load();
            },
            scope: this
        });
    },

    onSetDefaultThumbnailImage: function(record) {

        var params = {};
        params[this.entityName] = this.entityId;
        params.fileStockId = record.get('id');
        Ext.Ajax.request({
            url: link(this.module, this.controller, 'set-default-thumbnail', {format: 'json'}),
            params: Ext.apply(this.baseParams || {}, params),
            success: function(response, options) {
                var decResponse = Ext.decode(response.responseText);
                if (!decResponse.success) {
                    Application.notificate(decResponse.messages);
                    return;
                }

                this.getStore().load();
            },
            scope: this
        });
    },

    onPrepareViewData: function(data) {

        var fn = Ext.util.Format;
        data.shortName = fn.ellipsis(data.name, 15);
        data.sizeString = fn.fileSize(data.size);

        data.dateString = '1111111';//  new Date(data.modified || data.created).format("m/d/Y g:i a");
        this.lookup[data.id] = data;
        return data;
    },

    onItemContextMenu: function(dw, record, htmle, index, e, eOpts) {

        e.stopEvent();

        var o = [];
        if (true === this.enablePickerContextMenu) {
            o.push({
                text: lang('Insert image'),
                iconCls: 'icon-insert-image-16',
                handler: function() {
                    this.fireEvent('choose-image', this, record, record.get('link'));
                },
                scope: this
            });
        }

        o = o.concat([{
            text: lang('Set as default thumbnail'),
            iconCls: 'icon-thumb-16',
            handler: function() {
                this.onSetDefaultThumbnailImage(record);
            },
            scope: this
        }, '-', {
            text: lang('Edit'),
            iconCls: 'icon-update-16',
            handler: function() {
                this.onUpdateImage(record);
            },
            scope: this
        }, {
            text: lang('Delete'),
            iconCls: 'icon-delete-16',
            handler: function() {
                this.onDeleteImage(record);
            },
            scope: this
        }]);

        var m = new Ext.menu.Menu({
            items: o
        });

        m.showAt(e.getXY());
    },

    onItemDblClick: function(dw, record, ehtml, index, e) {
        this.fireEvent('choose-image', this, record, record.get('link'));
    }
});