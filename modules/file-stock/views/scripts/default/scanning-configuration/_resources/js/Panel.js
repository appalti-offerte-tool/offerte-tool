Ext.ns('Module.FileStock.ScanningConfiguration');

Module.FileStock.ScanningConfiguration.Panel = Ext.extend(Ext.Panel, {

    layout: 'fit',

    accountable: false,

    form: null,

    initComponent: function(){

        this.autoLoad = {
            url: link('file-stock', 'scanning-configuration', 'form'),
            callback: function() {
                this.form = new Ext.ux.form.FormMarkup('scanning-configuration-form');
            },
            scope: this,
            method: 'GET',
            nocache: true
        };

        this.resetBtn = new Ext.Button({
            text: lang('Reset'),
            iconCls: 'icon-reset-16',
            handler: function() {
                this.form.getForm().reset();
            },
            scope: this
        });

        this.submitBtn = new Ext.Button({
            text: lang('Save'),
            iconCls: 'icon-save-16',
            handler: this.doSave,
            scope: this
        });

        this.bbar = ['->', this.resetBtn, this.submitBtn];

//        var ScanObject = new Module.FileStock.Scanning.DinamicWebTwain({
//            hiddenRender: true
//        });
//
//        this.items = [this.form];
        Module.FileStock.ScanningConfiguration.Panel.superclass.initComponent.apply(this, arguments);

//        this.on('destroy', function (){
//            ScanObject.remove();
//        }, this);

//        this.form.on('ready', function () {
//
//            var scanners = ScanObject.getScanners.call(ScanObject);
//
//            var sourceField = this.form.getForm().findField('source');
//            if (OSDN.empty(scanners)) {
//                sourceField.getStore().loadData([['0', lang('Cannot detect any scanners!')]]);
//                sourceField.setValue('0');
//                return;
//            }
//
//            var sStore = [];
//            for (i in scanners) {
//                sStore.push([i, scanners[i]]);
//            }
//
//            if (sStore.length == 0) {
//                scannerStore.loadData([['0', lang('Cannot detect any scanners!')]]);
//                Ext.defer(function () {
//                    this.scannerCombo.setValue('0');
//                }), 2500, this);
//            } else {
//                sourceField.getStore().loadData(sStore);
//            }
//
//        }, this);
//
//        this.form.on('ready', this.doLoad, this);
//
//        this.addEvents('save', 'load');
    },

    doLoad: function() {

        this.form.getForm().load({
            waitMsg: lang('Loading...'),
            url: link(this.accountable ? 'accounts' : 'admin', 'configuration', 'do-load'),
            params: {
                type: this.pid
            },
            callback: function() {
                this.fireEvent('load', this);
            },
            scope: this
        });
    },

    doSave: function() {

        this.form.getForm().submit({
            url: link(this.accountable ? 'accounts' : 'admin', 'configuration', 'do-update'),
            waitMsg: lang('Saving...'),
            params: {
                type: this.pid
            },
            success: function() {
                this.fireEvent('save', this);
            },
            scope: this
        });
    },

    showInWindow: function() {

        var w = new Ext.Window({
            iconCls: 'osdn-scanner-configuration',
            title: lang('Scanning preferences'),
            width: 600,
            autoHeight: true,
            items: [this],
            modal: true
        });

        w.show(false, function() {
            w.center();
        });

        return w;
    }
});

Ext.reg('module.file-stock.scanning-configuration.panel', 'Module.FileStock.ScanningConfiguration.Panel');