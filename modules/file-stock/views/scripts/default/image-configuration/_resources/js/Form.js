Ext.ns('Module.FileStock.ImageConfiguration');

Module.FileStock.ImageConfiguration.Form = Ext.extend(Ext.Panel, {

    layout: 'fit',

    accountable: false,

    form: null,

    initComponent: function(){

        this.form = new Ext.ux.form.FormMarkup('image-configuration-form');

        this.resetBtn = new Ext.Button({
            text: lang('Reset'),
            iconCls: 'icon-reset-16',
            handler: function() {
                this.form.getForm().reset();
            },
            scope: this
        });

        this.submitBtn = new Ext.Button({
            text: lang('Save'),
            iconCls: 'icon-save-16',
            handler: this.doSave,
            scope: this
        });

        this.bbar = ['->', this.resetBtn, this.submitBtn];

        Module.FileStock.ImageConfiguration.Form.superclass.initComponent.apply(this, arguments);
    },

    doLoad: function() {

        this.form.getForm().load({
            waitMsg: lang('Loading...'),
            url: link(this.accountable ? 'accounts' : 'admin', 'configuration', 'do-load'),
            params: {
                type: this.pid
            },
            callback: function() {
                this.fireEvent('load', this);
            },
            scope: this
        });
    },

    doSave: function() {

        this.form.getForm().submit({
            url: link(this.accountable ? 'accounts' : 'admin', 'configuration', 'do-update'),
            waitMsg: lang('Saving...'),
            params: {
                type: this.pid
            },
            success: function() {
                this.fireEvent('save', this);
            },
            scope: this
        });
    }
});

Ext.reg('module.file-stock.scanning-configuration.panel', 'Module.FileStock.ImageConfiguration.Panel');