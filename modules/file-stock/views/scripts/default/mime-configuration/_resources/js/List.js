Ext.define('Module.FileStock.Model.MimeConfiguration', {
    extend: 'Ext.data.Model',
    fields: ['mime-type', 'extension', 'isUsed']
});


Ext.define('Module.FileStock.MimeConfiguration.List', {
    extend: 'Ext.ux.grid.GridPanel',

    stateful: true,

    baseRelativeURL: null,

    identity: null,

    initComponent: function() {

        this.stateId = 'module.file-stock.mime.list';

        this.store = new Ext.data.JsonStore({
            model: 'Module.FileStock.Model.MimeConfiguration',
            proxy: {
                type: 'ajax',
                url: link('file-stock', 'mime-configuration', 'fetch-complete-list', {format: 'json'}),
                extraParams: {
                    identity: this.identity
                },
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.columns = [{
            header: 'Mime type',
            flex: 1,
            sortable : true,
            dataIndex: 'mime-type'
        }, {
            header: lang('Extension'),
            sortable : true,
            dataIndex: 'extension'
        }, {
            header: 'Icon',
            sortable: false,
            width: 50,
            renderer: function(v, md, record) {
                md.attr = 'style="background: url(' + this.baseRelativeURL + '/_resources/images/' + record.get('extension') + '.png' + ') no-repeat 50% 50%;"'
            },
            scope: this
        }, {
            xtype: 'checkcolumn',
            header: lang('Used'),
            dataIndex: 'isUsed',
            width: 50,
            listeners: {
                checkchange: function(cc, rowIndex, checked, record) {
                    checked ? this.onAttach(record) : this.onDetach(record);
                },
                scope: this
            }
        }];

        this.features = [{
            ftype: 'filters'
        }];

        this.tbar = [];
        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 2
        })];

        this.bbar = [{
            text: lang('Refresh'),
            iconCls: 'icon-refresh-16',
            qtip: lang('Refresh'),
            handler: function() {
                this.getStore().load();
            },
            scope: this
        }]

        this.callParent();
    },

    onAttach: function(record) {
        Ext.Ajax.request({
            url: link('file-stock', 'mime-configuration', 'attach', {format: 'json'}),
            method: 'POST',
            params: {
                extension: record.get('extension'),
                identity: this.identity
            },
            success: function(response, options) {
                var decResponse = Ext.decode(response.responseText);
                Application.notificate(decResponse.messages);
                if (decResponse.success) {
                    record.commit();
                } else {
                    record.reject();
                }
            }
        });
    },

    onDetach: function(record) {
        Ext.Ajax.request({
            url: link('file-stock', 'mime-configuration', 'detach', {format: 'json'}),
            method: 'POST',
            params: {
                extension: record.get('extension'),
                identity: this.identity
            },
            success: function(response, options) {
                var decResponse = Ext.decode(response.responseText);
                Application.notificate(decResponse.messages);
                if (decResponse.success) {
                    record.commit();
                } else {
                    record.reject();
                }
            }
        });
    }
});