Ext.define('Module.FileStock.Form', {

    extend: 'Ext.window.Window',
    alias: 'widget.module.file-stock.form',

    module: null,
    controller: null,

    width: 480,

    modal: true,

    fileStockId: null,

    buttonAlign: "left",

    baseParams: null,

    permissions: true,

    _uploadMask: null,

    _fileInfo: null,

    autoCloseAfterUpload: true,

    fields: null,
    subform: null,

    downloadLinkPanel: null,

    browseBtn: null,

    initComponent: function() {

        if (Ext.isIE || Ext.isGecko) {
//            this.tools = [{
//                id: 'gear',
//                qtip: lang('Scanner settings'),
//                handler: this.openScanConfiguration,
//                scope: this
//            }];
        }

        if (!Ext.isObject(this.baseParams)) {
            this.baseParams = {};
        }

        if (this.baseParams.fileStockId) {
            this.fileStockId = this.baseParams.fileStockId;
        }

        var fields = [
            'id', 'description', 'name', 'type', 'size'
        ];
        if (null != this.fields) {
            fields = fields.concat(this.fields);
        }

        var items = [{
            xtype: 'textarea',
            fieldLabel: lang('File description'),
            name: 'description',
            anchor: "-24",
            enableKeyEvents: true,
            listeners:{
                keyup: this.disableEnableButtons,
                scope: this
            }
        }];
        if (null != this.subform) {
            items = items.concat(this.subform);
        }

        Ext.define('Module.FileStock.Model.FileStock', {
            extend: 'Ext.data.Model',
            fields: fields
        });

        this.form = new Ext.form.Panel({
            autoScroll: false,
            border: false,
            padding: '5px',
            autoHeight: true,
            labelAlign: 'top',
            items: items,
            reader: Ext.create('Ext.data.reader.Json', {
                model: 'Module.FileStock.Model.FileStock',
                root: 'data'
            })
        });

        this.items = [
            this.form
        ];

        if (this.fileStockId) {
            this.title = lang('File changing');
        } else {
            this.title = lang('Adding file');
        }

        this.buttons = [];

//        if (Ext.isIE || Ext.isGecko) {
//            this.buttons.push({
//                iconCls : 'osdn-scanner-configuration',
//                tooltip: lang('Scanner settings'),
//                text: '',
//                minWidth: 16,
//                handler: this.openScanConfiguration,
//                scope: this
//            });
//            this.buttons.push(new Ext.Toolbar.Fill({own: true}));
//
//            this.scanBtn = new OSDN.Scan.Button({
//                url: scope.scanUrl,
//                text: lang('Scan'),
//                parameters: scope._parameters.createDelegate(scope),
//                iconCls: 'osdn-scan',
//                listeners: {
//                    uploaded: function(button, response, errorCode, errorString) {
//                        var res = Ext.decode(response);
//                        var rr = {
//                            responseText: response
//                        };
//                        if (res.success) {
//                            scope.fireEvent('updateFile', scope, rr);
//                            return;
//                        }
//                        evalErrors(res, response);
//                    },
//                    ready : this.disableEnableButtons.createDelegate(this)
//                }
//            });
//            this.buttons.push(this.scanBtn);
//        }

        this.downloadLinkPanel = new Ext.Panel({
            html: '',
            border: false,
            hidden: true
        });
        this.buttons.push(this.downloadLinkPanel);

        this.browseBtn = new Module.FileStock.Button({
            module: this.module,
            controller: this.controller,
            fileStockId: this.fileStockId,
            getBaseParams: Ext.bind(this.getbaseParamsWithFormValues, this),
            isFileStockCreate: Ext.bind(function() {
                return ! this.fileStockId;
            }, this)
        });

        this.browseBtn.on({
            upload: function(btn, response) {

                Application.notificate(response.messages);
                if (response.success) {
                    this.fireEvent('update-file', btn, response);
                }

                if (true === this.autoCloseAfterUpload) {
                    this.close();
                }
            },
            beforeupload: function() {
                return this.form.getForm().isValid();
            },
            scope: this
        });

        this.buttons.push(this.browseBtn);
        this.buttons.push('->');

        if (this.fileStockId) {
            this.saveButton = new Ext.Button({
                text: lang('Save'),
                minWidth: 75,
                handler: this.onUpdateFileStockRow,
                scope: this
            });
            this.buttons.push(this.saveButton);
        }

        this.buttons.push({
            text: lang('Cancel'),
            minWidth: 75,
            handler: function() {
                this.close();
            },
            scope: this
        });

        this.callParent(arguments);

        this.addEvents('load', 'update-file');

        this.form.on('actioncomplete', function(form, action) {
            var self = this;
            this.disableEnableButtons();
            if (action.type == 'load' && action.result.data.id) {
                var file = action.result.data;
                var p = Ext.clone(this.baseParams);
                this.downloadLinkPanel.update('<div style="float:left"><nobr><a href="'
                            + link(this.module, this.controller, 'download', Ext.apply(p || {}, {
                                id: file['id']
                            })) + '">' + file['name'] + '</a> ( ' + Ext.util.Format.fileSize(file['size']) + ' ) </nobr></div>');
                this.downloadLinkPanel.show();
            }
        }, this);

        this.on('render',function() {
            this.doLoad();
            this.addSpacerToButtons();
        }, this, {delay: 50});

        this.on('resize',function() {
            this.addSpacerToButtons();
        }, this, {delay: 50});

        this.form.on('ready', function() {
            this.disableEnableButtons();
        }, this);

    },

    addSpacerToButtons: function() {
        var tr = this.getEl().select('tr.x-toolbar-left-row').first();
        if (tr) {
            Ext.DomHelper.insertAfter(tr.dom.childNodes[0], {tag: 'td', style: 'width: 100%;'});
        }
    },

    openScanConfiguration: function() {

        var configuration = new OSDN.Configuration.Preferences.Scanning({
            accountable: true
        });

        var w = configuration.showInWindow();
        configuration.on('save', function() {
            w.close();
            this.scanBtn.update();
        }, this, {single: true});
    },

    disableEnableButtons: function() {
        if (this.form.getForm().isValid()) {
            if (this.scanBtn) {
                if (this.scanBtn.isReady()) {
                    this.scanBtn.enable();
                }
            }
            if (this.saveButton) {
                this.saveButton.enable();
            }
            this.browseBtn.enable();
        } else {
            if (this.scanBtn) {
                this.scanBtn.disable();
            }
            if (this.saveButton) {
                this.saveButton.disable();
            }
            this.browseBtn.disable();
        }
    },

    getbaseParamsWithFormValues: function() {
        var p = Ext.clone(this.baseParams);
        p = Ext.apply(p, this.form.getForm().getValues());
        p.fileStockId = this.fileStockId;
        return p;
    },

    doLoad: function(params) {

        var p = Ext.clone(this.baseParams);
        p = Ext.apply(p, params);
        if (p.fileStockId) {
            this.fileStockId = this.baseParams.fileStockId;
        }

        if (this.fileStockId) {
            p.fileStockId = this.fileStockId;
            this.form.getForm().load({
                url: link(this.module, this.controller, 'fetch', {format: 'json'}),
                method: 'POST',
                params: p,
                waitMsg: true,
                success: function(form, options) {
                    this.fireEvent('load', this.form);
                },
                scope: this
            });
        }
    },

    onUpdateFileStockRow: function(callback) {

        if (!this.fileStockId) {
            throw 'Unable to update file. Identifier is missing.';
        }

        if (!this.form.getForm().isValid()) {
            return;
        }

        if (!Ext.isObject(this.baseParams)) {
            this.baseParams = {};
        }
        this.baseParams.fileStockId = this.fileStockId;

        if (false === this.fireEvent('before-submit', this, this.form, this.baseParams)) {
            return false;
        }

        this.form.getForm().submit({
            url: link(this.module, this.controller, 'update', {format: 'json'}),
            params: this.baseParams,
            success: function(form, options) {
                if (true !== options.result.success) {
                    Application.notificate(lang('Unable to update.'));
                    return;
                }

                this.fireEvent('update-file', this, Ext.decode(options.response.responseText));
                if (Ext.isFunction(callback)) {
                    callback();
                }

                if (true === this.autoCloseAfterUpload) {
                    this.close();
                }
            },
            scope: this
        });
   },

    destroy: function() {
        Ext.destroy(this.form);

        this.callParent();
    }
});