Ext.namespace('Module.FileStock');

Ext.define('Module.FileStock.List', {

    extend: 'Ext.ux.grid.GridPanel',

    alias: 'module.file-stock.list',

    module: 'document',
    controller: 'document-file-stock',

    entityType: 'document',
    entityName: null,
    entityId: null,

    extraParams: null,

    autoLoadRecordsCallbackFn: 'doLoad',

    initComponent: function() {

        if (!Ext.isObject(this.extraParams)) {
            this.extraParams = {};
        }

        var p = Ext.clone(this.extraParams);
        p[this.entityName] = this.entityId;

        var uploadButton = new Module.FileStock.Button({
            module: this.module,
            controller: this.controller,
            mode: 'toolbar',
            extraParams: p
        });

        this.tbar = [{
            text: lang('Create'),
            iconCls: 'icon-create-16',
            handler: this.onCreateFileStockRow,
            scope: this
        }, '-', uploadButton, '->', {
            text: lang('Refresh'),
            iconCls: 'x-tbar-loading',
            handler: function() {
                this.doLoad();
            },
            scope: this
        }];

        Ext.define('fileList', {
            extend: 'Ext.data.Model',
            fields: [
                'id', 'description', 'name', 'type', 'size'
            ]
        });

        this.store = new Ext.data.Store({
            model: 'fileList',
            proxy: {
                type: 'ajax',
                url: link(this.module, this.controller, 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset',
                    totalProperty: 'total'
                },
                extraParams: this.extraParams,
                simpleSortMode: true
            },
            remoteSort: true,
            sorters: {
                property: 'name',
                direction: 'ASC'
            }
        });

        this.columns = [new Ext.grid.RowNumberer(), {
            header: lang('Filename'),
            dataIndex: 'name',
            width: 225
        }, {
            header: lang('Size'),
            dataIndex: 'size',
            width: 75,
            renderer: function(value, metadata, record, rowIndex,  colIndex, store) {
                return Ext.util.Format.fileSize(value);
            }
        }, {
            header: lang('Type'),
            dataIndex: 'type',
            hidden:true,
            width: 80,
            renderer: function(value) {
                return value;
            }
        }, {
            header: lang('Description'),
            hideable: false,
            dataIndex: 'description',
            flex: 1,
            renderer: function(value, metadata, record, rowIndex,  colIndex, store) {
                return value || record.get('name');
            }
        }, {
            xtype: 'actioncolumn',
            header: lang('Action'),
            items: [{
                tooltip: lang('Edit'),
                iconCls: 'icon-edit-16 icon-16',
                handler: function(g, rowIndex) {
                    this.onEditFileStockRow(g.getStore().getAt(rowIndex));
                },
                scope: this
            }, {
                text: lang('Download'),
                iconCls: 'osdn-icon-attached-file icon-16',
                handler: function(g, rowIndex) {
                    this.onDocumentDownload(g.getStore().getAt(rowIndex));
                },
                scope: this
            }, {
                text: lang('Delete'),
                iconCls: 'icon-delete-16 icon-16',
                handler: function(g, rowIndex) {
                    Ext.Msg.confirm(lang('Question'), lang('Are you sure?'), function(b) {
                        'yes' == b && this.onDeleteFileStockRow(g.getStore().getAt(rowIndex));
                    }, this);
                },
                scope: this
            }]
        }];

        this.callParent(arguments);

        uploadButton.on('upload', function(btn, response) {
            this.onUploadButtonCallback(btn, response);
            this.doLoad();
        }, this);

        this.addEvents('before-upload', 'after-upload',
            /**
             * Fires when entity changed
             *
             * @param {Module.FileStock.List}
             * @param {Integer}
             * @param {Object} Response from the server
             */
            'entitychanged'
        );
    },

    onUploadButtonCallback: function(btn, response) {

        if (this.entityName && response[this.entityName]) {
            this.extraParams[this.entityName] = response[this.entityName];
            this.entityId = response[this.entityName];

            this.fireEvent('entitychanged', this, response[this.entityName], response);
        }
    },

    createContextMenu: function(record) {
        var items = [];

        return new Ext.menu.Menu({

        });

    },

    onDocumentDownload: function(record) {
        var p = {};
        p[this.entityName] = this.entityId;
        p.fileStockId = record.get('id');

        location.href = link(this.module, this.controller, 'download', p);
    },

    onCreateFileStockRow: function() {

        Application.require([
            'file-stock/./form'
        ], function() {

            var p = Ext.clone(this.extraParams);
            p.fileStockId = null;
            p[this.entityName] = this.entityId;

            var w = new Module.FileStock.Form({
                baseParams: p,
                module: this.module,
                controller: this.controller
            });

            w.on('update-file', function(button, response) {
                this.onUploadButtonCallback(button, response);
                this.doLoad();
                w.close();
            }, this);

            w.show();
        }, this);
    },

    onEditFileStockRow: function(record) {

        Application.require([
            'file-stock/./form'
        ], function() {

            var p = Ext.clone(this.extraParams);
            p.fileStockId = record.get('id');
            p[this.entityName] = this.entityId;

            var w = new Module.FileStock.Form({
                baseParams: p,
                entityType: this.entityType,
                module: this.module,
                controller: this.controller
            });

            w.on('update-file', function(btn, response) {
                this.onUploadButtonCallback(btn, response);
                this.doLoad();
                w.close();
            }, this);

            w.show();
        }, this);
    },

    onDeleteFileStockRow: function(record) {

        var p = Ext.clone(this.extraParams);
        p.fileStockId = record.get('id');
        p[this.entityName] = this.entityId;

        Ext.Ajax.request({
            url: link(this.module, this.controller, 'delete', {format: 'json'}),
            success: function(response, options) {
                var decResponse = Ext.decode(response.responseText);
                if (!decResponse.success) {
                    Application.notificate('Unknown error occured');
                    return;
                }

                this.doLoad();
            },
            params: p,
            scope: this
        });
    },

    doLoad: function(p) {

        var params = Ext.clone(this.extraParams);
        if (this.entityId) {
            params[this.entityName] = this.entityId;
        }
        this.params = Ext.apply(this.params, p);

        if (!this.entityId) {
        	return;
        }
        
        this.getStore().load({
            params: params
        });
    },

    showInWindow: function() {
        this.window = new Ext.Window({
            layout: 'fit',
            title: this.title,
            width: this.hasHistory ? 700 : 400,
            height: 330,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Close'),
                iconCls: 'icon-cancel-16',
                handler: function() {
                    this.window.close();
                },
                scope: this
            }]

        });
        this.setTitle('');
        this.window.show.apply(this.window, arguments);
        return this.window;
    }

});
