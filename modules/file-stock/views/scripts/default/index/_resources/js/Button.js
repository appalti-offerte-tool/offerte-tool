Ext.define('Module.FileStock.Button', {
    extend: 'Ext.form.Panel',
    stateful: false,

    mode: null,

    border: false,
    maxHeight: 22,
    module: null,
    controller: null,
    fileStockId: null,

    cls: 'file-stock-button',

    buttonText: null,
    extraParams: null,
    uploadUrl: null,
    iconCls: '',
    buttonConfig: null,

    layout: 'anchor',
    defaults: {
        anchor: '100%'
    },

    getBaseParams: Ext.emptyFn,

    initComponent: function() {

        this.buttonText = this.buttonText || 'Upload file';

        if (Ext.isEmpty(this.buttonConfig)) {
            this.buttonConfig = {};
        }

        this.buttonConfig.height = this.maxHeight;
        this.setHeight(this.maxHeight);

        if (this.mode) {
            this.addCls('mode-file-stock-button');
            switch(this.mode) {
                case 'menu':
                    this.buttonConfig.cls = 'menu-file-stock-button';
                    break;
            }
        }

        Ext.apply(this.buttonConfig, {
            iconCls: this.iconCls ? this.iconCls : 'icon-update-16'
        });

        this.items = [{
            xtype: 'filefield',
            autoRender: true,
            labelAlign: 'left',
            buttonOnly: true,
            buttonConfig: this.buttonConfig,
            buttonText: this.buttonText,
            listeners: {
                change: this.onFileFieldChange,
                scope: this
            }
        }];

        this.callParent(arguments);

        this.addEvents('beforeupload', 'upload', 'failureupload');

    },

    onFileFieldChange: function(fb, v) {

        if (!this.form.isValid()) {
            return;
        }

        if (false === this.fireEvent('beforeupload', this)) {
            return false;
        }

        var p = Ext.clone(this.extraParams);
        var action = this.isFileStockCreate() ? 'create' : 'update';

        this.form.submit({
            url: this.uploadUrl || link(this.module, this.controller, action, {format: 'json'}),
            params: Ext.apply(p || {}, this.getBaseParams() || {}),
            waitMsg: lang('Uploading your file...'),
            success: function(form, action) {
                this.fireEvent('upload', this, Ext.decode(action.response.responseText));
            },
            failure: function(form, action) {
                var decResponse = Ext.decode(action.response.responseText);
                Application.notificate(decResponse.messages);
            },
            scope: this
        });
    },

    isFileStockCreate: function() {
        return true;
    }
});
