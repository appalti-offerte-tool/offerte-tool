<?php

interface FileStock_Instance_EntityFileStockControllerInterface extends \Zend_Acl_Resource_Interface
{
    public function getEntityType();
}