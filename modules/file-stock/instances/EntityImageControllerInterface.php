<?php

interface FileStock_Instance_EntityImageControllerInterface extends \Zend_Acl_Resource_Interface
{
    public function getEntityType();
}