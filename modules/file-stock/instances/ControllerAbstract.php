<?php

class FileStock_Instance_ControllerAbstract extends Zend_Controller_Action
{
    /**
     * @var FileStock_Service_FileStock_StorageAbstract
     */
    protected $_service;

    public function init()
    {
        parent::init();

        $this->_service = new \FileStock_Service_FileStock();

        $this->_helper->ajaxContext()
            ->addContextAction('delete', 'json')
            ->initContext();
    }

    public function downloadAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $id = $this->_getParam('id');

        $response = $this->file->getFileWithContent($id);
        if ($response->isError()) {
            return;
        }
        $data = $response->getRow();

        /**
         * @FIXME Change to Zend_Controller_Response header
         */
        header('Content-disposition: attachment; filename="' . $data['name'] . '"');

        header('Content-Type: ' . $data['type']);

        header('Content-Transfer-Encoding: binary');

        header('Content-Length: '. $data['size']);

        header('Pragma: no-cache');

        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

        header('Expires: 0');

        echo $data['filecontent'];
    }

    public function deleteAction()
    {
        try {
            $result = (int) $this->_service->delete($this->_getParam('file_id'));

            if (true === $result && method_exists($this, '_afterDeleteFile')) {
                $this->_afterDeleteFile($response);
            }

            $this->view->success = $result;

        } catch(FileStock_Services_Exception $e) {
            $this->_helper->information($e);
        }
    }

    public function fetchAction()
    {
        $fileStockRow = $this->_service->find($this->_getParam('fileStockId'));
        $this->view->row = $fileStockRow;
        $this->view->success = true;
    }

    public function updateFileAction()
    {

        $data = $this->_getAllParams();

        $data['file'] = array();
        if (!empty($_FILES['RemoteFile']) && $_FILES['RemoteFile']['name']) {
            $data['file'] = $_FILES['RemoteFile'];
        }
        $data['file']['description'] = $data['description'];

        if (empty($data['fileStockId'])  || $data['fileStockId'] == 'null') {
            $response = $this->file->insert($data['file']);
            if (method_exists($this, '_afterInsertFile')) {
                $this->_afterInsertFile($response);
            }
            $this->view->fileStockId = $response->fileStockId;
        } else {
            $response = $this->file->update($this->_getParam('fileStockId'), $data['file']);
            if (method_exists($this, '_afterUpdateFile')) {
                $this->_afterUpdateFile($response);
            }
        }
        if ($response->isError()) {
            $this->_collectErrors($response);
            $this->view->success = false;
            return;
        }
        $this->view->success = true;
    }
}