<?php

class FileStock_Model_DbTable_FileStockRow extends Zend_Db_Table_Row_Abstract implements OSDN_Application_Model_Interface
{
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }
}