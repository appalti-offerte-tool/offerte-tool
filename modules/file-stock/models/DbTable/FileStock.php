<?php

/**
 * Files table
 */
class FileStock_Model_DbTable_FileStock extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'fileStock';

    protected $_rowClass = '\\FileStock_Model_DbTable_FileStockRow';

    protected $_omitColumns = array(
        'filecontent'    => false
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        if (empty($data['createdDatetime'])) {
            $dt = new \DateTime();
            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        if (empty($data['createdAccountId'])) {
            $data['createdAccountId'] = \Zend_Auth::getInstance()->getIdentity()->getId();
        }

        return parent::insert($data);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
        if (empty($data['modifiedDatetime'])) {
            $dt = new \DateTime();
            $data['modifiedDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        if (empty($data['modifiedAccountId'])) {
            $data['modifiedAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();
        }

        return parent::update($data, $where);
    }
}