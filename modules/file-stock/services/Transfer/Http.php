<?php

class FileStock_Service_Transfer_Http extends \Zend_File_Transfer_Adapter_Http
{
    protected $_skipReceiveProcess = false;

    public function  setSkipReceiveProcess($flag)
    {
        $this->_skipReceiveProcess = (boolean) $flag;
        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see Zend_File_Transfer_Adapter_Http::receive()
     */
    public function receive($files = null)
    {
        if (false === $this->_skipReceiveProcess) {
            return parent::receive($files);
        }

        if (!$this->isValid($files)) {
            return false;
        }

        $check = $this->_getFiles($files);
        foreach ($check as $file => $content) {
            if (!$content['filtered']) {
                if (!$this->_filter($file)) {
                    $this->_files[$file]['filtered'] = false;
                    return false;
                }

                $this->_files[$file]['filtered'] = true;
            }
        }

        return true;
    }
}