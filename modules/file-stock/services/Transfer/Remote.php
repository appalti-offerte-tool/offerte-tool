<?php

final class FileStock_Service_Transfer_Remote extends \Zend_File_Transfer_Adapter_Abstract
{
    /**
     * (non-PHPdoc)
     * @see Zend_File_Transfer_Adapter_Abstract::send()
     */
    public function send($options = null)
    {
        require_once 'Zend/File/Transfer/Exception.php';
        throw new Zend_File_Transfer_Exception('Method not implemented');
    }

    /**
     * (non-PHPdoc)
     * @see Zend_File_Transfer_Adapter_Abstract::receive()
     */
    public function receive($options = null)
    {
        foreach($this->_files as $i => $content) {

            if (!$content['filtered']) {

                if (!$this->_filter($i)) {
                    $this->_files[$i]['filtered'] = false;
                    return false;
                }

                $this->_files[$i]['filtered'] = true;
            }
        }

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Zend_File_Transfer_Adapter_Abstract::isSent()
     */
    public function isSent($files = null)
    {
        require_once 'Zend/File/Transfer/Exception.php';
        throw new Zend_File_Transfer_Exception('Method not implemented');
    }

    /**
     * (non-PHPdoc)
     * @see Zend_File_Transfer_Adapter_Abstract::isReceived()
     */
    public function isReceived($files = null)
    {
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Zend_File_Transfer_Adapter_Abstract::isUploaded()
     */
    public function isUploaded($files = null)
    {
        return true;
    }

    public function isFiltered($files = null)
    {
        return true;
    }

    public function addFile($file, $validator = null, $filter = null)
    {
        $i = 'file_' . count($this->_files);

        $name = uniqid('cw-account-') . '.' . pathinfo($file, PATHINFO_EXTENSION);
        $destination = $this->_getTmpDir() . DIRECTORY_SEPARATOR . $name;

        $content = file_get_contents($file);
        file_put_contents($destination, $content);


        $this->_files[$i]['name'] = $name;
        $this->_files[$i]['tmp_name'] = $destination;
        $this->_files[$i]['destination'] = dirname($destination);
        $this->_files[$i]['options'] = $this->_options;
        $this->_files[$i]['validated'] = false;
        $this->_files[$i]['received'] = false;
        $this->_files[$i]['filtered'] = false;

        $mimetype = $this->_detectMimeType($this->_files[$i]);
        $this->_files[$i]['type'] = $mimetype;

        $filesize = $this->_detectFileSize($this->_files[$i]);
        $this->_files[$i]['size'] = $filesize;

        return $this;
    }
}