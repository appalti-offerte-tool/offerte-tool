<?php

interface FileStock_Service_Storage_StorageInterface
{
    public function getStorageType();

    public function create(Zend_File_Transfer_Adapter_Abstract $transfer, \FileStock_Model_DbTable_FileStockRow $row);

    public function update(Zend_File_Transfer_Adapter_Abstract $transfer, \FileStock_Model_DbTable_FileStockRow $row);

    public function delete(\FileStock_Model_DbTable_FileStockRow $row);

    public function fetchFileContent(FileStock_Model_DbTable_FileStockRow $row);
}