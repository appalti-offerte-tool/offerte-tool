<?php

class FileStock_Service_Storage_File extends OSDN_Application_Service_Dbable implements FileStock_Service_Storage_StorageInterface
{
    protected $_customExtension = 'file';

    protected $_useCustomExtension = true;

    /**
     * Base file path
     *
     * @var $_baseFilePath
     */
    protected $_baseFilePath;

    public function __construct(array $config = array())
    {
        parent::__construct();

        foreach($config as $o => $value) {
            $method = 'set' . ucfirst($o);
            if (method_exists($this, $method)) {
                call_user_func(array($this, $method), $value);
            }
        }
    }

    public function setBaseFilePath($basePath)
    {
        if (false === ($basePath = realpath($basePath))) {
            throw new \OSDN_Exception('Unable to locate path');
        }

        $this->_baseFilePath = rtrim($basePath, '/\\') . DIRECTORY_SEPARATOR;
        return $this;
    }

    public function setUseCustomExtension($flag)
    {
        $this->_useCustomExtension = (boolean) $flag;
        return $this;
    }

    public function setCustomExtension($extension)
    {
        $this->_customExtension = $extension;
        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see FileStock_Services_Storage_StorageAbstract::getStorageType()
     */
    public function getStorageType()
    {
        return \FileStock_Service_FileStock::FILESTOCK_STORAGE_FILE;
    }

    public function create(Zend_File_Transfer_Adapter_Abstract $transfer, \FileStock_Model_DbTable_FileStockRow $row)
    {
        $destination = $this->toFilePath($row, true);
        $transfer->addFilter(new Zend_Filter_File_Rename($destination));

        $result = $transfer->receive();

        if (false === $result || $transfer->hasErrors()) {
            $e = new OSDN_Exception();
            $e->assign($transfer->getErrors());
            throw $e;
        }

        return $result;
    }

    public function update(Zend_File_Transfer_Adapter_Abstract $transfer, \FileStock_Model_DbTable_FileStockRow $row)
    {
        $destination = $this->toFilePath($row, true);

        /**
         * We should store the original information (filename, etc)
         */
        $fileInformationOriginal = current($transfer->getFileInfo());

        $transfer->addFilter(new Zend_Filter_File_Rename(array(
            'target'    => $destination,
            'overwrite' => true
        )));

        $result = $transfer->receive();

        if (false === $result || $transfer->hasErrors()) {
            $e = new OSDN_Exception();
            $e->assign($transfer->getErrors());
            throw $e;
        }

        $row->storageType = $this->getStorageType();

        $fileInformation = current($transfer->getFileInfo());
        $row->name = $fileInformationOriginal['name'];
        $row->type = $fileInformation['type'];
        $row->size = $fileInformation['size'];

        return $result;
    }

    public function delete(\FileStock_Model_DbTable_FileStockRow $row)
    {
        $destination = $this->toFilePath($row);

        if (!file_exists($destination)) {
            return false;
        }

        if (!is_writable($destination)) {
            throw new \OSDN_Exception('There is no permissions to write in ' . $destination);
        }

        if (is_file($destination)) {
            return @unlink($destination);
        }

        return false;
    }

    public function fetchFileContent(FileStock_Model_DbTable_FileStockRow $row)
    {
        $destination = $this->toFilePath($row);
        if (!file_exists($destination)) {
            throw new \OSDN_Exception('Unable to find file');
        }

        return file_get_contents($destination, false);
    }

    /**
     * Return file path
     *
     * @param FileStock_Model_DbTable_FileStockRow $fileStockRow
     * @return string
     */
    public function toFilePath(\FileStock_Model_DbTable_FileStockRow $fileStockRow, $ensureWritableDirectoryCheck = false)
    {
        $folder = 1000 + floor($fileStockRow->getId() / 1000) * 1000;
        $directory = $this->_baseFilePath . $folder . DIRECTORY_SEPARATOR;

        if (false !== $ensureWritableDirectoryCheck) {

            $message = '';
            if (!file_exists($directory) || !is_dir($directory)) {

                if (!is_writable($this->_baseFilePath)) {
                    throw new OSDN_Exception('There is no permission to write in ' . $this->_baseFilePath);
                }

                mkdir($directory, 0775, true);
            }
        }

        $path = $directory . $fileStockRow->getId();
        if (true === $this->_useCustomExtension) {
            return $path . '.' . $this->_customExtension;
        }

        $extension =  pathinfo($fileStockRow->name, PATHINFO_EXTENSION);
        if (!empty($extension)) {
            $path .= '.' . $extension;
        }

        return $path;
    }

    public function toFilePathHost(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
    {
        $path = $this->toFilePath($fileStockRow);
        if (false === ($path = realpath($path))) {
            return false;
        }

        $appPath = APPLICATION_PATH;
        $path = str_replace(DIRECTORY_SEPARATOR, '/', $path);
        $appPath = str_replace(DIRECTORY_SEPARATOR, '/', $appPath);

        return str_replace($appPath, '', $path);
    }
}