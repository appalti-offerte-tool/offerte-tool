<?php

class FileStock_Service_Storage_Db extends OSDN_Application_Service_Dbable implements FileStock_Service_Storage_StorageInterface
{
    /**
     * (non-PHPdoc)
     * @see FileStock_Services_Storage_StorageAbstract::getStorageType()
     */
    public function getStorageType()
    {
        return \FileStock_Service_FileStock::FILESTOCK_STORAGE_DB;
    }
}