<?php

class FileStock_Service_FileStock extends OSDN_Application_Service_Dbable
{
    const FILESTOCK_STORAGE_DB = 'DB';

    const FILESTOCK_STORAGE_FILE = 'FILE';

    /**
     * @var FileStock_Model_DbTable_FileStock
     */
    protected $_table;

    /**
     * Default allowed file mime types
     *
     * @var $_defaultAllowedFileTypes
     */
    protected static $_defaultAllowedFileTypes = array();

    /**
     * Allowed default file extensions
     *
     * @var $_defaultAllowedFileExtensions
     */
    protected static $_defaultAllowedFileExtensions = array();

    protected $_configuration;

    public function __construct(array $configuration = array())
    {
        $this->_configuration = $configuration;

        parent::__construct();
    }

    protected function _init()
    {
        $this->_table = new \FileStock_Model_DbTable_FileStock();

        if (empty(self::$_defaultAllowedFileExtensions) && empty(self::$_defaultAllowedFileTypes)) {

            $identity = null;
            if (!empty($this->_configuration['identity'])) {
                $identity = $this->_configuration['identity'];
            }

            $configuration = new \FileStock_Service_MimeConfiguration($identity);
            self::$_defaultAllowedFileExtensions = $configuration->fetchAllExtensions();
            self::$_defaultAllowedFileTypes = $configuration->fetchAllMimeTypes();
        }

        $this->_attachValidationRules('insert', array(
            'description'       => array(array('StringLength', 0, 250), 'allowEmpty' => true)
        ));

        $this->_attachValidationRules('update', array(
            'fileStockId'            => array('id', 'presence' => 'required'),
            'description'       => array(array('StringLength', 0, 250), 'allowEmpty' => true)
        ));
    }

    /**
     * Create file storage adapter
     *
     * @param string $adapter   The file storage adapter type
     * @param array  $config    The configuration of adapter
     *
     * @return FileStock_Service_Storage_StorageInterface
     */
    public function factory($adapter = self::FILESTOCK_STORAGE_FILE, array $config = array())
    {
        if (empty($adapter)) {
            throw new OSDN_Exception('The adapter cannot be empty.');
        }

        if (!in_array($adapter, array(self::FILESTOCK_STORAGE_DB, self::FILESTOCK_STORAGE_FILE))) {
            throw new OSDN_Exception('The adapter is not allowed.');
        }

        if (empty($config)) {
            $config = $this->_configuration;
        }

        $adapterCls = '\\FileStock_Service_Storage_' . ucfirst(strtolower($adapter));
        return new $adapterCls($config);
    }

    public static function setDefaultAllowedFileTypes(array $ft, $append = false)
    {
        $ft = array_values($ft);

        if (true === $append) {
            $ft = array_merge(self::$_defaultAllowedFileTypes, $ft);
            self::$_defaultAllowedFileTypes = array_unique($ft);
        } else {
            self::$_defaultAllowedFileTypes = $ft;
        }
    }

    public static function getDefaultAllowedFileTypes()
    {
        return self::$_defaultAllowedFileTypes;
    }

    public static function setDefaultAllowedFileExtensions(array $ex, $append = false)
    {
        $ex = array_values($ex);

        if (true === $append) {
            $ex = array_merge(self::$_defaultAllowedFileExtensions, $ex);
            self::$_defaultAllowedFileExtensions = array_unique($ex);
        } else {
            self::$_defaultAllowedFileExtensions = $ex;
        }
    }

    public static function getDefaultAllowedFileExtensions()
    {
        return self::$_defaultAllowedFileExtensions;
    }

    public function create(Zend_File_Transfer_Adapter_Abstract $transfer, array $options = array(), Closure $callbackFn = null)
    {
        $this->_validate($options, 'insert');

        $transfer->addValidator(new Zend_Validate_File_Count(1, 1), true);
        $transfer->addValidator(new Zend_Validate_File_Extension(self::getDefaultAllowedFileExtensions()));

        if (!$transfer->isValid()) {
            $e = new OSDN_Exception();
            $e->assign($transfer->getMessages());
            throw $e;
        }

        $stock = $this->factory();

        $data = $options;
        $data['storageType'] = $stock->getStorageType();

        $fileInformation = current($transfer->getFileInfo());
        $data['name'] = $fileInformation['name'];
        $data['type'] = $fileInformation['type'];
        $data['size'] = $fileInformation['size'];

        $this->getAdapter()->beginTransaction();

        try {

            $fileStockRow = $this->_table->createRow($data);

            $fileStockRow->save();

            $stock->create($transfer, $fileStockRow);

            if (null !== $callbackFn && is_callable($callbackFn)) {
                call_user_func($callbackFn, $fileStockRow, $options);
            }

            $this->getAdapter()->commit();

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();

            throw $e;
        }

        return $fileStockRow;
    }

    /**
     * @deprecated
     */
    public function createWithoutTransactionWrap(Zend_File_Transfer_Adapter_Abstract $transfer, array $options = array(), Closure $callbackFn = null)
    {
        return $this->create($transfer, $options, $callbackFn, true);
    }

    public function cloneRow(\FileStock_Service_Transfer_Raw $rawFileTransfer, \FileStock_Model_DbTable_FileStockRow $fileStockRow, array $options = array(), Closure $callbackFn = null)
    {
        $this->getAdapter()->beginTransaction();
        try {
            $newFileStockRowId = $this->_table->cloneRow($fileStockRow->getId());

            if (empty($newFileStockRowId)) {
                throw new \OSDN_Exception('Unable to clone fileStockRow');
            }

            $newFileStockRow = $this->find($newFileStockRowId);
            $storage = $this->factory();

            $storage->create($rawFileTransfer, $newFileStockRow);

            if (null !== $callbackFn && is_callable($callbackFn)) {
                call_user_func($callbackFn, $newFileStockRow, $options);
            }

            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $newFileStockRow;

    }

    public function update(Zend_File_Transfer_Adapter_Abstract $transfer, array $options = array(), Closure $callbackFn = null)
    {
        $f = $this->_validate($options, 'update');
        $fileStockRow = $this->find($f->fileStockId);

        try {
            $this->getAdapter()->beginTransaction();

            $fileStockRow->setFromArray($options);

            if (true === $transfer->isUploaded()) {

                $transfer->addValidator(new Zend_Validate_File_Count(1, 1), true);
//                $transfer->addValidator(new Zend_Validate_File_MimeType(self::getDefaultAllowedFileTypes()));
                $transfer->addValidator(new Zend_Validate_File_Extension(self::getDefaultAllowedFileExtensions()));

                if (!$transfer->isValid()) {
                    $e = new OSDN_Exception();
                    $e->assign($transfer->getMessages());
                    throw $e;
                }

                $stock = $this->factory();
                $stock->update($transfer, $fileStockRow);
            }

            $fileStockRow->save();

            if (null !== $callbackFn && is_callable($callbackFn)) {
                call_user_func($callbackFn, $fileStockRow, $options);
            }

            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $fileStockRow;
    }

    /**
     * @deprecated
     */
    public function updateWithoutTransactionWrap(Zend_File_Transfer_Adapter_Abstract $transfer, array $options = array(), Closure $callbackFn = null)
    {
        return $this->update($transfer, $options, $callbackFn);
    }

    /**
     * @param int $id
     * @return FileStock_Model_DbTable_FileStockRow
     */
    public function find($id)
    {
        return $this->_table->findOne($id);
    }

    public function findAll(array $ids)
    {
        $ids = array_map('intval', $ids);
        return $this->_table->fetchAll(array('id IN(?)' => $ids));
    }

    public function delete($id, Closure $callbackFn = null)
    {
        $fileStockRow = $this->_table->findOne($id);
        if (empty($fileStockRow)) {
            throw new \OSDN_Exception('Unable to find file');
        }

        $this->getAdapter()->beginTransaction();

        try {

            if (null !== $callbackFn && is_callable($callbackFn)) {
                call_user_func($callbackFn, $fileStockRow);
            }

            $stock = $this->factory();
            $stock->delete($fileStockRow);

            $fileStockRow->delete();

            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }
    }

    /**
     * @deprecated
     */
    public function deleteWithoutTransactionWrap($id, Closure $callbackFn = null)
    {
        return $this->delete($id, $callbackFn);
    }

    protected function _doInitResponseHeaders(
        FileStock_Model_DbTable_FileStockRow $fileStockRow,
        Zend_Controller_Response_Http $response,
        $keepExisting = false
    ) {
        if (true !== $keepExisting) {
            $response->clearAllHeaders();
        }

        $response->setHeader('Content-disposition', sprintf('attachment; filename="%s"', $fileStockRow->getName()));
        $response->setHeader('Content-Type', $fileStockRow->type);
        $response->setHeader('Content-Transfer-Encoding', 'binary');
        $response->setHeader('Content-Length', $fileStockRow->size);
        $response->setHeader('Pragma', 'no-cache');
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->setHeader('Expires', 0);
    }

    public function download(
        FileStock_Model_DbTable_FileStockRow $fileStockRow,
        Zend_Controller_Response_Http $response,
        $throwContent = true,
        Closure $callbackFn = null
    ) {
        $this->_doInitResponseHeaders($fileStockRow, $response);

        if (null !== $callbackFn && is_callable($callbackFn)) {
            $callbackFn($fileStockRow, $response);
        }

        $storage = $this->factory();
        if (true !== $throwContent) {
            $content = $storage->fetchFileContent($fileStockRow);
            $response->setBody($content);
            return $response;
        }

        $response->sendHeaders();

        /**
         * @FIXME readfile
         */
        $o = $storage->fetchFileContent($fileStockRow);

        echo $o;

        die;
    }

    public function fetchFileContent(FileStock_Model_DbTable_FileStockRow $fileStockRow, $throwContent = true)
    {
        return $this->factory()->fetchFileContent($fileStockRow);
    }
}