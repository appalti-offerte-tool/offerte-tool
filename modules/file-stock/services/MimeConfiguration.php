<?php

class FileStock_Service_MimeConfiguration extends Configuration_Service_ConfigurationAbstract
{
    protected $_identity = 'file-stock.mime';

    protected $_fields = array(
        'extension' => array('default' => 'pdf')
    );

    public function __construct($identity = null)
    {
        if (null !== $identity) {
            $this->_identity = $identity;
        }

        parent::__construct();
    }

    public function fetchCompleteList()
    {
        $rowset = $this->_fetchAll(false);

        usort($rowset, function($a1, $a2) {
            return strcasecmp($a1['extension'], $a2['extension']);
        });

        return $rowset;
    }

    protected function _fetchAll($omitNotUsed = false)
    {
        $filename = realpath(__DIR__ . '/../views/scripts/default/mime-configuration/_resources/_files/mime-types.csv');
        if (false === $filename || !file_exists($filename)) {
            throw new \OSDN_Exception('Unable to locale "mime-types.csv"');
        }


        if (false === ($h = @fopen($filename, 'r', false))) {
            throw new \OSDN_Exception('Unable to read file "mime-types.csv"');
        }

        $extensions = $this->toInstance()->getExtension();

        $rowset = array();
        while(false !== ($row = fgetcsv($h))) {

            $extension = trim($row[0]);
            $isUsed = in_array($extension, $extensions);

            if (true === $omitNotUsed && true !== $isUsed) {
                continue;
            }

            $rowset[] = array(
                'extension'     => $extension,
                'mime-type'     => trim($row[1]),
                'isUsed'        => $isUsed
            );
        }

        return $rowset;
    }

    public function fetchAll()
    {
        return $this->_fetchAll(true);
    }

    public function fetchAllExtensions()
    {
        $rowset = $this->fetchAll();
        $output = array();
        foreach($rowset as $row) {
            $output[] = $row['extension'];
        }

        return array_unique($output);
    }

    public function fetchAllMimeTypes()
    {
        $rowset = $this->fetchAll();
        $output = array();
        foreach($rowset as $row) {
            $output[] = $row['mime-type'];
        }

        return array_unique($output);
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_toValue()
     */
    public function _toValue($value, $field)
    {
        $value = array_unique($value);
        return join(',', $value);
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_fromValue()
     */
    public function _fromValue($value, $field)
    {
        return explode(',', $value);
    }

    public function attach($code)
    {
        $rowset = $this->getRowset();
        if (!isset($rowset['extension']) || ! is_array($rowset['extension'])) {
            throw new OSDN_Exception('Configuration data is corrupted');
        }

        $rowset['extension'][] = $code;
        return $this->update($rowset);
    }

    public function detach($code)
    {
        $rowset = $this->getRowset();
        if (!isset($rowset['extension']) || ! is_array($rowset['extension'])) {
            throw new OSDN_Exception('Configuration data is corrupted');
        }

        $si = array_search($code, $rowset['extension']);
        if (-1 !== $si) {
            unset($rowset['extension'][$si]);
        }

        return $this->update($rowset);
    }
}