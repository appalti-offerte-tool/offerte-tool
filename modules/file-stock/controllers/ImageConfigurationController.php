<?php

class FileStock_ImageConfigurationController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    protected $_service;

    public function init()
    {
        $this->_helper->layout->setLayout('administration');

        $this->_service = new \FileStock_Service_MimeConfiguration();

        $this->_helper->ajaxContext()
            ->addActionContext('save', 'json')
            ->initContext();

        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     *
     * @return string
     */
    public function getResourceId()
    {
        return 'file-stock:image';
    }

    public function indexAction()
    {}

    public function saveAction()
    {}
}