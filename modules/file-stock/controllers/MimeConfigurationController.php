<?php

class FileStock_MimeConfigurationController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    protected $_service;

    protected $_identity;

    public function init()
    {
        $this->_helper->layout->setLayout('administration');

        $identity = trim($this->_getParam('identity'));
        if (!empty($identity)) {
            $this->_identity = $identity;
        }

        $this->_service = new FileStock_Service_MimeConfiguration($this->_identity);

        $this->_helper->ajaxContext()
            ->addActionContext('attach', 'json')
            ->addActionContext('detach', 'json')
            ->addActionContext('fetch-complete-list', 'json')
            ->initContext();

        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        if ($this->getRequest()->getActionName() == 'fetch-all-allowed-extensions') {
            return 'guest';
        }

        return 'file-stock:mime';
    }

    public function indexAction()
    {
        $this->view->identity = $this->_identity;
    }

    public function fetchCompleteListAction()
    {
        $countries = $this->_service->fetchCompleteList();
        $this->view->rowset = $countries;
        $this->view->total = count($countries);
    }

    public function attachAction()
    {
        $this->view->success = (boolean) $this->_service->attach($this->_getParam('extension'));
    }

    public function detachAction()
    {
        $this->view->success = (boolean) $this->_service->detach($this->_getParam('extension'));
    }

    public function fetchAllAllowedExtensionsAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-Type', 'application/javascript');

        $this->view->extensions = $this->_service->fetchAllExtensions();
    }
}