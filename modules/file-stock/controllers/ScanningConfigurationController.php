<?php

class FileStock_ScanningConfigurationController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    protected $_service;

    public function init()
    {
        $this->_helper->layout->setLayout('administration');

        $this->_service = new FileStock_Service_MimeConfiguration();

        $this->_helper->ajaxContext()
            ->addActionContext('attach', 'json')
            ->addActionContext('detach', 'json')
            ->addActionContext('fetch-complete-list', 'json')
            ->initContext();

        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        if ($this->getRequest()->getActionName() == 'fetch-all-allowed-extensions') {
            return 'guest';
        }

        return 'file-stock:mime';
    }

    public function indexAction()
    {}

    public function formAction()
    {
        $this->_helper->layout->disableLayout();
    }

    public function fetchCompleteListAction()
    {
        $countries = $this->_service->fetchCompleteList();
        $this->view->rowset = $countries;
        $this->view->total = count($countries);
    }

    public function attachAction()
    {
        $this->view->success = (boolean) $this->_service->attach($this->_getParam('extension'));
    }

    public function detachAction()
    {
        $this->view->success = (boolean) $this->_service->detach($this->_getParam('extension'));
    }

    public function fetchAllAllowedExtensionsAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-Type', 'application/javascript');

        $this->view->extensions = $this->_service->fetchAllExtensions();
    }
}