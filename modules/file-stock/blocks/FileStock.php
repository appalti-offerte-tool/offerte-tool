<?php

final class FileStock_Block_FileStock extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('File Stock');
    }

    protected function _toHtml()
    {}
}