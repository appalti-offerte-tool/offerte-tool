<?php

final class FileStock_Block_ImageLayout extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Images');
    }
}