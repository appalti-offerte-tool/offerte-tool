<?php

class FileStock_Migration_20111102_152252_06 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createColumn('fileStock', 'createdDatetime', self::TYPE_DATETIME, null, null, true);
        $this->createColumn('fileStock', 'modifiedDatetime', self::TYPE_DATETIME, null, null, false);

        $this->dropColumn('fileStock', 'created');
        $this->dropColumn('fileStock', 'modified');
    }

    public function down()
    {
        $this->createColumn('fileStock', 'created', self::TYPE_DATETIME, null, null, true);
        $this->createColumn('fileStock', 'modified', self::TYPE_DATETIME, null, null, false);

        $this->dropColumn('fileStock', 'createdDatetime');
        $this->dropColumn('fileStock', 'modifiedDatetime');
    }
}