<?php

class FileStock_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('fileStock');
        $this->createColumn('fileStock', 'name', self::TYPE_VARCHAR, 100, null, true);
        $this->createColumn('fileStock', 'description', self::TYPE_VARCHAR, 255, null, false);
        $this->createColumn('fileStock', 'type', self::TYPE_VARCHAR, 100, null, true);
        $this->createColumn('fileStock', 'size', self::TYPE_INT, 11, null, true);
        $this->createColumn('fileStock', 'created', self::TYPE_DATETIME, null, null, true);
        $this->createColumn('fileStock', 'createdAccountId', self::TYPE_INT, 11, null, true);
        $this->createColumn('fileStock', 'modified', self::TYPE_DATETIME, null, null, false);
        $this->createColumn('fileStock', 'modifiedAccountId', self::TYPE_INT, 11, null, false);
        $this->createColumn('fileStock', 'storageType', self::TYPE_VARCHAR, 10, null, true);
        $this->createColumn('fileStock', 'filecontent', self::TYPE_TEXT, null, null, false);
        $this->createIndex('fileStock', array('createdAccountId'), 'IX_createdAccountId');
        $this->createForeignKey('fileStock', array('createdAccountId'), 'account', array('id'), 'FK_createdAccountId');
        $this->createIndex('fileStock', array('modifiedAccountId'), 'IX_modifiedAccountId');
        $this->createForeignKey('fileStock', array('modifiedAccountId'), 'account', array('id'), 'FK_modifiedAccountId');

        foreach(array(
            array('roleId' => 1, 'resource' => 'file-stock', 'privilege' => null),
            array('roleId' => 1, 'resource' => 'file-stock:mime', 'privilege' => null),
            array('roleId' => 1, 'resource' => 'file-stock:image', 'privilege' => null)
        ) as $o) {
            $this->insert('aclPermission', $o);
        }
    }

    public function down()
    {
        $this->getDbAdapter()->delete('aclPermission', array(
            'roleId = 1',
            "(resource = 'file-stock' OR resource LIKE 'file-stock:%')"
        ));

        $this->dropTable('fileStock');
    }
}
