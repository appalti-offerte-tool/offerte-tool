Ext.ns('CA.Configuration.Admin.Cron.DeniedTimes');

CA.Configuration.Admin.Cron.DeniedTimes.Form = Ext.extend(OSDN.form.FormPanel, {
    
    autoHeight: true,
    
    onRenderLoading: true,
    
	itemId: null,
	
    defaultAllowBlank: true,
    
    defaultType: 'textfield',
    
    jobId: null,
    
    itemId: null,
    
    permissions: true,
    
    initComponent: function() {
        this.items = [{
        	fieldLabel: lang('Job'),
        	xtype: 'osdncombo',
            name: 'job_id',
            hiddenName: 'job_id',
            valueField: 'id', 
            displayField: 'name',
            allowBlankOption: true,
            allowBlank: true,
            typeAhead: true,
            mode: 'local',
            border: false,
            anchor: '-10',
            value: this.jobId,
            store: new Ext.data.JsonStore({
                autoLoad: true,
                url: link('admin', 'jobs', 'get-list'),
                root: 'rowset',
                fields: ['id', 'name']
            })
        }, {
        	fieldLabel: lang('Date'),
        	xtype: 'osdndatefield',
        	name: 'date',
        	hiddenName: 'date',
            defaultValue: null,
            allowBlank: true
        }, {
        	fieldLabel: lang('Start time'),
        	xtype: 'timefield',
			name: 'start_time',
			dataIndex: 'start_time',  
			allowBlank: true,
            minValue: '00:00',
            maxValue: '23:59',
            altFormats: OSDN.date.TIME_FORMAT + '|' + OSDN.date.TIME_FORMAT_SERVER, 
            format: OSDN.date.TIME_FORMAT
        }, {
        	fieldLabel: lang('End time'),
        	xtype: 'timefield',
			name: 'end_time',
			dataIndex: 'end_time',  
			allowBlank: true,
            minValue: '00:00',
            maxValue: '23:59',
            altFormats: OSDN.date.TIME_FORMAT + '|' + OSDN.date.TIME_FORMAT_SERVER,
            format: OSDN.date.TIME_FORMAT
        }];
		
		CA.Configuration.Admin.Cron.DeniedTimes.Form.superclass.initComponent.apply(this, arguments);
        
        if (this.onRenderLoading) {
            this.on('ready', function() {
                this.loadData();
            }, this, {delay: 100});
        }
    },
    
    onSave: function() {
        if (this.getForm().isValid()) {
            var params = {};
    		var url = link('admin', 'jobs-denied-times', 'insert');
            if (this.itemId) {
            	params['id'] = this.itemId;
            	url = link('admin', 'jobs-denied-times', 'update');
            }
            this.getForm().submit({
                url: url,
                params: params,
                success: function(f, action) {
                    if (true === action.result.success) {
						this.fireEvent('saved', action.result.id || this.itemId);
                        this.onSuccess(f, action, action.result.id || this.itemId);
                    } else {
                        this.onFailure(f, action);
                    }
                }, 
                failure: function(response, options) {
                    var res = Ext.decode(response.responseText);
                    this.onFailure(res, options);
                },
                scope: this
            });
        }
    },
    
    showInWindow: function(cfg) {
        var w = new Ext.Window(Ext.apply({
            width: 300,
            autoHeight: true,
            layout: 'fit',
            resizable: false,
            items: [this],
            modal: true,
            buttons: [{
                text: lang('Save'),
                handler: this.onSave.createDelegate(this)    
            }, {
                text: lang('Cancel'),
                handler: function() {
                    w.close();
                }
            }] 
        }, cfg || {}));
        w.show().center();
        return w;
    },
    
    loadData: function() {
    	if (this.itemId) {
	        this.load({
	            url: link('admin', 'jobs-denied-times', 'get'),
	            params: {id: this.itemId},
	            waitMsg: lang('Loading ...'),
	            success: function(form, options) {
	                this.fireEvent('load');
	            },
	            scope: this
	        });
    	}
    },
    
    onSuccess: Ext.emptyFn,
    
    onFailure: Ext.emptyFn
});