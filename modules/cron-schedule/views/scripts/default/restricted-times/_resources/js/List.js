Ext.namespace('CA.Configuration.Admin.Cron.DeniedTimes');

CA.Configuration.Admin.Cron.DeniedTimes.List = Ext.extend(Ext.grid.GridPanel, {

	stateId: 'ca.configuration.admin.cron.deniedtimes.list',

	loadLink: link('admin', 'jobs-denied-times', 'get-all'),

    autoScroll: true,

    layout: 'fit',

    monitorResize: true,

    border: false,

    loadMask: {msg: lang('Loading...')},

    stripeRows: true,

    jobId: null,

    viewConfig: {
        forceFit: true,
        autoFill: true
    },

    initComponent: function() {

        this.columns = [{
            header: lang('Job'),
            dataIndex: 'job_name'
        }, {
            header: lang('Date'),
            dataIndex: 'date',
            renderer: OSDN.util.Format.dateRenderer(OSDN.date.DATE_FORMAT)
        }, {
            header: lang('Start time'),
            dataIndex: 'start_time',
            renderer: OSDN.util.Format.dateRenderer(OSDN.date.TIME_FORMAT)
        }, {
        	header: lang('End time'),
            dataIndex: 'end_time',
            renderer: OSDN.util.Format.dateRenderer(OSDN.date.TIME_FORMAT)
        }];

        this.store = new Ext.data.JsonStore({
	        url: this.loadLink,
	        remoteSort: true,
	        sortInfo: {field: 'name', direction: 'ASC'},
	        root: 'rowset',
	        fields: [
	            {name: 'id'},
	            {name: 'job_id'},
	            {name: 'job_name'},
	            {name: 'date', type: 'date', dateFormat: OSDN.date.DATE_FORMAT_SERVER},
	            {name: 'start_time', type: 'date', dateFormat: OSDN.date.TIME_FORMAT_SERVER},
                {name: 'end_time', type: 'date', dateFormat: OSDN.date.TIME_FORMAT_SERVER}
	        ]
	    });

        this.sm = new Ext.grid.RowSelectionModel({singleSelect: true});

        var actionsPlugin = new OSDN.grid.Actions({
	        autoWidth: true,
	        items: [{
                text: lang('Edit'),
                iconCls: 'edit',
                handler: this.edit.createDelegate(this)
            }, function(g, ri) {
                var record = g.getStore().getAt(ri);
                var bossId = g.getStore().getAt(ri).get('boss_id');
                return (bossId > 0 ? {
                    text: lang('Boss info'),
                    iconCls: 'osdn-reports',
                    handler: function() {g.showBoss(bossId)}
                } : false);
            }, {
                text: lang('Delete'),
                iconCls: 'delete',
                handler: function(g, rowIndex) {
            		CA.Configuration.Admin.Cron.DeniedTimes.Delete({
                        itemId: g.getStore().getAt(rowIndex).get('id'),
                        onsuccess: function (res) {
                        	var errors = Ext.decode(res.responseText).errors;
                        	if (errors && errors.length > 0) {
	                        	OSDN.Msg.error(lang(errors[0].msg));
                                return;
                        	}
                            g.getStore().load();
                        }
                    });
                }
            }]
	    });

	    this.plugins = [actionsPlugin];

        this.tbar = [{
            text: lang('Add'),
            iconCls: 'add',
            handler: this.add.createDelegate(this)
        }];

        this.bbar = new OSDN.PagingToolbar({
            store: this.store,
            displayInfo: true
        });

        CA.Configuration.Admin.Cron.DeniedTimes.List.superclass.initComponent.apply(this, arguments);

        this.addEvents('added', 'updated');

        this.on({
            render: function() {
        		if (this.jobId) {
        			this.getStore().baseParams.jobId = this.jobId;
        		}
                this.getStore().load({params: {start: 0, limit:33}});
            },
            rowdblclick: this.edit,
            scope: this
        });
    },

    add: function() {
		var form = new CA.Configuration.Admin.Cron.DeniedTimes.Form({
			jobId: this.jobId
		});
		var w = form.showInWindow({
            title: lang('Adding')
        });
		form.on('saved', function (){
			this.getStore().load();
			w.close();
		}, this);
	},

	edit: function(g, rowIndex) {
		var form = new CA.Configuration.Admin.Cron.DeniedTimes.Form({
			itemId: g.getStore().getAt(rowIndex).get('id')
		});
		var w = form.showInWindow({title: lang('Editing')});
		form.on('saved', function (){
			g.getStore().load();
			w.close();
		}, this);
	},

    showInWindow: function(cfg) {
        var w = new Ext.Window(Ext.apply({
        	title: lang('Denied periods'),
            width: 600,
            height: 450,
            layout: 'fit',
            resizable: false,
            items: [this],
            modal: true
        }, cfg || {}));
        w.show().center();
        return w;
    }
});

Ext.reg('ca.departments.list', CA.Departments.List);