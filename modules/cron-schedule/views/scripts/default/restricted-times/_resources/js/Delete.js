Ext.namespace('CA.Configuration.Admin.Cron.DeniedTimes');

CA.Configuration.Admin.Cron.DeniedTimes.Delete = function(config) {
	Ext.Msg.show({
        title: lang('Question!'),
        msg: lang('Are you sure?'),
        buttons: Ext.Msg.YESNO,
        icon: Ext.MessageBox.QUESTION,
        fn: function(b) {
            if ('yes' == b) {
                Ext.Ajax.request({
			        url: link('admin', 'jobs-denied-times', 'delete'),
			        success: config.onsuccess || Ext.emptyFn,
			        failure: config.onfailure || Ext.emptyFn,
			        params: {id: config.itemId}
			    });
            }
        }
    });
}