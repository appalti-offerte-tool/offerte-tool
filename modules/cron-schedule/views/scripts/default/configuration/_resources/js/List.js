Ext.define('Module.CronSchedule.Configuration.List', {
    extend: 'Ext.ux.grid.GridPanel',
    cls: 'm-cron-schedule',

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.CronSchledule.Model.Configuration',
            groupField: 'moduleName',
            proxy: {
                type: 'ajax',
                url: link('cron-schedule', 'configuration', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.comboStore = new Ext.data.ArrayStore({
            data: [
                [null, ' '],
                [300, lang('Each {0} minutes', 5)],
                [900, lang('Each {0} minutes', 15)],
                [1800, lang('Each {0} minutes', 30)],
                [3600, lang('Each hour')],
                [21600, lang('Each {0} hours', 6)],
                [43200, lang('Each {0} hours', 12)],
                [86400, lang('Each day')],
                [259200, lang('Each 3 days')],
                [604800, lang('Each week')],
                [2592000, lang('Each month')]
            ],
            fields: [{name: 'time', type: 'int'}, {name: 'timename'}]
        });

        this.features = [{
            ftype: 'grouping',
            groupHeaderTpl: lang('Module') + ': {name}'
        }];

        this.columns = [new Ext.grid.RowNumberer(), {
            header: lang('Module'),
            dataIndex: 'moduleName',
            hidden: true,
            hideable: true
        }, {
            header: lang('Name'),
            dataIndex: 'name',
            width: 300,
            sortable: true
        }, {
            header: lang('Path'),
            dataIndex: 'path',
            flex: 1
        }, {
            xtype: 'datecolumn',
            header: lang('Last run'),
            dataIndex: 'lastRunDatetime',
            width: 150,
            menuDisabled: true,
            format: 'd-m-Y H:i',
            sortable: true
        }, {
            header: lang('Periodicity'),
            dataIndex: 'periodicity',
            width: 150,
            renderer: function (v) {
                var timename;
                this.comboStore.each(function (rec) {
                    if (v == rec.get('time')) {
                        timename = rec.get('timename');
                    }
                })
                return timename;
            },
            scope: this,
            editor: new Ext.form.ComboBox({
                mode: 'local',
                typeAhead: true,
                allowBlank: true,
                store: this.comboStore,
                displayField: 'timename',
                valueField: 'time'
            }),
            menuDisabled: true,
            sortable: true
        }, {
            header: lang('Valid'),
            type: 'booleancolumn',
            dataIndex: 'isValid'
        }, {
            header: lang('Runned'),
            type: 'booleancolumn',
            dataIndex: 'isRunned'

        }, {
            header: lang('Enabled'),
            xtype: 'checkcolumn',
            dataIndex: 'isActive',
            listeners: {
                checkchange: function(cm, rowIndex, isChecked, record) {
                    this.onToggleTaskActiveState(record, isChecked);
                },
                scope: this
            }
        }, {
            xtype: 'actioncolumn',
            header: lang('Action'),
            align: 'center',
            items: [{
                iconCls: 'icon-edit-16  ',
                text: lang('Denied periods'),
                handler: this.doShowDeniedPeriods,
                scope: this
            }, {
                iconCls: 'icon-start-16 icon-16',
                text: lang('Run now'),
                handler: this.onExecuteSingleTask,
                scope: this
            }]
        }];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            plugins: 'pagesize',
            items: ['-', {
                iconCls: 'icon-start-16',
                text: lang('Execute all background tasks'),
                handler: function() {
                    Ext.Msg.confirm(
                        lang('Confirmation'),
                        'This action can couse slow down of the system.<br/>Do you really want to continue?',
                        function(b) {
                            b == 'yes' && this.onExecuteAllTasks();
                        },
                        this
                    );
                },
                scope: this
            }, '-', {
                iconCls: 'icon-restricted-time-16',
                text: lang('Restricted time'),
                handler: this.onRestrictedTimeClick,
                scope: this
            }]
        });

        this.callParent();
    },

    onRestrictedTimeClick: function() {},

    onExecuteSingleTask: function (g, rowIndex) {

        var record = g.getStore().getAt(rowIndex);

        var mask = new Ext.LoadMask(Ext.getBody(), {
            msg: Ext.LoadMask.prototype.msg
        });
        mask.show();

        Ext.Ajax.request({
            url: link('cron-schedule', 'execute', 'run-single-task', {format: 'json'}),
            params: {
                id: record.get('id')
            },
            success: function(response) {

                var decResponse = Ext.decode(response.responseText);
                Application.notificate(decResponse.messages);

                mask.hide();

                if (decResponse.success) {
                    this.getStore().load();
                }

            },
            scope: this
        });
    },

    onExecuteAllTasks: function () {

        var mask = new Ext.LoadMask(Ext.getBody(), {
            msg: Ext.LoadMask.prototype.msg
        });
        mask.show();

        Ext.Ajax.request({
            url: link('cron-schedule', 'execute', 'run-all-tasks', {format: 'json'}),
            success: function(response) {

                mask.hide();

                var decResponse = Ext.decode(response.responseText);
                Application.notificate(decResponse.messages);

                if (decResponse.success) {
                    this.getStore().load();
                }
            },
            failure: function(){
                mask.hide();
            },
            scope: this
        });
    },

    onToggleTaskActiveState: function(record, isChecked) {
        Ext.Ajax.request({
            url: link('cron-schedule', 'configuration', 'update', {format: 'json'}),
            params: {
                id: record.get('id'),
                isActive: +isChecked
            },
            success: function(response) {
                var decResponse = Ext.decode(response.responseText);
                Application.notificate(decResponse.messages);

                if (decResponse.success) {
                    record.commit();
                }
            },
            scope: this
        });
    }
});