Ext.define('Module.CronSchledule.Model.Configuration', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name',
        'module', 'moduleName', 'path', 'periodicity',
        {name: 'lastRunDatetime', type: 'date', dateFormat: 'Y-m-d H:i:s'},
        {name: 'isValid', type: 'boolean'},
        {name: 'isActive', type: 'boolean'},
        {name: 'isRunned', type: 'boolean'}
    ]
});