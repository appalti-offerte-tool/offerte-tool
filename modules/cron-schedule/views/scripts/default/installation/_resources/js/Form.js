Ext.define('Module.CronSchedule.Model.Installation', {
    extend: 'Ext.data.Model',
    fields: ['mailto', 'periodicity', 'isActive']
});

Ext.define('Module.Application.Model.Error', {
    extend: 'Ext.data.Model',
    fields: [
        'message', 'type', 'field',
        {name: 'id', mapping: 'field'},
        {name: 'msg', mapping: 'message'}
    ]
});

Ext.define('Module.CronSchedule.Installation.Form', {
    extend: 'Ext.form.Panel',

    bodyPadding: 5,

    initComponent: function() {

        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.CronSchedule.Model.Installation',
            type: 'json',
            root: 'row'
        });

        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
            model: 'Module.Application.Model.Error',
            type: 'json',
            root: 'messages',
            successProperty: 'success'
        });


        var labelCssDecoration = 'text-align: right;margin-top: -5px;font-style: italic;font-size: 90%';
        this.items = [{
            xtype: 'textfield',
            fieldLabel: 'Period',
            name: 'periodicity',
            allowBlank: false,
            value: '*/10 * * * *',
            anchor: '100%'
        }, {
            xtype: 'displayfield',
            hideEmptyLabel: false,
            value: '<div style="' + labelCssDecoration + '">Cron period for this domain (format: * * * * *)</div>'
        }, {
            xtype: 'textfield',
            fieldLabel: 'Email',
            allowBlank: false,
            name: 'mailto',
            vtype: 'email',
            anchor: '100%'
        }, {
            xtype: 'displayfield',
            hideEmptyLabel: false,
            value: '<div style="' + labelCssDecoration + '">Email where error messages will be send to</div>'
        }, {
            xtype: 'checkbox',
            fieldLabel: 'Active',
            name: 'isActive'
        }];

        this.callParent();
    },

    doLoad: function() {

        this.form.load({
            url: link('cron-schedule', 'installation', 'fetch', {format: 'json'}),
            waitMsg: Ext.LoadMask.prototype.msg,
        })
    },

    onCronScheduleSubmit: function(panel, wnd) {

        if (!this.form.isValid()) {
            return;
        }

        this.form.submit({
            url: link('cron-schedule', 'installation', 'install', {format: 'json'}),
            waitMsg: Ext.LoadMask.prototype.msg,
            success: function(options, action) {
                Application.notificate(action.result.messages);

                wnd.close();
            }
        });
    },

    showInWindow: function() {

        var w = this.wnd = new Ext.Window({
            title:  'Cron schedule installation',
            iconCls: 'm-cron-schedule-icon-16',
            resizable: false,
            width: 415,
            modal: true,
            border: false,
            items: [this],
            buttons: [{
                iconCls: 'icon-submit-16',
                text: lang('Submit'),
                handler: function() {
                    this.onCronScheduleSubmit(this, w);
                },
                scope: this
            }, {
                text: lang('Cancel'),
                handler: function() {
                    w.close();
                }
            }],
            tools: [{
                id: 'help',
                handler: function() {
                    window.open('http://en.wikipedia.org/wiki/Cron');
                }
            }],
            scope: this
        });

        w.show();
        this.doLoad();
        return w;
    }
});