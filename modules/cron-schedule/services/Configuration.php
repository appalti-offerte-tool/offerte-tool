<?php

/**
 * CronSchedule_Service_Configuration
 *
 * @version $Id: Configuration.php 1849 2012-03-01 12:15:45Z yaroslav $
 */
class CronSchedule_Service_Configuration extends OSDN_Application_Service_Dbable
{
    /**
     * @var CronSchedule_Model_DbTable_Configuration
     */
    protected $_table;

    protected function _init()
    {
        $this->_table = new CronSchedule_Model_DbTable_Configuration($this->getAdapter());

        parent::_init();
    }

    public function find($id, $throwException = true)
    {
        $csConfigurationRow = $this->_table->findOne($id);
        if (null === $csConfigurationRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find row #' . $id);
        }

        return $csConfigurationRow;
    }

    public function fetchAllWithResponse(
        array $params = array(),
        $periodicity = CronSchedule_Model_DbTable_Configuration::PERIODICITY_MULTIPLE,
        $isActive = null
    ) {
        $select = $this->_table->select(true);
        $select->where('periodicity = ?', $periodicity, Zend_Db::INT_TYPE);

        if (null !== $isActive) {
            $select->where('isActive = ?', (int) $isActive, Zend_Db::INT_TYPE);
        }

        $this->_initDbFilter($select)->parse($params);
        return $this->getDecorator('response')->decorate($select, $this->_table);
    }

    public function update($id, array $data)
    {
        unset($data['module']);

        $cronScheduleRow = $this->find($id);
        $this->_attachValidationRules('update', array(
            'id'          => array('id', 'presence' => 'required', 'allowEmpty' => false),
            'isActive'    => array('boolean', 'presence' => 'required', 'allowEmpty' => false)
        ));

        $f = $this->_validate($data);
        $cronScheduleRow->setFromArray($f->getData());
        $cronScheduleRow->save();

        return $cronScheduleRow;
    }

    protected function _validateData(array $data)
    {
        $response = new OSDN_Response();
        if (empty($data['className']) || !class_exists($data['className'])) {
            $response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::INPUT_PARAMS_INCORRECT, 'className'));
            return $response;
        }

        if (empty($data['methodName'])) {
            $response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::INPUT_PARAMS_INCORRECT, 'methodName'));
            return $response;
        }
        if (empty($data['params'])) {
            $data['params'] = array();
        }
        $response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::OK));
        return $response;
    }

    /**
     * @FIXME Add validation
     *
     * @return \CronSchedule_Model_Task
     */
    public function create(array $data)
    {
        $configurationRow = $this->_table->createRow();
        $configurationRow->setFromArray($data);

        $configurationRow->save();

        return $configurationRow;
    }

    public function fetchWaitForRunning(array $params = array())
    {
        $todayTime = time() + 300;
        $today = getdate();
        $seconds = ($today['hours'] * 60 + $today['minutes']) * 60 + $today['seconds'];

        $response = new OSDN_Response();
        $select = $this->_table->getAdapter()->select()
            ->from($this->_table->getTableName())
            ->where('(`type` = ? ', self::TYPE_PERIODIC)
            ->where('period IS NOT NULL')
            ->where('last_run + period < ?', $todayTime)
            ->where('`time` IS NULL OR ((`time` - 300 < ?) AND (`time` + 300 > ?)))', $seconds)
            ->orWhere('(`type` = ?', self::TYPE_SINGLE)
            ->where('runtime < ? )', time());

        $plugin = new OSDN_Db_Plugin_Select($this->_table, $select);
        $select = $plugin->parse($params);
        try {
            $rowset = $query = $select->query()->fetchAll();
            $response->setRowset($rowset);
            $response->totalCount = $plugin->getTotalCount();
        } catch (Exception $e) {
            return $response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::FAILURE));
        }
        return $response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::OK));
    }
}