<?php

class CronSchedule_Service_Execute extends OSDN_Application_Service_Dbable
{
    /**
     * @var CronSchedule_Model_DbTable_Configuration
     */
    protected $_table;

    /**
     * @var Module_Instance_Manager
     */
    protected $_mm;

    public function __construct(Module_Instance_Manager $mm)
    {
        $this->_mm = $mm;
        $this->_table = new CronSchedule_Model_DbTable_Configuration($this->getAdapter());

        parent::__construct();
    }

    public function execute()
    {
        $configuration = new CronSchedule_Service_Configuration();
        $response = $configuration->fetchAllWithResponse(
            array(),
            CronSchedule_Model_DbTable_Configuration::PERIODICITY_MULTIPLE,
            true
        );

        $response->setScope($this);
        $response->setRowsetCallback(function($row, $rowset, $me) {

            if ($row->isRunned) {
                return false;
            }

            $me->executeTask($row);
        });

        /**
         * Execute the rowset callback handler
         */
        $rowset = $response->getRowset();
    }

    public function executeTask(CronSchedule_Model_Task $taskRow)
    {
        $information = $this->_mm->fetch($taskRow->module);

        $taskRow->setRunned(true);
        $taskRow->save();

        $path = $information->getRoot() . DIRECTORY_SEPARATOR . $taskRow->path;
        file_put_contents(
            APPLICATION_PATH . '/data/logs/cron-schedule.log',
            sprintf("%20s | %5s | %s\n", date('Y-m-d H:i:s'), 'START', $path),
            FILE_APPEND
        );

        $result = @ system(sprintf('php %s', $path), $o);

        $taskRow->setRunned(false);
        $taskRow->save();

        file_put_contents(
            APPLICATION_PATH . '/data/logs/cron-schedule.log',
            sprintf("%20s | %5s | %s\n", date('Y-m-d H:i:s'), empty($result) ? 'OK' : 'ERROR', $path),
            FILE_APPEND
        );
    }
}