<?php

/**
 * CA Jobs Denied times class
 *
 * @category CA
 * @package CronSchedule_Service_CronSchedule
 */
class CronSchedule_Service_CronSchedule_DeniedTimes
{
	/**
	 * 
	 * @var CronSchedule_Service_CronSchedule_DeniedTimes_Table
	 */
	protected $_table = null;
	
	/**
	 * Constructor
	 */
    public function __construct() 
    {
        $this->_table = new CronSchedule_Service_CronSchedule_DeniedTimes_Table();
    }
    
    /**
     * Retrieve all times 
     * 
     * @param $jobId
     * 
     * @retrun OSDN_Response
     */
    public function getAllTimes($jobId = null)
    {
    	$response = new OSDN_Response();
    	$rowset = array();
    	if (!empty($jobId)) {
    		$validate = new OSDN_Validate_Id();
	        if (!$validate->isValid($jobId)) {
	            return $response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::INPUT_PARAMS_INCORRECT, 'job_id'));
	        }
    	}
    	
    	$adapter = $this->_table->getDefaultAdapter();
        $prefix =  $this->_table->getPrefix();
        $select = $adapter->select()->from(array(
	        	'd' => $this->_table->getTableName()
	        ))
	        ->joinLeft(array('j' => $prefix . 'jobs'), 'j.id = d.job_id', array(
				'job_name' => 'name'
	        ));
		if (!empty($jobId)) {
			$select->where('d.job_id = ? OR d.job_id IS NULL', $jobId);
		}
		
		$rowset = $select->query()->fetchAll();
    	$response->setRowset($rowset);
    	$response->setSuccess();
    	return $response;
    }

    /**
     * Return denied times rows 
     * 
     * @param int $jobId
     * @return OSDN_Response
     */
    public function getDeniedTimes($jobId = null)
    {
    	$response = new OSDN_Response();
    	$rowset = array();
    	if (!empty($jobId)) {
    		$validate = new OSDN_Validate_Id();
	        if (!$validate->isValid($jobId)) {
	            return $response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::INPUT_PARAMS_INCORRECT, 'job_id'));
	        }
	    	$rowset = array_merge($rowset, $this->_table->fetchAll(array(
	    		'job_id = ?' => $jobId
	    	)));
    	}
    	$rowset = array_merge($rowset, $this->_table->fetchAll(array(
    		'job_id IS NULL'
    	)));
    	$response->setRowset($rowset);
    	$response->setSuccess();
    	return $response;
    }
    
    /**
     * Check if job allowed to run now 
     * 
     * @param int $jobId
     * @return OSDN_Response
     */
    public function isJobAllowed($jobId = null)
    {
    	$response = new OSDN_Response();
    	if (!empty($jobId)) {
    		$validate = new OSDN_Validate_Id();
	        if (!$validate->isValid($jobId)) {
	            return $response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::INPUT_PARAMS_INCORRECT, 'job_id'));
	        }
    	}
    	$date = date('Y:m:d');
    	$time = date('H:i:s');
    	$adapter = $this->_table->getDefaultAdapter();
    	$select = $adapter->select()
    		->from($this->_table->getTableName(), array(
    			'c' => new Zend_Db_Expr('COUNT(*)')
			));
		if (empty($jobId)) {
			$select->where('job_id IS NULL');
		} else {
			$select->where('job_id IS NULL OR job_id = ?', $jobId);
		}
		$select->where('`date` IS NULL OR `date` = ?', $date)
			->where('`start_time` IS NULL OR `start_time` <= ?', $time)
			->where('`end_time` IS NULL OR `end_time` >= ?', $time);
			
		$count = $select->query()->fetchColumn(0);
		$response->count = $count;
		$response->allowed = $count == 0;
		$response->setSuccess();
    	return $response;
		
    }
    
    /**
     * Check passed data
     * 
     * @param struct $data
     * @param int $id
     */
    protected function _checkData(array $data, $id = null)
    {
    	$response = new OSDN_Response();
    	
    	if (empty($data['date']) && empty($data['start_time']) && empty($data['end_time'])) {
    		return $response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::INPUT_PARAMS_INCORRECT, 'start_time'));
    	}    	
    	
//    	if (!empty($id)) {
//    		$validate = new OSDN_Validate_Id();
//	        if (!$validate->isValid($id)) {
//	            return $response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::INPUT_PARAMS_INCORRECT, 'id'));
//	        }
//    	}
//    	

//    	
//    	$adapter = $this->_table->getDefaultAdapter();
//    	$select = $adapter->select()
//    		->from($this->_table->getTableName());
//		if (empty($data['job_id'])) {
//			$select->where('job_id IS NULL');
//		} else {
//			$select->where('job_id IS NULL OR job_id = ?', intval($data['job_id']));
//		}
//		$select->where('id IS NULL');
//		if (empty($id)) {
//			$select->where('id != ?', intval($id));
//		}
//		if (empty($data['date'])) {
//			$select->where('`date` IS NULL');
//		} else {
//			$select->where('`date` IS NULL OR `date` = ?', $data['date']);
//		}
//    	if (empty($data['start_time'])) {
//			$select->where('`start_time` IS NULL');
//		} else {
//			$select->where('`start_time` IS NULL OR `start_time` <= ?', $data['start_time']);
//		}
//    	if (empty($data['end_time'])) {
//			$select->where('`end_time` IS NULL');
//		} else {
//			$select->where('`end_time` IS NULL OR `end_time` >= ?', $data['end_time']);
//		}
//		
//		$rowset = $select->query()->fetchAll();
//		$response->setRowset($rowset);
		$rowset = array();
		$response->overlapped = count($rowset) > 0;
		if ($response->overlapped) {
			$response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::TIME_OVERLAPPED, 'start_time'));
		} else {
			$response->setSuccess();
		}
    	return $response;
    } 
    
    /**
     * Fetch denied time by id
     * 
     * @param int $id
     * @return OSDN_Response 
     */
    public function fetch($id)
    {
    	$response = new OSDN_Response();
    	if (!empty($id)) {
    		$validate = new OSDN_Validate_Id();
	        if (!$validate->isValid($id)) {
	            return $response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::INPUT_PARAMS_INCORRECT, 'id'));
	        }
    	}
    	
    	$row = $this->_table->fetchRow($id);
    	if ($row) {
    		$response->setRow($row->toArray());
    	}
    	$response->setSuccess();
    	return $response;
    }
    
    /**
     * Insert denied time
     * 
     * @param array $data
     */
    public function insert(array $data)
    {
    	$response = $this->_checkData($data);
    	if ($response->isError()) {
    		return $response;
    	}
    	$response = new OSDN_Response();
    	$id = $this->_table->insert($data);
    	if ($id === false) {
    		$response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::DATABASE_ERROR));
    		return $response;
    	}
    	$response->id = $id;
    	$response->setSuccess();
    	return $response;
    }
    
    /**
     * Update denied time by id
     *
     * @param int $id
     * @param array $data
     */
    public function update($id, array $data)
    {
    	$response = new OSDN_Response();
    	if (!empty($id)) {
    		$validate = new OSDN_Validate_Id();
	        if (!$validate->isValid($id)) {
	            return $response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::INPUT_PARAMS_INCORRECT, 'id'));
	        }
    	}
    	$response = $this->_checkData($data, $id);
    	if ($response->isError()) {
    		return $response;
    	}
		$affectedRows = $this->_table->updateByPk($data, $id);
        return $response->addStatus(new OSDN_Response_Status_Storage(
            OSDN_Response_Status_Storage::retrieveAffectedRowStatus($affectedRows)));
    }
    
    /**
     * Delete by id
     * 
     * @param int $id
     */
    public function delete($id)
    {
    	$response = new OSDN_Response();
    	if (!empty($id)) {
    		$validate = new OSDN_Validate_Id();
	        if (!$validate->isValid($id)) {
	            return $response->addStatus(new CronSchedule_Service_CronSchedule_Status(CronSchedule_Service_CronSchedule_Status::INPUT_PARAMS_INCORRECT, 'id'));
	        }
    	}
		$affectedRows = $this->_table->deleteByPk($id);
        return $response->addStatus(new OSDN_Response_Status_Storage(
            OSDN_Response_Status_Storage::retrieveAffectedRowStatus($affectedRows)));
    }
}