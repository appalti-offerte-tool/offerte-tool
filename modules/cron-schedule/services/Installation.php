<?php

/**
 * CronSchedule_Service_Installation
 *
 * @version $Id: Installation.php 1795 2012-02-10 11:57:09Z yaroslav $
 */
class CronSchedule_Service_Installation extends OSDN_Application_Service_Abstract
{
    protected function _init()
    {
        $this->_attachValidationRules('default', array(
            'mailto'        => array('EmailAddress', 'presence' => 'required', 'allowBlank' => false),
            'periodicity'   => array('presence' => 'required', 'allowBlank' => false)
        ));
    }

    /**
     * @FIXME Define company name
     */
    protected function _toCompanyName()
    {
        return $_SERVER['HTTP_HOST'];
    }

    public function fetch()
    {
        $crontab = new CronSchedule_Service_Crontab();

        $crontabRow = $crontab->readForCompany($this->_toCompanyName());
        return $crontabRow;
    }

    public function install(array $params)
    {
        $f = $this->_validate($params);

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $mm =$bootstrap->getResource('ModulesManager');
        $information = $mm->fetch('cron-schedule');

        $pathToExecuteFile = $information->getRoot() . '/bin/execute.php';

        $arguments = $f->isActive ? array(
            CronSchedule_Service_Crontab::CRON_ASSIGN => $f->mailto,
            CronSchedule_Service_Crontab::CRON_CMD    => sprintf('%s php %s', $f->periodicity, $pathToExecuteFile)
        ) : array();

        /**
         * @FIXME
         */
        $crontab = new CronSchedule_Service_Crontab();
        $crontab->resetAndUpdateForCompany($this->_toCompanyName(), $arguments);

        return true;
    }
}