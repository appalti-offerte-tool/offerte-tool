<?php

interface CronSchedule_Service_Crontab_Adapter_Line_Interface
{
    public function getType();
    
    function toString();
    
    function __toString();
}