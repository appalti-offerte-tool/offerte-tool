<?php

/**
 * CronSchedule_Service_Crontab_Adapter_Line_Special
 *
 * @version $Id: Special.php 1793 2012-02-09 16:25:56Z yaroslav $
 */
class CronSchedule_Service_Crontab_Adapter_Line_Special implements CronSchedule_Service_Crontab_Adapter_Line_Interface
{
    protected $_elements;

    public function __construct($line)
    {
        $this->setElements($line);

    }

    /**
     * return type of line
     *
     * @return string
     */
    public function getType()
    {
        return 'special';
    }

    /**
     * set elements
     *
     * @param array | string $line
     *
     * @return array
     */
    public function setElements($line)
    {
        $this->_elements = is_array($line)? $line: split("[ \t]", $line, 2);
    }

    /**
     * get elements
     *
     * @return array
     */
    public function getElements()
    {
        return $this->_elements;
    }

    /**
     * convert object value to string
     *
     * @return string
     */
    public function toString()
    {
        return implode(" ", $this->getElements());
    }

    /**
     * convert object value to string
     *
     * @return string
     */
    function __toString()
    {
        return $this->toString();
    }
}

