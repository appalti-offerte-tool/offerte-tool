<?php

/**
 * CronSchedule_Service_Crontab_Adapter_Line_Empty
 *
 * @version $Id: Empty.php 1793 2012-02-09 16:25:56Z yaroslav $
 */
class CronSchedule_Service_Crontab_Adapter_Line_Empty implements CronSchedule_Service_Crontab_Adapter_Line_Interface
{
    /**
     * return type of line
     *
     * @return string
     */
    public function getType()
    {
        return 'empty';
    }

    /**
     * convert object value to string
     *
     * @return string
     */
    public function toString()
    {
        return "\n";
    }

    /**
     * convert object value to string
     *
     * @return string
     */
    function __toString()
    {
        return "\n";
    }
}

