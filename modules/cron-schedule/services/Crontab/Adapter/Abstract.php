<?php

/**
 * @version $Id: Abstract.php 1793 2012-02-09 16:25:56Z yaroslav $
 */
abstract class CronSchedule_Service_Crontab_Adapter_Abstract
{
    protected $_debug = false;

    /**
     * Collection container
     *
     * @var OSDN_Collection_ArrayList
     */
    protected $_collection;

    public function __construct(array $configs = array())
    {
        $this->_collection = new OSDN_Collection_ArrayList();
    }

    public function setDebug($flag)
    {
        $this->_debug = (boolean) $flag;
        return $this;
    }

    abstract function read();

    abstract function write();

    abstract function append();

    abstract function add(CronSchedule_Service_Crontab_Adapter_Line_Interface $type);

    abstract function addMultiple(array $tasks);

    abstract function deleteById($id);

    abstract function clear();
}