<?php

/**
 * Class for using the schedule mechanizm
 *
 * @category		OSDN
 * @package		OSDN_Cron
 * @version		$Id: Cron.php 1795 2012-02-10 11:57:09Z yaroslav $
 */
class OSDN_Cron
{
    /**
     * Create the cron instance
     *
     * @param string $adapter
     * @return CronSchedule_Service_Interface
     */
    public static function factory($adapter = 'Unix')
    {
        $adapter = ucfirst(strtolower($adapter));
        $adapterNamespace = 'CronSchedule_Service_';
        $adapterCls = $adapterNamespace . $adapter;
        $adapterInstance = new $adapterCls();
        return $adapterInstance;
    }
}