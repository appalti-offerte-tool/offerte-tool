<?php

class CronSchedule_Model_Task extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    public function refreshLastRunDatetime(\DateTime $dt = null)
    {
        if (null === $dt) {
            $dt = new DateTime();
        }

        $this->lastRunDatetime = $dt->format('Y-m-d H:i:s');
    }

    public function setRunned($isRunned)
    {
        $isRunned = (int) (boolean) $isRunned;
        $this->isRunned = $isRunned;

        if (! $isRunned) {
            $this->refreshLastRunDatetime();
        }
    }
}