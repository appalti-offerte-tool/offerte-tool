<?php

class CronSchedule_Model_DbTable_RestrictedTime extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'cronScheduleRestrictedTime';

    protected $_nullableFields = array(
        'job_id',
        'date',
        'startTime',
        'endTime'
    );
}