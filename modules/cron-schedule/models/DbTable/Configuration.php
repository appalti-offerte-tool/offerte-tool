<?php

class CronSchedule_Model_DbTable_Configuration extends \OSDN_Db_Table_Abstract
{
    const PERIODICITY_MULTIPLE = 1;
    const PERIODICITY_SINGLE   = 2;

    protected $_primary = 'id';

    protected $_name = 'cronScheduleConfiguration';

    protected $_rowClass = '\\CronSchedule_Model_Task';

    protected $_serializableColumns = array(
        'data'
    );

    protected $_nullableFields = array(
        'path', 'data',
        'period', 'lastRunDatetime'
    );
}