<?php

define('SILENT_MODE', true);

/**
 * @var Zend_Application
 */
$application = require __DIR__ . '/../../../index.php';

/**
 * Here we create the bootstrap for get
 * easy access for all configured caches
 *
 * @var Zend_Application_Bootstrap_Bootstrap
 */
$application->bootstrap();

$bootstrap = $application->getBootstrap();

$mm = $bootstrap->getResource('ModulesManager');

set_time_limit(0);

file_put_contents(
    APPLICATION_PATH . '/data/logs/cron-schedule.log',
    sprintf("%20s | INIT\n", date('Y-m-d H:i:s')),
    FILE_APPEND
);

$execute = new CronSchedule_Service_Execute($mm);
$execute->execute();