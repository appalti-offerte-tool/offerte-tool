<?php

class CronSchedule_ConfigurationController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var CronSchedule_Service_Configuration
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'cron-schedule:configuration';
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_service = new CronSchedule_Service_Configuration();

        $this->_helper->ajaxContext()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('update', 'json')
            ->initContext();

        parent::init();
    }

    public function indexAction()
    {}

    public function fetchAllAction()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        $mm = $bootstrap->getResource('ModulesManager');

        $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
        $response->setRowsetCallback(function($row) use ($mm) {

            $information = $mm->fetch($row->module);

            $aRow = $row->toArray();
            $aRow['moduleName'] = $information->getTitle();

            return $aRow;
        });
        $this->view->assign($response->toArray());
    }

    public function updateAction()
    {
        try {
            $this->_service->update($this->_getParam('id'), $this->_getAllParams());
            $this->_helper->information('Updated successfully.', array(), E_USER_NOTICE);
            $this->view->success = true;
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessage());
        }
    }
}