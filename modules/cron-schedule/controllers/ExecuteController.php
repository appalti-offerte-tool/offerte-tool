<?php

class CronSchedule_ExecuteController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var CronSchedule_Service_Execute
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'cron-schedule:execute';
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        $mm = $bootstrap->getResource('ModulesManager');

        $this->_service = new CronSchedule_Service_Execute($mm);

        $this->_helper->ajaxContext()
            ->addActionContext('run-all-tasks', 'json')
            ->addActionContext('run-single-task', 'json')
            ->initContext();

        parent::init();
    }

    public function runSingleTaskAction()
    {
        $configuration = new CronSchedule_Service_Configuration();

        try {

            $taskRow = $configuration->find($this->_getParam('id'));

            $this->_service->executeTask($taskRow);
            $this->view->success = true;
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function runAllTasksAction()
    {
        try {
            $this->_service->execute();
            $this->view->success = true;
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}