<?php

class CronSchedule_RestrictedTimeController extends Zend_Controller_Action
{
    public function permission(OSDN_Controller_Action_Helper_Acl $acl)
    {
        $acl->setResource(OSDN_Acl_Resource_Generator::getInstance()->admin->configuration);

        $acl->isAllowed(OSDN_Acl_Privilege::VIEW, 'get-all');
        $acl->isAllowed(OSDN_Acl_Privilege::VIEW, 'get');
        $acl->isAllowed(OSDN_Acl_Privilege::ADD, 'insert');
        $acl->isAllowed(OSDN_Acl_Privilege::UPDATE, 'update');
        $acl->isAllowed(OSDN_Acl_Privilege::DELETE, 'delete');

        $this->_deniedTimes = new CronSchedule_Service_CronSchedule_DeniedTimes();
    }

    public function getAllAction()
    {
        $response = $this->_deniedTimes->getAllTimes($this->_getParam('jobId'));
        if ($response->isError()) {
            $this->_collectErrors($response);
            $this->view->success = false;
            return;
        }
        $this->view->rowset = $response->getRowset();
        $this->view->success = true;
    }

    public function getAction()
    {
        $response = $this->_deniedTimes->fetch($this->_getParam('id'));
        $this->_collectErrors($response);
        $this->view->data = $response->getRow();
    }

    public function insertAction()
    {
        $response = $this->_deniedTimes->insert($this->_getAllParams());
        $this->_collectErrors($response);
        $this->view->overlapped = $response->overlapped ? true : false;
        $this->view->rowset = $response->getRowset();

    }

    public function updateAction()
    {
        $response = $this->_deniedTimes->update($this->_getParam('id'), $this->_getAllParams());
        $this->_collectErrors($response);
        $this->view->overlapped = $response->overlapped ? true : false;
        $this->view->rowset = $response->getRowset();
    }

    public function deleteAction()
    {
        $response = $this->_deniedTimes->delete($this->_getParam('id'));
        $this->_collectErrors($response);
    }

}