<?php

/**
 * @version $Id: InstallationController.php 1796 2012-02-10 15:05:50Z yaroslav $
 */
class CronSchedule_InstallationController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var CronSchedule_Service_Configuration
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'cron-schedule:configuration';
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_service = new CronSchedule_Service_Installation();

        $this->_helper->ContextSwitch()
            ->addActionContext('install', 'json')
            ->addActionContext('fetch', 'json')
            ->initContext();

        parent::init();
    }

    public function fetchAction()
    {
        try {
            $row = $this->_service->fetch();

            if (!empty($row)) {
                $row['isActive'] = 1;
                $this->view->row = $row;
            }

            $this->view->success = true;
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }

    public function installAction()
    {
        try {
            $this->_service->install($this->_getAllParams());
            $this->view->success = true;
            $this->_helper->information('Updated successfully.', array(), E_USER_NOTICE);
        } catch(OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }
}