<?php

class CronSchedule_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('cronScheduleConfiguration');
        $this->createColumn('cronScheduleConfiguration', 'periodicity', self::TYPE_INT, 11, '1', true);
        $this->createColumn('cronScheduleConfiguration', 'name', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('cronScheduleConfiguration', 'module', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('cronScheduleConfiguration', 'path', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('cronScheduleConfiguration', 'data', self::TYPE_TEXT, 255, null, false);
        $this->createColumn('cronScheduleConfiguration', 'lastRunDatetime', self::TYPE_DATETIME, null, null, false);
        $this->createColumn('cronScheduleConfiguration', 'period', self::TYPE_INT, 11, null, true);
        $this->createColumn('cronScheduleConfiguration', 'isActive', self::TYPE_INT, 11, '1', true);

        $this->createUniqueIndexes('cronScheduleConfiguration', array('name'), 'UX_name');
    }

    public function down()
    {
        $this->dropTable('cronScheduleConfiguration');
    }
}