<?php

class CronSchedule_Migration_20120301_135650_21 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createColumn('cronScheduleConfiguration', 'isRunned', self::TYPE_INT, 1, '0', true);
    }

    public function down()
    {
        $this->dropColumn('cronScheduleConfiguration', 'isRunned');
    }
}