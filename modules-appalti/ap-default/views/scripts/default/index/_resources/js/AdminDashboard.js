;(function($, win) {
    var AdminDashboard = function() {
        $(this.init);
    }

    function getModalContainer() {
        if (!ad.modalCnt) {
            ad.modalCnt = $('<div class="modal" id="ticket-details-cnt" />').appendTo('body');
        }

        return ad.modalCnt;
    }

    AdminDashboard.prototype = {
        modalCnt: null,

        init: function() {
            $('#filter').change(function() {
                if (!!this.value) {
                    $(this).parent().submit();
                }
            });

            $('.tickets-list')
                .on('click', '.view-ticket', function() {
                    $.get(this.href, {format: 'json'}, function(html) {
                        getModalContainer().html(html).modal('show');
                    })
                    return false;
                });
        }
    }

    win.AdminDashboard = new AdminDashboard();
    var ad = win.AdminDashboard;
})(jQuery, window);