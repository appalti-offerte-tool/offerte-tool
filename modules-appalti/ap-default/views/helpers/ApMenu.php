<?php

final class ApDefault_View_Helper_ApMenu extends \Zend_View_Helper_Abstract
{
    protected $_navigation;

    protected $_admin = array(
        '/dashboard'         => array(
            '/support-ticket/assign-writer'     => array(),
            '/faq'                              => array(),
            '/company-administration/management'  => array(),
            '/dashboard/price'  => array(),
        ),

        '/text-writer' => array(
            '/text-writer/details' => array(
                '/support-ticket/support-ticket-details'     => array(),
                '/support-ticket/moderator-support-ticket-details'     => array(),
            ),
        ),

        '/company'           => array(
            '/company/create'                     => array(),
            '/company/client'                     => array(
                '/proposal/client-proposal-details'    => array(),
                '/proposal/client-proposal-change'    => array(),
                '/proposal/create'            => array(),
                '/proposal/update'            => array(),
                '/proposal/blocks'            => array(),
                '/proposal/personal-intro'    => array(),
                '/proposal/price'             => array(),
                '/proposal/summary'           => array(),
                '/proposal/information'       => array()
            ),
            '/company/client-detail-information'  => array(),
            '/company/client-create'              => array(),
            '/company/index/update'               => array(),
        ),

        '/news-and-events/news-events'      => array(
            '/news-and-events/news-events/news-preview'    => array(),
            '/news-and-events/news-events/news-update'    => array(),
            '/news-and-events/news-events/news-create'    => array(),
            '/news-and-events/news-events/event-preview'    => array(),
            '/news-and-events/news-events/event-update'    => array(),
            '/news-and-events/news-events/event-create'    => array(),
        ),
        '/proposal-template'   => array(),
        '/translations'   => array(),
        '/configuration/admin'   => array(
            '/configuration/admin/summary'   => array()

        ),
    );

    protected $_textWriter = array(
        '/dashboard'         => array(
            '/support-ticket/writer-support-ticket-details' => array()
        ),

    );

    protected $_companyOwner = array(
        '/dashboard'           => array(
            '/support-ticket/support-ticket-details' => array(),
            '/news/list' => array(
                '/news/list/preview' => array(),
            ),
            '/events/list' => array(
                '/events/list/preview' => array(),
            ),
        ),
        '/proposal-block'    => array(),
        '/proposal'          => array(
            '/proposal/create'            => array(),
            '/proposal/update'            => array(),
            '/proposal/blocks'            => array(),
            '/proposal/personal-intro'    => array(),
            '/proposal/price'             => array(),
            '/proposal/summary'           => array(),
            '/proposal/information'       => array()
        ),
        '/company'           => array(
            '/company/create'                     => array(),
            '/company/client'                     => array(
                'company/index/update/'  => array(),
            ),
            '/company/client-detail-information'  => array(),
        ),

        '/company-offer'     => array(),
        '/faq'               => array(),
        '/administration'   => array(
            '/administration/company-detail-information'    => array(),
            '/administration/company-information'           => array(),
            '/administration/proposal-template-defaults'    => array(),
            '/administration/proposal-template'             => array(),
            '/administration/company-create-template'       => array(),
            '/administration/create-template'               => array(),
            '/administration/proposal-typography'           => array(),
            '/administration/company-summary'               => array(),
            '/administration/management'                    => array(),
            '/administration/cart'                          => array(),
        )
    );

    protected $_manager = array(
        '/dashboard'           => array(
            '/support-ticket/support-ticket-details' => array(),
            '/news/list' => array(
                '/news/list/preview' => array(),
            ),
            '/events/list' => array(
                '/events/list/preview' => array(),
            ),
        ),
        '/proposal-block'    => array(),
        '/proposal'          => array(
            '/proposal/create'            => array(),
            '/proposal/update'            => array(),
            '/proposal/blocks'            => array(),
            '/proposal/personal-intro'    => array(),
            '/proposal/price'             => array(),
            '/proposal/summary'           => array(),
            '/proposal/information'       => array()
        ),
        '/company'           => array(
            '/company/create'                     => array(),
            '/company/client'                     => array(
                'company/index/update/'  => array(),
            ),
            '/company/client-detail-information'  => array(),
        ),

        '/company-offer'     => array(),
        '/faq'               => array(),
    );

    protected $_proposalBuilder = array(
        '/dashboard'         => array(
            '/support-ticket/support-ticket-details' => array(),
            '/news/list' => array(
                '/news/list/preview' => array(),
            ),
            '/events/list' => array(
                '/events/list/preview' => array(),
            ),
        ),
        '/proposal-block'    => array(),
        '/proposal'          => array(
            '/proposal/create'            => array(),
            '/proposal/update'            => array(),
            '/proposal/blocks'            => array(),
            '/proposal/personal-intro'    => array(),
            '/proposal/price'             => array(),
            '/proposal/summary'           => array(),
            '/proposal/information'       => array()
        ),
//        '/company'           => array(
//            '/company/create'            => array(),
//            '/company/client'            => array(),
//            '/company/client-detail-information'  => array(),
//        ),
//        '/company-offer'     => array(),
        '/faq'               => array(),
    );

    protected $_membership = array(
        '/dashboard'    => array(),
        '/administration'    => array()
    );

    private $__cache = null;

    public function __construct()
    {
        $navigation = new Menu_Service_Navigation();
        $this->_navigation = $navigation->fetchAllWithModuleInformation();
    }

    final public function ApMenu()
    {
        if (null === $this->__cache) {
            $account = Zend_Auth::getInstance()->getIdentity();

            $menu = null;

            $acl = $account->getAcl();
            $roleRow = $account->getRole();
            switch($roleRow->luid ?: $roleRow->name) {
                case 'admin':
                    $menu = $this->_admin;
                    break;

                case 'textWriter':
                    $menu = $this->_textWriter;
                    break;

                case 'proposalBuilder':
                    $menu = $this->_proposalBuilder;
                    break;

                case 'companyOwner':
                    $menu = $this->_companyOwner;
                    break;

                case 'manager':
                    $menu = $this->_manager;
                    break;

                case 'membership':
                    $menu = $this->_membership;
                    break;

                default:
                    $acl = $account->getAcl();
                    $menu = array('/dashboard' => array());
                    if ($account->acl('text-writer'))
                        $menu['/text-writer'] = array();

                    if ($account->acl('company'))
                        $menu['/company'] = array();

                    if ($account->acl('news')|| $account->acl('event'))
                        $menu['/news-and-events/news-events'] = array();

                    if ($account->acl('proposal', 'template','font'))
                        $menu['/proposal-template'] = array();

                    if ($account->acl('il8n','translation'))
                        $menu['/translations'] = array();

                    if ($account->acl('configuration','admin'))
                        $menu['/configuration/admin'] = array();

//                    if ($account->acl('company', 'library'))
//                        $menu['/company/library'] = array();

                    if ($account->acl('proposal'))
                        $menu['/proposal'] = array();

                    if ($account->acl('company'))
                        $menu['/company-offer'] = array();

                    if ($account->acl('faq'))
                        $menu['/faq'] = array();

                    if ($account->acl('company','company'))
                        $menu['/administration'] = array();
            }
            $pages = $this->_toMenu($menu);
            $this->__cache = new Zend_Navigation($pages);
        }
        return $this->__cache;
    }

    protected function _toMenu(array $scheme)
    {
        $output = array();

        foreach($scheme as $name => $oscheme) {

            $idx = false;

            foreach($this->_navigation as $o) {
                if ($o['path'] == $name) {

                    $idx = count($output);

                    $output[] = array(
                        'class'         => isset($o['class']) ? $o['class'] : '',
                        'label'         => $o['text'],
                        'module'        => $o['module'],
                        'controller'    => isset($o['controller']) ? $o['controller'] : 'index',
                        'action'        => isset($o['action']) ? $o['action'] : 'index',
                        'params'        => $o['params'],
                        'type'          => isset($o['type']) ? $o['type'] : null
                    );

                    break;
                }
            }

            if (false !== $idx && !empty($oscheme)) {
                $output[$idx]['pages'] = $this->_toMenu($oscheme);
            }
        }

        return $output;
    }
}