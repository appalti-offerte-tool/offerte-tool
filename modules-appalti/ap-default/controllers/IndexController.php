<?php

class ApDefault_IndexController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    protected $_service;

    public function init()
    {
        $this->_helper->ajaxContext()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('update-field', 'json')
            ->addActionContext('activate', 'json')
            ->addActionContext('deactivate', 'json')
            ->addActionContext('delete', 'json')
            ->initContext();

        parent::init();
    }

    public function preDispatch()
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();

        if ($accountRow->isGuest()) {
            $this->_redirect('/account/authenticate/index');
        }

        $action = $this->getRequest()->getActionName();
        if ($accountRow->isAdmin() && 0 === strcasecmp('index', $action)) {
            $this->_forward('admin');
        } elseif ($accountRow->isTextWriter() && 0 !==strcasecmp('text-writer', $action)) {
            $this->_forward('text-writer');
        }

        $companyRow = $accountRow->getCompanyRow(false);
        if ($accountRow->isCompanyOwner() && null === $companyRow) {
            $this->_redirect('/company/create/index');
        }

        if ($accountRow->isCompanyOwner() && !$companyRow->isMembershipValid()) {
            $this->_redirect('/cart');
        }
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
//        return 'ap-account:ap-account';
        return 'guest';
    }

    public function indexAction()
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $this->view->accountRow = $accountRow;

        $proposalSrv = new \Proposal_Service_Proposal();
        $this->view->stat = $proposalSrv->fetchStatistics();

        $newsSrv = new \News_Service_News();
        $response = $newsSrv->fetchLast();
        $rowset = $response->getRowset();
        $this->view->news = $rowset[0];

        $eventSrv = new \Event_Service_Event();
        $response = $eventSrv->fetchLast();
        $rowset = $response->getRowset();
        $row = $rowset[0];
        $this->view->event = $row;
    }

    public function adminAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(10);

        $params = $this->_getAllParams();

        $cookie = isset($_COOKIE["dashboardCustomFilter"]) ? $_COOKIE["dashboardCustomFilter"] : null;
        if (!empty($params['dashboardCustomFilter'])
                && \is_array($params['dashboardCustomFilter'])
                && isset($params['dashboardCustomFilter']['value'])) {
            setcookie('dashboardCustomFilter', \Zend_Json::encode($params['dashboardCustomFilter']), time()+60*60*24*3, '/');
        } elseif($cookie) {
            $params['dashboardCustomFilter'] = \Zend_Json::decode($cookie);
        }

        $this->view->filterVal = !empty($params['dashboardCustomFilter']) ? $params['dashboardCustomFilter']['value'] : null;

        $stSrv = new \SupportTicket_Service_SupportTicketModerator();
        $response = $stSrv->fetchAll($params);
        $this->view->rowset = $response->getRowset();
        $newTicketsCount = $stSrv->fetchNewTicketsCount();
        $this->view->newTicketsCount = $newTicketsCount;

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }

    public function textWriterAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(10);

        $params = $this->_getAllParams();

        $cookie = isset($_COOKIE["dashboardCustomFilter"]) ? $_COOKIE["dashboardCustomFilter"] : null;
        if (!empty($params['dashboardCustomFilter'])
                && \is_array($params['dashboardCustomFilter'])
                && isset($params['dashboardCustomFilter']['value'])) {
            setcookie('dashboardCustomFilter', \Zend_Json::encode($params['dashboardCustomFilter']), time()+60*60*24*3, '/');
        } elseif($cookie) {
            $params['dashboardCustomFilter'] = \Zend_Json::decode($cookie);
        }

        $this->view->filterVal = !empty($params['dashboardCustomFilter']) ? $params['dashboardCustomFilter']['value'] : null;

        $stSrv = new \SupportTicket_Service_SupportTicketModerator();
        $response = $stSrv->fetchAllAssigned($params);
        $this->view->rowset = $response->getRowset();
        $newTicketsCount = $stSrv->fetchNewTicketsCount();
        $this->view->newTicketsCount = $newTicketsCount;

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }

    public function newsAndEventsAction()
    {

    }

    public function attachFormAction()
    {
        $this->_helper->layout->disableLayout();

        if ($this->_getParam('id')) {
            $account = $this->_service->find($this->_getParam('id'));

            if (empty($account)) {
                throw new OSDN_Exception('Unable to find account');
            }

            $this->view->account = $account;
        }
    }

    public function fetchAction()
    {
        $account = $this->_service->find($this->_getParam('id'));
        if (empty($account)) {
            throw new OSDN_Exception('Unable to find account');
        }

        $this->view->row = $account->toArray();
        $this->view->success = true;
    }

    public function createAction()
    {
        try {
            $params = $this->_getAllParams();
            $accountRow = $this->_service->create($this->_getAllParams());
            $params['id'] = $accountRow->getId();

            $this->view->accountId = $accountRow->getId();
            $this->_helper->information(
                array('Account has been successfully created.'),
                false, E_USER_NOTICE
            );

            $createNotify = new Account_Misc_Email_CreateAccount();
            $this->view->success = (boolean) $createNotify->notificate($params);
            $this->_helper->information('Email was sent successfully', true, E_USER_NOTICE);

        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch (\Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessage());
        }
    }

    public function updateAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $account = $this->_service->find($this->_getParam('accountId'));
            if (null === $account) {
                throw new OSDN_Exception('Unable to find account');
            }

            $affectedRows = $this->_service->update($account->id, $this->_getAllParams());

            $this->view->success = true;
            $this->_helper->information(
                'Account has been successfully updated.',
                true,
                E_USER_NOTICE
            );

            $this->view->success = true;

        } catch (OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $this->_service->delete($this->_getParam('id'));

            $this->view->success = true;
            $this->_helper->information('Account has been successfully updated.', true, E_USER_NOTICE);

        } catch (\OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateFieldAction()
    {
        if (!$this->getRequest()->isPost()
            || !$this->_getParam('accountId')
            || !$this->_getParam('field')
            || !$this->_getParam('value')) {
            return;
        }

        try {
            $account = $this->_service->find($this->_getParam('accountId'));
            if (null === $account) {
                throw new OSDN_Exception('Unable to find account');
            }

            $params = array(
                'id'    =>  $this->_getParam('accountId'),
                $this->_getParam('field') => $this->_getParam('value')
            );

            $affectedRows = $this->_service->update($account->id, $params);

            $this->view->success = true;
            $this->_helper->information(
                array('Account has been successfully updated.'),
                true,
                E_USER_NOTICE
            );

            $this->view->success = true;

        } catch (OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAllAction()
    {
        $i18nLanguage = new I18n_Service_LanguageConfiguration();
        $languageRowset = $i18nLanguage->fetchAllValuePairs();

        $response = $this->_service->fetchAllByRoleWithResponse($this->_getParam('roleId', null), $this->_getAllParams());
        $response->setRowsetCallback(function($row) use ($languageRowset) {

            $lcode = $row['language'];
            return array(
                'id'        => $row['id'],
                'username'  => $row['username'],
                'fullname'  => $row['fullname'],
                'email'     => $row['email'],
                'fullname'  => $row['fullname'],
                'phone'     => $row['phone'],
                'language'  => isset($languageRowset[$lcode]) ? $languageRowset[$lcode] : '',
                'isActive'  => $row['isActive'],

                /**
                 * @FIXME Make definition of fields, which are should be visible in list
                 */
//                'dateOfBirth' => $row['dateOfBirth'],
//                'landOfBirth' => $row['landOfBirth'],
//                'test'        => $row['test']
            );
        });

        $this->view->assign($response->toArray());
    }

    public function accountMetadataAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');

        $md = new Ddbm_Instance_Metadata('account');
        $this->view->metadata = $md->getMetadata();
    }

    public function activateAction()
    {
        $this->view->success = $this->_service->activate($this->_getParam('accountId'));
    }

    public function deactivateAction()
    {
        $this->view->success = $this->_service->deactivate($this->_getParam('accountId'));
    }


    public function priceAction()
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if (!$accountRow->isAdmin()) {
            $this->_redirect('/ap-default/index');
        }
    }


    public function managementAction()
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if (!$accountRow->isAdmin()) {
            $this->_helper->information('Permision denied');
            $this->_redirect('/');
        }
    }
}