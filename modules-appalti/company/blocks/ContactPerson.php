<?php

final class Company_Block_ContactPerson extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Contact persons');
    }

    protected function _toHtml()
    {}
}