<?php

final class Company_Block_Information extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Company information');
    }

    protected function _toHtml()
    {}
}