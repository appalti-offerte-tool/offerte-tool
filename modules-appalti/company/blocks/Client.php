<?php

final class Company_Block_Client extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Clients');
    }

    protected function _toHtml()
    {}
}