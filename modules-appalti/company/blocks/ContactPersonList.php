<?php

final class Company_Block_ContactPersonList extends Application_Block_Abstract
{
    /**
     * @var Account_Model_DbTable_AccountRow
     */
    protected $_accountRow;

    /**
     * @var Company_Model_CompanyRow
     */
    protected $_companyRow;

    /**
     * @var Company_Service_Company
     */
    protected $_companyService;

    /**
     * @var Company_Service_ContactPerson
     */
    protected $_contactPersonService;

    protected function _toTitle()
    {
        return $this->view->translate('Contact persons');
    }

    protected function _fetch()
    {
        $companyId = $this->getRequest()->getParam('companyId');

        if ($this->_accountRow->isAdmin() || !empty($companyId)) {
            $this->_companyRow = $this->_companySrv->find($this->getRequest()->getParam('companyId'));
        } else {
            $this->_companyRow = $this->_accountRow->getCompanyRow();
        }

        $this->_contactPersonSrv = new \Company_Service_ContactPerson($this->_companyRow);
        $response = $this->_contactPersonSrv->fetchAllWithResponse($this->getRequest()->getParams());

        $this->view->rowset = $response->getRowset();
        $this->view->company = $this->_companyRow;

        $this->view->blockId = $this->getId();

        $this->_pagination->setTotalCount($response->count());
        $this->view->pagination = $this->_pagination;
    }

    protected function _toHtml()
    {
        $this->view->listOnly = $this->_getParam('listOnly');
        $this->_fetch();
    }

    public function init()
    {
        parent::init();

        $this->_pagination = new OSDN_Pagination();
        $this->_pagination->setItemsCountPerPage(10);
        $this->_accountRow = Zend_Auth::getInstance()->getIdentity();
        $this->_companySrv = new \Company_Service_Company();
    }


}