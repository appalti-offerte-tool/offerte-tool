<?php

final class Company_Misc_Navigation_CompanyDetailPageMvc extends Zend_Navigation_Page_Mvc
{
    protected $_isCompleted = false;

    protected $_companyId;

    protected function _init()
    {
        $request =  Zend_Controller_Front::getInstance()->getRequest();
        $this->_companyId = (int) $request->getParam('companyId');
        $proposalId = (int) $request->getParam('proposalId');

        $params = $this->getParams();

        if (!empty($this->_companyId)) {
            $params['companyId'] = $this->_companyId;
        } elseif (!empty($proposalId)) {
            $proposalSrv = new \Proposal_Service_Proposal();
            $proposalRow = $proposalSrv->find($proposalId);
            $this->_companyId = $proposalRow->companyId;
            $params['companyId'] = $this->_companyId;
        }

        $this->setParams($params);
        parent::_init();

    }

    /**
     * (non-PHPdoc)
     * @see Zend_Navigation_Page::getLabel()
     */
    public function getLabel()
    {
        if (true !== $this->_isCompleted) {
            $this->_label = 'Detailinformatie klant';

            if (!empty($this->_companyId)) {
                $companyService = new Company_Service_Company();
                $companyRow = $companyService->find($this->_companyId);

                $this->_label .= ' "' . $companyRow->getFullName() . '"';
            }

            $this->_isCompleted = true;
        }

        return parent::getLabel();
    }

}