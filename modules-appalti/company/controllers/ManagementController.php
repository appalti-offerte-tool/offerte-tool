<?php

class Company_ManagementController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Company_Service_Company
     */
    protected $_service;

    /**
     * @var \Account_Model_DbTable_AccountRow
     */
    protected $_accountRow;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();

        $this->_service = new \Company_Service_Company();

        $this->_accountRow = Zend_Auth::getInstance()->getIdentity();

        $this->_helper->ajaxContext()
            ->addActionContext('fetch-company-clients', 'json')
            ->initContext();

    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'company:reporting';
    }

    protected function _getAdminStatistics()
    {
        $params = $this->_getAllParams();
        $proposalSrv = new \Proposal_Service_Proposal();

        $clients = $this->_service->fetchAllParentCompaniesWithResponse();
        $this->view->assign($clients->toArray());


        $stat = array();
        $children = array();
        if (!empty($params['stat'])) {
            foreach ($params['stat'] as $p) {
                $stat[] = $proposalSrv->fetchStatistics($p);

                if (!empty($p['companyId'])) {
                    $companyRow = $this->_service->find($p['companyId']);
                    $childrenRowset = $companyRow->getChildrenRowset();
                    $children[] = $childrenRowset;
                } else {
                    $children[] = array();
                }
            }
        } else {
            $stat[0] = $proposalSrv->fetchStatistics();
            $stat[1] = $stat[0];
        }

        //get proposal with maximal amount
        $proposalRowMaxAmount = $proposalSrv->fetchProposalRowWithMaxAmount();

        // prepare view
        $this->view->stat = $stat;
        $this->view->children = $children;
        $this->view->proposalRowMaxAmount = $proposalRowMaxAmount;
    }

    protected function _getClientStatistics(&$params = null)
    {
        // init
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
        $proposalSrv = new \Proposal_Service_Proposal();
        $proposalRowMaxAmount = $proposalSrv->fetchProposalRowWithMaxAmount();
        $params = ($params)? $params:$this->_getAllParams();
        $proposal_max_amount = ($proposalRowMaxAmount)? $proposalRowMaxAmount->amount:5000;
        $params['priceslider_max']  = $this->_getParam('priceslider_max', $proposal_max_amount);

        // get data
        $clients = $this->_service->fetchAllByParentWithResponse($companyRow, $params);
        $statistics = $proposalSrv->fetchStatistics($params);
        $locations = $companyRow->getLocations();
        $themes = $companyRow->getThemes();
        $regions = $companyRow->getRegions();
        $accounts = $companyRow->getAccounts();

        // prepare view
        $this->view->assign($clients->toArray());
        $this->view->accountRow = $accountRow;
        $this->view->companyRow = $companyRow;
        $this->view->stat = $statistics;
        $this->view->proposalRowMaxAmount = $proposalRowMaxAmount;
        $this->view->locations = $locations;
        $this->view->themes = $themes;
        $this->view->regions = $regions;
        $this->view->accounts = $accounts;
    }

    public function indexAction()
    {
        // get params
        $params = array();
        $params['startDate']        = $this->_getParam('startDate', null);
        $params['endDate']          = $this->_getParam('endDate', null);
        $params['priceslider_min']  = $this->_getParam('priceslider_min', 0);
        $params['clientCompanyId']  = $this->_getParam('clientCompanyId', 0);
        $params['locations']        = $this->_getParam('locations', array());
        $params['themes']           = $this->_getParam('themes', array());
        $params['regions']          = $this->_getParam('regions', array());
        $params['contactpersons']   = $this->_getParam('contactpersons', array());

        if ($this->_accountRow->isAdmin()) {
            $this->_getAdminStatistics();
            $this->render('admin');
        } else {
            $this->_getClientStatistics($params);
        }

        // prepare view
        $this->view->postParams = $params;
    }

    public function fetchCompanyClientsAction()
    {
        if ($this->getRequest()->isPost() && $this->_getParam('companyId')) {
            try {
                $companyRow = $this->_service->find($this->_getParam('companyId'));
                $response = $this->_service->fetchAllByParentWithResponse($companyRow);
                $this->view->success = true;

                $clients = array();
                foreach($response->getRowset() as $row) {
                    $clients[] = array(
                        'id' => $row->getId(),
                        'name' => $row->getFullName()
                    );
                }

                $this->view->clients = $clients;
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->view->error = $e->getMessage();
            }
        }
    }

    public function exportAction()
    {
        // get data
        $req = $this->getRequest();
        $req->setParam('data', new stdClass());
        if(($json = $this->_getParam('data:json')) != false)
        {
            $req->setParam('data', json_decode($json));
        }

        // determine format
        switch($this->_getParam('format'))
        {
            case 'excel':
                $this->_forward('export-as-xls', 'management','company', $req->getParams());
                break;
            default:
                die('No valid format found.');
                break;
        }
    }

    public function exportAsXlsAction()
    {
        function objectToArray($d) {
            if (is_object($d)) {
                // Gets the properties of the given object
                // with get_object_vars function
                $d = get_object_vars($d);
            }

            if (is_array($d)) {
                /*
                * Return array converted to object
                * Using __FUNCTION__ (Magic constant)
                * for recursive call
                */
                return array_map(__FUNCTION__, $d);
            }
            else {
                // Return array
                return $d;
            }
        }
        set_time_limit( 0 );

        // get data
        $proposalSrv = new \Proposal_Service_Proposal();
        $statistics = $proposalSrv->fetchStatistics(get_object_vars($this->getRequest()->getParam('data')));

//echo '<pre>incoming: '.print_r(get_object_vars($this->getRequest()->getParam('data')),1).'</pre>';
//echo '<pre>exporting: '.print_r($statistics,1).'</pre>';
//die('stopping');

        // create excel file
        $filename = APPLICATION_PATH . "/tmp/excel-" . date( "m-d-Y" ) . ".xls";
        $realPath = realpath( $filename );
        if ( false === $realPath )
        {
            touch( $filename );
            chmod( $filename, 0777 );
        }
        $filename = realpath( $filename );
        $handle = fopen( $filename, "w" );
        $finalData = array();
        foreach ( $data AS $row )
        {
            $finalData[] = array(
                utf8_decode( $row["col1"] ), // For chars with accents.
                utf8_decode( $row["col2"] ),
                utf8_decode( $row["col3"] ),
            );
        }

        foreach ( $finalData AS $finalRow )
        {
            fputcsv( $handle, $finalRow, "\t" );
        }
        fclose( $handle );

        // stream excel file
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $this->getResponse()->setRawHeader( "Content-Type: application/vnd.ms-excel; charset=UTF-8" )
        ->setRawHeader( "Content-Disposition: attachment; filename=excel.xls" )
        ->setRawHeader( "Content-Transfer-Encoding: binary" )
        ->setRawHeader( "Expires: 0" )
        ->setRawHeader( "Cache-Control: must-revalidate, post-check=0, pre-check=0" )
        ->setRawHeader( "Pragma: public" )
        ->setRawHeader( "Content-Length: " . filesize( $filename ) )
        ->sendResponse();
        readfile( $filename );

        // delete excel file
        unlink($filename);
        exit();
    }
}