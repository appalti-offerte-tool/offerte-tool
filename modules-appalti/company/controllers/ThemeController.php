<?php
class Company_ThemeController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    protected function _toCompanyRow()
    {
        return Zend_Auth::getInstance()->getIdentity()->getCompanyRow(false);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();
        $this->accountRow = Zend_Auth::getInstance()->getIdentity();
        $this->companyRow = $this->_toCompanyRow();
        $this->_themes = new Proposal_Service_CustomTemplate($this->companyRow);
        $this->view->themes = array();
        $this->themeRow = new Proposal_Model_CustomTemplate();

        // prepare view
        $this->view->accountRow = $this->accountRow;
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'company:theme';
    }


    public function indexAction()
    {
        $this->_forward('select');
    }

    public function selectAction()
    {

        $themes = $this->_themes->fetchAllActive();
        $this->view->themes = $themes;
        if (count($this->view->themes) == 1)
        {
            /*
             * If only 1 theme exists, this may be the first visit
             * if first visit
             *     get company proposal themes
             *     assign each to company custom template
             */

            // get custom template
            $customTemplateRow = $this->view->themes->current();

            // get custom template proposal themes
            $_proposalThemeTable = new Proposal_Model_DbTable_ProposalTheme();
            $select = $_proposalThemeTable->select()
                                            ->where('companyId = ?', $customTemplateRow->companyId)
                                            ->where('proposalId IS NULL')
                                            ->where('useCustomTemplate = ? ', $customTemplateRow->getId())
            ;
            $proposalThemeRowset = $_proposalThemeTable->fetchAll($select);
            if (count($proposalThemeRowset))
            {
                //
                return;
            }

            // get proposal theme
            $select = $_proposalThemeTable->select()
                                            ->where('companyId = ?', $customTemplateRow->companyId)
            ;
            $proposalThemeRowset = $_proposalThemeTable->fetchAll($select);
            foreach($proposalThemeRowset as $proposalThemeRow)
            {
                // save as custom template proposal theme
                $proposalThemeRow->useCustomTemplate = $customTemplateRow->getId();
                $proposalThemeRow->save();
            }
        }
    }

    public function createAction()
    {
        //process post
        if ($this->getRequest()->isPost())
        {
            try{
                // copy selected theme
                $fromThemeRow = $this->_themes->find($this->_getParam('from_theme'));
                $toThemeRow = $this->_themes->copy($fromThemeRow, $this->_getParam('to_theme'));
                if($toThemeRow)
                {
                    $this->view->succes = true;
                    $this->_helper->information('Het nieuwe thema is aangemaakt.', true, E_USER_NOTICE);
                    $this->_redirect('/company/theme/update/id/'.$toThemeRow->getId());
                }

            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information(array('Error creating theme. Error: %s.', array($e->getMessage())), true);
            }
        }

        // get themes
        $this->selectAction();

    }

    public function updateAction()
    {
        if ($this->getRequest()->isPost())
        {
            try {
                $id = $this->getRequest()->getParam('id');
                $theme = $this->getRequest()->getParam('theme');
                if($id != $theme['id'])
                {
                    throw new Exception($this->view->translate('Geen geldig id.'));
                }
                $this->_themes = new Proposal_Service_CustomTemplate($this->companyRow);
                $customTemplateRow = $this->_themes->find($theme['id']);
                $customTemplateRow->setFromArray($theme);
                if ($customTemplateRow->save())
                {
                    $this->_themes->setLibraries($customTemplateRow, $this->_getParam('libraries', array()));

                    $this->view->success = true;
                    $this->_helper->information($this->view->translate('Thema is aangepast.'), true, E_USER_NOTICE);
                    $this->_redirect('/company/theme/update/id/'.$customTemplateRow->getId());
                }

            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information(array('Error updating theme. Error: %s.', array($e->getMessage())), true);
            }
        }
        $this->_forward('company-defaults','template', 'proposal');
    }

    public function deleteAction()
    {
        // check theme is in use
        // if so, deactive
        // else delete

        // init
        $this->_themes = new Proposal_Service_CustomTemplate();
        $this->_styles = new Proposal_Model_DbTable_ProposalTheme();
        $this->_settings = new Proposal_Model_DbTable_CustomTemplateSettings();

        try {
            // validate input
            if(($id = (int)$this->_getParam('id', 0)) === 0)
            {
                throw new OSDN_Exception($this->view->translate('Ongeldig id.'));
            }
            // prevent deleting last active theme
            $themes = $this->_themes->fetchAllActive();
            if (count($themes) < 2)
            {
                throw new OSDN_Exception($this->view->translate('U mag het laatste thema niet verwijderen.'));
            }

            // get theme
            $customTemplateRow = $this->_themes->find($id);

            // prevent deletion of theme if in use by locations
            $_locations = new Company_Model_DbTable_Location();
            $select = $_locations->select()
                                    ->from(array('lo'=>$_locations->getTableName()))
                                    ->join(array('lot'=>'locationProposalcustomtemplate'),'lot.locationId = lo.id',array())
                                    ->where('lo.isActive = 1')
                                    ->where('lot.proposalcustomtemplateId = ?', $customTemplateRow->getId())
            ;
            if (count($_locations->fetchAll($select)))
            {
                throw new Exception('Het thema kan niet verwijderd worden, omdat het in gebruik is bij 1 of meer bedrijfslocaties.');
            }

            // check if customTemplateRow in use by proposals
            $proposalThemesRowset = $this->_themes->fetchProposalsUsingTheme($customTemplateRow);
            if (($this->_themes->deActivateTheme($customTemplateRow)) != false)
            {
                // success delete
                $this->view->succes = true;
                $this->_helper->information('Het thema is verwijderd.', true, E_USER_NOTICE);
                $this->_redirect($this->view->url(array('action'=>null,'id'=>null)));
            }
            // failure
            $this->_helper->information('Het thema kon niet worden verwijderd.', true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
            $this->_redirect($this->view->url(array('action'=>null,'id'=>null)));
        } catch(\Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessage());
            $this->_redirect($this->view->url(array('action'=>null,'id'=>null)));
        }
    }
}