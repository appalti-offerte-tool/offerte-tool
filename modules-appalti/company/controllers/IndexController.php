<?php

class Company_IndexController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{

    /**
     * @var \Company_Service_Company
     */
    protected $_service;

    /**
     * @return \Company_Model_CompanyRow
     */
    protected function _toCompanyRow($throwException = true)
    {
        $companyId = $this->_getParam('companyId');
        if (!empty($companyId)) {
            $companyRow = $this->_service->find($companyId, $throwException);
        } else {
            $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow($throwException);
        }

        return $companyRow;
    }

    public function init()
    {
        $this->_helper->contextSwitch()
            ->addContext('plain', array(
                'suffix'    => 'plain'
            ))
            ->addActionContext('fetch', array('plain', 'json'))
            ->addActionContext('fetch-by-account', 'plain')
            ->addActionContext('fetch-with-contact-person', 'json')
            ->addActionContext('update-info-only', 'json')
            ->initContext();

        parent::init();

        $this->_service = new \Company_Service_Company();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'company';
    }

    public function indexAction()
    {
        $companyRow = $this->_toCompanyRow(false);

        if (null === $companyRow) {
            $this->_redirect('/company/create/index');
        }

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if ($accountRow->isCompanyOwner() && !$companyRow->isMembershipValid()) {

            $this->_redirect('/cart');
        }
        $this->view->accountRow = $accountRow;

        $summary = $this->_service->getAccountCompanySummary();
        $this->view->summary = $summary;
    }


    /**
     * Can be used with different context:
     *     + admin
     *
     */
    public function fetchAction()
    {
        try {
            $this->view->companyRow = $this->_toCompanyRow();
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchByAccountAction()
    {
        $account = new Account_Service_Account();
        $accountRow = $account->find($this->_getParam('accountId'));

        $companyRow = $accountRow->getCompanyRow(false);
        if (null !== $companyRow) {
            Company_Service_Company::assertAccountOwnershipToCompany($companyRow);
        }

        $this->view->companyRow = $companyRow;
        $this->view->success = true;

        if (0 !== strcasecmp('json', $this->_helper->contextSwitch()->getCurrentContext())) {
            $this->getRequest()->setActionName('fetch');
        }
    }

    public function createAction()
    {
        if ($this->getRequest()->isPost())
        {
            $this->view->formSubmittedPostData = $this->getRequest()->getParams();
            try {
                $params = $this->_getAllParams();
                $params['accountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

                $companyRow = $this->_service->create($params, function(Company_Model_CompanyRow $companyRow) use ($params) {
                    $bulkOperation = new Company_Service_CompanyBulkOperation($companyRow);
                    $bulkOperation->bulk($params,false);
                });

                $this->view->success = true;
                $this->_helper->information('Relatie toegevoegd.', true, E_USER_NOTICE);

                if ($companyRow->isRoot()) {
                    $this->_redirect('/company/');
                } else {
//                    $this->_redirect('/company/client/summary/companyId/' . $companyRow->getId());
                    $this->_redirect('/company/client');
                }

            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information('Unable to create');
            }
        }

        $companyRow = $this->_toCompanyRow();
//        $_contactpersons = new Company_Service_ContactPerson();
//        $contactPersonRow = $_contactpersons->createRow();
        $this->view->company = $companyRow;
//        $this->view->contactPersonRow = $contactPersonRow;
//echo '<pre>'.print_r($contactPersonRow,1).'</pre>';
//die();
        $this->view->parentId = $this->_getParam('parentId');
        if (!empty($this->view->parentId)) {
            $this->view->createContactPersonLater = true;
        }
        $this->render('form');
    }

    private function _update(\Company_Model_CompanyRow $companyRow, array $params = array())
    {
        $this->view->formSubmittedPostData = $this->getRequest()->getParams();
        try {
            $params = $this->_getAllParams();
            $companyRow = $this->_service->update(
                $companyRow,
                $params,
                function(Company_Model_CompanyRow $companyRow) use ($params) {
                    $bulkOperation = new Company_Service_CompanyBulkOperation($companyRow);
                    $bulkOperation->bulk($params, false);
                }
            );

            $this->view->success = true;


            $this->_helper->information('Klant bedrijf met succes bijgewerkt.', true, E_USER_NOTICE);

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch (\Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateInfoOnlyAction()
    {
        $companyRow = $this->_toCompanyRow();

        if ($this->getRequest()->isPost()) {
            $params = $this->_getAllParams();
            $this->_update($companyRow, $params);
        }

        $this->view->companyRow = $companyRow;
        $this->view->namespace = 'companyInfo';
    }

    public function updateContactPersonsAction()
    {
        $companyRow = $this->_toCompanyRow();
        $this->view->companyRow = $companyRow;
    }

    public function updateAction()
    {
        $companyRow = $this->_toCompanyRow();
        $this->view->companyRow = $companyRow;

        $currAccount = Zend_Auth::getInstance()->getIdentity();
        $ac = new Account_Service_Account();
        $accountRowset = $ac->getChildAccounts($currAccount->getId());
        if (empty($accountRowset)) {
            $accountRowset = array();
        }
        $accountRowset[] = $currAccount;
        $this->view->currAccount = $currAccount;
        $this->view->accounts = $accountRowset;

        if ($this->getRequest()->isPost()) {
            $this->_update($companyRow, $this->_getAllParams());

            if ($this->_getParam('returnURL')) {
                $this->_redirect($this->_getParam('returnURL'));
            } else {
                $this->_redirect($companyRow->isRoot() ? '/company/' : '/company/index/update/companyId/'.$companyRow->getId());
            }
        }
    }

    public function fetchWithContactPersonAction()
    {
        $companyRow = $this->_toCompanyRow();
        $aCompanyRow = $companyRow->toArray();
        $aCompanyRow['logo'] = (string) $this->view->modules('company')->CompanyLogoFileStock($companyRow)->resize(70);
        $this->view->row = $aCompanyRow;

        $this->view->contactPersonRowset = $companyRow->getCompanyContactPersonRowset()->toArray();
        $this->view->success = true;
    }

    public function deleteAction()
    {
        $id = $this->_getParam('companyId');
        if ($id) {
            try {
                $this->_service->delete($id);
                $this->view->success = true;
                $this->_helper->information('Client company deleted successfully.', true, E_USER_NOTICE);
            } catch(\Exception $e) {
                $this->view->success = false;
//                $this->_helper->information('Unable to delete company because there are proposals connected to it.');
                $this->_helper->information($e->getMessage());
            }
        } else {
            $this->_helper->information('Company id is not specified. Select a company to delete from list below', true, E_USER_WARNING);
        }

        $this->_redirect('/company/client/');
    }
}