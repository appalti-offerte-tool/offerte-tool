<?php

class Company_MainController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Company_Service_Company
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_service = new \Company_Service_Company();

        $this->_helper->contextSwitch()
            ->addActionContext('index', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch', 'json')
            ->initContext();

        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'company:list';
    }

    public function indexAction()
    {
        // prepare layout
    }

    public function fetchAllAction()
    {
        $response = $this->_service->fetchAllParentCompaniesWithResponse($this->_getAllParams());
        $response->setRowsetCallback(function($row) {
            return $row;
        });
        $this->view->assign($response->toArray());
    }

    public function comboBoxAction()
    {

        $this->_helper->layout->disableLayout();
        $response = $this->_service->fetchAllParentCompaniesWithResponse($this->_getAllParams());
        $response->setRowsetCallback(function($row) {
            return $row;
        });

        $this->view->rowset = $response->getRowset();
    }

    public function fetchAction()
    {
        $companyRow = $this->_service->find($this->_getParam('companyId'));

        $aCompanyRow = $companyRow->toArray();
        $aCompanyRow['logo'] = (string) $this->view->modules('company')->CompanyLogoFileStock($companyRow)->resize(70);
        $this->view->row = $aCompanyRow;

        $this->view->success = true;
    }

    public function createAction()
    {
        $this->getResponse()->setHeader('Content-Type', 'text/html', true);
        $params = $this->_getAllParams();
        if (empty( $params['accountId'])){
            $params['accountId'] = Zend_Auth::getInstance()->getIdentity()->getId();
        }
        $params ['createdAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();


        try {
            $companyRow = $this->_service->create($params);
            $this->view->companyId = $companyRow->getId();
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHeader('Content-Type', 'text/html', true);

        try {
            $companyRow = $this->_service->find($this->_getParam('companyId'));
            $companyRow = $this->_service->update($companyRow, $this->_getAllParams());
            $this->view->companyId = $companyRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->view->success = $this->_service->delete($this->_getParam('companyId'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}