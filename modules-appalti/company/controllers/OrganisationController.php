<?php
class Company_OrganisationController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'company';
    }

    
    public function indexAction()
    {
    }
}