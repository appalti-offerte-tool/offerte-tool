<?php

class Company_OfferController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Company_Service_Offer
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();

        $this->_service = new \Company_Service_Offer();
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'company';
    }

    public function indexAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(20);

        $accountRow = Zend_Auth::getInstance()->getIdentity();

        $response = $this->_service->fetchAllByCompanyWithResponse($accountRow->getCompanyRow(), $this->_getAllParams());
        $this->view->assign($response->toArray());

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }

    public function createAction()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->render('form');
        }

        try {
            $this->view->formSubmittedPostData = $this->getRequest()->getParams();
            $params = $this->_getAllParams();
            $accountRow = Zend_Auth::getInstance()->getIdentity();
            $params['accountId'] = $accountRow->getId();

            $companyRow = $accountRow->getCompanyRow();
            $params['companyId'] = $companyRow->getId();

            $offerRow = $this->_service->create($params);

            $this->view->offerId = $offerRow->getId();
            $this->view->success = true;
            $this->_helper->information(
                array('%s "%s" is gemaakt.', array(ucfirst($offerRow->getKindNameNL()), $offerRow->name)),
                true, E_USER_NOTICE
            );
            $this->_redirect('/company/offer/');
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch(\Exception $e) {
            $this->view->success = false;
            $this->_helper->information('Kan te maken product / dienst.');
        }

        $this->render('form');
    }

    public function updateAction()
    {
        $offerRow = $this->_service->find($this->_getParam('offerId'), false);
        if (null === $offerRow) {
            return $this->_redirect('/company/offer/');
        }

        $this->view->offerRow = $offerRow;

        if (!$this->getRequest()->isPost()) {
            return $this->render('form');
        }

        try {
            $this->view->formSubmittedPostData = $this->getRequest()->getParams();
            $offerRow = $this->_service->update($offerRow, $this->_getAllParams());
            $this->view->offerId = $offerRow->getId();
            $this->view->success = true;
            $this->_helper->information(
                array('%s "%s" is bijgewerkt.', array(ucfirst($offerRow->getKindNameNL()), $offerRow->name)),
                true, E_USER_NOTICE
            );
            $this->_redirect('/company/offer/');
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch(\Exception $e) {
            $this->view->success = false;
            $this->_helper->information('Niet werken product / dienst.');
        }

        $this->render('form');
    }

   public function deleteAction()
    {
        $offerId = $this->_getParam('offerId');

        try {
            $offerRow = $this->_service->find($offerId);
            $this->_service->delete($offerRow->getId());
            $this->_helper->information(
                array('%s "%s" is verwijderd.', array(ucfirst($offerRow->getKindNameNL()), $offerRow->name)),
                true, E_USER_NOTICE
            );
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }

        $this->_redirect('/company/offer/');
    }

}