<?php

class Company_ContactPersonController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Company_Service_ContactPerson
     */
    protected $_contactpersons;
    protected $_service;

    protected function _toAccountRow()
    {
        return Zend_Auth::getInstance()->getIdentity();
    }

    protected function _toCompanyRow()
    {
        return Zend_Auth::getInstance()->getIdentity()->getCompanyRow(false);
    }

    public function init()
    {
        parent::init();
        $this->_helper->ContextSwitch()
            ->setContext('form', array(
                'suffix'    => 'form'
            ))
            ->setContext('block', array(
                'suffix'    => 'block'
            ))
            ->addActionContext('fetch', array('json'))
            ->addActionContext('fetch-all', array('json'))
            ->addActionContext('create', array('json', 'block'))
            ->addActionContext('delete', 'json')
            ->addActionContext('update', array('json', 'block'))
            ->addActionContext('update-account', array('json'))
            ->addActionContext('get-photo-src', array('json'))
            ->addActionContext('get-themes', array('json'))
            ->initContext();

        $companyRow = $this->_toCompanyRow();
        $this->companyRow = $companyRow;
        $this->view->company = $this->companyRow;

        Company_Service_Company::assertAccountOwnershipToCompany($companyRow);

        $this->_contactpersons = new \Company_Service_ContactPerson($companyRow);
        $this->_service = $this->_contactpersons;
        $this->_locations = new Company_Service_Location();
        $this->_regions = new Company_Service_Region();
    }

    public function getResourceId()
    {
        return 'company:company-contact-person';
    }

    public function indexAction()
    {
        $this->_forward('select');
    }

    public function selectAction()
    {
        // get contactpersons
        $this->contactpersons = array();
        $contactpersons = $this->_contactpersons->fetchAll()->getRowset();
        if (!$contactpersons)
        {
            return;
        }
        $this->contactpersons = $contactpersons;
        $_accounts = new Account_Model_DbTable_Account();
        foreach($this->contactpersons as &$contactperson)
        {
            $accountRow = $_accounts->find($contactperson['accountId'])->current();
            $contactperson['admin'] = 0;
            if ($accountRow)
            {
                $account = $accountRow->toArray();
                $contactperson['admin'] = ($account['admin']||$account['roleId']==6);
            }
        }
        $this->view->contactpersons = $this->contactpersons;

        // get roles
        $_luids = array(
            'admin' => array('companyOwner', 'manager', 'proposalBuilder'),
            'companyOwner' => array('companyOwner', 'manager', 'proposalBuilder'),
            'manager' => array('manager', 'proposalBuilder'),
            '' => array(''),
        );
        $roleSrv = new \Account_Service_AclRole();
        $luid = ($this->_toAccountRow()->isAdmin())? 'admin' : $this->_toAccountRow()->getRole()->luid;
        $rowset1 = $roleSrv->fetchByLuids($_luids[ $luid ]);
        foreach($rowset1 as $row) {
            $this->roles[$row->id] = $row->toArray();
        }
        $company = $this->_toAccountRow()->getCompanyRow();
        $rowset2 = $roleSrv->findChildRolesByCompanyId($company->getId());
        foreach($rowset2 as $row) {
            $this->roles[$row->id] = $row->toArray();
        }
        if ($this->roles)
        {
            $this->view->roles = $this->roles;
        }
    }

    public function createAction()
    {
        $companyId = $this->_getParam('companyId', $this->companyRow->getId());
        $companySrv = new Company_Service_Company();
        $companyRow = $companySrv->find($companyId);
        $this->view->canHaveAccount = $companyRow->isRoot();

        $currAccount = Zend_Auth::getInstance()->getIdentity();
        $ac = new Account_Service_Account();
        $accountRowset = $ac->getChildAccounts($currAccount->getId());
        if (empty($accountRowset)) {
            $accountRowset = array();
        }

        $accountRowset[] = $currAccount;
        $config = array(
            'data' => array(
                'id'                    => 0,
                'accountId'             => 0,
                'companyId'             => $companyRow->getId(),
                'sex'                   => 'man',
                'title'                 => 'Dhr.',
                'relationship'          => 'formal',
            ),
        );

        $contactPersonRow = $this->_contactpersons->createRow($config);
        $this->view->contactPersonRow = $contactPersonRow;
        $this->view->contactpersonLocations = $this->_contactpersons->getLocations($this->view->contactPersonRow);
        $this->view->currAccount = $currAccount;
        $this->view->accounts = $accountRowset;

        $aclRole = new Account_Service_AclRole();
        $this->view->roles = array();
        $this->roles = $aclRole->findChildRolesByCompanyId($this->companyRow->getId());
        if ($this->roles)
        {
            $this->view->roles = $this->roles->toArray();
        }

        // get locations
        $locations = $this->_locations->fetchAllActiveLocations();
        $this->view->locations = $locations;

        // get location regions
        $locationRegions = array();
        $locationRegions[ $this->companyRow->getId() ] = $this->_regions->getByLocation($this->companyRow);
        foreach($locations as $location)
        {
            $locationRegions[ $location->getId() ] = $this->_regions->getByLocation($location);
        }
        $this->view->locationRegions = $locationRegions;

        $this->view->isXmlHttpRequest = $this->_request->isXmlHttpRequest();

        if (!$this->getRequest()->isPost()) {

            $namespace = $this->_getParam('namespace');
            if (!empty($namespace)) {
                $this->view->namespace = $namespace;
            }
            return;
        }
        unset(
            $this->view->company,
            $this->view->canHaveAccount,
            $this->view->contactPersonRow,
            $this->view->contactpersonLocations,
            $this->view->currAccount,
            $this->view->accounts,
            $this->view->roles,
            $this->view->locations,
            $this->view->locationRegions,
            $this->view->isXmlHttpRequest
        );
        $this->getResponse()->setHeader('Content-Type', 'text/html', true);
        try {
            $data = $this->_getAllParams();
            $data['companyId'] = (isset($data['companyId']) && (int)$data['companyId'])? $data['companyId']:$this->_toCompanyRow()->getId();
            $data['username'] = trim($data['email']);
            if (($contactPersonRow = $this->_service->create($data)) !=  false)
            { // new user created
                $this->view->contactPersonId = $contactPersonRow->getId();
                $this->view->contactPersonRow = $contactPersonRow;
                if ((int)$contactPersonRow->getCompanyRow()->getId() == (int)$this->_toCompanyRow()->getId())
                {
                    $username = $contactPersonRow->getAccountRow()->username;
                    $password = $data['password'];

                    // email new user
                    // - create emailbody
                    $view = new Zend_View();
                    $view->assign('contactpersonRow', $contactPersonRow);
                    $view->assign('username', $username);
                    $view->assign('password', $password);
                    $view->addScriptPath(dirname(__FILE__).'/../views/scripts/default/');
                    $content = $view->render('contact-person/email/user.created.phtml');

                    // - get emailbody with layout
                    $layout = Zend_Layout::getMvcInstance();
                    $layout->setLayout('mail')
                                ->disableLayout();
                    $layout->title = 'Nieuwe gebruiker aangemaakt';
                    $layout->content = $content;
                    $body = $layout->render();

                    // - send email
                    $mail = new Zend_Mail();
                    $mail->setSubject('Offertemaken.com: Er is een account voor u aangemaakt.');
                    $mail->addTo($contactPersonRow->email, $contactPersonRow->getFullname());
                    $mail->addBcc('nhofs@appalti.nl');
                    $mail->setBodyHtml($body);
                    $mail->setBodyText(html_entity_decode(strip_tags($body)));
                    if (!$mail->send())
                    {
                        throw new Exception('Kon geen email versturen.');
                    }
                }

            }
            $this->_helper->information('De gebruiker is aangemaakt.', true, E_USER_NOTICE);
            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch (\Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessage());
        }


        if ($this->_getParam('returnHtml')) {
            /*
             * Workaround on bug:
             * when posting multipart/form-data in IE
             * it tries to download the response
             * showing file download box - Security warning
             */
            $this->getResponse()->setHeader('Content-Type', 'text/html', true);
        }
    }

    public function emailAction()
    {
        $contactPersonRow = $this->_service->find(693);
        $accountRow = $contactPersonRow->getAccountRow();
        $password = 'apeldoorn';

        $view = new Zend_View();
        $view->assign('contactpersonRow', $contactPersonRow);
        $view->assign('username', $accountRow->username);
        $view->assign('password', $password);
        $view->addScriptPath(dirname(__FILE__).'/../views/scripts/default/');
        $content = $view->render('contact-person/email/user.created.phtml');

        // - get emailbody with layout
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('mail')
                    ->disableLayout();
        $layout->title = 'Nieuwe gebruiker aangemaakt';
        $layout->content = $content;
        $body = $layout->render();
die($body);
    }

    public function fetchAllAction()
    {
        $contactPersonRow = $this->_service->fetchAll($this->_getAllParams());
        $this->view->assign($contactPersonRow->toArray());
        $this->view->success = true;
    }

    public function fetchAction()
    {
        $contactPersonRow = $this->_service->find($this->_getParam('contactPersonId'));
        $this->view->row = $contactPersonRow->toArray();
        $this->view->success = true;
    }

    public function deleteAction()
    {
        try {
            $contactPersonId = $this->_getParam('contactPersonId');
            $this->_service->delete($contactPersonId);

            $this->view->contactPersonId = $this->_getParam('contactPersonId');
            $this->view->success = true;
            $this->_helper->information('De gebruiker is verwijderd.', true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch (Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessage());
        }

    }

    public function updateAccountAction()
    {
        $currAccount = Zend_Auth::getInstance()->getIdentity();
        $this->view->accountRow = $currAccount;

        if (!$this->getRequest()->isPost()) {
            $contactPersonRow = $currAccount->getAccountRelatedContactPersonRow(false);
            $this->view->contactPersonRow = $contactPersonRow;
            return;
        }

        $this->getResponse()->setHeader('Content-Type', 'text/html', true);

        try {
            $contactPersonRow = $currAccount->getAccountRelatedContactPersonRow(false);
            $contactPersonRow = $this->_service->updateAccountContactPerson($contactPersonRow, $this->_getAllParams(), false);
            $this->view->contactPersonRow = ($contactPersonRow instanceof \Company_Model_ContactPersonRow) ? $contactPersonRow->toArray() : null;
            $this->view->success = true;
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch (OSDN_Validate_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        if ($this->getRequest()->isPost()) {
            $this->getResponse()->setHeader('Content-Type', 'text/html', true);
            try {
                $params = $this->_getAllParams();
                $id = $this->_getParam('id', $this->_getParam('contactPersonId', 0));
                $contactPersonRow = $this->_service->find($id);
                $this->_service->update($contactPersonRow, $params);
                $contactPersonRow = $this->_service->find($id);
                if (isset($params['locations']) && $params['locations'])
                {
                    $this->_contactpersons->setLocations($contactPersonRow, $params['locations']);
                }
                if (isset($params['themes']) && $params['themes'])
                {
                    $this->_contactpersons->setThemes($contactPersonRow, $params['themes']);
                }
                $this->view->canHaveAccount = !empty($contactPersonRow->accountId);
                $this->view->success = true;
                $this->_helper->information('De gebruiker is aangepast.', true, E_USER_NOTICE);
//                $this->_redirect('/company/contact-person/update/id/'.$contactPersonRow->getId());
            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            }
        }

        // get contactperson
        $id = $this->_getParam('id', $this->_getParam('contactPersonId',0));
        $contactPersonRow = $this->_contactpersons->find($id);
        $this->view->contactPersonRow = $contactPersonRow;
        $this->view->canHaveAccount = !empty($contactPersonRow->accountId);

        // get locations for checkboxes
        $locations = $this->_locations->fetchAllActiveLocations();
        $this->locations = $locations;
        $this->view->locations = $this->locations;

        // get contactperson assigned locations
        $contactpersonLocations = $this->_contactpersons->getLocations($contactPersonRow);
        $this->view->contactpersonLocations = $contactpersonLocations;

        // get contactperson assigned locations themes for checkboxes
        $contactpersonLocationsThemes = $this->_contactpersons->getLocationsThemes($contactPersonRow);
        $this->view->contactpersonLocationsThemes = $contactpersonLocationsThemes;

        // get contactperson assigned themes
        $contactpersonThemes = $this->_contactpersons->getThemes($contactPersonRow);
        $this->view->contactpersonThemes = $contactpersonThemes;

        // get location regions
        $locationRegions = array();
        $locationRegions[ $this->companyRow->getId() ] = $this->_regions->getByLocation($this->companyRow);
        foreach($locations as $location)
        {
            $locationRegions[ $location->getId() ] = $this->_regions->getByLocation($location);
        }
        $this->view->locationRegions = $locationRegions;

        // get all active themes
        $_themes = new Proposal_Service_CustomTemplate();
        $this->view->themes = $_themes->fetchAllActive();

        // used to determine url cancel button
        $this->view->isXmlHttpRequest = $this->_request->isXmlHttpRequest();
    }

    public function getPhotoSrcAction()
    {
        try {
            $contactPersonRow = $this->_service->find($this->_getParam('contactPersonId'));
            $src = $this->view->contactPersonPhotoFileStock($contactPersonRow)->resize(30);
            $this->view->src = $src;
            $this->view->success = true;
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }


}