<?php

class Company_BankAccountController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Company_Service_BankAccount
     */
    protected $_service;

    public function init()
    {
        parent::init();

        $this->_helper->ContextSwitch()
            ->addContext('block', array(
                'suffix'    => 'block'
            ))
            ->addActionContext('fetch', array('json'))
            ->addActionContext('create', array('json', 'block'))
            ->addActionContext('delete', 'json')
            ->initContext();

        $this->_service = new \Company_Service_BankAccount();
    }

    public function getResourceId()
    {
        return 'company';
    }

    public function createAction()
    {
        $this->view->companyId = $this->_getParam('companyId');

        if ( !$this->getRequest()->isPost()) {
            $namespace = $this->_getParam('namespace');
            if (!empty($namespace)) {
                $this->view->namespace = $namespace;
            }

            return;
        }

        try {
            $contactPersonRow = $this->_service->create($this->_getAllParams());
            $this->view->bankAccountId = $contactPersonRow->getId();
            $this->view->success = true;
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAction()
    {
        $contactPersonRow = $this->_service->find($this->_getParam('bankAccountId'));
        $this->view->row = $contactPersonRow->toArray();
        $this->view->success = true;
    }

    public function deleteAction()
    {
        $bankAccountId = $this->_getParam('bankAccountId');
        $this->_service->delete($bankAccountId);

        $this->view->bankAccountId = $this->_getParam('bankAccountId');
        $this->view->success = true;
    }
}