<?php

class Company_CreateController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var \Company_Service_Company
     */
    protected $_service;

    /**
     * @var \Company_Service_Wizard
     */
    protected $_serviceWizard;

    protected function _toAccountRow()
    {
        return Zend_Auth::getInstance()->getIdentity();
    }

    protected function _toCompanyRow()
    {
        return Zend_Auth::getInstance()->getIdentity()->getCompanyRow(false);
    }

    public function init()
    {
        $this->_service = new \Company_Service_Company();
        $this->_wizard = new \Company_Service_Wizard();
        $this->_wizard->init();
        $this->view->wizard = $this->_wizard;
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'company';
    }

    public function indexAction()
    {
        $this->_helper->information('U bent succesvol ingelogd.', true, E_USER_NOTICE);
    }

    public function companyAction()
    {
        // init
        $this->_wizard->setCurrent(Company_Service_Wizard::STEP_COMPANY_COMPANY);

        // process
        $accountRow = $this->_toAccountRow();
        $signupData = json_decode($accountRow->signupData);
        if (!$signupData)
        {
            $signupData = (object) array(
                'firstname'     => '',
                'prefix'        => '',
                'lastname'      => $accountRow->lastname,
                'sex'           => ($accountRow->sex == 'Dhr.')? 'man':'vrouw',
                'company_name'  => '',
                'form'          => '',
                'street'        => '',
                'homenr'        => '',
                'postal_code'   => '',
                'city'          => '',
                'phone'         => $accountRow->phone,
                'email'         => $accountRow->email,
            );
        }
        // get initials from firstname
        $firstnames = explode(' ', $signupData->firstname);
        $initials = array();
        foreach($firstnames as $firstname)
        {
            $initial = '';
            // ex. Erik Bert => E. B.
            // ex. Henk-Jan => H.J.
            // ex. Erik Bert Henk-Jan Robert => E. B. H.J. R.
            $parts = explode('-',$firstname);
            foreach($parts as $part)
            {
                $initial .= trim(strtoupper(substr($part, 0, 1)));
                $initial .= ($initial)? '.':'';
            }
            $initials[]= $initial;
        }
        $signupData->initials = implode(' ', $initials);

        // is posted?
        if ($this->getRequest()->isPost())
        {
            $this->view->formSubmittedPostData = $this->getRequest()->getParams();
            try {
                $companyRow = $accountRow->getCompanyRow(false);
                $contactpersonRow = $accountRow->getContactPersonRow(false);

                if (null !== $companyRow)
                { // update company
                    $params = array_merge(
                        array(
                            'contactPerson'     => array(
                                $contactpersonRow->getId() => $contactpersonRow->toArray(),
                            )
                        ),
                        $this->getRequest()->getParams(),
                        array(
                            'accountId' => $accountRow->getId(),
                        )
                    );
                    $this->_service->update($companyRow, $params, function(Company_Model_CompanyRow $companyRow) use ($params) {
                        $bulkOperation = new Company_Service_CompanyBulkOperation($companyRow);
                        $bulkOperation->bulk($params);
                    });
                }
                else
                { // insert company
                    $data = array(
                        'contactPerson'     => array(
                            'create0'           => array(
                                'accountId'         => $accountRow->getId(),
                                'sex'               => $signupData->sex,
                                'initials'          => $signupData->initials,
                                'firstname'         => $signupData->firstname,
//                                'prefix'            => $signupData->prefix,
                                'lastname'          => $signupData->lastname,
                                'email'             => $signupData->email,
                                'phone'             => $signupData->phone,
                            ),
                        )
                    );
                    $params = array_merge(
                        $data,
                        $this->getRequest()->getParams(),
                        array(
                            'accountId' => $accountRow->getId(),
                            'activeTillDate' => date('Y-m-d', time() + 14 * 24 * 60 * 60), // now + 14 days
                        )
                    );
                    $companyRow = $this->_service->create($params, function(Company_Model_CompanyRow $companyRow) use ($params)
                    { // company is inserted
                        $bulkOperation = new Company_Service_CompanyBulkOperation($companyRow);
                        $bulkOperation->bulk($params);

                        // added
                        $dbAdapter = $companyRow->getTable()->getAdapter();
                        try {
                            $dbAdapter->beginTransaction();

                            // init
                            $_accounts                  = new Account_Model_DbTable_Account();
                            $_roles                     = new Account_Model_DbTable_AclRole();
                            $_companies                 = new Company_Model_DbTable_Company();
                            $_libraries                 = new Company_Model_DbTable_Library();
                            $_librariesThemes           = new Company_Model_DbTable_LibraryProposalCustomTemplate();
                            $_locationThemes            = new Company_Model_DbTable_LocationProposalCustomTemplate();
                            $_contactpersonLocations    = new Company_Model_DbTable_ContactPersonLocation();
                            $_contactpersonThemes       = new Company_Model_DbTable_ContactPersonProposalCustomTemplate();
                            $_themes                    = new Proposal_Service_CustomTemplate($companyRow);
                            $_templates                 = new Proposal_Model_DbTable_CustomTemplateSettings();

                            $logger = Zend_Registry::get('logger');
                            // create role for company
                            // - find company Appalti by username 'nhofs@appalti.nl'
                            $select = $_companies->select()
                                                    ->setIntegrityCheck(false)
                                                    ->from(array('c'=>'company'))
                                                    ->columns(array(
                                                        'role_id'           =>'ar.id',
                                                        'role_companyId'    =>'ar.companyId',
                                                        'role_name'         =>'ar.name',
                                                        'role_parentId'     =>'ar.parentId',
                                                        'role_luid'         =>'ar.luid',
                                                    ))
                                                    ->join(array('cp'=>'contactPerson'), 'cp.companyId = c.id', array())
                                                    ->join(array('a'=>'account'), 'a.id = cp.accountId', array())
                                                    ->joinLeft(array('ar'=>'aclRole'), ' ar.companyId = c.id', array())
                                                    ->where('c.parentId IS NULL') // remove company relations
                                                    ->where('c.motherId IS NULL') // remove company extra locations
                                                    ->where('a.username = ?', 'nhofs@appalti.nl')
                                                    ->order('ar.id')    // company admin role is created before company user roles
                                                    ->limit(1)          // only need company admin role
                            ;
                            $companyRowset = $_companies->fetchAll($select);
                            if (count($companyRowset) != 1)
                            {
                                throw new Exception('Too many parent companies found.');
                            }
                            $appaltiRow = $companyRowset->current();
                            $logger->info('company Appalti found.');

                            // - find appalti company role
                            if (($appaltiRoleRow = $_roles->fetchRow('id = '.$appaltiRow->role_id)) == false)
                            { // company not converted
                                throw new Exception('Could not find role.');
                            }
                            $logger->info('role Appalti found.');

                            // - copy role from default beheerderRole (including permissions)
                            $select = $_roles->select()->where('id = ?', 6);
                            $beheerderRoleRow = $_roles->fetchRow($select);
                            $data = array(
                                'id'        => null,
                                'companyId' => $companyRow->getId(),
                                'name'      => $companyRow->name,
                                'parentId'  => $appaltiRoleRow->getId(),
                                'luid'      => null,
                            );
                            $companyAdminRoleRow = $_roles->copyRole($beheerderRoleRow, $data, true);
                            $logger->info('role '.$beheerderRoleRow->name.' copied to company '.$companyAdminRoleRow->name.' admin role.');

                            // create userrole from default userRole (including permissions)
                            $select = $_roles->select()->where('id = ?', 5);
                            $userRoleRow = $_roles->fetchRow($select);
                            $data = array(
                                'id'        => null,
                                'companyId' => $companyRow->getId(),
                                'name'      => $userRoleRow->name,
                                'parentId'  => $companyAdminRoleRow->getId(),
                                'luid'      => null,
                            );
                            $companyUserRoleRow = $_roles->copyRole($userRoleRow, $data, true);
                            $logger->info('role '.$userRoleRow->name.' copied to company '.$companyUserRoleRow->name.' role.');

                            // assign new role to account
                            $select = $_accounts->select()
                                                    ->from(array('a'=>'account'), array('*'))
                                                    ->join(array('cp'=>'contactPerson'), 'cp.accountId = a.id', array())
                                                    ->join(array('c'=>'company'), 'c.id = cp.companyId', array())
                                                    ->where('c.id = ?', $companyRow->getId())
                                                    ->where('a.roleId = ?', 6)
                            ;
                            $adminRowset = $_accounts->fetchAll($select);
                            foreach($adminRowset as $adminRow)
                            {
                                $adminRow->roleId = $companyUserRoleRow->getId();
                                $adminRow->admin = 1;
                                $adminRow->save();
                            }
                            $logger->info('company '.$companyAdminRoleRow->name.' admin role added to account.');

                            // copy Appalti library to new company
                            // - get Appalti library
                            $_libraries = new Company_Service_Library();
                            $libraries = $_libraries->fetchAll("companyId = ".$appaltiRow->getId()." AND name = 'Bij aanmaken klant'");
                            if ($libraries)
                            {
                                $AppaltiLibraryRow = $libraries->current();
                            }
                            if(!$AppaltiLibraryRow)
                            {
                                throw new Exception('Could not get library.');
                            }

                            // - create new library from Appalti library
                            $libraryData = array(
                                'name'          => 'Standaard',
                                'description'   => 'Uw standaard bibliotheek',
                            );
                            $libraryRow = $_libraries->copy($AppaltiLibraryRow, $libraryData, $companyRow);

                            // create theme
                            $themeRow = $_themes->createRow();
                            $themeRow->companyId = $companyRow->getId();
                            $themeRow->name = 'Standaard';
                            $themeRow->active = 1;
                            if (!$themeRow->save())
                            {
                                throw Exception("Couldn't create theme.");
                            }

                            // - add style
                            $_proposalThemes = new Proposal_Model_DbTable_ProposalTheme();
                            $themeSettings = $_proposalThemes->createRow(array(
                                'companyId'                     => $themeRow->companyId,
                                'useCustomTemplate'             => $themeRow->getId(),
                                'includePrice'                  => 0,
                                'includePersonalIntroduction'   => 0,
                            ));
                            if (!$themeSettings->save())
                            {
                                throw Exception("Couldn't create theme settings.");
                            }

                            // - add templates
                            // -- add template 'common'
                            $themeTemplateRowCommon = $_templates->createRow(array(
                                'proposalCustomTemplateId' => $themeRow->getId(),
                                'scope' => 'common',
                            ));
                            if (!$themeTemplateRowCommon->save())
                            {
                                throw Exception("Couldn't create theme template 'common'.");
                            }
                            // -- add template 'introduction'
                            $themeTemplateRowIntro = $_templates->createRow(array(
                                'proposalCustomTemplateId' => $themeRow->getId(),
                                'scope' => 'introduction',
                            ));
                            if (!$themeTemplateRowIntro->save())
                            {
                                throw Exception("Couldn't create theme template 'introduction'.");
                            }

                            // -- add template 'content'
                            $themeTemplateRowContent = $_templates->createRow(array(
                                'proposalCustomTemplateId' => $themeRow->getId(),
                                'scope' => 'content',
                            ));
                            if (!$themeTemplateRowContent->save())
                            {
                                throw Exception("Couldn't create theme template 'content'.");
                            }
                            // -- add template 'attachment'
                            $themeTemplateRowAttach = $_templates->createRow(array(
                                'proposalCustomTemplateId' => $themeRow->getId(),
                                'scope' => 'attachment',
                            ));
                            if (!$themeTemplateRowAttach->save())
                            {
                                throw Exception("Couldn't create theme template 'attachment'.");
                            }

                            // link library to theme
                            // - get company theme
                            if (($themeRow = $companyRow->findDependentRowset('Proposal_Model_DbTable_CustomTemplate', 'Company')->current()) == false)
                            {
                                throw new Exception('Could not get company theme.');
                            }

                            // - get company library
                            if (($libraryRow = $companyRow->findDependentRowset('Company_Model_DbTable_Library')->current()) == false)
                            {
                                throw new Exception('Could not get company library.');
                            }

                            // - link library to theme
                            $themeLibraryRow = $_librariesThemes->createRow(array(
                                'libraryId'                 => $libraryRow->getId(),
                                'proposalcustomtemplateId'  => $themeRow->getId(),
                            ));
                            if (!$themeLibraryRow->save())
                            {
                                throw new Exception('Could not link library to theme.');
                            }
                            // make standard theme available to company
                            $locationThemeRow = $_locationThemes->createRow(array(
                                'locationId' => $companyRow->getId(),
                                'proposalcustomtemplateId'  => $themeRow->getId(),
                            ));
                            if (!$locationThemeRow->save())
                            {
                                throw new Exception('Could not link theme to company.');
                            }

                            // get company users
                            $contactpersonRowset = $companyRow->findDependentRowset('Company_Model_DbTable_ContactPerson', 'Company');
                            foreach($contactpersonRowset as $contactpersonRow)
                            {
                                // make company available to user
                                $contactpersonLocationRow = $_contactpersonLocations->createRow(array(
                                    'contactpersonId'   => $contactpersonRow->getId(),
                                    'locationId'        => $companyRow->getId(),
                                ));
                                if (!$contactpersonLocationRow->save())
                                {
                                    throw new Exception('Could not link contactperson to company.');
                                }

                                // make standard theme available to user
                                $contactpersonThemeRow = $_contactpersonThemes->createRow(array(
                                    'contactpersonId'           => $contactpersonRow->getId(),
                                    'proposalcustomtemplateId'  => $themeRow->getId(),
                                ));
                                if (!$contactpersonThemeRow->save())
                                {
                                    throw new Exception('Could not link contactperson to theme.');
                                }
                            }
                            $dbAdapter->commit();
                        } catch(Exception $e) {
                            $dbAdapter->rollback();
                            throw $e;
                        }
                    });
                }
                $this->view->success = true;
                $this->_helper->information('Bedrijfsinformatie is succesvol opgeslagen.', true, E_USER_NOTICE);

                if (!empty($params['next-step'])) {
                    $this->_redirect($this->_wizard->getNextStepUrl());
                }
                $this->_redirect($this->view->url());

            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information(array('Error creating company. Error: %s.', array($e->getMessage())), true);
            }
        }

        // get data for view
        if (($companyRow = $this->_toCompanyRow()) == false)
        {
            $companyRow = $this->_service->createRow(array(
                'name'              => $signupData->company_name,
                'form'              => strtoupper($signupData->form),
                'actualStreet'      => $signupData->street,
                'actualHouseNumber' => $signupData->homenr,
                'actualPostCode'    => $signupData->postal_code,
                'actualCity'        => $signupData->city,
            ));
        }
        $this->view->companyRow = $companyRow;
        $this->view->parentId = $this->_getParam('parentId');
        $this->view->canHaveAccount = empty($this->view->parentId);
        $aclRole = new Account_Service_AclRole();
        $this->view->roles = $aclRole->fetchByLuids(array(
            'manager',
            'proposalBuilder',
            'companyOwner',
        ));
    }

    public function contactpersonAction()
    {
        // init
        $this->_wizard->setCurrent(Company_Service_Wizard::STEP_COMPANY_CONTACTPERSON);
        $accountRow = $this->_toAccountRow();
        $companyRow = $accountRow->getCompanyRow(false);
        $contactpersonRow = $accountRow->getContactPersonRow(false);

        // validate
        if (empty($companyRow)) {
            $this->_helper->information('Voer eerst uw bedrijfsgegevens in.', true, E_USER_WARNING);
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }

        // process
        $this->view->companyRow = $companyRow;
        $this->view->parentId = $this->_getParam('parentId');
        $this->view->canHaveAccount = empty($this->view->parentId);

        if ($this->getRequest()->isPost())
        {
            $this->view->formSubmittedPostData = $this->getRequest()->getParams();
            try {
                $params = $this->_getAllParams();
                $params['accountId'] = $accountRow->getId();
                foreach($params['contactPerson'] as $data)
                {
                    $data['title'] = ($data['sex'] == 'man')? 'Dhr.':'Mevr.';
                    $contactpersonRow->setFromArray($data);
                }

                $this->view->success = true;
                $this->_helper->information('Gebruiker is succesvol opgeslagen.', true, E_USER_NOTICE);

                if (!empty($params['next-step'])) {
                    $this->_redirect($this->_wizard->getNextStepUrl());
                }
                $this->_redirect($this->view->url());

            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information(array('Error creating company. Error: %s.', array($e->getMessage())), true);
            }
        }
    }

    public function summaryAction()
    {
        // init
        $this->_wizard->setCurrent($this->_wizard->getStepSummary());

        // validate
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $companyRow = $accountRow->getCompanyRow(false);
        if (empty($companyRow)) {
            $this->_helper->information('Voer eerst de bedrijfsgegevens in.', true, E_USER_WARNING);
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }
        $contactpersonRow = $accountRow->getContactPersonRow(false);
        if (empty($contactpersonRow)) {
            $this->_helper->information('Voer eerst uw gegevens in.', true, E_USER_WARNING);
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }

        $summary = $this->_service->getAccountCompanySummary();
        $this->view->summary = $summary;

        $this->view->libraryRow = $this->_toCompanyRow()->getActiveLibraries()->current();
    }

}