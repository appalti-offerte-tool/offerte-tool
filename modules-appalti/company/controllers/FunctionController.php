<?php
class Company_FunctionController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    protected $_accountRow;
    protected $_companyRow;

    protected $_roles;
    protected $_permissions;
    protected $_contactpersons;
    protected $_mapped_permissions;

    protected function _toAccountRow()
    {
        if (!$this->_accountRow)
        {
            $this->_accountRow = Zend_Auth::getInstance()->getIdentity();
        }
        return $this->_accountRow;
    }

    protected function _toCompanyRow()
    {
        if (!$this->_companyRow)
        {
            $this->_companyRow = $this->_toAccountRow()->getCompanyRow();
        }
        return $this->_companyRow;
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();

        $this->_roles = new Account_Service_AclRole();

        $bootstrap = $this->getInvokeArg('bootstrap');
        $im = $bootstrap->getResource('ModulesManager');
        $this->_permissions = new Account_Service_AclPermission($im);

        $this->_map_permissions = array(
            'access_all_proposals'      => array(
                'module'    => 'proposal',
                'resource'  => 'list'
            ),
            'update_proposals'          => array(
                'module'    => 'proposal',
                'resource'  => 'update'
            ),
            'edit_texts'                => array(
                'module'    => 'proposal',
                'resource'  => 'template'
            ),
            'edit_introduction'         => array(
                'module'    => 'proposal',
                'resource'  => 'introduction'
            ),
            'edit_price_sheet'          => array(
                'module'    => 'proposal',
                'resource'  => 'price-sheet'
            ),
            'grant_discount'            => array(
                'module'    => 'proposal',
                'resource'  => 'grant-discount'
            ),
            'approve_proposal'          => array(
                'module'    => 'proposal',
                'resource'  => 'approve'
            ),
            'management_information'    => array(
                'module'    => 'company',
                'resource'  => 'reporting'
            ),
        );
        $this->view->mapped_permissions = $this->_map_permissions;

        if (!$this->view->role)
        {
            $this->view->role = array(
                'id'            => 0,
                'name'          => '',
                'permissions'   => array(
                ),
            );
        }
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'company';
    }

    public function indexAction()
    {
        $this->_forward('select');
    }

    public function selectAction()
    {
        // get account
        $this->account = $this->_toAccountRow();
        $this->view->account = $this->account;

        // get contactperson by account
        $this->contactperson = $this->_toAccountRow()->getContactPersonRow();
        $this->view->contactperson = $this->contactperson;

        // get company
        $this->company = $this->_toCompanyRow();
        $this->view->company = $this->company;

        // get company role
        $this->companyAdminRole = $this->company->getCompanyRole();
        $this->view->admin = $this->companyAdminRole;

        // get company permissions
        $this->resources = array();
        $rowset = $this->_permissions->fetchAllResourcesByRole($this->companyAdminRole->getId());
        if (!$rowset)
        {
            return;
        }
        $resources = array();
        foreach($rowset as $resource)
        {
            $resource['resources'] = $this->_permissions->fetchAllByRoleAndResource($this->companyAdminRole->getId(), $resource['name']);
            $resources[] = $resource;
        }
        $this->resources = $resources;
        $this->view->modules = $this->resources;


        // get company roles
        $this->companyRoles = null;
        $companyRoles = $this->_roles->findChildRolesByCompanyId($this->company['id']);
        if (!$companyRoles)
        {
            return;
        }
        $this->companyRoles = $companyRoles->toArray();
        $this->view->companyRoles = $this->companyRoles;

        // get permissions per  role
        foreach($this->companyRoles as &$role)
        {
            $role['permissions'] = array();
            $modules = $this->_permissions->fetchAllResourcesByRole($role['id']);
            if ($modules)
            {
                foreach($modules as $module)
                {
                    // module
                    if ($module['isAllowed'])
                    {
                        $role['permissions'][] = array(
                            'module' => $module['name']
                        );
                    }

                    // module/resource
                    $module['resources'] = $this->_permissions->fetchAllByRoleAndResource($role['id'], $module['name']);
                    if ($module['resources'])
                    {
                        foreach($module['resources'] as $resource)
                        {
                            if (isset($resource['isAllowed']) && $resource['isAllowed'])
                            {
                                $role['permissions'][] = array(
                                    'module'    => $module['name'],
                                    'resource'  => $resource['name'],
                                );
                            }

                            if ($resource['privileges'])
                            {
                                foreach($resource['privileges'] as $privilege) {
                                    if (isset($privilege['isAllowed']) && $privilege['isAllowed'])
                                    {
                                        $role['permissions'][] = array(
                                            'module'=>$module['name'],
                                            'resource'=>$resource['name'],
                                            'privilege'=>$privilege['name']
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $this->view->roles = $this->companyRoles;
    }

    public function createAction()
    {
        if (Zend_Controller_Front::getInstance()->getRequest()->isPost())
        {
            // init
            $this->selectAction();
            try {
                $companyAdminRole = $this->_companyRow->getCompanyRole();

                // get submitted role
                $role = $this->_getParam('role');

                // validate submitted role
                if (!isset($role['name']) || $role['name'] == '')
                {
                    throw new Exception('U heeft geen naam ingevuld.');
                }
                if (!isset($role['permissions']) || !is_array($role['permissions']))
                {
                    $role['permissions'] = array();
                }
                $role['companyId'] = (int)$this->company['id'];
                $role['parentId']  = (int)$companyAdminRole->getId();
                $role['luid'] = null;
                $this->view->role = $role;

                // save role
                $roleRow = $this->_roles->createUserRoleRow($role);

                // update permissions
                $params = array(
                    'role' => array_merge(
                        $role,
                        $roleRow->toArray()
                    ),
                );

                // save permissions
                $permissions = array();
                foreach($role['permissions'] as $simple_permission)
                {
                    if (in_array($simple_permission, array_keys($this->_map_permissions)))
                    {
                        $permissions[] = $this->_map_permissions[ $simple_permission ];
                    }
                }

                $_aclpermissions = new Account_Model_DbTable_AclPermission();
                // - delete mapped permissions for this role
                foreach($this->_map_permissions as $map_permission)
                {
                    $resource = $map_permission['module'];
                    if (isset($map_permission['resource']))
                    {
                        $resource .= ':'.$map_permission['resource'];
                    }
                    $select = $_aclpermissions->select()
                                                ->where('roleId = ?', $roleRow->getId())
                                                ->where('resource = ?', $resource)
                    ;
                    if (isset($map_permission['privilege']))
                    {
                        $select->where('privilege = ?', $map_permission['privilege']);
                    }
                    $permissionRow = $_aclpermissions->fetchRow($select);
                    if ($permissionRow)
                    {
                        $permissionRow->delete();
                    }
                }
                // - save mapped permissions for this role
                foreach($permissions as $permission)
                {
                    $resource = $permission['module'];
                    if (isset($permission['resource']))
                    {
                        $resource .= ':'.$permission['resource'];
                    }
                    // add permission for this role
                    $data = array(
                        'id' => 0,
                        'roleId' => $roleRow->getId(),
                        'resource' => $resource,
                    );
                    if (isset($permission['privilege']))
                    {
                        $data['privilege'] = $permission['privilege'];
                    }
                    $permissionRow = $_aclpermissions->createRow($data);
                    $permissionRow->save();
                }

                $this->view->role = $roleRow->toArray();
                $this->view->role['permissions'] = array();
                foreach($this->_map_permissions as $simple_permission => $permission)
                {
                    $select  = $_aclpermissions->select()
                                                ->where('roleId = ?', $roleRow->getId())
                                                ->where('resource = ?', implode(':', $permission))
                    ;
                    if (($permissionRow = $_aclpermissions->fetchRow($select)) != false)
                    {
                        $this->view->role['permissions'][] = $permission;
                    }
                }

                // redirect to index
                $this->_redirect($this->view->url(array('action'=>null,'id'=>null)));

            } catch(Exception $e) {
                $this->_helper->information($e->getMessage());
            }
        }

        // get roles from selectAction() using select.phtml for view
        $this->_forward('select');
    }

    public function updateAction()
    {
        // init
        $this->selectAction();
        $id = $this->_getParam('id', 0);

        // process post
        if (Zend_Controller_Front::getInstance()->getRequest()->isPost())
        {
            try {
                // validate input
                $role = $this->_getParam('role');
                if (!isset($role['name']) || $role['name'] == '')
                {
                    throw new Exception('U heeft geen naam ingevuld.');
                }
                if (!isset($role['permissions']) || !is_array($role['permissions']))
                {
                    $role['permissions'] = array();
                }
                if (isset($role['companyId']))
                {
                    unset($role['companyId']);
                }
                if (isset($role['parentId']))
                {
                    unset($role['parentId']);
                }

                // save role
                $this->_roles->saveRole($role);

                // save permissions
                $permissions = array();
                foreach($role['permissions'] as $simple_permission)
                {
                    if (in_array($simple_permission, array_keys($this->_map_permissions)))
                    {
                        $permissions[] = $this->_map_permissions[ $simple_permission ];
                    }
                }
                $this->role = $this->_roles->find($id);
                {
                    $_aclpermissions = new Account_Model_DbTable_AclPermission();
                    // delete mapped permissions for this role
                    foreach($this->_map_permissions as $map_permission)
                    {
                        $resource = $map_permission['module'];
                        if (isset($map_permission['resource']))
                        {
                            $resource .= ':'.$map_permission['resource'];
                        }
                        $select = $_aclpermissions->select()
                                                    ->where('roleId = ?', $this->role->getId())
                                                    ->where('resource = ?', $resource)
                        ;
                        if (isset($map_permission['privilege']))
                        {
                            $select->where('privilege = ?', $map_permission['privilege']);
                        }
                        $permissionRow = $_aclpermissions->fetchRow($select);
                        if ($permissionRow)
                        {
                            $permissionRow->delete();
                        }
                    }
                    // save mapped permissions for this role
                    foreach($permissions as $permission)
                    {
                        $resource = $permission['module'];
                        if (isset($permission['resource']))
                        {
                            $resource .= ':'.$permission['resource'];
                        }
                        // add permission for this role
                        $data = array(
                            'id' => 0,
                            'roleId' => $this->role->getId(),
                            'resource' => $resource,
                        );
                        if (isset($permission['privilege']))
                        {
                            $data['privilege'] = $permission['privilege'];
                        }
                        $permissionRow = $_aclpermissions->createRow($data);
                        $permissionRow->save();
                    }
                }

                // redirect
                $this->_helper->information('De functie is aangepast.', true, E_USER_NOTICE);
                $this->_redirect($this->view->url(array('action'=>null,'id'=>null)));
            } catch(Exception $e) {
                $this->_helper->information($e->getMessage());
            }
        }

        // get role
        if (($role = $this->_roles->find($id)) != false)
        {
            $this->role = $role->toArray();
            $this->role['permissions'] = array();
            $_aclpermissions = new Account_Model_DbTable_AclPermission();
            $select = $_aclpermissions->select()
                                        ->where('roleId = ?',$role->getId())
            ;
            $permissionRowset = $_aclpermissions->fetchAll($select)->toArray();
            $keys = array('module','resource');
            $permissions = array();
            foreach($permissionRowset as $permissionRow)
            {
                $data = explode(':', $permissionRow['resource']);

                $permission = array();
                $permission['module'] = $data[0];
                if (isset($data[1]))
                {
                    $permission['resource'] = $data[1];
                }
                if ($permissionRow['privilege'])
                {
                    $permission['privilege'] = $permissionRow['privilege'];
                }

                $permissions[] = $permission;
            }
            $this->role['permissions'] = $permissions;
        }
        $this->view->role = $this->role;

        // get roles
        $this->_forward('select');
    }

    public function deleteAction()
    {
        $this->_helper->layout->disableLayout(true);

        $id = $this->_getParam('id', 0);
        if ($id)
        {
            try {
                // prevent deletion if in use by active users
                $_accounts = new Account_Model_DbTable_Account();
                $select = $_accounts->select()
                                    ->where('roleId = ?', $id)
                ;
                if (count($_accounts->fetchAll($select)))
                {
                    throw new Exception('De functie kon niet worden verwijderd, omdat deze wordt gebruikt voor 1 of meer gebruikers.');
                }
                // delete
                $this->_roles->delete($id);

                // response
                $this->_helper->information('De functie is verwijderd.', true, E_USER_NOTICE);
                $this->_redirect($this->view->url(array('action'=>null,'id'=>null)));

            } catch (Exception $e) {
                $this->_helper->information($e->getMessage());
                $this->_redirect($this->view->url(array('action'=>null,'id'=>null)));
            }
        }
    }
}