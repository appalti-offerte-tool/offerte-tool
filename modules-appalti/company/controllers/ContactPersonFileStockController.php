<?php

final class Company_ContactPersonFileStockController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    public function init()
    {
        $this->_helper->ContextSwitch()
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('fetch', 'json')
            ->addActionContext('fetch-all-by-account', 'json')
            ->initContext();

        $this->_service = new \Company_Service_ContactPersonPhotoFileStock();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'company';
    }

    public function fetchAction()
    {
        $accountFileStockRow = $this->_service->find(
            $this->_getParam('companyId'), $this->_getParam('fileStockId'),
            $this->_getParam('side'), true
        );

        $data = $accountFileStockRow->getFileStockRow()->toArray();

        foreach($accountFileStockRow->toArray() as $k => $v) {
            $data['accountFileStock_' . $k] = $v;
        }

        $this->view->data = $data;
    }

    /**
     * Back compability for fileStock
     */
    public function downloadAction()
    {
        $this->_service->download($this->_getParam('companyId'), $this->_getParam('fileStockId'), $this->getResponse());
    }

    public function viewAction()
    {
        $this->_service->view($this->_getParam('companyId'), $this->_getParam('fileStockId'), $this->getResponse());
    }

    public function createAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');

        try {

            $fileStockRow = $this->_service->create($this->_getAllParams());
            $this->view->fileStockId = $fileStockRow->getId();

            $this->view->success = true;
            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);

        } catch(OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');

        try {

            $fileStockRow = $this->_service->update($this->_getAllParams());
            $this->view->fileStockId = $fileStockRow->getId();

            $this->view->success = true;
            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);

        } catch(OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }
}