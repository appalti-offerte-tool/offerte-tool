<?php

class Company_ClientController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Company_Service_Company
     */
    protected $_service;

    /**
     * @var \Account_Model_DbTable_AccountRow
     */
    protected $_accountRow;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();
        $this->_service = new \Company_Service_Company();
        $this->_accountRow = Zend_Auth::getInstance()->getIdentity();

        // prepare view
        $this->view->accountRow = $this->_accountRow;
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'company:relation';
    }

    public function indexAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(20);
        if ($this->_accountRow->isAdmin()) {
            $response = $this->_service->fetchAllParentCompaniesWithResponse($this->_getAllParams(), false);
        } else {
            $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
            $response = $this->_service->fetchAllByParentWithResponse($companyRow, $this->_getAllParams());
            $this->view->companyRow = $companyRow;
        }

        $this->view->assign($response->toArray());

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;

        $this->_accountRow->isAdmin() && $this->render('admin-index');
    }

    public function createAction()
    {
    }

    public function contactpersonAction()
    {
        // init
        $_companies = new Company_Service_Company();
        $_contactpersons = new Company_Service_ContactPerson();

        // get contactpersonRow
        $companyId = $this->_getParam('companyId',0);
        $contactpersonId = $this->_getParam('id', $this->_getParam('contactPersonId',0));

        try{
            $contactpersonRow = $_contactpersons->find((int)$contactpersonId);
        } catch(Exception $e) {
            $contactpersonRow = $_contactpersons->createRow(array(
                'companyId' => $companyId,
            ));
        }
        $this->view->contactpersonRow = $contactpersonRow;
    }

    public function summaryAction()
    {
        $id = $this->_getParam('companyId');
        if (!$id) {
            $this->_helper->information('Select client company from list below', true, E_USER_WARNING);
            $this->_redirect('/company/client/index');
        }

        try {
            $companyRow = $this->_service->find($id);
            $this->view->row = $companyRow;
            if ($this->_accountRow->isAdmin()) {
                $this->render('admin-summary');
            }
        } catch (\Exception $e) {
            $this->_helper->information($e->getMessage());
            $this->_redirect('/company/client/index');
        }
    }

    public function changeStatusAction()
    {
        $companyId = $this->_getParam('companyId');
        $return = base64_decode($this->_getParam('return'));
        $redirectUrl = $companyId ? $return . '/companyId/' . $companyId : $return;

        if ($companyId) {
            try {
                $companyRow = $this->_service->find($companyId);
                $this->view->companyRow = $companyRow;
                $params = array('isActive' => $this->_getParam('active'));

                if ($this->_getParam('active')) {
                    $activeTill = $this->_getParam('activeTillDate');
                    $params['activeTillDate'] = !empty($activeTill) ? $activeTill : null;
                }

                $this->_service->update($companyRow, $params);
                $this->view->success = true;
            } catch (\Exception $e) {
                $this->view->false = true;
                $this->_helper->information(array('Error updating company information. Error: %s', array($e->getMessage())), true);
            }
        } else {
            $this->_helper->information('Wrong request.', true, E_USER_WARNING);
        }

        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_redirect($redirectUrl);
        }
    }
}