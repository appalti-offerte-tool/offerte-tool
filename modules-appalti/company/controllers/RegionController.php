<?php
class Company_RegionController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    protected function _toCompanyRow()
    {
        return Zend_Auth::getInstance()->getIdentity()->getCompanyRow(false);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();

        $this->_regions = new Company_Service_Region();
        $this->companyRow = $this->_toCompanyRow();
        $this->view->regions = array();
        $this->region = new Company_Model_RegionRow();
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'company';
    }


    public function indexAction()
    {
        $this->_forward('select');
    }

    public function selectAction()
    {
        $regions = $this->_regions->fetchAllByCompanyId($this->companyRow->getId())->toArray();
        if ($regions)
        {
            $this->view->regions = $regions['rowset'];
        }
    }

    public function createAction()
    {
        //process post
        if ($this->getRequest()->isPost())
        {
            $region = $this->_getParam('region', $this->region);
            $region['companyId'] = $this->companyRow->getId();
            try{
                $regionRow = $this->_regions->create($region);
                if($regionRow)
                {
                    $this->view->succes = true;
                    $this->_helper->information('De regio is aangemaakt.', true, E_USER_NOTICE);
                    $this->_redirect('/company/region');
                }
            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information(array('Error creating region. Error: %s.', array($e->getMessage())), true);
            }
        }

        // get regions
        $this->_forward('select');
    }

    public function updateAction()
    {
        $id = $this->_getParam('id', 0);
        $this->view->region = $this->_regions->find($id);

        //process post
        if ($this->getRequest()->isPost())
        {
            $region = $this->_getParam('region', $this->region);
            $region['companyId'] = $this->companyRow->getId();
            try{
                $regionRow = $this->_regions->update($this->_regions->find($id), $region);
                if($regionRow)
                {
                    $this->view->region = $regionRow;
                    $this->view->succes = true;
                    $this->_helper->information('De regio is gewijzigd.', true, E_USER_NOTICE);
                    $this->_redirect('/company/region');
                }
            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information(array('Error creating region. Error: %s.', array($e->getMessage())), true);
            }
        }

        // get regions
        $this->_forward('select');
    }

    public function deleteAction()
    {
        $id = $this->_getParam('id', 0);
        if ($id)
        {
            try {
                $regionRow = $this->_regions->find($id);
                $params = $regionRow->toArray();
//                $params['isActive'] = 0;
                if ($this->_regions->delete($id))
                {
                    // success delete
                    $this->view->succes = true;
                    $this->_helper->information('Bedrijfslocatie is succesvol verwijderd.', true, E_USER_NOTICE);
                    $this->_redirect($this->view->url(array('action'=>null,'id'=>null)));
                }
                // failure
                $this->_helper->information('Bedrijfslocatie kon niet worden verwijderd.', true, E_USER_NOTICE);
                $this->_redirect($this->view->url(array('action'=>null,'id'=>null)));
            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information(array('Error creating company. Error: %s.', array($e->getMessage())), true);
            }
        }
    }
}