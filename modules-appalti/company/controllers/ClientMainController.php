<?php

class Company_ClientMainController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Company_Service_ClientMain
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();
        $this->_helper->ajaxContext()
            ->addActionContext('edit', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch-row', 'json')
            ->initContext();

        $this->_service = new \Company_Service_ClientMain();
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'company';
    }

    public function indexAction()
    {
    }

    public function summaryAction()
    {
        $id = $this->_getParam('companyId');
        if (!$id) {
            $this->_helper->information('Select client company from list below', true, E_USER_WARNING);
            $this->_redirect('/company/client/index');
        }

        try {
            $companyRow = $this->_service->find($id);
            $this->view->row = $companyRow;
        } catch (\Exception $e) {
            $this->_helper->information($e->getMessage());
            $this->_redirect('/company/client/index');
        }
    }


    public function fetchAllAction()
    {
        $companySrv = new Company_Service_Company();
        $companyRow = $companySrv->find($this->_getParam('companyId'));
        $response = $this->_service->fetchAllByParentWithResponse($companyRow, $this->_getAllParams());
        $response = $response->getRowset();
        $this->view->assign($response);
    }
}