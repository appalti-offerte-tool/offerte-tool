<?php
class Company_LocationController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    protected function _toCompanyRow()
    {
        return Zend_Auth::getInstance()->getIdentity()->getCompanyRow(false);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();

        $this->_locations = new Company_Service_Location();
        $this->_service = $this->_locations;
        $this->_regions = new Company_Service_Region();
        $this->_themes = new Company_Service_Theme();
        $this->companyRow = $this->_toCompanyRow();
        $this->accountRow = Zend_Auth::getInstance()->getIdentity();
        $this->route = array(
            $this->getRequest()->getModuleKey()     => $this->getRequest()->getModuleName(),
            $this->getRequest()->getControllerKey() => $this->getRequest()->getControllerName(),
            $this->getRequest()->getActionKey()     => $this->getRequest()->getActionName(),
        );

    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'company';
    }

    public function indexAction()
    {
        $this->_forward('select');
    }

    public function selectAction()
    {
        // get locations
//        $locations = $this->_locations->fetchAllLocations();
        $locations = $this->_locations->fetchAllActiveLocations();
        $this->view->locations = $locations;

        // get location regions
        $locationRegions = array();
        $locationRegions[ $this->companyRow->getId() ] = $this->_regions->getByLocation($this->companyRow);
        foreach($locations as $location)
        {
            $locationRegions[ $location->getId() ] = $this->_regions->getByLocation($location);
        }
        $this->view->locationRegions = $locationRegions;
    }

    public function createAction()
    {
        $this->view->companyRow = null;
        $this->view->companyRegions = array();
        $regions = $this->_regions->fetchAllByCompanyId($this->_toCompanyRow()->getId())->toArray();
        $this->view->regions = $regions['rowset'];

        if ($this->getRequest()->isPost())
        {
            $this->view->formSubmittedPostData = $this->getRequest()->getParams();

            $params = $this->_getAllParams();
            $params['accountId'] = null;
            if (!$this->companyRow->getId())
            {
                $params['accountId'] = $this->accountRow->getId();
            }
            $params['motherId'] = $this->companyRow->getId();
            try {
                $companyRow = $this->_service->create($params);
                if ($companyRow)
                {
                    if (!empty($params['bankAccount'])) {
                        $bankDataNS = $params['bankAccount']['namespace'];
                        unset($params['bankAccount']['namespace']);
                        if (!empty($params['bankAccount'][$bankDataNS]['bankAccount']) && is_array($params['bankAccount'])) {
                            $companyBankAccount = new \Company_Service_BankAccount();
                            $companyBankAccount->bulk($companyRow, $params['bankAccount']);
                        }
                    }
                    if (isset($params['regions'])) {
                        $this->_regions->setLocationRegions($companyRow, $params['regions']);
                    }
                    $this->view->succes = true;
                    $this->_helper->information('Bedrijfsinformatie is succesvol opgeslagen.', true, E_USER_NOTICE);
                    $this->_redirect('/company/location/update/id/'.$companyRow->getId());
                }
                $this->_helper->information('Bedrijfsinformatie kon niet worden opgeslagen.', true, E_USER_NOTICE);
            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information(array('Error creating company. Error: %s.', array($e->getMessage())), true);
            }
        }
    }

    public function updateAction()
    {
        // init
        $id = $this->_getParam('id',0);

        // process post
        if ($this->getRequest()->isPost())
        {
            $params = $this->_getAllParams();
            try {
                $companyRow = $this->_service->update($this->_locations->find($id), $params);
                if ($companyRow)
                {
                    // update company role name
                    $companyRoleRow = $companyRow->getAclRole();
                    // only the company has a role, not the other locations
                    if ($companyRoleRow)
                    {
                        $companyRoleRow->name = $companyRow->name;
                        $companyRoleRow->save();
                    }

                    if (!empty($params['bankAccount'])) {
                        $bankDataNS = $params['bankAccount']['namespace'];
                        unset($params['bankAccount']['namespace']);
                        if (!empty($params['bankAccount'][$bankDataNS]['bankAccount']) && is_array($params['bankAccount'])) {
                            $companyBankAccount = new \Company_Service_BankAccount();
                            $companyBankAccount->bulk($companyRow, $params['bankAccount']);
                        }
                    }
                    $this->_locations->setRegions($companyRow, $this->_getParam('regions', array()));
                    $this->_locations->setLibraries($companyRow, $this->_getParam('libraries', array()));
                    $this->_locations->setThemes($companyRow, $this->_getParam('themes', array()));
                    $this->view->succes = true;
                    $this->_helper->information('Bedrijfsinformatie is succesvol opgeslagen.', true, E_USER_NOTICE);
                    $this->_redirect($this->view->url());
                }
                $this->_helper->information('Bedrijfsinformatie kon niet worden opgeslagen.', true, E_USER_NOTICE);
            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information(array('Error creating company. Error: %s.', array($e->getMessage())), true);
            }
        }

        try
        {
            // get
            $companyRow = $this->_locations->find($id);
            $this->view->companyRow = $companyRow;
            $this->view->companyRegions = $this->_regions->getByLocation($companyRow);
            $this->view->parentId = $this->_getParam('parentId');
            $regions = $this->_regions->fetchAllByCompanyId($this->_toCompanyRow()->getId())->toArray();
            $this->view->regions = $regions['rowset'];

            // get themes
            $_themes = new Company_Service_Theme();
            $this->view->themes = $_themes->fetchAllActive();
            $this->view->locationThemes = $this->_locations->getThemes($companyRow);

            // get libraries
            $_libraries = new Company_Service_Library();
            $libraries = $_libraries->fetchAllActive();
            $this->libraries = array();
            foreach($libraries as $libraryRow)
            {
                $this->libraries[ $libraryRow->getId() ] = $libraryRow;
            }
            $this->view->libraries = $this->libraries;
//            $this->view->locationLibraries = $this->_locations->getLibraries($companyRow);
        } catch(Exception $e) {
            $body = '<div>sent from '.$this->view->url()
                    .'  <div>Errors</div>'
                    .'  <div>errorcode: '.$e->getCode().'</div>'
                    .'  <div>where '.$e->getFile().':'.$e->getLine().'</div>'
                    .'  <div>'.$e->getMessage().'</div>'
                    .'  <pre>'.print_r($e->getTrace(),1).'</pre>'
                    .'  <pre>'.print_r($e, 1).'</pre>'
                    .'</div>'
            ;
            $mail = new Zend_Mail();
            $mail->addTo('elanenga@appalti.nl')
                    ->setBodyHtml($body)
                    ->send();
            throw $e;
        }
    }


    public function deleteAction()
    {
//        $this->_helper->layout->disableLayout(true);

        $id = $this->_getParam('id', 0);
        if ($id)
        {
            try {
                $companyRow = $this->_locations->find($id);
                $params = $companyRow->toArray();
                $params['isActive'] = 0;
                if ($this->_locations->delete($id))
                {
                    // success delete
                    $this->view->succes = true;
                    $this->_helper->information('Bedrijfslocatie is succesvol verwijderd.', true, E_USER_NOTICE);
                    $this->_redirect($this->view->url(array('action'=>null,'id'=>null)));
                }
                // failure
                $this->_helper->information('Bedrijfslocatie kon niet worden verwijderd.', true, E_USER_NOTICE);
                $this->_redirect($this->view->url(array('action'=>null,'id'=>null)));
            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information(array('Error creating company. Error: %s.', array($e->getMessage())), true);
            }
        }
    }
}