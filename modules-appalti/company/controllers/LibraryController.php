<?php
class Company_LibraryController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    protected function _toCompanyRow()
    {
        return Zend_Auth::getInstance()->getIdentity()->getCompanyRow(false);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();
        $this->accountRow = Zend_Auth::getInstance()->getIdentity();
        $this->companyRow = $this->_toCompanyRow();
        $this->_libraries = new Company_Service_Library($this->companyRow);
        $this->view->libraries = array();
        $this->library = new Company_Model_LibraryRow();

        // prepare view
        $this->view->accountRow = $this->accountRow;
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'company:library';
    }


    public function indexAction()
    {
        $this->_forward('select');
    }

    public function selectAction()
    {
        $libraries = $this->_libraries->fetchAllByCompanyId($this->companyRow->getId())->toArray();
        if ($libraries['total'])
        {
            $this->view->libraries = $libraries['rowset'];
        }
        else
        {
            // current clients do already have proposalblocks
            // find these blocks and gather them in a new 'default' library

            // create new library
            $libraryRow = $this->_libraries->create(array(
                'id'            => 0,
                'companyId'     => $this->companyRow->getId(),
                'name'          => 'Standaard',
                'description'   => 'Uw standaard bibliotheek',
            ));

            // add existing proposalblocks to newly created library
            $proposalblockSrv = new ProposalBlock_Service_ProposalBlock($this->_toCompanyRow());
            $proposalBlocks = $proposalblockSrv->fetchAll();
            foreach($proposalBlocks as $proposalBlock)
            {
                $this->_libraries->addProposalBlock($libraryRow, $proposalBlock);
            }

            // add existing company subcategories to newly created library
            $categorySrv = new ProposalBlock_Service_Category();
            $categories = $categorySrv->fetchAll()->getRowset();
            while($category = $categories->current())
            {
                $category->libraryId = $libraryRow->getId();
                $category->save();
                $categories->next();
            }

            // reload page
            $this->_redirect('/company/library');
        }
    }

    public function createAction()
    {
        //process post
        if ($this->getRequest()->isPost())
        {
            try{
                $fromLibraryRow = $this->_libraries->find($this->_getParam('from_library'));
                $toLibraryRow = $this->_libraries->copy($fromLibraryRow, $this->_getParam('to_library'));
                if($toLibraryRow)
                {
                    $this->view->succes = true;
                    $this->_helper->information('De bibliotheek is aangemaakt.', true, E_USER_NOTICE);
                    $this->_redirect('/company/library/update/id/'.$toLibraryRow->getId());
                }
            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information(array('Error creating library. Error: %s.', array($e->getMessage())), true);
            }
        }

        // get libraries
        $this->selectAction();

    }

    public function updateAction()
    {
        $this->_forward('index','index','proposal-block');
    }


    public function themesAction()
    {

    }

    public function deleteAction()
    {
        $id = $this->_getParam('id', 0);
        if ($id)
        {
            try {
                // prevent deleting last library
                $libraries = $this->_libraries->fetchAll();
                if (count($libraries) < 2)
                {
                    throw new OSDN_Exception($this->view->translate('U mag de laatste bibliotheek niet verwijderen.'));
                }

                // get library
                $libraryRow = $this->_libraries->find($id);
                if ($this->_libraries->deleteLibrary($libraryRow))
                {
                    // success delete
                    $this->view->succes = true;
                    $this->_helper->information('De bibliotheek is succesvol verwijderd.', true, E_USER_NOTICE);
                    $this->_redirect($this->view->url(array('action'=>null,'id'=>null)));
                }
                // failure
                $this->_helper->information('De bibliotheek kon niet worden verwijderd.', true, E_USER_NOTICE);
            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessage());
            }
            $this->_redirect($this->view->url(array('action'=>null,'id'=>null)));
        }
    }
}