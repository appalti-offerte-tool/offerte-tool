<?php

class Company_OfferMainController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Company_Service_Offer
     */
    protected $_service;

    public function init()
    {
        parent::init();

        $this->_helper->ContextSwitch()
            ->addContext('block', array(
                'suffix'    => 'block'
            ))
            ->addActionContext('fetch-all', array('json'))
            ->addActionContext('fetch', array('json'))
            ->addActionContext('create', array('json', 'block'))
            ->addActionContext('delete', 'json')
            ->addActionContext('update', 'json')
            ->initContext();

        $this->_service = new \Company_Service_Offer();
    }

    public function getResourceId()
    {
        return 'company';
    }

    public function createAction()
    {
        $this->view->companyId = $this->_getParam('companyId');

        if ( !$this->getRequest()->isPost()) {
            $namespace = $this->_getParam('namespace');
            if (!empty($namespace)) {
                $this->view->namespace = $namespace;
            }

            return;
        }

        try {
            $offerRow = $this->_service->create($this->_getAllParams());
            $this->view->bankAccountId = $offerRow->getId();
            $this->view->success = true;
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        try {
            $offerRow = $this->_service->find($this->_getParam('offerId'));
            $offerRow = $this->_service->update($offerRow, $this->_getAllParams());
            $this->view->offerId = $offerRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
            $this->getResponse()->setHeader('Content-type', 'text/html', true);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAction()
    {
        $offerRow = $this->_service->find($this->_getParam('offerId'));
        $this->view->row = $offerRow->toArray();
        $this->view->success = true;
    }

    public function fetchAllAction()
    {
        $response = $this->_service->fetchAllByCompanyId($this->_getParam('companyId'), $this->_getAllParams());
        $this->view->assign($response->getRowset());
    }

    public function deleteAction()
    {
        $offerId = $this->_getParam('offerId');
        $this->_service->delete($offerId);

        $this->view->offerId = $this->_getParam('offerId');
        $this->view->success = true;
    }

    public function comboBoxAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');
    }
}