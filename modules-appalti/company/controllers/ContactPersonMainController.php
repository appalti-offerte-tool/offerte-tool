<?php

class Company_ContactPersonMainController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Company_Service_ContactPersonMain
     */
    protected $_service;

    public function init()
    {
        parent::init();
        $this->_helper->ContextSwitch()
            ->addContext('form', array (
            'suffix'    => 'form'
             ))
            ->addContext('block', array (
            'suffix'    => 'block'
             ))
            ->addActionContext('fetch', array ('json'))
            ->addActionContext('fetch-all-by-company', array ('json'))
            ->addActionContext('fetch-all-by-client-id', array ('json'))
            ->addActionContext('fetch-all-by-account', array ('json'))
            ->addActionContext('create', array ('json'))
            ->addActionContext('delete', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('update-account', array ('json'))
            ->initContext();

        $this->_service = new \Company_Service_ContactPersonMain();
    }

    public function getResourceId()
    {
        return 'company:company-contact-person';
    }

    public function comboBoxAction()
    {

        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');
        $service = new Account_Service_AclRole();
        $response = $service->fetchByLuids(array ('manager', 'textWriter', 'proposalBuilder'));
        $this->view->rowset = $response->toArray();
    }

    public function createAction()
    {
        $this->getResponse()->setHeader('Content-Type', 'text/html', true);
        $params =   $this->_getAllParams();
        try {

            if (!empty($params['companyId'])) {
                $companySrv = new Company_Service_Company();
                $companyRow = $companySrv->find($params['companyId'], false);
                $companyOwner = $companyRow->getAccountOwner();
            } else {
                $accountService = new Account_Service_Account();
                $companyOwner = $accountService->find($params['accountId']);
                $companyRow = $companyOwner->getCompanyRow();
                $params['companyId'] = $companyRow->getId();
            }

            $contactPersonRow = $this->_service->create($params,$companyOwner==null ? null :  $companyOwner->toArray());
            $this->view->assign($contactPersonRow->toArray());
            $this->view->success = true;
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAllByCompanyAction()
    {
        $contactPersonRow = $this->_service->fetchAllWithResponse($this->_getAllParams(), 'cp.accountId IS NOt NULL');

        $this->view->assign($contactPersonRow->toArray());
        $this->view->success = true;
    }

    public function fetchAllByAccountAction()
    {
        try {
            $params = $this->_getAllParams();
            $accountService = new Account_Service_Account();
            $accountRow = $accountService->find($params['accountId']);
            $companyRow = $accountRow->getCompanyRow();
            $params['companyId'] = $companyRow->getId();
            $contactPersonRow = $this->_service->fetchAllWithResponse($params, 'cp.accountId IS NOt NULL');

            $this->view->assign($contactPersonRow->toArray());
            $this->view->success = true;
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }


    }

    public function fetchAllByClientIdAction()
    {
        $contactPersonRow = $this->_service->fetchAllWithResponse($this->_getAllParams(), 'cp.accountId IS NULL');

        $this->view->assign($contactPersonRow->toArray());
        $this->view->success = true;
    }

    public function fetchAction()
    {
        $contactPersonRow = $this->_service->find($this->_getParam('contactPersonId'));
        $response = $contactPersonRow->toArray();

        if ($response['accountId'] != NULL) {
            $ac = new Account_Service_Account();
            $ac = $ac->find($response['accountId']);
            $ac = $ac->toArray();
            $response['username'] = $ac['username'];
            $response['roleId'] = $ac['roleId'];
            $response['username_old'] = $ac['username'];
            $response['isActive'] = $ac['isActive'];
        }

        $this->view->row = $response;
        $this->view->success = true;
    }

    public function deleteAction()
    {
        try {
            $contactPersonId = $this->_getParam('contactPersonId');
            $this->_service->delete($contactPersonId);
            $this->view->success = true;
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHeader('Content-Type', 'text/html', true);

        try {
            $contactPersonRow = $this->_service->find($this->_getParam('contactPersonId'));
            $this->_service->update($contactPersonRow, $this->_getAllParams());
            $this->view->success = true;
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }


}