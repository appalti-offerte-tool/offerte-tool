Ext.define('Module.Application.Model.Error', {
    extend: 'Ext.data.Model',
    fields: [
        'message', 'type', 'field',
        {name: 'id', mapping: 'field'},
        {name: 'msg', mapping: 'message'}
    ]
});


Ext.define('Module.Company.Offer.Form', {
    extend: 'Ext.form.Panel',

    bodyPadding: 5,

    wnd: null,
    offerId: null,
    companyId: null,
    model: 'Module.Company.Model.Offer',

    fileStockImage: null,

    initComponent: function() {

        this.initialConfig.trackResetOnLoad = true;
        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.Company.Model.Offer',
            type: 'json',
            root: 'row'
        });

        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
            model: 'Module.Application.Model.Error',
            type: 'json',
            root: 'messages',
            successProperty: 'success'
        });

        this.items = [{
            xtype: 'textfield',
            fieldLabel: lang('Name'),
            name: 'name',
            allowBlank: 0,
            anchor: '100%'
        },{
            xtype: 'module.company.offer.combo-box',
            fieldLabel: lang('Kind'),
            name: 'kind',
            hiddenName: 'kind',
            allowBlank: 0,
            anchor: '100%'
        },{
            xtype: 'textareafield',
            fieldLabel: lang('Description'),
            name: 'description',
            grow: true,
            allowBlank: 0,
            anchor: '100%'
        },{
            xtype: 'numberfield',
            fieldLabel: lang('Price'),
            name: 'pricePerUnit',
            allowBlank: 0,
            value: 0,
            minValue: 0,
            anchor: '100%'
        },{
            xtype: 'numberfield',
            fieldLabel: lang('In stock'),
            name: 'inStock',
            value: 0,
            allowBlank: 0,
            minValue: 0,
            anchor: '100%'
        },{
            xtype: 'numberfield',
            fieldLabel: lang('Amount'),
            name: 'amount',
            value: 0,
            allowBlank: 0,
            minValue: 0,
            anchor: '100%'
        },{
            xtype: 'numberfield',
            fieldLabel: lang('Discount'),
            name: 'discount',
            value: 0,
            allowBlank: 0,
            minValue: 0,
            anchor: '100%'
        }];

        this.callParent();

        this.addEvents('complete');
    },

    doLoad: function() {

        if (!this.offerId) {
            return;
        }

        this.form.load({
            url: link('company', 'offer-main', 'fetch', {format: 'json', offerId: this.offerId}),
            scope: this
        });
    },

    onSubmit: function() {

        if (!this.form.isValid()) {
            return false;
        }

        var action = this.offerId ? 'update' : 'create';
        var o = {};
        if (this.offerId) {
            o.offerId = this.offerId;
        }
        if (this.companyId) {
            o.companyId = this.companyId;
        }

        this.form.submit({
            url: link('company', 'offer-main', action, {format: 'json'}),
            params: o,
            success: function(options, action) {

                var decResponse = Ext.decode(action.response.responseText);
                Application.notificate(decResponse.messages);

                if (decResponse.success) {
                    this.fireEvent('complete', this, decResponse.offerId);
                    this.wnd && this.wnd.close();
                }
            },
            scope: this
        });
    },

    showInWindow: function() {
        this.border = false;
        var w = this.wnd = new Ext.Window({
            title: this.offerId ? lang('Update company offer') : lang('Create company offer'),
            resizable: false,
            layout: 'fit',
            iconCls: '',
            width: 550,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Save'),
                handler: this.onSubmit,
                scope: this
            }, {
                text: lang('Close'),
                handler: function() {
                    w.close();
                    this.wnd = null;
                },
                scope: this
            }]
        });

        w.show();
        return w;
    }
});