

Ext.define('Module.Company.Offer.List', {
    extend: 'Ext.ux.grid.GridPanel',
    alias: 'widget.module.company.offer.list',

    filterRequestParam: null,

    features: [{
        ftype: 'filters'

    },{
        id: 'group',
        ftype: 'groupingsummary',
        groupHeaderTpl: '{name}',
        hideGroupedHeader: true,
        remoteRoot: 'summaryData'
    }],

    iconCls: '',

    modeReadOnly: false,

    companyId: null,

    entityId: null,

    initComponent: function() {
        this.store = new Ext.data.Store({
            remoteGroup: true,
            remoteSort: true,
            model: 'Module.Company.Model.Offer',
            proxy: {
                type: 'ajax',
                actionMethods: {
                    read: 'POST',
                    update: 'POST'
                },
                url: link('company', 'offer-main', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            },
            groupField: 'kind'
        });

        this.columns = [{
            header: lang('Name'),
            dataIndex: 'name',
            flex: 3
        }, {
            header: lang('Kind'),
            dataIndex: 'kind',
            flex: 2
        }, {
            header: lang('Price'),
            dataIndex: 'pricePerUnit',
            flex: 1
        }, {
            header: lang('In stock'),
            dataIndex: 'inStock',
            flex: 1
        }, {
            header: lang('Amount'),
            dataIndex: 'amount',
            flex: 1
        }, {
            header: lang('Discount'),
            dataIndex: 'discount',
            flex: 1
        }];

        if (false === this.modeReadOnly) {
            this.columns.push({
                xtype: 'actioncolumn',
                header: lang('Actions'),
                width: 50,
                fixed: true,
                items: [{
                    tooltip: lang('Edit'),
                    iconCls: 'icon-edit-16 icon-16',
                    handler: function(g, rowIndex) {
                        this.onEditOffer(g, g.getStore().getAt(rowIndex));
                    },
                    scope: this
                }, {
                    tooltip: lang('Delete'),
                    iconCls: 'icon-delete-16 icon-16',
                    handler: function(g, rowIndex) {
                        this.onDeleteOffer(g, g.getStore().getAt(rowIndex));
                    },
                    scope: this
                }]
            });
        }

        this.tbar = [{
            text: lang('Create'),
            iconCls: 'icon-create-16',
            qtip: lang('Create new default proposal block'),
            handler: this.onCreateOffer,
            scope: this
        }, '-'];

        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 2
        })];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent();

        if (false === this.modeReadOnly) {
            this.getView().on('itemdblclick', function(w, record) {
                this.onEditOffer(this, record);
            }, this);
        }
    },

    onCreateOffer: function() {
        Application.require([
            'company/offer/form'
        ], function() {
            var f = new Module.Company.Offer.Form({
                companyId: this.companyId
            });

            f.on('complete', function(form, companyId) {
                this.setLastInsertedId(companyId);
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditOffer: function(g, record) {
        Application.require([
            'company/offer/form'
        ], function() {
            var f = new Module.Company.Offer.Form({
                offerId: record.get('id'),
                companyId: this.companyId
            });
            f.doLoad();
            f.on('complete', function(form, companyId) {
                this.setLastInsertedId(companyId);
                g.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onDeleteOffer: function(g, record) {

        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
            if (b != 'yes') {
                return;
            }

            Ext.Ajax.request({
                url: link('company', 'offer-main', 'delete', {format: 'json'}),
                method: 'POST',
                params: {
                    offerId: record.get('id')
                },
                success: function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);

                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope: this
            });
        }, this);
    },

    setCompanyId: function(companyId, forceReload) {

        this.companyId = companyId;

        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.companyId = companyId;
        if (true === forceReload) {
            this.getStore().load();
        }

        return this;
    }


});