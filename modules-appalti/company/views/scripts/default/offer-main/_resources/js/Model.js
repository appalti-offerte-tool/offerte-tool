Ext.define('Module.Company.Model.Offer', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name', 'kind', 'description', 'pricePerUnit', 'inStock', 'amount', 'discount'
    ]
});