Ext.define('Module.Company.Model.Company', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'parentId', 'accountId',
        'name', 'fullname', 'email', 'logo',
        'website', 'kvk', 'btw', 'subscriptionId',
        {name: 'isSubscribe', type: 'boolean', convert: function(rec){ return !!rec }, mapping:'subscriptionId'},
        'actualStreet', 'actualHouseNumber', 'actualPostCode', 'actualCity',
        'postStreet', 'postHouseNumber', 'postPostCode', 'postCity',
        'phone', 'fax', 'type'
    ]
});