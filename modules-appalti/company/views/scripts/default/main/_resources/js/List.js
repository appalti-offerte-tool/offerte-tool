Ext.define('Module.Company.List', {
    extend: 'Ext.ux.grid.GridPanel',
    alias: 'widget.module.company.list',

    filterRequestParam: null,

    features: [{
        ftype: 'filters'
    }],

    iconCls: 'm-company-icon-16',

    modeReadOnly: false,

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.Company.Model.Company',
            proxy: {
                type: 'ajax',
                url: link('company', 'main', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.columns = [{
            header: lang('Name'),
            dataIndex: 'name',
            flex: 1
        }, {
            header: lang('Full name'),
            dataIndex: 'fullname'
        }, {
            header: lang('E-mail'),
            dataIndex: 'email'
        }, {
            header: 'KVK',
            dataIndex: 'kvk'
        }, {
            header: 'BTW',
            dataIndex: 'btw'
        }];

        this.columns.push({
            xtype: 'actioncolumn',
            header: lang('Actions'),
            width: 50,
            fixed: true,
            items: [{
                tooltip: lang('Edit'),
                iconCls: 'icon-edit-16 icon-16',
                handler: function(g, rowIndex) {
                    this.onEditCompany(g, g.getStore().getAt(rowIndex));
                },
                scope: this
            }, {
                text: lang('Delete'),
                iconCls: 'icon-delete-16 icon-16',
                handler: this.onDeleteCompany,
                scope: this
            }]
        });

        this.tbar = [
//            {
//            text: lang('Create'),
//            iconCls: 'icon-create-16',
//            qtip: lang('Create new company'),
//            handler: this.onCreateCompany,
//            scope: this
//        }, '-'
        ];

        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 2
        })];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent();

        if (false === this.modeReadOnly) {
            this.getView().on('itemdblclick', function(w, record) {
                this.onEditCompany(this, record);
            }, this);
        }
    },

    onCreateCompany: function() {
        Application.require([
            'company/./form'
        ], function() {
            var f = new Module.Company.Form({
                accountId: record.get('accountId')
            });

            f.on('complete', function(form, companyId) {
                this.setLastInsertedId(companyId);
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditCompany: function(g, record) {
        Application.require([
            'company/./form'
        ], function() {
            var f = new Module.Company.Form({
                companyId: record.get('id'),
                accountId: record.get('accountId')
            });
            f.doLoad();
            f.on('complete', function(form, companyId) {
                this.setLastInsertedId(companyId);
                g.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onDeleteCompany: function(g, rowIndex) {

        var record = g.getStore().getAt(rowIndex);

        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
            if (b != 'yes') {
                return;
            }

            Ext.Ajax.request({
                url: link('company', 'main', 'delete', {format: 'json'}),
                method: 'POST',
                params: {
                    companyId: record.get('id')
                },
                success: function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);

                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope: this
            });
        }, this);
    }
});