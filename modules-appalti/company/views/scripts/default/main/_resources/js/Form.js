Ext.define('Module.Application.Model.Error', {
    extend: 'Ext.data.Model',
    fields: [
        'message', 'type', 'field',
        {name: 'id', mapping: 'field'},
        {name: 'msg', mapping: 'message'}
    ]
});


Ext.define('Module.Company.Form', {
    extend: 'Ext.form.Panel',

    bodyPadding: 5,

    wnd: null,
    companyId: null,
    parentId : null,
    accountId: null,
    model: 'Module.Company.Model.Company',

    image: null,

    initComponent: function() {

        this.initialConfig.trackResetOnLoad = true;
        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.Company.Model.Company',
            type: 'json',
            root: 'row'
        });

        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
            model: 'Module.Application.Model.Error',
            type: 'json',
            root: 'messages',
            successProperty: 'success'
        });

        this.items = [{
            xtype: 'fieldcontainer',
            layout: 'hbox',
            defaults: {
                border: false
            },
            items: [{
                xtype: 'panel',
                layout: 'anchor',
                flex: 0.7,
                items: [{
                    xtype: 'textfield',
                    fieldLabel: lang('Name'),
                    name: 'name',
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('Full name'),
                    name: 'fullname',
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: 'KVK',
                    name: 'kvk',
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: 'BTW',
                    name: 'btw',
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('E-mail'),
                    name: 'email',
                    vtype: 'email',
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('Website'),
                    name: 'website',
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('Phone'),
                    name: 'phone',
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('Fax'),
                    name: 'fax',
                    anchor: '100%'
                }, {
                    xtype: 'module.company.kind.combo-box',
                    fieldLabel: lang('Type'),
                    name: 'type',
                    hiddenName: 'type',
                    anchor: '100%'
                }]
            }, this.image = new Module.FileStock.Image.Panel({
                flex: 0.3,
                margin: '0 0 0 5',
                bodyPadding: 5,
                height: 240,
                border: false,
                module: 'company',
                controller: 'logo-file-stock',
                useUploadFormField: true,
                onImageClickHandle: false,
                field: {
                    fieldLabel: lang('Image'),
                    name: 'logo'
                }
            })]
        }, {
            xtype: 'panel',
            layout: 'hbox',
            border: false,
            defaults: {
                flex: 0.5,
                defaults: {
                    labelWidth: 70
                }
            },
            items: [{
                xtype: 'fieldset',
                title: lang('Actual address'),
                items: [{
                    xtype: 'textfield',
                    fieldLabel: lang('Street'),
                    name: 'actualStreet',
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('House'),
                    name: 'actualHouseNumber',
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('Post code'),
                    name: 'actualPostCode',
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('City'),
                    name: 'actualCity',
                    anchor: '100%'
                }]
            }, {
                xtype: 'fieldset',
                title: lang('Post address'),
                margin: '0 0 0 5',
                items: [{
                    xtype: 'textfield',
                    fieldLabel: lang('Street'),
                    name: 'postStreet',
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('House'),
                    name: 'postHouseNumber',
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('Post code'),
                    name: 'postPostCode',
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('City'),
                    name: 'postCity',
                    anchor: '100%'
                }]
            }]
        }]

        this.callParent();

        this.addEvents('complete');

        this.on('actioncomplete', function(bf, action) {
            var row = bf.reader.jsonData.row;
            if (row.logo) {
                this.image.setImageLink(row.logo);
            }
        }, this);
    },

    doLoad: function() {

        if (!this.companyId) {
            return;
        }


        this.form.load({
            url: link('company', 'main', 'fetch', {format: 'json', companyId: this.companyId}),
            waitMsg: Ext.LoadMask.prototype.msg,
            scope: this
        });
    },

    onSubmit: function() {

        if (!this.form.isValid()) {
            return false;
        }

        var action = this.companyId ? 'update' : 'create';
        var o = {
            accountId: this.accountId,
            parentId: this.parentId
        };
        if (this.companyId) {
            o.companyId = this.companyId;

        };
        this.form.submit({
            url: link('company', 'main', action, {format: 'json'}),
            params: o,
            waitMsg: Ext.LoadMask.prototype.msg,
            success: function(options, action) {

                var decResponse = Ext.decode(action.response.responseText);
                Application.notificate(decResponse.messages);

                if (decResponse.success) {
                    this.fireEvent('complete', this, decResponse.companyId);
                    this.wnd && this.wnd.close();
                }
            },
            scope: this
        });
    },

    showInWindow: function() {
        this.border = false;
        var w = this.wnd = new Ext.Window({
            title: this.companyId ? lang('Update company') : lang('Create company'),
            resizable: false,
            layout: 'fit',
            iconCls: 'm-company-icon-16',
            width: 650,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Save'),
                handler: this.onSubmit,
                scope: this
            }, {
                text: lang('Close'),
                handler: function() {
                    w.close();
                    this.wnd = null;
                },
                scope: this
            }]
        });

        w.show();

        return w;
    }
});