Ext.define('Module.Company.Model.Company.ContactPerson', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'accountId', 'companyId','username','username_old',
        'firstname', 'lastname', 'sex','fullname',
        'title', 'function', 'department','isActive',
        'phone', 'mobile', 'email', 'photo','roleId',
        'canApproveProposal', 'relationship', 'signature', 'isDefault',
        'phone', 'fax', 'type' ,'luid'
    ]
});