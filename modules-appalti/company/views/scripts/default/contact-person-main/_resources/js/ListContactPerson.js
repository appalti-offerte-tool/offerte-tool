Ext.define('Module.Company.List.ContactPerson', {
    extend: 'Ext.ux.grid.GridPanel',
    alias: 'widget.module.company.list.contact-person',

    filterRequestParam: null,

    features: [{
        ftype: 'filters'
    }],

    iconCls: 'm-company-icon-16',

    modeReadOnly: false,

    accountId : null,
    entityName: null,
    companyId: null,
    method : null,

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.Company.Model.Company.ContactPerson',
            proxy: {
                type: 'ajax',
                url: link('company', 'contact-person-main', this.method, {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.columns = [ {
            header: lang('Full name'),
            dataIndex: 'fullname',

            renderer: function(v, m,record  ){
                return record.raw.firstname+' '+record.raw.lastname
            }
        }, {
            header: lang('Sex'),
            dataIndex: 'sex'
        },  {
            header: 'Phone',
            dataIndex: 'phone'
        }, {
            header: 'Mobile',
            dataIndex: 'mobile'
        }, {
            header: 'E-mail',
            dataIndex: 'email'
        }, {
            header: 'CanApproveProposal',
            dataIndex: 'canApproveProposal',
            renderer: function(value){
                if (value=='1'){
                    return 'Yes'
                } else {
                    return 'No'
                }
            }
        }, {
            header: 'Relationship',
            dataIndex: 'relationship'
        }, {
            header: 'IsDefault',
            dataIndex: 'isDefault',
            renderer: function(value){
                if (value=='1'){
                    return 'Yes'
                } else {
                    return 'No'
                }
            }
        }];

        this.columns.push({
            xtype: 'actioncolumn',
            header: lang('Actions'),
            fixed: true,
            items: [{
                tooltip: lang('Edit'),
                iconCls: 'icon-edit-16 icon-16',
                handler: function(g, rowIndex) {
                    this.onEditPerson(g, g.getStore().getAt(rowIndex));
                },
                scope: this
            }, {
                text: lang('Delete'),
                iconCls: 'icon-delete-16 icon-16',
                handler: this.onDeleteCompany,
                scope: this
            }]
        });

        this.tbar = [
            {
            text: lang('Create'),
            iconCls: 'icon-create-16',
            handler: this.onCreateCompany,
            scope: this
        }, '-'
        ];

        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 2
        })];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent();

        if (false === this.modeReadOnly) {
            this.getView().on('itemdblclick', function(w, record) {
                this.onEditCompany(this, record);
            }, this);
        }
    },

    onCreateCompany: function() {
        Application.require([
            'company/contact-person/form'
        ], function() {
            var f = new Module.Company.Form.ContactPerson({
                companyId: this.companyId,
                accountId: this.accountId
            });
            f.on('complete', function(form, companyId) {
                this.setLastInsertedId(companyId);
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditPerson: function(g, record) {
        Application.require([
            'company/contact-person/form'
        ], function() {

            var f = new Module.Company.Form.ContactPerson({
                companyId: record.get('companyId'),
                contactPersonId: record.get('id')
            });
            f.doLoad();
            f.on('complete', function(form, companyId) {
                this.setLastInsertedId(companyId);
                g.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onDeleteCompany: function(g, rowIndex) {

        var record = g.getStore().getAt(rowIndex);

        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
            if (b != 'yes') {
                return;
            }

            Ext.Ajax.request({
                url: link('company', 'contact-person-main', 'delete', {format: 'json'}),
                method: 'POST',
                params: {
                    contactPersonId: record.get('id')
                },
                success: function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);

                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope: this
            });
        }, this);
    },

    setBaseParams : function(companyId, accountId, method) {


        this.companyId = companyId;
        this.accountId = accountId;
        this.method = method;

        var p = this.getStore().getProxy();

        p.extraParams = p.extraParams || {};
        p.extraParams.companyId = companyId;
        p.extraParams.accountId = accountId;
        p.extraParams.method = method;


        this.getStore().load();


        return this;

    }
});