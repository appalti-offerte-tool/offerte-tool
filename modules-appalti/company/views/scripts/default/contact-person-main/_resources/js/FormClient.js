Ext.define('Module.Application.Model.Error', {
    extend: 'Ext.data.Model',
    fields: [
        'message', 'type', 'field',
        {name: 'id', mapping: 'field'},
        {name: 'msg', mapping: 'message'}
    ]
});


Ext.define('Module.Company.Form.ContactClient', {
    extend: 'Ext.form.Panel',

    bodyPadding: 5,

    wnd: null,
    companyId: null,
    contactPersonId: null,

    model: 'Module.Company.Model.Company.ContactClient',

    photo: null,
    signature: null,

    initComponent: function() {

        this.initialConfig.trackResetOnLoad = true;
        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.Company.Model.Company.ContactPerson',
            type: 'json',
            root: 'row'
        });

        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
            model: 'Module.Application.Model.Error',
            type: 'json',
            root: 'messages',
            successProperty: 'success'
        });

        this.items = [{
            xtype: 'fieldcontainer',
            layout: 'hbox',
            defaults: {
                border: false
            },
            items: [{
                xtype: 'panel',
                border: true,
                width: 335,
                height:160,
                layout: 'anchor',
                items: [

//                    {
//                        xtype: 'textfield',
//                        fieldLabel: 'Prefix',
//                        labelWidth:130,
//                        margin: '10 10 5 10',
//                        name: 'prefix',
//                        allowBlank:false,
//                        anchor: '100%'
//                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: lang('First name'),
                        name: 'firstname',
                        margin: '0 10 5 10',
                        labelWidth:130,
                        allowBlank:false,
                        anchor: '100%'
                    }, {
                        xtype: 'textfield',
                        fieldLabel: lang('Last name'),
                        name: 'lastname',
                        margin: '0 10 5 10',
                        labelWidth:130,
                        allowBlank:false,
                        anchor: '100%'
                    },{
                        xtype:'radiogroup',
                        fieldLabel:'Sex',
                        margin: '0 10 5 10',
                        columns:2,
                        labelWidth:130,
                        vertical:true,
                        allowBlank:false,
                        items:[
                            {
                                boxLabel:'Man',
                                name:'sex',
                                inputValue:'man'
                            }, {
                                boxLabel:'Vrouw',
                                name:'sex',
                                inputValue:'vrouw'
                            }
                        ]
                    }
//                    ,
//                    {
//                        xtype:'radiogroup',
//                        fieldLabel:'Title',
//                        columns:2,
//                        margin: '0 10 5 10',
//                        labelWidth:130,
//                        vertical:true,
//                        allowBlank:false,
//                        items:[
//                            {
//                                boxLabel:'Dhr.',
//                                name:'title',
//                                inputValue:'Dhr.'
//                            }, {
//                                boxLabel:'Mevr.',
//                                name:'title',
//                                inputValue:'Mevr.'
//                            }
//                        ]
//                    }
                ]
            },
            this.photo = new Module.FileStock.Image.Panel({
                bodyPadding: 5,
                height: 140,
                border: false,
                margin: '0 0 0 10',
                module: 'company',
                controller: 'logo-file-stock',
                useUploadFormField: true,
                onImageClickHandle: false,
                field: {
                    fieldLabel: lang('Photo'),
                    name: 'photo'
                }
            }),
            this.signature = new Module.FileStock.Image.Panel({
                bodyPadding: 5,
                height: 140,
                border: false,
                margin: '0 0 0 10',
                module: 'company',
                controller: 'logo-file-stock',
                useUploadFormField: true,
                onImageClickHandle: false,
                field: {
                    fieldLabel: lang('Signature'),
                    name: 'signature'
                }
            })
            ]
        }, {
            xtype: 'panel',
            layout: 'hbox',
            border: false,

            defaults: {
                flex: 0.5,
                defaults: {
                    labelWidth: 70
                }
            },
            items: [{
                xtype: 'fieldset',
                height:130,
                items: [{
                    xtype: 'textfield',
                    fieldLabel: lang('Function'),
                    name: 'function',
                    margin: '10 0 5 0',
                    labelWidth:130,
                    allowBlank:true,
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('Department'),
                    name: 'department',
                    allowBlank:true,
                    labelWidth:130,
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('Phone'),
                    labelWidth:130,
                    name: 'phone',
                    allowBlank:false,
                    anchor: '100%'
                }, {
                    xtype: 'textfield',
                    fieldLabel: lang('Mobile'),
                    labelWidth:130,
                    name: 'mobile',
                    allowBlank:true,
                    anchor: '100%'
                }]
            }, {
                xtype: 'fieldset',
                margin: '0 0 0 5',
                height:130,

                items: [{
                    xtype: 'textfield',
                    labelWidth:130,
                    margin: '10 0 5 0',
                    fieldLabel: 'E-mail',
                    name: 'email',
                    vtype: 'email',
                    allowBlank:false,
                    anchor: '100%'
                },{
                    xtype:'radiogroup',
                    fieldLabel:'Relationship',
                    labelWidth:130,
                    columns:2,
                    vertical:true,
                    allowBlank:false,
                    items:[
                        {
                            boxLabel:'Formal',
                            name:'relationship',
                            inputValue:'formal',
                            checked: true
                        }, {
                            boxLabel:'Informal',
                            name:'relationship',
                            inputValue:'informal'
                        }
                    ]
                },{
                    xtype:'radiogroup',
                    fieldLabel:'Can approve proposal',
                    columns:2,
                    labelWidth:130,
                    vertical:true,
                    allowBlank:false,
                    items:[
                        {
                            boxLabel:'Yes',
                            name:'canApproveProposal',
                            inputValue:'1',
                            checked: true
                        }, {
                            boxLabel:'No',
                            name:'canApproveProposal',
                            inputValue:'0'
                        }
                    ]
                }]
            }]
        }];

        this.callParent();

        this.addEvents('complete');

        this.on('actioncomplete', function(bf, action) {
            var row = bf.reader.jsonData.row;
            if (row.photo) {
                this.photo.setImageLink('/modules-appalti/company/data/images-contact-person-photo/'+row.photo);
            }
            if (row.signature) {
                this.signature.setImageLink('/modules-appalti/company/data/images-contact-person-signature/'+row.signature);
            }
        }, this);
    },

    doLoad: function() {

        if (!this.companyId) {
            return;
        }

        this.form.load({
            url: link('company', 'contact-person-main', 'fetch', {format: 'json',
                companyId: this.companyId,
                contactPersonId: this.contactPersonId,
            }),
            waitMsg: Ext.LoadMask.prototype.msg,
            scope: this
        });
    },

    onSubmit: function() {

        if (!this.form.isValid()) {
            return false;
        }

        var action = this.contactPersonId ? 'update' : 'create';
        var o = {
            companyId: this.companyId
        };
        if (this.contactPersonId) {
            o.contactPersonId = this.contactPersonId;
        }
        this.form.submit({
            url: link('company', 'contact-person-main', action, {format: 'json'}),
            params: o,
            waitMsg: Ext.LoadMask.prototype.msg,
            success: function(options, action) {

                var decResponse = Ext.decode(action.response.responseText);

                Application.notificate(decResponse.messages);

                if (decResponse.success) {
                    this.fireEvent('complete', this, decResponse.companyId);
                    this.wnd && this.wnd.close();
                }
            },
            scope: this
        });
    },

    showInWindow: function() {
        this.border = false;
        var w = this.wnd = new Ext.Window({
            title: this.companyId ? lang('Update company') : lang('Create company'),
            resizable: true,
            layout: 'fit',
            iconCls: 'm-company-icon-16',
            width: 700,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Save'),
                handler: this.onSubmit,
                scope: this
            }, {
                text: lang('Close'),
                handler: function() {
                    w.close();
                    this.wnd = null;
                },
                scope: this
            }]
        });

        w.show();
        return w;
    }
});