Modules = typeof Modules == 'object' ? Modules : {};
Modules.Company = function(companyId) {
    this.companyId = companyId;
    var me = this;

    this.bankAccountBox = $("#bank-accounts");
    this.bankAccountBox.on('click', 'a.remove-bank-account', function() {
        if ($('.bank-account-box').length > 1) {
            confirm('Weet je het zeker?') && me.onDeleteBankAccount(this);
        }
        return false;
    });

    $('#btn-create-bank-account').click(function() {
        me.onAttachBlankBankAccount();
        return false;
    });

    this.contactPersonBox = $("#contact-person-box");
    this.contactPersonBox.on('click', 'a.remove-contact-person', function() {
        if ($('.contact-person-item').length > 1) {
            confirm('Weet je het zeker?') && me.onDeleteContactPerson(this);
        }
        return false;
    });

    this.addrSourceInputs = $('#actual-address input:text').each(function() {
        $(this).data('related', $('input[name="' + this.name.replace('actual', 'post') + '"]'));
    });

    this.addrTargetInputs = $('#post-address input:text');

    this.el = this.addrTargetInputs.closest('form');

    me.addrSourceInputs.blur(function() {
        var related = $(this).data('related');
        me.actualEqualsPostAddr[0].checked && related.val(this.value);
        me.el.validate().element(related);
    });

    this.actualEqualsPostAddr = $('#post-same-as-actual').change(function() {
        if (this.checked) {
            me.addrSourceInputs.each(function() {
                $(this).triggerHandler('blur');
            });
            me.addrTargetInputs.attr('readonly', 'readonly')
        } else {
            me.addrTargetInputs.removeAttr('readonly');
        }
    });

    if (!me.el.data('validator')) {
        me.el.observe().validate();
    }

    $('#create-contact-person-later').change(function() {
        var container = $('.contact-person-item'),
            cpTargetInputs = $(':input', container);
        this.checked ? cpTargetInputs.attr('disabled', 'disabled') :
                       cpTargetInputs.removeAttr('disabled');
        container.toggleClass('hidden', this.checked);
    })
    .triggerHandler('change');
};

Modules.Company.prototype = {

    companyId: null,
    bankAccount: null,
    contactPersonBox: null,

    onDeleteBankAccount: function(ba) {
        var $ba = $(ba);
        var box = $('#' + $ba.attr('rel'));

        var bankAccountId = $ba.data('id');
        if (!bankAccountId || !this.companyId) {
            box.remove();
            return false;
        }

        $.post(link('company', 'bank-account', 'delete', {
            format: 'json',
            companyId: this.companyId,
            bankAccountId: bankAccountId
        }), function() {
            box.remove();
        }, 'json');

        return false;
    },

    onAttachBlankBankAccount: function() {

        var me = this.bankAccountBox;
        var ns = $('div', me).length;

        $.ajax(link('company', 'bank-account', 'create', {format: 'block', namespace: 'create' + ns}), {
            method: 'get'
        }).done(function(response, success) {
            me.append(response);
        });

        return false;
    },

    onAttachBlankContactPerson: function() {

        var me = this.contactPersonBox;
        var ns = $('table', me).length;

        $.ajax(link('company', 'contact-person', 'create', {
            format: 'block',
            namespace: 'create' + ns,
            companyId: $('input[name="companyId"]').val()
        }), {
            method: 'get'
        }).done(function(response, success) {
            me.append(response);

            /**
             * Go to last contact person form
             */
            window.location.href = '#' + ns;
        });

        return false;
    },

    removeBox: function($box) {
        $box.fadeOut($box.remove);
    },

    onDeleteContactPerson: function(a) {

        var $a = $(a);
        var box = $('#' + $a.attr('rel'));
        var me = this;

        var contactPersonId = $a.data('id');
        if (!contactPersonId || !this.companyId) {
            me.removeBox(box);
            return false;
        }

        $.post(link('company', 'contact-person', 'delete', {
            format: 'json',
            companyId: this.companyId,
            contactPersonId: contactPersonId
        }), function() {
            me.removeBox(box);
        }, 'json');

        return false;
    }
};