!function($) {
    var ClientSummary = function() {
        $('#saveCompanyData').click($.proxy(this.save, this));
        this.initStatusForm();
    }

    ClientSummary.prototype = {

        initStatusForm: function() {
            var me = this;
            me.companyStatusForm = $('#companyStatus').observe()
                .on('change', 'input[name=active]', function() {
                    var target = $('[name=setActiveTill]', me.companyStatusForm),
                        dp = $('.datepicker', me.companyStatusForm);

                    if (parseInt(this.value)) {
                        target.removeAttr('disabled');
                    } else {
                        dp.attr('disabled', 'disabled').val('').datepicker("setDate", null);
                        target.attr('disabled', 'disabled').removeAttr('checked');
                    }
                })
                .on('change', 'input[name=setActiveTill]', function() {
                    var dp = $('.datepicker', me.companyStatusForm)
                    if(this.checked) {
                        var currentDate = new Date(new Date().getTime());
                        $(dp).datepicker('option','minDate', currentDate);
                        $(dp).datepicker('setDate', currentDate);
                        dp.removeAttr('disabled');
                    }
                    else {
                        dp.attr('disabled', 'disabled').val('').datepicker("setDate", null);
                    }
                });
        },

        saveCompanyStatus: function() {
            var me = this, form = me.companyStatusForm;

            if (form.observable().isDirty()) {
                return $.ajax({
                    type: 'post',
                    url: form[0].action,
                    data: form.serialize(),
                    success: function(response) {
                        var parent = form.parent();
                        form.remove();
                        parent.append(response);
                        me.initStatusForm();
                        AppaltiLayout.initDatePicker(me.companyStatusForm);
                    }
                });
            }
        },

        saveCompanyInfo: function() {
            var form = $('#company-info');

            if (form.length && form.observable().isDirty()) {
                return form.ajaxSubmit({
                    target: form.parents('.accordion-section'),
                    beforeSend: function() {
                        return form.data('validator').form();
                    }
                });
            }

        },

        saveCompanyDefaults: function() {
            var form = $('#company-default-fields');

            if (form.length && form.observable().isDirty()) {
                return $.ajax({
                    url: form[0].action,
                    data: form.serialize(),
                    type: 'post',
                    success: function(html) {
                        form.parents('.accordion-section').html(html);
                    }
                });
            }
        },

        save: function() {
            $.when(
                AppaltiLayout.mask('Wijzigingen op te slaan. Een ogenblik geduld.'),
                this.saveCompanyStatus(),
                this.saveCompanyInfo(),
                this.saveCompanyDefaults()
            ).done(function() {
                AppaltiLayout.unmask();
            });

            return false;
        }
    }

    $(function() {
        window.AdminClientSummary = new ClientSummary();
    });
}(window.jQuery);
