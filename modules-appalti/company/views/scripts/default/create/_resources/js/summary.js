$(function() {
    var $modalCnt, $previewImg;
    function preview() {
        if (!$modalCnt) {
            $modalCnt = $('<div id="modal-container" class="tpl-preview modal"/>').appendTo('body');
            $previewImg = $('<img/>').appendTo($modalCnt);
        }

        $previewImg.attr('src', this.href);
        $modalCnt.modal('toggle');

        return false;
    }

    $('body').on('click','.icon-preview-tpl', preview);
})
