Ext.define('Module.Company.Model.Kind', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name'
    ]
});

Ext.define('Module.Company.Kind.ComboBox', {

    extend: 'Ext.form.ComboBox',
    alias: 'widget.module.company.kind.combo-box',
    queryMode: 'local',
    editable: false,
    listConfig: {
        resizable: false
    },

    valueField: 'id',
    displayField: 'name',
    trackResetOnLoad: true,

    initComponent: function() {

        var data = [];
        Ext.each(['suspect', 'prospect', 'account'], function(i) {
            data.push({
                id: i,
                name: lang(i.ucFirst())
            });
        });

        this.store = Ext.create('Ext.data.Store', {
            model: 'Module.Company.Model.Kind',
            data: data
        });

        this.callParent(arguments);
    }
});