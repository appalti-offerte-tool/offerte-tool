$(function() {
    var modalCnt;
    $('#create-offer, .edit-offer').click(function() {
        $.ajax({
            url: this.href,
            mask: true,
            success: function(html) {
                if (!modalCnt) {
                    modalCnt = $('<div class="modal offer-modal-container" data-backdrop="static" />').appendTo('body')
                        .on('hide', function() {
                            return !modalCnt.find('form').observable().isDirty() || confirm('Wijzigingen annuleren?');
                        })
                        .on('hidden', function() {
                            modalCnt.empty();
                        });
                }

                modalCnt.html(html).modal('show')
                AppaltiLayout.initTooltip(modalCnt);
            }
        });

        return false;
    });
});