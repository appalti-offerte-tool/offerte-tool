Ext.define('Module.Company.Block.Information', {

    extend: 'Ext.panel.Panel',
    loader: true,

    btn: null,

    entityId: null,
    entityName: null,
    method: null,
    companyId: null,

    initComponent: function() {

        this.tbar = [];
        this.btn = new Ext.Button({
            handler: Ext.emptyFn,
            scope: this
        });
        this.tbar.push(this.btn);

        this.tbar.push('->');
        this.tbar.push({
            text: lang('Refresh'),
            iconCls: 'x-tbar-loading',
            handler: this.doLoad,
            scope: this
        });

        this.callParent();
    },

    doLoad: function() {

        this.companyId = null;
        var o = {
            format: 'plain'
        };
        o[this.toEntity()] = this.entityId;
        this.getLoader().load({
            url: link('company', 'index', this.method, o),
            method: 'GET',
            loadMask: true,
            callback: this.onRequestCompleted,
            scope: this
        });
    },

    toEntity: function() {
        return this.entityName + 'Id';
    },

    setEntityId: function(entityId, entityName, method, forceReload) {
        this.entityId = entityId;
        this.entityName = entityName;
        this.method = method;

        if (true === forceReload) {
            this.doLoad();
        }
    },

    onRequestCompleted: function() {

        if ('account' != this.entityName) {
            return;
        }

        var ih = this.getEl().down('input[name=companyId]');
        if (!ih) {
            this.btn.setHandler(this.onCreateCompany, this),
            this.btn.setText('Create');
            this.btn.setIconCls('icon-create-16');
        } else {
            this.companyId = ih.dom.value;
            this.btn.setHandler(this.onEditCompany, this);
            this.btn.setText('Edit');
            this.btn.setIconCls('icon-edit-16');
        }
    },

    onCreateCompany: function() {
        Application.require([
            'company/./form'
        ], function() {
            var f = new Module.Company.Form({
                accountId: this.entityId
            });

            f.on('complete', this.doLoad, this);
            f.showInWindow();
        }, this);
    },

    onEditCompany: function(g, record) {
        Application.require([
            'company/./form'
        ], function() {
            var f = new Module.Company.Form({
                companyId: this.companyId
            });
            f.doLoad();
            f.on('complete', this.doLoad, this);
            f.showInWindow();
        }, this);
    }
});