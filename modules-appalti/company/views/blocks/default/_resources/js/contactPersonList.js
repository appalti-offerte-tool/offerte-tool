AppaltiLayout.blocks.ContactPersonList = new AppaltiLayoutBlock({
    module: 'company',
    block: 'contact-person-list',
    msg: {
        confirmDelete: 'Verwijder deze contactpersoon?',
        confirmCancelChanges: 'Annuleren contactpersoon gegevens veranderingen?'
    },

    onBeforeInit: function() {
        $.extend(this, AppaltiLayout.blocks.contactPersonListBlockParams || {});
    },

    onInit: function() {
        var me = this;
        me.$el
        .on('click', '.icon-delete', function() {
            if (confirm(me.msg.confirmDelete)) {
                $(this).tooltip('hide');
                $.post(this.href, function(response) {
                    if (response.success) {
                        me.reloadList();
                    } else {
                        if (response.messages[0] && response.messages[0].message) {
                            alert(response.messages[0].message);
                        }
                    }
                });
            }

            return false;
        })
        .on('click', '.icon-edit', function() {
            me.getForm(this.href);
            return false;
        });

        $('#contact-person-create').click(function() {
            me.getForm(this.href);
            return false;
        });
    },

    onReloadList: function() {
        AppaltiLayout.initTooltip(this.$el);
    },

    bindFormSubmitEvent: function ($form, submitCallback, scope) {
        var $preloader = $('.ajax-preloader', $form), me = this;
        $form.attr('action', $form.attr('action').replace('/format/json','') +'/format/json');
        $form.ajaxForm({
            data: window.isIE ? {returnHtml: true} : {},
            beforeSubmit: function() {
                if (!$form.valid()) {
                    return false;
                }
                $preloader.addClass('show');
            },
            success: function(data) {
                $preloader.removeClass('show');
                data = $.parseJSON(data);
                $.isFunction(submitCallback) && submitCallback.call(scope || me, data);
            }
        });
    },

    getForm: function (url) {
        var me = this;
        return $.ajax({
            url: url,
            mask: true,
            success: function(data) {
                if (!me.modalCnt) {
                    me.modalCnt = $('<div class="modal" data-backdrop="static"></div>').appendTo('body');
                }

                me.modalCnt.html(data);

                var $form = $('form', me.modalCnt);
                me.bindFormSubmitEvent($form, function(data) {
                    if (data.success) {
                        me.reloadList();
                        me.modalCnt.off('hide').modal('hide');
                    } else {
                        alert(data.messages[0].message);
                    }
                }, me);

                me.modalCnt
                    .off('hide hidden')
                    .on('hide', function() {
                        return !$form.observable().isDirty() ||
                               confirm(me.msg.confirmCancelChanges);
                    })
                    .on('hidden', function() {
                        me.modalCnt.off('hide hidden').empty()
                    })
                    .modal('show');

                AppaltiLayout.initTooltip(me.modalCnt);
            }
        })
    }
});