<?php

class Company_View_Helper_CompanyLogoFileStock extends Zend_View_Helper_Abstract
{
    /**
     * @var Company_Model_CompanyRow
     */
    protected $_companyRow;

    protected $_imageName;
    protected $_imagePath;
    protected $_imageRelativePath;

    public function CompanyLogoFileStock(Company_Model_CompanyRow $companyRow)
    {
        $this->_companyRow = $companyRow;
        $this->_imageName = basename($this->_companyRow->logo);

        /**
         * @todo Make it more lazy...
         */
        $service = new \Company_Service_CompanyLogoFileStock($this->_companyRow);
        $this->_imagePath = rtrim($service->toImageFileStockPath(), '\\\/') . DIRECTORY_SEPARATOR;
        $this->_imageRelativePath = rtrim($service->toImageFileStockRelativePath(), '\\\/') . '/';

        return $this;
    }

    public function __toString()
    {
        return '';
    }

    public function resize($width, $height = null)
    {
        if (empty($this->_imageName)) {
            return '';
        }
        $imagePath = $this->_imagePath . $this->_imageName;

        if (!file_exists($imagePath)) {
            return '';
        }


        if (!\is_numeric($height) || \intval($height) == 0) {
            $height = $width;
        }

        $resizedPath = $this->_imagePath . 'resized' . DIRECTORY_SEPARATOR . $width . 'x' . $height;
        if (!\is_dir($resizedPath)) {
            \mkdir($resizedPath, 0755, true);
        }

        $imageResized = $resizedPath . \DIRECTORY_SEPARATOR . $this->_imageName;
        if (
            !file_exists($imageResized) || (
                file_exists($imagePath) && filemtime($imagePath) > filemtime($imageResized)
            )
        ) {
            $imgFilter = new Zend_Filter_ImageSize();
            $imgFilter
                ->setWidth($width)
                ->setHeight($height)
                ->setQuality(100)
                ->setStrategy(new \Zend_Filter_Imagesize_Strategy_Fit())
                ->setThumnailDirectory($resizedPath)
                ->filter($imagePath);
        }

        if (file_exists($imageResized)){
            return $this->_imageRelativePath . 'resized/' . $width . 'x' . $height . '/' . $this->_imageName;
        }

        return $this->_imageRelativePath . $this->_imageName;
    }
}