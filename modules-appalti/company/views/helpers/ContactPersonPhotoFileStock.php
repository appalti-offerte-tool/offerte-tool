<?php

class Company_View_Helper_ContactPersonPhotoFileStock extends Zend_View_Helper_Abstract
{
    /**
     * @var Company_Model_ContactPersonRow
     */
    protected $_contactPersonRow;

    protected $_imageName;
    protected $_imagePath;
    protected $_imageRelativePath;

    public function ContactPersonPhotoFileStock(\Company_Model_ContactPersonRow $contactPersonRow = null, $name = 'photo')
    {

        if (empty($contactPersonRow)) {
            return $this;
        }

        $this->_contactPersonRow = $contactPersonRow;

        $this->_imageName = basename($this->_contactPersonRow->$name);

        /**
         * @todo Make it more lazy...
         */
        $service = new \Company_Service_ContactPersonPhotoFileStock($this->_contactPersonRow, $name);
        $this->_imagePath = rtrim($service->toImageFileStockPath(), '\\\/') . DIRECTORY_SEPARATOR;
        $this->_imageRelativePath = rtrim($service->toImageFileStockRelativePath(), '\\\/') . '/';

        return $this;
    }

    public function __toString()
    {
        return '';
    }


    public function _toPlaceholder($width = null, $height = null)
    {
        if (empty($width)) {
            $width = 70;
        }
        if (empty($height)) {
            $height = $width;
        }
        return '/layouts/scripts/appalti/main/_resources/images/30x30.gif';
    }

    public function resize($width, $height = null)
    {
        if (empty($this->_imageName)) {
            return $this->_toPlaceholder($width, $height);
        }

        $imagePath = $this->_imagePath . $this->_imageName;
        if (!file_exists($imagePath)) {
            return $this->_toPlaceholder($width, $height);
        }

        if (!\is_numeric($height) || \intval($height) == 0) {
            $height = $width;
        }

        $resizedPath = $this->_imagePath . 'resized' . DIRECTORY_SEPARATOR . $width . 'x' . $height;
        if (!\is_dir($resizedPath)) {
            \mkdir($resizedPath, 0755, true);
        }

        $imageResized = $resizedPath . \DIRECTORY_SEPARATOR . $this->_imageName;
        if (
            !file_exists($imageResized) || (
                file_exists($imagePath) && filemtime($imagePath) > filemtime($imageResized)
            )
        ) {
            $imgFilter = new Zend_Filter_ImageSize();
            $imgFilter
                ->setWidth($width)
                ->setHeight($height)
                ->setQuality(100)
                ->setStrategy(new \Zend_Filter_Imagesize_Strategy_Fit())
                ->setThumnailDirectory($resizedPath)
                ->filter($imagePath);
        }

        if (file_exists($imageResized)){
            return $this->_imageRelativePath . 'resized/' . $width . 'x' . $height . '/' . $this->_imageName . '?_dc=' . \uniqid('cp');
        }

        return $this->_imageRelativePath . $this->_imageName . '?_dc=' . \uniqid('cp');
    }
}