<?php

class Company_View_Helper_ContactPersonComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    public function contactPersonComboBox(Company_Model_CompanyRow $companyRow)
    {
        $service = new Company_Service_ContactPerson($companyRow);
        $response = $service->fetchAllWithResponse();

        $options = array();
        $response->getRowset(function($row) use (& $options) {
            $options[$row->getId()] = $row->getFullname();
            return false;
        });
        $this->_options = $options;

        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }
}