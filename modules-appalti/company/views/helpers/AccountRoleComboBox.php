<?php

class Company_View_Helper_AccountRoleComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    protected $_luids = array(
        'admin' => array('companyOwner', 'manager', 'proposalBuilder'),
        'companyOwner' => array('companyOwner', 'manager', 'proposalBuilder'),
        'manager' => array('manager', 'proposalBuilder'),
        '' => array(''),
    );

    /**
     * The constructor
     *
     * Initialize options for combo field
     */
    public function __construct()
    {

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $accountRole = $accountRow->getRole();

        $roleSrv = new \Account_Service_AclRole();
        $luid = $accountRow->isAdmin() ? 'admin' : $accountRole->luid;
        $rowset = $roleSrv->fetchByLuids($this->_luids[ $luid ]);
        foreach ($rowset as $row) {
            $this->_options[$row->id] = $row->name;
        }

        $company = $accountRow->getCompanyRow();
//        $rowset = $roleSrv->findCompanyRoleById($company->getId());
        $rowset = $roleSrv->findChildRolesByCompanyId($company->getId());
        foreach ($rowset as $row) {
            $this->_options[$row->id] = $row->name;
        }
    }

    public function accountRoleComboBox()
    {
        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }

}