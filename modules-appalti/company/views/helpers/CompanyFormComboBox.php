<?php

class Company_View_Helper_CompanyFormComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    /**
     * The constructor
     *
     * Initialize options for combo field
     */
    public function __construct()
    {
        $this->_options = array(
            '' => 'Leeg',
            'BV' => 'BV',
            'NV' => 'NV',
            'VOF' => 'VOF'
        );
    }

    public function companyFormComboBox()
    {
        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }

    public function toLabel($value)
    {
        return $this->_options[$value];
    }

    public function toArray()
    {
        return $this->_options;
    }
}