<?php

class Company_View_Helper_OfferKindComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    /**
     * The constructor
     *
     * Initialize options for combo field
     */
    public function __construct()
    {
        $this->_options = array(
            'service' => 'Dienst',
            'product' => 'Product'
        );
    }

    public function offerKindComboBox()
    {
        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formRadio($name, $value, $attribs, $this->_options);
    }

    public function toLabel($value)
    {
        return $this->_options[$value];
    }

    public function toArray()
    {
        return $this->_options;
    }
}