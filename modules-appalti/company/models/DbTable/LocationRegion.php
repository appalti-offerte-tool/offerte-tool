<?php

class Company_Model_DbTable_LocationRegion extends \OSDN_Db_Table_Abstract
{
    //protected $_primary = 'id';

    protected $_name = 'locationregion';

//    protected $_rowClass = '\\Company_Model_LocationRegionRow';

    protected $_referenceMap    = array(
        'Location'    => array(
            'columns'       => array('locationId'),
            'refTableClass' => 'Company_Model_DbTable_Location',
            'refColumns'    => array('id'),
        ),
        'Region'    => array(
            'columns'       => array('regionId'),
            'refTableClass' => 'Company_Model_DbTable_Region',
            'refColumns'    => array('id'),
        ),
    );
}