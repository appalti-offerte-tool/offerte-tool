<?php

class Company_Model_DbTable_ContactPerson extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'contactPerson';

    protected $_rowClass = 'Company_Model_ContactPersonRow';

    protected $_referenceMap    = array(
        'Company'    => array(
            'columns'       => 'companyId',
            'refTableClass' => 'Company_Model_DbTable_Company',
            'refColumns'    => 'id'
        ),
        'Account'    => array(
            'columns'       => 'accountId',
            'refTableClass' => 'Account_Model_DbTable_Account',
            'refColumns'    => 'id'
        ),
        'Proposal'    => array(
            'columns'       => 'id',
            'refTableClass' => 'Proposal_Model_DbTable_Proposal',
            'refColumns'    => 'contactPersonId'
        )
    );
}