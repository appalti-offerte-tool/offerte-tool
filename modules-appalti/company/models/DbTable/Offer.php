<?php

class Company_Model_DbTable_Offer extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'companyOffer';

    protected $_rowClass = '\\Company_Model_OfferRow';

    protected $_referenceMap    = array(
        'offer'    => array(
            'columns'       => 'companyId',
            'refTableClass' => 'Company_Model_DbTable_Company',
            'refColumns'    => 'id'
        )
    );
}