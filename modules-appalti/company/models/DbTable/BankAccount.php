<?php

class Company_Model_DbTable_BankAccount extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'companyBankAccount';

    protected $_rowClass = '\\Company_Model_BankAccountRow';

    protected $_referenceMap    = array(
        'BankAccount'    => array(
            'columns'       => 'companyId',
            'refTableClass' => 'Company_Model_DbTable_Company',
            'refColumns'    => 'id'
        ),
//        'BankAccountLocation'    => array(
//            'columns'       => 'companyId',
//            'refTableClass' => 'Company_Model_DbTable_Company',
//            'refColumns'    => 'id'
//        ),
    );
}