<?php

class Company_Model_DbTable_LibraryProposalCustomTemplate extends \OSDN_Db_Table_Abstract
{
    protected $_primary = array(
        'libraryId',
        'proposalcustomtemplateId'
    );

    protected $_name = 'libraryProposalcustomtemplate';

//    protected $_rowClass = '\\Company_Model_LibraryProposalCustomTemplateRow';

    protected $_referenceMap    = array(
        'Library'    => array(
            'columns'       => array('libraryId'),
            'refTableClass' => 'Company_Model_DbTable_Library',
            'refColumns'    => array('id'),
        ),
        'ProposalCustomTemplate'    => array(
            'columns'       => array('proposalcustomtemplateId'),
            'refTableClass' => 'Proposal_Model_DbTable_CustomTemplate',
            'refColumns'    => array('id'),
        ),
    );
}