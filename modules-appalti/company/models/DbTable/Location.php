<?php

class Company_Model_DbTable_Location extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'company';

    protected $_rowClass = '\\Company_Model_CompanyRow';

    protected $_nullableFields = array(
        'accountId', 'parentId'
    );

    protected $_referenceMap    = array(
        'Location'    => array(
            'columns'       => 'motherId',
            'refTableClass' => 'Company_Model_DbTable_Company',
            'refColumns'    => 'id',
        ),
        'Proposal'    => array(
            'columns'       => 'id',
            'refTableClass' => 'Proposal_Model_DbTable_Proposal',
            'refColumns'    => 'locationId',
        ),
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        if (empty($data['createdDatetime'])) {
            $dt = new \DateTime();
            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        if (!empty($data['parentId'])) {
            unset($data['accountId']);
        }

        $data['createdAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        return parent::insert($data);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
        /**
         * Omit changing company owner
         */
        unset($data['accountId'], $data['parentId']);

        return parent::update($data, $where);
    }

    public function fetchAllLocations()
    {
        // own company always first
        $locations = array(
            $this->_companyRow,
        );
        // find child locations
        $locationRowset = $this->_companyRow->findDependentRowset($this->_table, null, $this->_table->select()->order('isActive DESC'));
        foreach($locationRowset as $locationRow)
        {
            $locations[] = $locationRow;
        }
        return $locations;
    }
}