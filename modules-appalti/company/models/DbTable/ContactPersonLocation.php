<?php

class Company_Model_DbTable_ContactPersonLocation extends \OSDN_Db_Table_Abstract
{
    //protected $_primary = 'id';

    protected $_name = 'contactpersonLocation';

//    protected $_rowClass = '\\Company_Model_LocationProposalCustomTemplateRow';

    protected $_referenceMap    = array(
        'ContactPerson'    => array(
            'columns'       => array('contactpersonId'),
            'refTableClass' => 'Company_Model_DbTable_ContactPerson',
            'refColumns'    => array('id'),
        ),
        'Location'    => array(
            'columns'       => array('locationId'),
            'refTableClass' => 'Company_Model_DbTable_Location',
            'refColumns'    => array('id'),
        ),
    );
}