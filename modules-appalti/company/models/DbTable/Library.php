<?php

class Company_Model_DbTable_Library extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'library';

    protected $_rowClass = '\\Company_Model_LibraryRow';


    protected $_referenceMap = array(
        'Company' => array(
            'columns'       => 'companyId',
            'refTableClass' => 'Company_Model_DbTable_Company',
            'refColumns'    => 'id'
        ),
    );
}