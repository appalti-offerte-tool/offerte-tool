<?php

class Company_Model_DbTable_LocationLibrary extends \OSDN_Db_Table_Abstract
{
    //protected $_primary = 'id';

    protected $_name = 'locationLibrary';

//    protected $_rowClass = '\\Company_Model_LocationProposalCustomTemplateRow';

    protected $_referenceMap    = array(
        'Location'    => array(
            'columns'       => array('locationId'),
            'refTableClass' => 'Company_Model_DbTable_Location',
            'refColumns'    => array('id'),
        ),
        'Library'    => array(
            'columns'       => array('libraryId'),
            'refTableClass' => 'Company_Model_DbTable_Library',
            'refColumns'    => array('id'),
        ),
    );
}