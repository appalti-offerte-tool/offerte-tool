<?php

class Company_Model_DbTable_LocationProposalCustomTemplate extends \OSDN_Db_Table_Abstract
{
    //protected $_primary = 'id';

    protected $_name = 'locationProposalcustomtemplate';

    protected $_rowClass = '\\Company_Model_LocationProposalCustomTemplateRow';

    protected $_referenceMap    = array(
        'Location'    => array(
            'columns'       => array('locationId'),
            'refTableClass' => 'Company_Model_DbTable_Location',
            'refColumns'    => array('id'),
        ),
        'Company'    => array(
            'columns'       => array('locationId'),
            'refTableClass' => 'Company_Model_DbTable_Company',
            'refColumns'    => array('id'),
        ),
        'ProposalCustomTemplate'    => array(
            'columns'       => array('proposalcustomtemplateId'),
            'refTableClass' => 'Proposal_Model_DbTable_CustomTemplate',
            'refColumns'    => array('id'),
        ),
    );
}