<?php

class Company_Model_DbTable_Company extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'company';

    protected $_rowClass = '\\Company_Model_CompanyRow';

    protected $_nullableFields = array(
        'accountId', 'parentId'
    );

    protected $_referenceMap    = array(
        'Children'    => array(
            'columns'       => 'parentId',
            'refTableClass' => 'Company_Model_DbTable_Company',
            'refColumns'    => 'id'
        ),
        'Account'    => array(
            'columns'       => 'accountId',
            'refTableClass' => 'Account_Model_DbTable_Account',
            'refColumns'    => 'id'
        ),
        'Location' => array(
            'columns'       => 'motherId',
            'refTableClass' => 'Company_Model_DbTable_Company',
            'refColumns'    => 'id',
        ),
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        if (empty($data['createdDatetime'])) {
            $dt = new \DateTime();
            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        if (!empty($data['parentId'])) {
            unset($data['accountId']);
        }

        $data['createdAccountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        return parent::insert($data);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
        /**
         * Omit changing company owner
         */
        unset($data['accountId'], $data['parentId']);

        return parent::update($data, $where);
    }
}