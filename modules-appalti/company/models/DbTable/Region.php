<?php

class Company_Model_DbTable_Region extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'region';

    protected $_rowClass = '\\Company_Model_RegionRow';
}