<?php

class Company_Model_DbTable_ContactPersonProposalCustomTemplate extends \OSDN_Db_Table_Abstract
{
    //protected $_primary = 'id';

    protected $_name = 'contactpersonProposalcustomtemplate';

//    protected $_rowClass = '\\Company_Model_ContactPersonProposalCustomTemplateRow';

    protected $_referenceMap = array(
        'ContactPerson'    => array(
            'columns'       => array('contactpersonId'),
            'refTableClass' => 'Company_Model_DbTable_ContactPerson',
            'refColumns'    => array('id'),
        ),
        'ProposalCustomTemplate'    => array(
            'columns'       => array('proposalcustomtemplateId'),
            'refTableClass' => 'Proposal_Model_DbTable_CustomTemplate',
            'refColumns'    => array('id'),
        ),
    );
}