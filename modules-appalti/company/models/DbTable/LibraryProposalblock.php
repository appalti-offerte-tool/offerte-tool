<?php

class Company_Model_DbTable_LibraryProposalblock extends \OSDN_Db_Table_Abstract
{
    protected $_primary = array(
        'libraryId',
        'proposalblockId'
    );

    protected $_name = 'libraryProposalblock';

//    protected $_rowClass = '\\Company_Model_LibraryProposalCustomTemplateRow';

    protected $_referenceMap    = array(
        'Library'    => array(
            'columns'       => array('libraryId'),
            'refTableClass' => 'Company_Model_DbTable_Library',
            'refColumns'    => array('id'),
        ),
        'ProposalBlock'    => array(
            'columns'       => array('proposalblockId'),
            'refTableClass' => 'ProposalBlock_Model_DbTable_ProposalBlock',
            'refColumns'    => array('id'),
        ),
    );
}