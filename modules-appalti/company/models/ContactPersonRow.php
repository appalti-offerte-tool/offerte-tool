<?php

final class Company_Model_ContactPersonRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    const MALE = 'man';
    const FEMALE = 'vrouw';

    const DHR = 'Dhr.';
    const MEVR = 'Mevr.';

    protected $_accountRow;

    protected $_companyRow;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    public function hasUploaded($fieldName)
    {
        return !empty($this->$fieldName);
    }

    public function getFullname()
    {
        return trim(sprintf('%s %s', $this->firstname, $this->lastname));
    }

    public function getFullNameWithInitials()
    {
        $name = $this->initials ? $this->initials . ' (' . $this->firstname . ')' : $this->firstname;
//        $name .= ' ' . $this->prefix;
        $name .= ' ' . $this->lastname;

        return trim($name);
    }

    /**
     * @return Account_Model_DbTable_AccountRow
     */
    public function getAccountRow()
    {
        if (empty($this->accountId)) {
            return null;
        }

        if (null === $this->_accountRow) {
            $this->_accountRow = $this->findParentRow('Account_Model_DbTable_Account', 'Account');
        }

        return $this->_accountRow;
    }

    public function isBeheerder()
    {
        if ($this->getAccountRow())
        {
            return $this->_accountRow->isBeheerder();
        }
        return 0;
    }

    public function isEditable()
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();

        if ($accountRow->isCompanyOwner() || $accountRow->isAdmin()) {
            return true;
        }

        if (empty($this->accountId)) {
            return true;
        }

        if ($this->accountId === $accountRow->getId()) {
            return true;
        }

        return false;
    }

    public function isDeletable()
    {
        $currentAccount = Zend_Auth::getInstance()->getIdentity();
        $cpAccount = $this->getAccountRow();

        $response = array(
            'deletable' => true,
            'msg'       => ''
        );

        if (!$cpAccount) {
            $response['deletable'] = true;
        } elseif ($cpAccount->getId() == $currentAccount->getId()) {
            $response['deletable'] = false;
            $response['msg'] = 'Currently logged in contact person can not be deleted.';
        } elseif ($cpAccount->isCompanyOwner()) {
            $response['deletable'] = false;
            $response['msg'] = 'Company owner can not be deleted.';
        } elseif ($currentAccount->isCompanyOwner() || $currentAccount->isAdmin()) {
            $response['deletable'] = true;
        }

        return $response;
    }

    /**
     * @return Company_Model_CompanyRow
     */
    public function getCompanyRow()
    {
        if (null === $this->_companyRow) {
            $this->_companyRow = $this->findParentRow('Company_Model_DbTable_Company', 'Company');
        }

        return $this->_companyRow;
    }

    public function getRoleName()
    {
        $account = $this->getAccountRow();
        return $account ? $account->getRoleName() : '';
    }

    /**
     * returns the locations that are assigned to this contactperson
     * @param bool $integrity optional if true returns only locations that have theme(s) that have librarie(s)
     * @return Zend_Model_DbTable_Rowset Company_Model_CompanyRow
     */
    public function getLocations($integrity = false)
    {
        if (!$integrity)
        {
            return $this->findManyToManyRowset('Company_Model_DbTable_Location', 'Company_Model_DbTable_ContactPersonLocation');
        }
        $_locations = new Company_Model_DbTable_Location();
        $select = $_locations
                        ->select()
                        ->setIntegrityCheck(false)
                        ->from(array('lo'   =>'company'), array('*'))
                        ->join(array('c'    =>'company'),'c.id = lo.motherId OR c.id = lo.id',array())
                        ->join(array('lot'  =>'locationProposalcustomtemplate'), 'lot.locationId = lo.id', array())
                        ->join(array('t'    =>'proposalCustomTemplate'), 't.id = lot.proposalcustomtemplateId', array())
                        ->join(array('cp'   =>'contactPerson'),'cp.companyId = c.id',array())
                        ->join(array('cplo' =>'contactpersonLocation'), '(cplo.contactpersonId = cp.id AND cplo.locationId = lo.id)', array())
                        ->join(array('cploc'=>'company'),'cploc.id = cplo.locationId',array())
                        ->join(array('cpt'  =>'contactpersonProposalcustomtemplate'),'cpt.contactpersonId = cp.id AND cpt.proposalcustomtemplateId = t.id', array())
                        ->join(array('cppct'=>'proposalCustomTemplate'),'cppct.id = cpt.proposalcustomtemplateId',array())
                        ->join(array('lit'  =>'libraryProposalcustomtemplate'), '(lit.proposalcustomtemplateId = t.id AND lit.proposalcustomtemplateId = cppct.id)', array())
                        ->join(array('li'   =>'library'), 'li.id = lit.libraryId', array())
                        ->where('TRUE')
                        ->where('c.id = ?', $this->companyId)
                        ->where('cp.id = ?', $this->getId())
                        ->where('lo.isActive = 1')
                        ->where('t.active = 1')
                        ->where('li.isActive = 1')
                        ->group(array('lo.id'))
        ;
        return $_locations->fetchAll($select);
    }

    /**
     * returns the locations that are assigned to this contactperson
     * @return Zend_Model_DbTable_Rowset Company_Model_CompanyRow
     */
    public function getLocationThemes(Company_Model_CompanyRow $locationRow)
    {
        $_themes = new Proposal_Model_DbTable_CustomTemplate();
        $select = $_themes
                        ->select()
                        ->setIntegrityCheck(false)
                        ->from(array('t'    =>'proposalCustomTemplate'))
                        ->join(array('lot'  =>'locationProposalcustomtemplate'), 'lot.proposalcustomtemplateId = t.id', array())
                        ->join(array('lo'   =>'company'),'lo.id = lot.locationId', array())
                        ->join(array('c'    =>'company'),'c.id = lo.motherId OR c.id = lo.id',array())
                        ->join(array('cp'   =>'contactPerson'),'cp.companyId = c.id',array())
                        ->join(array('cplo' =>'contactpersonLocation'), '(cplo.contactpersonId = cp.id AND cplo.locationId = lo.id)', array())
                        ->join(array('cploc'=>'company'),'cploc.id = cplo.locationId',array())
                        ->join(array('cpt'  =>'contactpersonProposalcustomtemplate'),'cpt.contactpersonId = cp.id AND cpt.proposalcustomtemplateId = t.id', array())
                        ->join(array('cppct'=>'proposalCustomTemplate'),'cppct.id = cpt.proposalcustomtemplateId',array())
                        ->join(array('lit'  =>'libraryProposalcustomtemplate'), '(lit.proposalcustomtemplateId = t.id AND lit.proposalcustomtemplateId = cppct.id)', array())
                        ->join(array('li'   =>'library'), 'li.id = lit.libraryId', array())
                        ->columns(array('locationIds' => 'GROUP_CONCAT(DISTINCT(lo.id))'))
                        ->where('TRUE')
                        ->where('c.id = ?', $this->companyId)
                        ->where('cp.id = ?', $this->getId())
                        ->where('lo.id = ?', $locationRow->getId())
                        ->where('lo.isActive = 1')
                        ->where('t.active = 1')
                        ->where('li.isActive = 1')
                        ->group(array('t.id'))
        ;
        $themeRowset = $_themes->fetchAll($select);
        return $themeRowset;
    }

    /**
     * returns the themes that are assigned to this contactperson
     * @return Zend_Model_DbTable_Rowset Proposal_Model_CustomTemplate
     */
    public function getThemes($integrity = false)
    {
        if (!$integrity)
        {
            return $this->findManyToManyRowset('Proposal_Model_DbTable_CustomTemplate', 'Company_Model_DbTable_ContactPersonProposalCustomTemplate');
        }
        $_themes = new Proposal_Model_DbTable_CustomTemplate();
        $select = $_themes
                        ->select()
                        ->setIntegrityCheck(false)
                        ->from(array('t'    =>'proposalCustomTemplate'))
                        ->join(array('lot'  =>'locationProposalcustomtemplate'), 'lot.proposalcustomtemplateId = t.id', array())
                        ->join(array('lo'   =>'company'),'lo.id = lot.locationId', array())
                        ->join(array('c'    =>'company'),'c.id = lo.motherId OR c.id = lo.id',array())
                        ->join(array('cp'   =>'contactPerson'),'cp.companyId = c.id',array())
                        ->join(array('cplo' =>'contactpersonLocation'), '(cplo.contactpersonId = cp.id AND cplo.locationId = lo.id)', array())
                        ->join(array('cploc'=>'company'),'cploc.id = cplo.locationId',array())
                        ->join(array('cpt'  =>'contactpersonProposalcustomtemplate'),'cpt.contactpersonId = cp.id AND cpt.proposalcustomtemplateId = t.id', array())
                        ->join(array('cppct'=>'proposalCustomTemplate'),'cppct.id = cpt.proposalcustomtemplateId',array())
                        ->join(array('lit'  =>'libraryProposalcustomtemplate'), '(lit.proposalcustomtemplateId = t.id AND lit.proposalcustomtemplateId = cppct.id)', array())
                        ->join(array('li'   =>'library'), 'li.id = lit.libraryId', array())
                        ->columns(array('locationIds' => 'GROUP_CONCAT(DISTINCT(lo.id))'))
                        ->where('TRUE')
                        ->where('c.id = ?', $this->companyId)
                        ->where('cp.id = ?', $this->getId())
                        ->where('lo.isActive = 1')
                        ->where('t.active = 1')
                        ->where('li.isActive = 1')
                        ->group(array('t.id'))
        ;
        return $_themes->fetchAll($select);
    }

    public function canApproveProposal()
    {
        $accountRow = $this->getAccountRow();
        if ($accountRow->admin)
        {
            return true;
        }
        $_permissions = new Account_Model_DbTable_AclPermission();
        $select = $_permissions->select()
                                ->where('roleId = ?', $accountRow->roleId)
                                ->where("resource = 'proposal:approve'" )
        ;
        if ($_permissions->fetchRow($select))
        {
            return true;
        }
        return false;
    }
}