<?php

class Company_Model_OfferRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    public function getKindNameNL()
    {
        return $this->kind === 'service' ? 'dienst' : 'product';
    }
}