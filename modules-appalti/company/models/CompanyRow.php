<?php

class Company_Model_CompanyRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    const PROSPECT = 'prospect';
    const SUSPECT = 'suspect';
    const ACCOUNT = 'account';
    const FLAG_IS_ACTIVE = 1;

    /**
     * @var Account_Model_DbTable_AccountRow
     */
    protected $_accountRow;

    protected $_logoDir = '/modules-appalti/company/data/images-company-logo/';

    /**
     * @var Membership_Service_MembershipCompany
     */
    protected $_membershipService;

    /**
     * @var Membership_Model_MembershipCompany
     */
    protected $_membershipRow;

    /* @var */
    protected $_acl;

    /**
     * @return Membership_Model_MembershipCompany
     */
    protected function _toMembershipRow()
    {
        if (!empty($this->_membershipRow)) {
            return $this->_membershipRow;
        }

        if (empty($this->_membershipService)) {
            $this->_membershipService = new \Membership_Service_MembershipCompany($this);
            $this->_membershipRow = $this->_membershipService->fetch();
        }

        return $this->_membershipRow;
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    public function getAcl()
    {
        if (!$this->_acl)
        {
            try {
                // find role
                $roleSrv = new Account_Service_AclRole();
                $role = $roleSrv->findCompanyRoleById($this->getId());

                // create acl
                $acl = new Zend_Acl();
                $acl->addRole($role);
                $acl->deny($role);
            } catch(Exception $e) {
                if (!($role instanceof Zend_Acl_Role_Interface))
                {
                    throw new Exception('Dit bedrijf heeft nog geen rol toegekend gekregen.', $e->getCode(), $e);
                }
                else
                {
                    throw new Exception('Onbekende fout.', $e->getCode(), $e);
                }
            }

            // add permissions
            $tablePermission = new Account_Model_DbTable_AclPermission($this->getTable()->getAdapter());
            foreach($acl->getRoles() as $roleId)
            {
                $permissionRowset = $tablePermission->fetchAll(array(
                    'roleId = ?'    => $roleId,
                ), array('resource ASC', 'privilege ASC'));

                foreach($permissionRowset as $permissionRow) {
                    $resource = $permissionRow->resource;
                    if (!$acl->has($resource)) {
                        if (false === stripos($resource, ':')) {
                            $parentResource = new Zend_Acl_Resource($resource);
                            $acl->addResource($parentResource);
                            $acl->deny($role, $parentResource);
                            $acl->allow($role, $parentResource, 'default');
                            continue;
                        }

                        list($parentResource, ) = explode(':', $resource);
                        if (!$acl->has($parentResource)) {
                            $acl->addResource($parentResource);
                        }

                        $acl->addResource($resource);
                        $acl->deny($role, $resource);
                        $acl->allow($role, $resource, 'default');
                    }

                    if (null !== $permissionRow->privilege) {
                        $acl->allow($role, $resource, $permissionRow->privilege);
                    }
                }
            }
            $this->_acl = $acl;
        }
        return $this->_acl;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getFullName()
    {
        return $this->name . ' ' . $this->form;
    }

    public function getPostAddress()
    {
        return sprintf('%s %d %s %s', $this->postStreet,  $this->postHouseNumber, $this->postPostCode, $this->postCity);
    }

    /**
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getChildrenRowset()
    {
        $select = $this->select()->where('isActive = ?', self::FLAG_IS_ACTIVE, Zend_Db::INT_TYPE);

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if ($accountRow->isProposalBuilder()) {
            $select->where('createdAccountId = ?', $accountRow->getId(), Zend_Db::INT_TYPE);
        }

        return $this->findDependentRowset('Company_Model_DbTable_Company', 'Children', $select);
    }

    /**
     * @return Account_Model_DbTable_AccountRow
     */
    public function getAccountRow()
    {
        if (empty($this->accountId)) {
            return null;
        }

        if (null === $this->_accountRow) {
            $this->_accountRow = $this->findParentRow('Account_Model_DbTable_Account', 'Account');
        }

        return $this->_accountRow;
    }

    public function getAccountOwner()
    {
        $this->_accountRow = $this->findParentRow('Account_Model_DbTable_Account', 'Account');
        return $this->_accountRow;
    }

    public function hasUploadedLogo()
    {
        return !empty($this->logo);
    }

    public function isRoot()
    {
        return empty($this->parentId);
    }

    public function getMembership()
    {
        $membershipRow = $this->_toMembershipRow();
        return $membershipRow;
    }

    public function isMembershipValid()
    {
        return true;

        $membershipRow = $this->_toMembershipRow();
        return !empty($membershipRow) && $membershipRow->isValid();
    }

    public function getMembershipStart()
    {
        $start = null;
        if ($this->isMembershipValid()) {
            $membershipRow = $this->_toMembershipRow();
            $start = $membershipRow->startDatetime;
        }

        return $start;
    }

    public function getMembershipEnd()
    {
        $end = null;
        if ($this->isMembershipValid()) {
            $membershipRow = $this->_toMembershipRow();
            $end = $membershipRow->endDatetime;
        }

        return $end;
    }


    public function isEditable()
    {
        $accountRow = Zend_Auth::getInstance()->getInstance()->getIdentity();
        return $accountRow->acl('company');

        if ($accountRow->isAdmin()) {
            return true;
        }

        if ($this->isRoot()) {
            return $accountRow->isAdmin() || $accountRow->isCompanyOwner();
        }

        if ($accountRow->isCompanyOwner() || $accountRow->isManager()) {
            return true;
        }

        if ($accountRow->isProposalBuilder() && $this->createdAccountId === $accountRow->getId()) {
            return true;
        }

        return false;
    }

    /**
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getCompanyBankAccountRowset()
    {

        return $this->findDependentRowset('Company_Model_DbTable_BankAccount', 'BankAccount');
    }

    /**
     * @return Zend_Db_Table_Row_Abstract
     */
    public function getCompanyDefaultBankAccountRow()
    {
        $select = $this->select()->where('isDefault IS NOT NULL')->limit(1);
        $rowSet = $this->findDependentRowset('Company_Model_DbTable_BankAccount', 'BankAccount', $select);

        if (!$rowSet->count()) {
            $rowSet = $this->getCompanyBankAccountRowset();
        }

        return $rowSet->current();
    }

    /**
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getCompanyContactPersonRowset()
    {
        $select = $this->select()->where('isActive = 1');
        return $this->findDependentRowset('Company_Model_DbTable_ContactPerson', 'Company', $select);
    }

    /**
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getCompanyContactPersonRow($id = null)
    {
        if ($id) {
            $select = $this->select()
                    ->where('isActive = 1')
                    ->where('id = ?', $id)->limit(1);
            $row = $this->findDependentRowset('Company_Model_DbTable_ContactPerson', 'Company', $select)->current();
        } else {
            $row = $this->getCompanyDefaultContactPersonRow();
        }

        return $row;
    }

    /**
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getCompanyContactPersonRowByAccountId($accountId)
    {
        $select = $this->select()
                ->where('isActive = 1')
                ->where('accountId = ?', $accountId)->limit(1);
        $row = $this->findDependentRowset('Company_Model_DbTable_ContactPerson', 'Company', $select)->current();

        return $row;
    }

    /**
     * @return Zend_Db_Table_Row_Abstract
     */
    public function getCompanyDefaultContactPersonRow()
    {
        $select =$this->select()
                ->where('isActive = 1')
                ->where('isDefault IS NOT NULL')->limit(1);
        return $this->findDependentRowset('Company_Model_DbTable_ContactPerson', 'Company', $select)->current();
    }

    /**
     * @return Zend_Db_Table_Row_Abstract
     */
    public function getContactPersonCanApproveRowset()
    {
        $select =$this->select()
                ->where('isActive = 1')
                ->where('canApproveProposal =?', 1);
        return $this->findDependentRowset('Company_Model_DbTable_ContactPerson', 'ContactPerson', $select);
    }

    public function getProposalThemeRow($proposalId = null)
    {
        if ($proposalId) {
            $select = $this->select()->where('proposalId = ?', $proposalId)->limit(1);
        } else {
            $select = $this->select()->where('proposalId IS NULL')->limit(1);
        }

        return $this->findDependentRowset('Proposal_Model_DbTable_ProposalTheme', 'Theme', $select)->current();
    }

    public function getBase64EncodedLogo()
    {
        $data = '';
        $fileName = APPLICATION_PATH . $this->_logoDir . $this->logo;

        if (file_exists($fileName)) {
            $imgType = array('jpeg', 'jpg', 'gif', 'png');
            $fileName = htmlentities($fileName);
            $fileType = pathinfo($fileName, PATHINFO_EXTENSION);

            if (in_array($fileType, $imgType)){
                $imgBinary = fread(fopen($fileName, "r"), filesize($fileName));
                $base64 = chunk_split(base64_encode($imgBinary));
                $fileType = 'jpg' === $fileType ? 'jpeg' : $fileType;
                $data = sprintf('data:image/%s;base64,%s', $fileType, $base64);
            }
        }

        return $data;
    }

    public function getLogoFilePath()
    {
        $filePath = null;

        if ($this->hasUploadedLogo()) {
            $filePath = APPLICATION_PATH . $this->_logoDir . $this->logo;
            if (!file_exists($filePath)) {
                $filePath = null;
            }
        }

        return $filePath;
    }

    public function getCompanyRole()
    {
        $_roles = new Account_Service_AclRole();
        return $_roles->findCompanyRoleById($this->getId());
    }

    public function getLocations()
    {
        $_locations = new Company_Service_Location();
        $_locations->_companyRow = $this;
        return $_locations->fetchAllActiveLocations();
    }

    public function getThemes()
    {
        $_themes = new Company_Service_Theme();
        return $_themes->fetchAllActive();
    }

    public function getLibrary($libraryId)
    {
        $_libraries = new Company_Service_Library();
        $libraries = $_libraries->fetchAll('id = '.(int)$libraryId);
        if (count($libraries) == 1)
        {
            return $libraries->current();
        }
    }

    public function getLibraries()
    {
        $_libraries = new Company_Service_Library();
        return $_libraries->fetchAll;
    }

    public function getActiveLibraries()
    {
        $_libraries = new Company_Service_Library();
        return $_libraries->fetchAllActive();
    }

    public function getRegions()
    {
        $_regions = new Company_Service_Region();
        return $_regions->fetchAllByCompanyId($this->getId());
    }

    public function getAccounts()
    {
        return $this->findManyToManyRowset('Account_Model_DbTable_Account', 'Company_Model_DbTable_ContactPerson');
    }

    public function getAclRole()
    {
        $_aclroles = new Account_Service_AclRole();
        return $_aclroles->findCompanyRoleById($this->getId());
    }
}