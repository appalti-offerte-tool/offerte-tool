<?php

class Company_Summary_Company extends \Application_Instance_Summary
{
    public function __construct()
    {
        parent::__construct();

        $this->_setHref('company', 'main');
    }

    /**
     * (non-PHPdoc)
     * @see Application_Instance_Summary::_bind()
     */
    protected function _bind()
    {
        $this->_title = $this->getTranslator()->translate('Company');

        $company = new Company_Service_Company();
        $this->_value = $company->getCompanyCount();
    }
}