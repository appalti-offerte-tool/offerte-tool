DROP TRIGGER IF EXISTS incProposalCount;
DELIMITER $$
CREATE TRIGGER incProposalCount
AFTER INSERT ON proposal
FOR EACH ROW BEGIN
	UPDATE company SET proposalCount= proposalCount + 1 WHERE id = NEW.clientCompanyId;
	UPDATE company SET proposalCount= proposalCount + 1 WHERE id = NEW.companyId;
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS decProposalCount;
DELIMITER $$
CREATE TRIGGER decProposalCount
AFTER DELETE ON proposal
FOR EACH ROW
BEGIN
	UPDATE company SET proposalCount = IF(proposalCount > 0, proposalCount - 1, 0) WHERE id = OLD.clientCompanyId;
	UPDATE company SET proposalCount = IF(proposalCount > 0, proposalCount - 1, 0) WHERE id = OLD.companyId;
END$$
DELIMITER ;
