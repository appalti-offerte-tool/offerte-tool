<?php

class Company_Migration_20120327_184458_54 extends Core_Migration_Abstract
{

    public function up()
    {
        $this->createTable('company');
        $this->createColumn('company', 'parentId', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createColumn('company', 'name', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('company', 'fullname', Core_Migration_Abstract::TYPE_VARCHAR, 20);
        $this->createColumn('company', 'email', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('company', 'logo', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('company', 'kvk', Core_Migration_Abstract::TYPE_VARCHAR, 20);
        $this->createColumn('company', 'btw', Core_Migration_Abstract::TYPE_VARCHAR, 20);
        $this->createColumn('company', 'actualStreet', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('company', 'actualHouseNumber', Core_Migration_Abstract::TYPE_VARCHAR, 5);
        $this->createColumn('company', 'actualPostCode', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('company', 'actualCity', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('company', 'postStreet', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('company', 'postHouseNumber', Core_Migration_Abstract::TYPE_VARCHAR, 5);
        $this->createColumn('company', 'postPostCode', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('company', 'postCity', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('company', 'phone', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('company', 'fax', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('company', 'type', Core_Migration_Abstract::TYPE_VARCHAR, 2);
        $this->createIndex('company', 'parentId');
        $this->createForeignKey('company', array('parentId'), 'company', array('id'), 'FK_companyId');

        $this->createTable('companyBankAccount');
        $this->createColumn('companyBankAccount', 'companyId', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createColumn('companyBankAccount', 'bankAccount', Core_Migration_Abstract::TYPE_INT, 11);
        $this->createIndex('companyBankAccount', 'companyId');
        $this->createForeignKey('companyBankAccount', array('companyId'), 'company', array('id'), 'FK_companyId');

        $this->createTable('companyOffer');
        $this->createColumn('companyOffer', 'companyId', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createColumn('companyOffer', 'name', Core_Migration_Abstract::TYPE_VARCHAR);
        $this->createColumn('companyOffer', 'kind', Core_Migration_Abstract::TYPE_VARCHAR);
        $this->createColumn('companyOffer', 'description', Core_Migration_Abstract::TYPE_TEXT);
        $this->createColumn('companyOffer', 'pricePerUnit', Core_Migration_Abstract::TYPE_FLOAT, 9);
        $this->createColumn('companyOffer', 'inStock', Core_Migration_Abstract::TYPE_INT, 2);
        $this->createColumn('companyOffer', 'amount', Core_Migration_Abstract::TYPE_INT, 11);
        $this->createColumn('companyOffer', 'discount', Core_Migration_Abstract::TYPE_FLOAT, 9);
        $this->createIndex('companyOffer', 'companyId');
        $this->createForeignKey('companyOffer', array('companyId'), 'company', array('id'), 'FK_companyId');

        $this->createTable('contactPerson');
        $this->createColumn('contactPerson', 'companyId', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createColumn('contactPerson', 'firstname', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('contactPerson', 'prefix', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('contactPerson', 'lastname', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('contactPerson', 'sex', Core_Migration_Abstract::TYPE_VARCHAR);
        $this->createColumn('contactPerson', 'title', Core_Migration_Abstract::TYPE_VARCHAR);
        $this->createColumn('contactPerson', 'function', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('contactPerson', 'department', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('contactPerson', 'phone', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('contactPerson', 'mobile', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('contactPerson', 'email', Core_Migration_Abstract::TYPE_VARCHAR, 11);
        $this->createColumn('contactPerson', 'photo', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('contactPerson', 'canApproveProposal', Core_Migration_Abstract::TYPE_INT, 2);
        $this->createColumn('contactPerson', 'relationship', Core_Migration_Abstract::TYPE_VARCHAR);
        $this->createColumn('contactPerson', 'signatureImg', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createIndex('contactPerson', 'companyId');
        $this->createForeignKey('contactPerson', array('companyId'), 'company', array('id'), 'FK_companyId');

        //  ALTER TABLE `company` ADD `accountId` INT( 11 ) NOT NULL AFTER `parentId`
        //  ALTER TABLE `company` CHANGE `email` `email` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
        //  ALTER TABLE `company` ADD `website` VARCHAR( 255 ) NOT NULL AFTER `logo`
        //  ALTER TABLE `companyBankAccount` CHANGE `bankAccount` `bankAccount` VARCHAR( 50 ) NOT NULL
        //  ALTER TABLE `contactPerson` CHANGE `photo` `photo` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL


    }

    public function down()
    {
        $this->dropTable('company');
        $this->dropTable('companyBankAccount');
        $this->dropTable('companyOffer');
        $this->dropTable('contactPerson');
    }


}

