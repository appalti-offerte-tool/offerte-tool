<?php

class Company_Service_Theme extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Company_Model_DbTable_Region
     */
    protected $_table;

    protected $_accountRow;

    protected $_companyRow;

    protected function _init()
    {
        $this->_table = new \Company_Model_DbTable_LocationProposalCustomTemplate($this->getAdapter());

        $this->_accountRow = Zend_Auth::getInstance()->getIdentity();
        $this->_companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();

        $this->_attachValidationRules('default', array(
            'companyId' => array(
                'allowEmpty' => false,
                'presence' => 'required',
            ),
            'proposalcustomtemplateId' => array(
                'allowEmpty' => false,
                'presence' => 'required',
            ),
        ));

        $this->_attachValidationRules('update', array(
//            'companyId' => array(
//                'allowEmpty' => false,
//                'presence' => null
//            ),
//            'kind' => array(
//                'allowEmpty' => false,
//                'presence' => 'required'
//            ),
//            'name' => array(
//                'allowEmpty' => false,
//                'presence' => 'required'
//            ),
//            'pricePerUnit' => array(
//                'allowEmpty' => false,
//                'presence' => 'required'
//            ),
        ), 'default');

        parent::_init();
    }

    /**
     * @param array $data
     */
    protected function _create(array $data)
    {
        $f = $this->_validate($data);
        $row = $this->_table->createRow($f->getData());
        $row->save();

        return $row;
    }

    /**
     * @param Company_Model_CompanyRow $companyRow
     * @return Zend_Db_Table_Rowset
     */
    public function fetchAllByLocation(Company_Model_CompanyRow $companyRow)
    {
        if (!$companyRow->getId()) {
            throw new OSDN_Exception('Company id is not defined.');
        }
        return $companyRow->findManyToManyRowset('Proposal_Model_DbTable_CustomTemplate', 'Company_Model_DbTable_LocationProposalCustomTemplate');
    }

    public function fetchAllActive()
    {
        $_table = new Proposal_Service_CustomTemplate();
        return $_table->fetchAllActive();
    }


    /**
     * @param Company_Model_CompanyRow $companyRow
     * @param array $themes contains themeids
     * return void
     */
    public function setLocationThemes(Company_Model_CompanyRow $companyRow, array $themes)
    {
        $adapter = $this->getAdapter();
        $count_rows_deleted = $adapter->delete('locationProposalcustomtemplate', 'locationId = '.(int)$companyRow->getId());
        foreach($themes as $themeId)
        {
            if ((int)$themeId)
            {
                $data = array(
                    'locationId' => $companyRow->getId(),
                    'proposalcustomtemplateId'  => $themeId
                );
                $adapter->insert('locationProposalcustomtemplate', $data);
            }
        }
    }

}