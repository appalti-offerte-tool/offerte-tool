<?php

class Company_Service_ClientMain extends Company_Service_Company
{
    /**
     * @var \Company_Model_DbTable_Company
     */
    protected $_table;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new Company_Model_DbTable_Company($this->getAdapter());

        parent::_init();
    }


    public function fetchAllByParentWithResponse(\Company_Model_CompanyRow $companyRow, array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(array(
                'c'    => 'company',
            ))
            ->where('c.parentId = ?', $companyRow->getId(), Zend_Db::INT_TYPE);


        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if ($accountRow->isProposalBuilder()) {
            $select->where('createdAccountId = ?', $accountRow->getId(), Zend_Db::INT_TYPE);
        }

        if (!$accountRow->isAdmin()) {
            $select->where('parentId = ?', $accountRow->getCompanyRow()->getId(), Zend_Db::INT_TYPE);
        }

        $this->_initDbFilter($select, $this->_table, array('name'))->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        $table = $this->_table;
        return $response;
    }

    /**
     *
     * Role "Proposal builder"
     * Can select own root company or client which he created
     *
     *
     * @param $id
     * @param bool $throwException
     * @throws OSDN_Exception
     * @return \Company_Model_CompanyRow
     */
    public function find($id, $throwException = true)
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();

        $companyRow = $this->_table->findOne($id);
        if (null !== $companyRow) {
            self::assertAccountOwnershipToCompany($companyRow, $accountRow);    // $companyRow->getAccountRow()
        }

        if (null === $companyRow && true === $throwException) {
            throw new \OSDN_Exception('Unable to find company: ' . (int) $id);
        }

        if (
            ! $companyRow->isRoot() &&
            $accountRow->isProposalBuilder() &&
            $companyRow->createdAccountId != $accountRow->getId()
        ) {
            throw new OSDN_Exception('You do not have permission');
        }

        return $companyRow;
    }

}