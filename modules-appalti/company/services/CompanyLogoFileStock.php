<?php

/**
 * This is simply emulation of filestock
 * In future can be easily adjusted to full filestock support
 */
class Company_Service_CompanyLogoFileStock extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Company_Model_DbTable_Company
     */
    protected $_table;

    /**
     * @var Company_Model_CompanyRow
     */
    protected $_companyRow;

    protected $_imageFileStockPath;

    public function __construct(Company_Model_CompanyRow $companyRow)
    {
        $this->_companyRow = $companyRow;

        parent::__construct();

        $this->_table = new \Company_Model_DbTable_Company($this->getAdapter());

        $this->_imageFileStockPath = __DIR__ . '/../data/images-company-logo/';
    }

    public function toImageFileStockPath()
    {
        return $this->_imageFileStockPath;
    }

    /**
     * @FIXME        Change to automatic path detection
     */
    public function toImageFileStockRelativePath()
    {
        return '/modules-appalti/company/data/images-company-logo/';
    }

    public function create(array $data, \Zend_File_Transfer_Adapter_Abstract $transfer = null)
    {
        if (null === $transfer) {
            $transfer = new \Zend_File_Transfer_Adapter_Http();
        }

        if (!$transfer->isUploaded('logo')) {
            return;
        }

        if ($this->_companyRow->hasUploadedLogo()) {
            return $this->update($data, $transfer);
        }

        return $this->_onImageProcessing($transfer);
    }

    protected function _onImageProcessing(\Zend_File_Transfer_Adapter_Abstract $transfer)
    {
        $this->_doAttachTransferValidators($transfer);
        $transfer->setDestination($this->_imageFileStockPath);

        if (!$transfer->isValid('logo')) {
            $e = new OSDN_Exception('Company logo is invalid');
            $e->assign($transfer->getMessages());
            throw $e;
        }

        $fi = $transfer->getFileInfo('logo');
        $extension = pathinfo($fi['logo']['name'], PATHINFO_EXTENSION);
        $filename = sprintf('%s.%s', $this->_companyRow->getId(), $extension);

        $transfer->addFilter('Rename', array(
            'target'    => $this->_imageFileStockPath . $filename,
            'overwrite' => true
        ));

        $transfer->receive('logo');

        /**
         * @FIXME        Change to automatic path detection
         */

        $this->_companyRow->logo = $filename;
        $this->_companyRow->save();

        return true;
    }

    /**
     * @return boolean
     */
    public function update(array $data, \Zend_File_Transfer_Adapter_Abstract $transfer = null)
    {
        if (null === $transfer) {
            $transfer = new \Zend_File_Transfer_Adapter_Http();
        }

        if (!$transfer->isUploaded('logo')) {
            return;
        }

        return $this->_onImageProcessing($transfer);
    }

    public function delete(\CwAccount_Model_DbTable_AccountRow $accountRow, $fileStockId)
    {
        $tableAccountFileStockRel = $this->_tableAccountFileStockRelation;
        $result = $this->_fileStock->delete($fileStockId, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow) use (
            $accountRow, $tableAccountFileStockRel
        ) {
            $tableAccountFileStockRel->delete(array(
                'companyId = ?'    => $accountRow->getId(),
                'fileStockId = ?'  => $fileStockRow->getId()
            ));
        });

        return (boolean) $result;
    }

    public function deleteByAccount(CwAccount_Model_DbTable_AccountRow $accountRow)
    {
        $accountFileStockRelation = new \CwAccount_Model_DbTable_AccountFileStockRel();
        $rowset = $accountFileStockRelation->fetchAll(array(
            'companyId = ?'    => $accountRow->getId()
        ));

        foreach($rowset as $row) {
            $this->delete($accountRow, $row->fileStockId);
        }

        return true;
    }

    protected function _doAttachTransferValidators(\Zend_File_Transfer_Adapter_Abstract $transfer)
    {
        $transfer->addValidator('IsImage', true);

        return $transfer;
    }

    public function getFileStockStorage()
    {
        return $this->_fileStock->factory();
    }

    public function view($companyId, $fileStockId, \Zend_Controller_Response_Abstract $response)
    {
        $accountFileStockRow = $this->find($companyId, $fileStockId, true);

        $this->_fileStock->download($accountFileStockRow->getFileStockRow(), $response, true, function(
            FileStock_Model_DbTable_FileStockRow $fileStockRow,
            Zend_Controller_Response_Http $response
        ) {
            /**
             * @FIXME
             *
             * Dummy replacement of header disposition
             */
            $response->setHeader('Content-disposition', sprintf('inline; filename="%s"', $fileStockRow->getName()), true);
        });
    }

    public function download($companyId, $fileStockId, \Zend_Controller_Response_Abstract $response)
    {
        $accountFileStockRow = $this->find($companyId, $fileStockId, true);
        $this->_fileStock->download($accountFileStockRow->getFileStockRow(), $response, true);
    }
}