<?php

class Company_Service_ContactPerson extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Company_Model_DbTable_ContactPerson
     */
    protected $_table;

    protected $_companyRow;

    protected $_companyDependentRoleIds = array(5, 3);

    public function __construct(\Company_Model_CompanyRow $companyRow = null)
    {
        if ($companyRow)
        {
            $this->_companyRow = $companyRow;
        }
        else
        {
            $this->companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
        }

        $this->_table = new \Company_Model_DbTable_ContactPerson($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'companyId' => array('allowEmpty' => false, 'presence' => 'required'),
            'firstname' => array('allowEmpty' => true, 'presence' => 'required'),
//            'prefix' => array('allowEmpty' => false, 'presence' => 'required'),
            'lastname' => array('allowEmpty' => false, 'presence' => 'required'),
//            'phone' => array('allowEmpty' => false, 'presence' => 'required'),
            'email' => array('allowEmpty' => false, 'presence' => 'required')
        ));

        $this->_attachValidationRules('update', array(
            'lastname' => array(
                'allowEmpty' => false, 'presence' => 'required',
                'messages'    => array('hello')
            ),
        ), 'default');

        parent::__construct();
    }

    public function fetchAllWithResponse(array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('cp' => 'contactPerson')
            )
            ->where('cp.isActive = ?', 1)
            ->where('cp.companyId = ?', $this->_companyRow->getId(), Zend_Db::INT_TYPE);

        $this->_initDbFilter($select, $this->_table)->parse($params);

        $response = $this->getDecorator('response')->decorate($select);
        $table = $this->_table;
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });

        return $response;
    }

    public function fetchAll(array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(array(
                'cp' => 'contactPerson',
            ))
            ->joinLeft (
                array (
                    'ac'=>'account'
                ),
                'cp.accountId = ac.id',
                array(
                    'username'=>'ac.username',
                    'roleId'=>'ac.roleId',
                )
            )
            ->where('cp.isActive = ?', 1)
            ->where('cp.companyId = ?', $this->_companyRow->getId(), Zend_Db::INT_TYPE);

        $this->_initDbFilter($select, $this->_table)->parse($params);

        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

    /**
     * @param $id
     * @param bool $throwException
     * @throws OSDN_Exception
     * @return Company_Model_ContactPersonRow
     */
    public function find($id, $throwException = true)
    {
        $contactPersonRow = $this->_table->findOne($id);
        if (null === $contactPersonRow && true === $throwException) {
            throw new \OSDN_Exception('Unable to find row #' . $id);
        }

        return $contactPersonRow;
    }

    /**
     * @param array $data
     */
    public function create(array $data)
    {
        $this->getAdapter()->beginTransaction();
        try {
            $f = $this->_validate($data);
            $params = $f->getData();

            if (!empty($params['accountId'])) {
                $accountSrv = new \Account_Service_Account();
                $accountRow = $accountSrv->find($params['accountId']);
                $params['username'] = $accountRow->getUsername();
                $params['username_old'] = $params['username'];
            }

            $params['accountId'] = $this->_proceedAccount($params);
            if (!$params['accountId']) {
                unset($params['accountId']);
            }
            if ($params['sex'] == 'vrouw')
            {
                $params['title'] = 'Mevr.';
            }
            $contactPersonRow = $this->_table->createRow($params);

            $contactPersonRow->save();

            $this->_onPhotoFileStockProcessing($contactPersonRow);
            $this->_onPhotoFileStockProcessing($contactPersonRow, 'signature');

            $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to save contact person', null, $e);
        }

        return $contactPersonRow;
    }

    /**
     * @param $id
     * @param array $data
     * @return bool
     * @throws OSDN_Exception
     */
    public function update(Company_Model_ContactPersonRow $contactPersonRow, array $data)
    {

        if (isset($data['photo']) && empty($data['photo'])) {
            unset ($data['photo']);
        }
        if (isset($data['signature']) && empty($data['signature'])) {
            unset ($data['signature']);
        }

        $f = $this->_validate($data, 'update');

        $this->getAdapter()->beginTransaction();
        try {

            $params = $f->getData();

            $accountRow = $contactPersonRow->getAccountRow();
            if (!empty($accountRow) && ($accountRow instanceof \Account_Model_DbTable_AccountRow)) {
                if (empty($params['username'])) {
                    $params['username'] = $accountRow->username;
                }
                if (empty($params['username_old'])) {
                    $params['username_old'] = $accountRow->username;
                }
            }

            $params['accountId'] = $this->_proceedAccount($params);
            if (!$params['accountId']) {
                unset($params['accountId']);
            }
            if ($params['sex'] == 'vrouw')
            {
                $params['title'] = 'Mevr.';
            }

            $contactPersonRow->setFromArray($params);
            $contactPersonRow->save();

            $this->_onPhotoFileStockProcessing($contactPersonRow);
            $this->_onPhotoFileStockProcessing($contactPersonRow, 'signature');

            $this->getAdapter()->commit();
        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update');
        }

        return $contactPersonRow;
    }

    /**
     * @param $id
     * @param array $data
     * @return bool
     * @throws OSDN_Exception
     */
    public function updateAccountContactPerson(Company_Model_ContactPersonRow $contactPersonRow = null, array $data = array(), $throwException = true)
    {

        if ( ! $contactPersonRow instanceof \Company_Model_ContactPersonRow && true == $throwException) {
            throw new OSDN_Exception('Unable to update. Contact person not found.');
        }

        if (isset($data['photo']) && empty($data['photo'])) {
            unset ($data['photo']);
        }
        if (isset($data['signature']) && empty($data['signature'])) {
            unset ($data['signature']);
        }

        $this->getAdapter()->beginTransaction();
        try {

            if ($contactPersonRow instanceof \Company_Model_ContactPersonRow) {
                $accountRow = $contactPersonRow->getAccountRow();
                if (!empty($accountRow) && ($accountRow instanceof \Account_Model_DbTable_AccountRow)) {
                    if (empty($data['username'])) {
                        $data['username'] = $accountRow->username;
                    }
                    if (empty($data['username_old'])) {
                        $data['username_old'] = $accountRow->username;
                    }
                }
            }

            $data['accountId'] = $this->_proceedAccount($data);
            if (!$data['accountId']) {
                unset($data['accountId']);
            }

            if ($contactPersonRow instanceof \Company_Model_ContactPersonRow) {

                $f = $this->_validate($data, 'update');
                $params = $f->getData();

                $contactPersonRow->setFromArray($params);
                $contactPersonRow->save();

                $this->_onPhotoFileStockProcessing($contactPersonRow);
                $this->_onPhotoFileStockProcessing($contactPersonRow, 'signature');
            }

            $this->getAdapter()->commit();
        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update');
        }

        return $contactPersonRow;
    }

    /**
     * Delete contact person  with:
     *  + PhotoFileStock
     *  + SignatureFileStock
     *  + Contact person account
     *
     * @param $id
     *
     * @return boolean | true on success
     * @throws OSDN_Exception
     */
    public function delete($id)
    {
        $this->getAdapter()->beginTransaction();
        try {
            $contactPersonRow = $this->find($id);

            $contactPersonRow->setFromArray(array(
                'isActive' => 0
            ));
            $contactPersonRow->save();

//            $contactPersonPhotoFileStock = new Company_Service_ContactPersonPhotoFileStock($contactPersonClone);
//            $contactPersonPhotoFileStock->delete();

            if (null !== ($accountRow = $contactPersonRow->getAccountRow())) {

                $authHash = $accountRow->authhash;

                if (!empty($authHash)) {
                    throw new \OSDN_Exception('Unable to delete. Contact person is logged in.');
                }

                $accountRow->setActive(false);
                $accountRow->save();
            }

            $this->getAdapter()->commit();
        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to delete', null, $e);
        }

        return true;
    }

    public function bulk(\Company_Model_CompanyRow $companyRow, array $data)
    {
        $default = (int) (count($data) == 1);
        foreach ($data as $id => $prm) {

            $prm['companyId'] = $companyRow->getId();
            $prm['isDefault'] = $default;

            $f = $this->_validate($prm);
            $params = $f->getData();

            try {

                if (is_numeric($id) && intval($id) == $id) {
                    $contactPersonRow = $this->find($id);
                    $this->update($contactPersonRow, $params);
                } else {
                    $f = $this->_validate($params);
                    $contactPersonRow = $this->create($f->getData());
                }


                $name = sprintf('contactPerson_%s_photo', $id);
                $this->_onPhotoFileStockProcessing($contactPersonRow, $name);

                $name = sprintf('contactPerson_%s_signature', $id);
                $this->_onPhotoFileStockProcessing($contactPersonRow, $name);

            } catch(\OSDN_Exception $e) {
                $namespace = sprintf('contactPerson[%s]', $id);
                foreach($e as $m) {
                    $m->wrapFieldWithNamespace($namespace);
                }

                throw $e;
            } catch(\Exception $e) {
                throw new OSDN_Exception('Unable to save contact person', 0, $e);
            }
        }

        return true;
    }

    protected function _proceedAccount(array $params)
    {
        if (isset($params['username_old']) && empty($params['username_old']) && !empty($params['username'])) {
            return $this->_createAccount($params);
        }
        if (!empty($params['username_old']) && !empty($params['username'])) {
            return $this->_updateAccount($params);
        }
        return false;
    }

    protected function _createAccount(array $params)
    {
        if (empty($params['username'])) {
            return false;
        }

        $sAccount = new Account_Service_Account();
        $accountRow = $sAccount->fetchByUsername($params['username'], false);
        if ($accountRow) {
            $e = new OSDN_Validate_Exception();
            $e->addMessage('Username is already in use.', array(), 'username');
            throw $e;
        }

        $params['isActive'] = 1;

        $currentAccount = Zend_Auth::getInstance()->getIdentity();
        if ($currentAccount->isAdmin()) {
            if (in_array((int)$params['roleId'], $this->_companyDependentRoleIds)) {
                $companySrv = new \Company_Service_Company();
                $companyRow = $companySrv->find($params['companyId']);
                $params['parentId'] = $companyRow->accountId;
            } else {
                $params['parentId'] = null;
            }
        } else if ($currentAccount->isCompanyOwner()) {
            $params['parentId'] = $currentAccount->getId();
        } else {
            $companyRow = $currentAccount->getCompanyRow();
            $params['parentId'] = $companyRow->accountId;
        }

        $params['createdAccountId'] = $currentAccount->getId();

        $accountRow = $sAccount->create($params);
        return $accountRow->id;
    }

    protected function _updateAccount(array $params)
    {
        if (empty($params['username']) || empty($params['username_old'])) {
            return false;
        }

        if (empty($params['password'])) {
            unset($params['password']);
            unset($params['confirmPassword']);
        }

        $sAccount = new Account_Service_Account();

        if ($params['username'] !== $params['username_old']) {
            $accountRow = $sAccount->fetchByUsername($params['username'], false);
            if ($accountRow) {
                $e = new OSDN_Validate_Exception();
                $e->addMessage('Gebruikersnaam bestaat al', array(), 'username');
                throw $e;
            }
        }

        $accountRow = $sAccount->fetchByUsername($params['username_old'], false);
        if ($accountRow) {
            $sAccount->update($accountRow->id, $params);
        } else {
            $e = new OSDN_Exception();
            $e->addMessage('Vindt u niet gehouden op gebruikersnaam', array(), 'username');
            throw $e;
        }

        return $accountRow->id;
    }

    protected function _onPhotoFileStockProcessing(\Company_Model_ContactPersonRow $contactPersonRow, $name = 'photo')
    {
        $transfer = new Zend_File_Transfer_Adapter_Http();
        if (!$transfer->isUploaded($name)) {
            return;
        }

        $contactPersonPhotoFileStock = new Company_Service_ContactPersonPhotoFileStock($contactPersonRow, $name);
        $contactPersonPhotoFileStock->create(array(), $transfer);
    }

    public function createRow(array $data = array())
    {
        return $this->_table->createRow($data);
    }

    /**
     * @param Company_Model_ContactpersonRow $contactpersonRow
     * @return array array Company_Model_CompanyRow
     */
    public function getLocations(Company_Model_ContactPersonRow $contactpersonRow)
    {
        return $contactpersonRow->findManyToManyRowset('Company_Model_DbTable_Location', 'Company_Model_DbTable_ContactPersonLocation');
    }

    /**
     * @param Company_Model_ContactpersonRow $contactpersonRow
     * @param array $regions - contains integer regionids
     * return void
     */
    public function setLocations(Company_Model_ContactPersonRow $contactpersonRow, array $locationIds)
    {
        $adapter = $this->getAdapter();
        $adapter->delete('contactpersonLocation', 'contactpersonId = '.(int)$contactpersonRow->getId());
        foreach($locationIds as $locationId)
        {
            if ((int)$locationId)
            {
                $data = array(
                    'contactpersonId' => $contactpersonRow->getId(),
                    'locationId'  => $locationId,
                );
                $adapter->insert('contactpersonLocation', $data);
            }
        }
    }

    /**
     * @param Company_Model_ContactPersonRow $contactpersonRow
     * @return array Proposal_Model_CustomTemplate
     */
    public function getLocationsThemes(Company_Model_ContactPersonRow $contactpersonRow)
    {
        $locations = $this->getLocations($contactpersonRow);
        $themes = array();
        foreach($locations as $location)
        {
            $locationThemes = $location->findManyToManyRowset('Proposal_Model_DbTable_CustomTemplate','Company_Model_DbTable_LocationProposalCustomTemplate');
            if ($locationThemes)
            {
                foreach($locationThemes as $theme)
                {
                    if (!isset($themes[ $theme->getId() ]))
                    {
                        $themes[ $theme->getId() ] = array(
                            'locations' => array(),
                            'theme' => $theme,
                        );
                    }
                    $themes[ $theme->getId() ]['locations'][ $location->getId() ] = $location;
                }
            }
        }
        return $themes;
    }

    /**
     * @param Company_Model_ContactpersonRow $contactpersonRow
     * @param
     * @return array
     * @todo Not implemented yet
     */
    public function getThemes(Company_Model_ContactPersonRow $contactpersonRow)
    {
        return $contactpersonRow->findManyToManyRowset('Proposal_Model_DbTable_CustomTemplate', 'Company_Model_DbTable_ContactPersonProposalCustomTemplate');
    }

    /**
     * @param Company_Model_ContactpersonRow $contactpersonRow
     * @param array $regions - contains integer regionids
     * return void
     */
    public function setThemes(Company_Model_ContactPersonRow $contactpersonRow, array $themeIds)
    {
        $adapter = $this->getAdapter();
        $adapter->delete('contactpersonProposalcustomtemplate', 'contactpersonId = '.(int)$contactpersonRow->getId());
        foreach($themeIds as $themeId)
        {
            if ((int)$themeId)
            {
                $data = array(
                    'contactpersonId' => $contactpersonRow->getId(),
                    'proposalcustomtemplateId'  => $themeId,
                );
                $adapter->insert('contactpersonProposalcustomtemplate', $data);
            }
        }
    }
}
