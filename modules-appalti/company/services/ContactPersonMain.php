<?php

class Company_Service_ContactPersonMain extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Company_Model_DbTable_ContactPerson
     */
    protected $_table;


    public function __construct()
    {

        $this->_table = new \Company_Model_DbTable_ContactPerson($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'companyId' => array('allowEmpty' => false, 'presence' => 'required'),

            'firstname' => array('allowEmpty' => false, 'presence' => 'required'),
//            'prefix'    => array('allowEmpty' => false, 'presence' => 'required'),
            'lastname'  => array('allowEmpty' => false, 'presence' => 'required'),
            'sex'       => array('allowEmpty' => false, 'presence' => 'required'),
            'phone'     => array('allowEmpty' => false, 'presence' => 'required'),
            'email'     => array('allowEmpty' => false, 'presence' => 'required')
        ));

        $this->_attachValidationRules('update', array(
            'lastname' => array(
                'allowEmpty' => false, 'presence' => 'required',
                'messages'    => array('hello')
            ),
        ), 'default');

        parent::__construct();
    }

    public function fetchAllWithResponse(array $params = array() , $where)
    {

        $select = $this->getAdapter()->select()
            ->from(
                array('cp' => 'contactPerson')
            )
            ->joinLeft (
                array ('ac'=>'account'),
                'cp.accountId = ac.id',
                array('username'=>'ac.username')
            )
            ->where('cp.companyId = ?',$params['companyId'],  Zend_Db::INT_TYPE)
            ->where($where);

        $this->_initDbFilter($select, $this->_table)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

    /**
     * @param $id
     * @param bool $throwException
     * @throws OSDN_Exception
     */
    public function find($id, $throwException = true)
    {
        $contactPersonRow = $this->_table->findOne($id);
        if (null === $contactPersonRow && true === $throwException) {
            throw new \OSDN_Exception('Unable to find row #' . $id);
        }

        return $contactPersonRow;
    }

    /**
     * @param array $data
     */
    public function create(array $data, $companyOwner)
    {
        $f = $this->_validate($data);

        $this->getAdapter()->beginTransaction();
        try {

            $params = $f->getData();
            if (isset($params['username'])) {
            $params['accountId'] = $this->_createAccount($params , $companyOwner);
            }

//            if (!$params['accountId']) {
//                unset($params['accountId']);
//            }
            $contactPersonRow = $this->_table->createRow($params);

            $contactPersonRow->save();

            $this->_onPhotoFileStockProcessing($contactPersonRow);
            $this->_onPhotoFileStockProcessing($contactPersonRow, 'signature');

            $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to save contact person', null, $e);
        }

        return $contactPersonRow;
    }

    /**
     * @param $id
     * @param array $data
     * @return bool
     * @throws OSDN_Exception
     */
    public function update(Company_Model_ContactPersonRow $contactPersonRow, array $data)
    {

        if (isset($data['photo']) && empty($data['photo'])) {
            unset ($data['photo']);
        }
        if (isset($data['signature']) && empty($data['signature'])) {
            unset ($data['signature']);
        }

        $f = $this->_validate($data, 'update');


        $this->getAdapter()->beginTransaction();
        try {

            $params = $f->getData();
            $params['accountId'] = $this->_proceedAccount($params);

            if (!$params['accountId']) {
                unset($params['accountId']);
            }

            $contactPersonRow->setFromArray($params);
            $contactPersonRow->save();

            $this->_onPhotoFileStockProcessing($contactPersonRow);
            $this->_onPhotoFileStockProcessing($contactPersonRow, 'signature');

            $this->getAdapter()->commit();
        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        }
        return $contactPersonRow;
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        try {
            $contactPersonRow = $this->find($id);

            $contactPersonClone = clone $contactPersonRow;
            $contactPersonRow->delete();

            $contactPersonPhotoFileStock = new Company_Service_ContactPersonPhotoFileStock($contactPersonClone);
            $contactPersonPhotoFileStock->delete();

            if (null !== ($accountRow = $contactPersonClone->getAccountRow())) {
                $accountRow->delete();
            }


        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to delete', null, $e);
        }

        return true;
    }

    protected function _proceedAccount(array $params)
    {

        if (isset($params['username_old']) && empty($params['username_old']) && !empty($params['username'])) {
            return $this->_createAccount($params);
        }

        if (!empty($params['username_old']) && !empty($params['username'])) {

            return $this->_updateAccount($params);
        }


        return false;
    }


    protected function _createAccount(array $params, array $companyOwner = null)
    {
        if (empty($params['username'])) {
            return false;
        }

        $sAccount = new Account_Service_Account();
        $accountRow = $sAccount->fetchByUsername($params['username'], false);
        if ($accountRow) {
            $e = new OSDN_Validate_Exception();
            $e->addMessage('Such username already is in using', array(), 'username');
            throw $e;
        }

        if ($companyOwner != null) {
            $params['parentId'] = $companyOwner['id'];
        }

        $params['fullname'] = $params['firstname'].' '.$params['lastname'];
        $accountRow = $sAccount->create($params);

        return $accountRow->id;
    }

    protected function _updateAccount(array $params)
    {

        if (empty($params['username']) || empty($params['username_old'])) {
            return false;
        }

        if (empty($params['password'])) {

            unset($params['password']);
            unset($params['confirmPassword']);
        }


        $sAccount = new Account_Service_Account();

        if ($params['username'] !== $params['username_old']) {

            $accountRow = $sAccount->fetchByUsername($params['username'], false);
            if ($accountRow) {
                $e = new OSDN_Validate_Exception();
                $e->addMessage('Gebruikersnaam bestaat al', array(), 'username');
                throw $e;
            }
        }

        $accountRow = $sAccount->fetchByUsername($params['username_old'], false);
        if ($accountRow) {
            $table =   new Account_Model_DbTable_Account();
            $table->updateByPk($params,$accountRow->id);
        } else {

            $e = new OSDN_Validate_Exception();
            $e->addMessage('Vindt u niet gehouden op gebruikersnaam', array(), 'username');
            throw $e;
        }

        return $accountRow->id;
    }

    protected function _onPhotoFileStockProcessing(\Company_Model_ContactPersonRow $contactPersonRow, $name = 'photo')
    {
        $transfer = new Zend_File_Transfer_Adapter_Http();
        if (!$transfer->isUploaded($name)) {
            return;
        }

        $contactPersonPhotoFileStock = new Company_Service_ContactPersonPhotoFileStock($contactPersonRow, $name);
        $contactPersonPhotoFileStock->create(array(), $transfer);
    }
}
