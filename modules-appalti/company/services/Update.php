<?php

class Company_Service_Update extends OSDN_Application_Service_Dbable
{
    protected function init()
    {
    }

    public function update()
    {
        die ('not implemented yet');
    }

    public function createLibrary(Company_Model_CompanyRow $companyRow)
    {
        // current clients do already have proposalblocks
        // find these blocks and gather them in a new 'default' library
        try {
            $_libraries = new Company_Service_Library($companyRow);
            $_libraries->getAdapter()
                                ->beginTransaction();

            // create new library
            $data = array(
                'id'            => 0,
                'companyId'     => $companyRow->getId(),
                'name'          => $this->view->translate('Standaard'),
                'description'   => $this->view->translate('Uw standaard bibliotheek'),
            );
            if(!$libraryRow = $_libraries->create($data))
            {
                throw new Exception('Could not create new library.');
            }
            $this->view->libraryRow = $libraryRow;

            // add existing proposalblocks to newly created library
            $proposalblockSrv = new ProposalBlock_Service_ProposalBlock($companyRow);
            $proposalBlocks = $proposalblockSrv->fetchAll();
            foreach($proposalBlocks as $proposalBlock)
            {
                $this->view->proposalblock = $proposalBlock;
                if(!$_libraries->addProposalBlock($libraryRow, $proposalBlock))
                {
                    throw new Exception("Could not add proposalblock");
                }
            }

            // add existing company subcategories to newly created library
            $categorySrv = new ProposalBlock_Service_Category($companyRow);
            $categories = $categorySrv->fetchAll()->getRowset();
            while($category = $categories->current())
            {
                $category->libraryId = $libraryRow->getId();
                if (!$category->save())
                {
                    throw new Exception("Could not add category to library.");
                }
                $categories->next();
            }

            // commit changes
            $_libraries->getAdapter()->commit();
        } catch(Exception $e) {
            $_libraries->getAdapter()->rollback();
            $this->_helper->information($e->getMessage());
            return false;
        }
        return $libraryRow;
    }

    public function updateTheme(Company_Model_CompanyRow $companyRow)
    {
        /*
         * If only 1 theme exists, this may be the first visit
         * if first visit
         *     get company proposal themes
         *     assign each to company custom template
         */
        $_themes = new Proposal_Service_CustomTemplate($companyRow);
        $themes = $_themes->fetchAll();
        $this->view->themes = $themes;

        if (count($themes) == 0)
        {
            $this->_helper->information('No themes');
            try {
                $_themes->getAdapter()->beginTransaction();

                // create theme
                $themeRow = $_themes->createRow();
                $themeRow->companyId = $companyRow->getId();
                $themeRow->name = 'Standaard';
                $themeRow->active = 1;
                if (!$themeRow->save())
                {
                    throw Exception("Couldn't create theme.");
                }

                // add settings
                $_proposalThemes = new Proposal_Model_DbTable_ProposalTheme();
                $themeSettings = $_proposalThemes->createRow(array(
                    'companyId' => $themeRow->companyId
                ));
                if (!$themeSettings->save())
                {
                    throw Exception("Couldn't create theme settings.");
                }

                // add templates
                $_templates = new Proposal_Model_DbTable_CustomTemplateSettings();

                // - add template 'common'
                $themeTemplateRowCommon = $_templates->createRow(array(
                    'proposalCustomTemplateId' => $themeRow->getId(),
                    'scope' => 'common',
                ));
                if (!$themeTemplateRowCommon->save())
                {
                    throw Exception("Couldn't create theme template 'common'.");
                }
                // - add template 'introduction'
                $themeTemplateRowIntro = $_templates->createRow(array(
                    'proposalCustomTemplateId' => $themeRow->getId(),
                    'scope' => 'introduction',
                ));
                if (!$themeTemplateRowIntro->save())
                {
                    throw Exception("Couldn't create theme template 'introduction'.");
                }

                // - add template 'content'
                $themeTemplateRowContent = $_templates->createRow(array(
                    'proposalCustomTemplateId' => $themeRow->getId(),
                    'scope' => 'content',
                ));
                if (!$themeTemplateRowContent->save())
                {
                    throw Exception("Couldn't create theme template 'content'.");
                }
                // - add template 'attachm'
                $themeTemplateRowAttach = $_templates->createRow(array(
                    'proposalCustomTemplateId' => $themeRow->getId(),
                    'scope' => 'attachm',
                ));
                if (!$themeTemplateRowAttach->save())
                {
                    throw Exception("Couldn't create theme template 'attachm'.");
                }

                // commit changes
                $_themes->getAdapter()->commit();
                $this->_helper->information('Theme created', true, E_USER_NOTICE);
                return $themeRow;

            } catch (Exception $e) {
                $_themes->getAdapter()->rollback();
                $this->_helper->information($e->getMessage());
            }
        }
        if (count($themes) > 1)
        {
            $this->_helper->information('Too many themes');
            return;
        }
        try {
//die('stop');
            // get custom template
            $customTemplateRow = $themes->current();

            // get custom template proposal themes
            $_proposalThemes = new Proposal_Model_DbTable_ProposalTheme();
            $_proposalThemes->getAdapter()
                                ->beginTransaction();

            $select = $_proposalThemes->select()
                                        ->where('companyId = ?', $customTemplateRow->companyId)
                                        ->where('proposalId IS NULL')
                                        ->where('useCustomTemplate = ? ', $customTemplateRow->getId())
            ;
            $proposalThemeRowset = $_proposalThemes->fetchAll($select);
            if (count($proposalThemeRowset))
            {
                return $customTemplateRow;
            }

            // get proposal theme
            $select = $_proposalThemes->select()
                                            ->where('companyId = ?', $customTemplateRow->companyId)
            // all templates are selected
            // selects theme templates      ->where('proposalId IS NULL')
            // selects proposal templates   ->where('proposalId IS NOT NULL')
            ;
            $proposalThemeRowset = $_proposalThemes->fetchAll($select);
            foreach($proposalThemeRowset as $proposalThemeRow)
            {
                // save as custom template proposal theme
                $proposalThemeRow->useCustomTemplate = $customTemplateRow->getId();
                $proposalThemeRow->save();
            }

            // commit changes
            $_proposalThemes->getAdapter()->commit();
            return $customTemplateRow;
        } catch(Exception $e) {
            $_proposalThemes->getAdapter()->rollback();
            $this->_helper->information($e->getMessage());
        }
    }

}
