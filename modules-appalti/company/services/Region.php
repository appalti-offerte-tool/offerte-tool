<?php

class Company_Service_Region extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Company_Model_DbTable_Region
     */
    protected $_table;

    protected $_accountRow;

    protected function _init()
    {
        $this->_table = new \Company_Model_DbTable_Region($this->getAdapter());

        $this->_accountRow = Zend_Auth::getInstance()->getIdentity();

        $this->_attachValidationRules('default', array(
            'companyId' => array(
                'allowEmpty' => false,
                'presence' => 'required',
            ),
            'name' => array(
                'allowEmpty' => false,
                'presence' => 'required',
            ),
            'description' => array(
                'allowEmpty' => true,
                'presence' => 'required',
            ),
        ));

        $this->_attachValidationRules('update', array(
//            'companyId' => array(
//                'allowEmpty' => false,
//                'presence' => null
//            ),
//            'kind' => array(
//                'allowEmpty' => false,
//                'presence' => 'required'
//            ),
//            'name' => array(
//                'allowEmpty' => false,
//                'presence' => 'required'
//            ),
//            'pricePerUnit' => array(
//                'allowEmpty' => false,
//                'presence' => 'required'
//            ),
        ), 'default');

        parent::_init();
    }

    /**
     * @param $id
     * @param bool $throwException
     * @throws OSDN_Exception
     */
    public function find($id, $throwException = true)
    {
        $row = $this->_table->findOne($id);
        if (null === $row && true === $throwException) {
            throw new \OSDN_Exception('Unable to find row #' . $id);
        }

        return $row;
    }

    /**
     * @param array $data
     */
    public function create(array $data)
    {
        $f = $this->_validate($data);
        $row = $this->_table->createRow($f->getData());
        $row->save();

        return $row;
    }

    /**
     * @param $id
     * @param array $data
     * @return bool
     * @throws OSDN_Exception
     */
    public function update(\Company_Model_RegionRow $regionRow, array $data)
    {
        $f = $this->_validate($data, 'update');
        try {
            $regionRow->setFromArray($f->getData());
            $regionRow->save();
        } catch(\OSDN_Exception $e) {
            throw $e;
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to update');
        }

        return $regionRow;
    }

    /**
     * @param $id
     */
    public function delete($id)
    {

        if ($this->_accountRow->isProposalBuilder()) {
            throw new \OSDN_Exception('You do not have rights to delete.');
        }

        try {
            $row = $this->find($id);
            $row->delete();
        } catch(\Exception $e) {
            throw new OSDN_Exception($e->getMessage(), null, $e);
        }

        return true;
    }

    public function fetchAllByCompanyId($id, array $params = array())
    {

        if (empty($id)) {
            throw new OSDN_Exception('Company id is not defined.');
        }

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('r' => $this->_table->getTableName()),
                $this->_table->getAllowedColumns(\OSDN_Db_Table_Abstract::OMIT_FETCH_ROW)
            )
            ->where('companyId = ?', $id);

        if (!empty($params)) {
            $this->_initDbFilter($select, $this->_table)->parse($params);
        }

        return $this->getDecorator('response')->decorate($select);
    }

    public function fetchAllByCompanyWithResponse(\Company_Model_CompanyRow $companyRow, array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(array(
                'co'    => 'companyOffer',
            ))
            ->where('companyId = ?', $companyRow->getId(), Zend_Db::INT_TYPE);

        $searchFields = array(
            'co.name' => 'name',
            'co.kind' => 'kind',
            'co.pricePerUnit' => 'pricePerUnit'
        );

        $this->_initDbFilter($select, $this->_table, $searchFields)->parse($params);

        $response = $this->getDecorator('response')->decorate($select);
        $table = $this->_table;
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });

        return $response;
    }

    /**
     * @param Company_Model_CompanyRow $companyRow
     * @return array
     */
    public function getByLocation(Company_Model_CompanyRow $companyRow)
    {
        $adapter = $this->getAdapter();
        $sql = $adapter->select()
                            ->from(array('cr'=>'companyRegion'),null)
                            ->join(array('r'=>'region'), 'cr.regionId = r.id',array('id','name','description'))
                            ->where("cr.companyId = ?", $companyRow->getId())
        ;
        $result = $adapter->fetchAll($sql);
        if ($result)
        {
            return $result;
        }
        return array();
    }

    /**
     * @param Company_Model_CompanyRow $companyRow
     * @param array $regions contains regionids
     * return void
     */
    public function setLocationRegions(Company_Model_CompanyRow $companyRow, array $regions)
    {
        $adapter = $this->getAdapter();
        $count_rows_deleted = $adapter->delete('companyRegion', 'companyId = '.(int)$companyRow->getId());
        foreach($regions as $regionId)
        {
            if ((int)$regionId)
            {
                $data = array(
                    'companyId' => $companyRow->getId(),
                    'regionId'  => $regionId
                );
                $adapter->insert('companyRegion', $data);
            }
        }
    }

}