<?php

/**
 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
 * @version     SVN: $Id: Wizard.php 968 2012-10-04 10:04:37Z vanya $
 * @changedby   $Author: vanya $
 */

class Company_Service_Wizard
{
    const STEP_COMPANY_COMPANY      = 0;
    const STEP_COMPANY_CONTACTPERSON= 1;
//    const STEP_PROPOSAL_DEFAULTS    = 2;
//    const STEP_TEMPLATES            = 2;
    const STEP_TEMPLATE_TYPOGRAPHY  = 2;
    const STEP_SUMMARY              = 3;
//    const STEP_PROPOSAL_CREATE      = 3;

    protected $_wizardSteps = array(
        self::STEP_COMPANY_COMPANY => array(
            'enabled'   => true,
            'isActive'  => false,
            'title'     => 'Bedrijfsgegevens',
            'url'       => '/company/create/company',
        ),
        self::STEP_COMPANY_CONTACTPERSON => array(
            'enabled'   => true,
            'isActive'  => false,
            'title'     => 'Persoonsgegevens',
            'url'       => '/company/create/contactperson'
        ),
//        self::STEP_PROPOSAL_DEFAULTS => array(
//            'enabled'   => true,
//            'isActive'  => false,
//            'title'     => 'Offertegegevens',
//            'url'       => '/proposal/template/default'
//        ),
//        self::STEP_TEMPLATES => array(
//            'enabled'   => true,
//            'isActive'  => false,
//            'title'     => 'Template',
//            'url'       => '/proposal/template/template'
//        ),
        self::STEP_TEMPLATE_TYPOGRAPHY => array(
            'enabled'   => true,
            'isActive'  => false,
            'title'     => 'Huisstijl',
            'url'       => '/proposal/template/typography'
        ),
        self::STEP_SUMMARY => array(
            'enabled'   => true,
            'isActive'  => false,
            'title'     => 'Afronding',
            'url'       => '/company/create/summary',
            'redirect'  => '/company/index/index'
        ),
//        self::STEP_PROPOSAL_CREATE => array(
//            'enabled'   => true,
//            'isActive'  => false,
//            'title'     => 'Offerte maken',
//            'url'       => '/proposal/create/index',
//            'params'    => ''
//        ),
    );

    /**
     * @var Zend_Session_Namespace
     */
    protected $_session;

    public function __construct()
    {
        $accountId = Zend_Auth::getInstance()->getIdentity()->getId();
        $this->_session = new \Zend_Session_Namespace(__CLASS__ . '_' . $accountId);
        $this->_session->step = null;
    }

    public function init()
    {
        reset($this->_wizardSteps);
        $this->_session->step = key($this->_wizardSteps);
        $this->_wizardSteps[$this->_session->step]['isActive'] = true;
    }

    public function setCurrent($stepnr)
    {
        while(list($key, $step) = each($this->_wizardSteps))
        {
            $this->_wizardSteps[ $key ]['isActive'] = false;
        }
        $this->_session->step = $stepnr;
        $this->_wizardSteps[$this->_session->step]['isActive'] = true;
    }

    public function nextStep()
    {
        if (!$this->isLast()) {
            $this->_session->step++;
            $this->_wizardSteps[$this->_session->step]['isActive'] = true;
        }
    }

    public function getSteps()
    {
        if (!empty($this->_session->step)) {
            $this->_wizardSteps[$this->_session->step]['isActive'] = true;
        }
        return $this->_wizardSteps;
    }

    public function isLast()
    {
        end($this->_wizardSteps);
        return $this->_session->step == key($this->_wizardSteps);
    }

    public function getNextStepUrl()
    {
        if ($this->isLast()) {
            $url = $this->_wizardSteps[self::STEP_SUMMARY]['redirect'];
        } else {
            $url = $this->_wizardSteps[$this->_session->step + 1]['url'];
        }

        return $url;
    }

    public function getPrevStepUrl()
    {
        reset($this->_wizardSteps);
        $url = $this->_wizardSteps[ key($this->_wizardSteps) ]['url'];
        if (!empty($this->_session->step) && $this->_session->step - 1) {
            $url = $this->_wizardSteps[$this->_session->step - 1]['url'];
        }

        return $url;
    }

    public function getFirstStepUrl()
    {
        reset($this->_wizardSteps);
        return $this->_wizardSteps[ key($this->_wizardSteps) ]['url'];
    }

    public function getStepCompanyCompany()
    {
        return self::STEP_COMPANY_INFO;
    }

    public function getStepCompanyContactperson()
    {
        return self::STEP_COMPANY_INFO;
    }

    public function getStepProposalDefault()
    {
        return self::STEP_PROPOSAL_DEFAULTS;
    }

    public function getStepTemplates()
    {
        return self::STEP_TEMPLATES;
    }

    public function getStepTemplateTypography()
    {
        return self::STEP_TEMPLATE_TYPOGRAPHY;
    }

    public function getStepSummary()
    {
        return self::STEP_SUMMARY;
    }
}
