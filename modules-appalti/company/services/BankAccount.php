<?php

class Company_Service_BankAccount extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Company_Model_DbTable_BankAccount
     */
    protected $_table;

    protected function _init()
    {
        $this->_table = new \Company_Model_DbTable_BankAccount($this->getAdapter());
        $this->_attachValidationRules('default', array(
            'bankAccount' => array('allowEmpty' => false, 'presence' => 'required'))
        );
        $this->_attachValidationRules('update', array(
            'bankAccount' => array('allowEmpty' => false, 'presence' => 'required')), 'default'
        );
        parent::_init();
    }

    /**
     * @param $id
     * @param bool $throwException
     * @throws OSDN_Exception
     */
    public function find($id, $throwException = true)
    {
        $row = $this->_table->findOne($id);
        if (null === $row && true === $throwException) {
            throw new \OSDN_Exception('Unable to find row #' . $id);
        }
        return $row;
    }

    /**
     * @param array $data
     */
    public function create(array $data)
    {
        $f = $this->_validate($data);
        $row = $this->_table->createRow($f->getData());
        $row->save();
        return $row;
    }

    /**
     * @param $id
     * @param array $data
     * @return bool
     * @throws OSDN_Exception
     */
    public function update(\Company_Model_BankAccountRow $bankAccountRow, array $data)
    {
        $f = $this->_validate($data, 'update');
        try {
            $bankAccountRow->setFromArray($f->getData());
            $bankAccountRow->save();
        } catch(\OSDN_Exception $e) {
            throw $e;
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to update');
        }
        return $bankAccountRow;
    }

    public function bulk(\Company_Model_CompanyRow $companyRow, array $data)
    {
        foreach ($data as $id => $params)
        {
            $params['companyId'] = $companyRow->getId();
            try {
                if (is_numeric($id) && intval($id) == $id) {
                    $bankAccountRow = $this->find($id);
                    $this->update($bankAccountRow, $params);
                } else {
                    $f = $this->_validate($params);
                    $bankAccountRow = $this->create($f->getData());
                }
            } catch(\OSDN_Exception $e) {
                $namespace = sprintf('bankAccount[%s]', $id);
                foreach($e as $m) {
                    $m->wrapFieldWithNamespace($namespace);
                }
                throw $e;
            } catch(\Exception $e) {
                throw new OSDN_Exception('Unable to save bank account', 0, $e);
            }
        }
        return true;
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        try {
            $row = $this->find($id);
            $row->delete();
        } catch(\Exception $e) {
            throw new OSDN_Exception('Unable to delete', null, $e);
        }
        return true;
    }
}