<?php

class Company_Service_Create extends OSDN_Application_Service_Dbable
{
    /** @var Account_Model_DbTable_AccountRow */
    protected $accountRow;

    /** @var Company_Model_CompanyRow */
    protected $companyRow;

    /** @var Account_Model_DbTable_AclRole */
    protected $_roles;


    protected function init()
    {
        $this->accountRow = Zend_Auth::getInstance()->getIdentity();
        $this->_roles = new Account_Model_DbTable_AclRole();
    }

    public function __call($method, $arguments) {
        parent::__call($method, $arguments);
        throw new Exception('Method does not exists.');
    }

    public function create(array $data = array())
    {
        $dbAdapter = $this->_roles->getAdapter();
        try{
            $dbAdapter->beginTransaction();
            $this->createCompany($data);
            $this->createUserRole();
            $this->createContactperson($data);
            $this->createLibrary();
            $this->createTheme();
            $dbAdapter->commit();
        } catch(Exception $e) {
            $dbAdapter->rollBack();
            throw $e;
        }
    }

    protected function createCompany(array $data = array())
    {
        if (($this->companyRow = $this->accountRow->getCompanyRow(false)) != false)
        {
            throw new Exception('Company already exists');
        }
        // create company
        $_companies = new Company_Model_DbTable_Company();
        $this->companyRow = $_companies->createRow($data);
        if (!$this->companyRow->save())
        {
            throw new Exception('Could not save company.');
        }
        return $this->companyRow;
    }

    protected function createCompanyAdminRole(Company_Model_CompanyRow $parentCompany)
    {
        // get parent company role
        $selectParentCompanyRoleRow = $this->_roles
                                            ->select()
                                            ->where('companyId = ?', $parentCompany->getId())
                                            ->order('id ASC')
        ;
        $parentCompanyRoleRow = $this->_roles->fetchRow( $selectParentCompanyRoleRow );

        // get basic admin role
        $selectBasicAdminRoleRow = $this->_roles
                                            ->select()
                                            ->where('id = ?', 6)
        ;
        $basicAdminRoleRow = $this->_roles->fetchRow( $selectBasicAdminRoleRow );

        // create company role
        $data = array(
            'id'        => null,
            'companyId' => $this->companyRow->getId(),
            'name'      => $this->companyRow->name,
            'parentId'  => $parentCompanyRoleRow->getId(),
            'luid'      => null,
        );
        return $this->_roles->copyRole($basicAdminRoleRow, $data, true);
    }

    protected function createUserRole()
    {
        // get company role
        $selectCompanyAdminRoleRow = $this->_roles
                                            ->select()
                                            ->where('companyId = ')
                                            ->order('id ASC')
        ;
        $companyAdminRoleRow = $this->_roles->fetchRow( $selectCompanyAdminRoleRow );

        // get basic user role
        $selectBasicUserRoleRow = $this->_roles
                                            ->select()
                                            ->where('id = ?', 5)
        ;
        $basicUserRoleRow = $this->_roles->fetchRow( $selectBasicUserRoleRow );

        // create user role
        $data = array(
            'id'        => null,
            'companyId' => $this->companyRow->getId(),
            'name'      => 'Gebruiker',
            'parentId'  => $companyAdminRoleRow->getId(),
            'luid'      => null,
        );
        $userRoleRow = $this->_roles->copyRole($basicUserRoleRow, $data, true);

        // assign to account
        $_accounts = new Account_Model_DbTable_Account();
        $selectAccount = $_accounts->select()
                                    ->where('id = ?', $this->accountRow->getId())
        ;
        $accountRow = $_accounts->fetchRow( $selectAccount );
        $accountRow->roleId = $userRoleRow->getId();
        $accountRow->roleId = $userRoleRow->getId();
        $accountRow->save();

        // return user role
        return $userRoleRow;
    }

    protected function createContactperson(array $data = array())
    {
        // correct data
        $data['accountId'] = $this->accountRow->getId();
        $data['companyId'] = $this->companyRow->getId();

        // create contactperson
        $_contactpersons = new Company_Model_DbTable_ContactPerson();
        $contactpersonRow = $_contactpersons->createRow($data);
        $contactpersonRow->save();

        // return contactperson
        return $contactpersonRow;
    }

    protected function createLibrary()
    {
         throw new Exception('not implemented yet');
   }

    protected function createTheme()
    {
        throw new Exception('not implemented yet');
    }

}
