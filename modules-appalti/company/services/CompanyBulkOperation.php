<?php

class Company_Service_CompanyBulkOperation extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \Company_Model_CompanyRow
     */
    protected $_companyRow;

    public function __construct(\Company_Model_CompanyRow $companyRow)
    {
        $this->_companyRow = $companyRow;

        parent::__construct();
    }

    public function bulk(array $data, $throwException = true)
    {
        $existCPParams = (empty($data['contactPerson']) || !is_array($data['contactPerson'])) ? false : true;

        if (!$existCPParams && true === $throwException) {
            throw new OSDN_Exception('Unable to find contact person details');
        }

        if ($existCPParams) {
            $contactPerson = new Company_Service_ContactPerson($this->_companyRow);
            $contactPerson->bulk($this->_companyRow, $data['contactPerson']);
        }

        if (!empty($data['bankAccount'])) {
            $bankDataNS = $data['bankAccount']['namespace'];
            unset($data['bankAccount']['namespace']);
            if (!empty($data['bankAccount'][$bankDataNS]['bankAccount']) && is_array($data['bankAccount'])) {
                $companyBankAccount = new \Company_Service_BankAccount();
                $companyBankAccount->bulk($this->_companyRow, $data['bankAccount']);
            }
        }
    }
}