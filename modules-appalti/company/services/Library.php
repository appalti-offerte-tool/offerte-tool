<?php

class Company_Service_Library extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Company_Model_DbTable_Library
     */
    protected $_table;

    protected $_accountRow;

    protected $_companyRow;

    protected function _toCompanyRow()
    {
        if (!($this->_companyRow instanceof Company_Model_CompanyRow))
        {
            $this->_companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
        }
        return $this->_companyRow;
    }

    public function __construct(Company_Model_CompanyRow $companyRow = null)
    {
        $this->_companyRow = $companyRow;
        if(!$this->_companyRow)
        {
            $this->_companyRow = $this->_toCompanyRow();
        }
        if (!$this->_companyRow->isRoot()) {
            throw new OSDN_Exception('Only main companies can be accessed here');
        }
        parent::__construct();
    }

    protected function _init()
    {
        $this->_table = new \Company_Model_DbTable_Library($this->getAdapter());

        $this->_accountRow = Zend_Auth::getInstance()->getIdentity();

        $this->_attachValidationRules('default', array(
            'companyId' => array(
                'allowEmpty' => false,
                'presence' => 'required',
            ),
            'name' => array(
                'allowEmpty' => false,
                'presence' => 'required',
            ),
            'description' => array(
                'allowEmpty' => true,
                'presence' => 'required',
            ),
        ));

        $this->_attachValidationRules('update', array(
            'companyId' => array(
                'allowEmpty' => false,
                'presence' => 'required',
            ),
            'name' => array(
                'allowEmpty' => false,
//                'presence' => 'required',
            ),
            'description' => array(
                'allowEmpty' => true,
//                'presence' => 'required',
            ),
        ), 'default');

        parent::_init();
    }

    /**
     * @param $id
     * @param bool $throwException
     * @throws OSDN_Exception
     */
    public function find($id, $throwException = true)
    {
        $row = $this->_table->findOne($id);
        if (null === $row && true === $throwException) {
            throw new \OSDN_Exception('Unable to find row #' . $id);
        }

        return $row;
    }

    /**
     * @param array $data
     */
    public function create(array $data)
    {
        $f = $this->_validate($data);
        $row = $this->_table->createRow($f->getData());
        $row->save();

        return $row;
    }

    /**
     * @param $id
     * @param array $data
     * @return bool
     * @throws OSDN_Exception
     */
    public function update(\Company_Model_LibraryRow $row, array $data)
    {
        $f = $this->_validate($data, 'update');
        try {
            $row->setFromArray($f->getData());
            $row->save();
        } catch(\OSDN_Exception $e) {
            throw $e;
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to update');
        }

        return $row;
    }

    /**
     * @param $id
     */
    public function delete($id)
    {

        if ($this->_accountRow->isProposalBuilder()) {
            throw new \OSDN_Exception('You do not have rights to delete.');
        }

        try {
            $row = $this->find($id);
            $row->delete();
        } catch(\Exception $e) {
            throw new OSDN_Exception($e->getMessage(), null, $e);
        }

        return true;
    }

    public function fetchAllActive()
    {
        $select = $this->_table->select()
                                ->where('companyId = ?', $this->_toCompanyRow()->getId())
                                ->where('isActive = ?', 1)
        ;
        return $this->_table->fetchAll($select);
    }

    public function fetchAllByCompanyId($id, array $params = array())
    {
        if (empty($id)) {
            throw new OSDN_Exception('Company id is not defined.');
        }

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('r' => $this->_table->getTableName()),
                $this->_table->getAllowedColumns(\OSDN_Db_Table_Abstract::OMIT_FETCH_ROW)
            )
            ->where('companyId = ?', $id);

        if (!empty($params)) {
            $this->_initDbFilter($select, $this->_table)->parse($params);
        }

        return $this->getDecorator('response')->decorate($select);
    }

    public function fetchAllByCompanyWithResponse(\Company_Model_CompanyRow $companyRow, array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(array(
                'co'    => 'companyOffer',
            ))
            ->where('companyId = ?', $companyRow->getId(), Zend_Db::INT_TYPE);

        $searchFields = array(
            'co.name' => 'name',
            'co.kind' => 'kind',
            'co.pricePerUnit' => 'pricePerUnit'
        );

        $this->_initDbFilter($select, $this->_table, $searchFields)->parse($params);

        $response = $this->getDecorator('response')->decorate($select);
        $table = $this->_table;
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });

        return $response;
    }

    public function addProposalBlock(Company_Model_LibraryRow $libraryRow, array $proposalBlock)
    {
        $adapter = $this->_table->getAdapter();
        $data = array(
            'libraryId'         => $libraryRow->getId(),
            'proposalblockId'   => $proposalBlock['id'],
        );
        return $adapter->insert('libraryProposalblock', $data);
    }

    public function fetchAll($where = null)
    {
        $select = $this->_table
                        ->select()
        ;
        if ($where)
        {
            $select->where($where);
        }
        return $this->_table->fetchAll($select);
    }

    /**
     * @param Company_Model_LibraryRow $libraryRow - The library to copy
     * @param array $data - new library name and description
     * @param Company_Model_CompanyRow $companyRow - The company to copy to
     * @return Company_Model_LibraryRow new libraryRow
     */
    public function copy(Company_Model_LibraryRow $libraryRow, array $data = array(), Company_Model_CompanyRow $companyRow = null)
    {
        // init
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $adapter = $this->_table->getAdapter()
                                ->beginTransaction();
        $companyId = ($companyRow)? $companyRow->getId():$this->_companyRow->getId();
        // process
        try {
            // create new library from libraryRow using data
            $libraryData = array_merge(
                // get data from library to copy
                $libraryRow->toArray(),
                // change name and description
                $data,
                // change name (in cause data[name]
                array(
                    'name' => (isset($data['name']) && trim($data['name']))? $data['name']:'copy_of_'.$libraryRow->name,
                ),
                // prepare save
                array(
                    'id' => null,
                    'companyId' => $companyId,
                    'isActive'  => 1,
                )
            );
            $newLibraryRow = $this->_table->createRow($libraryData);
            $newLibraryRow->save();

            // get old proposalblock categories
            $_proposalblockcategories = new ProposalBlock_Service_Category();
            $proposalBlockCategories = $_proposalblockcategories->fetchAll($libraryRow);
            $mapOld2New = array();
            foreach($proposalBlockCategories->getRowset() as $oldProposalBlockCategory)
            {
                $data = $oldProposalBlockCategory->toArray();
                $newProposalBlockCategory = $_proposalblockcategories->createRow($data);
                $newProposalBlockCategory->libraryId = $newLibraryRow->getId();
                $newProposalBlockCategory->companyId = $companyId;
                $newProposalBlockCategory->save();

                // map ids
                $oldCatId = $oldProposalBlockCategory->getId();
                $newCatId = $newProposalBlockCategory->getId();
                $mapOld2New[ $oldCatId ] = $newCatId;
            }
//echo '<pre>'.print_r($proposalBlockCategories->getRowset()->toArray(),1).'</pre>';

            // get old library proposalblocks
            $proposalblockSrv = new ProposalBlock_Service_ProposalBlock($this->_companyRow);
            $proposalBlocks = $proposalblockSrv->fetchAll($libraryRow);
//echo '<pre>'.print_r($proposalBlocks,1).'</pre>';
//die('dfvdv');
            $data = array();
            foreach($proposalBlocks as $proposalBlock)
            {
                // create new library proposalblock from old library proposalblock
                $proposalBlockData = array_merge(
                    $proposalBlock,
                    array(
                        'id'                => 0,
                        'accountId'         => $accountRow->getId(),
                        'companyId'         => $companyId,
                        'libraryId'         => null,
                        'proposalblockId'   => null,
                    )
                );
                $newProposalBlock = $proposalblockSrv->createRow($proposalBlockData);
                $oldCatId = $proposalBlockData['categoryId'];
                if (isset($mapOld2New[ $oldCatId ]))
                {
                    $newProposalBlock->categoryId = $mapOld2New[ $oldCatId ];
                }
                $newProposalBlock->save();

                if ($newProposalBlock->imageName)
                {
                    $imageName = $proposalBlock['imageName'];
                    $newImageName = str_replace($proposalBlock['id'], $newProposalBlock->id, $newProposalBlock->imageName);
                    $basepath = APPLICATION_PATH .'/modules-appalti/proposal-block/data/block-kind-file/';
                    $oldpath = $basepath . sprintf('%d/%d/%s', $proposalBlock['companyId'], $proposalBlock['id'], $imageName);
                    if (!is_file($oldpath))
                    {
//                        throw new Exception('Could not find file to copy. ('.$oldpath.')');
                    }
                    $newpath = $basepath . sprintf('%d/%d/%s', $companyId, $newProposalBlock->id, $newImageName);
                    if (!is_dir($newpath) && !mkdir(dirname($newpath), 0777, true))
                    {
//                        throw new Exception('Could not create path.');
                    }
                    if (!copy($oldpath, $newpath))
                    {
//                        throw new Exception('Could not copy file.');
                    }
                    $newProposalBlock->imageName = $newImageName;
                    $newProposalBlock->save();
                }

                // add new proposalblock to new library
                $linkData = array(
                    'libraryId' => $newLibraryRow->getId(),
                    'proposalblockId' => $newProposalBlock->getId(),
                );
                $adapter->insert('libraryProposalblock', $linkData);
            }

            // commit changes
            $adapter->commit();

            // return new library
            return $newLibraryRow;

        } catch (OSDN_Exception $e) {
            $adapter->rollback();
            throw $e;

        } catch(\Exception $e) {
            $adapter->rollback();
            throw $e;
        }
        return false;
    }

    /**
     * @param Company_Model_LibraryRow $libraryRow
     * @return bool true on success, false on failure
     */
    public function deleteLibrary(Company_Model_LibraryRow $libraryRow)
    {
        try {
            $this->_table->getAdapter()
                            ->beginTransaction();
            // check library is owned by company
            if ($libraryRow->companyId !== $this->_companyRow->getId())
            {
                throw new OSDN_Exception($this->view->translate('U mag deze bibliotheek niet verwijderen.'));
            }

            // check library is not used by an active theme
            $_themes = new Proposal_Model_DbTable_CustomTemplate();
            $select = $_themes->select()
                                ->from(array('t'=>$_themes->getTableName()))
                                ->join(array('lit'=>'libraryProposalcustomtemplate'),'lit.proposalcustomtemplateId = t.id',array())
                                ->where('t.active = 1')
                                ->where('lit.libraryId = ?', $libraryRow->getId())
            ;
            if(count($_themes->fetchAll($select)))
            {
                throw new Exception("Deze bibliotheek kan niet verwijderd worden, omdat deze gebruikt wordt door 1 of meer thema's");
            }

            // get library proposalblocks
            $proposalBlockSrv = new ProposalBlock_Service_ProposalBlock($this->_companyRow);
            $proposalBlocks = $proposalBlockSrv->fetchAll($libraryRow);

            // delete library proposalblocks
            foreach($proposalBlocks as $proposalBlock)
            {
                $proposalBlockSrv->deleteProposalBlock($proposalBlock);
            }

            // get library categories
            $categorySrv = new ProposalBlock_Service_Category();
            $categories = $categorySrv->fetchAll($libraryRow)->getRowset();
            while(($category = $categories->current()) != false)
            {
                $category->delete();
                $categories->next();
            }

            // delete library
            $libraryRow->delete();

            // all ok
            $this->_table->getAdapter()->commit();
            return true;

        } catch (OSDN_Exception $e) {
            $this->_table->getAdapter()->rollback();
            throw new Exception($e->getMessage());
        } catch(\Exception $e) {
            $this->_table->getAdapter()->rollback();
            throw new Exception($e->getMessage());
        }
        return false;
    }


    /**
     * @param Company_Model_LibraryRow $libraryRow
     * return void
     */
    public function getThemes(Company_Model_LibraryRow $libraryRow)
    {
        return $libraryRow->findManyToManyRowset('Proposal_Model_DbTable_CustomTemplate', 'Company_Model_DbTable_LibraryProposalCustomTemplate');
    }

    /**
     * @param Company_Model_LibraryRow $libraryRow
     * @param array $themeIds
     * return void
     */
    public function setThemesById(Company_Model_LibraryRow $libraryRow, array $themeIds)
    {
        $adapter = $this->getAdapter();
        $adapter->delete('libraryProposalcustomtemplate', 'libraryId = '.(int)$libraryRow->getId());
        foreach($themeIds as $themeId)
        {
            if ((int)$themeId)
            {
                $data = array(
                    'libraryId' => $libraryRow->getId(),
                    'proposalcustomtemplateId'  => $themeId
                );
                $adapter->insert('libraryProposalcustomtemplate', $data);
            }
        }
    }

}