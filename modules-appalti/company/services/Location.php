<?php

class Company_Service_Location extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \Company_Model_DbTable_Company
     */
    protected $_table;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new Company_Model_DbTable_Company($this->getAdapter());
        $this->_accountRow = Zend_Auth::getInstance()->getIdentity();
        $this->_companyRow = $this->_accountRow->getCompanyRow(false);

        $this->_attachValidationRules('default', array(
            'name'         => array(
                'allowEmpty' => false,
                'presence' => 'required',
            ),
            'accountId'    => array(
                'id',
                'presence' => 'required',
                'allowEmpty' => true,
            ),
            'email'        => array(
                'EmailAddress',
                'allowEmpty' => true,
            ),
        ));

        $this->_attachValidationRules('update', array(
            'name'         => array('allowEmpty' => true,),
            'accountId'    => array('id', 'allowEmpty' => true),
            'email'        => array('EmailAddress', 'allowEmpty' => true)
        ), 'default');

        parent::_init();
    }

    public function fetchAllWithResponse()
    {
        $select = $this->_table->select(true);
        $accountRow = Zend_Auth::getInstance()->getIdentity();
//        if ($accountRow->isProposalBuilder()) {
//            $select->where('createdAccountId = ?', $accountRow->getId(), Zend_Db::INT_TYPE);
//        }

        if (!$accountRow->isAdmin()) {
            $select
                ->where('parentId = ?', $accountRow->getCompanyRow()->getId(), Zend_Db::INT_TYPE)
                ->where('isActive = 1');
        }

        return $this->getDecorator('response')->decorate($select, $this->_table);
    }

    public function fetchAllParentCompaniesWithResponse(array $params = array(), $activeOnly = true)
    {
        $select = $this->_table->select()
            ->from(
                array('c' => $this->_table->getTableName()),
                $this->_table->getAllowedColumns(\OSDN_Db_Table_Abstract::OMIT_FETCH_ROW)
            )
            ->where('c.parentId IS NULL');

        if ($activeOnly) {
            $select->where('isActive = 1');
        }

        $searchFields = array(
            'c.name' => 'name',
            'c.postCity' => 'postCity',
            'c.email' => 'email',
            'c.proposalCount' => 'proposalCount'
        );

        $this->_initDbFilter($select, $this->_table, $searchFields)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        $table = $this->_table;
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });

        return $response;

//        return $this->getDecorator('response')->decorate($select, $this->_table);
    }

    public function fetchAllByParentWithResponse(\Company_Model_CompanyRow $companyRow, array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(array(
                'c'    => 'company',
            ))
            ->where('c.parentId = ?', $companyRow->getId(), Zend_Db::INT_TYPE)
            ->where('c.isActive = 1');


        $accountRow = Zend_Auth::getInstance()->getIdentity();

        /* if this is a simple user then show only companies for which he created proposals */
        if ($accountRow->isProposalBuilder()) {
            $select1 = $this->getAdapter()->select()
                ->from(array('proposal'), array('clientCompanyId'))
                ->where('accountId = ? ', $accountRow->getId(), Zend_Db::INT_TYPE)
                ->group('clientCompanyId');

            $result = $select1->query()->fetchAll();

            if (!empty($result)) {
                foreach ($result as $k => $id) {
                    $select->{$k ? 'orWhere' : 'where'}('c.id =?', $id);
                }
            }
        }

//        if (!$accountRow->isAdmin()) {
//            $select->where('parentId = ?', $accountRow->getCompanyRow()->getId(), Zend_Db::INT_TYPE);
//        }

        $searchFields = array(
            'c.name' => 'name',
            'c.postCity' => 'postCity',
            'c.email' => 'email',
            'c.proposalCount' => 'proposalCount'
        );

        $this->_initDbFilter($select, $this->_table, $searchFields)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        $table = $this->_table;
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });

        return $response;
    }

    /**
     *
     * Role "Proposal builder"
     * Can select own root company or client which he created
     *
     *
     * @param $id
     * @param bool $throwException
     * @throws OSDN_Exception
     * @return \Company_Model_CompanyRow
     */
    public function find($id, $throwException = true)
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();

        $params = array('id = ?' => $id);

//        if (!$accountRow->isAdmin()) {
//            $params['isActive = ?'] = 1;
//        }

        $companyRow = $this->_table->fetchRow($params);

        if (null !== $companyRow) {
            self::assertAccountOwnershipToCompany($companyRow, $accountRow);
        }

        if (null === $companyRow && true === $throwException) {
            throw new \OSDN_Exception('Unable to find company: ' . (int) $id);
        }

//        if (
//            ! $companyRow->isRoot() &&
//            $accountRow->isProposalBuilder() &&
//            $companyRow->createdAccountId != $accountRow->getId()
//        ) {
//            throw new OSDN_Exception('You do not have permission');
//        }

        return $companyRow;
    }

    public function create(array $data, Closure $callbackFn = null)
    {
        $this->getAdapter()->beginTransaction();
        try {

            $f = $this->_validate($data);
            $companyRow = $this->_table->createRow($f->getData());

            if (!$companyRow->isRoot()) {
                unset($companyRow->accountId);
            }
            $companyRow->save();

            $transfer = new Zend_File_Transfer_Adapter_Http();
            $transfer->addValidator('MimeType', false, array(
                'image/png',
                'image/jpg',
                'image/jpeg',
                'image/gif'
            ));
            if ($transfer->isUploaded('logo')) {
                if(!$transfer->isValid())
                {
                    throw new OSDN_Exception('Het plaatje is niet van een toegestaan type (toegestane types: JPG, GIF, PNG)');
                }
                $logoFileStock = new Company_Service_CompanyLogoFileStock($companyRow);
                $logoFileStock->create(array(), $transfer);
            }

            if (null !== $callbackFn) {
                $callbackFn($companyRow);
            }

            $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception($e->getMessage());
        }

        return $companyRow;
    }

    public function update(\Company_Model_CompanyRow $companyRow, array $data, Closure $callbackFn = null)
    {
        if (empty($data['logo'])) {
            unset($data['logo']);
        }

        $f = $this->_validate($data, 'update');

        if (!$companyRow->isEditable()) {
            throw new OSDN_Exception('Restricted access to company data');
        }

        $this->getAdapter()->beginTransaction();
        try {
            $companyRow->setFromArray($f->getData());
            $companyRow->save();

            $transfer = new Zend_File_Transfer_Adapter_Http();
            $transfer->addValidator('MimeType', false, array(
                'image/png',
                'image/jpg',
                'image/jpeg',
                'image/gif'
            ));
            if ($transfer->isUploaded('logo')) {
                if(!$transfer->isValid())
                {
                    throw new OSDN_Exception('Het plaatje is niet van een toegestaan type (toegestane types: JPG, GIF, PNG)');
                }
                $logoFileStock = new Company_Service_CompanyLogoFileStock($companyRow);
                $logoFileStock->create(array(), $transfer);
            }

            if (null !== $callbackFn) {
                $callbackFn($companyRow);
            }

            $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update', 0, $e);
        }

        return $companyRow;
    }

    public function delete($id)
    {
        $companyRow = $this->find($id);
        self::assertAccountOwnershipToCompany($companyRow);

        if (!$companyRow->isEditable()) {
            throw new OSDN_Exception('Restricted access to company data');
        }

        $this->update($companyRow, array(
            'id'        => $companyRow->getId(),
            'name'      => $companyRow->name,
            'accountId' => $companyRow->accountId,
            'email'     => $companyRow->email,
            'isActive'  => 0
        ));

        return true;
    }

    /**
     * Assert account ownershipt to some company
     *
     * @param Company_Model_CompanyRow $companyRow
     * @param Account_Model_DbTable_AccountRow $accountRow
     * @param boolean $throwException
     */
    public static function assertAccountOwnershipToCompany(
        \Company_Model_CompanyRow $companyRow,
        \Account_Model_DbTable_AccountRow $accountRow = null,
        $throwException = true
    )
    {
        $accountAuthRow = Zend_Auth::getInstance()->getIdentity();
        if ($accountAuthRow->acl('company','company'))
        {
            return true;
        }

        if ($accountAuthRow->isAdmin() || $accountAuthRow->isCompanyOwner()) {
            return true;
        }

        if ($accountAuthRow->getId() == $companyRow->accountId) {
            return true;
        }

        if (!empty($accountAuthRow->parentId) && $accountAuthRow->parentId == $companyRow->accountId) {
            return true;
        }

        if (empty($companyRow->accountId)) {
            $companyAuthRow = $accountAuthRow->getCompanyRow(false);
            if (null !== $companyAuthRow && $companyRow->parentId == $companyAuthRow->getId()) {
                return true;
            }
        }

        if (true === $throwException) {
            throw new OSDN_Exception('Restricted access to company data');
        }

        return false;
    }

    /**
     * Retrieve company count per account
     * If account is "administrator" then retrieve all accounts
     *
     * @return int
     */
    public function getCompanyCount()
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();

        $clause = array();
        if ($accountRow->isProposalBuilder()) {
            $clause['createdAccountId = ?'] = $accountRow->getId();
        } elseif (!$accountRow->isAdmin()) {
            $orWhere = array();
            $orWhere[] = $this->getAdapter()->quoteInto('accountId = ?', $accountRow->getId(), Zend_Db::INT_TYPE);
            $orWhere[] = $this->getAdapter()->quoteInto('parentId = ?', $accountRow->getCompanyRow()->getId(), Zend_Db::INT_TYPE);
            $clause[] = sprintf('((%s))', join(') OR (', $orWhere));
        }

        return $this->_table->count($clause);
    }

    public function getAccountCompanySummary()
    {
        $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();

        $themeSrv = new \Proposal_Service_ProposalTheme($companyRow);
        $themeRow = $themeSrv->getDefaultRow();

        $tplSrv = new \Proposal_Service_Template();
        $templates = $tplSrv->fetchByTheme($themeRow);

        $cartSrv = new \Cart_Service_Cart();
        $orders = $cartSrv->fetchLastOrdersByCompany($companyRow);

        return array(
            'companyRow' => $companyRow,
            'themeRow'   => $themeRow,
            'templates'  => $templates,
            'lastOrders'  => $orders->getRowset()
        );
    }

    /**
     *
     * @param \Company_Model_CompanyRow $companyRow
     */
    public function addSupportMinutes(\Company_Model_CompanyRow $companyRow, \SupportTicket_Model_SupportMinutesRow $supportMinutesRow)
    {

        $amount = $supportMinutesRow->amount;
        if ($amount <= 0) {
            return false;
        }

        $last = $companyRow->supportMinutesAmount;

        $companyRow = $this->update($companyRow, array(
            'supportMinutesAmount' => $companyRow->supportMinutesAmount + $amount,
            'name' => $companyRow->name
        ));

        return true;
    }

    public function fetchAllActiveLocations()
    {
        // own company always first
        $locations = array(
            $this->_companyRow,
        );
        // find child locations
        $locationRowset = $this->_companyRow->findDependentRowset('Company_Model_DbTable_Location', 'Location', $this->_table->select()->where('isActive = 1')->order('isActive DESC'));
        foreach($locationRowset as $locationRow)
        {
            $locations[] = $locationRow;
        }

        return $locations;
    }

    public function fetchAllLocations()
    {
        // own company always first
        $locations = array(
            $this->_companyRow,
        );
        // find child locations
        $locationRowset = $this->_companyRow->findDependentRowset($this->_table, 'Location', $this->_table->select()->order('isActive DESC'));
        foreach($locationRowset as $locationRow)
        {
            $locations[] = $locationRow;
        }

        return $locations;
    }

    public function getRegions(Company_Model_CompanyRow $locationRow)
    {
        return $locationRow->findManyToManyRowset('Company_Model_DbTable_Region', 'Company_Model_DbTable_LocationRegion');
    }

    /**
     * @param Company_Model_CompanyRow $companyRow
     * @param array $regions contains regionids
     * return void
     */
    public function setRegions(Company_Model_CompanyRow $companyRow, array $regions = array())
    {
        $adapter = $this->getAdapter();
        $count_rows_deleted = $adapter->delete('companyRegion', 'companyId = '.(int)$companyRow->getId());
        foreach($regions as $regionId)
        {
            if ((int)$regionId)
            {
                $data = array(
                    'companyId' => $companyRow->getId(),
                    'regionId'  => $regionId
                );
                $adapter->insert('companyRegion', $data);
            }
        }
    }

    public function getLibraries(Company_Model_CompanyRow $locationRow)
    {
        return $locationRow->findManyToManyRowset('Company_Model_DbTable_Library', 'Company_Model_DbTable_LocationLibrary');
    }

    public function setLibraries(Company_Model_CompanyRow $locationRow, array $libraryIds = array())
    {
        $adapter = $this->getAdapter();
        $count_rows_deleted = $adapter->delete('locationLibrary', 'locationId = '.(int)$locationRow->getId());
        foreach($libraryIds as $libraryId)
        {
            if ((int)$libraryId)
            {
                $data = array(
                    'locationId' => $locationRow->getId(),
                    'libraryId'  => $libraryId,
                );
                $adapter->insert('locationLibrary', $data);
            }
        }
    }

    public function getThemes(Company_Model_CompanyRow $locationRow)
    {
        return $locationRow->findManyToManyRowset('Proposal_Model_DbTable_CustomTemplate', 'Company_Model_DbTable_LocationProposalCustomTemplate');
    }
    /**
     * @param Company_Model_CompanyRow $companyRow
     * @param array $themes contains themeids
     * return void
     */
    public function setThemes(Company_Model_CompanyRow $companyRow, array $themes = array())
    {
        $adapter = $this->getAdapter();
        $count_rows_deleted = $adapter->delete('locationProposalcustomtemplate', 'locationId = '.(int)$companyRow->getId());
        foreach($themes as $themeId)
        {
            if ((int)$themeId)
            {
                $data = array(
                    'locationId' => $companyRow->getId(),
                    'proposalcustomtemplateId'  => $themeId
                );
                $adapter->insert('locationProposalcustomtemplate', $data);
            }
        }
    }

}