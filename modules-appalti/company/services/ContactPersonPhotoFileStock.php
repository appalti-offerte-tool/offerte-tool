<?php

/**
 * This is simply emulation of filestock
 * In future can be easily adjusted to full filestock support
 */
class Company_Service_ContactPersonPhotoFileStock extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Company_Model_DbTable_ContactPerson
     */
    protected $_table;

    /**
     * @var Company_Service_ContactPerson
     */
    protected $_contactPersonRow;

    protected $_imageFileStockPath;

    protected $_imageFileStockName = 'photo';

    protected $_imageFileStockDbFieldname = 'photo';

    public function __construct(
        \Company_Model_ContactPersonRow $contactPersonRow,
        $name = 'photo'
    ) {
        $this->_contactPersonRow = $contactPersonRow;

        if (!preg_match('/(photo|signature)$/', $name, $matches)) {
            throw new OSDN_Exception('Only "photo" or "signature" is accepted');
        }

        $this->_imageFileStockName = $name;
        $this->_imageFileStockDbFieldname = $matches[1];

        parent::__construct();

        $this->_table = new \Company_Model_DbTable_Company($this->getAdapter());

        $this->_imageFileStockPath = __DIR__ . '/../data/images-contact-person-' . $this->_imageFileStockDbFieldname . '/';
    }

    public function toImageFileStockPath()
    {
        return $this->_imageFileStockPath;
    }

    /**
     * @FIXME        Change to automatic path detection
     */
    public function toImageFileStockRelativePath()
    {
        return '/modules-appalti/company/data/images-contact-person-' . $this->_imageFileStockDbFieldname . '/';
    }

    public function create(array $data, \Zend_File_Transfer_Adapter_Abstract $transfer = null)
    {
        if (null === $transfer) {
            $transfer = new \Zend_File_Transfer_Adapter_Http();
        }

        if (!$transfer->isUploaded($this->_imageFileStockName)) {
            return;
        }

        if ($this->_contactPersonRow->hasUploaded($this->_imageFileStockName)) {
            return $this->update($data, $transfer);
        }

        return $this->_onImageProcessing($transfer);
    }

    protected function _onImageProcessing(\Zend_File_Transfer_Adapter_Abstract $transfer)
    {
        $this->_doAttachTransferValidators($transfer);
        $transfer->setDestination($this->_imageFileStockPath);

        if (!$transfer->isValid($this->_imageFileStockName)) {
            $e = new OSDN_Exception('Contact person ' . $this->_imageFileStockDbFieldname . ' is invalid');
            $e->assign($transfer->getMessages());
            throw $e;
        }

        $fi = $transfer->getFileInfo($this->_imageFileStockName);
        $extension = pathinfo($fi[$this->_imageFileStockName]['name'], PATHINFO_EXTENSION);
        $filename = sprintf('%s.%s', $this->_contactPersonRow->getId(), $extension);

        $transfer->addFilter('Rename', array(
            'target'    => $this->_imageFileStockPath . $filename,
            'overwrite' => true
        ));

        $transfer->receive($this->_imageFileStockName);

        $this->_contactPersonRow->{$this->_imageFileStockDbFieldname} = $filename;
        $this->_contactPersonRow->save();

        return true;
    }

    /**
     * @return boolean
     */
    public function update(array $data, \Zend_File_Transfer_Adapter_Abstract $transfer = null)
    {
        if (null === $transfer) {
            $transfer = new \Zend_File_Transfer_Adapter_Http();
        }

        if (!$transfer->isUploaded($this->_imageFileStockName)) {
            return;
        }

        return $this->_onImageProcessing($transfer);
    }

    public function delete($throwException = false)
    {
        $files = array('photo', 'signature');

        foreach ($files as $file) {
            $filepath = $this->_imageFileStockPath . $this->_contactPersonRow->$file;
            if ($result = (false === @ unlink($filepath))) {
                if (true === $throwException) {
                    throw new OSDN_Exception('Unable to delete ' . $file);
                }
            }
        }

        return $result;
    }

    public function deleteByContactPerson(CwAccount_Model_DbTable_AccountRow $accountRow)
    {
        $accountFileStockRelation = new \CwAccount_Model_DbTable_AccountFileStockRel();
        $rowset = $accountFileStockRelation->fetchAll(array(
            'companyId = ?'    => $accountRow->getId()
        ));

        foreach($rowset as $row) {
            $this->delete($accountRow, $row->fileStockId);
        }

        return true;
    }

    protected function _doAttachTransferValidators(\Zend_File_Transfer_Adapter_Abstract $transfer)
    {
        $transfer->addValidator('IsImage', true);

        return $transfer;
    }

    public function getFileStockStorage()
    {
        return $this->_fileStock->factory();
    }

    public function view($companyId, $fileStockId, \Zend_Controller_Response_Abstract $response)
    {
        $accountFileStockRow = $this->find($companyId, $fileStockId, true);

        $this->_fileStock->download($accountFileStockRow->getFileStockRow(), $response, true, function(
            FileStock_Model_DbTable_FileStockRow $fileStockRow,
            Zend_Controller_Response_Http $response
        ) {
            /**
             * @FIXME
             *
             * Dummy replacement of header disposition
             */
            $response->setHeader('Content-disposition', sprintf('inline; filename="%s"', $fileStockRow->getName()), true);
        });
    }

    public function download($companyId, $fileStockId, \Zend_Controller_Response_Abstract $response)
    {
        $accountFileStockRow = $this->find($companyId, $fileStockId, true);
        $this->_fileStock->download($accountFileStockRow->getFileStockRow(), $response, true);
    }
}