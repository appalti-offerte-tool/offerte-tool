$(function () {

    var $modal = null, categories = null;

    function onCategoryCreate(category) {

        if (!categories) {
            categories = $('#faqCategories');
        }

        var content = '2323323';
        var id = 'el' + category.id;
        var container = document.getElementById(id);

        if (container == null) {
            var html = [
                '<label id="el' + category.id + '">',
                '<input type="checkbox" class="required" name="categoryId[]" value="' + category.id + '" />',
                '\r\n',
                category.name,
                '<a class="icon-delete float-r"  href="/faq/category/delete/id/' + category.id + '">&nbsp;</a> ',
                '<a class="icon-edit category-edit float-r"  href="/faq/category/update/id/' + category.id + '">&nbsp;</a>',

                '</label>'
            ].join('');
            categories.append(html);
        } else {
            html = [
                '<input type="checkbox" class="required" name="categoryId[]" value="' + category.id + '" />'+
                '\r\n'+
                category.name +
                '<a class="icon-delete float-r"  href="/faq/category/delete/id/' + category.id + '">&nbsp;</a> '+
                '<a class="icon-edit category-edit float-r"  href="/faq/category/update/id/' + category.id + '">&nbsp;</a>']
            container.innerHTML = html;
        }
    }

    function onCategorySubmit() {
        var $form = $(this);
        if ($form.valid()) {
            $.post(this.action, $form.serialize(), function (response) {
                if (response.success) {
                    onCategoryCreate(response.row);
                    $modal.modal('hide');
                } else {
                    alert(response.messages[0].message);
                }
            })
        }
        return false;
    }

    $('#categoryCreate').click(function () {
        $.get(this.href, function (html) {
            if (!$modal) {
                $modal = $('<div class="modal" id="faqCategoryCreate" />').appendTo('body');
            }

            $modal.html(html).modal('show');
            var $form = $modal.find('form').submit(onCategorySubmit);
            $form.validate();
        });
        return false;
    });

    $('.category-edit').click(function () {
        $.get(this.href, function (html) {
            if (!$modal) {
                $modal = $('<div class="modal" id="faqCategoryCreate" />').appendTo('body');
            }

            $modal.html(html).modal('show');
            var $form = $modal.find('form').submit(onCategorySubmit);
            $form.validate();
        });
        return false;
    });

    var $faqForm = $('#faqForm');

    var $faqTitle = $('input[name="title"]', $faqForm).change(function () {
        $(this).next().val(this.value);
    });
});