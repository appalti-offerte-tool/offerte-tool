<?php

final class Faq_Block_LastAdded extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return 'Laatst toegevoegde vragen';
    }

    protected function _toHtml()
    {
        $faqSrv = new \Faq_Service_Faq();
        $response = $faqSrv->fetchLastAdded();
        $this->view->rowset = $response->getRowset();
    }
}
