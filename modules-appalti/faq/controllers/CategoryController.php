<?php

class Faq_CategoryController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Faq_Service_Faq
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_service = new \Faq_Service_Faq();

        $this->_helper->ContextSwitch()
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->initContext();

        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */

    public function getResourceId()
    {
        return 'faq';
    }

    public function createAction()
    {
        if ($this->getRequest()->isPost()) {
            try {
                $response = $this->_service->createCategory($this->_getAllParams());
                $this->view->row = $response->toArray();
                $this->view->success = true;
                $this->_helper->information(array('Categorie "%s" aangemaakt met succes', array($response->name)), true, E_USER_NOTICE);
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information(array(
                    'Fout bij het maken F.A.Q categorie. Error: %s.',
                    array($e->getMessage())
                ), true);
            }
        } else {
            $allowDefault = $this->_service->allowCreateDefaultCategory();
            $this->view->allowDefault = $allowDefault;

            $this->render('form');
        }
    }

    public function updateAction()
    {
        if ($this->getRequest()->isPost() && $this->_getParam('categoryId')) {
            try {
                $row = $this->_service->updateCategory($this->_getParam('categoryId'), $this->_getAllParams());

                $this->view->row = $row->toArray();
                $this->view->success = true;
                $this->_helper->information(array('Categorie "%s" aangemaakt met succes', array($row->name)), true, E_USER_NOTICE);
            } catch (\Exception $e) {
                $this->view->success = false;
                $this->_helper->information('Fout bij het updaten F.A.Q. ' . $e->getMessage());
            }

        } else {

            $id = $this->_getParam('id');
            $category = $this->_service->findCategoryById($id);
            $allowDefault = $this->_service->allowCreateDefaultCategory($id);
            $this->view->row = $category;
            $this->view->allowDefault = $allowDefault;
            $this->render('form');
        }

    }

    public function deleteAction()
    {
        if ($this->_getParam('id')) {
         try {

            $this->_service->deleteCategory($this->_getParam('id'));
            $this->_helper->information('Category was deleted', true, E_USER_NOTICE);
         } catch (\Exception $e) {
             $this->_helper->information('Fout bij het updaten F.A.Q. ' . $e->getMessage());
         }
         } else {
            $this->_helper->information('Selecteer een F.A.Q om te verwijderen uit de onderstaande lijst', true, E_USER_WARNING);
        }

        $this->_redirect($this->_getParam('return') ? base64_decode($this->_getParam('return')) : '/faq/index');
    }


}