<?php

class Faq_IndexController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Faq_Service_Faq
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_service = new \Faq_Service_Faq();

        $this->_helper->ContextSwitch()
            ->addActionContext('create', 'json')
            ->initContext();

        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */

    public function getResourceId()
    {
        return 'guest';
    }

    public function indexAction()
    {
        $admin = false;
        if (Zend_Auth::getInstance()->getIdentity()->isAdmin()) {
            $admin = true;
        }
        $this->view->rowset = $this->_service->fetchAll($this->_getAllParams(),$admin);

        if ($this->_getParam('edit')) {
            $this->view->faqToEditId = $this->_getParam('edit');
            $this->view->faqToEditCategories = $this->_service->fetchFaqCategories($this->_getParam('edit'));
        }

        if (Zend_Auth::getInstance()->getIdentity()->isAdmin()) {
            $this->render('admin');
        }
    }

    public function createAction()
    {
        if ($this->getRequest()->isPost()) {
            try {
                $this->_service->create($this->_getAllParams());
                $this->_helper->information('F.A.Q gemaakt met succes', true, E_USER_NOTICE);
            } catch(\Exception $e) {
                $this->_helper->information('Fout bij het maken F.A.Q.');
            }
        }

        $this->_redirect('/faq/index');
    }

    public function updateAction()
    {
        if ($this->getRequest()->isPost() && $this->_getParam('id')) {
            try {
                $row = $this->_service->update($this->_getParam('id'), $this->_getAllParams());
                $this->_helper->information(array('F.A.Q "%s" bijgewerkt', array($row->title)), true, E_USER_NOTICE);
            } catch (\Exception $e) {
                $this->_helper->information(array(
                    'Fout bij het updaten F.A.Q. Error %s.',
                    array($e->getMessage())
                ));
            }
        } else {
            $this->_helper->information('Verkeerde verzoek.', true, E_USER_WARNING);
        }

        $this->_redirect('/faq/index/index/');
    }

    public function deleteAction()
    {
        if ($this->_getParam('id')) {
            $this->_service->delete($this->_getParam('id'));
            $this->_helper->information('F.A.Q met succes verwijderd', true, E_USER_NOTICE);
        } else {
            $this->_helper->information('Selecteer een F.A.Q om te verwijderen uit de onderstaande lijst', true, E_USER_WARNING);
        }

        $this->_redirect($this->_getParam('return') ? base64_decode($this->_getParam('return')) : '/faq/index');
    }
}