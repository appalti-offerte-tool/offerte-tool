<?php

class Faq_Service_Faq extends OSDN_Application_Service_Dbable
{
    const CATEGORY_LUID = 'faq';
    const FAQ_CATEGORY_ID = '3';

    /**
     * @var ArticleCategory_Model_DbTable_Category
     */
    protected $_table;

    /**
     * @var Article_Service_Article
     */
    protected $_articleService;

    /**
     * @var ArticleCategory_Service_Category
     */
    protected $_catService;

    protected function _init()
    {
        $this->_table = new \ArticleCategory_Model_DbTable_Category();
        $this->_articleService = new \Article_Service_Article();
        $this->_catService = new \ArticleCategory_Service_Category();
        parent::_init();
    }

    protected function _fetchCategories($admin = true)
    {
        $serviceCategory = new ArticleCategory_Service_Category();
        $articleCategoryRowRoot = $serviceCategory->findByLuid(self::CATEGORY_LUID);

        $select = $this->_table->select(true)
            ->where('isActive = 1')
            ->where('parentId = ?', $articleCategoryRowRoot->getId(), Zend_Db::INT_TYPE);

        if ($admin != true) {
            $select->where('isProtected = 0');
        }

        $response = $this->getDecorator('response')->decorate($select, $this->_table);

        return $response;
    }


    protected function _isDefaultCategory()
    {
        $articleCategoryRowRoot = $this->_catService->findByLuid(self::CATEGORY_LUID);

        $select = $this->_table->getAdapter()->select()
            ->from(
            $this->_table->getTableName(),
            array ('id')
        )
            ->where('isActive = 1')
            ->where('isProtected = 1')
            ->where('parentId = ?', $articleCategoryRowRoot->getId(), Zend_Db::INT_TYPE);

        $response = $this->getDecorator('response')->decorate($select);

        if ($response->count() == 0) {
            return false;
        }

        $response = $response->getRowset();
        return $response[0]['id'];
    }

    protected function _fetchAllConnectedArticleIds($categoryId)
    {
        $relTable = new ArticleCategory_Model_DbTable_ArticleCategoryRel();

        $select = $relTable->select()
            ->where('categoryId = ?', $categoryId, Zend_Db::INT_TYPE);

        $response = $this->getDecorator('response')->decorate($select);

        return $response->getRowset();
    }


    protected function _isConnected($articleId, $categoryId)
    {
        $relTable = new ArticleCategory_Model_DbTable_ArticleCategoryRel();

        $select = $relTable->select()
            ->where('articleId = ?', $articleId, Zend_Db::INT_TYPE)
            ->where('categoryId <> ?', $categoryId, Zend_Db::INT_TYPE);

        $response = $this->getDecorator('response')->decorate($select);

        $response = $response->toArray();
        if ($response['total'] == 0) {
            return false;
        }
        return true;
    }


    protected function _deleteConnection($connectionId)
    {
        $relTable = new ArticleCategory_Model_DbTable_ArticleCategoryRel();
        $relTable->deleteByPk($connectionId);
        return true;
    }


    protected function _updateConnection($id, $categoryId)
    {
        $relTable = new ArticleCategory_Model_DbTable_ArticleCategoryRel();
        $result = $relTable->updateByPk(array ('categoryId' => $categoryId), $id);
        return $result;
    }


    public function findCategoryById($id, $throwException = true)
    {
        $categoryRow = $this->_table->findOne($id);
        if (NULL === $categoryRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find category #' . $id);
        }
        return $categoryRow;
    }


    public function allowCreateDefaultCategory($id = null)
    {
        $select = $this->_table->getAdapter()->select()
            ->from(
            $this->_table->getTableName(),
            array ('id')
        )
            ->where('isActive = 1')
            ->where('isProtected = 1')
            ->where('parentId = ?', self::FAQ_CATEGORY_ID, Zend_Db::INT_TYPE);
        $response = $this->getDecorator('response')->decorate($select);

        $response = $response->getRowset();

        if (sizeof($response) == 0 || $response[0]['id'] == $id) {
            return true;
        }

        return false;
    }

    public function fetchAll(array $params = array (),$admin = true)
    {
        $faqCategories = $this->_fetchCategories($admin);

        $categoryIds = array ();
        $rowset = array ();
        $faqCategories->setRowsetCallback(function(\Zend_Db_Table_Row_Abstract $row) use (& $categoryIds, & $rowset)
        {
            $categoryIds[] = $row->getId();
            $rowset[$row->getId()]['category'] = $row;
            $rowset[$row->getId()]['rowset'] = array ();
            return false;
        });

        $faqCategories->getRowset();

        if (empty($categoryIds)) {
            return $rowset;
        }

        $articleSelect = $this->getAdapter()->select()
            ->from(
            array ('a' => 'article')
        )
            ->join(
            array ('acr' => 'articleCategoryRel'),
            'a.id = acr.articleId',
            array ('categoryId')
        )
            ->where('a.isActive = 1')
            ->where('acr.categoryId IN(?)', $categoryIds, Zend_Db::INT_TYPE);

        $articleTable = new \Article_Model_DbTable_Article();
        $this->_initDbFilter($articleSelect, $articleTable, array ('title', 'content', 'overview'))->parse($params);

        $query = $articleSelect->query();
        while (false != ($row = $query->fetch())) {
            $articleRow = $articleTable->createRow($row);
            $rowset[$row['categoryId']]['rowset'][] = $articleRow;
        }

        return $rowset;
    }

    public function fetchCategories()
    {
        return $this->_fetchCategories();
    }

    public function fetchLastAdded()
    {
        $select = $this->getAdapter()->select()
            ->from(array ('a' => 'article'))
            ->join(
            array ('acr' => 'articleCategoryRel'),
            'a.id = acr.articleId',
            array ('categoryId')
        )
            ->where('a.isActive = 1')
            ->order('a.modifiedDatetime DESC')
            ->limit(5);

        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

    public function create(array $data = array ())
    {
        $row = $this->_articleService->create($data);

        if ($row) {
            $articleCatSrv = new \ArticleCategory_Service_ArticleCategory();
            $categories = !is_array($data['categoryId']) ? array ($data['categoryId']) : $data['categoryId'];

            foreach ($categories as $cat) {
                $articleCatSrv->connectArticleWithCategory($row->getId(), $cat);
            }

        }

    }

    public function update($id, array $params = array ())
    {
        $this->getAdapter()->beginTransaction();
        try {
            $row = $this->_articleService->find($id);
            $row->setFromArray($params);
            $row->save();

            $articleCatSrv = new \ArticleCategory_Service_ArticleCategory();
            $categories = !is_array($params['categoryId']) ? array ($params['categoryId']) : $params['categoryId'];

            $articleCatSrv->deleteAllArticleConnections($row);

            foreach ($categories as $cat) {
                $articleCatSrv->connectArticleWithCategory($row->getId(), $cat);
            }

            $this->getAdapter()->commit();
        } catch (\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch (\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception($e->getMessage());
        }

        return $row;
    }

    public function fetchFaqCategories($faqId)
    {
        $select = $this->getAdapter()->select()
            ->from('articleCategoryRel', 'categoryId')
            ->where('articleId = ?', $faqId, Zend_Db::INT_TYPE);

        $response = $select->query()->fetchAll(PDO::FETCH_COLUMN);
        return $response;
    }

    public function createCategory(array $data = array ())
    {
        $name = trim($data['name']);
        $data['luid'] = preg_replace('/\s+/', '_', strtolower($name));
        $data['parentId'] = self::FAQ_CATEGORY_ID;
        $data['isActive'] = 1;
        $articleCatSrv = new \ArticleCategory_Service_Category();
        $row = $articleCatSrv->create($data);
        return $row;
    }

    public function updateCategory($id, array $data = array ())
    {
        $name = trim($data['name']);
        $data['parentId'] = self::FAQ_CATEGORY_ID;
        $data['isActive'] = 1;

        $data['luid'] = preg_replace('/\s+/', '_', strtolower($name));
        $articleCatSrv = new \ArticleCategory_Service_Category();
        $row = $articleCatSrv->update($id, $data);
        return $row;
    }

    public function delete($id)
    {
        $this->_articleService->delete($id);
    }

    public function deleteCategory($id)
    {
        $articleCategoryRow = $this->_catService->find($id);

        if (self::FAQ_CATEGORY_ID == $articleCategoryRow->parentId && $articleCategoryRow->isProtected == 1) {
            throw new OSDN_Exception('Unable to delete default category.');
        }

        $defaultCategory = $this->_isDefaultCategory();

        if ($defaultCategory == false) {
            throw new OSDN_Exception('To delete category you must create and activate default category.');
        }

        $articleIds = $this->_fetchAllConnectedArticleIds($id);

        if (count($articleIds) > 0) {
            foreach ($articleIds as $connection) {
                if ($this->_isConnected($connection['articleId'], $id)) {
                    $this->_deleteConnection($connection['id']);
                } else {
                    $this->_updateConnection($connection['id'], $defaultCategory);
                }
            }
        }

        return $this->_table->deleteByPk($id);
        ;
    }
}