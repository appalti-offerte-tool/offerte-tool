<?php

class ProposalSchedule_Migration_20120615_133201_84 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('proposalSchedule');
        $this->createColumn('proposalSchedule', 'proposalId', self::TYPE_INT, 11, null, true);
        $this->createColumn('proposalSchedule', 'name', self::TYPE_VARCHAR, 50);
    }

    public function down()
    {
        // degrade
    }
}