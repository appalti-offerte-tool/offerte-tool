Ext.define('Module.Jargon.Block.ExpandableList', {
    extend: 'Module.Jargon.JargonList',

    categoryId: null,

    plugins: [{
        ptype: 'rowexpander',
        rowBodyTpl: ['{explanation}']
    }],


    initComponent: function() {
        this.callParent();
        this.view.on('expandbody', function(rowNode, record, nextBd) {
            if (record.get('jargon')) {
                return;
            }
            record.set('jargon', lang('Loading...'));
            this.doLoadJargonContent(record, function(response) {
                record.set('explanation', response);
                record.commit();
                this.doLayout();
            }, this);
        }, this);
    },

    doLoadJargonContent: function(record, callbackFn, scope) {
        Ext.Ajax.request({
            url: link('jargon', 'main', 'fetch-row', {jargonId: record.internalId}),
            success: function(response, options) {
                callbackFn.call(scope, response.responseText);
            },
            failure: function() {
                Application.notificate(false);
            },
            scope: this
        });
    },

    setCategoryId: function(categoryId, forceReload) {
    
        this.categoryId = categoryId;
        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.categoryId = categoryId;
        if (true === forceReload) {
            this.getStore().load();
        }
        return this;
    }
});