Ext.define('Module.Jargon.JargonList', {
    extend : 'Ext.ux.grid.GridPanel',
    alias : 'widget.module.jargon.list',

    features : [{
        ftype : 'filters'
    }],

    modeReadOnly : false,
    dataUrl : null,
    jargonId : null,
    categoryId : null,

    initComponent : function() {

        this.store = new Ext.data.Store({
            model : 'Module.Jargon.Model.Jargon',
            proxy : {
                type : 'ajax',
                url : link('jargon', 'main', 'fetch-all', { format : 'json' }),
                reader : {
                    type : 'json',
                    root : 'rowset'
                }
            }
        });
        this.columns = [ {
            header : lang('Title'),
            dataIndex : 'title',
            flex : 1
            }, {
                header : lang('Rating'),
                dataIndex : 'rating',
                width : 300
            }, {
                xtype : 'actioncolumn',
                header : lang('Actions'),
                width : 50,
                fixed : true,
                items : [ {
                    tooltip : lang('Edit'),
                    iconCls : 'icon-edit-16 icon-16',
                    handler : function(g, rowIndex) {
                        this.onEditCategory(g, g.getStore().getAt(rowIndex));
                    },
                    scope : this
                    }, {
                        tooltip : lang('Delete'),
                        iconCls : 'icon-delete-16 icon-16',
                        handler : this.onDeleteJargon,
                        scope : this
                    } ]
            } ];
        this.tbar = [{
            text : lang('Create'),
            iconCls : 'icon-create-16',
            handler : this.onJargonCreate,
            scope : this
        }];
        this.plugins = [ new Ext.ux.grid.Search({
            minChars : 1,
            stringFree:true,
            showSelectAll:false,
            disableIndexes:['rating'],
            align : 2
        }) ].concat(Ext.isArray(this.plugins) ? this.plugins : []);
        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store : this.store,
            plugins : 'pagesize'
        });
        this.callParent();
    },

    onEditCategory : function(g, record) {
        Application.require([ 'jargon/./form' ], function() {
            var f = new Module.Jargon.Form({
                jargonId : record.internalId
                });
            f.categoryId = this.categoryId;
            f.on('completed', function(clientId) {
                g.getStore().load();
                }, this);
            f.showInWindow();
            }, this);
        },

    onDeleteJargon : function(g, rowIndex) {
        var record = g.getStore().getAt(rowIndex);
        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'),
        function(b) {
            if (b != 'yes') {
                return;
            }
            Ext.Ajax.request({ url : link('jargon', 'main', 'delete', { format : 'json' }),
                method : 'POST',
                params : {
                    id : record.internalId
                },
                success : function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);
                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
             scope : this
            });
        }, this);
    },

    onJargonCreate : function() {
        Application.require([ 'jargon/./form' ], function() {
            var f = new Module.Jargon.Form({});
            f.categoryId = this.categoryId;
            f.on('completed', function(clientId) {
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
        this.Reload;
    }

});