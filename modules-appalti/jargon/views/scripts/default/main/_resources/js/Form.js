Ext.define('Module.Application.Model.Error', {
    extend: 'Ext.data.Model',
    fields: [
        'message', 'type', 'field',
        {name: 'id', mapping: 'field'},
        {name: 'msg', mapping: 'message'}
    ]
});

Ext.define('Module.Jargon.Form', {
    extend: 'Ext.form.Panel',
    alias: 'widget.module.jargon.jargon-form',

    bodyPadding: 5,
    jargonId: null,
    categoryId:null,
    trackResetOnLoad: true,
    waitMsgTarget: true,
    wnd: null,
    model: 'Module.Jargon.Model.Jargon',
    
    initComponent: function() {
        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.Jargon.Model.Jargon',
            type: 'json',
            root: 'rowset'
        });
        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
            model: 'Module.Application.Model.Error',
            type: 'json',
            root: 'messages',
            successProperty: 'success'
        });
        this.initialConfig.trackResetOnLoad = true;
        this.items = [{
            xtype: 'textfield',
            fieldLabel: lang('Title'),
            labelWidth: 100,
            name: 'title',
            anchor:'100%'
        },{
            xtype: 'htmleditor',
            fieldLabel: lang('Explanation'),
            name: 'explanation',
            height : 150,
            width:600,
            labelWidth: 100,
            allowBlank: true
        }];
        this.callParent(arguments);
    },
    doLoad: function() {
        if (this.jargonId) {
        this.form.load({
          url: link('jargon', 'main', 'fetch-row', {format: 'json'}),
            method: 'get',
            params: {
                jargonId: this.jargonId	
            },
            scope: this
        });
        }
    },
    
    showInWindow : function() {
        this.border = false;
        var w = this.wnd = new Ext.Window({
            title : lang('Jargon'),
            resizable : false,
            height : 250,
            border : false,
            modal : true,
            items : [ this ],
            buttons : [ {
                text : lang('Save'),
                handler : this.onSubmit,
                scope : this
                }, {
                    text : lang('Close'),
                    handler : function() {
                        w.close();
                        this.wnd = null;
                    },
                    scope : this
                } ]
        });
        this.doLoad();
        w.show();
        return w;
    },
    onSubmit : function(panel, w) {
        var params = {};
        if (this.jargonId) {
            var action = 'edit';
            params['jargonId'] = this.jargonId;
            params['jargonCategoryId'] = this.categoryId;
        }
        if (!this.jargonId) {
            params['jargonCategoryId'] = this.categoryId;
            var action = 'create';
        }
        this.form.submit({
            url: link('jargon', 'main', action, {format: 'json'}),
            method: 'post',
            params: params,
            waitMsg: Ext.LoadMask.prototype.msg,
            success: function(form, action) {
                var responseObj = Ext.decode(action.response.responseText);
                Application.notificate(action);
                if (action.result.success) {
                    this.fireEvent('completed', this, responseObj.id, action.response);
                    this.wnd && this.wnd.close();
                }
            },
            scope: this
        });
    }
});