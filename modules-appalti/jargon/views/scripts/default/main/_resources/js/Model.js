Ext.define('Module.Jargon.Model.Jargon', {
    extend : 'Ext.data.Model',
    fields : ['id', 'title', 'explanation', 'rating' ]
});