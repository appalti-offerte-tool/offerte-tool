Ext.define('Module.JargonCategory.Model.Jargon.Category', {
    extend:'Ext.data.Model',
    fields:[ 'id', 'name', 'desc' ]
});