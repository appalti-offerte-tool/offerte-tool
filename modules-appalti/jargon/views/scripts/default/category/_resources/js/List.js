Ext.define('Module.JargonCategory.List', {
    extend:'Ext.ux.grid.GridPanel',
    alias:'widget.module.jargon.list',
    filterRequestParam:null,

    features:[ {
        ftype:'filters'
    } ],

    initComponent:function() {

        this.store = new Ext.data.Store({
            model:'Module.JargonCategory.Model.Jargon.Category',
            proxy:{
                type:'ajax',
                url:link('jargon', 'category', 'fetch-all', { format:'json' }),
                reader:{
                    type:'json',
                    root:'rowset'
                }
            }
        });

        this.columns = [ {
            header:lang('name'),
            dataIndex:'name',
            flex:1
        }, {
            header:lang('Description'),
            dataIndex:'desc',
            flex:1
        }, {
            xtype:'actioncolumn',
            header:lang('Actions'),
            width:50,
            fixed:true,
            items:[ {
                tooltip:lang('Edit'),
                iconCls:'icon-edit-16 icon-16',
                handler:function(g, rowIndex) {
                    this.onEditCategory(g, g.getStore().getAt(rowIndex));
                },
                scope:this
            }, {
                tooltip:lang('Delete'),
                iconCls:'icon-delete-16 icon-16',
                handler:this.onDeleteCategory,
                scope:this
            } ]
        } ];

        this.tbar = [ {
            text:lang('Create'),
            iconCls:'icon-create-16',
            handler:this.onJargonCreate,
            scope:this
        } ];

        this.plugins = [ new Ext.ux.grid.Search({
            minChars:2,
            stringFree:true,
            align:2
        }) ].concat(Ext.isArray(this.plugins) ? this.plugins : []);
        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store:this.store,
            plugins:'pagesize'
        });
        ;
        this.callParent();

        this.getView().on('itemdblclick', function(w, record) {
            this.onEditCategory(this, record);
        }, this);
    },

    onDeleteCategory:function(g, rowIndex) {
        var record = g.getStore().getAt(rowIndex);
        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'),
        function(b) {
            if (b != 'yes') {
                return;
            }
            Ext.Ajax.request({
                url:link('jargon', 'category', 'delete', { format:'json' }),
                method:'POST',
                params:{
                    id:record.get('id')
                },
                success:function(response, options) {
                    var decResponse = Ext
                            .decode(response.responseText);
                    Application
                            .notificate(decResponse.messages);
                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope:this
            });
        }, this);
    },

    onJargonCreate:function() {
        Application.require([ 'jargon/category/form' ], function() {
            var f = new Module.Jargon.Category.Form({});
            f.on('completed', function(clientId) {
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
        this.Reload;
    },

    onEditCategory:function(g, record) {
        Application.require([ 'jargon/category/form' ], function() {
            var f = new Module.Jargon.Category.Form({
                categoryId:record.get('id')
            });
            f.on('completed', function(clientId) {
                g.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    }
});