<?php


class Jargon_Model_DbTable_Jargon extends OSDN_Db_Table_Abstract
{

    protected $_primary = 'id';

    protected $_name = 'jargon';

    protected $_rowClass = 'Jargon_Model_JargonRow';


}
