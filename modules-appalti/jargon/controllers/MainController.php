<?php

class Jargon_MainController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{

    public function init()
    {
        $this->_helper->ajaxContext()
            ->addActionContext('edit', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch-row', 'json')
            ->initContext();

        $this->_service = new Jargon_Service_Jargon();
        parent::init();
    }

    public function getResourceId()
    {
        return 'jargon';
    }

    
    public function indexAction()
        {

        }
    public function fetchAllAction()
    {
        try {
            $jargonRow = $this->_service->fetchAll($this->_getAllParams());
            $this->view->assign($jargonRow->toArray());
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchRowAction()
    {
        try {
            $data = $this->_service->fetchRow($this->_getParam('jargonId'));
            $this->view->assign($data->toArray());
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function createAction()
    {
        try {
            $params = $this->_getAllParams();
            $response = $this->_service->create($params);
            $this->view->id = $response['id'];
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function editAction()
    {
        try {
            $this->_service->update($this->_getParam('jargonId'), $this->_getAllParams());
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->view->success = $this->_service->delete($this->_getParam('id'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }
}