<?php

class Jargon_Migration_20120504_084751_89 extends Core_Migration_Abstract
{

    public function up()
    {
        $this->createTable('jargonCategory');
        $this->createColumn('jargonCategory', 'name', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('jargonCategory', 'desc', Core_Migration_Abstract::TYPE_VARCHAR, 255);

        $this->createTable('jargon');
        $this->createColumn('jargon', 'jargonCategoryId', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createColumn('jargon', 'title', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('jargon', 'explanation', Core_Migration_Abstract::TYPE_TEXT);
        $this->createColumn('jargon', 'rating', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createIndex('jargon', 'jargonCategoryId');

        $this->createForeignKey('jargon', array('jargonCategoryId'), 'jargonCategory', array('id'), 'FK_jargonCategoryId');
    }

    public function down()
    {
        $this->dropTable('jargon');
        $this->dropTable('jargonCategory');
    }


}

