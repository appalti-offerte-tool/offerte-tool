<?php

class Lib_Migration_00000000_000000_00 extends Core_Migration_Abstract
{

    public function up()
    {
         
        $this->createTable('jargonCategory');
        $this->createColumn('jargonCategory', 'name', self::TYPE_VARCHAR, 50);
        $this->createColumn('jargonCategory', 'desc', self::TYPE_VARCHAR, 50);

        $this->createTable('jargon');
        $this->createColumn('jargon', 'jargonCategoryId', self::TYPE_INT, 11, null, false);
        $this->createColumn('jargon', 'title', self::TYPE_VARCHAR, 50);
        $this->createColumn('jargon', 'explanation', self::TYPE_TEXT);
        $this->createColumn('jargon', 'priority', self::TYPE_INT, 11, null, false);
        $this->createColumn('jargon', 'rating', self::TYPE_INT, 11, null, false);

        $this->createIndex('jargon', 'jargonCategoryId');
        $this->createForeignKey('jargon', array('accountId'), 'jargonCategory', array('id'), 'FK_jargoncategoryId');
    }

    public function down()
    {
        $this->dropTable('jargonCategory');
        $this->dropTable('jargon');
    }
}

