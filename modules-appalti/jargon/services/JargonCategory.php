<?php

class Jargon_Service_JargonCategory extends \OSDN_Application_Service_Dbable
{
    protected $_table;

    protected function _init()
    {
        $this->_table = new Jargon_Model_DbTable_JargonCategory($this->getAdapter());
        $this->_attachValidationRules('default', array(
            'name'              => array('allowEmpty' => false, 'presence' => 'required'),
            'desc'              => array('allowEmpty' => false, 'presence' => 'required')
        ));

        parent::_init();
    }

    /**
     * @param $id
     * @param bool $throwException
     * @return mixed
     * @throws OSDN_Exception
     */
    public function find ($id, $throwException = true)
    {
        $categoryRow = $this->_table->findOne($id);
        if (null === $categoryRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find category #' . $id);
        }
        return $categoryRow;
    }

    public function fetchAllWithResponse(array $params = array())
    {
        try {
            $select = $this->_table->getAdapter()->select()
                ->from(
                    $this->_table->getTableName(),
                    array('id', 'name', 'desc')
            );
            $this->_initDbFilter($select, $this->_table)->parse($params);
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to select category', 0, $e);
        }
        return $this->getDecorator('response')->decorate($select);
    }

    public function update($id, $data)
    {
        $categoryRow = $this->find($id);

        try {
            $categoryRow->setFromArray($data);
            $categoryRow->save();
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to update category', 0, $e);
        }

        return $categoryRow;
    }

    public function delete($id)
    {
        $jargonService = new Jargon_Service_Jargon();
        try {
            $categoryRow = $this->find($id);
            $jargonService->deleteAllByCategoryId($id);
            $categoryRow->delete();
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to delete category');
            return false;
        }
        return true;
    }

    public function create($data)
    {
        try {
            $f = $this->_validate($data);
            $row = $this->_table->createRow($f->getData());
            $row->save();
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to create category', 0, $e);
        }

        return $row->toArray();
    }
}
