<?php


class Jargon_Service_Jargon extends \OSDN_Application_Service_Dbable
{

    protected $_table;

    protected function _init()
    {
        $this->_table = new Jargon_Model_DbTable_Jargon($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'jargonCategoryId'    => array('allowEmpty' => true, 'presence' => 'required'),
            'title'               => array('allowEmpty' => false, 'presence' => 'required'),
            'explanation'         => array('allowEmpty' => false, 'presence' => 'required')

        ));

        parent::_init();
    }

    /**
     * @param $id
     * @param bool $throwException
     * @return null|Zend_Db_Table_Row
     * @throws OSDN_Exception
     */
    public function find($id, $throwException = true)
    {
        $jargonRow = $this->_table->findOne($id);
        if (null === $jargonRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find jargon #' . $id);
        }
        return $jargonRow;
    }

    public function fetchAll(array $params = array())
    {
        try {
            $select = $this->_table->getAdapter()->select()
            ->from(
                $this->_table->getTableName(),
                    array('id', 'title', 'rating')
            )
            ->order('id ASC');

            if (isset($params['categoryId'])){
                $select->where ('jargonCategoryId = ?',$params['categoryId'] , Zend_Db::INT_TYPE);
            }
            $this->_initDbFilter($select,$this->_table)->parse($params);
            $response = $this->getDecorator('response')->decorate($select);
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to select jargon list', 0, $e);
        }
        return $response;
    }

    public function fetchRow($id)
    {
        try {
            $select = $this->_table->getAdapter()->select()
                ->from(
                    $this->_table->getTableName(),
                    array('id', 'title', 'explanation', 'rating')
                )
                ->where ('id = ?', $id, Zend_Db::INT_TYPE)
                ->order('id ASC');
            $response = $this->getDecorator('response')->decorate($select);
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to select jargons', 0, $e);
        }
        return $response;
    }

    public function create($data)
    {
        try {
            $f = $this->_validate($data);
            $row = $this->_table->createRow($f->getData());
            $row->save();
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to create jargon', 0, $e);
        }
        return $row->toArray();
    }

    public function delete($id)
    {
        try {
            $jargonRow = $this->find($id);
            $jargonRow->delete();
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to delete jargon');
        }
        return true;
    }

    public function deleteAllByCategoryId($id)
    {
        try {
             $this->_table->deleteQuote(array('jargonCategoryId = ?' => $id));
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to delete jargons');
        }
        return true;
    }

    public function update($id, array $data)
    {
        $jargonRow = $this->find($id);

        try {
            $jargonRow->setFromArray($data);
            $jargonRow->save();
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to update jargon', 0, $e);
        }

        return $jargonRow;
    }
}
