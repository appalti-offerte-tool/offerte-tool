<?php

final class Jargon_Block_Preview extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Jargon List');
    }

    protected function _toHtml()
    {
        $categoryId = $this->_getParam('categoryId');
        $this->view->assign('categoryId',$categoryId);
    }
}
?>