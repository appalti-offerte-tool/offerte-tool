<?php


class SupportTicket_Model_SupportTicketRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{

    protected $_select;

    const CLIENT = 'client';

    public function init()
    {
        parent::init();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAccountRow()
    {
        return $this->findParentRow('Account_Model_DbTable_Account', 'Account');
    }

    /**
     * @return Company_Model_CompanyRow
     */
    public function getCompanyRow()
    {
        return $this->findParentRow('Company_Model_DbTable_Company', 'Company');
    }

    public function getWriterRow()
    {
        return $this->findParentRow('Account_Model_DbTable_Account', 'WriterPerson');
    }

    public function getContactPersonRow()
    {
        return $this->findParentRow('Company_Model_DbTable_ContactPerson', 'ContactPerson');
    }

    public function getUsername()
    {
        if (isset($this->username)) {
            return $this->username;
        }
        $accountRow = $this->getAccountRow();
        if (null !== $accountRow) {
            return $accountRow->username;
        }
        return 'Anonymous';
    }
}
