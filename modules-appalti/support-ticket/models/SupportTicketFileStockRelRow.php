<?php

class SupportTicket_Model_SupportTicketFileStockRelRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    public function getId()
    {
        return $this->id;
    }

    public function getFileStockRow()
    {
        return $this->findParentRow('FileStock_Model_DbTable_FileStock', 'FileStock');
    }

    public function getDocumentRow()
    {
        return $this->findParentRow('SupportTicket_Model_DbTable_SupportTicket', 'Document');
    }
}