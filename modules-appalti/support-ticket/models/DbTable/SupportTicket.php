<?php


class SupportTicket_Model_DbTable_SupportTicket extends OSDN_Db_Table_Abstract
{
    const STATUS_NEW            = 'new';
    const STATUS_IN_PROGRESS    = 'pending';
    const STATUS_CLOSED         = 'close';

    const PRIORITY_LOW      = 'low';
    const PRIORITY_NORMAL   = 'normal';
    const PRIORITY_URGENT   = 'urgent';

    protected $_primary = 'id';

    protected $_name = 'supportTicket';

    protected $_rowClass = 'SupportTicket_Model_SupportTicketRow';

    protected $_nullableFields = array(
        'modifiedDatetime'
    );

    protected $_referenceMap    = array(
        'Account'       => array(
            'columns'       => 'accountId',
            'refTableClass' => 'Account_Model_DbTable_Account',
            'refColumns'    => 'id'
        ),

        'Company'       => array(
            'columns'       => 'companyId',
            'refTableClass' => 'Company_Model_DbTable_Company',
            'refColumns'    => 'id'
        ),

        'WriterPerson' => array(
            'columns'       =>'writerId',
            'refTableClass' =>'Account_Model_DbTable_Account',
            'refColumns'    =>'id'
        ),
        'ContactPerson' => array(
            'columns'       =>'contactPersonId',
            'refTableClass' =>'Company_Model_DbTable_ContactPerson',
            'refColumns'    =>'id'
        )

    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        if (empty($data['createdDatetime'])) {
            $dt = new DateTime();
            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        $data['accountId'] = Zend_Auth::getInstance()->getIdentity()->getId();
        return parent::insert($data);
    }

    public function update(array $data, $where)
    {
        if (empty($data['modifiedDatetime'])) {
            $dt = new DateTime();
            $data['modifiedDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        unset($data['accountId'], $data['companyId']);
        return parent::update($data, $where);
    }

    public static function getStatusOptions()
    {
        return array(
            self::STATUS_NEW            => 'Nieuwe',
            self::STATUS_IN_PROGRESS    => 'Aan de gang',
            self::STATUS_CLOSED         => 'Gesloten',
        );
    }

    public static function getPriorityOptions()
    {
        return array(
            self::PRIORITY_LOW      => 'Laag',
            self::PRIORITY_NORMAL   => 'Normaal',
            self::PRIORITY_URGENT   => 'Urgent',
        );
    }
}