<?php

class SupportTicket_Model_DbTable_ServicePricePriorityRel extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'servicePricePriorityRel';

    protected $_rowClass = '\\SupportTicket_Model_ServicePricePriorityRelRow';
}