<?php

class SupportTicket_Model_DbTable_SupportMinutes extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'supportMinutes';

    protected $_rowClass = '\\SupportTicket_Model_SupportMinutesRow';
}