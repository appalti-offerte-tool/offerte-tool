<?php

class SupportTicket_Model_DbTable_SupportTicketFileStockRel extends OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name    = 'supportTicketFileStockRel';

    protected $_rowClass = '\SupportTicket_Model_SupportTicketFileStockRelRow';
}