<?php

class SupportTicket_Model_DbTable_SupportTicketPost extends OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'supportTicketPost';

    protected $_rowClass = 'SupportTicket_Model_SupportTicketPostRow';

    protected $_referenceMap = array(
        'Account' => array(
            'columns'       => 'accountId',
            'refTableClass' => 'Account_Model_DbTable_Account',
            'refColumns'    => 'id'
        ),
        'FileStock'    => array(
            'columns'       => 'id',
            'refTableClass' => 'SupportTicket_Model_DbTable_SupportTicketFileStockRel',
            'refColumns'    => 'ticketPostId'
        )
    );

    public function insert(array $data)
    {
        if (empty($data['createdDatetime'])) {
            $dt = new DateTime();
            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        $account = Zend_Auth::getInstance()->getIdentity();

        $data['accountId'] = $account->getId();
        if ($account->isAdmin()) {
            $data['type'] = 'support';
        }
        return parent::insert($data);
    }
}