<?php
class SupportTicket_Model_DbTable_SupportTicketServicePrice extends OSDN_Db_Table_Abstract
{
    protected $_select;
    
    protected $_name    = 'servicePrice';

    protected $_rowClass = 'SupportTicket_Model_SupportTicketServicePriceRow';

    protected $_nullableFields = array(
        'wordCount'
    );

}