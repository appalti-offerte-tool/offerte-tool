<?php

class SupportTicket_Model_SupportTicketPostRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return Zend_Db_Table_Rowset
     */
    public function fetchAllFileStockRowset()
    {
        $select = $this->getTable()->getAdapter()->select()
            ->from(array('fs' => 'fileStock'))
            ->join(
                array('stfs' => 'supportTicketFileStockRel'),
                'stfs.fileStockId = fs.id',
                array()
            )
            ->where('stfs.ticketPostId = ?', $this->getId());

        $query = $select->query();
        $rowset = array();
        while ($row = $query->fetch()) {
            $rowset[] = $row;
        }

        $table = new FileStock_Model_DbTable_FileStock($this->getTable()->getAdapter());
        return new Zend_Db_Table_Rowset(array(
            'table'    => $table,
            'data'     => $rowset,
            'readOnly' => false,
            'rowClass' => $table->getRowClass(),
            'stored'   => true
        ));
    }

    /**
     * @return Account_Model_DbTable_AccountRow
     */
    public function getAccountRow()
    {
        return $this->findParentRow('Account_Model_DbTable_Account', 'Account');
    }

    public function sendEmail($isSend)
    {
        $this->sendMail = $isSend ? 'yes' : 'no';
        $this->accountId = Zend_Auth::getInstance()->getIdentity()->getId();

        return $this;
    }
}