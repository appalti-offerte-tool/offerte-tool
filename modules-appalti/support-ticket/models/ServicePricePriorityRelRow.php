<?php

class SupportTicket_Model_ServicePricePriorityRelRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{

    const PRIORITY_LOW = 'low';
    const PRIORITY_NORMAL = 'normal';
    const PRIORITY_URGENT = 'urgent';

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }
}