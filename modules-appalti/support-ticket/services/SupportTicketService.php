<?php

class SupportTicket_Service_SupportTicketService extends \OSDN_Application_Service_Dbable
{
    protected $_table;
    protected $_fileStock;

    protected function _init()
    {
        $this->_table = new SupportTicket_Model_DbTable_SupportTicketService($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'name' => array('allowEmpty' => false, 'presence' => 'required'),
            'desc' => array('allowEmpty' => false, 'presence' => 'required')
        ));

        parent::_init();
    }

    public function find($id, $throwException = true)
    {
        $serviceRow = $this->_table->findOne($id);
        if (null === $serviceRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find service');
        }
        return $serviceRow;
    }

    public function findByLuid($luid)
    {
        try {
            $select = $this->_table->getAdapter()->select()
                ->from($this->_table->getTableName())
                ->where('luid = ?', $luid);
            $row = $select->query()->fetch();
        } catch(\Exception $e) {
            $row = null;
        }

        return $row;
    }

    public function fetchAllWithResponse(array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(
            $this->_table->getTableName(),
            array('id', 'name', 'desc')
        );
        $this->_initDbFilter($select, $this->_table)->parse($params);
        return $this->getDecorator('response')->decorate($select);
    }

    public function  fetchAllByMembershipWithResponse(array $params = array())
    {
        $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();

        $select = $this->getAdapter()->select()
            ->from(array('sr' => 'service'),
            array('id', 'name', 'desc')
        )
            ->join(
            array('ma' => 'membershipCompany'),
            'ma.companyId =' . $companyRow->getId(),
            array()
        )
            ->join(
            array('sp' => 'servicePrice'),
            'sp.serviceId = sr.id AND sp.membershipTypeId = ma.membershipId',
            array('price', 'discount')
        );
        $this->_initDbFilter($select, $this->_table)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

    public function  fetchAllAllowServicesByDefaultMembershipWithResponse(array $params = array())
    {

        $select = $this->getAdapter()->select()
            ->from(array('sr' => 'service'),
            array('id', 'name', 'desc')
        );

        $this->_initDbFilter($select, $this->_table)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

    /**
     * @param $data
     * @return $response price by payed membership
     */
    public function fetchPrice($data)
    {

        try {
            $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
            $serviceRow = $this->find($data['serviceId']);

            $select = $this->getAdapter()->select()
                ->from(array('sr' => 'service'),
                array('name', 'desc')
            )
                ->join(
                array('ma' => 'membershipCompany'),
                'ma.companyId =' . $companyRow->getId(),
                array('id', 'startDatetime', 'endDatetime')
            )
                ->join(
                array('sp' => 'servicePrice'),
                'sp.serviceId = sr.id AND sp.membershipTypeId = ma.membershipId',
                array('price', 'discount', 'wordCount')
            )
                ->where('sr.id = ?', $serviceRow->getId());

            $row = $select->query()->fetch();

            $servicePriorityPercents = new \SupportTicket_Service_ServicePricePriority();
            $pricePriorityPercents = $servicePriorityPercents->fetchAllByService($serviceRow);

            if (!empty($row)) {
                $price = $row['price'];
                $discount = $price * $row['discount'] / 100;

                if (!empty($data['priorityId'])) {
                    switch(strtolower($data['priorityId'])) {
                        case \SupportTicket_Model_ServicePricePriorityRelRow::PRIORITY_LOW:
                            $price -= $price * $pricePriorityPercents[\SupportTicket_Model_ServicePricePriorityRelRow::PRIORITY_LOW] / 100;
                            break;

                        case \SupportTicket_Model_ServicePricePriorityRelRow::PRIORITY_URGENT :
                            $price += $price * $pricePriorityPercents[\SupportTicket_Model_ServicePricePriorityRelRow::PRIORITY_URGENT] / 100;
                            break;

                        default:
                            break;
                    }
                }

                $price = $price - $discount;

                return (array('price' => ceil($price), 'wordCount' => $row['wordCount']));
            } else {
                return null;
            }
        } catch (\Exception $e) {
            return null;
        }
    }

    public function fetchAllWithPrices()
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('s' => 'service'),
                array('id', 'name')
            )
            ->join(
                array('sp' => 'servicePrice'),
                's.id = sp.serviceId',
                array('price', 'discount', 'wordCount')
            )
            ->group('s.id');

        return $select->query()->fetchAll();
    }

    public function update($id, $data)
    {
        $serviceRow = $this->find($id);
        try {
            $serviceRow->setFromArray($data);
            $serviceRow->save();
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to update service');
        }
        return $serviceRow;
    }

    public function delete($id)
    {
        $ticketService = new SupportTicket_Service_SupportTicket();
        $priceService = new SupportTicket_Service_SupportTicketServicePrice();
        try {
            $serviceRow = $this->find($id);
            $ticketService->deleteAllByServiceId($id);
            $priceService->deleteByServiceId($id);
            $serviceRow->delete();
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to delete service');
        }
        return true;
    }

    public function create($data)
    {
        try {
            $f = $this->_validate($data);
            $row = $this->_table->createRow($f->getData());
            $row->save();
            return $row->toArray();
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to create service');
        }
    }
}
