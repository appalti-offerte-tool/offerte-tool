<?php

class SupportTicket_Service_SupportTicket extends \OSDN_Application_Service_Dbable
{
    /**
     * @var SupportTicket_Model_DbTable_SupportTicket
     */
    protected $_table;
    protected $_fileStock;

    protected function _init()
    {
        $this->_table = new SupportTicket_Model_DbTable_SupportTicket($this->getAdapter());
        $this->_attachValidationRules('default', array(
            'serviceId'      => array('allowEmpty'          => false,   'presence'   => 'required'),
            'numberOfPages'  => array('int', 'allowEmpty'   => true,    'presence'   => 'required'),
            'amountOfPeople' => array('int','allowEmpty'    => true,    'presence'   => 'required')
        ));

        $this->_attachValidationRules('update', array(
            'serviceId'      => array('int', 'allowEmpty' => true),
            'numberOfPages'  => array('int', 'allowEmpty' => true),
            'amountOfPeople' => array('int', 'allowEmpty' => true),
        ), 'default');

        parent::_init();
    }


    protected function _countAndSubmitMinutes($data)
    {
        $wordCount = str_word_count($data['comment'], 0, '0123456789');
        $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();

        if ($data['wordCount'] != "") {
            $data['price'] = ceil($wordCount * ($data['price'] / $data['wordCount']));
        }

        if ($companyRow->supportMinutesAmount - $data['price'] < 0) {
            throw new OSDN_Exception('You have not minutes for this service');
        }

        $data['companyId'] = $companyRow->getId();
        $company = new Company_Model_DbTable_Company();

        $company->updateByPk(array('supportMinutesAmount' => $companyRow->supportMinutesAmount - $data['price']), $companyRow->id);

        return $data['companyId'];
    }


    protected function _findProposalAndUpdate($id)
    {
        $proposalService = new Proposal_Service_Proposal();

        $proposalRow = $proposalService->findOneByTicketId($id, false);
        if ($proposalRow != null)
        {
            $data = $proposalRow->toArray();
            $data['createSupportTicket'] = 0;
            $data['supportTicketId'] = null;

            $proposalService->update($proposalRow, $data);
        }

        return true;
    }

    public function find($id , $throwException = true)
    {
        $ticketRow = $this->_table->findOne($id);
        if (null === $ticketRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find ticket');
        }
        return $ticketRow;
    }

    public function create($data)
    {

        if (!isset($data['proposalId']))
        {
            $transfer = new \FileStock_Service_Transfer_Http();
            if ($transfer->isUploaded() == false && empty($data['comment'])) {
                throw new OSDN_Exception('You must upload file or write data');
            }

        }

         $data['companyId'] = $this->_countAndSubmitMinutes($data);

        $f = $this->_validate($data);
        $row = $this->_table->createRow($f->getData());
        $row->save();

        if (isset($data['proposalId'])) {
            $proposalService = new Proposal_Service_Proposal();
            $proposalRow = $proposalService->find($data['proposalId']);
            $proposalRow->supportTicketId = $row->id;
            $proposalService->update($proposalRow, $proposalRow->toArray());
        }
        $ticketData = $f->getData();
        $ticketData['supportTicketId'] = $row['id'];

        $table = new SupportTicket_Service_SupportTicketPost();
        $row = $table->create($ticketData);
        return $row;
    }

    public function fetchNewTicketsCount()
    {
        $select = $this->_table->getAdapter()->select()
            ->from($this->_table->getTableName(), array(
                'COUNT(id) as amount'
            ));


        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if ($accountRow->isTextWriter()) {
            $select
                ->where('writerId =?', $accountRow->getId())
                ->where('status = ?', 'pending');
        } else {
            $select->where('status = ?', 'new');
        }

        $result = $select->query()->fetch();
        return $result['amount'];
    }

    public function fetchAll(array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(array('t' => $this->_table->getTableName()), array(
                'id', 'accountId', 'companyId', 'serviceId', 'priority', 'status', 'createdDatetime', 'modifiedDatetime',
                'contactPersonId', 'writerId'
             ))
            ->join(
                array('c' => 'company'),
                't.companyId = c.id',
                array('name')
            )
            ->join(
                array('cp' => 'contactPerson'),
                't.contactPersonId = cp.id',
                array('lastname')
            )
            ->join(
                array('sr' => 'service'),
                ' t.serviceId = sr.id',
                array('serName'=> 'name')
             )
            ->joinLeft(
                array('ac'=> 'account'),
                't.writerId = ac.id',
                array('writerName' => 'fullname')
             )
            ->order('id ASC');
        $fields = array(
            't.title'       => 'title',
            'c.name'        => 'name',
            'cp.lastname'   => 'lastname',
            'sr.name'    => 'serName',
            'ac.fullname' => 'writerName'
        );
        $this->_initDbFilter($select, null, $fields)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

    public function update($id, $data)
    {
        $ticketRow = $this->find($id);
        $row = $ticketRow->toArray();

        try {
            $f = $this->_validate($data, 'update');
            $ticketRow->setFromArray($f->getData());
            $ticketRow->save();
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to update document');
        }

        if ($data['writerId'] != $row['writerId']) {
            try {
                $feedback = new SupportTicket_Misc_Email_WriterFeedback();
                $feedback->notificate(array('supportTicketId' => $id));
            } catch (\Exception $e) {
                throw new \OSDN_Exception($e->getMessage());
            }
        }

        return $ticketRow;
    }

    public function updateProposalTicketAndCreatePost($data)
    {
        $ticketRow = $this->find($data['ticketId']);

        try {

            if ($ticketRow->priority != $data['priority'])
            {
             $data['companyId'] = $this->_countAndSubmitMinutes($data);
            }

            $ticketRow->status == 'close' ? $data['status'] = 'new' :  $data['status'] =  $ticketRow->status;
            $ticketRow->setFromArray($data);
            $ticketRow->save();

            $data['supportTicketId'] = $data['ticketId'];

            $table = new SupportTicket_Service_SupportTicketPost();
            $table->create($data);

        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to update document');
        }
        return $ticketRow;
    }

    public function fetchAllWriterWithResponse(array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(array('r'=> 'aclRole'))
            ->join(array('ac' => 'account'),
            'r.id = ac.roleId',
            array('writerId' => 'id','writerName'=> 'fullname','email')
        )
            ->where("r.luid = 'textWriter'");

        $fields = array(
            'ac.fullname'   => 'fullname',
            'ac.email'      => 'email'
        );

        $this->_initDbFilter($select, $this->_table, $fields)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

    public function deleteAllByServiceId($id)
    {
        $tableTicketPost = new SupportTicket_Service_SupportTicketPost();
        try {
            $ticketRow = $this->_table->fetchAll(array('serviceId = ?'=> $id));
            foreach ($ticketRow as $row) {
                $tableTicketPost->deleteByTicketId($row['id']);
                $this->_findProposalAndUpdate($row['id']);
            }
            $this->_table->deleteQuote(array('serviceId = ?' => $id));
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to delete document');
        }
        return true;
    }

    public function delete($id)
    {
        $tableTicketPost = new SupportTicket_Service_SupportTicketPost();
        try {
            $ticketRow = $this->_table->find($id);
            foreach ($ticketRow as $row) {
                $tableTicketPost->deleteByTicketId($row['id']);
            }
            $this->_table->deleteQuote(array('id = ?' => $id));
            $this->_findProposalAndUpdate($id);
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to delete ticket');
        }
        return true;
    }
}
