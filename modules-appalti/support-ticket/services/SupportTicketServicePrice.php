<?php

class SupportTicket_Service_SupportTicketServicePrice extends \OSDN_Application_Service_Dbable
{
    protected $_table;
    protected $_fileStock;

    protected function _init()
    {
        $this->_table = new SupportTicket_Model_DbTable_SupportTicketServicePrice($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'membershipTypeId'    => array('allowEmpty' => false, 'presence'   => 'required'),
            'serviceId'       => array('allowEmpty' => false, 'presence'   => 'required'),
            'price'           => array('allowEmpty' => false),
            'discount'        => array('allowEmpty' => false),
        ));

        parent::_init();
    }

    public function find($id, $throwException = true)
    {

        $priceRow = $this->_table->findOne($id);
        if (null === $priceRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find  membership price');
        }
        return $priceRow;
    }

    public function findByParams($serviceId, $membershipTypeId)
    {
        try {
            $select = $this->_table->getAdapter()->select()
                ->from($this->_table->getTableName(), array('id'))
                ->where('serviceId = ?', $serviceId, Zend_Db::INT_TYPE)
                ->where('membershipTypeId = ?', $membershipTypeId, Zend_Db::INT_TYPE);
            
            $id = $select->query()->fetchColumn(0);

            if (empty($id)) {
                return false;
            }
            return $id;
            
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to find price');
        }
        return false;
    }

    public function fetchAllWithResponse(array $params = array())
    {

        $joinCondition =  empty($params['membershipId']) ? 'sp.serviceId = sr.id' : 'sp.serviceId = sr.id AND sp.membershipTypeId =' . $params['membershipId'];

        $select = $this->getAdapter()->select()
            ->from(array('sr'    => 'service'),
            array('id', 'name', 'desc')
        )
        ->joinLeft(
            array('sp'    => 'servicePrice'),
            $joinCondition,
            array('price', 'discount', 'membershipTypeId', 'wordCount', 'priceId' => 'id')
        )
        ->group('sr.id');
//        $serviceTable = new \SupportTicket_Model_DbTable_SupportTicketService();
        $this->_initDbFilter($select, $this->_table, array('name', 'desc', 'price', 'discount'))->parse($params);

        $response = $this->getDecorator('response')->decorate($select);

        $priorityService = new \SupportTicket_Service_ServicePricePriority();
        $serviceService = new \SupportTicket_Service_SupportTicketService();
        $response->setRowsetCallback(function($row) use ($priorityService, $serviceService) {
            $serviceRow = $serviceService->find($row['id']);
            $prioritySet = $priorityService->fetchAllByService($serviceRow);
            $row['servicePriority'] = array(
                \SupportTicket_Model_ServicePricePriorityRelRow::PRIORITY_LOW => $prioritySet[\SupportTicket_Model_ServicePricePriorityRelRow::PRIORITY_LOW],
                \SupportTicket_Model_ServicePricePriorityRelRow::PRIORITY_URGENT => $prioritySet[\SupportTicket_Model_ServicePricePriorityRelRow::PRIORITY_URGENT]
            );
            return $row;
        });

        return $response;

    }


    public function fetchAllByCompanyWithResponse(array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(array('mc'    => 'membershipCompany'),
            array('membershipId')
             )
            ->join(
                array('sp'    => 'servicePrice'),
                'mc.membershipId =sp.membershipTypeId',
                array('price', 'discount', 'membershipTypeId', 'serviceId', 'wordCount')
             )
            ->join(
                array('s'    => 'service'),
                's.id =sp.serviceId',
                array('id', 'name', 'desc')
             )
            ->where('mc.companyId =?', $params['companyId']);
        $serviceTable = new \SupportTicket_Model_DbTable_SupportTicketService();
        $this->_initDbFilter($select, $serviceTable, array('name', 'desc', 'price', 'discount'))->parse($params);
        return $this->getDecorator('response')->decorate($select);

    }

    public function update($id, $data)
    {
        $row = $this->find($id);
        $f = $this->_validate($data);

        $this->getAdapter()->beginTransaction();
        try {
            $row->setFromArray($f->getData());
            $row->save();
            $this->getAdapter()->commit();
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update membership price');
        }
        return $row;
    }

    public function create($data)
    {
        try {
            $f = $this->_validate($data);
            $row = $this->_table->createRow($f->getData());
            $row->save();
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to create membership price');
        }
        return $row;
    }

    public function bulk($data)
    {

        if (empty($data['servicePrice']) 
            || !\is_array($data['servicePrice'])
            || empty($data['servicePriority']) 
            || !\is_array($data['servicePriority'])) {
            return false;
        }

        $serviceService = new \SupportTicket_Service_SupportTicketService();
        $servicePricePriorityService = new \SupportTicket_Service_ServicePricePriority();
        $membershipService = new \Membership_Service_Membership();

        $this->getAdapter()->beginTransaction();
        try {

            $resp = $membershipService->fetchAllWithResponse();
            $memberships = $resp->getRowset();

            foreach ($data['servicePrice'] as $serviceId => $params) {
                $stService = $serviceService->find($serviceId);

                $servicePricePriorityService->bulk($stService, $data['servicePriority']);

                foreach ($memberships as $membership) {

                    $priceId = $this->findByParams($stService->getId(), $membership->getId());

                    $params['serviceId'] = $stService->getId();
                    $params['membershipTypeId'] = $membership->getId();

                    if (false === $priceId) {
                        $priceRow = $this->create($params);
                    } else {
                        $priceRow = $this->update($priceId, $params);
                    }

                }
            }

            $this->getAdapter()->commit();
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update membership price');
        }
        return true;
    }

    public function deleteByMembershipId($id)
    {
        try {
            $this->_table->deleteQuote(array('membershipTypeId = ?' => $id));
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to delete service');
        }
        return true;
    }

    public function deleteByServiceId($id)
    {
        try {
            $this->_table->deleteQuote(array('serviceId = ?' => $id));
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to delete service');
        }
        return true;
    }
}
