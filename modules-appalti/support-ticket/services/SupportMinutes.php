<?php

class SupportTicket_Service_SupportMinutes extends \OSDN_Application_Service_Dbable
{
    protected $_table;

    protected function _init()
    {
        $this->_table = new \SupportTicket_Model_DbTable_SupportMinutes($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'name'         => array('allowEmpty' => false, 'presence' => 'required'),
            'amount'       => array('allowEmpty' => false, 'presence' => 'required'),
            'price'        => array('allowEmpty' => false, 'presence' => 'required')
        ));

        parent::_init();
    }

    public function find($id, $throwException = true)
    {

        $serviceRow = $this->_table->findOne($id);
        if (null === $serviceRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find service');
        }
        return $serviceRow;
    }

    public function fetchAllWithResponse(array $params = array(), $returnModeRows = true)
    {
        $select = $this->_table->getAdapter()->select()
            ->from($this->_table->getTableName());
        $this->_initDbFilter($select, $this->_table)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);

        if (true === $returnModeRows) {
            $table = $this->_table;
            $response->setRowsetCallback(function($row) use ($table) {
                return $table->createRow($row);
            });
        }

        return $response;

    }

    public function update($id, $data)
    {
        $row = $this->find($id);
        $f = $this->_validate($data);

        $this->getAdapter()->beginTransaction();
        try {
            $row->setFromArray($f->getData());
            $row->save();
            $this->getAdapter()->commit();
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update support minutes');
        }
        return $row;
    }

    public function delete($id)
    {
        try {
            $row = $this->find($id);
            $row->delete();
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to delete support minutes');
        }
        return true;
    }

    public function create($data)
    {
        try {
            $f = $this->_validate($data);
            $row = $this->_table->createRow($f->getData());
            $row->save();
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to create support minutes');
        }
        return $row;
    }

}
