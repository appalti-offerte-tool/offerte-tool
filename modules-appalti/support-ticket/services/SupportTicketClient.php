<?php

class SupportTicket_Service_SupportTicketClient extends SupportTicket_Service_SupportTicket
{
    public function fetchAll( array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(array ('t' => $this->_table->getTableName()), array(
                '*',
                'companyName'               => 'c.name',
                'companyId'                 => 'c.id',
                'contactPersonFirstname'    => 'cp.firstname',
                'contactPersonPrefix'       => 'cp.prefix',
                'contactPersonLastname'     => 'cp.lastname',
                'serviceName'               => 'se.name'
            ))
            ->join(
                array ('cp' => 'contactPerson'),
                ' t.contactPersonId = cp.id',
                array()
            )
            ->join(
                array ('c' => 'company'),
                ' t.companyId = c.id',
                array()
            )
            ->join(
                array ('se' => 'service'),
                't.serviceId = se.id',
                array()
            )
//            ->order('IF (t.modifiedDatetime IS NOT NULL AND t.modifiedDatetime > t.createdDatetime, t.modifiedDatetime, t.createdDatetime) DESC');
            ->order('t.status');

        $accountRow = Zend_Auth::getInstance()->getIdentity();

        if ($accountRow->isProposalBuilder()) {
            $select->where('t.accountId = ?', $accountRow->getId(), Zend_Db::INT_TYPE);
        } else {
            $select->where('t.companyId = ?', $accountRow->getCompanyRow()->getId(), Zend_Db::INT_TYPE);
        }

//        if (!empty($params['filter']) && 'all' !== $params['filter']['value']) {
//            $select->where($params['filter']['field'] . ' = ?', $params['filter']['value']);
//        }

        if (!empty($params['writerId'])) {
            $select->where('writerId = ?', $params['writerId'], Zend_Db::INT_TYPE);
        }

        $fields = array(
            'se.name'           => 'serviceName',
            'cp.lastname'       => 'contactPersonLastname',
            't.createdDatetime' => 'createdDatetime',
            'c.name'            => 'companyName'
        );

        $this->_initDbFilter($select, $this->_table, $fields)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

    protected function _checkFiles($files)
    {
        if (empty($this->_fileStockSrv)) {
            $this->_fileStockSrv = new \FileStock_Service_FileStock(array(
                'baseFilePath'         => APPLICATION_PATH . '/modules-appalti/support-ticket/data/attachments',
                'useCustomExtension'   => false,
                'identity'             => 'support-ticket'
            ));
        }

        foreach($files as $k => $file) {
            $fileStockRow = $this->_fileStockSrv->find($file['fileStockId']);
            if ($fileStockRow) {
                try {
                    $fileContent = $this->_fileStockSrv->fetchFileContent($fileStockRow);
                } catch (\Exception $e) {
                    unset($files[$k]);
                }
            }
        }

        return $files;
    }

    public function fetchTicketDetails($ticketId, $page = 1, $limit = 5)
    {
        $db = $this->_table->getAdapter();

        $select = $db->select()
            ->from(array('t' => $this->_table->getTableName()))
            ->join(
                array('cp' => 'contactPerson'),
                'cp.id = t.contactPersonId',
                array(
                    'cpName' => "CONCAT(cp.firstname, ' ', cp.prefix, ' ', cp.lastname)",
                    'cpEmail'    => 'email'
                )
            )
            ->join(
                array('se' => 'service'),
                'se.id = t.serviceId',
                array(
                    'serviceName' => 'name'
                )
            )
            ->join(
                array('ac' => 'account'),
                't.accountId = ac.id',
                array(
                    'accountFirstname'  => 'firstname',
                    'accountPrefix'     => 'prefix',
                    'accountLastname'   => 'lastname',
                    'companyName'       => 'com.name',
                )
            )
            ->joinLeft(
            array('com' => 'company'),
            't.accountId = com.accountId || com.accountId = ac.parentId',
            array()
             )
            ->where('t.id = ?', $ticketId)
            ->where('t.companyId = ?', Zend_Auth::getInstance()->getIdentity()->getCompanyRow()->getId(), Zend_Db::INT_TYPE);


        $ticket = $select->query()->fetch();
        if (empty($ticket)) {
            return array();
        }

        $select = $db->select()
            ->from(
                array('supportTicketPost'),
                array('COUNT(id) as amount')
            )
            ->where('supportTicketId = ?', $ticketId);

        $result = $select->query()->fetch();
        $ticketPostsCount = $result['amount'];

        $select = $db->select()
            ->from(array('st' => 'supportTicketPost'))
            ->join(
                array('ac' => 'account'),
                'ac.id = st.accountId',
                array(
                    'senderName'       => "IF ((fullname = '' OR fullname IS NULL), CONCAT(firstname, ' ', prefix, ' ', lastname), fullname)",
                    'senderMail'       => 'email'
                )
            )
            ->where('st.supportTicketId = ?', $ticketId)
            ->limitPage($page, $limit)
            ->order('st.dateTime ASC');

        $ticketPosts = $select->query()->fetchAll();
        foreach ($ticketPosts as & $ticketPost) {
            $select = $db->select()
                ->from(array('stf' => 'supportTicketFileStockRel'))
                ->join(
                    array('f' => 'fileStock'),
                    'stf.fileStockId = f.id'
                )
                ->where('stf.ticketPostId = ?', $ticketPost['id']);

            $files = $select->query()->fetchAll();
            $files = $this->_checkFiles($files);
            $ticketPost['files'] = $files;
        }

        return array(
            'ticket'        => $ticket,
            'ticketPosts'   => $ticketPosts,
            'ticketPostsCount'   => $ticketPostsCount
        );
    }

    public function getFileStock($id)
    {
        $db = $this->_table->getAdapter();
        $select = $db->select()
            ->from(array('t' => $this->_table->getTableName()))
            ->join(array('tp' => 'supportTicketPost'), 'tp.supportTicketId = t.id')
            ->join(array('tpf' => 'supportTicketFileStockRel'), 'tpf.ticketPostId = tp.id')
            ->where('tpf.fileStockId = ?', $id)
            ->where('t.companyId = ?', Zend_Auth::getInstance()->getIdentity()->getCompanyRow()->getId(), Zend_Db::INT_TYPE);

        $row = $select->query()->fetch();
        if (empty($row)) {
            return false;
        }

        $fileStock = new FileStock_Service_FileStock();
        return $fileStock->find($id);
    }

}