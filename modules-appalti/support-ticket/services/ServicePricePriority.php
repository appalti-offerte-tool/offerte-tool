<?php

class SupportTicket_Service_ServicePricePriority extends \OSDN_Application_Service_Dbable
{
    protected $_table;
    protected $_fileStock;

    protected function _init()
    {
        $this->_table = new \SupportTicket_Model_DbTable_ServicePricePriorityRel($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'serviceId'       => array('allowEmpty' => false, 'presence'   => 'required'),
            'priority'           => array('allowEmpty' => false),
        ));

        parent::_init();
    }

    public function find($id, $throwException = true)
    {

        $priceRow = $this->_table->findOne($id);
        if (null === $priceRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find  membership price');
        }
        return $priceRow;
    }

    public function findByParams($serviceId, $priority)
    {
        try {
            $select = $this->_table->getAdapter()->select()
                ->from($this->_table->getTableName(), array('id'))
                ->where('serviceId = ?', $serviceId, Zend_Db::INT_TYPE)
                ->where('priority = ?', $priority);

            $id = $select->query()->fetchColumn(0);

            if (empty($id)) {
                return false;
            }
            return $id;

        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to find price');
        }
        return false;
    }

    public function fetchAllByService(\SupportTicket_Model_SupportTicketServiceRow $serviceRow)
    {
        $select = $this->_table->getAdapter()->select()
            ->from($this->_table->getTableName())
            ->where('serviceId = ?', $serviceRow->getId(), Zend_Db::INT_TYPE);

        $rowset = $select->query()->fetchAll();
        $result = array(
            \SupportTicket_Model_ServicePricePriorityRelRow::PRIORITY_LOW => 0,
            \SupportTicket_Model_ServicePricePriorityRelRow::PRIORITY_NORMAL => 0,
            \SupportTicket_Model_ServicePricePriorityRelRow::PRIORITY_URGENT => 0,
        );

        foreach ($rowset as $v) {
            $result[$v['priority']] = $v['percent'];
        }

        return $result;
    }

    public function update($id, $data)
    {
        $row = $this->find($id);
        $f = $this->_validate($data);

        $this->getAdapter()->beginTransaction();
        try {
            $row->setFromArray($f->getData());
            $row->save();
            $this->getAdapter()->commit();
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update priority relation');
        }
        return $row;
    }

    public function create($data)
    {
        try {
            $f = $this->_validate($data);
            $row = $this->_table->createRow($f->getData());
            $row->save();
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to create priority relation');
        }
        return $row;
    }

    public function bulk(\SupportTicket_Model_SupportTicketServiceRow $serviceRow, $data)
    {
        
        $serviceId = $serviceRow->getId();

        if (empty($data[$serviceId]) || !\is_array($data[$serviceId])) {
            return false;
        }

        $data = $data[$serviceId];
        $data[\SupportTicket_Model_ServicePricePriorityRelRow::PRIORITY_NORMAL] = 0;

        $this->getAdapter()->beginTransaction();
        try {

            foreach ($data as $key => $value) {
                $servicePriorityId = $this->findByParams($serviceId, $key);

                $params['serviceId'] = $serviceId;
                $params['priority'] = $key;
                $params['percent'] = $value;

                if (false === $servicePriorityId) {
                    $servicePriorityRow = $this->create($params);
                } else {
                    $servicePriorityRow = $this->update($servicePriorityId, $params);
                }
            }

            $this->getAdapter()->commit();
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update service priority');
        }
        return true;
    }

}
