<?php


class SupportTicket_Service_SupportTicketPost extends \OSDN_Application_Service_Dbable
{
    /**
     * @var SupportTicket_Model_DbTable_SupportTicketPost
     */
    protected $_table;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new SupportTicket_Model_DbTable_SupportTicketPost($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'serviceId'              => array('allowEmpty' => false, 'presence' => 'required'),
            'numberOfPages'          => array('int', 'allowEmpty' => true,'presence' => 'required')
        ));

        $this->_attachValidationRules('createAnswer', array(
            'data'                  => array('allowEmpty' => false, 'presence' => 'required'),
            'comment'              => array('allowEmpty' => false, 'presence' => 'required'),
        ));
        parent::_init();
    }

    public function find($id, $throwException = true)
    {
        $ticketPostRow = $this->_table->findOne($id);
        if (null === $ticketPostRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find ticket post #' . $id);
        }

        return $ticketPostRow;
    }

    public function answer($data)
    {
        try {
            $data['accountId'] = Zend_Auth::getInstance()->getIdentity()->getId();
            $data['data'] = trim($data['data']);
            $data['comment'] =trim($data['comment']);
            $f = $this->_validate($data, 'createAnswer');
            $row = $this->_table->createRow($f->getData());
            $row->save();
        }  catch(\Exception $e) {
            throw new OSDN_Exception('Unable to create answer');
        }
        return $row;
    }

    public function create($data, Zend_File_Transfer_Adapter_Abstract $transfer = null)
    {
        try {
            $data['accountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

            $row = $this->_table->createRow($data);
            $row->save();
            $stock = new SupportTicket_Service_SupportTicketPostFileStock();

            if (isset($data['path'])){
                    $stock->createWithExistingFiles($row, $data['path']);
            } else {
                $stock->create($row);
            }

            return ($row);
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to upload document');
        }
    }

    public function createAndSend($data)
    {
        try {
            $data['accountId'] = Zend_Auth::getInstance()->getIdentity()->getId();
            $data['data'] = trim($data['data']);
            $data['comment'] =trim($data['comment']);
            $f = $this->_validate($data, 'createAnswer');
            $row = $this->_table->createRow($f->getData());
            $row->save();
            $row = $row->toArray();
            $data['ticketId'] = $row['supportTicketId'];
            $data['ticketPostId'] = $row['id'];
            $feedback = new SupportTicket_Misc_Email_Feedback();
            $feedback->notificate(array ( 'id'  => $row['id'], 'supportTicketId' => $row['supportTicketId']));
        }  catch(\Exception $e) {

            throw new OSDN_Exception('Unable to create answer');
        }
        return $row;

    }

    public function fetchAllById($id, array $params = array())
    {
        try {
            $select = $this->_table->getAdapter()->select()
            ->from(array ('p' =>$this->_table->getTableName()), array ('id','accountId','dateTime','type','sendMail'))
                -> join(
                    array ('a' => 'account'  ),
                    'p.accountId = a.id',
                    array('username' ,'roleId')
                )
                ->where ('supportTicketId = ?', $id, Zend_Db::INT_TYPE)
                ->order('id ASC');
            $this->_initDbFilter($select,$this->_table)->parse($params);
            $response = $this->getDecorator('response')->decorate($select);
            return $response;
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to select ticket');
            return false;
        }
    }

    public function fetchRowById($id, array $params = array())
    {
        try {
            $select = $this->_table->getAdapter()->select()
            ->from(array ('p' =>$this->_table->getTableName())
            )
            -> join(
                array ('a' => 'account'  ),
                    'p.accountId = a.id',
                    array('username' ,'roleId')
            )
            ->where ('p.id = ?', $id, Zend_Db::INT_TYPE);
            $response = $this->getDecorator('response')->decorate($select);
            $documentFileStock = new SupportTicket_Service_SupportTicketPostFileStock();
            $response->setRowsetCallback(function($row) use ($documentFileStock) {
                $response = $documentFileStock->fetchAllFileStockBySupportTicketId($row['id']);
                $row['files'] = $response->getRowset();
                return $row;
            });
            return $response;
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to select ticket');
            return false;
        }
    }

    public function deleteByTicketId ($id)
    {
        $documentFileStock = new SupportTicket_Service_SupportTicketPostFileStock();
        $ticketPostRow = $this->_table->fetchAll(array('supportTicketId = ?' => $id));
        $ticketPostRow = $ticketPostRow->toArray();
        try {
            foreach ($ticketPostRow as $row) {
                $documentFileStock->deleteBySupportTicket($row['id']);
            }
            $this->_table->deleteQuote(array('supportTicketId = ?' => $id));
        } catch(\Exception $e) {
            echo $e;
            throw new \OSDN_Exception('Unable to delete post');
        }
        return true;
    }


    public function delete ($id)
    {
        $documentFileStock = new SupportTicket_Service_SupportTicketPostFileStock();
        $ticketPostRow = $this->_table->fetchAll(array('id = ?' => $id));
        $ticketPostRow = $ticketPostRow->toArray();
        try {
            foreach ($ticketPostRow as $row) {
                $documentFileStock->deleteBySupportTicket($row['id']);
            }
            $this->_table->deleteQuote(array('id = ?' => $id));
        } catch(\Exception $e) {
            echo $e;
            throw new \OSDN_Exception('Unable to delete post');
        }
        return true;
    }

    public function update($id, array $data)
    {
        $ticketPostRow = $this->find($id);
        $this->getAdapter()->beginTransaction();
        try {
            $ticketPostRow->setFromArray($data);
            $ticketPostRow->save();
            $this->getAdapter()->commit();
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update post');
        }
        return $ticketPostRow;
    }

    public function sendMail($data)
    {
        try {
            $data['sendMail']="yes";
            $data['accountId'] = Zend_Auth::getInstance()->getIdentity()->getId();
            $this->_table->updateByPk($data, $data['ticketPostId']);
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to update post');
        }
    }

}
