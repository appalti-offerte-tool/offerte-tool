<?php

class SupportTicket_Service_SupportTicketModerator extends SupportTicket_Service_SupportTicket
{

    protected $_clientAccountId;

    /**
     * @var \FileStock_Service_FileStock
     */
    protected $_fileStockSrv;

    protected function _init()
    {
        parent::_init();
        $this->_accountId = Zend_Auth::getInstance()->getIdentity()->getId();
    }

    public function fetchAll(array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(array('t' => $this->_table->getTableName()), array(
                '*',
                'userFirstname'    => 'ac.firstname',
                'userPrefix'       => 'ac.prefix',
                'userLastname'     => 'ac.lastname',
                'companyName'      => 'com.name',
                'serviceName'      => 'se.name',
                'writerId'
        ))
            ->join(
                array('cp' => 'contactPerson'),
                't.contactPersonId = cp.id',
                array()
        )
            ->join(
                array('se' => 'service'),
                't.serviceId = se.id',
                array()
        )
            ->join(
                array('ac' => 'account'),
                't.accountId = ac.id',
                array()
        )
            ->join(
                array('com' => 'company'),
                't.accountId = com.accountId || com.accountId = ac.parentId',
                array()
            )
            ->order('t.status');

        $params['filter'] = $this->getFilterParams();

        if (!empty($params['dashboardCustomFilter'])
                && \is_array($params['dashboardCustomFilter'])
                && isset($params['dashboardCustomFilter']['value'])
                && 'all' !== $params['dashboardCustomFilter']['value']) {
            $select->where($params['dashboardCustomFilter']['field'] . ' = ?', $params['dashboardCustomFilter']['value']);
        }

        $fields = array(
            'se.name'           => 'serviceName',
            'cp.lastname'       => 'contactPersonLastname',
            't.createdDatetime' => 'createdDatetime',
            'com.name'            => 'companyName'
        );

        $this->_initDbFilter($select, $this->_table, $fields)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);

        return $response;
    }

    protected function _checkFiles($files)
    {
        if (empty($this->_fileStockSrv)) {
            $this->_fileStockSrv = new \FileStock_Service_FileStock(array(
                'baseFilePath'         => APPLICATION_PATH . '/modules-appalti/support-ticket/data/attachments',
                'useCustomExtension'   => false,
                'identity'             => 'support-ticket'
            ));
        }

        foreach($files as $k => $file) {
            $fileStockRow = $this->_fileStockSrv->find($file['fileStockId']);
            if ($fileStockRow) {
                try {
                    $fileContent = $this->_fileStockSrv->fetchFileContent($fileStockRow);
                } catch (\Exception $e) {
                    unset($files[$k]);
                }
            }
        }

        return $files;
    }

    public function fetchTicketDetails($ticketId, $page = 1, $limit = 5)
    {
        $db = $this->_table->getAdapter();

        $select = $db->select()
            ->from(array('t' => $this->_table->getTableName()))
            ->join(
            array('ac' => 'account'),
            't.accountId = ac.id',
            array(
                'accountFirstname'  => 'firstname',
                'accountPrefix'     => 'prefix',
                'accountLastname'   => 'lastname',
                'companyName'       => 'com.name',
            )
        )
            ->join(
            array('cp' => 'contactPerson'),
            't.contactPersonId = cp.id',
            array('cpFirstname'=> 'firstname',
                  'cpLastname' => 'lastname',
                  'cpEmail'    => 'email')
        )
            ->join(
            array('se' => 'service'),
            'se.id = t.serviceId',
            array(
                'serviceName' => 'name'
            )
        )
            ->joinLeft(
            array('com' => 'company'),
            't.accountId = com.accountId || com.accountId = ac.parentId',
            array()
        )
            ->where('t.id = ?', $ticketId);
        $ticket = $select->query()->fetch();


        if ($ticket['writerId']!= null) {
            $account = new Account_Service_Account();
            $ticket['writerName'] = $account->find($ticket['writerId'])->getFullname();
        }

        $select = $db->select()
            ->from(
                array('supportTicketPost'),
                array('COUNT(id) as amount')
            )
            ->where('supportTicketId = ?', $ticketId);

        $result = $select->query()->fetch();
        $ticketPostsCount = $result['amount'];

        $select = $db->select()
            ->from(array('st' => 'supportTicketPost'))
            ->join(
                array('ac' => 'account'),
                'st.accountId = ac.id',
                array(
                    'senderName'  => "IF(ac.fullname = '', CONCAT(ac.firstname, ' ', ac.prefix, ' ', ac.lastname), ac.fullname)",
                    'senderMail'        => 'email',
                )
            )
            ->where('st.supportTicketId = ?', $ticketId)
            ->limitPage($page, $limit)
            ->order('st.dateTime ASC');

        $ticketPosts = $select->query()->fetchAll();
        foreach ($ticketPosts as &$ticketPost) {
            $select = $db->select()
                ->from(array('stf' => 'supportTicketFileStockRel'))
                ->join(
                array('f' => 'fileStock'),
                'stf.fileStockId = f.id'
            )
                ->where('stf.ticketPostId = ?', $ticketPost['id']);

            $files = $select->query()->fetchAll();
            $files = $this->_checkFiles($files);
            $ticketPost['files'] = $files;
        }
        return array(
            'ticket'        => $ticket,
            'ticketPosts'   => $ticketPosts,
            'ticketPostsCount'   => $ticketPostsCount
        );
    }

    public function fetchAllAssigned($data)
    {
        $select = $this->_table->getAdapter()->select()
            ->from(array('t' => $this->_table->getTableName()), array(
            '*',
            'userName' => "IF(ac.fullname = '', CONCAT(ac.firstname, ' ', ac.prefix, ' ', ac.lastname), ac.fullname)",
            'userFirstname' => 'ac.firstname',
            'userPrefix'    => 'ac.prefix',
            'userLastname'  => 'ac.lastname',
            'companyId'     => 'com.id',
            'companyName'   => 'com.name',
            'serviceName'   => 'se.name',
            'writerId'
        ))
            ->join(
            array('cp' => 'contactPerson'),
            't.contactPersonId = cp.id',
            array()
        )
            ->join(
            array('se' => 'service'),
            't.serviceId = se.id',
            array()
        )
            ->join(
            array('ac' => 'account'),
            't.accountId = ac.id',
            array()
        )
        ->joinLeft(
            array('com' => 'company'),
            't.accountId = com.accountId OR com.accountId = ac.parentId',
            array()
        )

        ->where('t.writerId = ?', !empty($data['writerId']) ? $data['writerId'] : $this->_accountId)
        ->order('IF (t.modifiedDatetime IS NOT NULL AND t.modifiedDatetime > t.createdDatetime, t.modifiedDatetime, t.createdDatetime) DESC');

        if (!empty($data['status'])) {
            $select->where('t.status =?', $data['status']);
        }

        $data['filter'] = $this->getFilterParams();

        if (!empty($data['dashboardCustomFilter'])
                && \is_array($data['dashboardCustomFilter'])
                && isset($data['dashboardCustomFilter']['value'])
                && 'all' !== $data['dashboardCustomFilter']['value']) {
            $select->where($data['dashboardCustomFilter']['field'] . ' = ?', $data['dashboardCustomFilter']['value']);
        }

        $fields = array(
            'se.name'           => 'serviceName',
            'cp.lastname'       => 'contactPersonLastname',
            't.createdDatetime' => 'createdDatetime',
            'com.name'          => 'companyName'
        );


        $this->_initDbFilter($select, $this->_table, $fields)->parse($data);
        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

    public function fetchAssignedByStatus($data,$status)
    {
        if (isset($data['writerId'])) {
            $id = $data['writerId'];
        } else {
            $id = $this->_accountId;
        }
        $select = $this->_table->getAdapter()->select()
            ->from(array('t' => $this->_table->getTableName()), array(
            '*',
            'userFirstname'    => 'ac.firstname',
            'userPrefix'       => 'ac.prefix',
            'userLastname'     => 'ac.lastname',
            'companyName'      => 'com.name',
            'serviceName'      => 'se.name',
            'writerId'
        ))
            ->join(
            array('cp' => 'contactPerson'),
            't.contactPersonId = cp.id',
            array()
        )
            ->join(
            array('se' => 'service'),
            't.serviceId = se.id',
            array()
        )
            ->join(
            array('ac' => 'account'),
            't.accountId = ac.id',
            array(

            )
        )
            ->joinLeft(
            array('com' => 'company'),
            't.companyId = com.id OR com.accountId = ac.parentId',
            array()
        )

            ->where('t.writerId = ?', $id)
            ->where('t.status =?',$status);

        $fields = array(
            'se.name'           => 'serviceName',
            'cp.lastname'       => 'contactPersonLastname',
            't.createdDatetime' => 'createdDatetime',
            'com.name'          => 'companyName'
        );


        $this->_initDbFilter($select, $this->_table, $fields)->parse($data);
        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }


    public function fetchAllAccountWithRoleWriterWithResponse(array $params = array())
    {

        $select = $this->getAdapter()->select()
            ->from(array('r'=> 'aclRole'))
            ->join(
                array('ac' => 'account'),
                'r.id = ac.roleId',
                array(
                    'accountId' => 'ac.id',
                    'fullname' => "IF ((ac.fullname = '' OR ac.fullname IS NULL), CONCAT(ac.firstname, ' ', ac.prefix, ' ', ac.lastname), ac.fullname)",
                    'email'
                )
            )
            ->where("r.luid = 'textWriter'");


        $fields = array(
            'ac.fullname'   => "fullname",
            'ac.email'      => 'email'
        );

        $this->_initDbFilter($select, $this->_table, $fields)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        return $response;


    }

    public function fetchWriterById($id)
    {
        $select = $this->getAdapter()->select()
            ->from('account')
            ->where('id = ?', $id, Zend_Db::INT_TYPE);

        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

    public function assignWriter($data)
    {
        $row = $this->find($data['supportTicketId']);
        if ($row == null) {
            throw new OSDN_Exception('Error! Cannot find ticket');
        }

        $dt = new \DateTime();
        $this->_table->updateByPk(
            array(
                'writerId'       => $data['writerId'],
                'status'         => 'pending',
                'pendingDatetime'=> $dt->format('Y-m-d H:i:s')
            ), $data['supportTicketId']
        );

        $feedback = new SupportTicket_Misc_Email_WriterFeedback();
        $feedback->notificate($data);
        return true;
    }


    public function changeStatus($data)
    {
        $dt = new \DateTime();
        $data['modifiedDatetime'] = $dt->format('Y-m-d H:i:s');
        return $this->_table->updateByPk( $data, $data['supportTicketId']);
    }


    public function getFileStock($id)
    {
        $db = $this->_table->getAdapter();
        $select = $db->select()
            ->from(array('t' => $this->_table->getTableName()))
            ->join(array('tp' => 'supportTicketPost'), 'tp.supportTicketId = t.id')
            ->join(array('tpf' => 'supportTicketFileStockRel'), 'tpf.ticketPostId = tp.id')
            ->where('t.accountId = ?', $this->_clientAccountId)
            ->where('tpf.fileStockId = ?', $id);

        $row = $select->query()->fetch();
        if (empty($row)) {
            return false;
        }

        $fileStock = new FileStock_Service_FileStock();
        return $fileStock->find($id);
    }


}