<?php

class SupportTicket_Service_SupportTicketPostFileStock extends \OSDN_Application_Service_Dbable
{
    /**
     * @var SupportTicket_Model_DbTable_SupportTicketFileStockRel
     */
    protected $_table;
    /**
     * @var FileStock_Service_FileStock
     */
    protected $_fileStock;

    private $__fileStockStorageCache;

    protected $_defaultAllowedFileExtensions;

    protected $_defaultAllowedFileTypes;

    public function _init()
    {
        $this->_table = new \SupportTicket_Model_DbTable_SupportTicketFileStockRel($this->getAdapter());

        $this->_fileStock = new \FileStock_Service_FileStock(array(
            'baseFilePath'         => APPLICATION_PATH . '/modules-appalti/support-ticket/data/attachments',
            'useCustomExtension'   => false,
            'identity'             => 'support-ticket'
        ));

        $configuration = new \FileStock_Service_MimeConfiguration('support-ticket');
        $this->_defaultAllowedFileExtensions = $configuration->fetchAllExtensions();
        $this->_defaultAllowedFileTypes = $configuration->fetchAllMimeTypes();
    }

    public function getDefaultAllowedFileExtensions()
    {
        return $this->_defaultAllowedFileExtensions;
    }

    public function find($ticketPostId, $fileStockId)
    {
        return $this->_fileStock->find($fileStockId);
    }

    public function fetchAllFileStockBySupportTicketId($ticketPostId)
    {
        $select = $this->_table->getAdapter()->select()
        ->from(
            array('sf'    => 'supportTicketFileStockRel'),
            array('ticketPostId')
        )
        ->join(
            array('f'    => 'fileStock'),
            'f.id = sf.fileStockId'
        )
        ->where('sf.ticketPostId = ?', $ticketPostId, Zend_Db::INT_TYPE);
        return $this->getDecorator('response')->decorate($select);
    }


    public function create(SupportTicket_Model_SupportTicketPostRow $ticketPostRow, Zend_File_Transfer_Adapter_Abstract $transfer = null)
    {
//        if ($transfer == null) {
//            return array();
//        }

        $transfer = new \FileStock_Service_Transfer_Http();

        if ($transfer->getFileName() != null) {
            $this->getAdapter()->beginTransaction();
            $savedFiles = array();
            try {
                $fileStockTable = new FileStock_Model_DbTable_FileStock();
                $transfer->addValidator(new Zend_Validate_File_Extension($this->getDefaultAllowedFileExtensions()));
                if (!$transfer->isValid() || !$transfer->isUploaded()) {
                    $e = new OSDN_Exception();
                    $e->assign($transfer->getMessages());
                    throw $e;
                }
                foreach ($transfer->getFileInfo() as $key => $value) {
                    $data = array();
                    $data['name'] = $value['name'];
                    $data['type'] = $value['type'];
                    $data['size'] = $value['size'];

                    $fileStockRow = $fileStockTable->createRow($data);
                    $fileStockRow->save();
                    $savedFiles[] = $fileStockRow;

                    $suportTicketFileStockRelationRow = $this->_table->createRow(array(
                        'fileStockId'      => $fileStockRow->getId(),
                        'ticketPostId'     => $ticketPostRow->getId()
                    ));
                    $suportTicketFileStockRelationRow->save();

                    $extension = pathinfo($value['name'], PATHINFO_EXTENSION);
                    $subFolder = floor($fileStockRow->getId() / 1000 + 1) * 1000 ;
                    $path = APPLICATION_PATH . '/modules-appalti/support-ticket/data/attachments/' . $subFolder;

                    if (!is_dir($path)) {
                        mkdir($path, 0775, true);
                        chmod($path, 0775);
                    }
                    $transfer->addFilter('Rename', array(
                        'target'    =>
                            $path
                            . '/'
                            . $fileStockRow->getId()
                            . '.'
                            . $extension,
                        'overwrite' => true
                    ),$key);

                    $transfer->receive($key);
                }
                $this->getAdapter()->commit();
            } catch (Exception $e) {
                foreach ($savedFiles as $fileStockRow) {
                    @unlink(
                        APPLICATION_PATH
                        . '/modules-appalti/support-ticket/data/attachments/'
                        . (floor($fileStockRow->getId() / 1000 + 1) * 1000)
                        . '/'
                        . $fileStockRow->getId()
                        . '.'
                        . pathinfo($fileStockRow->name, PATHINFO_EXTENSION)
                    );
                }

                $this->getAdapter()->rollBack();
                throw $e;
            }
            return array($ticketPostRow, $savedFiles);
        }
    }

    public function createWithExistingFiles(SupportTicket_Model_SupportTicketPostRow $ticketPostRow, array $filePath = array())
    {
       try {
           require_once APPLICATION_PATH . '/modules/file-stock/views/scripts/filters/File/Duplicate.php';
           $supportTicketFileStockRelation = new \SupportTicket_Model_DbTable_SupportTicketFileStockRel($this->getAdapter());
           foreach($filePath as $row){
                $rawFileTransfer = new FileStock_Service_Transfer_Raw();
                $rawFileTransfer->addFile(base64_decode($row));
                $rawFileTransfer->addFilter(new FileStock_View_Filter_File_Duplicate(base64_decode($row) . '.tmp', null, true));
                $data =  array();
                $supportTicketFileStockRelationRow = null;
                $this->_fileStock
                    ->create($rawFileTransfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
                use ($ticketPostRow, $supportTicketFileStockRelation, & $supportTicketFileStockRelationRow)
                {
                    $supportTicketFileStockRelationRow = $supportTicketFileStockRelation->createRow(array(
                        'fileStockId'  => $fileStockRow->getId(),
                        'ticketPostId' => $ticketPostRow->getId()
                    ));
                    $supportTicketFileStockRelationRow->save();
                });

            }
       } catch (\Exception $e) {
           throw $e;
       }
        return true;
    }

    public function delete($ticketId, $fileStockId)
    {
        $ticketRow = $this->_table->fetchRow(array('ticketPostId = ?' => $ticketId));
        if (empty($ticketRow)) {
            throw new OSDN_Exception('Unable to find Document #' . $ticketId);
        }
        try {
            $supportTicketFileStockRelation = new \SupportTicket_Model_DbTable_SupportTicketFileStockRel();
            $scope = $this->_fileStock;
            $callbackFn = function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
            use ($ticketRow, $supportTicketFileStockRelation, $scope, & $callbackFn)
            {
                $supportTicketFileStockRelationRow = $supportTicketFileStockRelation->fetchRow(array(
                    'id = ?'    => $ticketRow->getId(),
                    'fileStockId = ?'       => $fileStockRow->getId()
                ));
                if (null !== $supportTicketFileStockRelationRow) {
                    $supportTicketFileStockRelationRow->delete();
                }
            };
            $result = $this->_fileStock->delete($fileStockId, $callbackFn);
        } catch (\Exception $e) {
            throw $e;
        }
        return (boolean) $result;
    }

    public function deleteBySupportTicket( $ticketRowId)
    {
        $supportTicketFileStockRelation = new \SupportTicket_Model_DbTable_SupportTicketFileStockRel();
        $rowset = $supportTicketFileStockRelation->fetchAll(
                array( 'ticketPostId = ?'=> $ticketRowId)
        );
        foreach($rowset as $row) {
            $this->delete($ticketRowId, $row->fileStockId);
        }
        return true;
    }


    public function download($ticketPostId, $fileStockId, Zend_Controller_Response_Abstract $response, $isInline = false)
    {
        $fileStockRow = $this->find($ticketPostId, $fileStockId);
        $this->_fileStock->download(
            $fileStockRow,
            $response,
            true,
            function($fileStockRow, Zend_Controller_Response_Abstract $response) use ($isInline) {
                if (true === $isInline) {
                    $response->setHeader('Content-disposition', sprintf('inline; filename="%s"', $fileStockRow->getName()), true);
                }
            }
        );
    }

    public function update(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {
        $ticketPostRow = $this->_table->fetchRow(array('ticketPostId = ?' => $data['ticketPostId']));
        if (empty($ticketPostRow)) {
            throw new OSDN_Exception('Unable to find dmsDocument #' . $data['ticketPostId']);
        }
        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }
        $fileStockRow = $this->_fileStock->update($transfer, $data);
        return $fileStockRow;
    }

    /**
     * @return FileStock_Service_Storage_StorageInterface
     */
    public function toStorage()
    {
        if (null === $this->__fileStockStorageCache) {
            $this->__fileStockStorageCache = $this->_fileStock->factory();
        }
        return $this->__fileStockStorageCache;
    }
}