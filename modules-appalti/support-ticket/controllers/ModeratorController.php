<?php

class SupportTicket_ModeratorController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var SupportTicket_Service_SupportTicketModerator
     */
    protected $_service;

    public function init()
    {
        $this->_accountRow = Zend_Auth::getInstance()->getIdentity();

        $this->_service = new SupportTicket_Service_SupportTicketModerator();
        parent::init();

        $this->_helper->contextSwitch()
            ->addActionContext('create', 'json')
//            ->addActionContext('assign-writer', 'json')
            ->addActionContext('change-status', 'json')
            ->initContext();

    }

    public function getResourceId()
    {
        return 'support-ticket';
    }

    public function indexAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(20);

        $response = $this->_service->fetchAll($this->_getAllParams());
        $this->view->rowset = $response->getRowset();

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }

    public function viewPostsAction()
    {
        $pagination = new \OSDN_Pagination();
        $pagination->setItemsCountPerPage(5);

        $result = $this->_service->fetchTicketDetails($this->_getParam('ticketId'), $this->_getParam('page', 1));
        $this->view->assign($result);
        $this->view->ticketId = $this->_getParam('ticketId');

        $pagination->setTotalCount($result['ticketPostsCount']);
        $this->view->pagination = $pagination;
    }

    public function ticketDetailsAction()
    {
        $this->view->assign($this->_service->fetchTicketDetails($this->_getParam('supportTicketId')));
    }

    public function assignWriterAction()
    {
        if ($this->getRequest()->isPost()) {
            try {
                $this->_service->assignWriter($this->_getAllParams());
                $this->view->success = true;
                $this->view->assign($this->_service->fetchTicketDetails($this->_getParam('supportTicketId')));
                $response = $this->_service->fetchWriterById($this->_getParam('writerId'));
                $this->view->writer = current($response->getRowset());
            } catch (\OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessage());
            }

            $this->render('assign-result');
        } else {
            $this->view->assign($this->_service->fetchTicketDetails($this->_getParam('supportTicketId')));
        }
    }

    public function changeStatusAction()
    {
        try {
            $this->_service->changeStatus($this->_getAllParams());
            $this->_helper->information('Ticket status successfully changed.', null, E_USER_NOTICE);
        } catch (\Exception $e) {
            $this->_helper->information(array(
                'Error updating support ticket status. Error: %s',
                array($e->getMessage())
            ), false);
        }

        $this->_redirect('/support-ticket/moderator/assign-writer/supportTicketId/' . $this->_getParam('supportTicketId'));
    }


    public function downloadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $fileStockRow = $this->_service->getFileStock($this->_getParam('id'));
        $dfs = new SupportTicket_Service_SupportTicketPostFileStock();
        $dfs->download('huy znae sho blya', $this->_getParam('id'), $this->getResponse());
    }

}
