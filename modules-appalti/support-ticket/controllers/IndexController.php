<?php

class SupportTicket_IndexController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var SupportTicket_Service_SupportTicket
     */
    protected $_service;

    public function init()
    {

        $this->_helper->ajaxContext()
            ->addActionContext('fetch-price', 'json')
            ->initContext();
        $this->_service = new SupportTicket_Service_SupportTicket();
        parent::init();

    }

    public function getResourceId()
    {
        return 'support-ticket:company';
    }

    public function indexAction()
    {
        if ($this->getRequest()->isPost()) {
            $this->view->formSubmittedPostData = $this->getRequest()->getParams();
            try {
                $params = $this->_getAllParams();
                $this->_service->create($params);
                $this->_helper->information('Completed successfully', array(), E_USER_NOTICE);
                $this->_redirect('/support-ticket/index/');
            } catch (\OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
                $this->render('index');
            }
        }
    }

    /**
     * @return price for current service
     *
     * @param serviceId
     */
    public function fetchPriceAction()
    {
        try {
            $service = new SupportTicket_Service_SupportTicketService();
            $response = $service->fetchPrice($this->_getAllParams());

            if (null !== $response) {
                $this->view->success = true;
                $this->view->assign($response);
            } else {
                $this->view->success = false;
                $this->_helper->information('De prijs is niet gedefinieerd voor deze dienst.');
            }
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }

    }

}
