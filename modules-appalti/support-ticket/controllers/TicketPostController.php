<?php

class SupportTicket_TicketPostController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var SupportTicket_Service_SupportTicketPost
     */
    protected $_service;

    public function init()
    {
        parent::init();

        $this->_helper->ajaxContext()
            ->addActionContext('create-answer', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('fetch-all', 'json')
            ->initContext();
        $this->_service = new SupportTicket_Service_SupportTicketPost();
    }

    public function getResourceId()
    {
        return 'support-ticket';
    }

    public function fetchAllAction()
    {
        try {
            $ticketId = $this->_getParam('ticketId');
            $ticketRow = $this->_service->fetchAllById($ticketId ,$this->_getAllParams());
            $this->view->assign($ticketRow->toArray());
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $ticketPostId = $this->_getParam('ticketPostId');
            $data = $this->_service->delete($ticketPostId);
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }

    }

    public function fetchRowAction()
    {
        try {
            $ticketPostId = $this->_getParam('ticketPostId');
            $data = $this->_service->fetchRowById($ticketPostId);
            $this->view->assign($data->toArray());
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function createAnswerAction()
    {
        try {
            $params = $this->_getAllParams();
            $response = $this->_service->answer($params);
            $this->view->id = $response->getId();
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        }catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        try {
            $this->_service->update($this->_getParam('ticketPostId'), $this->_getAllParams());
            $this->view->id =$this->_getParam('ticketPostId');
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }
}