<?php

class SupportTicket_ServicePriceMainController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var SupportTicket_Service_SupportTicketServicePrice
     */

    protected $_service;

    protected $_rezerv;

    public function init()
    {
        parent::init();

        $this->_helper->ajaxContext()
            ->addActionContext('edit', 'json')
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch-all-by-company', 'json')
            ->initContext();

        $this->_service = new SupportTicket_Service_SupportTicketServicePrice();

        $this->_rezerv = new SupportTicket_Service_SupportTicketService();
    }

    public function getResourceId()
    {
        return 'support-ticket';
    }


    public function fetchAllAction()
    {
        try {
            $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
            $this->view->assign($response->toArray());
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }


    public function editAction()
    {
        try {

            $priceId = $this->_getParam('priceId', false);

            if (empty($priceId)) {
                $priceRow = $this->_service->create($this->_getAllParams());
            } else {
                $price = $this->_service->find($priceId);
                $priceRow = $this->_service->update($price->getId(), $this->_getAllParams());
            }

            $this->view->priceId = $priceRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAllByCompanyAction()
    {
        try {
            $response = $this->_service->fetchAllByCompanyWithResponse($this->_getAllParams());
            $this->view->assign($response->toArray());
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }

    }

}