<?php

final class SupportTicket_FeedbackController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    public function init()
    {
        $this->_helper->contextSwitch()
            ->addActionContext('send', 'json')
            ->addActionContext('create-and-send', 'json')
            ->addActionContext('email', 'json')
            ->initContext();
    }

    public function getResourceId()
    {
        return 'guest';
    }

    public function indexAction()
    {
        $this->_helper->layout()->setLayout('guest');
    }

    public function sendAction()
    {
        $servicePost = new SupportTicket_Service_SupportTicketPost();
        $service = new SupportTicket_Service_SupportTicket();

        $ticket = $service->find($this->_getParam('ticketId'), true);
        $ticketPost = $servicePost->find($this->_getParam('ticketPostId'), true);

        try {

            if ($ticket->accountId != $ticketPost->accountId) {

                $feedback = new SupportTicket_Misc_Email_Feedback();
                $params = array('id'=> $this->_getParam('ticketPostId'),
                                'supportTicketId'=> $this->_getParam('ticketId'));

            }  else {

                $feedback = new SupportTicket_Misc_Email_WriterFeedback();
                $params = array('id'=> $this->_getParam('ticketPostId'),
                                'supportTicketId'=> $this->_getParam('ticketId'));
            }


            $feedback->notificate($params);
            $this->_helper->information('Message has been send successfully', true, E_USER_NOTICE);
            $this->view->success = true;
        } catch(OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function createAndSendAction()
    {
        $servicePost = new SupportTicket_Service_SupportTicketPost();

        try {
            $this->view->success = true;
            $servicePost->createAndSend($this->_getAllParams());
        } catch(OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function formAction()
    {
        $this->_accountRow = Zend_Auth::getInstance()->getIdentity();
        $contactPersonRow = $this->_accountRow->getContactPersonRow(false);
        if (!($companyRow = $this->_accountRow->getCompanyRow(false)))
        {
            $companyRow = null;
        }

//        $this->view->contactPersonId = $contactPersonRow->getId();
        $this->view->accountRow = $this->_accountRow;
        $this->view->contactPersonRow = $contactPersonRow;
        $this->view->companyRow = $companyRow;

        if ($this->_request->isXmlHttpRequest()) {
            $this->_helper->layout()->disableLayout();
        }
    }

    public function emailAction()
    {
        if ($this->getRequest()->isPost())
        {
            $params = $this->getRequest()->getParams();
            $formValues = $params['email'];
            if(($accountRow = Zend_Auth::getInstance()->getIdentity()) !== null)
            {
                $contactPersonRow = $accountRow->getContactPersonRow(false);
                $companyRow = $accountRow->getCompanyRow(false);
            }

            // send email to backoffice
            // - create emailbody
            $view = new Zend_View();
            $view->assign('companyName', $formValues['company_name']);
            $view->assign('contactpersonName', $formValues['contactperson_name']);
            $view->assign('contactpersonPhone', $formValues['contactperson_phonenr']);
            $view->assign('contactpersonEmail', $formValues['contactperson_emailaddress']);
            $view->assign('subject', $formValues['subject']);
            $view->assign('message', $formValues['message']);
            if ($accountRow !== null)
            {
                $view->assign('accountRow', $accountRow);
            }
            if ($contactPersonRow !== null)
            {
                $view->assign('contactpersonRow', $contactPersonRow);
            }
            if ($companyRow !== null)
            {
                $view->assign('companyRow', $companyRow);
            }
            $view->addScriptPath(dirname(__FILE__).'/../views/scripts/default/');
            $body = $view->render('feedback/email/index.phtml');

            // - send email
            $mail = new Zend_Mail();
            $mail->setSubject('Offertemaken.com account aanvraag');
            $mail->addTo('nhofs@appalti.nl', 'Admin Offertetool');
//            $mail->addTo('elanenga@appalti.nl', 'Admin Offertetool :: vraag/opmerking');
            $mail->setBodyHtml($body);
            $mail->send();

            $this->_forward('email-sent');
        }
    }

    public function emailSentAction()
    {

    }
}