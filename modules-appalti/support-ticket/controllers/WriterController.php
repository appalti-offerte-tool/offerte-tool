<?php

class SupportTicket_WriterController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var SupportTicket_Service_SupportTicketModerator
     */
    protected $_service;

    /**
     * @var Account_Model_DbTable_AccountRow
     */
    protected $_accountRow;

    public function init()
    {
        $this->_accountRow = Zend_Auth::getInstance()->getIdentity();

        $this->_service = new SupportTicket_Service_SupportTicketModerator();
        parent::init();

        $this->_helper->contextSwitch()
             ->addActionContext('fetch-all', 'json')
             ->addActionContext('create', 'json')
             ->initContext();

    }

    public function getResourceId()
    {
        return 'support-ticket:writer';
    }

    public function indexAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(20);

        $response = $this->_service->fetchAllAssigned($this->_getAllParams());
        $this->view->rowset = $response->getRowset();

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }

    public function viewPostsAction()
    {
        $pagination = new \OSDN_Pagination();
        $pagination->setItemsCountPerPage(5);

        $result = $this->_service->fetchTicketDetails($this->_getParam('ticketId'), $this->_getParam('page', 1));
        $this->view->assign($result);
        $this->view->ticketId = $this->_getParam('ticketId');

        $pagination->setTotalCount($result['ticketPostsCount']);
        $this->view->pagination = $pagination;
    }

    public function createPostAction()
    {
        try {
            $supportTicketPostSrv = new SupportTicket_Service_SupportTicketPost();
            $row = $supportTicketPostSrv->create($this->_getAllParams());
            $ticketPostData = $row->toArray();
            $feedback = new SupportTicket_Misc_Email_Feedback();
            $feedback->notificate($ticketPostData);

            $close = $this->_getParam('saveAndClose');
            $reopen = $this->_getParam('saveAndReopen');
            if ($close || $reopen) {
                $stSrv = new \SupportTicket_Service_SupportTicket();
                $stSrv->update($this->_getParam('supportTicketId'), array(
                    'writerId' => $this->_accountRow->getId(),
                    'status' => $close ? 'close' : 'pending'
                ));
            }

            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }

        $this->_redirect('/support-ticket/writer/view-posts/ticketId/' . $this->_getParam('supportTicketId'));
    }


    public function changeStatusAction()
    {
        $this->_service->changeStatus($this->_getAllParams());
        $this->_redirect('/');
    }

    public function downloadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $fileStockRow = $this->_service->getFileStock($this->_getParam('id'));
        $dfs = new SupportTicket_Service_SupportTicketPostFileStock();
        $dfs->download('huy znae sho blya', $this->_getParam('id'), $this->getResponse());
    }

}
