<?php

/**
 * Description
 *
 * @category
 * @author     Kudrinsky Vitaly <vkudrinsky@gmail.com>
 * @version    SVN: $Id: $
 */
class SupportTicket_ServicePriceController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \SupportTicket_Service_SupportTicketServicePrice
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();

        $this->_helper->ajaxContext()
            ->addActionContext('bulk', 'json')
            ->initContext();


        $this->_service = new \SupportTicket_Service_SupportTicketServicePrice();
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'support-ticket:manage-service-price-list';
    }

    public function indexAction()
    {
        $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
        $this->view->assign($response->toArray());
    }

    public function bulkAction()
    {
        try {
            $res = $this->_service->bulk($this->_getAllParams());
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
            $this->view->success = $res;
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

}