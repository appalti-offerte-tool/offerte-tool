<?php

class SupportTicket_TicketFileStockController extends \Zend_Controller_Action implements \FileStock_Instance_EntityFileStockControllerInterface
{
   /**
    * @var SupportTicket_Service_SupportTicketPostFileStock
    */
    protected $_service;

    public function getResourceId()
    {
        return 'guest';
    }
    public function init()
    {
        $this->_service = new \SupportTicket_Service_SupportTicketPostFileStock($this->getEntityType());

        $this->_helper->contextSwitch()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->initContext();

        parent::init();
    }


    public function getEntityType()
    {
        return 'support-ticket';
    }

    public function fetchAllAction()
    {
        try {
            $response = $this->_service->fetchAllFileStockBySupportTicketId($this->_getParam('ticketPostId'));
            $this->view->assign($response->toArray());
            $this->view->success = true;
        } catch(OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAction()
    {

        $fileStockRow = $this->_service->find($this->_getParam('ticketPostId'), $this->_getParam('fileStockId'));
        $this->view->data = $fileStockRow->toArray();
        $this->view->success = true;
    }

    public function createAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');
        try {
            $service = new SupportTicket_Service_SupportTicketPost();
            $ticketPostRow = $service->find($this->_getParam('ticketPostId'));

            list($ticketPostRow, $fileStockRow) = $this->_service->create($ticketPostRow);

            $this->view->success = true;
            $this->view->ticketPostId = $ticketPostRow->getId();

            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);
        } catch(OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');
        try {
            $this->_service->update($this->_getAllParams());
            $this->view->success = true;
            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);
        } catch(OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->_service->delete($this->_getParam('ticketPostId'), $this->_getParam('fileStockId'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }

    public function downloadAction()
    {
        $this->_service->download(
                $this->_getParam('ticketPostId'), $this->_getParam('fileStockId'),
                $this->getResponse()
        );
    }

    public function viewInlineAction()
    {
        $this->_service->download(
                $this->_getParam('ticketId'), $this->_getParam('fileStockId'),
                $this->getResponse(),
                true
        );
    }
}