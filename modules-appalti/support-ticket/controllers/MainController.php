<?php

class SupportTicket_MainController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var SupportTicket_Service_SupportTicket
     */
    protected $_service;
    public function init()
    {


        $this->_helper->ajaxContext()
            ->addActionContext('edit', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch-row', 'json')
            ->initContext();

        $this->_service = new SupportTicket_Service_SupportTicket();
        parent::init();
    }

    public function getResourceId()
    {
        return 'support-ticket:list';
    }

    public function indexAction()
    {
    }

    public function fetchAllAction()
    {
        try {
            $response = $this->_service->fetchAll($this->_getAllParams());
            $this->view->assign($response->toArray());
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }


    public function fetchRowAction()
    {
        try {
            $ticket = $this->_service->find($this->_getParam('ticketId'));
            $this->view->row = $ticket->toArray();
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function editAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $service = new SupportTicket_Service_SupportTicket();
        try {
            $ticketRow = $service->update($this->_getParam('ticketId'), $this->_getAllParams());
            $this->view->id =$ticketRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->view->success = $this->_service->delete($this->_getParam('id'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function comboBoxAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');
        $response = $this->_service->fetchAllWriterWithResponse($this->_getAllParams());
        $this->view->rowset = $response->getRowset();
    }
}