<?php

class SupportTicket_ClientController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var SupportTicket_Service_SupportTicketClient
     */
    protected $_service;

    /**
     * @var Account_Model_DbTable_AccountRow
     */
    protected $_accountRow;

    public function init()
    {
        $this->_accountRow = Zend_Auth::getInstance()->getIdentity();

        $this->_service = new SupportTicket_Service_SupportTicketClient();
        parent::init();

        $this->_helper->contextSwitch()
             ->addActionContext('create', 'json')
             ->addActionContext('create-proposal', 'json')
             ->initContext();
    }

    public function getResourceId()
    {
        return 'support-ticket:company';
    }

    public function indexAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(20);

        $response = $this->_service->fetchAll($this->_getAllParams());
        $this->view->rowset = $response->getRowset();

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }

    public function viewPostsAction()
    {
        $pagination = new \OSDN_Pagination();
        $pagination->setItemsCountPerPage(5);

        $result = $this->_service->fetchTicketDetails($this->_getParam('ticketId'), $this->_getParam('page', 1));
        $this->view->assign($result);
        $this->view->ticketId = $this->_getParam('ticketId');

        $pagination->setTotalCount($result['ticketPostsCount']);
        $this->view->pagination = $pagination;
    }

    public function createPostAction()
    {
        try {
            $supportTicketPostSrv = new SupportTicket_Service_SupportTicketPost();
            $row = $supportTicketPostSrv->create($this->_getAllParams());
            $ticketPostData = $row->toArray();

            $ticketSrv = new \SupportTicket_Service_SupportTicket();
            $ticketRow = $ticketSrv->find($this->_getParam('supportTicketId'));

            if (null !== $ticketRow->writerId) {
                $feedback = new SupportTicket_Misc_Email_WriterFeedback();
                $feedback->notificate($ticketPostData);

                $close = $this->_getParam('saveAndClose');
                $reopen = $this->_getParam('saveAndReopen');
                if ($close || $reopen) {
                    $ticketSrv->update($this->_getParam('supportTicketId'), array(
                        'writerId' => $ticketRow->writerId,
                        'status' => $close ? 'close' : 'pending'
                    ));
                }
            }

            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }

        $this->_redirect('/support-ticket/client/view-posts/ticketId/' . $this->_getParam('supportTicketId'));
    }

    public function formAction()
    {
        $contactPersonRow = $this->_accountRow->getContactPersonRow();
        $companyRow = $this->_accountRow->getCompanyRow();

//        $this->view->contactPersonId = $contactPersonRow->getId();
        $this->view->accountRow = $this->_accountRow;
        $this->view->contactPersonRow = $contactPersonRow;
        $this->view->companyRow = $companyRow;
        $this->view->credits = $companyRow->supportMinutesAmount;

        if ($this->_request->isXmlHttpRequest()) {
            $this->_helper->layout()->disableLayout();
        }
    }

    public function proposalAction()
    {
        $service = new SupportTicket_Service_SupportTicketService();
        $service = $service->findByLuid('checkProposal');

        if (null === $service) {
            return;
        }

        $row['proposalId'] = $this->_getParam('proposalId');
        $row['serviceId'] = $service['id'];
        $row['serviceName'] = $service['name'];

        $proposalService  = new \Proposal_Service_Proposal();
        $proposalRow = $proposalService->find($this->_getParam('proposalId'), true);
        $contactPersonRow = $proposalRow->getContactPersonRow();

        $row['contactPersonFullName'] = $contactPersonRow->getFullname();
        $row['contactPersonId'] = $contactPersonRow->getId();

        $service = new Proposal_Service_Prince($proposalRow);
        $path = $service->generate(null, false);

        $directory = array();

        if(is_dir($path)) {
            $files = scandir($path);
            array_shift($files);
            array_shift($files);
            for($i = 0; $i < sizeof($files); $i++) {
                $directory[$i]['link'] = base64_encode($path . '/' . $files[$i]);
                $directory[$i]['name'] = $files[$i];
            }
        }

        $row['files'] = $directory;

        $companyRow = $this->_accountRow->getCompanyRow();
        $this->view->ticketId =  $proposalRow->supportTicketId;

        $this->view->companyRow = $companyRow;
        $this->view->credits =$companyRow->supportMinutesAmount;
        $this->view->row = $row;
    }

    public function createAction()
    {
        if ($this->getRequest()->isPost()) {
            $this->getResponse()->setHeader('Content-type', 'text/html', true);
            try {
                $params = $this->_getAllParams();

                if (isset($params['ticketId'])) {
                    $row = $this->_service->updateProposalTicketAndCreatePost($params);
                } else {
                    $row = $this->_service->create($params);
                }

                $this->view->row = $row;
                $this->view->success = true;
                if (!$this->getRequest()->isXmlHttpRequest()) {
                    $this->_helper->information('Support vraag aangemaakt succesvol.', true, E_USER_NOTICE);
                }
            } catch(\OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessage());
            }
        }
    }

    public function downloadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $fileStockRow = $this->_service->getFileStock($this->_getParam('id'));
        $dfs = new SupportTicket_Service_SupportTicketPostFileStock();
        $dfs->download('huy znae sho blya', $this->_getParam('id'), $this->getResponse());
    }

}
