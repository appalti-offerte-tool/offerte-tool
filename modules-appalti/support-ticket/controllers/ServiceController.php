<?php


class SupportTicket_ServiceController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var SupportTicket_Service_SupportTicketService
     */
    protected $_service;

    public function init()
    {
        parent::init();

        $this->_helper->ajaxContext()
            ->addActionContext('delete', 'json')
            ->addActionContext('edit', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('fetch-row', 'json')
            ->addActionContext('fetch-all', 'json')
            ->initContext();

        $this->_service = new SupportTicket_Service_SupportTicketService();
    }

    public function getResourceId()
    {
        return 'support-ticket';
    }

    public function indexAction()
    {

    }

    public function fetchAllAction()
    {
        try {
            $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
            $this->view->assign($response->toArray());
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchRowAction()
    {
        try {
            $response = $this->_service->find($this->_getParam('serviceId'));
            $this->view->row = $response->toArray();
            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->view->success = $this->_service->delete($this->_getParam('id'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function editAction()
    {
        try {
            $this->_service->update($this->_getParam('serviceId'), $this->_getAllParams());
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function createAction()
    {
        try {
            $params = $this->_getAllParams();
            $responce = $this->_service->create($params);
            $this->view->id = $responce['id'];
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        }catch(\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    /**
     * @return \SupportTicket_Service_SupportTicketService
     */
    public function getService()
    {
        return $this->_service;
    }
}