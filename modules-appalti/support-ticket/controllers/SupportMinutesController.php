<?php

class SupportTicket_SupportMinutesController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \SupportTicket_Service_SupportMinutes
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();

        $this->_helper->ContextSwitch()
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->initContext();


        $this->_service = new \SupportTicket_Service_SupportMinutes();
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'support-ticket:manage-list';
    }

    public function indexAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(20);

        $accountRow = Zend_Auth::getInstance()->getIdentity();

        $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
        $this->view->assign($response->toArray());

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }

    public function createAction()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->render('form');
        }

        try {
            $this->view->formSubmittedPostData = $this->getRequest()->getParams();
            $supportMinutesRow = $this->_service->create($this->_getAllParams());

            $this->view->supportMinutesId = $supportMinutesRow->getId();
            $this->view->success = true;
            $this->_helper->information(array('%s is gemaakt.', array($supportMinutesRow->name)), true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch(\Exception $e) {
            $this->view->success = false;
            $this->_helper->information('Kan te maken support minutes.');
        }
    }

    public function updateAction()
    {
        $supportMinutesRow = $this->_service->find($this->_getParam('supportMinutesId'), false);
        if (null === $supportMinutesRow) {
            return $this->_redirect('/support-ticket/support-minutes/');
        }

        $this->view->supportMinutesRow = $supportMinutesRow;

        if (!$this->getRequest()->isPost()) {
            return $this->render('form');
        }

        try {
            $this->view->formSubmittedPostData = $this->getRequest()->getParams();
            $supportMinutesRow = $this->_service->update($supportMinutesRow->getId(), $this->_getAllParams());
            $this->view->supportMinutesId = $supportMinutesRow->getId();
            $this->view->success = true;
            $this->_helper->information(array('%s is bijgewerkt.', array($supportMinutesRow->name)), true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch(\Exception $e) {
            $this->view->success = false;
            $this->_helper->information('Niet werken support minutes.');
        }
    }

   public function deleteAction()
    {
        $supportMinutesId = $this->_getParam('supportMinutesId');

        try {
            $supportMinutesRow = $this->_service->find($supportMinutesId);
            $name = $supportMinutesRow->name;
            $this->_service->delete($supportMinutesRow->getId());
            $this->_helper->information(array('%s is verwijderd.', array($name)), true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

}