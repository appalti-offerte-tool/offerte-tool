<?php

class SupportTicket_View_Helper_TextWriterAccountsComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    public function __construct()
    {
        $service = new SupportTicket_Service_SupportTicketModerator();
        $response = $service->fetchAllAccountWithRoleWriterWithResponse();

        $options = array(
            '' => 'Naam van de tekstschrijver'
        );
        $response->getRowset(function($row) use (& $options) {
            $options[$row['accountId']] = $row['fullname'];
            return false;
        });

        $this->_options = $options;
    }

    public function textWriterAccountsComboBox()
    {
        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }
}