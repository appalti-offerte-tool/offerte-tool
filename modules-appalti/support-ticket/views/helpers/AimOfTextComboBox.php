<?php

class SupportTicket_View_Helper_AimOfTextComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    public function __construct()
    {
        $options = array(
            'info'          => 'Informatie',
            'revitalize'    => 'Revitaliseren',
            'proposal'      => 'Interesse opwekken'
    )   ;

        $this->_options = $options;
    }

    public function aimOfTextComboBox()
    {
        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }
}