<?php

class SupportTicket_View_Helper_ServiceComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    public function __construct()
    {
        $service = new SupportTicket_Service_SupportTicketService();
        $response = $service->fetchAllByMembershipWithResponse();

        $options = array();

        $response->getRowset(function($row) use (& $options) {
            $options[$row['id']] = $row['name'];
            return false;
        });
        $this->_options = $options;
    }

    public function serviceComboBox()
    {
        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }
}