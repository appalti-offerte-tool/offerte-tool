<?php

class SupportTicket_View_Helper_PriorityComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    public function __construct()
    {

        $options = array(
            'low'       => 'Laag',
            'normal'    => 'Normaal',
            'urgent'    => 'Urgent'
        );
        $this->_options = $options;
    }

    public function priorityComboBox()
    {
        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }
}