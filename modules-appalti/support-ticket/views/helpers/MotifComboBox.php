<?php

class SupportTicket_View_Helper_MotifComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    public function __construct()
    {

        $options = array(
            'ease'      => 'Gemak',
            'status'    => 'Staat',
            'profit'    => 'Winst',
            'certainty' => 'Zekerheid'
        );

        $this->_options = $options;
    }

    public function motifComboBox()
    {
        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }
}