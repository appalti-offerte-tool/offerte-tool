Ext.define('Module.SupportTicket.Model.SupportMinutes', {
    extend:'Ext.data.Model',
    fields:[ 'id', 'name', 'amount', 'price' ]
});