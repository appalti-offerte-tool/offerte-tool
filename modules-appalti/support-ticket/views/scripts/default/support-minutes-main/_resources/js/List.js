Ext.define('Module.SupportTicket.ListSupportMinutes', {
    extend:'Ext.ux.grid.GridPanel',
    alias:'widget.module.support-ticket.support-minutes.list',
    filterRequestParam:null,
    features:[
        {
            ftype:'filters'
        }
    ],
    initComponent:function() {
        this.store = new Ext.data.Store({
            model:'Module.SupportTicket.Model.SupportMinutes',
            proxy:{
                type:'ajax',
                url:link('support-ticket', 'support-minutes-main', 'fetch-all', {
                    format:'json'
                }),
                reader:{
                    type:'json',
                    root:'rowset'
                }
            }
        });
        this.columns = [
            {
                header:lang('Name'),
                dataIndex:'name',
                flex:1
            }, {
                header:lang('Ammount(min)'),
                dataIndex:'amount',
                flex:1,
                renderer: function (value){
                    return value + ' min';
                }
            },  {
                header:lang('Price'),
                dataIndex:'price',
                flex:1,
                renderer: function(value){
                    return '€'+value;
                }
            }, {
                xtype:'actioncolumn',
                header:lang('Actions'),
                width:50,
                fixed:true,
                items:[
                    {
                        tooltip:lang('Edit'),
                        iconCls:'icon-edit-16 icon-16',
                        handler:function(g, rowIndex) {
                            this.onEditMinutes(g, g.getStore().getAt(rowIndex));
                        },
                        scope:this
                    },{
                        tooltip:lang('Delete'),
                        iconCls:'icon-delete-16 icon-16',
                        handler:this.onDeleteMinutes,
                        scope:this
                    }
                ]
            }
        ];
        this.tbar = [
            {
                text:lang('Create'),
                iconCls:'icon-create-16',
                handler:this.onMinutesCreate,
                scope:this
            }
        ];
        this.plugins = [
            new Ext.ux.grid.Search({
                minChars:2,
                stringFree:true,
                align:2
            })
        ].concat(Ext.isArray(this.plugins) ? this.plugins : []);
        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store:this.store,
            plugins:'pagesize'
        });
        this.callParent();
        this.getView().on('itemdblclick', function(w, record) {
            this.onEditMinutes(this, record);
        }, this);
    },
    onDeleteMinutes:function(g, rowIndex) {
        var record = g.getStore().getAt(rowIndex);
        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
                if (b != 'yes') {
                    return;
                }
                Ext.Ajax.request({url:link('support-ticket', 'support-minutes-main', 'delete', { format:'json' }),
                    method:'POST',
                    params:{
                        id:record.get('id')
                    },
                    success:function(response, options) {
                        var decResponse = Ext.decode(response.responseText);
                        Application.notificate(decResponse.messages);
                        if (true == decResponse.success) {
                            g.getStore().load();
                        }
                    },
                    scope:this
                });
            }, this);
    },
    onMinutesCreate:function() {
        Application.require(['support-ticket/support-minutes-main/form'], function() {
            var f = new Module.SupportTicket.FormSupportMinutes({});
            f.on('completed', function(clientId) {
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditMinutes:function(g, record) {
        Application.require(['support-ticket/support-minutes-main/form'], function() {
            var f = new Module.SupportTicket.FormSupportMinutes({
                minutesId:record.get('id')
            });
            f.on('completed', function(clientId) {
                g.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    }
});