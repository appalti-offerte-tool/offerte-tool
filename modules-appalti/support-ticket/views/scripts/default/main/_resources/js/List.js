Ext.define('Module.SupportTicket.ListTicket', {
    extend:'Ext.ux.grid.GridPanel',
    alias:'widget.module.support-ticket.list',

    serviceId:null,
    title: lang('Tickets'),
    stateful: false,
    stateId: false,
   
    filterRequestParam:null,

    features:[ {
        ftype:'filters'
    }, {
        id: 'group',
        ftype: 'groupingsummary',
        groupHeaderTpl: '{name}',
        hideGroupedHeader: true,
        remoteRoot: 'summaryData'
    }],
    initComponent:function() {
        this.store = new Ext.data.Store({
            model:'Module.SupportTicket.Model.Ticket',
            proxy:{
                type:'ajax',
                url:link('support-ticket', 'main', 'fetch-all', {
                    format:'json'
                }),
                reader:{
                    type:'json',
                    root:'rowset'
                }
            },
            groupField: 'serName'
        });
        this.columns = [ {
            header:lang('Service'),
            dataIndex:'serName',
            flex: 2
        }, {
            header:lang('CompanyName'),
            dataIndex:'name',
            flex:1
        }, {
            header:lang('ContactPerson '),
            dataIndex:'lastname',
            flex:1
        }, {
            header:lang('CreateDate'),
            xtype:'datecolumn',
            dataIndex:'createdDatetime',
            format:'d-m-Y H:i:s',
            width:120
        },{
            header:lang('ModifyDate'),
            xtype:'datecolumn',
            dataIndex:'modifiedDatetime',
            format:'d-m-Y H:i:s',
            width:120
        }, {
            header:lang('Statys'),
            dataIndex:'status',
            flex:1
        }, {
            header:lang('writer'),
            dataIndex:'writerName',
            flex:1,
            renderer: function(value){
                if (value ==null) {
                    return 'NONE';
                } else {
                    return value;
                }
            }
        }, {
            xtype:'actioncolumn',
            header:lang('Actions'),
            width:50,
            fixed:true,
            items:[ {
                tooltip:lang('Edit'),
                iconCls:'icon-edit-16 icon-16',
                handler:function(g, rowIndex) {
                    this.onEditSupportTicket(g, g.getStore().getAt(rowIndex));
                },
                scope:this
            }, {
                tooltip:lang('Delete'),
                iconCls:'icon-delete-16 icon-16',
                handler:this.onDeleteSupportTicket,
                scope:this
            } ]
        } ];
        this.tbar = [ '-' ];
        this.plugins = [ new Ext.ux.grid.Search({
            minChars:2,
            stringFree:true,
            align:2
        }) ].concat(Ext.isArray(this.plugins) ? this.plugins : []);
        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store:this.store,
            plugins:'pagesize'
        });
        ;
        this.callParent();
        this.getView().on('itemdblclick', function(w, record) {
            this.onEditSupportTicket(this, record);
        }, this);
    },

    onDeleteSupportTicket:function(g, rowIndex) {
        var record = g.getStore().getAt(rowIndex);
        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'),
            function(b) {
                if (b != 'yes') {
                    return;
                }
                Ext.Ajax.request({
                    url:link('support-ticket', 'main', 'delete', {format:'json'}),
                    method:'POST',
                    params:{
                        id:record.get('id')
                    },
                    success:function(response, options) {
                        var decResponse = Ext
                                .decode(response.responseText);
                        Application
                                .notificate(decResponse.messages);
                        if (true == decResponse.success) {
                            g.getStore().load();
                        }
                    },
                    scope:this
                });
            }, this);
    },

    onEditSupportTicket:function(g, record) {
        Application.require([ 'support-ticket/./form' ],
        function() {
            var f = new Module.SupportTicket.FormTicket({
                ticketId:record.get('id')
            });
            f.on('completed', function(clientId) {
                g.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    setServiceId:function(serviceId, forceReload) {
        this.serviceId = serviceId;
        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.serviceId = serviceId;
        if (true === forceReload) {
            this.getStore().load();
        }
        return this;
    }
});