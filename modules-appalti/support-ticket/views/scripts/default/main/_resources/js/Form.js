Ext.define('Module.Application.Model.Error', {
    extend:'Ext.data.Model',
    fields:[
        'message', 'type', 'field', {
            name:'id',
            mapping:'field'
        }, {
            name:'msg',
            mapping:'message'
        }
    ]
});
Ext.define('Module.SupportTicket.FormTicket', {
    extend:'Ext.form.Panel',
    alias:'widget.module.support-ticket.ticket.form',
    bodyPadding:5,
    ticketId:null,
    trackResetOnLoad:true,
    waitMsgTarget:true,
    wnd:null,
    model:'Module.SupportTicket.Model.Ticket',
    initComponent:function() {
        this.initialConfig.reader = new Ext.data.reader.Json({
            model:'Module.SupportTicket.Model.Ticket',
            type:'json',
            root:'row'
        });
        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
                model:'Module.Application.Model.Error',
                type:'json',
                root:'messages',
                successProperty:'success'
            });
        this.initialConfig.trackResetOnLoad = true;
        this.items = [
            {
                xtype:'radiogroup',
                fieldLabel:'Status',
                columns:3,
                labelWidth:115,
                vertical:true,
                items:[
                    {
                        boxLabel:'New',
                        name:'status',
                        inputValue:'new'
                    }, {
                        boxLabel:'Pending',
                        name:'status',
                        inputValue:'pending'
                    }, {
                        boxLabel:'Close',
                        name:'status',
                        inputValue:'close'
                    }
                ]
            }, {
                xtype:'radiogroup',
                fieldLabel:'Priority',
                columns:3,
                labelWidth:115,
                vertical:true,
                items:[
                    {
                        boxLabel:'Low',
                        name:'priority',
                        inputValue:'low'
                    }, {
                        boxLabel:'Normal',
                        name:'priority',
                        inputValue:'normal'
                    }, {
                        boxLabel:'Urgent',
                        name:'priority',
                        inputValue:'urgent'
                    }
                ]
            }, {
                xtype:'textfield',
                fieldLabel:lang('AmmountOfPeople'),
                labelWidth:115,
                name:'amountOfPeople'
            }, {
                xtype:'textfield',
                name:'numberOfPages',
                fieldLabel:lang('Number of pages'),
                labelWidth:115,
                readOnly:true
            }, {
                xtype:'radiogroup',
                fieldLabel:lang('PrimaryMotif'),
                columns:3,
                labelWidth:115,
                vertical:true,
                items:[
                    {
                        boxLabel:lang('Ease'),
                        name:'primaryMotif',
                        inputValue:'ease'
                    }, {
                        boxLabel:lang('Status'),
                        name:'primaryMotif',
                        inputValue:'status'
                    }, {
                        boxLabel:lang('Security'),
                        name:'primaryMotif',
                        inputValue:'security'
                    }
                ]
            },
            {
                xtype:'radiogroup',
                fieldLabel:lang('SecondaryMotif'),
                columns:3,
                labelWidth:115,
                vertical:true,
                items:[
                    {
                        boxLabel:lang('Ease'),
                        name:'secondaryMotif',
                        inputValue:'ease'
                    }, {
                        boxLabel:lang('Status'),
                        name:'secondaryMotif',
                        inputValue:'status'
                    }, {
                        boxLabel:lang('Security'),
                        name:'secondaryMotif',
                        inputValue:'security'
                    }
                ]
            },
            {
                xtype:'radiogroup',
                fieldLabel:lang('AimOfText'),
                columns:3,
                labelWidth:115,
                vertical:true,
                items:[
                    {
                        boxLabel:lang('Info'),
                        name:'aimOfText',
                        inputValue:'info'
                    }, {
                        boxLabel:lang('Revitalize'),
                        name:'aimOfText',
                        inputValue:'revitalize'
                    }, {
                        boxLabel:lang('Proposal'),
                        name:'aimOfText',
                        inputValue:'proposal'
                    }
                ]
            },{
                xtype: 'module.support-ticket.combo-box',
                fieldLabel: lang('Writer'),
                name: 'writerId',
                hiddenName: 'writerId',
                labelWidth:115


            },{
                xtype:'htmleditor',
                fieldLabel:lang('Writer function'),
                name:'writerFunc',
                readOnly:true,
                labelWidth:115,
                width:500,
                height:150,
                allowBlank:true
            }
        ];

        this.callParent(arguments);
    },
    doLoad:function() {
        if (!this.ticketId) {
            return;
        }
        this.form.load({
            url:link('support-ticket', 'main', 'fetch-row', { format:'json' }),
            method:'get',
            params:{
                ticketId:this.ticketId
            },
            scope:this
        });
    },
    showInWindow:function() {
        this.border = false;
        var w = this.wnd = new Ext.Window({
            title:lang('Edit ticket'),
            resizable:false,
            layout:'fit',
            border:true,
            width:520,
            modal:true,
            items:[this],
            buttons:[
                {
                    text:lang('Save'),
                    handler:this.onSubmit,
                    scope:this
                }, {
                    text:lang('Close'),
                    handler:function() {
                        w.close();
                        this.wnd = null;
                    },
                    scope:this
                }
            ]
        });
        this.doLoad();
        w.show();
        return w;
    },
    onSubmit:function(panel, w) {
        var params = {};
        var action = 'edit';
        params['ticketId'] = this.ticketId;
        if (!this.ticketId) {
            return false;
        }
        this.form.submit({
            url:link('support-ticket', 'main', action, { format:'json' }),
            method:'post',
            params:params,
            waitMsg:Ext.LoadMask.prototype.msg,
            success:function(form, action) {
                var responseObj = Ext.decode(action.response.responseText);
                Application.notificate(action);
                if (action.result.success) {
                    this.fireEvent('completed', this, responseObj.id, action.response);
                    this.wnd && this.wnd.close();
                }
            },
            scope:this
        });
    }
});