Ext.define('Module.SupportTicket.Model.Ticket', {
    extend:'Ext.data.Model',
    fields:[
                'serName', 'numberOfPages', 'priority', 'status', 'amountOfPeople',
                'primaryMotif', 'secondaryMotif', 'aimOfText','id',
                'accountId','createdDatetime','modifiedDatetime',
                'serviceId','name','lastname', 'writerFunc','writerName', 'writerId',
                {name: 'createdDatetime', type: 'date', dateFormat: 'Y-m-d H:i:s'},
                {name: 'modifiedDatetime', type: 'date', dateFormat: 'Y-m-d H:i:s'}
           ]
});