Ext.define('Module.SupportTicket.TicketPost.List', {
    extend:'Ext.ux.grid.GridPanel',
    alias:'widget.module.support-ticket.ticket-post.list',

    features:[ {
        ftype:'filters'
    } ],

    modeReadOnly:false,
    dataUrl:null,
    ticketPostId:null,
    ticketId:null,

    initComponent:function() {

        this.store = new Ext.data.Store(
                {
                    model:'Module.SupportTicket.Model.SupportTicketPost',
                    proxy:{
                        type:'ajax',
                        url:link('support-ticket','ticket-post', 'fetch-all', { format:'json' }),
                        reader:{
                            type:'json',
                            root:'rowset'
                        }
                    }
                });
        this.columns = [{
            header:lang('Type'),
            dataIndex:'type'
        }, {
            header:lang('UserName'),
            dataIndex:'username',
        }, {
            header:lang('Date'),
            xtype:'datecolumn',
            dataIndex:'dateTime',
            format:'d-m-Y'
        }, {
            header:lang('SendMail'),
            dataIndex:'sendMail'
        }, {
            xtype:'actioncolumn',
            header:lang('Actions'),
            width:50,
            fixed:true,
            items:[ {
                tooltip:lang('Send by e-mail'),
                iconCls:'icon-notificate-16 icon-16',
                handler:function(g, rowIndex) {
                    Ext.Msg.confirm(
                        lang('Confirmation'),
                        lang('Are you sure to send this by e-mail?'),
                        function(b) { 
                            b == 'yes' && this.onSendLetter(g,g.getStore().getAt(rowIndex));
                        }, this);
                },
                scope:this
            }, {
                tooltip:lang('Edit'),
                iconCls:'icon-edit-16 icon-16',
                handler:function(g, rowIndex) {
                    this.onTicketPostEdit(g, g.getStore().getAt(rowIndex));
                },
                scope:this
            }, {
                tooltip:lang('Delete'),
                iconCls:'icon-delete-16 icon-16',
                handler:this.onTicketPostDelete,
                scope:this
            }]
        } ];

        this.tbar = [ {
            text:lang('Create'),
            iconCls:'icon-create-16',
            handler:this.onTicketPostCreate,
            scope:this
        } ];
        this.plugins = [ new Ext.ux.grid.Search({
            minChars:1,
            stringFree:true,
            align:2
        }) ].concat(Ext.isArray(this.plugins) ? this.plugins
                : []);
        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store:this.store,
            plugins:'pagesize'
        });
        this.callParent();
    },

    onTicketPostCreate:function() {
        Application.require(
                [ 'support-ticket/ticket-post/form' ],
                function() {
                    var f = new Module.SupportTicket.Form({
                        ticketId:this.ticketId
                    });
                    f.on('completed', function() {
                        this.getStore().load();
                    }, this);
                    f.showInWindow();
                }, this);
    },

    onTicketPostEdit:function(g, record) {
        Application.require(
            [ 'support-ticket/ticket-post/form' ],
            function() {
                var f = new Module.SupportTicket.Form({
                    ticketId:this.ticketId,
                    ticketPostId:record.raw.id
                });
                f.on('completed', function() {
                    this.getStore().load();
                }, this);
                f.showInWindow();
            }, this);
    },

    onTicketPostDelete:function(g, rowIndex) {
        var record = g.getStore().getAt(rowIndex);
        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
            if (b != 'yes') {
                return;
            }
            Ext.Ajax.request({url:link('support-ticket', 'ticket-post', 'delete', { format:'json' }),
                method:'POST',
                params:{
                    ticketPostId:record.raw.id
                },
                success:function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);
                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope:this
            });
        }, this);
    },

    onSendLetter:function(g, record) {
        Ext.Ajax.request({
            url:link('support-ticket', 'feedback', 'send', { format:'json' }),
            method:'POST',
            params:{
                ticketPostId:record.internalId,
                ticketId:this.ticketId
            },
            success:function(response, options) {
                var decResponse = Ext.decode(response.responseText);
                Application.notificate(decResponse.messages);

                if (true == decResponse.success) {
                    this.setLastInsertedId(record.get('id'));
                    this.getStore().load();
                }
            },
            scope:this
        });
    }

});