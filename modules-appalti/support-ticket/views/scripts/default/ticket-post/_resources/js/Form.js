Ext.define('Module.SupportTicket.Form', {
    extend:'Ext.panel.Panel',
    alias:'widget.module.support-ticket.form',

    layout:'fit',
    form:null,
    ticketId:null,
    ticketPostId:null,
    wnd:null,
    initComponent:function() {
        this.blocks = new Module.Application.BlockTabPanel({
            activeTab:0,
            border:false,
            invokeArgs:{
                ticketId:this.ticketId,
                ticketPostId:this.ticketPostId
            }
        });
        this.items = [ this.blocks ];
        this.callParent(arguments);
        this.addEvents('completed');
        this.blocks.on('tabchange', function(tb, b) {
            b && b.reload();
            b && b.doLayout();
        });
        var params = {};
        if (this.ticketId) {
            params['ticketId'] = this.ticketId;
        }
        params.keywords = 'post-form';
        params.module = 'support-ticket';
        this.on('render', function() {
            var me = this;
            this.blocks.reload(params, function(tp) {

                var block = me.getFileStockBlock();
                if (null !== block) {
                    if (!me.ticketPostId) {
                        block.setDisabled (true);
                    }
                }
            });
        }, this);
        this.blocks.on('backgroundinvokeargschanged', function(bp, ia) {
            if (ia.ticketPostId) {
                this.ticketPostId = ia.ticketPostId;
            }
            return false;
        }, this);
    },

    getFileStockBlock:function() {
        var t = null;
        this.blocks.items.each(function(tab) {
            if (/file-stock/.test(tab.id)) {
                t = tab;
                return false;
            }
        });
        return t;
    },
    onSend:function(panel, w) {
        this.invokeArgs = this.invokeArgs || {};
        var params = Ext.apply({}, this.invokeArgs);
        if (!this.ticketPostId && this.invokeArgs.ticketPostId) {
            this.ticketPostId = this.invokeArgs.ticketPostId;
        }
        params['ticketPostId'] = this.ticketPostId;
        params['ticketId'] = this.ticketId;
        params['supportTicketId'] = this.ticketId
        var isValid = true;
        this.blocks.items.each(function(block) {
            if (!block.isValid()) {
                isValid = false;
                return isValid = false;
            }
            Ext.applyIf(params, block.toValues());
        });
        if (true !== isValid) {
            return;
        }
        var action = this.ticketPostId ? 'send' : 'create-and-send';
        this.el.mask(lang('Saving...'), 'x-mask-loading');
        Ext.Ajax.request({
            url:link('support-ticket', 'feedback', action, { format:'json' }),
            method:'POST',
            params:Ext.ux.OSDN.encode(params, true),
            success:function(response, options) {
                this.el.unmask();
                var responseObj = Ext.decode(response.responseText);
                Application.notificate(responseObj.messages);
                if (true === responseObj.success) {
                    this.fireEvent('completed', responseObj.id);
                    this.ticketPostId = responseObj.id;
                    this.wnd && this.wnd.close();
                    return;
                } else {
                    Ext.Msg.alert(lang('Error'),
                            lang('You don`t create answer!'), Ext.emptyFn);
                }
            },
            scope:this
        });
    },
    onSubmit:function(panel, w) {
        this.invokeArgs = this.invokeArgs || {};
        var params = Ext.apply({}, this.invokeArgs);
        if (!this.ticketPostId && this.invokeArgs.ticketPostId) {
            this.ticketPostId = this.invokeArgs.ticketPostId;
        }
        params['ticketPostId'] = this.ticketPostId;
        params['ticketId'] = this.ticketId;
        params['supportTicketId'] = this.ticketId;
        var isValid = true;
        this.blocks.items.each(function(block) {
            if (!block.isValid()) {
                isValid = false;
                return isValid = false;
            }
            Ext.applyIf(params, block.toValues());
        });
        if (true !== isValid) {
            return;
        }
        var action = this.ticketPostId ? 'update' : 'create-answer';
        this.el.mask(lang('Saving...'), 'x-mask-loading');
        Ext.Ajax.request({
            url:link('support-ticket', 'ticket-post', action, { format:'json' }),
            method:'POST',
            params:Ext.ux.OSDN.encode(params, true),
            success:function(response, options) {
                this.el.unmask();
                var responseObj = Ext.decode(response.responseText);
                Application.notificate(responseObj.messages);
                if (true == responseObj.success) {
                    this.ticketPostId = responseObj.id;
                    this.fireEvent('completed', responseObj.id);
                    var block = this.getFileStockBlock();
                    if (null !== block) {
                        block.setDisabled(false);
                        block.setInvokeArgs(Ext.apply(block.invokeArgs || {}, {
                            ticketPostId:responseObj.id
                        }));
                    }
                    return;
                }
            },
            scope:this
        });
    },

    showInWindow:function() {
        this.border = false;
        var w = this.wnd = new Ext.Window({
            title:this.ticketId ? lang('Update') : lang('Create'),
            resizable:false,
            layout:'fit',
            width:695,
            height:260,
            border:false,
            modal:true,
            items:[ this ],
            buttons:[ {
                text:lang('Send answer by e-mail'),
                handler:this.onSend,
                scope:this
            }, {
                text:lang('Attach files to answer/Update'),
                handler:this.onSubmit,
                scope:this
            }, {
                text:lang('Close'),
                handler:function() {
                    w.close();
                    this.wnd = null;
                },
                scope:this
            } ]
        });
        w.show();
        return w;
    },

    destroy:function() {
        Ext.destroy(this.blocks);
        this.callParent(arguments);
    }
});