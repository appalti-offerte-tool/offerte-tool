Ext.define('Module.SupportTicket.Model.SupportTicketPost', {
    extend: 'Ext.data.Model',
    fields:[
         'data', 'comment','type','dateTime','type','username','sendMail',
         {name: 'dateTime', type: 'date', dateFormat: 'Y-m-d H:i:s'},
    ]
});