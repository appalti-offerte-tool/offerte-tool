(function($, win) {
    var SupportTicketForm = function (conf) {
            $.extend(this, conf || {});
            $($.proxy(this.init, this));
        },
        fileTpl = [
            '<tr>',
                '<td class="info-cnt va-middle">&nbsp;</td>',
                '<td>',
                    '<span class="file-container">',
                        '<em class="file-name">',
                            '<span class="text-gray">Selecteer een bestand</span>',
                            '<input type="file" class="file" data-accept=".pdf, .doc, .docx, .rtf" name="files-[s]">',
                            '<a href="#" class="file-remove icon-delete-cross">&nbsp;</a>',
                        '</em>',
                    '</span>',
                '</td>',
            '</tr>'
        ].join('');


    SupportTicketForm.prototype = {

        el: null,
        maxFileId: 0,
        reloadOnCreate: false,
        onSuccessAdd: $.noop,

        init: function() {
            var me = this;
            $('.create-ticket, .help-btn').click(function() {
                me.showForm();
                return false;
            });
        },

        initEvents: function() {
            var me = this;
            me.el
                .on('hide', function() {
                    return !$('form', me.el).observable().isDirty() ||
                           confirm('Wil je support varaag creatie te annuleren?');
                })
                .on('hidden', function() {
                    me.maxFileId = 0;
                    me.el.remove();
                })
                .on('click', 'a.remove-extra-file', function() {
                    $(this).parent('div').remove();
                    return false;
                })
                .on('change', 'input[name^="files"]', function() {
                    $(this)
                    .parent()
                    .find('span:first')
                    .removeClass('text-gray')
                    .text(this.value.split(/(\\|\/)/g).pop());

                })
                .on('click', '.file-remove', function() {
                    var el = $(this), parent = el.closest('tr');

                    if (el.closest('#extra-files-container').length) {
                        parent.remove();
                    } else {
                        parent
                        .find('.text-gray').html('Selecteer een bestand')
                        .end()
                        .find('input[type=file]').val('');
                    }

                    return false;
                })
                .on('change', '#serviceId, #priority', $.proxy(this.getPrice, this))
                .on('click', 'a.add_file', function() {
                    me.maxFileId++;
                    $('#extra-files-container').append(fileTpl.replace('[s]', me.maxFileId));
                    $('input[name=files-' + me.maxFileId + ']', me.el).rules('add', { file: '.pdf, .doc, .docx, .rtf' });
                })
                .on('submit', 'form', function() {
                    $(this).valid() && me.doAdd.call(me, this);
                    return false;
                });
        },

        getPrice: function(refreshObservable) {
            var me = this;

            !me.availableMinutes && (me.availableMinutes = parseInt($('#availableMinutes', me.el).val()));

            if (!me.service) {
                me.service = $('#serviceId', me.el);
                me.priority = $('#priority', me.el);
            }

            if (!me.service.val()) {
                return;
            }

            $.ajax({
                url: '/support-ticket/index/fetch-price',
                data: {
                    format:'json',
                    serviceId: me.service.val(),
                    priorityId: me.priority.val()

                },
                mask: true,
                maskMsg: 'Het bijwerken...',
                maskTarget: $('.price-info > .box-gray', me.el),
                success: function(response) {
                    if (response.success) {
                        $('.ticketPrice', me.el)
                        .filter('input').val(response.price)
                        .end()
                        .filter('span').html(response.price);

                        $('#words', me.el).html(response.wordCount !=null ? 'per ' +response.wordCount+ ' words' : '');
                        $('#wordCount', me.el).val(response.wordCount);
                        $('#more-credits-question', me.el).toggle(me.availableMinutes < response.price);

                        refreshObservable && $('form', me.el).observable().refresh();
                    } else {
                        alert(response.messages[0].message);
                    }
                }
            });
        },

        /**
         * @param noInfo Do not show support info before main support form
         * @param url URL to get the form from
         * @param params URL params
         */
        showForm: function (noInfo, url, params) {
            var me = this;
            $.ajax({
                url: url || '/support-ticket/client/form',
                data: params || {},
                mask: true,
                success: function(data) {
                    me.el = $('<div />',  { id: 'support-ticket-form-modal', 'class': 'modal' })
                        .appendTo('body')
                        .html(data)
                        .data('backdrop', 'static');

                    me.initEvents.call(me);

                    if (!noInfo) {
                        var info = $('#support-info', me.el).appendTo('body')
                            .on('click', '.help-btn-pink', function() {
                                info.modal('hide');
                                me.getPrice.call(me, true);
                                me.el.modal('show');
                                return false;
                            })
                            .on('hidden', function() {
                                info.remove();
                                !$('.help-btn-pink', info).length && me.el.remove();
                            })
                            .modal('show');
                    } else {
                        me.getPrice.call(me, true);
                        me.el.modal('show');
                    }
                }
            });
        },

        doAdd: function(form) {
            var me = this;
            $(form).ajaxSubmit({
                url: '/support-ticket/client/create',
                dataType: 'json',
                data: { format: 'json' },
                type: 'post',
                mask: true,
                maskMsg: 'Support vraag wordt gemaakt. Een ogenblik geduld.',
                success: function(response) {
                    if (response && response.success) {
                        me.el.off('hide').modal('hide');
                        alert('Support vraag succesvol opgeslagen');

                        me.onSuccessAdd();

                        me.reloadOnCreate && window.location.reload();

                        if (undefined !== AppaltiLayout.blocks.SupportListBlock) {
                            AppaltiLayout.blocks.SupportListBlock.reloadList();
                        }
                    } else {
                        alert(response.messages[0].message);
                    }
                }
            });
        }
    }

    win.SupportTicketForm = new SupportTicketForm();

})(jQuery, window);