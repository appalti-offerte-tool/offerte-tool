$(function() {
    var $modal = null;
    $('#assignWriterForm').submit(function() {
        $.ajax({
            url: this.action,
            type: 'post',
            data: $(this).serialize(),
            mask: true,
            maskMsg: 'Assigning ticket to writer. Please wait.',
            success: function(response) {
                if (!$modal) {
                    $modal = $('<div class="modal" id="assign-result" />').appendTo('body');
                }

                $modal.html(response).modal('show');
            }
        });

        return false;
    });
});
