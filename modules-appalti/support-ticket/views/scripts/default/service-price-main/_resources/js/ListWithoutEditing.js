Ext.define('Module.SupportTicket.List.Service.Price.WithoutEdit', {
    extend:'Ext.ux.grid.GridPanel',
    alias:'widget.module.support-ticket.service.price.list.without-edit',
    filterRequestParam:null,

    stateFul:true,
    companyId:null,
    endDate : null,
    isActive: null,
    initComponent:function() {
        this.store = new Ext.data.Store({
            model:'Module.SupportTicket.Model.Service.Price',
            proxy:{
                type:'ajax',
                url:link('support-ticket', 'service-price-main', 'fetch-all-by-company', {
                    format:'json'
                }),
                reader:{
                    type:'json',
                    root:'rowset'
                }
            }
        });
        this.columns = [
            {
                header:lang('Service name'),
                dataIndex:'name',
                flex:1
            },
            {
                header:lang('Desc'),
                dataIndex:'desc',
                flex:1
            },
            {
                header:lang('Word Count'),
                dataIndex:'wordCount',
                flex:1
            },
            {
                header:lang('Price '),
                dataIndex:'price',
                flex:1,
                renderer: function(value){
                    if (value ==null) {
                        return '';
                    } else {
                        return value +' min';
                    }
                }
            },
            {
                header:lang('Discount'),
                dataIndex:'discount',
                flex:1,
                renderer: function(value){
                    if (value ==null) {
                        return '';
                    } else {
                        return value + '%';
                    }
                }
            }
        ];

        this.search = new Ext.ux.grid.Search({
            minChars:2,
            stringFree:true,
            align:2
        });


        var endDateF = new Ext.form.field.Text({
            fieldLabel: lang('End date'),
            name: 'endDate',
            labelWidth:60,
            readOnly:true
        });

        var assignButton = new Ext.create('Ext.Button', {
            text:lang('Assign membership'),
            iconCls:'icon-create-16',
            handler:this.onAssignMembership,
            scope:this

        });


        this.tbar = [
            '-',
            '-',
            '->',
            endDateF,
            assignButton
        ];
        this.plugins = [
            this.search
        ].concat(Ext.isArray(this.plugins) ? this.plugins : []);
        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store:this.store,
            plugins:'pagesize'
        });
        this.callParent();

        this.getStore().on('beforeload', function(store) {
            var endD = null;
            if (this.endDate) {
                var date = this.endDate.split('-');
                endD = new Date(date[2], date[1],date[0]).valueOf();
            }
            var today = new Date().valueOf();

            if (endD > today) {
                assignButton.setDisabled(true);
            } else {
                assignButton.setDisabled(false);
            }
            endDateF.setValue(this.endDate);
        }, this);


    },

    onAssignMembership:function() {

        Application.require([
            'membership/company-membership/form'
        ], function() {
            var f = new Module.Membership.CompanyMembership.Form({
                companyId:this.companyId
            });
            f.on('completed', function(clientId) {
                this.onReload(this);

            }, this);
            f.showInWindow();

        }, this);
    },


    onReload : function(me) {
        Ext.Ajax.request({
            url:link('membership', 'company', 'index', {companyId:this.companyId, format:'json'}),
            success: function(response){
                var decResponse = Ext.decode(response.responseText);
                me.endDate = decResponse.endDatetime;
                me.getStore().load();
            }
        });
        return this;
    },

    setCompanyId:function(companyId,endDate, forceReload) {
        this.companyId = companyId;
        this.endDate = endDate;
        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.companyId = companyId;
        if (true === forceReload) {
            this.getStore().load();
        }
        return this;
    }

});