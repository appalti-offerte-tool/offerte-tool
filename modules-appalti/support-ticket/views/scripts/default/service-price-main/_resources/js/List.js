Ext.define('Module.SupportTicket.List.Service.Price', {
    extend:'Ext.ux.grid.GridPanel',
    alias:'widget.module.support-ticket.service.price.list',
    filterRequestParam:null,
    features:[
        {
            ftype:'filters'
        }
    ],
    stateFul:true,
    membershipId:null,
    selModel:{
        selType:'cellmodel'
    },
    initComponent:function() {
        this.store = new Ext.data.Store({
            model:'Module.SupportTicket.Model.Service.Price',
            proxy:{
                type:'ajax',
                url:link('support-ticket', 'service-price-main', 'fetch-all', {
                    format:'json'
                }),
                reader:{
                    type:'json',
                    root:'rowset'
                }
            }
        });
        this.columns = [{
                dataIndex:'priceId',
                hidden: true
            }, {
                header:lang('Service name'),
                dataIndex:'name',
                flex:1
            }, {
                header:lang('Desc'),
                dataIndex:'desc',
                flex:1
            }, {
                header:lang('Word Count'),
                dataIndex:'wordCount',
                flex:1,
                editor:{
                    xtype:'numberfield',
                    allowBlank:true,
                    minValue:0,
                    maxValue:100000
                }

            }, {
                header:lang('Price '),
                dataIndex:'price',
                flex:1,
                renderer: function(value){
                    if (value ==null) {
                        return '';
                    } else {
                      return value+' min';
                    }
                },
                editor:{
                    xtype:'numberfield',
                    allowBlank:false,
                    minValue:0,
                    maxValue:100000
                }
            }, {
                header:lang('Discount'),
                dataIndex:'discount',
                flex:1,
                renderer: function(value){
                    if (value ==null) {
                        return '';
                    } else {
                        return value + '%';
                    }
                },
                editor:{

                    xtype:'numberfield',
                    allowBlank:false,
                    minValue:0,
                    maxValue:100
                }
            }];
        this.cellEditing = new Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit:1
        });
        this.search = new Ext.ux.grid.Search({
            minChars:2,
            stringFree:true,
            align:2
        });

        this.cellEditing.on('edit', function(b,row) {

            var field = row.field;

            var params = {
                membershipTypeId: this.membershipId, 
                serviceId : row.record.internalId,
                priceId : row.record.get('priceId')
            };

            params[row.field] = b.context.value;


            Ext.Ajax.request({
                url:link('support-ticket', 'service-price-main', 'edit', {
                    format:'json'
                }),
                params: params,
                success:function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);
                    if (true == decResponse.success) {
                        row.record.set('priceId', decResponse.priceId);
                        row.record.commit();
                    }
                }
            });
        }, this);

        this.tbar = [
                  '-' ];
        this.plugins = [
            this.search
            , this.cellEditing
        ].concat(Ext.isArray(this.plugins) ? this.plugins : []);
        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store:this.store,
            plugins:'pagesize'
        });
        this.callParent();


    },
    setMembershipId:function(membershipId, forceReload) {
        this.membershipId = membershipId;
        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.membershipId = membershipId;
        if (true === forceReload) {
            this.getStore().load();
        }
        return this;
    }

});