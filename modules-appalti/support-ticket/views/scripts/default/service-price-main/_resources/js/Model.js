Ext.define('Module.SupportTicket.Model.Service.Price', {
    extend:'Ext.data.Model',
    fields:['id', 'membershipTypeId', 'serviceId' , 'price','wordCount' ,'discount', 'name' , 'desc', 'priceId']
});