Ext.define('Module.SupportTicket.ListService', {
    extend:'Ext.ux.grid.GridPanel',
    alias:'widget.module.support-ticket.service.list',
    filterRequestParam:null,
    features:[
        {
            ftype:'filters'
        }
    ],
    initComponent:function() {
        this.store = new Ext.data.Store({
            model:'Module.SupportTicket.Model.Service',
            proxy:{
                type:'ajax',
                url:link('support-ticket', 'service', 'fetch-all', {
                    format:'json'
                }),
                reader:{
                    type:'json',
                    root:'rowset'
                }
            }
        });
        this.columns = [
            {
                header:lang('Name'),
                dataIndex:'name',
                flex:1
            }, {
                header:lang('Description'),
                dataIndex:'desc',
                flex:1
            }, {
                xtype:'actioncolumn',
                header:lang('Actions'),
                width:50,
                fixed:true,
                items:[
                    {
                        tooltip:lang('Edit'),
                        iconCls:'icon-edit-16 icon-16',
                        handler:function(g, rowIndex) {
                            this.onEditService(g, g.getStore().getAt(rowIndex));
                        },
                        scope:this
                    },{
                        tooltip:lang('Delete'),
                        iconCls:'icon-delete-16 icon-16',
                        handler:this.onDeleteService,
                        scope:this
                    }
                ]
            }
        ];
        this.tbar = [
            {
                text:lang('Create'),
                iconCls:'icon-create-16',
                handler:this.onServiceCreate,
                scope:this
            }
        ];
        this.plugins = [
            new Ext.ux.grid.Search({
                minChars:2,
                stringFree:true,
                align:2
            })
        ].concat(Ext.isArray(this.plugins) ? this.plugins : []);
        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store:this.store,
            plugins:'pagesize'
        });
        this.callParent();
        this.getView().on('itemdblclick', function(w, record) {
            this.onEditSupportTicket(this, record);
        }, this);
    },
    onDeleteService:function(g, rowIndex) {
        var record = g.getStore().getAt(rowIndex);
        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
                if (b != 'yes') {
                    return;
                }
                Ext.Ajax.request({url:link('support-ticket', 'service', 'delete', { format:'json' }),
                    method:'POST',
                    params:{
                        id:record.get('id')
                    },
                    success:function(response, options) {
                        var decResponse = Ext.decode(response.responseText);
                        Application.notificate(decResponse.messages);
                        if (true == decResponse.success) {
                            g.getStore().load();
                        }
                    },
                    scope:this
                });
            }, this);
    },
    onServiceCreate:function() {
        Application.require(['support-ticket/service/form'], function() {
            var f = new Module.SupportTicket.FormService({});
            f.on('completed', function(clientId) {
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditService:function(g, record) {
        Application.require(['support-ticket/service/form'], function() {
            var f = new Module.SupportTicket.FormService({
                serviceId:record.get('id')
            });
            f.on('completed', function(clientId) {
                g.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    }
});