Ext.define('Module.Application.Model.Error', {
    extend:'Ext.data.Model',
    fields:[
        'message', 'type', 'field', {
            name:'id',
            mapping:'field'
        }, {
            name:'msg',
            mapping:'message'
        }
    ]
});
Ext.define('Module.SupportTicket.FormService', {
    extend:'Ext.form.Panel',
    alias:'widget.module.support-ticket.service.form',
    bodyPadding:5,
    serviceId:null,
    trackResetOnLoad:true,
    waitMsgTarget:true,
    wnd:null,
    model:'Module.SupportTicket.Model.Service',
    initComponent:function() {
        this.initialConfig.reader = new Ext.data.reader.Json({
            model:'Module.SupportTicket.Model.Service',
            type:'json',
            root:'row'
        });
        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
                model:'Module.Application.Model.Error',
                type:'json',
                root:'messages',
                successProperty:'success'
            });
        this.initialConfig.trackResetOnLoad = true;
        this.items = [
            {
                xtype:'textfield',
                fieldLabel:lang('Name'),
                name:'name',
                anchor:'100%'
            },
            {
                xtype:'htmleditor',
                fieldLabel:lang('Description'),
                name:'desc',
                height:150,
                allowBlank:true
            }
        ];
        this.callParent(arguments);
    },
    doLoad:function() {
        if (this.serviceId) {
            this.form.load({
                url:link('support-ticket', 'service', 'fetch-row', { format:'json' }),
                method:'get',
                params:{
                    serviceId:this.serviceId
                },
                scope:this
            });
        }
    },
    showInWindow:function() {
        this.border = false;
        var w = this.wnd = new Ext.Window({
            title:lang('Service'),
            resizable:false,
            layout:'fit',
            border:false,
            modal:true,
            items:[this],
            buttons:[
                {
                    text:lang('Save'),
                    handler:this.onSubmit,
                    scope:this
                },
                {
                    text:lang('Close'),
                    handler:function() {
                        w.close();
                        this.wnd = null;
                    },
                    scope:this
                }
            ]
        });
        this.doLoad();
        w.show();
        return w;
    },
    onSubmit:function(panel, w) {
        var params = {};
        if (this.serviceId) {
            var action = 'edit';
            params['serviceId'] = this.serviceId;
        }
        if (!this.serviceId) {
            var action = 'create';
        }
        this.form.submit({
            url:link('support-ticket', 'service', action, { format:'json' }),
            method:'post',
            params:params,
            waitMsg:Ext.LoadMask.prototype.msg,
            success:function(form, action) {
                var responseObj = Ext.decode(action.response.responseText);
                Application.notificate(action);
                if (action.result.success) {
                    this.fireEvent('completed', this, responseObj.id, action.response);
                    this.wnd && this.wnd.close();
                }
            },
            scope:this
        });
    }
});