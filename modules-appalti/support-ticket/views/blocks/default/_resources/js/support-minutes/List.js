$(function() {
    var $modalCnt;
    $('#create-support-minutes, .edit-support-minutes').click(function() {
        $.get(this.href).done(function(html) {
            if (!$modalCnt) {
                $modalCnt = $('<div id="modal-container" class="modal support-minutes-modal-container" />').appendTo('body');
            }

            $modalCnt.html(html).modal('toggle');
        });

        return false;
    });
});