Ext.define('Module.SupportTicket.Block.ExpandableList', {
    extend:'Module.SupportTicket.TicketPost.List',

    ticketId:null,

    plugins:[ {
        ptype:'rowexpander',
        rowBodyTpl:[ '{comment}' ]
    } ],

    initComponent:function() {
        this.callParent();
        this.view.on('expandbody', function(rowNode, record, nextBd) {
            if (record.get('support-ticket')) {
                return;
            }
            record.set('support-ticket', lang('Loading...'));
            this.doLoadTicketContent(record, function(response) {
                record.set('comment', response);
                record.commit();
                this.doLayout();
            }, this);
        }, this);
    },

    doLoadTicketContent:function(record, callbackFn, scope) {
        Ext.Ajax.request({
            url:link('support-ticket', 'ticket-post', 'fetch-row', {
                ticketPostId:record.internalId
            }),
            success:function(response, options) {
                callbackFn.call(scope, response.responseText);
            },
            failure:function() {
                Application.notificate(false);
            },
            scope:this
        });
    },

    setTicketId:function(ticketId, forceReload) {
        this.ticketId = ticketId;
        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.ticketId = ticketId;
        if (true === forceReload) {
            this.getStore().load();
        }
        return this;
    }
});