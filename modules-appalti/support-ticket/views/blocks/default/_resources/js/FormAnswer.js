Ext.define('Module.SupportTicket.Block.FormAnswer', {
    extend:'Ext.form.Panel',
    alias:'widget.support-ticket.block.form-answer',

    bodyPadding:5,

    initComponent:function() {

        this.initialConfig.trackResetOnLoad = true;
        this.items = [  {
            xtype:'htmleditor',
            fieldLabel:lang('Data'),
            name:'comment',
            height:150,
            allowBlank:true
        } ];

        this.callParent(arguments);
    }
});