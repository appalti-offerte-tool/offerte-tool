<?php

final class SupportTicket_Misc_Email_Feedback extends Configuration_Service_ConfigurationAbstract implements Notification_Instance_NotifiableInterface

{
    protected $_identity = 'support-ticket.feedback';

    protected $_fields = array(
        'receipt' => array(),
        'subject',
        'body'    => array()
    );

    protected $_mui = true;

    protected $_keywords = array(
        'subject'    => 'E-mail subject',
        'name'       => 'Sender name',
        'email'      => 'Sender e-mail',
        'id'         => 'Ticket ID',

        'companyName'      => 'Company name',
        'contacpPerson'    => 'Contact Person',
        'cpEmail'          => 'Contack Person e-mail',
        'reseiver'         => 'Receiver',
        'service'          => 'E-mail body',
        'priority'         => 'Priority',
        'numberOfPages'    => 'Number of pages',
        'aimOfText'        => 'Aim of text',
        'amountOfPeople'   => 'Amount of people',
        'writerFunc'       => 'Writer function',
        'primaryMotif'     => 'Primary motif',
        'secondaryMotif'   => 'Secondary motif',
        'comment'          => 'E-mail body',

        'timestamp'=> 'Timestamp'
    );

    protected $_serialized = array('receipt', 'replyto');

    public function __construct()
    {
        parent::__construct();

        $defaultFrom = Zend_Mail::getDefaultFrom();
        if (is_array($defaultFrom)) {
            $this->_fields['receipt']['default'] = $this->_toValue($defaultFrom, 'receipt');
        }

        if (file_exists(__DIR__ . '/tpl.phtml')) {
            ob_start();
            require __DIR__ . '/tpl.phtml';
            $this->_fields['body']['default'] = ob_get_clean();
        }
    }

    public function notificate(array $params)
    {

        $supportTicketPostService = new SupportTicket_Service_SupportTicketPost();
        $supportTicketService = new SupportTicket_Service_SupportTicket();
        $supportTicketServiceService = new SupportTicket_Service_SupportTicketService();

        $ticketPostId = $params['id'];
        $ticketId = $params['supportTicketId'];



        $supportTicketPostRow = $supportTicketPostService->find($ticketPostId);

        $supportTicketRow = $supportTicketService->find($ticketId);
        $supportTicketServiceRow = $supportTicketServiceService->find($supportTicketRow->serviceId);;

        if ($supportTicketPostRow->sendMail =='yes')
        {
            throw new OSDN_Exception('This message has been already send to user');
        }

        $contactPersonRow = $supportTicketRow->getContactPersonRow();
        $companyRow = $supportTicketRow->getCompanyRow();

        $accountRow = $supportTicketRow->getAccountRow();
        $authAccountRow =  Zend_Auth::getInstance()->getIdentity();

        $dt = new \DateTime();

        $params = array(
            'timestamp' => $dt->format('Y-m-d H:i:s'),
            'email'     => $accountRow->getEmail(),
            'name'      =>  $accountRow->getFullname(),
            'subject'   => $supportTicketRow->writerFunc,

            'id'               =>$supportTicketRow->id,
            'companyName'      => $companyRow->name,
            'contacpPerson'    => $contactPersonRow->getFullname(),
            'cpEmail'          => $contactPersonRow->email,
            'reseiver'         => $accountRow->getFullname(),
            'service'          => $supportTicketServiceRow->name,
            'priority'         => $supportTicketRow->priority,
            'numberOfPages'    => $supportTicketRow->numberOfPages,
            'aimOfText'        => $supportTicketRow->aimOfText,
            'amountOfPeople'   => $supportTicketRow->amountOfPeople,
            'writerFunc'       => $supportTicketRow->writerFunc,
            'primaryMotif'     => $supportTicketRow->primaryMotif,
            'secondaryMotif'   => $supportTicketRow->secondaryMotif,
            'comment'          => $supportTicketPostRow->comment,
        );

        $instance = $this->toInstance();
        $mail = new Zend_Mail('UTF-8');
        $mail->setFrom($authAccountRow->getEmail(), $authAccountRow->getFullname());
        $mail->addTo($accountRow->getEmail(), $accountRow->getFullname());

        $mail->setSubject($instance->getSubject($params));
        $mail->setBodyHtml($instance->getBody($params));

        $fileStockRowset = $supportTicketPostRow->fetchAllFileStockRowset();
        $supportTicketPostFileStock = new SupportTicket_Service_SupportTicketPostFileStock();
        $storage = $supportTicketPostFileStock->toStorage();

        foreach($fileStockRowset as $fileStockRow) {
            try {
                $fileContent = $storage->fetchFileContent($fileStockRow);
                $at = $mail->createAttachment($fileContent);
                $at->filename = $fileStockRow->name;
            } catch (\Exception $e) {
                continue;
            }
        }

        try {
            $result = $mail->send();
            $supportTicketPostRow->sendEmail(true)->save();

        } catch(\Zend_Mail_Transport_Exception $e) {
            throw new OSDN_Exception('Unable to send. Transport error.', null, $e);
        } catch(\Exception $e) {
            throw new OSDN_Exception('Unable to send message', null, $e);
        }
        return (boolean) $result;
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_toValue()
     */
    protected function _toValue($value, $field)
    {
        if (in_array($field, $this->_serialized)) {
            return serialize($value);
        }

        return parent::_toValue($value, $field);
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_fromValue()
     */
    protected function _fromValue($value, $field)
    {
        if (in_array($field, $this->_serialized)) {
            return unserialize($value);
        }

        return parent::_fromValue($value, $field);
    }
}