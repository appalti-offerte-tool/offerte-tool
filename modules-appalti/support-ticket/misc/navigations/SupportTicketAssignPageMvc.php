<?php

final class SupportTicket_Misc_Navigation_SupportTicketAssignPageMvc extends Zend_Navigation_Page_Mvc
{
    protected $_isCompleted = false;

    protected $_ticketId;

    protected function _init()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $this->_ticketId = (int) $request->getParam('supportTicketId');

        $params = $this->getParams();

        if (!empty($this->_ticketId)) {
            $params['supportTicketId'] = $this->_ticketId;
        }

        $this->setParams($params);
        parent::_init();
    }


    /**
     * (non-PHPdoc)
     * @see Zend_Navigation_Page::getLabel()
     */
    public function getLabel()
    {
        if (true !== $this->_isCompleted) {
            $this->_label = 'Supportvraag';

            if (!empty($this->_ticketId)) {
                $supportTicketService = new SupportTicket_Service_SupportTicket();
                $supportTicketRow = $supportTicketService->find($this->_ticketId);

                $this->_label .= ' ' . $supportTicketRow->getCompanyRow()->getFullName();
                $this->_label .= ' #' . $supportTicketRow->getId();
            }

            $this->_isCompleted = true;
        }

        return parent::getLabel();
    }
}