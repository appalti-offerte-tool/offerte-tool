<?php

final class SupportTicket_Misc_Navigation_SupportTicketDetailPageMvc extends Zend_Navigation_Page_Mvc
{
    protected $_isCompleted = false;

    protected $_writerId;

    protected $_ticketId;

    protected function _init()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $this->_writerId = (int) $request->getParam('writerId');
        $this->_ticketId = (int) $request->getParam('ticketId');

        $params = $this->getParams();

        if (!empty($this->_writerId)) {
            $params['writerId'] = $this->_writerId;
        }

        if (!empty($this->_ticketId)) {
            $params['ticketId'] = $this->_ticketId;
        }

        $this->setParams($params);
        parent::_init();
    }


    /**
     * (non-PHPdoc)
     * @see Zend_Navigation_Page::getLabel()
     */
    public function getLabel()
    {
        if (true !== $this->_isCompleted) {
            $this->_label = 'Supportvraag';

            if (!empty($this->_ticketId)) {
                $supportTicketService = new SupportTicket_Service_SupportTicket();
                $supportTicketRow = $supportTicketService->find($this->_ticketId);

                $this->_label .= ' ' . $supportTicketRow->getCompanyRow()->getName();
                $this->_label .= ' #' . $supportTicketRow->getId();
            }

            $this->_isCompleted = true;
        }

        return parent::getLabel();
    }
}