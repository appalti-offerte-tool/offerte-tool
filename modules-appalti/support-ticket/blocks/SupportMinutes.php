<?php

class SupportTicket_Block_SupportMinutes extends \Application_Block_Abstract
{

    protected $_itemsPerPage = 10;
    
    protected function _toTitle()
    {
        return $this->view->translate('Support minutes');

    }

    protected function _toHtml()
    {
        $service = new \SupportTicket_Service_SupportMinutes();

        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage($this->_itemsPerPage);

        $params = $this->getRequest()->getParams();

        $params['filter'] = $this->getFilterParams();

        $response = $service->fetchAllWithResponse($params);
        $this->view->assign($response->toArray());

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
        $this->view->blockId = $this->getId();
        $this->view->listOnly = $this->_getParam('listOnly');
        
    }

}