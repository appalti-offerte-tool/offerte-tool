<?php

final class SupportTicket_Block_MySupportTicketList extends Application_Block_Abstract
{
    protected $_itemsPerPage = 5;

    protected function _toTitle()
    {
        return $this->view->translate('My support ticket');
    }

    protected function _toHtml()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage($this->_itemsPerPage);
        $this->view->limit = $this->_itemsPerPage;
        $this->getRequest()->setParam('filter', null);

        if ($this->_getParam('writerId')) {
            $stSrv = new \SupportTicket_Service_SupportTicketModerator();
            $response = $stSrv->fetchAllAssigned($this->getRequest()->getParams());
        } else {
            $stSrv = new \SupportTicket_Service_SupportTicketClient();
            $response = $stSrv->fetchAll($this->getRequest()->getParams());
        }

        $this->view->rowset = $response->getRowset();

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
        $this->view->listOnly = $this->_getParam('listOnly');
    }
}