<?php

class SupportTicket_Block_ServicePriceList extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Service prices');
    }

    protected function _toHtml()
    {
        $membershipSrv = new \Membership_Service_Membership();
        $response = $membershipSrv->fetchAllWithResponse();
        $this->view->membership = $response->getRowset();

        $servicesSrv = new \SupportTicket_Service_SupportTicketService();
        $this->view->services = $servicesSrv->fetchAllWithPrices();
    }
}