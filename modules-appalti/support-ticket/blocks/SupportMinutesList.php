<?php

class SupportTicket_Block_SupportMinutesList extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Support-minutes');
    }

    protected function _toHtml()
    {}
}