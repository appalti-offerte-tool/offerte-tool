<?php

final class SupportTicket_Block_FormAnswer extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Answer ticket');
    }

    protected function _toHtml()
    {
        $ticketId = $this->_getParam('ticketPostId');

        if (!empty($ticketId)) {
            $this->view->ticketId = $ticketId;
            $service = new \SupportTicket_Service_SupportTicketPost();
            $this->view->ticketRow = $service->find($ticketId);
        }
    }
}