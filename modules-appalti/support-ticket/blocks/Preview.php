<?php

final class SupportTicket_Block_Preview extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Preview');
    }

    protected function _toHtml()
    {
        $ticketId = $this->_getParam('ticketId');
        $this->view->assign('ticketId',$ticketId);
    }
}
?>