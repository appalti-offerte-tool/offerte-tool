<?php

final class SupportTicket_Block_WriterTicketList extends Application_Block_Abstract
{
    protected $_itemsPerPage = 10;

    protected function _toTitle()
    {
        return 'Gekoppelde supportvragen';
    }

    protected function _toHtml()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage($this->_itemsPerPage);

        $this->getRequest()->setParam('filter', null);

        $stSrv = new \SupportTicket_Service_SupportTicketModerator();
        $response = $stSrv->fetchAllAssigned($this->getRequest()->getParams());
        $this->view->rowset = $response->getRowset();

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
        $this->view->listOnly = $this->_getParam('listOnly');
        $this->view->status = $this->_getParam('status');
        $this->view->writerId = $this->_getParam('writerId', $this->getRequest()->getParam('writerId'));
    }
}