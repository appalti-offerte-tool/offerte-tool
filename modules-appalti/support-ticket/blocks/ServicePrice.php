<?php

class SupportTicket_Block_ServicePrice extends \Application_Block_Abstract
{

    protected function _toTitle()
    {
        return $this->view->translate('Support minutes');

    }

    protected function _toHtml()
    {
        $service = new \SupportTicket_Service_SupportTicketServicePrice();

        $response = $service->fetchAllWithResponse($this->getRequest()->getParams());
        $this->view->assign($response->toArray());

        $this->view->listOnly = $this->_getParam('listOnly');
        
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->view = clone $view;

        $basePath = __DIR__;
        $this->view->addScriptPath($basePath . '/../views/scripts/default/service-price/');

        return $this;
    }

    public function _toTemplateName()
    {
        return 'index.phtml';
    }


}