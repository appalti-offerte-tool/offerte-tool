<?php

class SupportTicket_Block_ServiceList extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Services');
    }

    protected function _toHtml()
    {}
}