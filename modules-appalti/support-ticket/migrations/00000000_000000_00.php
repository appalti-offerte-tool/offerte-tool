<?php

class Lib_Migration_00000000_000000_00 extends Core_Migration_Abstract
{

    public function up()
    {
       
        $this->createTable('service');
        $this->createColumn('service', 'membershipTypeId', self::TYPE_INT, 11, null, false);
        $this->createColumn('service', 'name', self::TYPE_VARCHAR, 50);
        $this->createColumn('service', 'desc', self::TYPE_VARCHAR, 50);

        $this->createIndex('service', 'membershipTypeId');
        $this->createForeignKey('service', array('membershipTypeId'), 'membershipType', array('id'), 'FK_membershipTypeId');

        $this->createTable('supportTicket');
        $this->createColumn('supportTicket', 'accountId', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'serviceId', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'numberOfPages', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'data', self::TYPE_VARCHAR, 50);
        $this->createColumn('supportTicket', 'lifepath', self::TYPE_VARCHAR, 50);
        $this->createColumn('supportTicket', 'priority', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'createDate', self::TYPE_TIMESTAMP,  null, false);
        $this->createColumn('supportTicket', 'modifyDate', self::TYPE_TIMESTAMP,  null, false);
        $this->createColumn('supportTicket', 'status', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'amountOfPeople', self::TYPE_SMALLINT, 6, null, false); // IT SHOULD BE SMALLint
        $this->createColumn('supportTicket', 'primaryMotif', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'secondaryMotif', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'additionalInfo', self::TYPE_MEDIUMTEXT, null, false);
        $this->createColumn('supportTicket', 'aimOfText', self::TYPE_INT, 11, null, false);
        
        
        $this->createIndex('supportTicket', 'accountId');
        $this->createForeignKey('supportTicket', array('accountId'), 'account', array('id'), 'FK_accountId');
        
        $this->createIndex('supportTicket', 'serviceId');
        $this->createForeignKey('supportTicket', array('serviceId'), 'service', array('id'), 'FK_serviceId');
        
        
       }

    public function down()
    {
        $this->dropTable('service');
        $this->dropTable('supportTicket');
    }
}

