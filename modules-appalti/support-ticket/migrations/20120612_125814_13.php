<?php

class SupportTicket_Migration_20120612_125814_13 extends Core_Migration_Abstract
{

    public function up()
    {
        $this->createTable('service');
        $this->createColumn('service', 'membershipTypeId', self::TYPE_INT, 11, null, false);
        $this->createColumn('service', 'name', self::TYPE_VARCHAR, 50);
        $this->createColumn('service', 'desc', self::TYPE_VARCHAR, 50);

        $this->createIndex('service', 'membershipTypeId');
        $this->createForeignKey('service', array('membershipTypeId'), 'membershipType', array('id'), 'FK_membershipTypeId');

        $this->createTable('servicePrice');
        $this->createColumn('servicePrice', 'membershipTypeId', self::TYPE_INT, 11, null, false);
        $this->createColumn('servicePrice', 'serviceId', self::TYPE_INT, 11, null, false);
        $this->createColumn('servicePrice', 'prise', self::TYPE_FLOAT, 9, 3);
        $this->createColumn('servicePrice', 'discount', self::TYPE_FLOAT, 9, 3);

        $this->createIndex('servicePrice', 'membershipTypeId');
        $this->createForeignKey('servicePrice', array('membershipTypeId'), 'membershipType', array('id'), 'FK_membershipTypeId');
        $this->createIndex('servicePrice', 'serviceId');
        $this->createForeignKey('servicePrice', array('serviceId'), 'service', array('id'), 'FK_serviceId');

        $this->createTable('supportTicket');
        $this->createColumn('supportTicket', 'accountId', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'serviceId', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'title', self::TYPE_VARCHAR, 250);
        $this->createColumn('supportTicket', 'numberOfPages', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'priority', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'createDatetime', self::TYPE_TIMESTAMP, null, false);
        $this->createColumn('supportTicket', 'modifiedDatetime', self::TYPE_TIMESTAMP, null, false);
        $this->createColumn('supportTicket', 'status', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'amountOfPeople', self::TYPE_SMALLINT, 6, null, false); // IT SHOULD BE SMALLint
        $this->createColumn('supportTicket', 'primaryMotif', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'secondaryMotif', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'aimOfText', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicket', 'contactPersonId', self::TYPE_INT, 11, null, false);

        $this->createIndex('supportTicket', 'accountId');
        $this->createForeignKey('supportTicket', array('accountId'), 'account', array('id'), 'FK_accountId');
        $this->createIndex('supportTicket', 'serviceId');
        $this->createForeignKey('supportTicket', array('service'), 'serviceId', array('id'), 'FK_serviceId');


        $this->createTable('supportTicketPost');
        $this->createColumn('supportTicketPost', 'supportTicketId', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicketPost', 'accountId', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicketPost', 'dateTime', self::TYPE_TIMESTAMP, null, false);
        $this->createColumn('supportTicketPost', 'data', self::TYPE_VARCHAR, 250);
        $this->createColumn('supportTicketPost', 'comment', self::TYPE_TEXT);
        /**
         * THIS FIELD SHOULD BE ENUM
         * VALUES ->client,support
         */
        $this->createColumn('supportTicketPost', 'type', self::TYPE_FLOAT);
        /**
         * THIS FIELD SHOULD BE ENUM
         * VALUES ->yes,no
         */
        $this->createColumn('supportTicketPost', 'sendMail', self::TYPE_FLOAT);

        $this->createIndex('supportTicketPost', 'accountId');
        $this->createForeignKey('supportTicketPost', array('accountId'), 'account', array('id'), 'FK_accountId');
        $this->createIndex('supportTicketPost', 'supportTicketId');
        $this->createForeignKey('supportTicketPost', array('supportTicketId'), 'supportTicket', array('id'), 'FK_supportTicketId');

        $this->createTable('supportTicketFiles');
        $this->createColumn('supportTicketFiles', 'ticketPostId', self::TYPE_INT, 11, null, false);
        $this->createColumn('supportTicketFiles', 'fileStockId', self::TYPE_INT, 11, null, false);

        $this->createIndex('supportTicketFiles', 'ticketPostId');
        $this->createForeignKey('supportTicketFiles', array('ticketPostId'), 'supportTicketPost', array('id'), 'FK_supportTicketPostId');
        $this->createIndex('supportTicketFiles', 'fileStockId');
        $this->createForeignKey('supportTicketFiles', array('fileStockId'), 'fileStock', array('id'), 'FK_fileStockId');

    }

    public function down()
    {
        $this->dropTable('service');
        $this->dropTable('servicePrice');
        $this->dropTable('supportTicket');
        $this->dropTable('supportTicketPost');
        $this->dropTable('supportTicketFiles');
    }


}

