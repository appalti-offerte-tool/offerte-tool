Ext.define('Module.ProposalBlock.List', {
    extend: 'Ext.ux.grid.GridPanel',

    features: [{
        ftype: 'filters'
    }],

    iconCls: 'm-rating-icon-16',

    modeReadOnly: false,

    categoryId: null,

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.ProposalBlock.Model.ProposalBlock',
            proxy: {
                type: 'ajax',
                url: link('proposal-block', 'main', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.columns = [{
            header: lang('Name'),
            dataIndex: 'name',
            flex: 2
        }, {
            header: lang('Kind'),
            dataIndex: 'kind',
            flex: 2
        }, {
            header: lang('Title'),
            dataIndex: 'title',
            flex: 1
        }];

        if (false === this.modeReadOnly) {
            this.columns.push({
                xtype: 'actioncolumn',
                header: lang('Actions'),
                width: 50,
                fixed: true,
                items: [{
                    tooltip: lang('Edit'),
                    iconCls: 'icon-edit-16 icon-16',
                    handler: function(g, rowIndex) {
                        this.onEditProposalBlock(g, g.getStore().getAt(rowIndex));
                    },
                    scope: this
                }, {
                    tooltip: lang('Delete'),
                    iconCls: 'icon-delete-16 icon-16',
                    handler: function(g, rowIndex) {
                        this.onDeleteProposalBlock(g, g.getStore().getAt(rowIndex));
                    },
                    scope: this
                }]
            });
        }

        this.tbar = [{
            text: lang('Create text block'),
            iconCls: 'icon-create-16',
            qtip: lang('Create new default proposal block'),
            handler: this.onCreateTextProposalBlock,
            scope: this
        }, '-', {
            text: lang('Create graphical block'),
            iconCls: 'icon-create-16',
            qtip: lang('Create new default proposal block'),
            handler: this.onCreateFileProposalBlock,
            scope: this
        }, '-'];

        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 3
        })];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent();

        if (false === this.modeReadOnly) {
            this.getView().on('itemdblclick', function(w, record) {
                this.onEditProposalBlock(this, record);
            }, this);
        }
    },

    onCreateTextProposalBlock: function() {
        Application.require([
            'proposal-block/./form'
        ], function() {
            var f = new Module.ProposalBlock.Main.Form({
                companyId: this.companyId,
                categoryId: this.categoryId,
                blockKind: 'text',
                creationFlag: true
            });
            f.doLoad();
            f.on('complete', function(form, categoryId) {
                this.setLastInsertedId(categoryId);
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onCreateFileProposalBlock: function() {
        Application.require([
            'proposal-block/./form'
        ], function() {
            var f = new Module.ProposalBlock.Main.Form({
                companyId: this.companyId,
                categoryId: this.categoryId,
                blockKind: 'file'
            });

            f.on('complete', function(form, categoryId) {
                this.setLastInsertedId(categoryId);
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditProposalBlock: function(g, record) {
        Application.require([
            'proposal-block/./form'
        ], function() {
            var f = new Module.ProposalBlock.Main.Form({
                categoryId: this.categoryId,
                companyId: this.companyId,
                blockId: record.get('id'),
                blockKind: record.get('kind')
            });
            f.doLoad();
            f.on('complete', function(form, blockId) {
                this.setLastInsertedId(blockId);
                g.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onDeleteProposalBlock: function(g, record) {
        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
            if (b != 'yes') {
                return;
            }

            this.deleteProposalBlock({
                categoryId: this.categoryId,
                companyId: this.companyId,
                blockId: record.get('id'),
                kind: record.get('kind')
            });
            
        }, this);


    },

    deleteProposalBlock: function(params) {
        Ext.Ajax.request({
            url: link('proposal-block', 'main', 'delete', {kind: params['kind'], format: 'json'}),
            method: 'POST',
            params: params,
            success: function(response, options) {
                var decResponse = Ext.decode(response.responseText);
                Application.notificate(decResponse.messages);

                if (true == decResponse.success) {
                    this.getStore().load();
                }
            },
            scope: this
        });
    },

    setCategoryId: function(categoryId, forceReload) {

        this.categoryId = categoryId;

        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.categoryId = categoryId;
        if (true === forceReload) {
            this.getStore().load();
        }

        return this;
    },
    
    setCompanyId: function(companyId, forceReload) {

        this.companyId = companyId;

        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.companyId = companyId;
        if (true === forceReload) {
            this.getStore().load();
        }

        return this;
    }

});