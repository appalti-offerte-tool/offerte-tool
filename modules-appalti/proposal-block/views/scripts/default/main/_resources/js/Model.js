Ext.define('Module.ProposalBlock.Model.ProposalBlock', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name', 'metadata', 'title', 'kind', 'categoryId', 'imageName', 
        {name: 'fitable', type: 'boolean'}
    ]
});