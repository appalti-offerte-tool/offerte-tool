Ext.define('Module.Application.Model.Error', {
    extend: 'Ext.data.Model',
    fields: [
        'message', 'type', 'field',
        {name: 'id', mapping: 'field'},
        {name: 'msg', mapping: 'message'}
    ]
});


Ext.define('Module.ProposalBlock.Main.Form', {
    extend: 'Ext.form.Panel',

    bodyPadding: 5,

    wnd: null,
    blockId: null,
    categoryId: null,
    defaults: { // defaults are applied to items, not the container
        labelWidth: 150
    },

    kindIsText: true,
    blockKind: 'text',

    creationFlag: false,

    image: null,

    model: 'Module.ProposalBlock.Model.ProposalBlock',

    fileStockImage: null,

    initComponent: function() {

        this.kindIsText = (this.blockKind === 'text');

        this.initialConfig.trackResetOnLoad = true;
        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.ProposalBlock.Model.ProposalBlock',
            type: 'json',
            root: 'row'
        });

        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
            model: 'Module.Application.Model.Error',
            type: 'json',
            root: 'messages',
            successProperty: 'success'
        });

        var types = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
            data : [
                {"id":"text", "name":"text"},
                {"id":"file", "name":"file"}
            ]
        });

        var activeTypes = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
            data : [
                {"id":"1", "name":"Enabled"},
                {"id":"0", "name":"Disabled"}
            ]
        });

        var fullPageTypes = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
            data : [
                {"id":"1", "name":"Yes"},
                {"id":"0", "name":"No"}
            ]
        });

        this.items = [{
            xtype: 'textfield',
            fieldLabel: lang('Title in bibliotheek'),
            name: 'name',
            allowBlank: 0,
            anchor: '100%'
        },{
            xtype: 'textfield',
            fieldLabel: lang('Title in offerte'),
            name: 'title',
            allowBlank: 0,
            anchor: '100%'
        },{
            xtype: 'module.proposal-block.category-main.combo-box',
            fieldLabel: lang('Type element'),
            name: 'categoryId',
            hiddenName: 'categoryId',
            value: this.categoryId,
            allowBlank: 0,
            anchor: '100%'
        },{
            xtype: 'hiddenfield',
            name: 'kind',
            value: this.blockKind
        }

//        ,{
//            xtype: 'htmleditor',
//            fieldLabel: lang('Tekst in offerte'),
//            name: 'metadata',
//            grow: true,
//            height: 400,
//            allowBlank: 0,
//            disabled: !this.kindIsText,
//            hidden: !this.kindIsText,
//            labelAlign: 'top',
//            anchor: '100%'
//        }

        ];

        if (this.kindIsText) {

            var field = Ext.widget('tinymcefield', {
                border: false,
                labelAlign: 'top',
                fieldLabel: lang('Tekst in offerte'),
                anchor: "100%",
                name: 'metadata',
                allowBlank: 0,
                margin: '0 0 12 0',
                height: 400,
                tinymceConfig: {
                    theme : "advanced",
                    plugins: "pagebreak,table,advhr,advimage,advlink,iespell,insertdatetime,media,searchreplace,print,contextmenu,paste",
                    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
                    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,|,forecolor,backcolor",
                    theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,iespell,media,advhr,|,print,|pagebreak",
                    theme_advanced_buttons4 : "",
                    theme_advanced_toolbar_location : "top",
                    theme_advanced_toolbar_align : "left",
                    theme_advanced_statusbar_location : "bottom",
                    theme_advanced_resizing : false,
                    extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
                    file_browser_callback : Ext.bind(this.fileCallbackFn, this),
                    baseURI: '',
                    relative_urls : false,
                    convert_urls : false,
                    skin: 'o2k7'
                }
            }, this);

            this.items.push(field);

        }

        if (!this.kindIsText) {
            this.image = new Module.FileStock.Image.Panel({
                margin: '0 5 5 155',
                bodyPadding: 5,
                height: 400,
                width: 400,
                useUploadFormField: true,
                onImageClickHandle: false,
                cacheable: false,
                field: {
                    fieldLabel: lang('Image'),
                    name: 'imageName'
                }
            });
            this.items.push(this.image);
        }


         this.items.push([{
            xtype: 'combobox',
            fieldLabel: lang('Neemt volledige pagina'),
            name: 'fitable',
            store: fullPageTypes,
            queryMode: 'local',
            displayField: 'name',
            value: 1,
            allowBlank: 0,
            valueField: 'id',
            editable: 0
        }]);

        this.callParent();

        this.addEvents('complete', 'actioncomplete', 'cancelcreation');

        this.on('actioncomplete', function(bf, action) {
            var row = bf.reader.jsonData.row;

            if (row.imageName && this.image) {
                this.image.setImageLink(row.imageName);
            }

            if (action.type == 'load' && row.id) {
                this.blockId = row.id;
            }

        }, this);

    },

    fileCallbackFn: function(field_name, url, type, win) {

        Application.require([
            'file-stock/image/image-layout'
        ], function() {

            var field = win.document.forms[0].elements[field_name];

            var fi = new Module.FileStock.Image.Layout({
                border: false,
                entityId: this.blockId,
                extraParams: {
                    companyId: this.companyId,
                    categoryId: this.categoryId,
                    kind: this.blockKind
                },
                enablePickerContextMenu: true,
                entityName: 'blockId',
                entityType: 'proposal-block',
                controller: 'proposal-block-file-stock',
                module: 'proposal-block'
            }, this);

            var w = new Ext.Window({
                modal: true,
                width: 650,
                height: 350,
                border: false,
                title: lang('Image dialogue'),
                layout: 'fit',
                items: [fi]
            });

            fi.on('choose-image', function(p, record, link) {

                field.value = link;
                Ext.fly(field).blur();

                if (Ext.isFunction(win.ImageDialog.getImageData)) {
                    win.ImageDialog.getImageData();
                }

                if (Ext.isFunction(win.ImageDialog.showPreviewImage)) {
                    win.ImageDialog.showPreviewImage(link);
                }
                w.close();
            }, this);

            fi.on('entity-change', function(image, blockId) {
                this.blockId = blockId;
            }, this);

            w.show(Ext.get(field));

        }, this);
    },

    doLoad: function() {

//        if (!this.blockId) {
//            return;
//        }

        var params = {
            format: 'json',
            blockId: this.blockId,
            companyId: this.companyId
        };

        if (this.creationFlag) {
            params['creationFlag'] = 1;
        }

        this.form.load({
            method: 'POST',
            url: link('proposal-block', 'main', 'fetch'),
            params: params,
            scope: this
        });
    },

    onSubmit: function() {

        if (!this.form.isValid()) {
            return false;
        }

        var action = this.blockId ? 'update' : 'create';
        var o = {};
        if (this.blockId) {
            o.blockId = this.blockId;
        }
        if (this.categoryId) {
            o.categoryId = this.categoryId;
        }
        if (this.companyId) {
            o.companyId = this.companyId;
        }

        this.form.submit({
            url: link('proposal-block', 'main', action, {format: 'json'}),
            params: o,
            success: function(options, action) {

                var decResponse = Ext.decode(action.response.responseText);
                Application.notificate(decResponse.messages);

                if (decResponse.success) {
                    this.fireEvent('complete', this, decResponse.blockId);
                    this.wnd && this.wnd.close();
                }
            },
            scope: this
        });
    },

    showInWindow: function() {
        this.border = false;
        var w = this.wnd = new Ext.Window({
            title: this.blockId ? lang('Update block') : lang('Create block'),
            resizable: false,
            layout: 'fit',
            iconCls: '',
            width: 800,
            autoHeight: true,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Save'),
                handler: this.onSubmit,
                scope: this
            }, {
                text: lang('Close'),
                handler: function() {
                    if (this.creationFlag && this.blockId && this.kindIsText) {
                       this.fireEvent('cancelcreation', this.blockId, this.blockKind);
                    }
                    w.close();
                    this.wnd = null;
                },
                scope: this
            }]
        });

        w.show();
        return w;
    }
});