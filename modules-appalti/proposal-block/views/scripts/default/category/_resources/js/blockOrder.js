(function($, win) {

    var upCls = 'icon-move-up',
        downCls = 'icon-move-down',
        upDisabledCls = 'icon-move-up-disabled',
        downDisabledCls = 'icon-move-down-disabled';

    function refreshPosArrowsState(el) {
        $('.' + upDisabledCls + ',.' + downDisabledCls, el).removeClass(upDisabledCls + ' ' + downDisabledCls);
        $('li.first, li.last', el).removeClass('first last');

        $('li:first', el)
            .addClass('first')
            .find('.' + upCls)
            .addClass(upDisabledCls);

        $('li:last', el)
            .addClass('last')
            .find('.' + downCls)
            .addClass(downDisabledCls);
    }

    function setOrderValues(el) {
        $('li', el).each(function() {
            $('input:hidden', this).val($(this).index())
        });

        refreshPosArrowsState(el);
    }

    var list = $('.reorder-blocks .sortable-list').sortable({
        cursor: 'move',
        placeholder: 'sortable-highlight',
        forcePlaceholderSize: true,
        forceHelperSize: true,
        tolerance: 'pointer',
        opacity: 0.6,
        stop: function(e, ui) {
            ui.item.css('zoom', 0);
            setOrderValues(list);
        }
    }).disableSelection();

    list.on('click', '[class^="icon-move-"]', function() {

        if (/-disabled/ig.test(this.className)) return;

        var li = $(this).closest('li');

        /-up/ig.test(this.className) ? li.insertBefore(li.prev()) : li.insertAfter(li.next());

        setOrderValues(list);
    });

    var form = list.closest('form').submit(function() {
        $.ajax({
            url: this.action,
            type: 'post',
            data: form.serialize(),
            mask: true,
            success: function(response) {
                if (response.success) {
                    form.closest('modal').modal('hide');
                    win.location.reload();
                } else {
                    alert(response.error);
                }
            }
        });

        return false;
    })

    setOrderValues(list);
})(jQuery, window);