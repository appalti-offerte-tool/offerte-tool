;(function($, win) {
    AppaltiFileBlock = function() {
        this.init();
    }
    AppaltiFileBlock.prototype = {

        constructor: AppaltiFileBlock,

        blockId: null,

        init: function() {
            $('body')
                .off('.AppaltiFileBlock')
                .on("click.AppaltiFileBlock", '#btn-add-upload-file-field',  function() {
                    afb.onAttachFileUploadField();
                    return false;
                });

            this.category = $('#categoryId');
            this.$form = $('#file-block-form');
            this.$modal = this.$form.closest('.modal')
                .off('hide hidden')
                .on('hidden', function() {
                    afb.$modal.empty();
                    afb.reloadOnClose && win.location.reload();
                })
                .on('hide', function() {
                    return !afb.$form.observable().isDirty() ||
                        !confirm('Blok gegevens wordt niet opgeslagen.\nKeer terug om op te slaan?');
                });

            $.validator.addMethod("imgOrPdf", function(value, element) {
                var pattern = /(\.jpe?g|\.gif|\.png|\.pdf)$/i;
                return !value || pattern.test(value);
            }, "Mogelijke bestandsformaten zijn: jpeg, jpg, gif, png, pdf.");


            $('#create-category').click(this.getCategoryForm);

            AppaltiLayout.initTooltip(this.$form);
        },

        onAttachFileUploadField: function() {
            var fieldsBox = $('#file-upload-fields');
            var uniqid = 'b' + Math.floor((Math.random()*100000)+1);

            var t = '<br/><input type="file" class="imgOrPdf" name="imageName'+uniqid+'" id="' + uniqid + '" />';
            fieldsBox.append(t);

            return false;
        },

        onCategoryCreate: function() {
            $.ajax({
                url: afb.categoryForm[0].action,
                type: 'post',
                data: afb.categoryForm.serialize(),
                success: function(response) {
                    if (response.success) {
                        var row = response.row,
                            parent = afb.category.find('option[value=' + row.parentId + ']'),
                            optgroup = null,
                            option = $('<option />', {
                                value: row.id,
                                text: row.name,
                                selected: 'selected'
                            });

                        if (parent.parent().is('select')) {
                            optgroup = $('<optgroup />', { label: parent.text() });
                            parent.replaceWith(optgroup);
                            optgroup.append($('<option />', {
                                value: row.parentId,
                                text: 'Uncategorized'
                            }))
                        } else {
                            optgroup = parent.parent('optgroup');
                        }

                        optgroup.prepend(option);
                        afb.categoryForm.observable().refresh();
                        afb.categoryModal.modal('hide');
                        afb.reloadOnClose = true;
                    } else {
                        alert(response.error);
                    }
                }
            });

            return false
        },

        getCategoryForm: function() {
            $.ajax({
                url: '/proposal-block/category/form',
                beforeSend: function() {
                    AppaltiLayout.mask('', afb.$form);
                },
                success: function(html) {
                    afb.categoryModal = $(html).appendTo('body'),
                    afb.categoryForm = $('form', afb.categoryModal);

                    afb.categoryForm.submit(afb.onCategoryCreate);

                    afb.categoryModal
                    .on('hide', function() {
                        return !afb.categoryForm.observable().isDirty() ||
                               confirm('Annuleren veranderingen?');
                    })
                    .on('hidden', function() {
                        afb.categoryModal.remove();
                        AppaltiLayout.unmask();
                    })
                    .modal('show');
                }
            });

            return false;
        }
    }

    win.AppaltiFileBlock = new AppaltiFileBlock();
    var afb = win.AppaltiFileBlock;

})(jQuery, window);