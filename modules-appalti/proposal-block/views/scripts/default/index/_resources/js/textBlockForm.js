;(function($, win) {
    var AppaltiTextBlock = function() {
        this.init();

        var interval = win.setInterval(function() {
            if (atb.$category.length) {
                atb.$category.triggerHandler('change');
                clearInterval(interval);
            }
        }, 200);
    };

    AppaltiTextBlock.prototype = {

        constructor: AppaltiTextBlock,

        libraryId: null,

        blockId: null,

        categoryData: null,

        init: function() {
            var me = this;

            $('body')
            .off('.AppaltiTextBlock')
            .on('click.AppaltiTextBlock', '.editorCommandTrigger', function() {
                var $el = $(this), cmd = $el.data('command');
                if (undefined !== win.tinyMCE && cmd) {
                    win.tinyMCE.execCommand(cmd);
                }
            });

            me.$form = $('#create-text-block-form').submit(function() {
                atb.tinymce.getEditor().save();
                return atb.$form.valid();
            });

            me.$modal = me.$form.closest('.modal')
                .off('hide hidden')
                .on('hidden', function() {
                    atb.$modal.empty();
                    atb.reloadOnClose && win.location.reload();
                })
                .on('hide', function() {
                    return !atb.$form.observable().isDirty() ||
                           !confirm('Blok gegevens wordt niet opgeslagen.\nKeer terug om op te slaan?');
                });

            me.$category = $('#categoryId')
                .off('.AppaltiTextBlock')
                .on('change.AppaltiTextBlock', function() {
                    var el = $(this),
                        selected = $(':selected', el),
                        regionIntroduction = el.data('region-introduction'),
                        hideTitle = regionIntroduction && this.value === regionIntroduction,
                        titleWrap = $('#titel-in-offerte'),
                        inputs = $('input', titleWrap);

                    titleWrap.toggleClass('hidden', hideTitle);

                    hideTitle ? inputs.attr('disabled', 'disabled') : inputs.removeAttr('disabled');

                    var id, d;

                    if (selected.parent().is('optgroup')) {
                        id = selected.siblings().andSelf().filter(':last').val();
                    } else {
                        id = this.value;
                    }

                    d = atb.categoryData[id];

                    $.get('/proposal/template/category-css/luid/' + (!!d ? d.luid : 'content'), function(data) {
                        win.tinyMceStyleSwitch(data);
                    });
                });

            me.isBlockDefinition = $('input[name="isBlockDefinition"]', this.$form).length > 0;

            me.tinymce = new AppaltiTinyMCE({
               onAjaxSave: function(data) {
                   me.$form.observable().refresh();
                   if (me.isBlockDefinition) return;
                   me.setUpdate(data);
               }
            });

            if (me.isBlockDefinition) {
                $(':submit', me.$form).click(function() {
                    $.when(me.tinymce.ajaxSave()).done(function() {
                        me.$modal.modal('hide');
                    });

                    return false;
                });
            }

            $('#create-category').click(me.getCategoryForm);

            AppaltiLayout.initTooltip(me.$form);
        },

        getCategoryId: function() {
            return this.$category.val();
        },

        onCategoryCreate: function() {
            $.ajax({
                url: atb.categoryForm[0].action,
                type: 'post',
                data: atb.categoryForm.serialize(),
                success: function(response) {
                    if (response.success) {
                        var row = response.row,
                            parent = atb.$category.find('option[value=' + row.parentId + ']'),
                            optgroup = null,
                            option = $('<option />', {
                                value: row.id,
                                text: row.name,
                                selected: 'selected'
                            });

                        if (parent.parent().is('select')) {
                            optgroup = $('<optgroup />', { label: parent.text() });
                            parent.replaceWith(optgroup);
                            optgroup.append($('<option />', {
                                value: row.parentId,
                                text: 'Uncategorized'
                            }));
                        } else {
                            optgroup = parent.parent('optgroup');
                        }

                        optgroup.prepend(option);
                        atb.$category.triggerHandler('change');
                        atb.categoryForm.observable().refresh();
                        atb.categoryModal.modal('hide');
                        atb.reloadOnClose = true;
                    } else {
                        alert(response.error);
                    }
                }
            });

            return false;
        },

        getCategoryForm: function() {
            $.ajax({
                url: '/proposal-block/category/form',
                beforeSend: function() {
                    AppaltiLayout.mask('', atb.$form);
                },
                success: function(html) {
                    atb.categoryModal = $(html).appendTo('body'),
                    atb.categoryForm = $('form', atb.categoryModal);

                    atb.categoryForm.submit(atb.onCategoryCreate);

                    atb.categoryModal
                    .on('hide', function() {
                        return !atb.categoryForm.observable().isDirty() ||
                               confirm('Annuleren veranderingen?');
                    })
                    .on('hidden', function() {
                        atb.categoryModal.remove();
                        AppaltiLayout.unmask();
                    })
                    .modal('show');
                }
            });

            return false;
        },

        setUpdate: function(data) {
            if (data.success) {
                if (!$('input[name="blockId"]', atb.$form).length) {
                    atb.$form
                        .attr('action', atb.$form[0].action.replace('/create/', '/update/'))
                        .prepend('<input type="hidden" name="blockId" value="' + data.blockId + '" />');
                }
            } else {
                alert(data.messages[0].message);
            }
        }
    };

    win.AppaltiTextBlock = new AppaltiTextBlock();
    var atb = win.AppaltiTextBlock;

})(jQuery, window);