Ext.define('Module.ProposalBlock.CategoryMain.Model.Category', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'name'
    ]
});