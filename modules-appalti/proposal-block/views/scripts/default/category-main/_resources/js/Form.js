Ext.define('Module.Application.Model.Error', {
    extend: 'Ext.data.Model',
    fields: [
        'message', 'type', 'field',
        {name: 'id', mapping: 'field'},
        {name: 'msg', mapping: 'message'}
    ]
});


Ext.define('Module.ProposalBlock.CategoryMain.Form', {
    extend: 'Ext.form.Panel',

    bodyPadding: 5,

    wnd: null,
    categoryId: null,
    model: 'Module.ProposalBlock.CategoryMain.Model.Category',

    fileStockImage: null,

    initComponent: function() {

        this.initialConfig.trackResetOnLoad = true;
        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.ProposalBlock.CategoryMain.Model.Category',
            type: 'json',
            root: 'row'
        });

        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
            model: 'Module.Application.Model.Error',
            type: 'json',
            root: 'messages',
            successProperty: 'success'
        });

        this.items = [{
            xtype: 'textfield',
            fieldLabel: lang('Name'),
            name: 'name',
            anchor: '100%'
        }];

        this.callParent();

        this.addEvents('complete');
    },

    doLoad: function() {

        if (!this.categoryId) {
            return;
        }

        this.form.load({
            url: link('proposal-block', 'category-main', 'fetch', {format: 'json', categoryId: this.categoryId}),
            scope: this
        });
    },

    onSubmit: function() {

        if (!this.form.isValid()) {
            return false;
        }

        var action = this.categoryId ? 'update' : 'create';
        var o = {};
        if (this.categoryId) {
            o.categoryId = this.categoryId;
        }

        this.form.submit({
            url: link('proposal-block', 'category-main', action, {format: 'json'}),
            params: o,
            success: function(options, action) {

                var decResponse = Ext.decode(action.response.responseText);
                Application.notificate(decResponse.messages);

                if (decResponse.success) {
                    this.fireEvent('complete', this, decResponse.categoryId);
                    this.wnd && this.wnd.close();
                }
            },
            scope: this
        });
    },

    showInWindow: function() {
        this.border = false;
        var w = this.wnd = new Ext.Window({
            title: this.categoryId ? lang('Update category') : lang('Create category'),
            resizable: false,
            layout: 'fit',
            iconCls: '',
            width: 350,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Save'),
                handler: this.onSubmit,
                scope: this
            }, {
                text: lang('Close'),
                handler: function() {
                    w.close();
                    this.wnd = null;
                },
                scope: this
            }]
        });

        w.show();
        return w;
    }
});