Ext.define('Module.ProposalBlock.CategoryMain.List', {
    extend: 'Ext.ux.grid.GridPanel',
    alias: 'widget.Module.ProposalBlock.CategoryMain.list',

    filterRequestParam: null,

    features: [{
        ftype: 'filters'
    }],

    iconCls: 'm-rating-icon-16',

    modeReadOnly: false,

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.ProposalBlock.CategoryMain.Model.Category',
            proxy: {
                type: 'ajax',
                url: link('proposal-block', 'category-main', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.columns = [{
            header: lang('Name'),
            dataIndex: 'name',
            flex: 1
        }];

        if (false === this.modeReadOnly) {
            this.columns.push({
                xtype: 'actioncolumn',
                header: lang('Actions'),
                width: 50,
                fixed: true,
                items: [{
                    tooltip: lang('Edit'),
                    iconCls: 'icon-edit-16 icon-16',
                    handler: function(g, rowIndex) {
                        this.onEditCategory(g, g.getStore().getAt(rowIndex));
                    },
                    scope: this
                }]
            });
        }

        this.tbar = [{
            text: lang('Create'),
            iconCls: 'icon-create-16',
            qtip: lang('Create new Category'),
            handler: this.onCreateCategory,
            scope: this
        }, '-'];

        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 2
        })];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent();

        if (false === this.modeReadOnly) {
            this.getView().on('itemdblclick', function(w, record) {
                this.onEditCategory(this, record);
            }, this);
        }
    },

    onCreateCategory: function() {
        Application.require([
            'proposal-block/category-main/form'
        ], function() {
            var f = new Module.ProposalBlock.CategoryMain.Form();

            f.on('complete', function(form, categoryId) {
                this.setLastInsertedId(categoryId);
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditCategory: function(g, record) {
        Application.require([
            'proposal-block/category-main/form'
        ], function() {
            var f = new Module.ProposalBlock.CategoryMain.Form({
                categoryId: record.get('id')
            });
            f.doLoad();
            f.on('complete', function(form, categoryId) {
                this.setLastInsertedId(categoryId);
                g.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    }

});