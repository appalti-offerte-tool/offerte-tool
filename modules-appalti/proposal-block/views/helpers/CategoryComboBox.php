<?php

class ProposalBlock_View_Helper_CategoryComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    protected $_rootCategoriesOnly = false;

    protected function _prepareOptions()
    {
        $translator = new Zend_View_Helper_Translate();
        $categoryService = new ProposalBlock_Service_Category();
        $response = $categoryService->fetchAllRootCategoriesWithResponse();
//        $this->_options[''] = $translator->translate('Select category');
        foreach($response->getRowset() as $categoryRow) {
            if ($this->_rootCategoriesOnly) {
                $this->_options[$categoryRow->getId()] = $categoryRow->name;
            } else {
                $children = $categoryRow->getChildren($this->libraryRow);
                if ($children->count()) {
                    $this->_options[$categoryRow->name] = array();
                    foreach ($children as $child) {
                        $this->_options[$categoryRow->name][$child->getId()] = $child->name;
                    }

                    $this->_options[$categoryRow->name][$categoryRow->getId()] = $translator->translate('Uncategorized');
                } else {
                    $this->_options[$categoryRow->getId()] = $categoryRow->name;
                }
            }
        }
    }

    public function categoryComboBox(Company_Model_LibraryRow $libraryRow, $rootCategoriesOnly = false)
    {
        $this->libraryRow = $libraryRow;
        $this->_rootCategoriesOnly = $rootCategoriesOnly;
        $this->_prepareOptions();
        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs,  $this->_options);
    }

    public function toLabel($value)
    {
        return $this->_options[$value];
    }

    public function toArray()
    {
        return $this->_options;
    }

}