;(function($, win) {
    /* Category of blocks */
    var Category = function(el, parent) {
        this.el = $(el);
        this.accordionEl = this.el.closest('.accordion');
        this.accordion = this.accordionEl.data('accordion');
        this.parent = parent;
        this.init();
    };

    Category.prototype = {
        children: {},

        modal: null,

        init: function() {
            this.id = this.el[0].id;
            this.library = this.el.data('library');
            this.hash = this.el.data('hash');
            this.titleEl = this.accordionEl.find('.accordion-section-title[data-id=' + this.id + ']');
            this.moveToCatForm = $(' > .lib-cat-head > .move-selected', this.el);

            var select = $('select', this.moveToCatForm);
            $('option[value="' + select.data('disable') + '"]', select).prop('disabled', 'disabled');

            var subCat = $('.lib-subcat', this.el);
            this.hasChildren = !!subCat.length;

            if (this.hasChildren) {
                var me = this;
                $('.sub-cat', subCat).each(function() {
                    me.children[this.id] = new Category(this, me);
                });
            }

            this.initEvents();
        },

        getForm: function(el, params) {
            var me = this;
            $(el).tooltip('hide');
            return $.ajax({
                url: '/proposal-block/category/form',
                data: params,
                mask: true,
                success: function(html) {
                    me.modal = $(html).appendTo('body')
                    .on('hide', function() {
                        return confirm('Mogen uw veranderingen verloren gaan?');
                    })
                    .on('hidden', function() {
                        $(this).remove();
                    })
                    .modal('show');
                    me.form = $('form', me.modal);
                }
            });
        },

        initEvents: function() {
            var me = this;
            !this.parent && me.titleEl.on('click', '.cat-create', function() {
                $.when(me.getForm(this, {
                    parentId: me.id,
                    libraryId: me.library
                })).done(function() {
                    me.form.submit(function() {
                        me.form.valid() && me.createChild();
                        return false;
                    });
                });
                return false;
            });

            me.titleEl.hasClass('accordion-section-title-sub') && me.titleEl
                .on('click', '.cat-rename', function(e) {
                    e.stopImmediatePropagation();
                    $.when(me.getForm(this, { id: me.id })).done(function() {
                        me.form.submit(function() {
                            me.form.valid() && me.renameChild();
                            return false;
                        });
                    });
                })
                .on('click', '.cat-delete', function(e) {
                    e.stopImmediatePropagation();
                    return confirm($(this).data('confirm'));
                });

            me.el
                .on('change', '.lib-cat-head > input:checkbox', function() {
                    $(this)
                    .parent()
                    .next()
                    .find('input:checkbox')
                    .prop('checked', this.checked ? 'checked' : false)
                    .each(function() { $(this).trigger('change'); } );
                })
                .on('change', '.lib-item-row > input:checkbox', function() {
                    var frm = me.moveToCatForm;

                    if (this.checked) {
                        frm.append($('<input />', {
                            type: 'hidden',
                            name: 'blocks[]',
                            value: this.value
                        }));
                    } else {
                        frm.find('input[value=' + this.value + ']').remove();
                    }

                    frm.toggleClass('invisible', !$('input[name="blocks[]"]', frm).length);
                })
                .on('click', '.reorder-items', function() {
                    $.ajax({
                        url: '/proposal-block/category/blocks-order-form',
                        type: 'post',
                        data: {
                            categoryId: $(this).data('category'),
                            libraryId: $(this).data('library')
                        },
                        mask: true,
                        success: function(html) {
                            $(html).appendTo('body')
                            .filter('.modal')
                            .on('hidden', function() {
                                $(this).remove();
                            })
                            .modal('show');
                        }
                    });

                    return false;
                });

            me.moveToCatForm
                .on('click', 'select', function(e) { e.stopPropagation(); })
                .on('change', 'select', function() {
                    if (parseInt(this.value)) {
                        me.moveToCatForm
                            .append($('<input />', {
                                type: 'hidden',
                                name: 'hash',
                                value: win.location.hash
                            }))
                            .submit();
                    }
                });

        },

        onSubmit: function(onSuccess) {
            var me = this;
            $.ajax({
                url: me.form[0].action,
                type: 'post',
                data: this.form.serialize(),
                success: function(response) {
                    if (response.success) {
                        $.isFunction(onSuccess) && onSuccess.call(me, response);
                    } else {
                        alert(response.error);
                    }
                }
            });
        },

        createChild: function() {
            this.onSubmit(function() {
                AppaltiLayout.mask('Reloading...');
                win.location.reload();
            });
        },

        renameChild: function() {
            var me = this;
            me.onSubmit(function(response) {
                $('.subcat-name', me.titleEl).html(response.name);
                me.form.observable().isDirty(false);
                me.modal.modal('hide');
            });
        }
    };

    /* Library */
    var ProposalBlocksLibrary = function() {
        $($.proxy(this.initMarkup, this));
    };

    ProposalBlocksLibrary.prototype = {

        constructor: ProposalBlocksLibrary,

        categories: {},

        initMarkup: function() {
            this.el = $('.library');
            this.initCategories();
            this.initBlockCreate();
        },

        initCategories: function() {
            var me = this;
            $('.root-cat', me.el).each(function() {
                me.categories[this.id] = new Category(this);
            });
        },

        initBlockCreate: function() {
            var me = this;
            me.el.on('click', 'a.action-create-block, a.library-block-action', function() {
                me.getModalForm(this);
                return false;
            });
        },

        getModalForm: function(action) {
            var me = this;
            return $.ajax({
                url: action.href,
                mask: true,
                success: function(data) {
                    if (!me.modalCnt) {
                        me.modalCnt = $('<div class="modal" data-backdrop="static"></div>').appendTo('body');
                    }

                    me.modalCnt
                        .toggleClass('fullscreen', !!$(action).data('fullscreen'))
                        .html(data)
                        .modal('show')
                        .find('#categoryId').val(me.activeCategoryId());
                }
            });
        },

        getActiveCategory: function(categories) {
            categories = categories || this.categories;
            var active;
            for (var id in categories) {
                active = categories[id].accordion.active;
                return active.length ? categories[active.data('id')] : null;
            }
        },

        activeCategoryId: function() {
            var activeRoot = this.getActiveCategory(), id = null;

            if (activeRoot) {
                id = activeRoot.id;
                if (activeRoot.hasChildren) {
                    var activeChild = this.getActiveCategory(activeRoot.children);
                    id = activeChild && !isNaN(activeChild.id) ? activeChild.id : id;
                }
            }

            return id;
        }
    };

    win.ProposalBlocksLibrary = new ProposalBlocksLibrary();
})(jQuery, window);