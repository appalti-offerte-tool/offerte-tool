;(function($, window) {

    var LibraryBlocks = function() {
        this.init();
    }

    function initMarkup() {
        lb.lib = $(".library");
    }

    function initEvents() {
        $('.blocks-container', lb.lib).on('click', 'a.icon-delete', function() {
            return confirm('Weet je zeker dat je dit item wilt verwijderen?')
        });
    }

    LibraryBlocks.prototype = {
        constructor: LibraryBlocks,

        onReload: $.noop,

        reloadUrl: "/lib/index/fetch-all",

        init: function() {
            $(function() {
                initMarkup();
                initEvents();
            })
        },

        reload: function() {
            $.get(lb.reloadUrl).done(function(data) {
                lb.lib.find('.lib-cat').empty().html(data);
                AppaltiLayout.initAccordion(lb.lib);
                lb.onReload();
            });
        }
    }

    window.LibraryBlocks = new LibraryBlocks();
    var lb = window.LibraryBlocks;
})(jQuery, window);