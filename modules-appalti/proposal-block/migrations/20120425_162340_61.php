<?php

class ProposalBlock_Migration_20120425_162340_61 extends Core_Migration_Abstract
{

    public function up()
    {
        $this->createColumn('libraryDefaultProposalBlock', 'title', self::TYPE_VARCHAR, 100);
        $this->createColumn('libraryProposalBlock', 'title', self::TYPE_VARCHAR, 100);
    }

    public function down()
    {
        $this->dropColumn('libraryDefaultProposalBlock', 'title');
        $this->dropColumn('libraryProposalBlock', 'title');
    }


}


//CREATE TABLE `appalti`.`proposalBlockFileStockRel` (
//`id` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
//`blockId` INT( 11 ) UNSIGNED NOT NULL ,
//`fileStockId` INT( 11 ) UNSIGNED NOT NULL ,
//`isThumbnail` INT( 1 ) UNSIGNED NOT NULL ,
//`parentId` INT( 1 ) UNSIGNED NULL DEFAULT NULL ,
//`isDefaultThumbnail` INT( 1 ) UNSIGNED NOT NULL DEFAULT '0',
//) ENGINE = InnoDB ;
//ALTER TABLE `appalti`.`proposalBlockFileStockRel` ADD UNIQUE `UX_blockId` ( `blockId` , `fileStockId` )
//ALTER TABLE `appalti`.`proposalBlockFileStockRel` ADD INDEX `IX_blockId` ( `blockId` )
//ALTER TABLE `appalti`.`proposalBlockFileStockRel` ADD INDEX `IX_fileStockId` ( `fileStockId` )

//ALTER TABLE `proposalBlock` ADD `emptyFlag` INT( 1 ) NOT NULL DEFAULT '0'




