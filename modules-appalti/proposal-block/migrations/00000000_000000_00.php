<?php

class Lib_Migration_00000000_000000_00 extends Core_Migration_Abstract
{

    public function up()
    {
        $this->createTable('libraryCategory');
        $this->createColumn('libraryCategory', 'name', self::TYPE_VARCHAR, 50);

        $this->createTable('libraryUserCategory');
        $this->createColumn('libraryUserCategory', 'accountId', self::TYPE_INT, 11, null, false);
        $this->createColumn('libraryUserCategory', 'libraryCategoryId', self::TYPE_INT, 11, null, false);
        $this->createColumn('libraryUserCategory', 'name', self::TYPE_VARCHAR, 50);

        $this->createIndex('libraryUserCategory', 'accountId');
        $this->createForeignKey('libraryUserCategory', array('accountId'), 'account', array('id'), 'FK_accountId');

        $this->createIndex('libraryUserCategory', 'libraryCategoryId');
        $this->createForeignKey('libraryUserCategory', array('libraryCategoryId'), 'libraryCategory', array('id'), 'FK_libraryCategoryId');

        $this->createTable('libraryDefaultProposalBlock');
        $this->createColumn('libraryDefaultProposalBlock', 'libraryCategoryId', self::TYPE_INT, 11, null, false);
        $this->createColumn('libraryDefaultProposalBlock', 'type', self::TYPE_VARCHAR, 50);
        $this->createColumn('libraryDefaultProposalBlock', 'data', self::TYPE_LONGTEXT);
        $this->createColumn('libraryDefaultProposalBlock', 'imageName', self::TYPE_VARCHAR, 50);
        $this->createColumn('libraryDefaultProposalBlock', 'isActive', self::TYPE_INT, 11);

        $this->createIndex('libraryDefaultProposalBlock', 'libraryCategoryId');
        $this->createForeignKey('libraryDefaultProposalBlock', array('libraryCategoryId'), 'libraryCategory', array('id'), 'FK_libraryCategoryId');

        $this->createTable('libraryProposalBlock');
        $this->createColumn('libraryProposalBlock', 'accountId', self::TYPE_INT, 11, null, false);
        $this->createColumn('libraryProposalBlock', 'libraryCategoryId', self::TYPE_INT, 11, null, false);
        $this->createColumn('libraryProposalBlock', 'libraryUserCategoryId', self::TYPE_INT, 11, null, false);
        $this->createColumn('libraryProposalBlock', 'type', self::TYPE_VARCHAR, 50);
        $this->createColumn('libraryProposalBlock', 'data', self::TYPE_LONGTEXT, 50);
        $this->createColumn('libraryProposalBlock', 'imageName', self::TYPE_VARCHAR, 50);
        $this->createColumn('libraryProposalBlock', 'isActive', self::TYPE_INT, 11);

        $this->createIndex('libraryProposalBlock', 'accountId');
        $this->createForeignKey('libraryProposalBlock', array('accountId'), 'account', array('id'), 'FK_accountId');

        $this->createIndex('libraryProposalBlock', 'libraryCategoryId');
        $this->createForeignKey('libraryProposalBlock', array('libraryCategoryId'), 'libraryCategory', array('id'), 'FK_libraryCategoryId');

        $this->createIndex('libraryProposalBlock', 'libraryUserCategoryId');
        $this->createForeignKey('libraryProposalBlock', array('libraryUserCategoryId'), 'libraryUserCategory', array('id'), 'FK_libraryUserCategoryId');
    }

    public function down()
    {
        $this->dropTable('libraryCategory');
        $this->dropTable('libraryUserCategory');
        $this->dropTable('libraryDefaultProposalBlock');
        $this->dropTable('libraryProposalBlock');
    }


}

