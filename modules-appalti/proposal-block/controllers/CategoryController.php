<?php

class ProposalBlock_CategoryController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    protected function _toCompanyRow()
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();

        if ($accountRow->isAdmin() && $this->_getParam('companyId')) {
            $companySrv = new \Company_Service_Company();
            $this->_company = $companySrv->find((int)$this->_getParam('companyId'));
        } else {
            $this->_company = $accountRow->getCompanyRow();
        }

        return $this->_company;
    }

    /**
     * @var \ProposalBlock_Service_Category
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_helper->ContextSwitch()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('save-blocks-order', 'json')
            ->initContext();

        parent::init();

        $this->_service = new ProposalBlock_Service_Category();
    }
    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'proposal-block:category';
    }

    public function indexAction()
    {
        // render view...
    }

    public function fetchAllAction()
    {
        $response = $this->_service->fetchAllRootCategoriesWithResponse();
        $response->setRowsetCallback(function($row) {
            return $row->toArray();
        });

        $this->view->assign($response->toArray());
    }

    public function formAction()
    {
        $this->_libraries = new Company_Service_Library($this->_toCompanyRow());
        $account = Zend_Auth::getInstance()->getIdentity();
        $company = $account->getCompanyRow();

        $categoryId = $this->_getParam('id');
        $libraryId = $this->_getParam('libraryId', 0);

        if ($libraryId)
        {
            $libraryRow = $this->_libraries->find($libraryId);
        }
        if ($categoryId) {
            $categoryRow = $this->_service->find($categoryId);
            $this->view->row = $categoryRow;
            $libraryRow = $this->_libraries->find($categoryRow->libraryId);
        }

        $this->view->assign($this->_getAllParams());
        $this->view->companyId = $company->getId();
        $this->view->libraryRow = $libraryRow;
    }

    public function createAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->_helper->information('Wrong request.');
            $this->_redirect('/proposal-block/category/index');
        }
        try {
            $row = $this->_service->create($this->_getAllParams());
            $this->view->success = true;
            $this->view->row = $row->toArray();
            $this->_helper->information(array('Category "%s" is toegevoegd.', array($row->name)), array(), E_USER_NOTICE);
//            $this->_redirect('/proposal-block/index/index/id/'.$this->_getParam('libraryId'));
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->error = $e->getMessages();
        }
    }

    public function updateAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->_helper->information('Wrong request.');
            $this->_redirect('/proposal-block/category/index');
        }

        try {
            $row = $this->_service->update($this->_getParam('id'), $this->_getAllParams());
            $this->view->id = $row->getId();
            $this->view->name = $row->name;
            $this->view->success = true;
            $this->_helper->information(array('Category "%s" is aangepast.', array($row->name)), array(), E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->error = $e->getMessages();
        }
    }

    public function deleteAction()
    {
        try {
            $id = $this->_getParam('id');
            // get category
            $categoryRow = $this->_service->find($id);

            // get library from category
            $this->_libraries = new Company_Service_Library($this->_toCompanyRow());
            $libraryRow = $this->_libraries->find($categoryRow->libraryId);

            // delete category
            $this->_service->delete($id);
            $this->_helper->information(array('Category "%s" is verwijderd.', array($categoryRow->name)), array(), E_USER_NOTICE);
        } catch (\Exception $e) {
            $this->_helper->information($e->getMessage());
        }

        $this->_redirect('/proposal-block/index/index/id/'.$libraryRow->getId().'#' . $this->_getParam('hash'));
    }

    public function blocksOrderFormAction()
    {
        $categoryId = $this->_getParam('categoryId');
        $libraryId = $this->_getParam('libraryId');
        if (!$categoryId || !$libraryId) {
            $this->_helper->information('Wrong request.');
            $this->_redirect('/proposal-block/category/index');
        }

        // get category
        $category = $this->_service->find($categoryId);
        $this->view->category = $category;

        // get library
        $this->_libraries = new Company_Service_Library($this->_toCompanyRow());
        $libraryRow = $this->_libraries->find($libraryId);
        $this->view->libraryRow = $libraryRow;

        // get library blocks
        $this->view->blocks = $category->getBlockRowset(null, $libraryRow);
    }

    public function saveBlocksOrderAction()
    {
        try {
            $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
            $proposalBlocksSrv = new \ProposalBlock_Service_ProposalBlock($companyRow);
            $proposalBlocksSrv->saveOrder($this->_getParam('blocks'));
            $this->view->success = true;
        } catch (\Exception $e) {
            $this->view->success = false;
            $this->view->error = $e->getMessage();
        }
    }
}
