<?php

class ProposalBlock_CategoryMainController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \ProposalBlock_Service_Category
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_helper->ContextSwitch()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->initContext();

        parent::init();

        $this->_service = new ProposalBlock_Service_Category();
    }
    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'proposal-block:category';
    }

    public function indexAction()
    {
//        $response = $this->_service->fetchAllRootCategoriesWithResponse();
//        $this->view->assign($response->toArray());
    }

    public function fetchAction()
    {
        $categoryRow = $this->_service->find($this->_getParam('categoryId'));
        $this->view->row = $categoryRow->toArray();
        $this->view->success = true;
    }

    public function fetchAllAction()
    {
        $response = $this->_service->fetchAllRootCategoriesWithResponse();
        $response->setRowsetCallback(function($row) {
            return $row->toArray();
        });

        $this->view->assign($response->toArray());
    }

    public function createAction()
    {
        if ( !$this->getRequest()->isPost()) {
            return;
        }
        
        try {
            $categoryMainRow = $this->_service->create($this->_getAllParams());
            $this->view->categoryId = $categoryMainRow->getId();
            $this->view->success = true;
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        if ( !$this->getRequest()->isPost()) {
            return;
        }

        try {
            $categoryRow = $this->_service->find($this->_getParam('categoryId'));
            $categoryRow = $this->_service->update($categoryRow->getId(), $this->_getAllParams());
            $this->view->categoryId = $categoryRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
            $this->getResponse()->setHeader('Content-type', 'text/html', true);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function comboBoxAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');
    }

}
