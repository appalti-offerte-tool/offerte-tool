<?php

class ProposalBlock_ProposalBlockFileStockController extends \Zend_Controller_Action implements \FileStock_Instance_EntityFileStockControllerInterface
{
    /**
     * @var \ProposalBlock_Service_ProposalBlockFileStock
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'proposal-block';
    }

    public function init()
    {
        $this->_service = new \ProposalBlock_Service_ProposalBlockFileStock($this->getEntityType());

        $this->_helper->contextSwitch()
           ->addActionContext('fetch-all', 'json')
           ->addActionContext('fetch', 'json')
           ->addActionContext('fetch-thumbnail', 'json')
           ->addActionContext('create', 'json')
           ->addActionContext('update', 'json')
           ->addActionContext('delete', 'json')
           ->addActionContext('set-default-thumbnail', 'json')
           ->initContext();
    }

    public function getEntityType()
    {
        return 'proposal-block';
    }

    public function indexAction()
    {
        try {
            $blockId = $this->_getParam('blockId');
            if (!empty($blockId)) {
                $response = $this->_service->fetchAllFileStockByProposalBlockIdWithResponse($blockId);
                $this->view->assign($response->toArray());
            }
            $account = Zend_Auth::getInstance()->getIdentity();
            $this->view->companyId = $account->getCompanyRow()->getId();
            $this->view->accountId = $account->getId();
            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function downloadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $fileStockRow = $this->_service->find($this->_getParam('blockId'), $this->_getParam('id'));

        $fileStockService = new FileStock_Service_FileStock();
        $fileStockService->download($fileStockRow, $this->getResponse(), true);
    }

    public function fetchAllAction()
    {
        try {
            $response = $this->_service->fetchAllFileStockByProposalBlockIdWithResponse($this->_getParam('blockId'));
            $this->view->assign($response->toArray());
            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAction()
    {
        $fileStockRow = $this->_service->find($this->_getParam('blockId'), $this->_getParam('fileStockId'));

        $this->view->data = $fileStockRow->toArray();
        $this->view->success = true;
    }

    public function fetchThumbnailAction()
    {
        try {
            $fileStockRow = $this->_service->fetchFileStockThumbnailRowByProposalBlock($this->_getParam('blockId'), $this->_getParam('fileStockId'));
            $this->view->assign($fileStockRow);
            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function createAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');

        try {

            list($articleRow, $fileStockRow) = $this->_service->create($this->_getAllParams());

            $this->view->success = true;
            $this->view->entityId = $articleRow->getId();
            $this->view->fileStockId = $fileStockRow->getId();

            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);

        } catch(OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function quickUploadAction()
    {
        $this->_helper->layout->disableLayout();
        try {
            list($blockRow, $fileStockRow) = $this->_service->create($this->_getAllParams());
            $this->view->success = true;
            $this->view->blockId = $blockRow->getId();
            $folder = 1000 + floor($fileStockRow->getId() / 1000) * 1000;
            $type = explode(".", $fileStockRow->getName());
            $type = $type[count($type)-1];
            $this->view->src = sprintf('/modules-appalti/proposal-block/data/block-kind-file/%s/%d.%s', $folder, $fileStockRow->getId(),$type);
        } catch(OSDN_Exception $e) {
            $this->view->success = false;
            $this->view->error = $e->getMessage();
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');

        try {

            $this->_service->update($this->_getAllParams());

            $this->view->success = true;
            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);

        } catch(OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->_service->delete($this->_getParam('blockId'), $this->_getParam('fileStockId'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }

    public function setDefaultThumbnailAction()
    {
        try {
            $this->_service->setDefaultThumbnail($this->_getParam('blockId'), $this->_getParam('fileStockId'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }
}