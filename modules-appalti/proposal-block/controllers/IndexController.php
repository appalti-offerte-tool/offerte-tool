<?php

class ProposalBlock_IndexController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \ProposalBlock_Service_ProposalBlock
     */
    protected $_service;

    protected $_kind;

    /**
     * @var Company_Model_CompanyRow
     */
    protected $_company;

    protected function _toCompanyRow()
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();

        if ($accountRow->isAdmin() && $this->_getParam('companyId')) {
            $companySrv = new \Company_Service_Company();
            $this->_company = $companySrv->find((int)$this->_getParam('companyId'));
        } else {
            $this->_company = $accountRow->getCompanyRow();
        }

        return $this->_company;
    }

    protected function _getService()
    {
        if (empty($this->_service)) {
            $this->_service = new \ProposalBlock_Service_ProposalBlock($this->_toCompanyRow());
        }

        return $this->_service;
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        /**
         * @FIXME Some kind of mock function for performance
         * In the index action we should display the list of categories with
         * assigned blocks. So... The service should be category
         */
        $this->_libraries = new Company_Service_Library($this->_toCompanyRow());
        if ($this->getRequest()->getActionName() == 'index') {
            return parent::init();
        }

        $this->_helper->ContextSwitch()
            ->addActionContext('delete', 'json')
            ->addContext('file', array(
                'suffix'    => 'file'
            ))
            ->addContext('text', array(
                'suffix'    => 'text'
            ))
            ->addActionContext('create', array('file', 'text'))
            ->addActionContext('update', array('file', 'text'))
            ->addActionContext('delete', array('file', 'text'))
            ->addActionContext('preview', array('file', 'text'))
            ->initContext();

        parent::init();
        $this->_kind = $this->_helper->ContextSwitch()->getCurrentContext();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'proposal-block';
    }

    public function indexAction()
    {
        // get library data
        $libraryRow = $this->_libraries->find($this->_getParam('id', 0));

        // get extra data
        if (!$this->getRequest()->isPost())
        {
            $blockCatSrv = new \ProposalBlock_Service_Category();
            $response = $blockCatSrv->fetchAllRootCategoriesWithResponse();
            $this->view->companyId = $this->_toCompanyRow()->getId();
            $this->view->libraryRow = $libraryRow;
            $this->view->libraryThemes = $this->_libraries->getThemes($libraryRow);
            $this->_themes = new Proposal_Service_CustomTemplate();
            $this->view->themes = $this->_themes->fetchAllActive();
            $this->view->response = $response;
            $this->view->isProposal = false;

            return $this->render('category/index', null, true);
        }

        try{
            $id = $this->_getParam('id', 0);
            $library = $this->_getParam('library');
            $library['companyId'] = $this->_toCompanyRow()->getId();
            $libraryRow = $this->_libraries->update($this->_libraries->find($id), $library);
            if (isset($library['themes']) && is_array($library['themes']))
            {
                $this->_libraries->setThemesById($libraryRow, $library['themes']);
            }
            $this->libraryThemes = $this->_libraries->getThemes($libraryRow);
            $this->view->success = true;
            $this->_helper->information('De aanpassingen zijn opgeslagen.', true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
        $this->_redirect($this->view->url(array('id'=>$libraryRow->getId())).( trim($hash)? '#'. $hash:'' ));
    }

    public function createAction()
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if (!$this->getRequest()->isPost()) {

            // get library
            $this->_libraries = new Company_Service_Library($accountRow->getCompanyRow());
            $libraryRow = $this->_libraries->find($this->_getParam('library', 0));
            $this->view->libraryRow = $libraryRow;

            // get empty proposalblock
//            $creation = $this->_getParam('creationFlag');
//            if (!empty($creation)) {
//                $blockRow = $this->_getService()->findEmpty(false);
//                if (!empty($blockRow)) {
//                    $this->view->blockRow = $blockRow;
//                }
//            }

            // set template
            $pt = new Proposal_Service_ProposalTheme($accountRow->getCompanyRow());
            $this->view->proposalTheme = $pt->getDefaultRow();
            return $this->render('form');
        }


        // get library
        $library = $this->_getParam('library', array('id'=>0));
        $this->_libraries = new Company_Service_Library($accountRow->getCompanyRow());
        $libraryRow = $this->_libraries->find($library['id']);
        $this->view->libraryRow = $libraryRow;

        try {
            $categoryService = new ProposalBlock_Service_Category();
            $categoryRow = $categoryService->find($this->_getParam('categoryId'));

            $hash = $categoryRow->name;
            if ($categoryRow->parentId) {
                $parentCat = $categoryService->find($categoryRow->parentId);
                $hash = $parentCat->name;
            }

            $params = $this->_getAllParams();
            $params['companyId'] = $accountRow->getCompanyRow()->getId();
            $params['accountId'] = $accountRow->getId();

            $blockRow = $this->_getService()->toServiceKind($this->_kind)->create($params);

            // add library proposalblock
            $this->_libraries->addProposalBlock($libraryRow, $blockRow->toArray());


            if ($this->getRequest()->isXmlHttpRequest()) {
                echo Zend_Json::encode(array_merge($blockRow->toArray(), array('success' => true)));
                die();
            } else {
                $this->view->blockId = $blockRow->getId();
                $this->view->success = true;
                $this->_helper->information(array('"%s" blok gemaakt', array($blockRow->name)), true, E_USER_NOTICE);
//                $this->_redirect('/proposal-block/#' . $hash);
            }
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
//            $this->_redirect('/proposal-block/#' . $hash);
        }
        $this->_redirect('/proposal-block/index/index/id/'.$libraryRow->getId().'#' . $hash);
    }

    public function updateAction()
    {
        $blockRow = $this->_getService()->find($this->_getParam('blockId'), false);
        if (null === $blockRow) {
            return $this->_redirect('/proposal-block/');
        }

        // get library
        $libraryRow = $this->_getService()->getLibrary($blockRow);
        $this->view->libraryRow = $libraryRow;

        $categoryService = new ProposalBlock_Service_Category();
        $categoryRow = $categoryService->find($blockRow->categoryId);
        $hash = $categoryRow->name;

        $this->view->blockRow = $blockRow;

        if (!$this->getRequest()->isPost())
        {
            return $this->render('form');
        }

        try {
            $blockRow = $this->_getService()->toServiceKind($this->_kind)->update($blockRow, $this->_getAllParams());
            if ($this->getRequest()->isXmlHttpRequest()) {
                echo Zend_Json::encode(array_merge($blockRow->toArray(), array('success' => true)));
                die();
            } else {
                $this->view->blockId = $blockRow->getId();
                $this->view->success = true;
                $this->_helper->information(array('"%s" blok bijgewerkt', array($blockRow->name)), true, E_USER_NOTICE);
            }
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
        $this->_redirect('/company/library/update/id/'.$libraryRow->getId() .'#'. $hash);
    }

    public function moveToAction()
    {
        if ($this->getRequest()->isPost()) {
            $params = $this->_getAllParams();
            if ($params['categoryId'] && $params['blocks']) {

                // get library
                $proposalBlockRow = $this->_getService()->find($params['blocks'][0]);
                $libraryRow = $this->_getService()->getLibrary($proposalBlockRow);

                // move blocks to new category
                $this->_getService()->moveToCategory($params['categoryId'], $params['blocks']);

                // reload page
                $this->_redirect('/proposal-block/index/index/id/'.$libraryRow->getId() . $params['hash']);

            } else {
                $this->_helper->information('Category is not defined');
            }
        } else {
            $this->_helper->information('Wrong request');
        }

    }

    public function previewAction()
    {
        $this->view->blockRow = $this->_getService()->find($this->_getParam('blockId'));

        if ($this->_getParam('iframe')) {
            $sectionSrv = new \Proposal_Service_Section();
            $categorySrv = new \ProposalBlock_Service_Category();
            $categoryRow = $categorySrv->find($this->_getParam('categoryId'));
            $sectionRow = $sectionSrv->findByRelatedCategoryId($categoryRow->parentId ?: $this->_getParam('categoryId'));

            $templateSrv = new \Proposal_Service_Template();

            $themeSrv = new \Proposal_Service_ProposalTheme($this->_toCompanyRow());
            $themeRow = $themeSrv->getDefaultRow();

            $cssArr = $templateSrv->getCssStyles($sectionRow, $themeRow);
            $this->view->css = !empty($cssArr) ? explode(',', $cssArr) : array();

            $this->view->layout()->disableLayout();

            return $this->render('preview-text-iframe');
        }
    }

    public function deleteAction()
    {
        $blockId = $this->_getParam('blockId');
//        if (!$this->getRequest()->isPost()) {
//            $this->_redirect('/proposal-block/');
//        }

        try {
            $blockRow = $this->_getService()->find($blockId);

            // get library
            $libraryRow = $this->_getService()->getLibrary($blockRow);
            $this->view->libraryRow = $libraryRow;

            $category = $blockRow->getCategoryRow();
            $blockName = $blockRow->name;
            $this->_getService()->toServiceKind($this->_kind)->delete($blockRow);
            $this->_helper->information(array('Blok "%s" verwijderd.', array($blockName)), array(), E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
        $this->_redirect('/proposal-block/index/index/id/'.$libraryRow->getId().'#' . $hash);
    }
}