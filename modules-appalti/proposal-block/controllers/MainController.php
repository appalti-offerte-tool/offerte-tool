<?php

class ProposalBlock_MainController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var ProposalBlock_Service_ProposalBlock
     */
    protected $_service;

    /*
     * @var \Company_Model_CompanyRow
     */
    protected $_companyRow;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $companyService = new \Company_Service_Company();
        $this->_companyRow = $companyService->find($this->_getParam('companyId'));
        $this->_service = new \ProposalBlock_Service_ProposalBlock($this->_companyRow);

        $this->_helper->contextSwitch()
            ->addActionContext('index', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch', 'json')
            ->initContext();

        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'proposal-block';
    }

    public function indexAction()
    {
        // prepare layout
    }

    public function fetchAllAction()
    {
        $response = $this->_service->fetchAllByCategoryId($this->_getParam('categoryId'), $this->_getAllParams());
        $this->view->assign($response->getRowset());
    }

    public function fetchAction()
    {
        $creation = $this->_getParam('creationFlag');

        if (!empty($creation)) {
            $blockRow = $this->_service->findEmpty(false);
        } else {
            $blockRow = $this->_service->find($this->_getParam('blockId'));
        }

        /**
         * @FIXME
         * I tried to find dependency
         * but do not know where this functionality is using
         * Currently I will disable this functionality
         *
         * @changedby Yaroslav
         */
        if (!empty($blockRow)) {
            $resArray = $blockRow->toArray();
            $resArray['imageName'] = null; //$blockRow->toFilePath();
            $this->view->row = $resArray;
        }

        $this->view->success = true;
    }

    public function createAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');
        try {
            $companyService = new \Company_Service_Company();
            $companyRow = $companyService->find($this->_getParam('companyId'));
            $params = $this->_getAllParams();
            $params['companyId'] = $companyRow->getId();
            $blockRow = $this->_service->toServiceKind($this->_getParam('kind'))->create($params);
            $this->view->blockId = $blockRow->getId();
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');
        try {
            $blockRow = $this->_service->find($this->_getParam('blockId'));
            $blockRow = $this->_service->toServiceKind($blockRow->getKind())->update($blockRow, $this->_getAllParams());
            $this->view->proposalBlockId = $blockRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $blockRow = $this->_service->find($this->_getParam('blockId'));
            $this->view->success = $this->_service->toServiceKind($blockRow->getKind())->delete($blockRow);
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}