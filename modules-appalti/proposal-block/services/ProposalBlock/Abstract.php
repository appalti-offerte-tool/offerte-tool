<?php

abstract class ProposalBlock_Service_ProposalBlock_Abstract extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \ProposalBlock_Model_DbTable_ProposalBlock
     */
    protected $_table;

    /**
     * @var \Company_Model_CompanyRow
     */
    protected $_companyRow;

    public function __construct(\Company_Model_CompanyRow $companyRow)
    {
        $this->_companyRow = $companyRow;
        if (!$this->_companyRow->isRoot()) {
            throw new OSDN_Exception('Only main companies can be accessed here');
        }

        parent::__construct();

        $this->_table = new ProposalBlock_Model_DbTable_ProposalBlock($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'companyId'     => array('allowEmpty' => false, 'presence' => 'required'),
            'name'          => array('allowEmpty' => false, 'presence' => 'required'),
            'title'         => array('allowEmpty' => false, 'presence' => 'required'),
            'kind'          => array('allowEmpty' => false, 'presence' => 'required')
        ));
    }
}