<?php

final class ProposalBlock_Service_ProposalBlock_Text
    extends ProposalBlock_Service_ProposalBlock_Abstract
    implements \ProposalBlock_Service_ProposalBlock_ServiceInterface
{
    /**
     * (non-PHPdoc)
     * @see ProposalBlock_Service_ProposalBlock_Abstract::_init()
     */
    protected function _init()
    {
        $this->_attachValidationRules('create', array(
            'metadata'     => array('allowEmpty' => false)
        ), 'default');

        $this->_attachValidationRules('update', array(
            'metadata'     => array('allowEmpty' => false)
        ), 'default');
        parent::_init();
    }

    /**
     * (non-PHPdoc)
     * @see ProposalBlock_Service_ProposalBlock_ServiceInterface::getKind()
     */
    public function getKind()
    {
        return 'text';
    }

    /**
     * (non-PHPdoc)
     * @see ProposalBlock_Service_ProposalBlock_ServiceInterface::create()
     */
    public function create(array $data)
    {
        $data['kind'] = $this->getKind();
        $data['companyId'] = empty($data['companyId']) ? $this->_companyRow->getId() : $data['companyId'];
        $data['accountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        $f = $this->_validate($data, 'create');

        $this->getAdapter()->beginTransaction();
        try {
            $blockRow = $this->_table->createRow($f->getData());
            $blockRow->save();

            $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to create block');
        }

        return $blockRow;
    }

    /**
     * (non-PHPdoc)
     * @see ProposalBlock_Service_ProposalBlock_ServiceInterface::update()
     */
    public function update(\ProposalBlock_Model_ProposalBlock $blockRow, array $data)
    {
        $data['emptyFlag'] = 0;
        $f = $this->_validate($data, 'update');

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if (
            $accountRow->isProposalBuilder() &&
            $accountRow->getId() != $blockRow->accountId
        ) {
            throw new OSDN_Exception('Restricted access to block data');
        }

        $blockRow->setFromArray($f->getData());

        $this->getAdapter()->beginTransaction();
        try {
            $blockRow->save();
            $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to update block');
        }

        return $blockRow;
    }

    /**
     * (non-PHPdoc)
     * @see ProposalBlock_Service_ProposalBlock_ServiceInterface::delete()
     */
    public function delete(\ProposalBlock_Model_ProposalBlock $blockRow)
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if (
            $accountRow->isProposalBuilder() &&
            $accountRow->getId() != $blockRow->accountId
        ) {
            throw new OSDN_Exception('Restricted access to block data');
        }

        $this->getAdapter()->beginTransaction();
        try {
            $blockRow->delete();

            $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to create block');
        }

        return true;
    }
}