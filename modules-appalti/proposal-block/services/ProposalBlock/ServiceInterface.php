<?php

interface ProposalBlock_Service_ProposalBlock_ServiceInterface
{
    public function create(array $data);

    public function update(\ProposalBlock_Model_ProposalBlock $blockRow, array $data);

    public function delete(\ProposalBlock_Model_ProposalBlock $blockRow);

    public function getKind();
}