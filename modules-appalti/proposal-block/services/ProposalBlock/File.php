<?php

final class ProposalBlock_Service_ProposalBlock_File
    extends ProposalBlock_Service_ProposalBlock_Abstract
    implements \ProposalBlock_Service_ProposalBlock_ServiceInterface
{
    protected $_imgResolution = 300;

    /**
     * (non-PHPdoc)
     * @see ProposalBlock_Service_ProposalBlock_Abstract::_init()
     */
    protected function _init()
    {
        $this->_attachValidationRules('create', array(
        ), 'default');

        $this->_attachValidationRules('update', array(
        ), 'default');
        parent::_init();
    }

    /**
     * (non-PHPdoc)
     * @see ProposalBlock_Service_ProposalBlock_ServiceInterface::getKind()
     */
    public function getKind()
    {
        return 'file';
    }

    /**
     * (non-PHPdoc)
     * @see ProposalBlock_Service_ProposalBlock_ServiceInterface::create()
     */
    public function create(array $data)
    {
        $data['kind'] = $this->getKind();
        $data['companyId'] = empty($data['companyId']) ? $this->_companyRow->getId() : $data['companyId'];
        $data['accountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        $f = $this->_validate($data, 'create');

        $this->getAdapter()->beginTransaction();
        try {
            $blockRow = $this->_table->createRow($f->getData());

            $blockRow->save();

            $transfer = new Zend_File_Transfer_Adapter_Http();
            $canFileProcess = true;
            foreach ($transfer->getFileInfo() as $key => $value) {
                if (!$transfer->isUploaded($key)) {
                    $canFileProcess = false;
                }
            }

            if ($canFileProcess) {
                $this->_onImageProcessing($blockRow, $transfer);
            }

            $blockRow->save();
            $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to create block');
        }

        return $blockRow;
    }

    /**
     * (non-PHPdoc)
     * @see ProposalBlock_Service_ProposalBlock_ServiceInterface::update()
     */
    public function update(\ProposalBlock_Model_ProposalBlock $blockRow, array $data)
    {
        $data['emptyFlag'] = 0;
        $f = $this->_validate($data, 'update');

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if (
            $accountRow->isProposalBuilder() &&
            $accountRow->getId() != $blockRow->accountId
        ) {
            throw new OSDN_Exception('Restricted access to block data');
        }

        $blockRow->setFromArray($f->getData());

        $this->getAdapter()->beginTransaction();
        try {
            $transfer = new Zend_File_Transfer_Adapter_Http();

            $canFileProcess = true;
            foreach ($transfer->getFileInfo() as $key => $value) {
                if (!$transfer->isUploaded($key)) {
                    $canFileProcess = false;
                }
            }

            if ($canFileProcess) {
                $this->_onImageProcessing($blockRow, $transfer);
            }

            $blockRow->save();

            $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to create block');
        }

        return $blockRow;
    }

    /**
     * (non-PHPdoc)
     * @see ProposalBlock_Service_ProposalBlock_ServiceInterface::delete()
     */
    public function delete(\ProposalBlock_Model_ProposalBlock $blockRow)
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if (
            $accountRow->isProposalBuilder() &&
            $accountRow->getId() != $blockRow->accountId
        ) {
            throw new OSDN_Exception('Restricted access to block data');
        }

        $this->getAdapter()->beginTransaction();
        try {
            if ($blockRow->isFileValid()) {
                $blockRow->dropFileCollection();
            }

            $blockRow->delete();

            $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to create block');
        }
        return true;
    }

    protected function _onImageProcessing(\ProposalBlock_Model_ProposalBlock $blockRow, \Zend_File_Transfer_Adapter_Abstract $transfer)
    {
        $companyRow = Zend_Auth::getInstance()->getInstance()->getIdentity()->getCompanyRow();

        /**
         * @FIXME Make path more abstract
         */
        $path = APPLICATION_PATH . '/modules-appalti/proposal-block/data/block-kind-file/';
        $path = $path . sprintf('%d/%d/', $companyRow->getId(), $blockRow->getId());

        if (!is_dir($path)) {
            mkdir($path, 0775, true);
            chmod($path, 0775);
        }
        $transfer->setDestination($path);

        $sectionService = new Proposal_Service_Section();
        $sectionRow = $sectionService->findByRelatedCategoryId($blockRow->getCategoryId(true));
        $isAttachment = $sectionRow->getLuid() === Proposal_Model_Section::SECTION_ATTACHMENT;

        $k = 0;
        $imageNames = array();

        foreach ($transfer->getFileInfo() as $key => $value) {

            $k++;

            $isPdf = false;
            do {
                $transfer->addValidator('MimeType', false, 'application/pdf', $key);
                if ($transfer->isValid($key)) {
                    $isPdf = true;
                    break;
                }

                $transfer->removeValidator('MimeType');
                $transfer->addValidator('IsImage', true, null, $key);
                if ($transfer->isValid($key)) {
                    break;
                }

                $e = new OSDN_Exception('Mogelijke bestandsformaten zijn: jpeg, jpg, gif, png, pdf.');
                $e->assign($transfer->getMessages());
                throw $e;

            } while(false);

            $fi = $transfer->getFileInfo($key);
            $extension = pathinfo($fi[$key]['name'], PATHINFO_EXTENSION);

            $filename = sprintf('%s-%d.%s', $blockRow->getId(), $k, $extension);
            $transfer->addFilter('Rename', array(
                'target'    => $path . $filename,
                'overwrite' => true
            ), $key);

            $transfer->receive($key);

            if (true !== $isPdf) {
                $imageNames[] = $filename;
                continue;
            }

            try {

                $im = new Imagick();
                $im->setResolution($this->_imgResolution, $this->_imgResolution);

                $pages = 1;
                if (true === $isAttachment) {
                    $im->readimage($path . $filename);
                    $pages = $im->getNumberImages();
                }

                /**
                 * set new max_execution_time cause on windows system
                 * large pdf conversion takes more time
                 */
                $isWindows = false !== stripos(PHP_OS, 'win');
                if ($isWindows) {
                    $timeLimit = ini_get('max_execution_time');
                    set_time_limit(120);
                }

                for($i = 0; $i < $pages; $i++) {
                    $im->readImage($path . sprintf('%s[%d]', $filename, $i));


                    $im->setOption('density','600x600');
                    $im->setImageCompressionQuality(92);

                    $im->setImageFormat('jpg');

                    $name = sprintf('%s-%d.jpg', $blockRow->getId(), $i);
                    $im->writeimage($path . $name);

                    $imageNames[] = $name;
                }

                if ($isWindows) {
                    set_time_limit((int)$timeLimit);
                }

            } catch (\Exception $e) {
                $e = new OSDN_Exception('Unable to convert PDF to image');
                $e->assign($e->getMessage());
                throw $e;
            }
        }

        $blockRow->imageName = implode(';', $imageNames);
        $blockRow->save();

        return true;
    }
}