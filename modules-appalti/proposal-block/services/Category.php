<?php

class ProposalBlock_Service_Category extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \ProposalBlock_Model_DbTable_Category
     */
    protected $_table;

    protected $_companyRow;

    public function __construct(Company_Model_CompanyRow $companyRow = null)
    {
        if ($companyRow)
        {
            $this->_companyRow = $companyRow;
        }
        parent::__construct();
    }
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new ProposalBlock_Model_DbTable_Category($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'name' => array('allowEmpty' => false, 'presence' => 'required'))
        );

        $this->_attachValidationRules('update', array(
            'name' => array('allowEmpty' => false, 'presence' => 'required')
        ), 'default');

        parent::_init();
    }

    /**
     * @param $id
     * @param bool $throwException
     * @return ProposalBlock_Model_Category
     * @throws OSDN_Exception
     */
    public function find($id, $throwException = true)
    {
        $row = $this->_table->findOne($id);
        if (null === $row && true === $throwException) {
            throw new \OSDN_Exception('Unable to find row #' . $id);
        }

        return $row;
    }

    /**
     * save new row
     *
     * @param array $data
     */
    public function create(array $data)
    {
        $f = $this->_validate($data);
        $row = $this->_table->createRow($f->getData());
        $row->save();

        return $row;
    }

    /**
     * return new row
     *
     * @param array $data
     */
    public function createRow(array $data)
    {
        $f = $this->_validate($data);
        return $this->_table->createRow($f->getData());
    }

    /**
     * @param $id
     * @param array $data
     * @return bool
     * @throws OSDN_Exception
     */
    public function update($id, array $data)
    {
        $row = $this->find($id);
        $f = $this->_validate($data, 'update');

        try {
            $row->setFromArray($f->getData());
            $row->save();
        } catch(\OSDN_Exception $e) {
            throw $e;
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to update');
        }

        return $row;
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $this->getAdapter()->beginTransaction();
        try {
            $row = $this->find($id);
            $row->delete();
            $this->getAdapter()->commit();
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to delete category.', null, $e);
        }

        return true;
    }

    public function fetchAllRootCategoriesWithResponse()
    {
        $select = $this->_table->select(true)
            ->where('parentId IS NULL')
            ->where('companyId IS NULL');

        return $this->getDecorator('response')->decorate($select, $this->_table);
    }

    public function fetchAll(Company_Model_LibraryRow $libraryRow = null)
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if (!$this->_companyRow)
        {
            $this->_companyRow = $accountRow->getCompanyRow();
        }

        $select = $this->_table->select(true)
//            ->where('companyId = ?', $this->_companyRow->getId())
        ;
        if ($libraryRow)
        {
            $select->where('libraryId = ?', $libraryRow->getId());
        }
        return $this->getDecorator('response')->decorate($select, $this->_table);
    }
}