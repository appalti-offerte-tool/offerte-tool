<?php

class ProposalBlock_Service_ProposalBlockFileStock extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \ProposalBlock_Model_DbTable_ProposalBlock
     */
    protected $_table;

    /**
     * @var \FileStock_Service_FileStock
     */
    protected $_fileStock;

    public function _init()
    {
        $this->_table = new \ProposalBlock_Model_DbTable_ProposalBlock($this->getAdapter());
        $this->_fileStock = new \FileStock_Service_FileStock(array(
            'baseFilePath'         => APPLICATION_PATH . '/modules-appalti/proposal-block/data/block-kind-file/',
            'useCustomExtension'   => false,
            'identity'             => 'proposal-block'
        ));
    }

    /**
     * Retrieve the filestock rows by article
     *
     * @param int $articleId
     * @return Zend_Db_Table_Rowset_Abstract
     * @throws OSDN_Exception
     */
    public function fetchAllFileStockByProposalBlockIdWithResponse($blockId)
    {
        $blockRow = $this->_table->findOne($blockId);
        if (empty($blockRow)) {
            throw new \OSDN_Exception('Unable to find #' . $blockId);
        }

        $fileStockTable = new \FileStock_Model_DbTable_FileStock($this->getAdapter());

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('df'    => 'proposalBlockFileStockRel'),
                array()
            )
            ->join(
                array('f'    => 'fileStock'),
                'f.id = df.fileStockId',
                $fileStockTable->getAllowedColumns()
            )
            ->join(
                array('dfThumbnail' => 'proposalBlockFileStockRel'),
                'dfThumbnail.parentId = df.id AND dfThumbnail.isThumbnail = 1',
                array(
                    'thumbnailFileStockId' => 'fileStockId',
                    'isThumbnail',
                    'isDefaultThumbnail'
                )
            )
            ->where('df.blockId = ?', $blockRow->getId(), Zend_Db::INT_TYPE)
            ->where('df.parentId IS NULL');

        $response = $this->getDecorator('response')->decorate($select);


        $storage = $this->_fileStock->factory();

        $response->setScope($this);
        $response->setRowsetCallback(function($row, $rowset, $scope) use ($storage, $blockRow) {

            $thumbnailFileStockRow = $scope->find($blockRow->getId(), $row['thumbnailFileStockId']);
            $row['thumbnailLink'] = $storage->toFilePathHost($thumbnailFileStockRow);

            $fileStockRow = $scope->find($blockRow->getId(), $row['id']);
            $row['link'] = $storage->toFilePathHost($fileStockRow);

            return $row;
        });

        return $response;
    }

    public function fetchFileStockThumbnailRowByProposalBlock($blockId, $fileStockId = null)
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('df'    => 'proposalBlockFileStockRel'),
                array('parentId')
            )
            ->join(
                array('f'    => 'fileStock'),
                'f.id = df.fileStockId',
                array('id')
            )
            ->join(
                array('dfp'    => 'proposalBlockFileStockRel'),
                'df.parentId = dfp.id',
                array()
            )
            ->where('df.blockId = ?', $blockId, Zend_Db::INT_TYPE)
            ->where('df.isThumbnail = 1')
            ->limit(1);

        if (!empty($fileStockId)) {
            $select->where('dfp.fileStockId = ?', $fileStockId, Zend_Db::INT_TYPE);
        }

        $foundRow = $select->query()->fetch();
        if (empty($foundRow)) {
            return null;
        }

        $fileStockId = $foundRow['id'];

        if (null == ($fileStockRow = $this->_fileStock->find($fileStockId))) {
            return null;
        }

        $row = $fileStockRow->toArray();

        $storage = $this->_fileStock->factory();

        if (!empty($foundRow['parentId'])) {
            $proposalBlockFileStockRelation = new ProposalBlock_Model_DbTable_ProposalBlockFileStockRel();
            $proposalBlockFileStockRelationRow = $proposalBlockFileStockRelation->findOne($foundRow['parentId']) ;
            $row['parentLink'] = $storage->toFilePathHost($proposalBlockFileStockRelationRow->getFileStockRow());

        }

        $row['link'] = $storage->toFilePathHost($fileStockRow);

        return $row;
    }

    public function find($blockId, $fileStockId)
    {
        /**
         * @todo
         * Implement verification on assignment article to filestock
         */
        return $this->_fileStock->find($fileStockId);
    }

    public function create(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {
        $account = Zend_Auth::getInstance()->getIdentity();

        if (empty($data['accountId'])) {
            $data['accountId'] = $account->getId();
        }

        if (empty($data['companyId'])) {
            $companyRow = $account->getCompanyRow();
            $data['companyId'] = $companyRow->getId();
        }

        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }

        if (empty($data['blockId']) || null === ($blockRow = $this->_table->findOne($data['blockId'])))
        {
            // create proposalblock
            $data['name'] = uniqid('Untitled ');
            $data['emptyFlag'] = 1;
            $blockRow = $this->_table->createRow($data);
            $blockRow->save();

            if (isset($data['libraryId']) && (int)$data['libraryId'])
            {
                // link proposalblock to library
                $_libraryProposalblocks = new Company_Model_DbTable_LibraryProposalblock();
                $libraryProposalblock = $_libraryProposalblocks->createRow();
                $libraryProposalblock->libraryId = $data['libraryId'];
                $libraryProposalblock->proposalblockId = $blockRow->getId();
                $libraryProposalblock->save();
            }
        }

        $proposalBlockFileStockRelation = new ProposalBlock_Model_DbTable_ProposalBlockFileStockRel();
        $proposalBlockFileStockRelationRow = null;

        $fileStockRow = $this->_fileStock->create($transfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
            use ($blockRow, $proposalBlockFileStockRelation, & $proposalBlockFileStockRelationRow) {

            $proposalBlockFileStockRelationRow = $proposalBlockFileStockRelation->createRow(array(
                'fileStockId' => $fileStockRow->getId(),
                'blockId'     => $blockRow->getId()
            ));
            $proposalBlockFileStockRelationRow->save();
        });

        $rawFileTransfer = new FileStock_Service_Transfer_Raw();

        $storage = $this->_fileStock->factory();
        $link = $storage->toFilePath($fileStockRow, true);

        $rawFileTransfer->addFile($link);

        /**
         * @FIXME
         */
        require_once APPLICATION_PATH . '/modules/file-stock/views/scripts/filters/File/Duplicate.php';
        $rawFileTransfer->addFilter(new FileStock_View_Filter_File_Duplicate($link . '.tmp', null, true));

        $isf = new Zend_Filter_ImageSize();
        $isf->setHeight(100);
        $isf->setWidth(100);
        $isf->setThumnailDirectory(dirname($link));
        $rawFileTransfer->addFilter($isf);

        $fileStockThumRow = $this->_fileStock->create($rawFileTransfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
            use ($blockRow, $proposalBlockFileStockRelationRow, $proposalBlockFileStockRelation) {

            $hasPredefinedThumbnail = $proposalBlockFileStockRelation->count(array(
                'blockId = ?' => $blockRow->getId(),
                'isThumbnail = 1'
            )) > 0;

            $blockRow->getTable()->getAdapter()->insert('proposalBlockFileStockRel', array(
                'fileStockId'            => $fileStockRow->getId(),
                'blockId'         => $blockRow->getId(),
                'isThumbnail'       => 1,
                'isDefaultThumbnail'=> ! $hasPredefinedThumbnail,
                'parentId'          => $proposalBlockFileStockRelationRow->id
            ));
        });

        return array($blockRow, $fileStockRow);
    }

    public function update(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {
        $proposalBlockRow = $this->_table->findOne($data['blockId']);
        if (empty($proposalBlockRow)) {
            throw new OSDN_Exception('Unable to find proposal block #' . $data['blockId']);
        }

        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }

        $this->getAdapter()->beginTransaction();
        try {
            $fileStockRow = $this->_fileStock->update($transfer, $data);

            $proposalBlockFileStockRelation = new ProposalBlock_Model_DbTable_ProposalBlockFileStockRel();
            $proposalBlockFileStockRelationRow = $proposalBlockFileStockRelation->fetchRow(array(
                'blockId = ?'    => $proposalBlockRow->getId(),
                'fileStockId = ?'       => $fileStockRow->getId(),
                'parentId IS NULL'
            ));

            if (null !== $proposalBlockFileStockRelationRow) {

                foreach($proposalBlockFileStockRelationRow->getChildrenRowset() as $row) {
                    $this->_fileStock->delete($row->fileStockId, function($fileStockRow) use ($row) {
                        $row->delete();
                    });
                }
            }

            $rawFileTransfer = new FileStock_Service_Transfer_Raw();

            $storage = $this->_fileStock->factory();
            $link = $storage->toFilePath($fileStockRow, true);

            $rawFileTransfer->addFile($link);

            /**
             * @FIXME
             */
            require_once APPLICATION_PATH . '/modules/file-stock/views/scripts/filters/File/Duplicate.php';
            $rawFileTransfer->addFilter(new FileStock_View_Filter_File_Duplicate($link . '.tmp', null, true));

            $isf = new Zend_Filter_ImageSize();
            $isf->setHeight(100);
            $isf->setWidth(100);
            $isf->setThumnailDirectory(dirname($link));
            $rawFileTransfer->addFilter($isf);

            $fileStockThumRow = $this->_fileStock->create($rawFileTransfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow) use ($proposalBlockRow, $proposalBlockFileStockRelationRow) {
                $proposalBlockRow->getTable()->getAdapter()->insert('proposalBlockFileStockRel', array(
                    'fileStockId'  => $fileStockRow->getId(),
                    'blockId'      => $proposalBlockRow->getId(),
                    'isThumbnail'  => 1,
                    'parentId'     => $proposalBlockFileStockRelationRow->id
                ));
            });


            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $fileStockRow;
    }

    public function delete($blockId, $fileStockId)
    {
        $proposalBlockRow = $this->_table->findOne($blockId);
        if (empty($proposalBlockRow)) {
            throw new OSDN_Exception('Unable to find proposal block #' . $blockId);
        }

        $this->getAdapter()->beginTransaction();
        try {

            $proposalBlockFileStockRelation = new \ProposalBlock_Model_DbTable_ProposalBlockFileStockRel();
            $scope = $this->_fileStock;

            $callbackFn = function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
                use ($proposalBlockRow, $proposalBlockFileStockRelation, $scope, & $callbackFn)
            {
                $proposalBlockFileStockRelationRow = $proposalBlockFileStockRelation->fetchRow(array(
                    'blockId = ?'    => $proposalBlockRow->getId(),
                    'fileStockId = ?'       => $fileStockRow->getId()
                ));

                if (null !== $proposalBlockFileStockRelationRow) {

                    foreach($proposalBlockFileStockRelationRow->getChildrenRowset() as $row) {
                        $scope->delete($row->fileStockId, $callbackFn);
                    }

                    $proposalBlockFileStockRelationRow->delete();
                }
            };

            $result = $this->_fileStock->delete($fileStockId, $callbackFn);
            $this->getAdapter()->commit();

        } catch (\Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        }

        return (boolean) $result;
    }

    public function deleteByProposalBlock(ProposalBlock_Model_DbTable_ProposalBlockRow $proposalBlockRow)
    {
        $proposalBlockFileStockRelation = new \ProposalBlock_Model_DbTable_ProposalBlockFileStockRel();
        $rowset = $proposalBlockFileStockRelation->fetchAll(array(
            'blockId = ?'    => $proposalBlockRow->getId()
        ));

        foreach($rowset as $row) {
            $this->delete($proposalBlockRow->getId(), $row->fileStockId);
        }

        return true;
    }

    public function setDefaultThumbnail($blockId, $fileStockId)
    {
        $proposalBlockFileStockRelation = new \ProposalBlock_Model_DbTable_ProposalBlockFileStockRel();

        $this->getAdapter()->beginTransaction();
        try {

            $proposalBlockFileStockRelation->updateQuote(array(
                'isDefaultThumbnail' => 0
            ), array(
                'blockId = ?' => (int) $blockId,
                'isThumbnail = 1'
            ));

            $proposalBlockFileStockRelationRow = $proposalBlockFileStockRelation->fetchRow(array(
                'blockId = ?' => (int) $blockId,
                'fileStockId = ?'    => (int) $fileStockId
            ));

            if (null == $proposalBlockFileStockRelationRow) {
                throw new OSDN_Exception('Unable to find row');
            }

            $affectedRows = $proposalBlockFileStockRelation->updateQuote(array(
                'isDefaultThumbnail' => 1
            ), array(
                'blockId = ?' => (int) $blockId,
                'parentId = ?'  => (int) $proposalBlockFileStockRelationRow->id,
                'isThumbnail = 1'
            ));

            if (0 == $affectedRows) {
                throw new \OSDN_Exception('Unable to change default thumbnail');
            }

            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }
    }

    public function getStorage() {
        return $this->_fileStock->factory();
    }

}