<?php

class ProposalBlock_Service_ProposalBlock extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \ProposalBlock_Model_DbTable_ProposalBlock
     */
    protected $_table;

    /**
     * @var \Company_Model_CompanyRow
     */
    protected $_companyRow;

    /**
     * @var \Account_Model_DbTable_AccountRow
     */
    protected $_accountRow;

    public function __construct(\Company_Model_CompanyRow $companyRow)
    {
        $this->_companyRow = $companyRow;
        if (!$this->_companyRow->isRoot()) {
            throw new OSDN_Exception('Only main companies can be accessed here');
        }

        $this->_accountRow = Zend_Auth::getInstance()->getIdentity();

        parent::__construct();

        $this->_table = new ProposalBlock_Model_DbTable_ProposalBlock($this->getAdapter());
    }

    public function fetchAll(Company_Model_LibraryRow $libraryRow = null, Company_Model_CompanyRow $companyRow = null)
    {
        $select = $this->_table->getAdapter()
            ->select()
            ->from(
                array('r' => $this->_table->getTableName()),
//                $this->_table->getAllowedColumns(\OSDN_Db_Table_Abstract::OMIT_FETCH_ROW)
                '*'
            )
            ->where('r.emptyFlag = 0')
        ;
        if($companyRow)
        {
            $select->where('r.companyId = ?', $companyRow->getId(), Zend_Db::INT_TYPE);
        }
        elseif ($libraryRow)
        {
            $select->join(
                        array('lp'=>'libraryProposalblock'),
                        'lp.proposalblockId = r.id AND lp.libraryId='.(int)$libraryRow->getId()
            )->where('r.companyId = ?', $libraryRow->companyId, Zend_Db::INT_TYPE);
        }
        else
        {
            $select->where('r.companyId = ?', $this->_companyRow->getId(), Zend_Db::INT_TYPE);
        }
        $select->order(array('r.position ASC'));
        if (!empty($params)) {
            $this->_initDbFilter($select, $this->_table)->parse($params);
        }
        return $this->_table->getAdapter()
                                ->fetchAll($select);
//        return $this->getDecorator('response')->decorate($select);
    }

    public function fetchAllByCategoryId($categoryId, array $params = array(), Company_Model_LibraryRow $libraryRow = null)
    {
        if (empty($categoryId)) {
            throw new OSDN_Exception('Category id is not defined.');
        }

        if (null === $libraryRow)
        {
            $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
            $libraryRow = new Company_Model_LibraryRow();
            $libraryRow->companyId = $companyRow->getId();
        }

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('r' => $this->_table->getTableName()),
                $this->_table->getAllowedColumns(\OSDN_Db_Table_Abstract::OMIT_FETCH_ROW)
            )
            ->join(array('lpb' => 'libraryProposalblock'),'lpb.proposalblockId = r.id')
            ->where('lpb.libraryId = ?', $libraryRow->getId(), Zend_Db::INT_TYPE)
            ->where('r.categoryId = ?', $categoryId, Zend_Db::INT_TYPE)
            ->where('r.emptyFlag = 0')
            ->where('r.companyId = ?', $this->_companyRow->getId(), Zend_Db::INT_TYPE)
            ->order(array('r.position ASC'));

//        if ($this->_accountRow->isProposalBuilder()) {
//            $select->where('r.accountId = ?', $this->_accountRow->getId(), Zend_Db::INT_TYPE);
//        }

        if (!empty($params)) {
            $this->_initDbFilter($select, $this->_table)->parse($params);
        }

        return $this->getDecorator('response')->decorate($select);
    }

    /**
     * Retrieve Block
     *
     * @param $id
     * @param bool $throwException
     * @throws OSDN_Exception
     *
     * @return \ProposalBlock_Model_ProposalBlock
     */
    public function find($blockId, $throwException = true)
    {
        $clause = array( 'id = ?' => (int) $blockId );

        if (!$this->_accountRow->isAdmin()) {
            $clause['companyId = ?'] = $this->_companyRow->getId();
        }

//        if ($this->_accountRow->isProposalBuilder()) {
//            $clause['accountId = ?'] = (int) $this->_accountRow->getId();
//        }

        $blockRow = $this->_table->fetchRow($clause);
        if (null === $blockRow && true === $throwException) {
            throw new \OSDN_Exception('Unable to find row #' . $blockId);
        }

        return $blockRow;
    }

    /**
     * Retrieve Block
     *
     * @param $id
     * @param bool $throwException
     * @throws OSDN_Exception
     */
    public function findEmpty($throwException = true)
    {
        $clause = array(
            'emptyFlag = 1',
            'companyId = ?' => $this->_companyRow->getId(),
            'accountId = ?' => $this->_accountRow->getId()
        );

        $blockRow = $this->_table->fetchRow($clause);
        if (null === $blockRow && true === $throwException) {
            throw new \OSDN_Exception('Unable to find empty row');
        }

        return $blockRow;
    }

    /**
     * @param string $kind
     * @return \ProposalBlock_Service_ProposalBlock_ServiceInterface
     */
    public function toServiceKind($kind)
    {
        if (is_array($kind)) {
            if (empty($kind['kind'])) {
                throw new OSDN_Exception('Unable to initialize service');
            }

            $kind = $kind['kind'];
        }

        if (!in_array($kind, array('file', 'text'))) {
            throw new OSDN_Exception('Wrong kind of block');
        }

        $serviceCls = '\ProposalBlock_Service_ProposalBlock_' . ucfirst(strtolower($kind));
        $serviceObj = new $serviceCls($this->_companyRow);

        return $serviceObj;
    }

    /**
     * @param int $categoryId Parent category id
     * @param array $blocks Ids of blocks to update
     * @return int Number of rows updated false If some exceptions throws
     */
    public function moveToCategory($categoryId, array $blocks)
    {
        return $this->_table->update(array('categoryId' => $categoryId), array('id IN (?)' => $blocks));
    }

    public function saveOrder($data)
    {
        $tblName = $this->_table->getTableName();
        $sql = "UPDATE $tblName SET `position` = CASE id ";

        foreach($data as $id => $order){
            $sql .= " WHEN $id THEN $order";
        }

        $sql .= " END WHERE id IN (" . implode(',', array_keys($data)) . ")";

        return $this->getAdapter()->query($sql);
    }


    /**
     * @param array $data
     */
    public function createRow(array $data = array())
    {
        $f = $this->_validate($data);
        return $this->_table->createRow($f->getData());
    }

    /**
     * @param array $proposalblock
     * @return bool true on success, false on failure
     */
    public function deleteProposalblock(array $proposalBlock)
    {
        if ($this->_table->delete('id = '.(int)$proposalBlock['id']))
            return true;
        return false;
    }


    /**
     * @param ProposalBlock_Model_ProposalBlock $proposalBlock
     * @return Company_Model_LibraryRow
     */
    public function getLibrary(ProposalBlock_Model_ProposalBlock $proposalBlockRow)
    {
        $adapter = $this->_table->getAdapter();
        $select = $adapter->select()
                            ->from(array('l'=>'library'))
                            ->join(array('lpb'=>'libraryProposalblock'), 'lpb.libraryId = l.id', array())
                            ->where('lpb.proposalblockId = ?', $proposalBlockRow->id)
        ;
        $libraryData = $adapter->fetchRow($select);
        $libraryRow = new Company_Model_LibraryRow(array('data'=>$libraryData));
        return $libraryRow;
    }

}