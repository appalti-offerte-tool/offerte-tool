<?php

final class ProposalBlock_Block_CategoryBlockList extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Category blocks');
    }

    protected function _toHtml()
    {
        $pagination = new \OSDN_Pagination();
        $pagination->setItemsCountPerPage(10);

        $account = Zend_Auth::getInstance()->getIdentity();
        if ($account->isAdmin() || $this->_getParam('companyId')) {
            $companySrv = new \Company_Service_Company();
            $companyRow = $companySrv->find($this->_getParam('companyId'));
        } else {
            $companyRow = $account->getCompanyRow();
        }

        $libraryRow = $this->_getParam('libraryRow', null);
        if(null === $libraryRow && $this->getRequest()->getParam('id'))
        {
            $libraryRow = $companyRow->getLibrary($this->getRequest()->getParam('id'));
        }
        if (null === $libraryRow)
        {
            $libraryRow = new Company_Model_LibraryRow();
            $libraryRow->companyId = $companyRow->getId();
        }
        $this->view->libraryRow = $libraryRow;

        $this->view->companyId = $companyRow->getId();

        $categoryId = $this->_getParam('categoryId');
        $categorySrv = new \ProposalBlock_Service_Category();
        $categoryRow = $categorySrv->find($categoryId);
        $this->view->categoryRow = $categoryRow;

        $proposalBlockSrv = new \ProposalBlock_Service_ProposalBlock($companyRow);
        $response = $proposalBlockSrv->fetchAllByCategoryId($categoryId, $this->getRequest()->getParams(), $libraryRow);

        $table = new \ProposalBlock_Model_DbTable_ProposalBlock();
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });

        $this->view->rowset = $response->getRowset();

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;

        $this->view->isProposal = $this->_getParam('isProposal', false);
        $this->view->listOnly = $this->_getParam('listOnly');
    }
}