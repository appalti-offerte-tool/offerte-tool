<?php

class ProposalBlock_Block_BlockList extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Blocks');
    }

    protected function _toHtml()
    {}
}