<?php

final class ProposalBlock_Block_CategoryList extends \Application_Block_Abstract
{
    /**
     * (non-PHPdoc)
     * @see Application_Block_Abstract::_toTitle()
     */
    protected function _toTitle()
    {
        return $this->view->translate('Blocks category list');
    }

    /**
     * (non-PHPdoc)
     * @see Application_Block_Abstract::_toHtml()
     */
    protected function _toHtml()
    {
        $service = new ProposalBlock_Service_Category();
        $this->view->response = $service->fetchAllRootCategoriesWithResponse();

        $this->view->definitions = $this->_getParam('definitions', array());
        $this->view->companyId = $this->_getParam('companyId');
        $this->view->isProposal = $this->_getParam('isProposal', false);
        $_libraries = new Company_Service_Library();
        $this->view->libraryRow = $_libraries->find($this->_getParam('libraryId'), 0);
    }
}