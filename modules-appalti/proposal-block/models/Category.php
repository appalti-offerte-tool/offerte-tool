<?php

class ProposalBlock_Model_Category extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Indicate that block in this category will
     * rendered on separated page
     *
     * @return boolean
     */
    public function inBetween()
    {
        return (boolean) $this->inBetween;
    }

    public function getBlockCount(Company_Model_LibraryRow $libraryRow = null)
    {
        if (null === $libraryRow) {
            $accountRow = \Zend_Auth::getInstance()->getIdentity();
            $companyId = $accountRow->getCompanyRow()->getId();

            $libraryRow = new Company_Model_LibraryRow();
            $libraryRow->companyId = $companyId;
        }

        $select = $this->getTable()->getAdapter()->select()
            ->from(
                array('pb' => 'proposalBlock'),
                array(
                    new Zend_Db_Expr('COUNT(*)')
                )
            )
            ->join(array('lpb' => 'libraryProposalblock'),'lpb.proposalblockId = pb.id')
            ->where('lpb.libraryId = ?', $libraryRow->getId(), Zend_Db::INT_TYPE)
            ->where('pb.companyId = ?', $libraryRow->companyId, Zend_Db::INT_TYPE)
            ->where('pb.categoryId = ?', $this->getId(), Zend_Db::INT_TYPE)
        ;
//        if ($accountRow->isProposalBuilder()) {
//            $select->where('pb.accountId = ?', $accountRow->getId());
//        }

        $select->where('pb.emptyFlag = 0');

        return $select->query()->fetchColumn();
    }

    /**
     * Retrieve block rowset
     *
     * @return Zend_Db_Table_Rowset
     */
    public function getBlockRowset($companyId = null, Company_Model_LibraryRow $libraryRow = null)
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();

        $select = $this->select();

        if ($accountRow->isAdmin() && $companyId) {
            $select->where('companyId = ?', $companyId, Zend_Db::INT_TYPE);
        } else {
            $select->where('companyId = ?', $accountRow->getCompanyRow()->getId(), Zend_Db::INT_TYPE);
        }
        $select->order('position ASC');
        $select->where('emptyFlag = 0');
        if (null !== $libraryRow)
        {
            $sql = $this->_table->getAdapter()
                                    ->select()
                                    ->from('libraryProposalblock', 'proposalblockId')
                                    ->where('libraryId = ?', $libraryRow->getId())
            ;
            $proposalblockIds = array(0);
            foreach($this->_table->getAdapter()->fetchAll($sql) as $proposalblock)
            {
                $proposalblockIds[] = $proposalblock['proposalblockId'];
            }
            $select->where('id IN ('.implode(',', $proposalblockIds).')');
        }
        return $this->findDependentRowset('ProposalBlock_Model_DbTable_ProposalBlock', 'Category', $select);
    }

    public function getChildren(Company_Model_LibraryRow $libraryRow)
    {
        try {
            $select = $this->select()
                            ->where('parentId = ?', $this->id, Zend_Db::INT_TYPE)
                            ->where('libraryId = ?', $libraryRow->getId(), Zend_Db::INT_TYPE)
                            ->where('companyId = ?', $libraryRow->companyId, Zend_Db::INT_TYPE)
            ;
            return $this->findDependentRowset('ProposalBlock_Model_DbTable_Category', 'Category', $select);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch(Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    /**
     * Update dependent blocks parent id
     */
    protected function _moveBlocksToParent()
    {
        $blocks = $this->getBlockRowset();

        if ($blocks->count()) {
            $blocksTable = new \ProposalBlock_Model_DbTable_ProposalBlock();
            $ids = array();

            foreach ($blocks as $block) {
                $ids[] = $block->getId();
            }

            $where = array('id IN (?)' => $ids);
            $blocksTable->update(array('categoryId' => $this->parentId), $where);
        }

    }

    public function delete()
    {
        if ($this->parentId) {
            $this->_moveBlocksToParent();
        }
        return parent::delete();
    }
}
