<?php

class ProposalBlock_Model_ProposalBlock
    extends \Zend_Db_Table_Row_Abstract
    implements \OSDN_Application_Model_Interface, \ProposalBlock_Instance_ModelInterface
{
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * (non-PHPdoc)
     * @see ProposalBlock_Instance_ModelInterface::isEditable()
     */
    public function isEditable()
    {
        $accountRow = Zend_Auth::getInstance()->getInstance()->getIdentity();
        return $accountRow->isCompanyOwner() || $this->accountId === $accountRow->getId();
    }

    public function getKind()
    {
        return $this->kind;
    }

    /**
     * (non-PHPdoc)
     * @see ProposalBlock_Instance_ModelInterface::isDefault()
     */
    public function isDefault()
    {
        return false;
    }

    public function getCategoryId($returnParentIfExists = false)
    {
        if ($returnParentIfExists) {
            $category = $this->getCategoryRow();
            $categoryId = $category->parentId ?: $category->getId();
        } else {
            $categoryId = $this->categoryId;
        }

        return $categoryId;
    }

    /**
     * Retrieve dependent category row
     *
     * @return \ProposalBlock_Model_Category
     */
    public function getCategoryRow()
    {
        return $this->findParentRow('ProposalBlock_Model_DbTable_Category', 'Category');
    }

    /**
     * @FIXME Should be moved to service?
     * Who should know about file storage location?
     *
     * @return string
     */
    public function toFilePath($isRelative = true)
    {
        $companyRow = Zend_Auth::getInstance()->getInstance()->getIdentity()->getCompanyRow();

        /**
         * @FIXME Make path more abstract
         */
        $path = '/modules-appalti/proposal-block/data/block-kind-file/';
        $path = $path . sprintf('%d/%d/%s', $companyRow->getId(), $this->getId(), $this->imageName);
        if (true !== $isRelative) {
            $path = APPLICATION_PATH . $path;
        }

        return $path;
    }

    /**
     * Retrieve block file collection
     *
     * @FIXME Should be moved to service?
     * Who should know about file storage location?
     *
     * @return array
     */
    public function toFilePaths($isRelative = true)
    {
        $companyRow = Zend_Auth::getInstance()->getInstance()->getIdentity()->getCompanyRow();

        $o = array();
        /**
         * @FIXME Make path more abstract
         */
        foreach(explode(';', $this->imageName) as $image) {

            $path = '/modules-appalti/proposal-block/data/block-kind-file/';
            $path = $path . sprintf('%d/%d/%s', $companyRow->getId(), $this->getId(), $image);
            if (true !== $isRelative) {
                $path = APPLICATION_PATH . $path;
            }

            $o[] = $path;
        }

        return $o;
    }

    public function isFileValid()
    {
        $collection = $this->toFilePaths(false);
        $died = array();

        foreach($collection as $k => $filepath) {
            if (!file_exists($filepath)) {
                $died[] = $filepath;
                unset($collection[$k]);
            }
        }

        if (count($died) > 0) {
            $this->imageName = join(';', $collection) ?: null;
            $this->save();
        }

        return count($collection) > 0;
    }

    /**
     * Kill block file collection
     *
     * @return void
     */
    public function dropFileCollection()
    {
        foreach($this->toFilePaths(false) as $filepath) {
            file_exists($filepath) && @ unlink($filepath);
        }
    }
}
