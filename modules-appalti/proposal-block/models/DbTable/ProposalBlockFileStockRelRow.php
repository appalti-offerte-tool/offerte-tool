<?php

class ProposalBlock_Model_DbTable_ProposalBlockFileStockRelRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    public function getId()
    {
        return $this->id;
    }

    public function getFileStockRow()
    {
        return $this->findParentRow('FileStock_Model_DbTable_FileStock', 'FileStock');
    }

    public function getProposalBlockRow()
    {
        return $this->findParentRow('ProposalBlock_Model_DbTable_ProposalBlock', 'ProposalBlock');
    }

    public function getParentRow()
    {
        return $this->findParentRow('ProposalBlock_Model_DbTable_ProposalBlockFileStockRel', 'Parent');
    }

    public function getChildrenRowset()
    {
        return $this->findDependentRowset('ProposalBlock_Model_DbTable_ProposalBlockFileStockRel', 'Parent');
    }
}