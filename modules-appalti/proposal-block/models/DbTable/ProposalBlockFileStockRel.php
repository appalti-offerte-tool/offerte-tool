<?php

class ProposalBlock_Model_DbTable_ProposalBlockFileStockRel extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'proposalBlockFileStockRel';

    protected $_nullableFields = array(
        'isThumbnail', 'parentId'
    );

    protected $_referenceMap    = array(
        'FileStock' => array(
            'columns'       => 'fileStockId',
            'refTableClass' => 'FileStock_Model_DbTable_FileStock',
            'refColumns'    => 'id'
        ),
        'Parent'    => array(
            'columns'       => 'parentId',
            'refTableClass' => __CLASS__,
            'refColumns'    => 'id'
        ),
        'ProposalBlock'    => array(
            'columns'       => 'blockId',
            'refTableClass' => 'ProposalBlock_Model_DbTable_ProposalBlock',
            'refColumns'    => 'id'
        )
    );

    protected $_rowClass = '\\ProposalBlock_Model_DbTable_ProposalBlockFileStockRelRow';
}