<?php

class ProposalBlock_Model_DbTable_ProposalBlock extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'proposalBlock';

    protected $_rowClass = '\\ProposalBlock_Model_ProposalBlock';

    protected $_referenceMap    = array(
        'Category'    => array(
            'columns'       => 'categoryId',
            'refTableClass' => 'ProposalBlock_Model_DbTable_Category',
            'refColumns'    => 'id'
        )
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        if (empty($data['createdDatetime'])) {
            $dt = new \DateTime();
            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
        }
        /* add modifiedDatetime for sorting purposes in library */
        $data['modifiedDatetime'] = $data['createdDatetime'];

        return parent::insert($data);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
        unset($data['kind'], $data['companyId'], $data['accountId']);

        $dt = new \DateTime();
        $data['modifiedDatetime'] = $dt->format('Y-m-d H:i:s');

        return parent::update($data, $where);
    }
}