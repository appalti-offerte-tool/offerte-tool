<?php

class ProposalBlock_Model_DbTable_Category extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'proposalBlockCategory';

    protected $_rowClass = '\\ProposalBlock_Model_Category';

    protected $_referenceMap    = array(
        'Category'    => array(
            'columns'       => 'parentId',
            'refTableClass' => 'ProposalBlock_Model_DbTable_Category',
            'refColumns'    => 'id'
        ),
        'Library'    => array(
            'columns'       => 'libraryId',
            'refTableClass' => 'Company_Model_DbTable_Library',
            'refColumns'    => 'id'
        ),
    );
}