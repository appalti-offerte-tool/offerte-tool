<?php

/**
 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
 * @version     SVN: $Id: ModelInterface.php 48 2012-04-27 08:08:37Z vanya $
 * @changedby   $Author: vanya $
 */

interface ProposalBlock_Instance_ModelInterface
{
    public function isEditable();

    public function isDefault();
}
