<?php

class Event_Service_EventSubscription extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Event_Model_DbTable_EventSubscription
     */
    protected $_table;

    protected function _init()
    {
        $this->_table = new Event_Model_DbTable_EventSubscription($this->getAdapter());

        parent::_init();
    }

    public function fetchAllByCompanyId(array $params = array())
    {
        if (!isset($params['companyId'])) {
            $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
            $params['companyId'] = $companyRow->id;
        }

        try {
            $select = $this->_table->getAdapter()->select()
                ->from(
                    array('es' =>$this->_table->getTableName()),
                    array('*')
                )
                ->join(
                    array('e' => 'event'),
                    'es.eventId = e.id',
                    array('*')
                )
                ->where('es.companyId = ?', $params['companyId'])
                ->order('es.id ASC');
            $this->_initDbFilter($select, $this->_table)->parse($params);
            $response = $this->getDecorator('response')->decorate($select);
            $response = $response->getRowset();
            return $response;
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to select subscription', 0, $e);
        }
    }

    public function fetchRowByCompanyIdAndEventId(array $params = array())
    {
        try {
            $select = $this->_table->getAdapter()->select()
                ->from(
                    array('es' =>$this->_table->getTableName()),
                    array('*')
                )
                ->join(
                    array('e' => 'event'),
                    'es.eventId = e.id',
                    array('*')
                )
                ->where('es.eventId =  ?', $params['eventId']);

            $accountRow = Zend_Auth::getInstance()->getIdentity();
            if (!$accountRow->isAdmin()) {
                $companyRow = $accountRow->getCompanyRow();
                $select->where('es.companyId = ?', $companyRow->getId());
            }
            $response = $this->getDecorator('response')->decorate($select);
            $response = $response->getRowset();
            return $response;
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to select subscription', 0, $e);
        }
    }

    public function fetchCompanySubscriptionIds($companyId)
    {
        try {
            $select = $this->_table->getAdapter()->select()
                ->from(
                    array($this->_table->getTableName()),
                    array('eventId')
                )
                ->where('companyId = ?', $companyId, Zend_Db::INT_TYPE);

            return $select->query()->fetchAll(PDO::FETCH_COLUMN);
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to select company subscriptions');
        }
    }

    public function fetchCompaniesWithStatus(array $params = array())
    {

        try {
            $select = $this->_table->getAdapter()->select()
                ->from(
                    array('c' => 'company'),
                    array('*')
                )
                ->joinLeft(
                    array('es' => $this->_table->getTableName()),
                    'c.id = es.companyId AND es.eventId ='.$params['eventId'],
                    array('subscriptionId'=> 'id')
                )
                ->where('c.parentId IS NULL');

            $fields = array(
                'c.name'        => 'name',
                'c.fullname'    => 'fullname',
                'c.email'       => 'email',
                'c.kvk'         => 'kvk',
                'c.btw'         =>'btw'
            );

            $this->_initDbFilter($select, $this->_table, $fields)->parse($params);
            $response = $this->getDecorator('response')->decorate($select);
            return $response;
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to select subscription', 0, $e);
        }
    }

    public function delete($data)
    {
        if (!isset($data['companyId'])) {
            $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
            $data['companyId'] = $companyRow->id;
        }

        try {
            $this->_table->deleteQuote(array('companyId = ?' => $data['companyId'], 'eventId = ?'=>$data['eventId']));
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to delete subscription');
            return false;
        }
        return true;
    }

    public function insert($data)
    {
        if (!isset($data['companyId'])) {
            $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
            $data['companyId'] = $companyRow->id;
        }
        try {

            $row = $this->_table->createRow($data);
            $row->save();
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to create subscription', 0, $e);
        }

        return $row->toArray();
    }

    public function deleteByEventId($id)
    {
        try {
            $this->_table->deleteQuote(array('eventId = ?' => $id));
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to delete subscription');
            return false;
        }
        return true;
    }

}
