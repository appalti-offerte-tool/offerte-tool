<?php

class Event_Service_Event extends \OSDN_Application_Service_Dbable
{
    const LINK = '/event/index/preview/?eventId=';
    const URL = 'http://';

    /**
     * @var Event_Model_DbTable_Event
     */
    protected $_table;

    protected function _init()
    {
        $this->_table = new Event_Model_DbTable_Event($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'eventCategoryId'              => array('allowEmpty' => false, 'presence'   => 'required'),
            'title'                        => array('allowEmpty' => false, 'presence'   => 'required'),
//            'publish'                      => array('allowEmpty' => false, 'presence'   => 'required'),
            'addressStreet'                => array('allowEmpty' => false, 'presence'   => 'required'),
            'addressHouseNumber'           => array('allowEmpty' => false, 'presence'   => 'required'),
            'addressPostCode'              => array('allowEmpty' => false, 'presence'   => 'required'),
            'addressCity'                  => array('allowEmpty' => false, 'presence'   => 'required'),
            'placeDesc'                    => array('allowEmpty' => false, 'presence'   => 'required'),
            'date'                         => array('allowEmpty' => false, 'presence'   => 'required'),
            'time'                         => array('allowEmpty' => false, 'presence'   => 'required'),
            'shortDesc'                    => array('allowEmpty' => false, 'presence'   => 'required'),
            'desc'                         => array('allowEmpty' => false, 'presence'   => 'required'),
        ));
        parent::_init();
    }

    private function createLink($id)
    {
        return self::URL . $_SERVER['HTTP_HOST'] . self::LINK . $id;
    }



    public function find($id, $throwException = true)
    {
        $eventRow = $this->_table->findOne($id);
        if (null === $eventRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find event #' . $id);
        }
        return $eventRow;
    }

    public function findWithEmptyFlag($categoryId = null, $throwException = true)
    {

        $params = array(
            'emptyFlag = 1',
            'accountId = ?' => Zend_Auth::getInstance()->getIdentity()->getId()
        );

        if ($categoryId != null) {
            $params['eventCategoryId'] =  $categoryId;
        }

        $eventRow = $this->_table->fetchRow($params);

        if (!$eventRow) {
            try {
                $eventRow = $this->_table->createRow(array(
                    'emptyFlag' => 1,
                    'accountId' => Zend_Auth::getInstance()->getIdentity()->getId()
                ));
                $eventRow->save();
            } catch (\Exception $e) {
                throw new \OSDN_Exception('Unable to create empty event. Error: ' . $e->getMessage());
            }
        }

        return $eventRow;
    }

    public function archiveEvent($data)
    {
        try {
            $this->_table->updateByPk($data, $data['id']);
            return true;
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to archive event', 0, $e);
            return false;
        }
    }


    public function fetchAll(array $params = array())
    {
        try {
            $select = $this->_table->getAdapter()->select()
                ->from(array('e' => $this->_table->getTableName()))
                ->joinLeft(
                    array('ec' => 'eventCategory'),
                    'e.eventCategoryId = ec.id',
                    array('name'=>'ec.name')
                )
                ->where('e.emptyFlag=0')
                ->where('e.isArchive=?', !empty($params['isArchive']) ? $params['isArchive'] : 0, Zend_Db::INT_TYPE)
                ->order('e.date DESC');

            $this->_initDbFilter($select, $this->_table)->parse($params);
            $response = $this->getDecorator('response')->decorate($select);
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to select event list', 0, $e);
        }
        return $response;
    }

    public function fetchAllArchive()
    {
        return $this->fetchAll(array( 'isArchive' => 1 ));
    }

    public function fetchAllByPeriod(array $params = array())
    {
        if (strtotime($params['startDate']) > strtotime($params['endDate'])) {
            throw new OSDN_Exception('Incorrect dates ');
        }
        $data = strtotime($params['startDate']);
        $params['startDate'] = date('Y-m-d', $data);

        $data = strtotime($params['endDate']);
        $params['endDate'] = date('Y-m-d', $data);

        try {
            $select = $this->_table->getAdapter()->select()
                ->from($this->_table->getTableName())
                ->where('emptyFlag=0')
                ->where('date >= ?', $params['startDate'])
                ->where('publishDate <= ?', $params['endDate'])
                ->where('isArchive=?',  !empty($params['isArchive']) ? $params['isArchive'] : 0 ,  Zend_Db::INT_TYPE)
                ->order('date DESC');

            if (!empty($params['categoryId'])) {
                $select->where('eventCategoryId = ?', $params['categoryId'], Zend_Db::INT_TYPE);
            }

            $this->_initDbFilter($select, $this->_table)->parse($params);
            $response = $this->getDecorator('response')->decorate($select);
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to select event list', 0, $e);
        }
        return $response;
    }

    public function fetchLast($archive = 0, $count = 1, $order = 'DESC')
    {
        try {
            $select = $this->_table->getAdapter()->select()
                ->from($this->_table->getTableName())
                ->where('emptyFlag=0')
                ->where('isArchive=?', $archive)
                ->order('date '.$order)
                ->limit($count);
            $response = $this->getDecorator('response')->decorate($select);
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to select event', 0, $e);
        }
        return $response;
    }

    public function create($data)
    {
        if (strtotime($data['publishDate']) >  strtotime($data['date'])) {
            throw new OSDN_Exception('Incorrect dates ');
        }
        $this->getAdapter()->beginTransaction();
        try {
            $data['desc'] = stripslashes($data['desc']);
            $f = $this->_validate($data);
            $row = $this->_table->createRow($f->getData());
            $row->save();
            $row = $row->toArray();
            $row['directShareLink'] = $this->createLink($row['id']);
            $this->_table->updateByPk($row, $row['id']);
            $this->getAdapter()->commit();
        } catch (\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to create event', 0, $e);
        }

        return $row;
    }

    public function delete($id)
    {
        $eventFileStockService = new Event_Service_EventFileStock();
        $subscriptionService = new Event_Service_EventSubscription();

        $this->getAdapter()->beginTransaction();
        try {
            $eventRow = $this->find($id);
            $subscriptionService->deleteByEventId($eventRow->getId());
            $eventFileStockService->deleteByEventId($eventRow->getId());
            $eventRow->delete();
            $this->getAdapter()->commit();
        } catch (\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to delete event', 0, $e);
        }
        return true;
    }

    public function deleteAllByCategoryId($id)
    {
        $eventFileStockService = new Event_Service_EventFileStock();
        $eventRow = $this->_table->fetchAll(array('eventCategoryId = ?' => $id));
        $eventRow = $eventRow->toArray();
        $this->getAdapter()->beginTransaction();
        try {
            foreach ($eventRow as $row) {
                $eventFileStockService->deleteByEventId($row['id']);
            }
            $this->_table->deleteQuote(array('eventCategoryId = ?' => $id));
            $this->getAdapter()->commit();
        } catch (\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to delete event', 0, $e);
        }
        return true;


    }

    public function update($id, array $data)
    {
        $eventRow = $this->find($id);
        if ($eventRow['emptyFlag'] == '1') {
            $data['emptyFlag'] = 0;
            $data['directShareLink'] = $this->createLink($id);
        }
        if ( strtotime($data['publishDate'])> strtotime($data['date'])) {
            throw new OSDN_Exception('Incorrect dates ');
        }


        try {
            $data['desc'] = stripslashes($data['desc']);

            $eventRow->setFromArray($data);
            $eventRow->save();
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to update event', 0, $e);
        }

        return $eventRow;
    }

}

