<?php

class Event_Service_EventFileStock extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Event_Model_DbTable_Event
     */
    protected $_table;
    /**
     * @var FileStock_Service_FileStock
     */
    protected $_fileStock;

    public function _init()
    {
        $this->_table = new \Event_Model_DbTable_Event($this->getAdapter());
        $this->_fileStock = new \FileStock_Service_FileStock(array(
            'baseFilePath'       => APPLICATION_PATH . '/modules-appalti/event/data/event-file/',
            'useCustomExtension' => false,
            'identity'           => 'event'
        ));
    }

    public function fetchAllFileStockByEventIdWithResponse($eventId)
    {
        $eventRow = $this->_table->findOne($eventId);
        if (empty($eventRow)) {
            throw new \OSDN_Exception('Unable to find #' . $eventId);
        }
        $fileStockTable = new \FileStock_Model_DbTable_FileStock($this->getAdapter());
        $select = $this->_table->getAdapter()->select()
            ->from(
                array('df' => 'eventFileStockRel'), array()
            )
            ->join(
                array('f' => 'fileStock'), 'f.id = df.fileStockId', $fileStockTable->getAllowedColumns()
            )
            ->join(
                array(
                    'dfThumbnail' => 'eventFileStockRel'), 'dfThumbnail.parentId = df.id AND dfThumbnail.isThumbnail = 1',
                    array(
                        'thumbnailFileStockId' => 'fileStockId',
                        'isThumbnail',
                        'isDefaultThumbnail'
                    )
            )
            ->where('df.eventId = ?', $eventRow->getId(), Zend_Db::INT_TYPE)
            ->where('df.parentId IS NULL');
        $response = $this->getDecorator('response')->decorate($select);
        $storage = $this->_fileStock->factory();
        $response->setScope($this);
        $response->setRowsetCallback(function($row, $rowset, $scope) use ($storage, $eventRow)
        {
            $thumbnailFileStockRow = $scope->find($eventRow->getId(), $row['thumbnailFileStockId']);
            $row['thumbnailLink'] = $storage->toFilePathHost($thumbnailFileStockRow);
            $fileStockRow = $scope->find($eventRow->getId(), $row['id']);
            $row['link'] = $storage->toFilePathHost($fileStockRow);
            return $row;
        });

        return $response;
    }

    public function fetchFileStockThumbnailRowByEvent($eventId, $fileStockId = null)
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('df' => 'eventFileStockRel'),
                array('parentId')
            )
            ->join(
                array('f' => 'fileStock'),
                'f.id = df.fileStockId',
                array('id')
            )
            ->join(
                array('dfp'    => 'eventFileStockRel'),
                'df.parentId = dfp.id',
                array()
            )
            ->where('df.eventId = ?', $eventId, Zend_Db::INT_TYPE)
            ->where('df.isThumbnail = 1')
            ->limit(1);

        if (empty($fileStockId)) {
            $select->where('dfp.fileStockId = ?', $fileStockId, Zend_Db::INT_TYPE);
        }

        $foundRow = $select->query()->fetch();
        if (empty($foundRow)) {
            return null;
        }

        $fileStockId = $foundRow['id'];

        if (null == ($fileStockRow = $this->_fileStock->find($fileStockId))) {
            return null;
        }

        $row = $fileStockRow->toArray();

        $storage = $this->_fileStock->factory();

        if (!empty($foundRow['parentId'])) {
            $eventFileStockRelation = new Event_Model_DbTable_EventFileStockRel();
            $eventFileStockRelationRow = $eventFileStockRelation->findOne($foundRow['parentId']) ;
            $row['parentLink'] = $storage->toFilePathHost($eventFileStockRelationRow->getFileStockRow());

        }

        $row['link'] = $storage->toFilePathHost($fileStockRow);

        return $row;
    }

    public function find($eventId, $fileStockId)
    {
        return $this->_fileStock->find($fileStockId);
    }

    public function create(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {

        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }
        if (null === ($eventRow = $this->_table->findOne($data['eventId']))) {
            $data['emptyFlag'] = '1';
            $data['title'] = null;
            $data['text'] = null;
            $eventRow = $this->_table->createRow($data);
            $eventRow->save();
        }
        $eventFileStockRelation = new Event_Model_DbTable_EventFileStockRel();
        $eventFileStockRelationRow = null;
        $fileStockRow = $this->_fileStock
            ->create($transfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
        use ($eventRow, $eventFileStockRelation, & $eventFileStockRelationRow)
        {
            $eventFileStockRelationRow = $eventFileStockRelation->createRow(array(
                'fileStockId' => $fileStockRow->getId(),
                'eventId'      => $eventRow->getId()
            ));
            $eventFileStockRelationRow->save();
        });
        $rawFileTransfer = new FileStock_Service_Transfer_Raw();
        $storage = $this->_fileStock->factory();
        $link = $storage->toFilePath($fileStockRow, true);
        $rawFileTransfer->addFile($link);
        require_once APPLICATION_PATH . '/modules/file-stock/views/scripts/filters/File/Duplicate.php';
        $rawFileTransfer->addFilter(new FileStock_View_Filter_File_Duplicate($link . '.tmp', null, true));

        $isf = new Zend_Filter_ImageSize();
        $isf->setHeight(100);
        $isf->setWidth(100);
        $isf->setThumnailDirectory(dirname($link));
        $rawFileTransfer->addFilter($isf);

        $fileStockThumRow = $this->_fileStock->create($rawFileTransfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
        use ($eventRow, $eventFileStockRelationRow, $eventFileStockRelation)
        {
            $hasPredefinedThumbnail = $eventFileStockRelation->count(array(
                'eventId = ?' => $eventRow->getId(),
                'isThumbnail = 1'
            )) > 0;
            $eventRow->getTable()->getAdapter()->insert('eventFileStockRel', array(
                'fileStockId'        => $fileStockRow->getId(),
                'eventId'             => $eventRow->getId(),
                'isThumbnail'        => 1,
                'isDefaultThumbnail' => !$hasPredefinedThumbnail,
                'parentId'           => $eventFileStockRelationRow->id
            ));
            $eventRow->save();
        });
        return array($eventRow, $fileStockRow);
    }

    public function update(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {
        $eventRow = $this->_table->findOne($data['eventId']);
        if (empty($eventRow)) {
            throw new OSDN_Exception('Unable to find event #' . $data['eventId']);
        }
        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }
        $this->getAdapter()->beginTransaction();
        try {
            $fileStockRow = $this->_fileStock->update($transfer, $data);
            $eventFileStockRelation = new Event_Model_DbTable_EventFileStockRel();
            $eventFileStockRelationRow = $eventFileStockRelation->fetchRow(array(
                'eventId = ?'      => $eventRow->getId(),
                'fileStockId = ?' => $fileStockRow->getId(),
                'parentId IS NULL'
            ));
            if (null !== $eventFileStockRelationRow) {
                foreach ($eventFileStockRelationRow->getChildrenRowset() as $row) {
                    $this->_fileStock->delete($row->fileStockId, function($fileStockRow) use ($row) {
                        $row->delete();
                    });
                }
            }
            $rawFileTransfer = new FileStock_Service_Transfer_Raw();
            $storage = $this->_fileStock->factory();
            $link = $storage->toFilePath($fileStockRow, true);
            $rawFileTransfer->addFile($link);
            require_once APPLICATION_PATH . '/modules/file-stock/views/scripts/filters/File/Duplicate.php';
            $rawFileTransfer->addFilter(new FileStock_View_Filter_File_Duplicate($link . '.tmp', null, true));

            $isf = new Zend_Filter_ImageSize();
            $isf->setHeight(100);
            $isf->setWidth(100);
            $isf->setThumnailDirectory(dirname($link));
            $rawFileTransfer->addFilter($isf);

            $fileStockThumRow = $this->_fileStock->create($rawFileTransfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow) use ($eventRow, $eventFileStockRelationRow)
            {
                $eventRow->getTable()->getAdapter()->insert('eventFileStockRel', array(
                    'fileStockId' => $fileStockRow->getId(),
                    'eventId'      => $eventRow->getId(),
                    'isThumbnail' => 1,
                    'parentId'    => $eventFileStockRelationRow->id
                ));
            });
            $this->getAdapter()->commit();
        } catch (\Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        }
        return $fileStockRow;
    }

    public function delete($eventId, $fileStockId)
    {
        $eventRow = $this->_table->findOne($eventId);
        if (empty($eventRow)) {
            throw new OSDN_Exception('Unable to find event #' . $eventId);
        }
        $this->getAdapter()->beginTransaction();
        try {
            $eventFileStockRelation = new \Event_Model_DbTable_EventFileStockRel();
            $scope = $this->_fileStock;
            $callbackFn = function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
            use ($eventRow, $eventFileStockRelation, $scope, & $callbackFn)
            {
                $eventFileStockRelationRow = $eventFileStockRelation->fetchRow(array(
                    'eventId = ?'      => $eventRow->getId(),
                    'fileStockId = ?' => $fileStockRow->getId()
                ));
                if (null !== $eventFileStockRelationRow) {
                    foreach ($eventFileStockRelationRow->getChildrenRowset() as $row) {
                        $scope->delete($row->fileStockId, $callbackFn);
                    }
                    $eventFileStockRelationRow->delete();
                }
            };
            $result = $this->_fileStock->delete($fileStockId, $callbackFn);
            $this->getAdapter()->commit();
        } catch (\Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        }
        return (boolean)$result;
    }

    public function deleteByEventId($id)
    {
        $eventFileStockRelation = new \Event_Model_DbTable_EventFileStockRel();
        $rowset = $eventFileStockRelation->fetchAll(array(
            'eventId = ?' => $id
        ));
        $rowset = $rowset->toArray();

        try {
            foreach ($rowset as $row) {
                if ($row['parentId'] == null) {
                    $this->delete($id, $row['fileStockId']);
                }
            }
        } catch (\Exception $e) {
            throw new OSDN_Exception('Unable to delete event files. Error: ' . $e->getMessage());
        }

        return true;
    }

    public function setDefaultThumbnail($eventId, $fileStockId)
    {
        $eventFileStockRelation = new \Event_Model_DbTable_EventFileStockRel();
        $this->getAdapter()->beginTransaction();
        try {
            $eventFileStockRelation->updateQuote(
                array(
                    'isDefaultThumbnail' => 0
                ), array(
                    'eventId = ?' => (int)$eventId,
                    'isThumbnail = 1'
                )
            );
            $eventFileStockRelationRow = $eventFileStockRelation->fetchRow(array(
                    'eventId = ?'      => (int)$eventId,
                    'fileStockId = ?' => (int)$fileStockId
                )
            );
            if (null == $eventFileStockRelationRow) {
                throw new OSDN_Exception('Unable to find row');
            }
            $affectedRows = $eventFileStockRelation->updateQuote(array(
                'isDefaultThumbnail' => 1
            ), array(
                'eventId = ?'   => (int)$eventId,
                'parentId = ?' => (int)$eventFileStockRelationRow->id,
                'isThumbnail = 1'
            ));
            if (0 == $affectedRows) {
                throw new \OSDN_Exception('Unable to change default thumbnail');
            }
            $this->getAdapter()->commit();
        } catch (\Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        }
    }

}