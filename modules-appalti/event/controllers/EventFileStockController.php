<?php

class Event_EventFileStockController extends \Zend_Controller_Action implements \FileStock_Instance_EntityFileStockControllerInterface
{
    /**
     * @var Event_Service_EventFileStock
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'event';
    }

    public function init()
    {
        $this->_service = new \Event_Service_EventFileStock($this->getEntityType());

        $this->_helper->contextSwitch()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('set-default-thumbnail', 'json')
            ->addActionContext('fetch-thumbnail', 'json')
            ->initContext();
    }

    public function getEntityType()
    {
        return 'event';
    }


    public function indexAction()
    {

        try {
            $eventId = $this->_getParam('eventId');
            if (!empty($eventId)) {

                $response = $this->_service->fetchAllFileStockByEventIdWithResponse($this->_getParam('eventId'));
                $this->view->assign($response->toArray());
                $this->view->eventId =  $eventId;
            }
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            var_dump($e->getMessage());die;
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function downloadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $fileStockRow = $this->_service->find($this->_getParam('eventId'), $this->_getParam('id'));
        $fileStockService = new FileStock_Service_FileStock();
        $fileStockService->download($fileStockRow, $this->getResponse(), true);
    }

    public function fetchAllAction()
    {
        try {
            $response = $this->_service->fetchAllFileStockByEventIdWithResponse($this->_getParam('eventId'));
            $this->view->assign($response->toArray());
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAction()
    {

        $fileStockRow = $this->_service->find($this->_getParam('eventId'), $this->_getParam('fileStockId'));
        $this->view->data = $fileStockRow->toArray();
        $this->view->success = true;
    }

    public function fetchThumbnailAction()
    {
        try {

            $fileStockRow = $this->_service->fetchFileStockThumbnailRowByEvent($this->_getParam('eventId'), $this->_getParam('fileStockId'));

            $this->view->assign($fileStockRow);
            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function createAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');
        try {

            list($eventRow, $fileStockRow) = $this->_service->create($this->_getAllParams());
            $this->view->success = true;
            $this->view->entityId = $eventRow->getId();
            $this->view->fileStockId = $fileStockRow->getId();
            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function quickUploadAction()
    {
        $this->_helper->layout->disableLayout();
        try {

            list($eventRow, $fileStockRow) = $this->_service->create($this->_getAllParams());
            $this->view->success = true;
            $this->view->id = $eventRow->getId();
            $folder = 1000 + floor($fileStockRow->getId() / 1000) * 1000;
            $this->view->src = sprintf('/modules-appalti/event/data/event-file/%d/%d.jpg', $folder, $fileStockRow->getId());
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->view->error = $e->getMessages();
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');
        try {
            $this->_service->update($this->_getAllParams());
            $this->view->success = true;
            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->_service->delete($this->_getParam('eventId'), $this->_getParam('fileStockId'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }

    public function setDefaultThumbnailAction()
    {
        try {
            $this->_service->setDefaultThumbnail($this->_getParam('eventId'), $this->_getParam('fileStockId'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }

}