<?php

class Event_MainController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var Event_Service_Event
     */
    protected $_service;

    public function init()
    {
        $this->_helper->ajaxContext()
            ->addActionContext('edit', 'json')
            ->addActionContext('archive', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('fetch', 'json')
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch-row', 'json')
            ->initContext();

        $this->_service = new Event_Service_Event();
        parent::init();
    }

    public function getResourceId()
    {
        return 'event';
    }


    public function indexAction()
    {

    }

    public function fetchAction()
    {
        try {
            $params = $this->_getAllParams();
            if ($params['isCategory'] == 0) {
                unset($params['categoryId']);
            }
            $eventRow = $this->_service->fetchAllByPeriod($params);
            $this->view->assign($eventRow->toArray());
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAllAction()
    {
        try {
            $eventRow = $this->_service->fetchAll($this->_getAllParams());
            $this->view->assign($eventRow->toArray());
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function archiveAction()
    {
        try {
            $this->_service->archiveEvent($this->_getAllParams());
            $this->view->success = true;
            $this->_helper->information('Operation has been completed successfully', true, E_USER_NOTICE);
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchRowAction()
    {
        try {
            $data = $this->_service->find($this->_getParam('eventId'));
            $this->view->assign($data->toArray());
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function createAction()
    {
        try {
            $params = $this->_getAllParams();
            $response = $this->_service->create($params);
            $this->view->id = $response['id'];
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function editAction()
    {
        try {
            $this->_service->update($this->_getParam('eventId'), $this->_getAllParams());
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->view->success = $this->_service->delete($this->_getParam('id'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }
}