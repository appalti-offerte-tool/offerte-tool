<?php

class Event_IndexController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    const PREV_PAGE = 1;
    const NEXT_PAGE = 0;
     /**
     * @var Event_Service_Event
     */
    protected $_service;

    protected function _getRedirectUrl($case)
    {
        $params = $this->_getAllParams();
        $eventId = $params['eventId'];

        $response = $this->_service->fetchAll($params);
        $rowset =$response->getRowset();

        for($i = 0; $i< count($rowset); $i++) {
            if ($params['eventId'] == $rowset[$i]['id']) {
                if ($case == 1) {
                    if ($i == count($rowset) -1 ) {
                        $eventId = $rowset[0]['id'];

                    } else {
                        $eventId = $rowset[$i+1]['id'];
                    }
                } else {
                    if ($i == 0 ) {
                        $eventId = $rowset[count($rowset) -1]['id'];
                    } else {
                        $eventId = $rowset[$i-1]['id'];
                    }
                }
            }
        }
        return '/event/index/preview/eventId/' . $eventId;
    }


    public function init()
    {
        $this->_service = new Event_Service_Event();
        $this->_helper->ContextSwitch()
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->initContext();

        parent::init();
    }

    public function getResourceId()
    {
        return 'event';
    }

    public function indexAction()
    {
        $pagination = new \OSDN_Pagination();
        $pagination->setItemsCountPerPage(5);

        try {
            $response = $this->_service->fetchAll($this->_getAllParams());
            $this->view->rowset = $response->getRowset();

            $accountRow = Zend_Auth::getInstance()->getIdentity();
            if (!$accountRow->isAdmin()) {
                $companyRow = $accountRow->getCompanyRow();
                $subscrSrv = new \Event_Service_EventSubscription();
                $companySubscriptions = $subscrSrv->fetchCompanySubscriptionIds($companyRow->getId());
                $this->view->companySubscriptions = $companySubscriptions;
            }

            $pagination->setTotalCount($response->count());
            $this->view->pagination = $pagination;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->_redirect('/');
        }
    }

    public function previewAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(1);
        $account = Zend_Auth::getInstance()->getIdentity();

        try {
            $response = $this->_service->find($this->_getParam('eventId'));
            $this->view->row = $response->toArray();

            $this->view->checkSubscription = !$account->isAdmin();
            if ($this->view->checkSubscription) {
                $subscriptionService = new Event_Service_EventSubscription();
                $response = $subscriptionService->fetchRowByCompanyIdAndEventId(array('eventId'=>$this->_getParam('eventId')));
                $this->view->isSubscribed = !empty($response);
            }

            $this->view->first = $this->_getParam('first');
            $this->view->last = $this->_getParam('last');
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->_redirect($account->isAdmin() ? '/ap-default/index/news-and-events' : '/');
        }


    }

    public function formAction()
    {
        $params = $this->_getAllParams();
        if (isset($params['eventId'])) {
            $eventRow = $this->_service->find($params['eventId']);
            $this->view->row = $eventRow;
        } else {
            $eventRow = $this->_service->findWithEmptyFlag();
            $this->view->row = $eventRow;
        }
    }

    public function deleteAction()
    {
        try {
            $this->_service->delete($this->_getParam('eventId'));
            $this->_helper->information('Event deleted successfully!', true, E_USER_NOTICE);
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
        $this->_redirect('/ap-default/index/news-and-events');
    }

    public function createAction()
    {
        try {
            $params = $this->_getAllParams();
            $params['eventCategoryId'] = null;
            $response = $this->_service->create($params);
            $this->view->id = $response['id'];
            $this->view->success = true;
            $this->_helper->information('Event created successfully', true, E_USER_NOTICE);
            if (!$this->_request->isXmlHttpRequest()) {
                $this->_redirect('/ap-default/index/news-and-events');
            }
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
            if (!$this->_request->isXmlHttpRequest()) {
                $this->_redirect('/event/index/form');
            }

        }
    }

    public function updateAction()
    {
        try {
            $params = $this->_getAllParams();
            $params['newsCategoryId'] = null;
            $this->_service->update($this->_getParam('eventId'), $params);
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
            if (!$this->_request->isXmlHttpRequest()) {
                if ($params['preview'] == 0) {
                    $this->_redirect('/ap-default/index/news-and-events');
                } else {
                    $this->_redirect('/event/index/preview/eventId/' . $params['newsId']);
                }
            }

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
            if (!$this->_request->isXmlHttpRequest()) {
                $this->_redirect('/event/index/form/eventId/'.$params['eventId']);
            }
        }
    }

    public function nextAction()
    {
        $this->_redirect($this->_getRedirectUrl(self::NEXT_PAGE));
    }

    public function previousAction()
    {
        $this->_redirect($this->_getRedirectUrl(self::PREV_PAGE));
    }
}