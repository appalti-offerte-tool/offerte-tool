<?php

class Event_SubscriptionController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var Event_Service_EventSubscription
     */
    protected $_service;

    public function init()
    {
        $this->_helper->ajaxContext()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('set-subscription-status', 'json')

            ->initContext();

        $this->_service = new Event_Service_EventSubscription();
        parent::init();
    }

    public function getResourceId()
    {
        return 'event';
    }

    public function indexAction()
    {
    }

    public  function fetchAllAction()
    {
        try {
            $companyRow = $this->_service->fetchCompaniesWithStatus($this->_getAllParams());
            $this->view->assign($companyRow->toArray());
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function setSubscriptionStatusAction()
    {
        try {

            $params = $this->_getAllParams();
            if ($params['status']) {
                $this->_service->insert($params);
            } else {
                $this->_service->delete($params);
            }
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function createAction()
    {
        $params = $this->_getAllParams();
        try {
            $this->_service->insert($params);
            $this->_helper->information('U bent succesvol aangemeld voor dit evenement', null ,E_USER_NOTICE );
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
        $this->_redirect('/event/index/preview/eventId/'.$params['eventId']);
    }

    public function deleteAction()
    {
        $params = $this->_getAllParams();
        try {
            $this->_service->delete($params);
            $this->_helper->information('U bent succesvol uitgeschreven van dit evenement', null ,E_USER_NOTICE );
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
        $this->_redirect('/event/index/preview/eventId/'.$params['eventId']);
    }

}