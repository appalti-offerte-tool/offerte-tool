Ext.define('Module.Event.Model.Category', {
    extend:'Ext.data.Model',
    fields:[ 'id', 'name']
});