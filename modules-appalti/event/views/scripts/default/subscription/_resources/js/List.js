Ext.define('Module.Company.SubscriptionList', {
    extend: 'Ext.ux.grid.GridPanel',
    alias: 'widget.module.company.subscription-list',

    filterRequestParam: null,

    features: [{
        ftype: 'filters'
    }],

    iconCls: 'm-company-icon-16',

    modeReadOnly: false,
    eventId:null,

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.Company.Model.Company',
            proxy: {
                type: 'ajax',
                url: link('event', 'subscription', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        var checkColumn = new Ext.ux.CheckColumn ({
            header:lang ('Subscribe'),
            dataIndex:'isSubscribe',
            width:80
        });

        this.columns = [{
            header: lang('Name'),
            dataIndex: 'name',
            flex: 1
        }, {
            header: lang('Full name'),
            dataIndex: 'fullname'
        }, {
            header: lang('E-mail'),
            dataIndex: 'email'
        }, {
            header: 'KVK',
            dataIndex: 'kvk'
        }, {
            header: 'BTW',
            dataIndex: 'btw'
        },
        checkColumn
        ];

        this.tbar = [
            '-'
        ];

        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 2
        })];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        checkColumn.on ('checkchange', function (cc, rowIndex, isChecked) {
            var record = this.getStore ().getAt (rowIndex);
            this.onChangeResourceState (record.get ('id'), isChecked, record);
            this.getStore ().load ();
        }, this);

        this.callParent();

    },

    onChangeResourceState:function (id, value, record) {
        Ext.Ajax.request ({
            url:link ('event', 'subscription', 'set-subscription-status', {format:'json'}),
            params:{
                companyId: id,
                eventId: this.eventId,
                status: value
            },
            success:function (response) {
                var decResponse = Ext.decode (response.responseText);
                Application.notificate (decResponse.messages);
                if (decResponse.success) {
                }
            }
        });
    },

    setEventId:function (eventId, forceReload) {
        this.eventId = eventId;
        var p = this.getStore ().getProxy ();
        p.extraParams = p.extraParams || {};
        p.extraParams.eventId = eventId;
        if (true === forceReload) {
            this.getStore ().load ();
        }
        return this;
    }
});