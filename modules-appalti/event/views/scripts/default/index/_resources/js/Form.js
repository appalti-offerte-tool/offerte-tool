;(function($, window) {
    var EventText = function() {
        this.init();
    }

    EventText.prototype = {

        constructor: EventText,

        eventId: null,

        init: function() {

            $('body')
            .off('.EventText')
            .on('click.EventText', '.editorCommandTrigger', function() {
                var $el = $(this), cmd = $el.data('command');
                if (undefined != window.tinyMCE && cmd) {
                    window.tinyMCE.execCommand(cmd);
                }
            });

            this.$form = $('#create-event-form');
            this.$form.submit(function() {
                var editor = window.tinyMCE.getInstanceById($('.tinymce', atb.$form)[0].id);
                editor.save();
                return atb.$form.valid();
            });

            this.tinymce = new AppaltiTinyMCE({
                onAjaxSave: this.setUpdate
            })
        },

        setUpdate: function(data) {
            if (data.success) {
                if (!$('input[name="eventId"]', atb.$form).length) {
                    atb.$form
                        .attr('action', atb.$form[0].action.replace('/create/', '/update/'))
                        .prepend('<input type="hidden" name="eventId" value="' + data.id + '" />');
                }
            } else {
                alert(data.messages[0].message);
            }
        }
    }

    window.EventText = new EventText();
    var atb = window.EventText;

})(jQuery, window);