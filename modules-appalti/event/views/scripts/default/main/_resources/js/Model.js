Ext.define ('Module.Event.Model.Event', {
    extend:'Ext.data.Model',
    fields:[
        'id', 'title','isArchive','name','shortDesc',
        {name:'publishDate', type:'date', dateFormat:'Y-m-d'},
        {name:'date', type:'date', dateFormat:'Y-m-d'}
    ]
});