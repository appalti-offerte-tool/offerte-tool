Ext.define('Module.Event.Form', {
    extend : 'Ext.form.Panel',
    alias : 'widget.module.event.event-form',
    model : 'Module.Event.Model.Event',

    eventId : null,
    categoryId : null,
    waitMsgTarget : true,
    wnd : null,

    initComponent : function() {

        this.blocks = new Module.Application.BlockTabPanel({
            border : false,
            invokeArgs : {
                categoryId : this.categoryId,
                eventId : this.eventId
            }
        });
        
        this.items = [ this.blocks ];
        this.callParent(arguments);
        this.blocks.on('tabchange', function(tb, b) {
            b && b.reload();
            b && b.doLayout();
        });
        var params = {};
        params.keywords = 'form';
        params.module = 'event';
        this.on('render', function() {
            var me = this;
            this.blocks.reload(params, function(tp) {
                var block = me.getFormPreviewBlock();
                if (null !== block) {
                    if (!me.eventId) {
                    block.setDisabled(true);
                    }
                }
            });
        }, this);
        this.blocks.on('backgroundinvokeargschanged', function(bp, ia) {
            if (ia.eventId) {
                this.eventId = ia.eventId;
            }
            return false;
        }, this);
    },

    getFormPreviewBlock : function() {
        var t = null;
        this.blocks.items.each(function(tab) {
            if (/form-preview/.test(tab.id)) {
                t = tab;
                return false;
            }
        });
        return t;
    },

    showInWindow : function() {
        this.border = false;
        var w = this.wnd = new Ext.Window({
            title : this.eventId ? lang('Update') : lang('Create'),
            resizable : false,
            layout : 'fit',
            width : 1000,
            height : 720,
            border : false,
            modal : true,
            items : [ this ],
            buttons : [ {
                text : this.eventId ? lang('Update') : lang('Create'),
                handler : this.onSubmit,
                scope : this
            }, {
                text : lang('Close'),
                handler : function() {
                    w.close();
                    this.wnd = null;
                },
                scope : this
            } ]
        });
        w.show();
        return w;
    },
    onSubmit : function(panel, w) {

        var params = {};
        var action = '';
        this.eventId = this.blocks.activeTab.eventId;
        if (this.eventId) {
            action = 'edit';
            params['eventId'] = this.eventId;

        }
        if (!this.eventId) {
            this.eventId = this.blocks.activeTab.form.eventId;
            if (this.eventId) {
                action = 'edit';
                params['eventId'] = this.eventId;
            } else {
                action = 'create';
            }
        }
        if (this.categoryId != null){
            params['eventCategoryId'] = this.categoryId;
        }
        var isValid = true;
        this.blocks.items.each (function (block) {
            if (!block.isValid ()) {
                isValid = false;
                return isValid = false;
            }
            Ext.applyIf (params, block.toValues ());
        });
        if (true !== isValid) {
            return;
        }
        this.form.submit ({
            url:link ('event', 'main', action, {
                format:'json'
            }),
            method:'post',
            params:params,
            waitMsg:Ext.LoadMask.prototype.msg,
            success:function (form, action) {
                var responseObj = Ext.decode(action.response.responseText);
                Application.notificate (responseObj.messages);
                if (action.result.success) {
                    this.fireEvent ('completed', this, responseObj.id, action.response);
                    if (!this.eventId) {
                        this.eventId = responseObj.id;
                    }
                    var block = this.getFormPreviewBlock ();
                    if (null !== block) {
                        block.setDisabled (false);
                        block.setInvokeArgs (Ext.apply (block.invokeArgs || {}, {
                            eventId:this.eventId,
                            categoryId:this.categoryId
                        }));
                    }
                    return;
                }
            },
            scope:this
        });
    }
});