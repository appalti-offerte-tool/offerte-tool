Ext.define ('Module.Event.Block.List', {
        extend:'Ext.ux.grid.GridPanel',
        alias:'widget.module.event.list',
        features:[
            {
                ftype:'filters'
            }
        ],

    modeReadOnly:false,
    dataUrl:null,
    eventId:null,
    firstDate:null,
    lastDate:null,
    isArchive:0,
    isCategory: 1,
    categoryId:null,

    initComponent:function () {
        this.store = new Ext.data.Store ({
                model:'Module.Event.Model.Event',
                proxy:{
                    type:'ajax',
                    url:link ('event', 'main', 'fetch', {format:'json'}),
                    reader:{
                        type:'json',
                        root:'rowset'
                    }
                }
            });

        var checkColumn = new Ext.ux.CheckColumn ({
            header:lang ('Archive'),
            dataIndex:'isArchive',
            width:50
        });

        this.columns = [
            {
                header:lang ('Title'),
                dataIndex:'title',
                flex:1
            },{
                header:lang ('PublishDate'),
                xtype:'datecolumn',
                dataIndex:'publishDate',
                format:'d-m-Y',
                width:120
            }, {
                header:lang ('Event date'),
                xtype:'datecolumn',
                dataIndex:'date',
                format:'d-m-Y',
                width:120
            },
            checkColumn
            , {
                xtype:'actioncolumn',
                header:lang ('Actions'),
                width:50,
                fixed:true,
                items:[
                    {
                        tooltip:lang ('Edit'),
                        iconCls:'icon-edit-16 icon-16',
                        handler:function (g, rowIndex) {
                            this.onEditCategory (g, g.getStore ().getAt (rowIndex));
                        },
                        scope:this
                    }, {
                        tooltip:lang ('Delete'),
                        iconCls:'icon-delete-16 icon-16',
                        handler:this.onDeleteEvent,
                        scope:this
                    }
                ]
            }
        ];

        var archiveButton = new Ext.create ('Ext.Button', {
            text:lang ('Archive'),
            enableToggle:true,
            pressed:false
        });

        var categoryButton = new Ext.create('Ext.Button', {
            text:lang('Show news without categories'),
            enableToggle:true,
            pressed:false
        });

        var resetButton = new Ext.create ('Ext.Button', {
            text:lang ('ResetDates')
        });

        var startDate = new Ext.create ('Ext.form.field.Date', {
            format:'d-m-Y',
            submitFormat:'Y-m-d',
            fieldLabel:lang ('startPublish'),
            name:'starDate',
            labelWidth:65,
            width:160,
            value:this.firstDay ()
        });

        var endDate = new Ext.create ('Ext.form.field.Date', {
            format:'d-m-Y',
            submitFormat:'Y-m-d',
            fieldLabel:lang ('endPublish'),
            name:'endDate',
            labelWidth:65,
            width:160,
            value:this.lastDay ()
        });

        this.tbar = [
            {
                text:lang ('Create'),
                iconCls:'icon-create-16',
                handler:this.onEventCreate,
                scope:this
            },
            '-',
            '-',
            startDate,
            '-',
            endDate,
            '-',
            resetButton,
            '-',
            archiveButton,
            '-',
            categoryButton,
            '-'
        ];
        this.plugins = [ new Ext.ux.grid.Search ({
                minChars:1,
                stringFree:true,
                showSelectAll:false,
                disableIndexes:['publishDate', 'date'],
                align:2
            }) ].concat (Ext.isArray (this.plugins) ? this.plugins : []);
        this.bbar = Ext.create ('Ext.toolbar.Paging', {
                store:this.store,
                plugins:'pagesize'
            });

        startDate.on ('change', function () {
            if (startDate.isValid () == true) {
                this.firstDate = startDate.getValue();
                this.lastDate = endDate.getValue();
                this.getStore ().load ();
            } else {
                Application.notificate (lang ("startDate is incorrect"));
            }
        },this);

        endDate.on ('change', function () {
            if (endDate.isValid () == true ) {
                this.firstDate = startDate.getValue();
                this.lastDate = endDate.getValue();
                this.getStore ().load ();
            } else {
                Application.notificate (lang ("endDate is incorrect"));
            }
        },this);

        archiveButton.on ('click', function (b) {
            this.outOfDate = false;
            if (this.isArchive == 1) {
                this.isArchive = 0;
            } else {
                this.isArchive = 1;
            }
            this.getStore ().load ();
        }, this);

        categoryButton.on('click', function(b) {
            this.outOfDate = false;
            if (this.isCategory == 1) {
                this.isCategory = 0;
            } else {
                this.isCategory = 1;
            }
            this.getStore().load();
        }, this);

        resetButton.on ('click', function (b) {
            startDate.setValue (this.firstDay ());
            endDate.setValue (this.lastDay ());
            this.getStore ().load ();
        }, this);

        checkColumn.on ('checkchange', function (cc, rowIndex, isChecked) {
            var record = this.getStore ().getAt (rowIndex);
            this.onChangeResourceState (record.get ('id'), isChecked, record);
            this.getStore ().load ();
        }, this);

        this.callParent ();

        this.getStore ().on ('beforeload', function (store) {
            if (this.firstDate==null) {
                this.firstDate = this.firstDay();
                this.lastDate = this.lastDay();
            }
            Ext.apply (this.getStore ().getProxy ().extraParams, {
                isArchive:this.isArchive,
                startDate :this.firstDate,
                endDate: this.lastDate ,
                isCategory: this.isCategory
            });
        }, this);

    },

    firstDay:function () {
        date = new Date();
        var month = date.getMonth();
        day = new Date(date.getFullYear(), month, 1);
        return new Date(day);
    },

    lastDay:function () {
        date = new Date ();
        var month = date.getMonth () + 1;
        day = new Date (date.getFullYear (), month, 1);
        return new Date (day-1);
    },

    onChangeResourceState:function (id, value, record) {
        Ext.Ajax.request ({
            url:link ('event', 'main', 'archive', {format:'json'}),
            params:{
                id:id,
                isArchive:value
            },
            success:function (response) {
                var decResponse = Ext.decode (response.responseText);
                Application.notificate (decResponse.messages);
                if (decResponse.success) {
                }
            }
        });
    },

    onEditCategory:function (g, record) {
        Application.require ([ 'event/./form' ], function () {
                var f = new Module.Event.Form ({
                        eventId:record.internalId
                    });
                f.categoryId = this.categoryId;
                f.on ('completed', function (clientId) {
                        g.getStore ().load ();
                    }, this);
                f.showInWindow ();
            }, this);
    },

    onDeleteEvent:function (g, rowIndex) {
        var record = g.getStore ().getAt (rowIndex);
        Ext.Msg.confirm (lang ('Confirmation'), lang ('Are you sure?'), function (b) {
                if (b != 'yes') {
                    return;
                }
                Ext.Ajax.request ({url:link ('event', 'main', 'delete', {format:'json'}),
                        method:'POST',
                        params:{
                            id:record.internalId
                        },
                        success:function (response, options) {
                            var decResponse = Ext.decode (response.responseText);
                            Application.notificate (decResponse.messages);
                            if (true == decResponse.success) {
                                g.getStore ().load ();
                            }
                        },
                        scope:this
                    });
            }, this);
    },

    onEventCreate:function () {
        Application.require ([ 'event/./form' ], function () {
                var f = new Module.Event.Form ({
                        categoryId:this.categoryId
                    });
                f.categoryId = this.categoryId;
                f.on ('completed', function (clientId) {
                        this.getStore ().load ();
                    }, this);
                f.showInWindow ();
            }, this);
        this.Reload;
    },

    setCategoryId:function (categoryId, forceReload) {
        this.categoryId = categoryId;
        var p = this.getStore ().getProxy ();
        p.extraParams = p.extraParams || {};
        p.extraParams.categoryId = categoryId;
        if (true === forceReload) {
            this.getStore ().load ();
        }
        return this;
    }

});