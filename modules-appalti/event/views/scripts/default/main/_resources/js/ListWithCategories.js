Ext.define ('Module.Event.ListWithCategories', {
    extend:'Ext.ux.grid.GridPanel',
    alias:'widget.module.event.list-with-categories',

    stateful: false,
    stateId: false,
    filterRequestParam:null,

    features:[
        {
            ftype:'filters'
        }, {
            id: 'group',
            ftype: 'groupingsummary',
            groupHeaderTpl: '{name}',
            hideGroupedHeader: true,
            remoteRoot: 'summaryData'
        }
    ],

    modeReadOnly:false,
    dataUrl:null,
    eventId:null,
    isArchive:0,
    categoryId:null,

    initComponent:function () {
        this.store = new Ext.data.Store ({
                model:'Module.Event.Model.Event',
                proxy:{
                    type:'ajax',
                    url:link ('event', 'main', 'fetch-all', {format:'json'}),
                    reader:{
                        type:'json',
                        root:'rowset'
                    }
                },
                groupField: 'name'
            });

        var checkColumn = new Ext.ux.CheckColumn ({
            header:lang ('Archive'),
            dataIndex:'isArchive',
            width:50
        });

        this.columns = [
            {
                header:lang ('Title'),
                dataIndex:'title',
                flex:1
            }, {
                header:lang ('Category'),
                dataIndex:'name',
                flex:1
            },{
                header:lang ('PublishDate'),
                xtype:'datecolumn',
                dataIndex:'publishDate',
                format:'d-m-Y',
                width:120
            }, {
                header:lang ('Date'),
                xtype:'datecolumn',
                dataIndex:'date',
                format:'d-m-Y',
                width:120
            },  {
                xtype:'actioncolumn',
                header:lang ('Actions'),
                width:50,
                fixed:true,
                items:[
                    {
                        tooltip:lang ('Edit'),
                        iconCls:'icon-edit-16 icon-16',
                        handler:function (g, rowIndex) {
                            this.onEditCategory (g, g.getStore ().getAt (rowIndex));
                        },
                        scope:this
                    }, {
                        tooltip:lang ('Delete'),
                        iconCls:'icon-delete-16 icon-16',
                        handler:this.onDeleteEvent,
                        scope:this
                    }
                ]
            }
        ];

        var archiveButton = new Ext.create ('Ext.Button', {
            text:lang ('Archive'),
            enableToggle:true,
            pressed:false
        });

        this.tbar = [
            archiveButton,
            '-'
        ];
        this.plugins = [ new Ext.ux.grid.Search ({
                minChars:3,
                stringFree:true,
                showSelectAll:false,
                disableIndexes:['publishDate', 'date'],
                align:2
            }) ].concat (Ext.isArray (this.plugins) ? this.plugins : []);
        this.bbar = Ext.create ('Ext.toolbar.Paging', {
                store:this.store,
                plugins:'pagesize'
            });


        archiveButton.on ('click', function (b) {
            this.outOfDate = false;
            if (this.isArchive == 1) {
                this.isArchive = 0;
            } else {
                this.isArchive = 1;
            }
            this.getStore ().load ();
        }, this);

        checkColumn.on ('checkchange', function (cc, rowIndex, isChecked) {
            var record = this.getStore ().getAt (rowIndex);
            this.onChangeResourceState (record.get ('id'), isChecked, record);
            this.getStore ().load ();
        }, this);

        this.callParent ();

        this.getStore ().on ('beforeload', function (store) {
            Ext.apply (this.getStore ().getProxy ().extraParams, {
                isArchive:this.isArchive,
            });
        }, this);

    },

    onChangeResourceState:function (id, value, record) {
        Ext.Ajax.request ({
            url:link ('event', 'main', 'archive', {format:'json'}),
            params:{
                id:id,
                isArchive:value
            },
            success:function (response) {
                var decResponse = Ext.decode (response.responseText);
                Application.notificate (decResponse.messages);
                if (decResponse.success) {
                }
            }
        });
    },

    onEditCategory:function (g, record) {
        Application.require ([ 'event/./form' ], function () {
                var f = new Module.Event.Form ({
                        eventId:record.internalId
                    });
                f.categoryId = this.categoryId;
                f.on ('completed', function (clientId) {
                        g.getStore ().load ();
                    }, this);
                f.showInWindow ();
            }, this);
    },

    onDeleteEvent:function (g, rowIndex) {
        var record = g.getStore ().getAt (rowIndex);
        Ext.Msg.confirm (lang ('Confirmation'), lang ('Are you sure?'), function (b) {
                if (b != 'yes') {
                    return;
                }
                Ext.Ajax.request ({url:link ('event', 'main', 'delete', {format:'json'}),
                        method:'POST',
                        params:{
                            id:record.internalId
                        },
                        success:function (response, options) {
                            var decResponse = Ext.decode (response.responseText);
                            Application.notificate (decResponse.messages);
                            if (true == decResponse.success) {
                                g.getStore ().load ();
                            }
                        },
                        scope:this
                    });
            }, this);
    }
});