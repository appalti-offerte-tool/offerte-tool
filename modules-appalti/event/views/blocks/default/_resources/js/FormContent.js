Ext.define ('Module.Application.Model.Error', {
    extend:'Ext.data.Model',
    fields:[
        'message', 'type', 'field', {name:'id', mapping:'field'}, {name:'msg', mapping:'message'}
    ]
});
Ext.define ('Module.Event.Block.FormContent', {
    extend:'Ext.form.Panel',
    alias:'widget.event.block.form-content',
    bodyPadding:5,
    eventId:null,
    categoryId:null,
    initComponent:function () {
        this.initialConfig.trackResetOnLoad = true;
        this.items = [{
            xtype: 'fieldcontainer',
            layout: 'hbox',
            defaults: {
                border: false
            },
            items: [{
                xtype: 'panel',
                border: false,
                width:'100%',
                layout: 'anchor',
                items: [
                        {
                            xtype:'textfield',
                            fieldLabel:lang ('Title') + '<span class="asterisk">*</span>',
                            labelWidth:100,
                            anchor: '100%',
                            allowBlank:false,
                            name: 'title'
                        },
            //            {
            //                xtype:'textfield',
            //                fieldLabel:lang ('Publish'),
            //                labelWidth:130,
            //                width:650,
            //                name:'publish'
            //            },

                        {
                            xtype:'textfield',
                            fieldLabel:lang ('PlaceDesc') + '<span class="asterisk">*</span>',
                            labelWidth:100,
                            anchor: '100%',
                            allowBlank:false,
                            name:'placeDesc'
                        } ,{
                        xtype:'htmleditor',
                        fieldLabel:lang('Description'),
                        name:'shortDesc',
                        height:100,
                        anchor:'100%',
                        labelWidth:100,
                        allowBlank:true
                    }      ]
            }]
        }, {
            xtype: 'panel',
            layout: 'hbox',
            border: false,

            defaults: {
                flex: 0.5,
                defaults: {
                    labelWidth: 70
                }
            },
            items: [{
                xtype: 'fieldset',
                height:80,
                items: [{
                    xtype: 'label',
                    text: lang('Address'),
                    style: {
                        fontWeight: 'bold'
                    }
                }, {
                    xtype:'textfield',
                    fieldLabel:lang ('AddressCity and Post Code') + '<span class="asterisk">*</span>',
                    labelWidth:200,
                    allowBlank:false,
                    margin: '5 0 0 0',
                    name:'addressCity'
                }, {
                    xtype:'textfield',
                    allowBlank:false,
                    margin: '-60 0 5 360',
                    width:80,
                    name:'addressPostCode'
                }, {
                    xtype:'textfield',
                    fieldLabel:lang ('AddressStreet and HouseNumber') + '<span class="asterisk">*</span>',
                    labelWidth:200,
                    allowBlank:false,
                    name:'addressStreet'
                }, {
                    xtype:'textfield',
                    allowBlank:false,
                    margin: '-60 0 10 360',
                    width:80,
                    height:22,
                    name:'addressHouseNumber'
                }]
            }, {
                xtype: 'fieldset',
                margin: '0 0 0 5',
                height:80,
                width: '400px',
                items: [{
                    xtype: 'label',
                    text: lang('Dates'),

                    style: {
                        float: 'left',
                        fontWeight: 'bold',
                        marginRight: '100px'
                    }
                }, {
                    xtype:'datefield',
                    format:'d-m-Y',
                    submitFormat:'Y-m-d',
                    fieldLabel:lang ('Start publish event') + '<span class="asterisk">*</span>',
                    name:'publishDate',
                    labelWidth:130,
                    margin: '5 0 5 0',
                    allowBlank:false,
                    width:240
                }, {
                    xtype:'datefield',
                    format:'d-m-Y',
                    submitFormat:'Y-m-d',
                    name:'date',
                    fieldLabel:lang ('Event date and time') + '<span class="asterisk">*</span>',
                    labelWidth:130,
                    allowBlank:false,
                    width:240
                }, {
                    xtype:'timefield',
                    name:'time',
                    minValue:'00:00',
                    maxValue:'23:00',
                    increment:30,
                    margin: '-60 0 5 250',
                    allowBlank:false,
                    width:100
                }]
            }]
        }];

        var field = Ext.widget ('tinymcefield', {
            border:false,
            labelAlign:'top',
            anchor:"100%",
            name:'desc',
            allowBlank:0,
            margin:'0 0 12 0',
            height:420,
            tinymceConfig:{
                theme:"advanced",
                plugins:"pagebreak,table,advhr,advimage,advlink,iespell,insertdatetime,media,searchreplace,print,contextmenu,paste",
                theme_advanced_buttons1:"bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
                theme_advanced_buttons2:"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,|,forecolor,backcolor",
                theme_advanced_buttons3:"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,iespell,media,advhr,|,print,|pagebreak",
                theme_advanced_buttons4:"",
                theme_advanced_toolbar_location:"top",
                theme_advanced_toolbar_align:"left",
                theme_advanced_statusbar_location:"bottom",
                theme_advanced_resizing:false,
                extended_valid_elements:"a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
                file_browser_callback:Ext.bind (this.fileCallbackFn, this),
                baseURI:'',
                relative_urls:false,
                convert_urls:false,
                skin:'o2k7'
            }
        }, this);
        this.items.push (field);
        this.callParent (arguments);
    },
    fileCallbackFn:function (field_name, url, type, win) {
        var eventId = this.eventId;
        var me = this;
        Application.require ([
            'file-stock/image/image-layout'
        ], function () {
            var field = win.document.forms[0].elements[field_name];
            var fi = new Module.FileStock.Image.Layout ({
                entityId:eventId,
                extraParams:{
                    eventCategoryId:this.categoryId
                },
                enablePickerContextMenu:true,
                entityName:'eventId',
                entityType:'event',
                controller:'event-file-stock',
                module:'event'
            }, this);
            var w = new Ext.Window ({
                modal:true,
                width:650,
                height:350,
                border:false,
                title:lang ('Image dialogue'),
                layout:'fit',
                items:[fi]
            });
            fi.on ('choose-image', function (p, record, link) {
                field.value = link;
                Ext.fly (field).blur ();
                if (Ext.isFunction (win.ImageDialog.getImageData)) {
                    win.ImageDialog.getImageData ();
                }
                if (Ext.isFunction (win.ImageDialog.showPreviewImage)) {
                    win.ImageDialog.showPreviewImage (link);
                }
                w.close ();
            }, this);
            fi.on ('entity-change', function (image, eventId) {
                this.eventId = eventId;
                me.eventId = eventId;
            }, this);
            w.show (Ext.get (field));
        }, this);
    },
    setEventId:function (eventId) {
        this.eventId = eventId;
        return this;
    },
    setCategoryId:function (categoryId) {
        this.categoryId = categoryId;
        return this;
    }

});