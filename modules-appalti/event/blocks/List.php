<?php

final class Event_Block_List extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Event List');
    }

    protected function _toHtml()
    {
        $categoryId = $this->_getParam('categoryId');
        $this->view->assign('categoryId', $categoryId);
    }
}
