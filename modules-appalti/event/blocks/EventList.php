<?php

final class Event_Block_EventList extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Event List');
    }

    protected function _toHtml()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(20);

        $params = $this->getRequest()->getParams();
        $params['isArchive'] = 0;

        $params['filter'] = $this->getFilterParams();

        $service = new Event_Service_Event();
        $response = $service->fetchAll($params);
        $this->view->assign('rowset', $response->getRowset());

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;

        $this->blockId = $this->getId();

        $this->view->listOnly = $this->_getParam('listOnly');
    }
}
