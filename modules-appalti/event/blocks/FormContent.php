<?php

final class Event_Block_FormContent extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Content');
    }

    protected function _toHtml()
    {
        $categoryId = $this->_getParam('categoryId', false);
        $eventId = $this->_getParam('eventId', false);
        $service = new \Event_Service_Event();
        $this->view->categoryId = $categoryId;
        if (!empty($eventId)) {
            $this->view->eventId = $eventId;
            $this->view->eventRow = $service->find($eventId);
        } else {
            $eventRow = $service->findWithEmptyFlag($categoryId, false);
            if (!empty($eventRow)) {
                $this->view->eventId = $eventRow->getId();
                $this->view->eventRow = $eventRow;
            }
        }


    }
}
