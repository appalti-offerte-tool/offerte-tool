<?php

final class Event_Block_CompaniesList extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Companies');
    }

    protected function _toHtml()
    {
        $eventId = $this->_getParam('eventId');
        $this->view->assign('eventId', $eventId);
    }
}
