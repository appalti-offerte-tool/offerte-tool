<?php

final class Event_Block_FormPreview extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Preview');
    }

    protected function _toHtml()
    {

        $eventId = $this->_getParam('eventId', false);
        if (!empty($eventId)) {
            $this->view->eventId = $eventId;
            $service = new \Event_Service_Event();
            $data = $service->find($eventId);
            $this->view->eventRow = $data;

        }
    }
}
