<?php

final class Event_Misc_Navigation_EventDetailPageMvc extends Zend_Navigation_Page_Mvc
{
    protected $_isCompleted = false;

    /**
     * (non-PHPdoc)
     * @see Zend_Navigation_Page::getLabel()
     */
    public function getLabel()
    {
        if (true !== $this->_isCompleted) {
            $eventId = (int) Zend_Controller_Front::getInstance()->getRequest()->getParam('eventId');
            if (!empty($eventId)) {
                $eventService = new Event_Service_Event();
                $eventRow = $eventService->find($eventId);

                $this->_label .= ' ' . $eventRow->title;
            }

            $this->_isCompleted = true;
        }

        return parent::getLabel();
    }
}