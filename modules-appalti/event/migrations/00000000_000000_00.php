<?php

class Event_Migration_00000000_000000_00 extends Core_Migration_Abstract
{

    public function up()
    {
        $this->createTable('eventCategory');
        $this->createColumn('eventCategory', 'name', Core_Migration_Abstract::TYPE_VARCHAR, 50);
       
        $this->createTable('event');
        $this->createColumn('event', 'eventCategoryId', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createColumn('event', 'title', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('event', 'publish', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('event', 'publishDate', Core_Migration_Abstract::TYPE_TIMESTAMP);
        $this->createColumn('event', 'addressStreet', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('event', 'addressHouseNumber', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('event', 'addressPostCode', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('event', 'addressCity', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('event', 'placeDesc', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('event', 'date', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('event', 'time', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('event', 'shortDesc', Core_Migration_Abstract::TYPE_TEXT);
        $this->createColumn('event', 'desc', Core_Migration_Abstract::TYPE_TEXT);
        $this->createColumn('event', 'accountId', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createColumn('event', 'isArchive', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createColumn('event', 'emptyFlag', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createColumn('event', 'directShareLink', Core_Migration_Abstract::TYPE_VARCHAR, 50);

        $this->createIndex('event', 'eventCategoryId');
        $this->createForeignKey('event', array('eventCategoryId'), 'eventCategory', array('id'), 'FK_eventCategoryId');

        $this->createTable('eventSubscription');
        $this->createColumn('eventCategory', 'companyId', Core_Migration_Abstract::TYPE_INT, 11);
        $this->createColumn('eventCategory', 'eventCategoryId', Core_Migration_Abstract::TYPE_INT, 11);

        $this->createForeignKey('eventSubscription', array('eventCategoryId'), 'eventCategory', array('id'), 'FK_eventCategoryId');
        $this->createForeignKey('eventSubscription', array('companyId'), 'company', array('id'), 'FK_accountId');

    }


    public function down()
    {
        $this->dropTable('event');
        $this->dropTable('eventCategory');
    }


}

