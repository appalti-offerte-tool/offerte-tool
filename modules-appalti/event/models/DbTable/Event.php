<?php


class Event_Model_DbTable_Event extends OSDN_Db_Table_Abstract
{

    protected $_primary = 'id';

    protected $_name = 'event';

    protected $_rowClass = 'Event_Model_EventRow';

    protected $_nullableFields = array(
        'publish'
    );

    public function insert(array $data)
    {
        $account = Zend_Auth::getInstance()->getIdentity();
        $account = $account->getId();
        $data['accountId'] = $account;
        return parent::insert($data);
    }

}
