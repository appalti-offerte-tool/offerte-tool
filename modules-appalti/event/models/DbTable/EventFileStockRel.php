<?php
class Event_Model_DbTable_EventFileStockRel extends OSDN_Db_Table_Abstract
{

    protected $id;

    protected $_name = 'eventFileStockRel';

    protected $_nullableFields = array(
        'isThumbnail', 'parentId'
    );

    protected $_referenceMap = array(
        'FileStock'     => array(
            'columns'       => 'fileStockId',
            'refTableClass' => 'FileStock_Model_DbTable_FileStock',
            'refColumns'    => 'id'
        ),
        'Parent'        => array(
            'columns'       => 'parentId',
            'refTableClass' => __CLASS__,
            'refColumns'    => 'id'
        ),
        'ProposalBlock' => array(
            'columns'       => 'eventId',
            'refTableClass' => 'Event_Model_DbTable_Event',
            'refColumns'    => 'id'
        )
    );

    protected $_rowClass = '\Event_Model_EventFileStockRelRow';

}