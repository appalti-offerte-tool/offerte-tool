<?php

class Proposal_Service_ProposalBulk extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Proposal_Model_Proposal
     */
    protected $_proposalRow;

    public function __construct(\Proposal_Model_Proposal $proposalRow)
    {
        $this->_proposalRow = $proposalRow;

        parent::__construct();

        $this->_attachValidationRules('default', array(
            'companyId' => array('allowEmpty' => false, 'presence' => 'required'),
            'accountId' => array('allowEmpty' => false, 'presence' => 'required'),
            'number' => array('allowEmpty' => false, 'presence' => 'required')
        ));
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \Proposal_Model_Proposal
     */
    public function find($id, $throwException = true)
    {
        $proposalRow = $this->_table->findOne($id);
        if (null === $proposalRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find proposal #' . $id);
        }

        return $proposalRow;
    }

    public function bulk(array $data)
    {
        $this->getAdapter()->beginTransaction();

        $sectionService = new Proposal_Service_Section();
        $definitionService = new Proposal_Service_ProposalBlockDefinition($this->_proposalRow);

        try {

//            $definitionService->flush(false);

            foreach($data as $sectionId => $si) {

                $sectionRow = $sectionService->find($sectionId);

                if (!empty($si['blocks']) && is_array($si['blocks'])) {
                    $definitionService->bulk($sectionRow, $si['blocks'], false);
                }
            }

            $this->getAdapter()->commit();
        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();

            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();

            throw new OSDN_Exception('Unable to save section definition', null, $e);
        }

        return true;
    }
}
