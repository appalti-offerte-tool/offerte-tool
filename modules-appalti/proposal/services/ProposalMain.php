<?php

class Proposal_Service_ProposalMain extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \Proposal_Model_DbTable_Proposal
     */
    protected $_table;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new Proposal_Model_DbTable_Proposal($this->getAdapter());

        parent::_init();
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \Proposal_Model_Proposal
     */
    public function find($id, $throwException = true)
    {
        $proposalRow = $this->_table->findOne($id);
        if (null === $proposalRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find proposal #' . $id);
        }

        return $proposalRow;
    }

    public function fetchAll(array $params = array())
    {

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('p' => 'proposal'),
                ('*')
            )
            ->join(
                array('c' => 'company'),
                'p.clientCompanyId = c.id',
                array()
            )
            ->where('p.companyId = ?', $params['companyId'], Zend_Db::INT_TYPE)
            ->where('p.clientCompanyId = ?', $params['clientCompanyId']);
        $select->order('p.status');
        $fields = array(
            'p.name'    => 'name',
            'p.luid'    => 'luid',
            'c.name'    => 'client'
        );
        $this->_initDbFilter($select, $this->_table, $fields)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);

        return $response;
    }

    public function fetchAllLast()
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('p' => 'proposal')
            )
            ->where('p.companyId = ?', $accountRow->getCompanyRow()->getId(), Zend_Db::INT_TYPE)
            ->order('createdDatetime DESC')
            ->limit(10);

        if ($accountRow->isProposalBuilder()) {
            $select->where('p.accountId = ?', $accountRow->getId(), Zend_Db::INT_TYPE);
        }

        $response = $this->getDecorator('response')->decorate($select);

        $table = $this->_table;
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });

        return $response;
    }

    /**
     * Create the new proposal row
     *
     * @param array $data
     * @return Proposal_Model_Proposal
     */
    public function create(array $data)
    {
        $this->_attachValidationRules('default', array(
            'accountId'             => array('id', 'allowEmpty' => false, 'presence' => 'required'),
            'companyId'             => array('id', 'allowEmpty' => false, 'presence' => 'required'),
            'clientCompanyId'       => array('id', 'allowEmpty' => false, 'presence' => 'required'),
            'clientContactPersonId' => array('id', 'allowEmpty' => false),
            'luid'                  => array('allowEmpty' => false, 'presence' => 'required')
        ));

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $data['accountId'] = $accountRow->getId();
        $data['companyId'] = $accountRow->getCompanyRow()->getId();

        $f = $this->_validate($data);

        $this->getAdapter()->beginTransaction();
        try {

            $proposalRow = $this->_table->createRow($f->getData());
            $proposalRow->save();

            $proposalTheme = new Proposal_Service_ProposalTheme($proposalRow->getAccountRow()->getCompanyRow());
            $proposalTheme->cloneThemeRow($proposalRow);

            if (!empty($data['theme']) && is_array($data['theme'])) {
                $this->_saveTheme($proposalRow, $data['theme']);
            }

            $this->getAdapter()->commit();
        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to create proposal', 0, $e);
        }

        return $proposalRow;
    }

    public function update(\Proposal_Model_Proposal $proposalRow, array $data)
    {
        $this->_attachValidationRules('update', array(
            'clientContactPersonId' => array('id', 'allowEmpty' => false)
        ));

        $f = $this->_validate($data, 'update');

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if ($accountRow->isProposalBuilder() && $accountRow->getId() != $proposalRow->accountId) {
            throw new OSDN_Exception('You do not have permission to this proposal');
        }

        $this->getAdapter()->beginTransaction();
        try {
            $proposalRow->setFromArray($f->getData());
            $proposalRow->save();
            if (!empty($data['theme']) && is_array($data['theme'])) {
                $this->_saveTheme($proposalRow, $data['theme']);
            }

            $this->getAdapter()->commit();
        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update proposal');
        }

        return $proposalRow;
    }

    public function getProposalSections()
    {
        $sectionSrv = new \Proposal_Service_Section();
        return $sectionSrv->fetchAll();
    }

    protected function _saveTheme(\Proposal_Model_Proposal $proposalRow, array $data)
    {
        $theme = $proposalRow->getThemeRow();
        $theme->setFromArray($data);
        $theme->save();
    }

    public function delete(\Proposal_Model_Proposal $proposalRow)
    {
        $this->getAdapter()->beginTransaction();
        try {

            $companyRow = $proposalRow->getAccountRow()->getCompanyRow();

            $proposalThemeService = new \Proposal_Service_ProposalTheme($companyRow);
            $proposalThemeService->deleteByProposalRow($proposalRow);

            $proposalBlockDefinitionService = new \Proposal_Service_ProposalBlockDefinition($proposalRow);
            $proposalBlockDefinitionService->flush(false);

            $proposalRow->delete();
            $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to create proposal');
        }
        return true;
    }

    /**
     * @FIXME Finish send message
     */
    public function send(\Proposal_Model_Proposal $proposalRow, $filesPath)
    {

        return;

        $mail = new Zend_Mail();

        $clientCompany = $proposalRow->getClientCompanyRow();
        $mail->addTo($clientCompany->email);

        $clientContactPerson = $clientCompany->getCompanyContactPersonRow($proposalRow->clientContactPersonId);
        $mail->addCc($clientContactPerson->email);

        foreach(new \DirectoryIterator($filesPath) as $f) {
            if ($f->isFile() && pathinfo($f->getFilename(), PATHINFO_EXTENSION) === 'pdf') {
                $pdfName = $f->getFilename();
                $pdf = $this->_outputPath . '/' . $pdfName;
                $at = new Zend_Mime_Part(readfile($pdf));
                $at->type        = 'application/pdf';
                $at->disposition = Zend_Mime::DISPOSITION_INLINE;
                $at->encoding    = Zend_Mime::ENCODING_BASE64;
                $at->filename    = $pdfName;

                $mail->addAttachment($at);
            }
        }

        $mail->send();
    }

}

