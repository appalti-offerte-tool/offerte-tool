<?php

class Proposal_Service_ProposalBlockDefinitionFileStock extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \Proposal_Model_DbTable_Proposal
     */
    protected $_table;

    /**
     * @var \Proposal_Model_DbTable_ProposalBlockDefinition
     */
    protected $_tableDefinition;

    /**
     * @var \FileStock_Service_FileStock
     */
    protected $_fileStock;

    /**
     * Proposal block file storage
     */
    protected $_proposalBlockStorage;


    public function _init()
    {
        $this->_table = new \Proposal_Model_DbTable_Proposal($this->getAdapter());
        $this->_tableDefinition = new \Proposal_Model_DbTable_ProposalBlockDefinition();
        $this->_blockTable = new \ProposalBlock_Model_DbTable_ProposalBlock($this->getAdapter());
        $this->_fileStock = new \FileStock_Service_FileStock(array(
            'baseFilePath'         => APPLICATION_PATH . '/modules-appalti/proposal/data/block-file/',
            'useCustomExtension'   => false,
            'identity'             => 'proposal-block'
        ));

        $pbFsService = new ProposalBlock_Service_ProposalBlockFileStock();
        $this->_proposalBlockStorage = $pbFsService->getStorage();

    }

    /**
     * Retrieve the filestock rows by article
     *
     * @param int $articleId
     * @return Zend_Db_Table_Rowset_Abstract
     * @throws OSDN_Exception
     */
    public function fetchAllFileStockByProposalBlockIdWithResponse($blockId)
    {
        $blockRow = $this->_tableDefinition->findOne($blockId);

        $fileStockTable = new \FileStock_Model_DbTable_FileStock($this->getAdapter());

        $select = $this->_tableDefinition->getAdapter()->select()
            ->from(
                array('df'    => 'proposalBlockDefinitionFileStockRel'),
                array()
            )
            ->join(
                array('f'    => 'fileStock'),
                'f.id = df.fileStockId',
                $fileStockTable->getAllowedColumns()
            )
            ->join(
                array('dfThumbnail' => 'proposalBlockDefinitionFileStockRel'),
                'dfThumbnail.parentId = df.id AND dfThumbnail.isThumbnail = 1',
                array(
                    'thumbnailFileStockId' => 'fileStockId',
                    'isThumbnail',
                    'isDefaultThumbnail'
                )
            )
            ->where('df.blockId = ?', $blockRow->getId(), Zend_Db::INT_TYPE)
            ->where('df.parentId IS NULL');

        $response = $this->getDecorator('response')->decorate($select);


        $storage = $this->_fileStock->factory();

        $response->setScope($this);
        $response->setRowsetCallback(function($row, $rowset, $scope) use ($storage, $blockRow) {

            $thumbnailFileStockRow = $scope->find($blockRow->getId(), $row['thumbnailFileStockId']);
            $row['thumbnailLink'] = $storage->toFilePathHost($thumbnailFileStockRow);

            $fileStockRow = $scope->find($blockRow->getId(), $row['id']);
            $row['link'] = $storage->toFilePathHost($fileStockRow);

            return $row;
        });

        return $response;
    }

    public function fetchFileStockThumbnailRowByProposal($blockId, $fileStockId = null)
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('df'    => 'proposalBlockDefinitionFileStockRel'),
                array('parentId')
            )
            ->join(
                array('f'    => 'fileStock'),
                'f.id = df.fileStockId',
                array('id')
            )
            ->join(
                array('dfp'    => 'proposalBlockDefinitionFileStockRel'),
                'df.parentId = dfp.id',
                array()
            )
            ->where('df.blockId = ?', $blockId, Zend_Db::INT_TYPE)
            ->where('df.isThumbnail = 1')
            ->limit(1);

        if (!empty($fileStockId)) {
            $select->where('dfp.fileStockId = ?', $fileStockId, Zend_Db::INT_TYPE);
        }

        $foundRow = $select->query()->fetch();
        if (empty($foundRow)) {
            return null;
        }

        $fileStockId = $foundRow['id'];

        if (null == ($fileStockRow = $this->_fileStock->find($fileStockId))) {
            return null;
        }

        $row = $fileStockRow->toArray();

        $storage = $this->_fileStock->factory();

        if (!empty($foundRow['parentId'])) {
            $proposalBlockFileStockRelation = new Proposal_Model_DbTable_ProposalBlockDefinitionFileStockRel();
            $proposalBlockFileStockRelationRow = $proposalBlockFileStockRelation->findOne($foundRow['parentId']) ;
            $row['parentLink'] = $storage->toFilePathHost($proposalBlockFileStockRelationRow->getFileStockRow());

        }

        $row['link'] = $storage->toFilePathHost($fileStockRow);

        return $row;
    }

    public function find($blockId, $fileStockId)
    {
        /**
         * @todo
         * Implement verification on assignment article to filestock
         */
        return $this->_fileStock->find($fileStockId);
    }

    public function create($proposalId, $fileStockId, $sectionId, $useInternalTransaction = true)
    {
        $fileStockRow = $this->_fileStock->find($fileStockId);
        $proposalRow = $this->_table->findOne($proposalId);

        $rawFileTransfer = new FileStock_Service_Transfer_Raw();
        $link = $this->_proposalBlockStorage->toFilePath($fileStockRow, true);
        $rawFileTransfer->addFile($link);

        require_once APPLICATION_PATH . '/modules/file-stock/views/scripts/filters/File/Duplicate.php';
        $rawFileTransfer->addFilter(new FileStock_View_Filter_File_Duplicate($link . '.tmp', null, true));

        $useInternalTransaction && $this->getAdapter()->beginTransaction();
        try {
            $newFileStockRow = $this->_fileStock->cloneRow($rawFileTransfer, $fileStockRow, array(), function(\FileStock_Model_DbTable_FileStockRow $newFileStockRow)
                use ($proposalRow, $fileStockRow, $sectionId) {

                $proposalRow->getTable()->getAdapter()->insert('proposalBlockDefinitionFileStockRel', array(
                    'fileStockId'     => $newFileStockRow->getId(),
                    'proposalId'      => $proposalRow->getId(),
                    'sectionId'       => $sectionId,
                    'isThumbnail'     => 0,
                    'isDefaultThumbnail'=> 0
                ));

            });

            $useInternalTransaction && $this->getAdapter()->commit();

        } catch(\Exception $e) {
            $useInternalTransaction && $this->getAdapter()->rollBack();

            throw $e;
        }

        return $newFileStockRow;
    }

    public function createNew(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {
        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }

        $blockRow = $this->_tableDefinition->findOne($data['blockDefinitionId']);

        $proposalBlockFileStockRelation = new \Proposal_Model_DbTable_ProposalBlockDefinitionFileStockRel();
        $proposalBlockFileStockRelationRow = null;

        $fileStockRow = $this->_fileStock->create($transfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
            use ($blockRow, $proposalBlockFileStockRelation, & $proposalBlockFileStockRelationRow) {

            $proposalBlockFileStockRelationRow = $proposalBlockFileStockRelation->createRow(array(
                'fileStockId'   => $fileStockRow->getId(),
                'proposalId'    => $blockRow->proposalId,
                'blockId'       => $blockRow->getId()
            ));
            $proposalBlockFileStockRelationRow->save();
        });

        $rawFileTransfer = new FileStock_Service_Transfer_Raw();

        $storage = $this->_fileStock->factory();
        $link = $storage->toFilePath($fileStockRow, true);

        $rawFileTransfer->addFile($link);

        /**
         * @FIXME
         */
        require_once APPLICATION_PATH . '/modules/file-stock/views/scripts/filters/File/Duplicate.php';
        $rawFileTransfer->addFilter(new FileStock_View_Filter_File_Duplicate($link . '.tmp', null, true));

        $isf = new Zend_Filter_ImageSize();
        $isf->setHeight(100);
        $isf->setWidth(100);
        $isf->setThumnailDirectory(dirname($link));
        $rawFileTransfer->addFilter($isf);

        $fileStockThumRow = $this->_fileStock->create($rawFileTransfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
            use ($blockRow, $proposalBlockFileStockRelationRow, $proposalBlockFileStockRelation) {

            $hasPredefinedThumbnail = $proposalBlockFileStockRelation->count(array(
                'proposalId = ?' => $blockRow->proposalId,
                'isThumbnail = 1'
            )) > 0;

            $blockRow->getTable()->getAdapter()->insert('proposalBlockDefinitionFileStockRel', array(
                'fileStockId'           => $fileStockRow->getId(),
                'proposalId'            => $blockRow->proposalId,
                'blockId'               => $blockRow->getId(),
                'isThumbnail'           => 1,
                'isDefaultThumbnail'    => ! $hasPredefinedThumbnail,
                'parentId'              => $proposalBlockFileStockRelationRow->id
            ));
        });

        return array($blockRow, $fileStockRow);
    }

    public function update(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {
        $proposalBlockRow = $this->_table->findOne($data['blockId']);
        if (empty($proposalBlockRow)) {
            throw new OSDN_Exception('Unable to find proposal block #' . $data['blockId']);
        }

        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }

        $this->getAdapter()->beginTransaction();
        try {
            $fileStockRow = $this->_fileStock->update($transfer, $data);

            $proposalBlockFileStockRelation = new ProposalBlock_Model_DbTable_ProposalBlockFileStockRel();
            $proposalBlockFileStockRelationRow = $proposalBlockFileStockRelation->fetchRow(array(
                'blockId = ?'    => $proposalBlockRow->getId(),
                'fileStockId = ?'       => $fileStockRow->getId(),
                'parentId IS NULL'
            ));

            if (null !== $proposalBlockFileStockRelationRow) {

                foreach($proposalBlockFileStockRelationRow->getChildrenRowset() as $row) {
                    $this->_fileStock->delete($row->fileStockId, function($fileStockRow) use ($row) {
                        $row->delete();
                    });
                }
            }

            $rawFileTransfer = new FileStock_Service_Transfer_Raw();

            $storage = $this->_fileStock->factory();
            $link = $storage->toFilePath($fileStockRow, true);

            $rawFileTransfer->addFile($link);

            /**
             * @FIXME
             */
            require_once APPLICATION_PATH . '/modules/file-stock/views/scripts/filters/File/Duplicate.php';
            $rawFileTransfer->addFilter(new FileStock_View_Filter_File_Duplicate($link . '.tmp', null, true));

            $isf = new Zend_Filter_ImageSize();
            $isf->setHeight(100);
            $isf->setWidth(100);
            $isf->setThumnailDirectory(dirname($link));
            $rawFileTransfer->addFilter($isf);

            $fileStockThumRow = $this->_fileStock->create($rawFileTransfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow) use ($proposalBlockRow, $proposalBlockFileStockRelationRow) {
                $proposalBlockRow->getTable()->getAdapter()->insert('proposalBlockFileStockRel', array(
                    'fileStockId'  => $fileStockRow->getId(),
                    'blockId'      => $proposalBlockRow->getId(),
                    'isThumbnail'  => 1,
                    'parentId'     => $proposalBlockFileStockRelationRow->id
                ));
            });


            $this->getAdapter()->commit();

        } catch(\Exception $e) {

            $this->getAdapter()->rollBack();
            throw $e;
        }

        return $fileStockRow;
    }

    public function delete($proposalId, $fileStockId, $useInternalTransaction = true, $blockId = null)
    {
        $proposalRow = $this->_table->findOne($proposalId);
        if (empty($proposalRow)) {
            throw new OSDN_Exception('Unable to find proposal #' . $proposalId);
        }

        $useInternalTransaction && $this->getAdapter()->beginTransaction();
        try {

            $proposalBlockDefinitionFileStockRelation = new \Proposal_Model_DbTable_ProposalBlockDefinitionFileStockRel();
            $scope = $this->_fileStock;

            $callbackFn = function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
                use ($proposalRow, $proposalBlockDefinitionFileStockRelation, $scope, & $callbackFn, $blockId)
            {
                $params = array(
                    'proposalId = ?'   => $proposalRow->getId(),
                    'fileStockId = ?'   => $fileStockRow->getId()
                );

                if (!empty($blockId)) {
                    $params['blockId = ?'] = $blockId;
                }

                $proposalBlockDefinitionFileStockRelationRow = $proposalBlockDefinitionFileStockRelation->fetchRow($params);

                if (null !== $proposalBlockDefinitionFileStockRelationRow) {

                    foreach($proposalBlockDefinitionFileStockRelationRow->getChildrenRowset() as $row) {
                        $scope->delete($row->fileStockId, $callbackFn);
                    }

                    $proposalBlockDefinitionFileStockRelationRow->delete();
                }
            };

            $result = $this->_fileStock->delete($fileStockId, $callbackFn);
            $useInternalTransaction && $this->getAdapter()->commit();

        } catch (\Exception $e) {
            $useInternalTransaction && $this->getAdapter()->rollBack();
            throw $e;
        }

        return (boolean) $result;
    }

    public function deleteByProposal(Proposal_Model_Proposal $proposalRow, $useInternalTransaction = true)
    {
        return $this->_deleteByCondition($proposalRow, null, $useInternalTransaction);

    }

    public function deleteByProposalAndSection(Proposal_Model_Proposal $proposalRow, \Proposal_Model_Section $sectionRow, $useInternalTransaction = true)
    {
        return $this->_deleteByCondition($proposalRow, $sectionRow, $useInternalTransaction);
    }

    protected function _deleteByCondition(Proposal_Model_Proposal $proposalRow, \Proposal_Model_Section $sectionRow = null, $useInternalTransaction = true)
    {

        $proposalBlockDefinitionFileStockRelation = new \Proposal_Model_DbTable_ProposalBlockDefinitionFileStockRel();

        $conditions = array(
            'proposalId = ?'    => $proposalRow->getId()
        );

        if (!\is_null($sectionRow)) {
            $conditions['sectionId = ?'] = $sectionRow->getId();
        }

        $rowset = $proposalBlockDefinitionFileStockRelation->fetchAll($conditions);

        $useInternalTransaction && $this->getAdapter()->beginTransaction();
        try {

            foreach($rowset as $row) {
                $this->delete($proposalRow->getId(), $row->fileStockId, false);
            }

            $useInternalTransaction && $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $useInternalTransaction && $this->getAdapter()->rollBack();

            throw $e;
        } catch(\Exception $e) {
            $useInternalTransaction && $this->getAdapter()->rollBack();

            echo $e;
            throw new OSDN_Exception('Unable to save proposal block definition', null, $e);
        }

        return true;
    }

    public function getStorage()
    {
        return $this->_fileStock->factory();
    }

    public function copyProposalBlockImage(Proposal_Model_Proposal $proposalRow, \ProposalBlock_Model_ProposalBlock $blockRow)
    {
        require_once APPLICATION_PATH . '/modules/file-stock/views/scripts/filters/File/Duplicate.php';
        $path = APPLICATION_PATH . '/modules-appalti/proposal/data/definition-block-kind-file/';
        $path = $path . sprintf('%d/%d/', $proposalRow->getId(), $blockRow->getId());

        if (!is_dir($path)) {
            mkdir($path, 0775, true);
            chmod($path, 0775);
        }

        foreach($blockRow->toFilePaths(false) as $filepath) {
            $rawFileTransfer = new FileStock_Service_Transfer_Raw();
            $rawFileTransfer->addFile($filepath);
            $rawFileTransfer->addFilter(new FileStock_View_Filter_File_Duplicate($filepath . '.tmp', null, true));
            $rawFileTransfer->addFilter(new Zend_Filter_File_Rename($path . basename($filepath)));
            $rawFileTransfer->receive();
        }

        return true;
    }

}