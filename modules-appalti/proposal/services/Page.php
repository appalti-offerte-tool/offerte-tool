<?php

final class Proposal_Service_Page
{
    /**
     * @var \Zend_Db_Table_Rowset_Abstract
     */
    protected $_blockDefinitionRowset;

    protected $_proposalTheme;

    /**
     * @var \Proposal_Model_Proposal
     */
    protected $_proposalRow;

    /**
     * @var \Proposal_Model_Section
     */
    protected $_sectionRow;

    /**
     * Source path to template definition
     *
     * @var string
     */
    protected $_path;

    protected $_command = array();

    protected $_outputPath;

    protected $_headerParams = '';
    protected $_footerParams = '';

    /**
     * @var Proposal_Service_Keyword
     */
    protected $_keywordService;

    public function __construct(
        \Proposal_Model_Proposal $proposal,
        \Zend_Db_Table_Rowset_Abstract $blockDefinitionRowset
    ) {
        $this->_proposalRow = $proposal;
        $this->_proposalTheme = $proposal->getThemeRow();

        $this->_blockDefinitionRowset = $blockDefinitionRowset;

        $this->_sectionRow = $this->_blockDefinitionRowset->current()->getSectionRow();

        $this->_path = realpath(APPLICATION_PATH . '/data/public-templates');

        $this->_keywordService = new Proposal_Service_Keyword($this->_proposalTheme);

        if (false === $this->_path) {
            throw new OSDN_Exception('Unable to find source directory with templates');
        }
    }

    public function setOutputPath($path)
    {
        $this->_outputPath = $path;
        return $this;
    }

    public function setHeaderFooterParams(array $params)
    {
        if (!empty($params)) {
            $this->_headerParams = !empty($params['header']) ? trim(join(' ', $params['header'])) : '';
            $this->_footerParams = !empty($params['footer']) ? trim(join(' ', $params['footer'])) : '';
        }
    }

    public function getCommand()
    {
        $this->run();

        return join(" ", $this->_command);
    }

    public function isValid()
    {
        return file_exists($this->_path . '/' . $this->_sectionRow->getLuid() . '/index.phtml');
    }

    public function run()
    {
        do {

            $metadata = array();
            do {
                $blockDefinitionRow = $this->_blockDefinitionRowset->current();

                if (null === $blockDefinitionRow || $blockDefinitionRow->isFitable()) {
                    break;
                }

                $metadata[] = $blockDefinitionRow;
                $this->_blockDefinitionRowset->next();

            } while($this->_blockDefinitionRowset->valid());


            if (count($metadata) > 0) {
                $this->_command[] = $this->_toOutput('index', $metadata);
                if (false !== ($hpath = $this->_toOutput('header', array($metadata[count($metadata)-1])))) {
                    $this->_command[] = sprintf('--header-html "%s" %s', $hpath, $this->_headerParams);
                }

                if (false !== ($fpath = $this->_toOutput('footer', array($metadata[count($metadata)-1])))) {
                    $this->_command[] = sprintf('--footer-html "%s" %s', $fpath, $this->_footerParams);
                }

                $metadata = array();
            }

            if (null !== $blockDefinitionRow && $blockDefinitionRow->isFitable()) {
                $this->_command[] = 'cover ' . $this->_toOutput('index', array($blockDefinitionRow));
                if (false !== ($hpath = $this->_toOutput('header', array($blockDefinitionRow)))) {
                    $this->_command[] = sprintf('--header-html "%s" %s', $hpath, $this->_headerParams);
                }

                if (false !== ($fpath = $this->_toOutput('footer', array($blockDefinitionRow)))) {
                    $this->_command[] = sprintf('--footer-html "%s" %s', $fpath, $this->_footerParams);
                }
            }

            $this->_blockDefinitionRowset->next();

        } while($this->_blockDefinitionRowset->valid());

    }

    protected function _toOutput($name, array $metadata)
    {
        $luid = $this->_sectionRow->getLuid();
        if (count($metadata) == 1) {
            list($blockDefinitionRow) = $metadata;
            if ($blockDefinitionRow->inBetween()) {
                $luid = Proposal_Model_Section::SECTION_INBETWEEN;
            }
        }

        $path = $this->_path . '/' . $this->_proposalTheme->getTemplateThemeBySection($this->_sectionRow) . '/' . $luid;

        $template = sprintf('%s/%s.phtml', $path, $name);
        if (false === ($template = realpath($template))) {
            return false;
        }

        $destination = $this->_toName($name);

        ob_start();

        /**
         * Used in template
         *
         * @var $company
         */
        $company = $this->_proposalRow->getAccountRow()->getCompanyRow();
        $clientCompany = $this->_proposalRow->getClientCompanyRow();
        require $template;
        $html = ob_get_clean();

        $html = $this->_keywordService->replace($html);

        $html = str_replace('TEMPLATE_PREFIX', $path, $html);

        if (preg_match_all('/src\s*=\s*[\'"](.+?)[\'"]/i', $html, $matches)) {
            $replacable = array();

            foreach($matches[1] as $url) {
                if (false !== stripos($url, 'data:image') || false === stripos($url, 'data')) {
                    continue;
                }

                $replacable[$url] = APPLICATION_PATH . $url;
            }

            if (!empty($replacable)) {
                $html = str_replace(array_keys($replacable), array_values($replacable), $html);
            }
        }

        file_put_contents($destination, $html);
        chmod($destination, 0777);

        return $destination;
    }

    protected function _toName($name)
    {
        return sprintf('%s/%s-%s.html', $this->_outputPath, $this->_proposalTheme->getTemplateThemeBySection($this->_sectionRow), uniqid($name));
    }
}