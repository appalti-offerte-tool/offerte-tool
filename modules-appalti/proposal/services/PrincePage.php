<?php

final class Proposal_Service_PrincePage
{
    /**
     * @var \Zend_Db_Table_Rowset_Abstract
     */
    protected $_blockDefinitionRowset;

    /**
     * @var Proposal_Model_ProposalTheme
     */
    protected $_proposalTheme;

    /**
     * @var \Proposal_Model_Proposal
     */
    protected $_proposalRow;

    /**
     * @var \Proposal_Model_Section
     */
    protected $_sectionRow;

    /**
     * @var Proposal_Service_ProposalOffer
     */
    protected $_proposalOfferService;

    /**
     * @var Proposal_Service_CustomTemplate
     */
    protected $_customTemplateSrv;

    /**
     * @var Proposal_Model_CustomTemplate
     */
    protected $_customTemplate = null;

    /**
     * Source path to template definition
     *
     * @var string
     */
    protected $_path;

    protected $_command = array();

    protected $_region = array();

    protected $_outputPath;

    protected $_company;

    protected $_clientCompany;

    /**
     * @var Proposal_Service_Keyword
     */
    protected $_keywordService;

    public function __construct(\Proposal_Model_Proposal $proposal, array $region) {
        $this->_proposalRow = $proposal;

        $this->_proposalTheme = $proposal->getThemeRow();

        $this->_proposalOfferService = new \Proposal_Service_ProposalOffer();

        $this->_customTemplateSrv = new \Proposal_Service_CustomTemplate();

        $this->_company = $this->_proposalRow->getCompanyRow();

        $this->_clientCompany = $this->_proposalRow->getClientCompanyRow();

        $this->_region = $region;

        $this->_path = APPLICATION_PATH . '/data/public-templates';

        $this->_keywordService = new Proposal_Service_Keyword($proposal);
    }

    public function setOutputPath($path)
    {
        $this->_outputPath = $path;
        return $this;
    }

    protected function _getCustomTemplate()
    {
        if (null === $this->_customTemplate) {
            $this->_customTemplate = $this->_customTemplateSrv->fetchByCompany($this->_company);
        }

        return $this->_customTemplate;
    }

    protected function _getBase64EncodedImg($file)
    {
        if( file_exists($file) && $fp = fopen($file,"rb", 0)) {
            $picture = fread($fp, filesize($file));
            fclose($fp);
            $base64 = chunk_split(base64_encode($picture));
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $ext = 'jpg' === $ext ? 'jpeg' : $ext;
            return sprintf('data:image/%s;base64,%s', $ext, $base64);
        }
    }

    protected function _toOutput($tpl, array $metadata = array(), $postProcessHtml = true)
    {
        /* to be used in template */
        $company = $this->_company;
        $clientCompany = $this->_clientCompany;
        $themeRow = $this->_proposalTheme;
        $customTpl = $themeRow->useCustomTemplate ? $this->_customTemplateSrv->find($themeRow->useCustomTemplate):null;

        ob_start();
        require $tpl . '/index.phtml';
        $html = ob_get_clean();
        
        if ($postProcessHtml) {
            $html = $this->_keywordService->replace($html);
            if (preg_match_all('/src\s*=\s*[\'"](.+?)[\'"]/i', $html, $matches)) {
                $replaceable = array();

                foreach($matches[1] as $url) {
                    if (false !== stripos($url, 'data:image') || false === stripos($url, 'data')) {
                        continue;
                    }

                    $replaceable[$url] = $this->_getBase64EncodedImg(APPLICATION_PATH . $url);
                }

                if (!empty($replaceable)) {
                    $html = str_replace(array_keys($replaceable), array_values($replaceable), $html);
                }
            }
        }

        return $html;
    }

    protected function _insertPrice($metadata, $priceHtml = null)
    {
        if (!empty($priceHtml)) {
            if (null === $this->_proposalTheme->pricePosition) {
                $metadata[] = $priceHtml;
            } else {
                array_splice($metadata, $this->_proposalTheme->pricePosition, 0, $priceHtml);
            }
        }

        return $metadata;
    }

    protected function _notoc($metadata)
    {
        return preg_replace('/<([hH][1-6]{1})(.+)?(>(?:.+)?<\/[hH][1-6]{1}>)/', '<$1 notoc="notoc"$2$3', $metadata);
    }

    /**
     * @param bool $returnContent
     * @return array|string
     * @example <code>
     *      array(
     *          'fileName'      => 'someFileName',
     *          'fileContent'   => 'someFileContent'
     *      )
     * </code>
     */
    public function getFile($returnContent = true)
    {
        $data = array();

        /**
         * to be used in templates
         */
        $company = $this->_proposalRow->getCompanyRow();
        $clientCompany = $this->_clientCompany;
        $themeRow = $this->_proposalTheme;
        $customTpl = $themeRow->useCustomTemplate ? $this->_getCustomTemplate() : null;

        foreach($this->_region as $sectionRow) {
            $priceHtml = null;
            $templateName = $themeRow->useCustomTemplate ?
                                'empty' :
                                $this->_proposalTheme->getTemplateThemeBySection($sectionRow);
            if ($sectionRow->isCalculatable() && $this->_proposalTheme->includeTOC) {
                ob_start();
                require $this->_path . '/common/toc.phtml';
                $tocHtml = ob_get_clean();
                continue;
            }

            $blockDefinitionRowset = $this->_proposalRow->fetchAllBlockDefinitionBySection($sectionRow);
            $metadata = array();

            if ('content' === $sectionRow->getLuid()) {
                if (!empty($tocHtml)) {
                    $data[] = $tocHtml;
                    $tocHtml = null;
                }

                if ($this->_proposalTheme->includePrice) {
                    $totalCost = $this->_proposalOfferService->getTotalCost($this->_proposalRow->getId());
                    if ($totalCost) {
                        ob_start();
                        $offers = $this->_proposalOfferService->fetchAll($this->_proposalRow->getId());
                        require $this->_path . '/' . $templateName . '/price/index.phtml';
                        $priceHtml = ob_get_clean();
                    }
                }
            }
            $tplPath = $this->_path . '/' . $templateName . '/';
            $sectionTpl = $tplPath . $sectionRow->getLuid();

            if ($blockDefinitionRowset->count() == 0) {
                if ('content' === $sectionRow->getLuid() && !empty($priceHtml)) {
                    $data[] = $priceHtml;
                    $priceHtml = null;
                }

                continue;
            }

            foreach($blockDefinitionRowset as $block) {
                if ($block->inBetween()) {
                    if ('file' !== $block->kind) {
                        $block->metadata = $this->_notoc($block->metadata);
                    }

                    $metadata[] = $this->_toOutput($tplPath . Proposal_Model_Section::SECTION_INBETWEEN, array($block), false);
                    continue;
                }

                if ('cover' == $sectionRow->getLuid()) {
                    $block->metadata = $this->_notoc($block->metadata);
                }

                $metadata[] = $block;
            }

            if ('content' === $sectionRow->getLuid() && !empty($metadata)) {
                $metadata = $this->_insertPrice($metadata, $priceHtml);
                $priceHtml = null;
            }

            $data[] = $this->_toOutput($sectionTpl, $metadata);
        }

        $fileName = '';
        $fileContent = '';

        if (!empty($data)) {
            $fileName = $this->_toName($sectionRow->getName());
            $contentHtml = join('', $data);
            ob_start();
            require $tplPath . 'template.phtml';
            $fileContent = ob_get_clean();
            file_put_contents($fileName, $fileContent);
        }

		$result = array(
			'fileNames' => array(
				'html' => $fileName,
				'pdf' => sprintf('%s %s.pdf', $sectionRow->getName(), $this->_clientCompany->getName()),
			)
		);

        if ($returnContent) {
            $result['fileContent'] = $fileContent;
        }

        return $result;
    }

    protected function _toName($name)
    {
        return sprintf('%s/%s.html', $this->_outputPath, md5($name . $this->_clientCompany->getName()));
    }
}