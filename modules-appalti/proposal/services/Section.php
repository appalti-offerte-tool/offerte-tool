<?php

/**
 * At the moment section are hardcoded
 *
 * @todo In the future it can be change to more flexible interface, like database
 */
class Proposal_Service_Section
{
    const REGION_INTRODUCTION = 1;
    const REGION_PROPOSAL = 2;
    const REGION_ATTACHMENT = 3;

    protected static $_regionRowset = array(
        self::REGION_INTRODUCTION => 'Begeleidend schrijven',
        self::REGION_PROPOSAL     => 'Offerte',
        self::REGION_ATTACHMENT   => 'Bijlagen'
    );

    /**
     * The initial structure of proposal's section definition
     *
     * @var array
     */

    protected $_rowset = array(
        array(
            'id'            => 1,
            'databaseId'    => 1,
            'name'          => 'Begeleidende brief',
            'luid'          => \Proposal_Model_Section::SECTION_INTRODUCTION,
            'position'      => 1,
            'related'       => 1,
            'regionId'      => self::REGION_INTRODUCTION,
            'isRequired'    => true,
            'place'         => true
        ),
        array(
            'id'            => 2,
            'databaseId'    => 8,
            'name'          => 'Voorblad',
            'luid'          => \Proposal_Model_Section::SECTION_COVER,
            'position'      => 2,
            'related'       => 8,
            'regionId'      => self::REGION_PROPOSAL,
            'isRequired'    => true,
            'place'         => true
        ),

        array(
            'id'            => 3,
            'databaseId'    => 9,
            'name'          => 'Inhoudsopgave',
            'description'   => 'automatisch',
            'luid'          => \Proposal_Model_Section::SECTION_TOC,
            'calculatable'  => true,
            'position'      => 3,
            'related'       => null,
            'regionId'      => self::REGION_PROPOSAL,
            'isRequired'    => false,
            'place'         => array('contentbuild')
        ),


        array(
            'id'            => 10,
            'databaseId'    => 10,
            'name'          => 'Tussenbladen',
            'luid'          => \Proposal_Model_Section::SECTION_INBETWEEN,
            'calculatable'  => false,
            'position'      => 3,
            'related'       => null,
            'regionId'      => self::REGION_PROPOSAL,
            'isRequired'    => false,
            'place'         => array('blockbuild')
        ),


        array(
            'id'            => 4,
            'databaseId'    => 9,
            'name'          => 'Offerte inhoud',
            'luid'          => \Proposal_Model_Section::SECTION_CONTENT,
            'position'      => 4,
            'related'       => 9,
            'isDefault'     => true,
            'regionId'      => self::REGION_PROPOSAL,
            'isRequired'    => true,
            'place'         => true
        ),
        array(
            'id'            => 5,
            'databaseId'    => 11,
            'name'          => 'Bijlagen',
            'luid'          => \Proposal_Model_Section::SECTION_ATTACHMENT,
            'position'      => 5,
            'related'       => 11,
            'regionId'      => self::REGION_ATTACHMENT,
            'isRequired'    => true,
            'place'         => true
        )
    );

    protected $_path = '/data/public-templates/';

    public function __construct()
    {
        $this->_path = realpath(APPLICATION_PATH . $this->_path);
    }

    /**
     * Retrieve rowset of Section object
     *
     * @return array The rowset of \Proposal_Model_Section
     */
    public function fetchAll($place = 'contentbuild')
    {
        $output = array();
        foreach($this->_rowset as $o) {
            if ($o['place'] !== true && (!is_array($o['place']) || !in_array($place, $o['place']))) {
                continue;
            }
            $output[] = new \Proposal_Model_Section($o);
        }

        return $output;
    }

    public function fetchAllCollapsedByRegion($regionId = null, $place = 'contentbuild')
    {
        $output = array();
        foreach($this->fetchAll($place) as $sectionRow) {
            if (empty($regionId)) {
                $output[$sectionRow->getRegionId()][] = $sectionRow;
            } else {
                if ($regionId == $sectionRow->getRegionId()) {
                    $output[$regionId][] = $sectionRow;
                }
            }
        }

        return $output;
    }

    public function fetchRegions()
    {
       return self::$_regionRowset;
    }

    public static function findRegion($regionId)
    {
        $regionId = (int) $regionId;
        if (isset(self::$_regionRowset[$regionId])) {
            return array('name' => self::$_regionRowset[$regionId]);
        }

        return null;
    }

    public function getRegionNameBySectionId($id)
    {
        $sectionId = (int) $id;
        foreach ($this->_rowset as $row) {
            if ($row['id'] === $sectionId) {
                return $row['name'];
            }
        }

        return null;
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \Proposal_Model_Section
     */
    public function find($id, $throwException = true)
    {
        $sectionRow = null;
        foreach($this->_rowset as $row) {
            if ($id == $row['id']) {
                $sectionRow = new \Proposal_Model_Section($row);
                break;
            }
        }

        if (null === $sectionRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find section #' . $id);
        }

        return $sectionRow;
    }

    public function findByRegion($region, $throwException = true)
    {
        $sections = array();
        foreach($this->_rowset as $row) {
            if ($region == $row['regionId']) {
                $sections[] = new \Proposal_Model_Section($row);
            }
        }

        if (empty($sections) && true === $throwException) {
            throw new OSDN_Exception('Unable to find section by region ' . $region);
        }

        return $sections;
    }

    public function findByLuid($luid, $throwException = true)
    {
        $sectionRow = null;
        foreach($this->_rowset as $row) {
            if ($luid == $row['luid']) {
                $sectionRow = new \Proposal_Model_Section($row);
                break;
            }
        }

        if (null === $sectionRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find section ' . $luid);
        }

        return $sectionRow;
    }

    public function findByRelatedCategoryId($id, $throwException = true)
    {
        $sectionRow = null;
        foreach($this->_rowset as $row) {
            if (!empty($row['calculatable'])) {
                continue;
            }

            if ($id == $row['databaseId']) {
                $sectionRow = new \Proposal_Model_Section($row);
                break;
            }
        }

        if (null === $sectionRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find section #' . $id);
        }

        return $sectionRow;
    }

    public function getGeneratorParams(
            \Proposal_Model_Section $sectionRow,
            \Proposal_Model_ProposalTheme $themeRow)
    {
        $params = array();
        $tpl = 'template' . ucfirst($sectionRow->getLuid());

        if (!empty($themeRow->$tpl)) {
            $configPath = $this->_path . '/' . $themeRow->$tpl . '/config.xml';
            if (file_exists($configPath)) {
                $sections = simplexml_load_file($configPath);
                foreach ($sections as $section) {
                    $sectionName = (string) $section->attributes()->name;
                    foreach ($section as $param) {
                        $attrs = $param->attributes();

                        if (empty($params[$sectionName])) {
                            $params[$sectionName] = array();
                            $params[$sectionName][(string)$attrs->type] = array();
                        }

                        $val = (string)$attrs->name . ' ' . (string)$attrs->value;
                        $params[$sectionName][(string)$attrs->type][] = $val;
                    }
                }
            }
        }

        return $params;
    }
}
