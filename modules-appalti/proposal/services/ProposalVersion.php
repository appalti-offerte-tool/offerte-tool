<?php

class Proposal_Service_ProposalVersion extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \Proposal_Model_DbTable_ProposalVersion
     */
    protected $_table;

    /**
     * @var \Proposal_Service_Section
     */
    protected $_proposalSectionSrv;

    /**
     * @var
     */
    protected $_path;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new Proposal_Model_DbTable_ProposalVersion($this->getAdapter());
        $this->_path = realpath(__DIR__ . '/../data/tmp');

        parent::_init();
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \Proposal_Model_ProposalVersion
     * @throws OSDN_Exception
     */
    public function find($id, $throwException = true)
    {
        $proposalRow = $this->_table->findOne($id);
        if (null === $proposalRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find proposal version by id #' . $id);
        }

        return $proposalRow;
    }

    /**
     * Fetch all available versions for specific proposal
     *
     * @param Proposal_Model_Proposal $proposalRow
     * @param array $params Filter params (limit, order, etc.)
     * @return OSDN_Db_Response
     */
    public function fetchAll(\Proposal_Model_Proposal $proposalRow, array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(array(
                'pv' => 'proposalVersion'
            ))
            ->where('pv.proposalId = ?', $proposalRow->getId(), Zend_Db::INT_TYPE);


        $this->_initDbFilter($select, $this->_table)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        $table = $this->_table;
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });

        return $response;
    }

    /**
     * @param Proposal_Model_ProposalVersion $v1
     * @param Proposal_Model_ProposalVersion | array $v2
     * @return array
     */
    protected function _compareHtml(\Proposal_Model_ProposalVersion $v1, $v2)
    {
        $proposalRegions = $this->_proposalSectionSrv->fetchRegions();
        $changed = array();

        foreach ($proposalRegions as $id => $name) {
            $html1 = $v1->getHtmlByRegionId($id);

            if (is_array($v2)) {
                $html2 = null;
                if (!empty($v2['html'][$id])) {
                    $html2 = !empty($v2['html'][$id]['hash']) ? $v2['html'][$id]['hash'] : md5($v2['html'][$id]['html']);
                }
            } else {
                $html2 = $v2->getId() ? $v2->getHtmlByRegionId($id) : '';
            }

            if (!empty($html1) && !empty($html2)) {
                if (0 !== strcasecmp($html1->hash, is_array($v2) ? $html2 : $html2->hash)) {
                    $changed[] = array(
                        'id'    => $id,
                        'name'  => $name,
                        'msg'   => 'Content changed'
                    );
                }

                continue;
            }

            if (empty($html1) && !empty($html2)) {
                $changed[] = array(
                    'id'    => $id,
                    'name'  => $name,
                    'msg'   => 'Added to proposal'
                );

                continue;
            }

            if (!empty($html1) && empty($html2)) {
                $changed[] = array(
                    'id'    => $id,
                    'name'  => $name,
                    'msg'   => 'Removed from proposal'
                );
            }
        }

        return $changed;
    }

    /**
     * @param Proposal_Model_ProposalVersion $v1
     * @param Proposal_Model_ProposalVersion | array $v2
     * @return array
     */
    protected function _compareBlocks(\Proposal_Model_ProposalVersion $v1, $v2)
    {
        $changed = array();

        $v1Blocks = unserialize($v1->proposalBlocks);
        $v2Blocks = unserialize(is_array($v2) ? $v2['proposalBlocks'] : $v2->proposalBlocks);

        foreach ($v1Blocks as $blockId => $block) {
            if (empty($v2Blocks[$blockId])) {
                $changed[] = array(
                    'id' => $blockId,
                    'name' => $block['name'],
                    'msg' => 'Removed'
                );

                continue;
            }

            $diff = array(
                'id' => $blockId,
                'name' => $block['name'],
                'messages' => array()
            );

            if ((int)$v2Blocks[$blockId]['sectionId'] !== (int)$block['sectionId']) {
                $diff['messages'][] = sprintf('Moved from section «%s» to section «%s»',
                    $this->_proposalSectionSrv->getRegionNameBySectionId($block['sectionId']),
                    $this->_proposalSectionSrv->getRegionNameBySectionId($v2Blocks[$blockId]['sectionId'])
                );
            }

            if ((int)$v2Blocks[$blockId]['position'] !== (int)$block['position']) {
                $diff['messages'][] = 'Position changed.';
            }

            if (0 !== strcasecmp($v2Blocks[$blockId]['metadataHash'], $block['metadataHash'])) {
                $diff['messages'][] = 'Content changed.';
            }

            if (!empty($diff['messages'])) {
                $changed[] = $diff;
            }
        }

        return $changed;
    }

    /**
     * Compare two version and return difference
     *
     * @param Proposal_Model_ProposalVersion $v1
     * @param Proposal_Model_ProposalVersion | array $v2
     * @return array
     * @example <code>
     *  array(
     *      'regions' => array(
     *          'id' => 'region_id'
     *          'name' => 'region_name'
     *          'msgs' => array()
     *      ),
     *      'blocks' => array(
     *          'id' => 'block_id'
     *          'name' => 'block_name'
     *          'msg' => 'some message explaining the difference'
     *      ),
     *      'total' => 2
     *  )
     * </code>
     * @throws OSDN_Exception
     */
    public function compare(\Proposal_Model_ProposalVersion $v1, $v2)
    {
        $diff = array(
            'total' => 0
        );

        $this->_proposalSectionSrv = new \Proposal_Service_Section();

        $diff['regions'] = $this->_compareHtml($v1, $v2);
        $diff['blocks'] = $this->_compareBlocks($v1, $v2);
        $diff['total'] = count($diff['regions']) + count($diff['blocks']);

        return $diff;
    }

    /**
     * Compare two version by id
     *
     * @param int $versionId1 Compared version ID
     * @param int $versionId2 Version ID to compare with
     * @return array
     * @throws OSDN_Exception
     */
    public function compareById(int $versionId1, int $versionId2)
    {
        try {
            $v1 = $this->find($versionId1);
            $v2 = $this->find($versionId2);
            return $this->compare($v1, $v2);
        } catch (\Exception $e) {
            throw new OSDN_Exception('Error comparing versions.\n' . $e->getMessage());
        }
    }

    protected function _removeDir($dirPath)
    {
        if (!file_exists($dirPath)) {
            return;
        }

        $dir = dir($dirPath);
        while(false !== ($node = $dir->read())) {
            if (($node == '.') || ($node == '..')) {
                continue;
            }

            $filePath = $dirPath . $node;

            if (is_file($filePath)) {
                unlink($filePath);
            } else if (is_dir($filePath)) {
                $this->_removeDir($filePath);
            }
        }

        rmdir($dirPath);
    }

    /**
     * Remove version for a specific proposal which are older then specified date
     *
     * @param Proposal_Model_Proposal $proposal
     * @param string $olderThanDate Date in "Y-m-d" format
     * @return bool
     * @throws OSDN_Exception
     */
    public function clean(\Proposal_Model_Proposal $proposal, $olderThanDate = null)
    {
        try {
            $rowset = $proposal->fetchAllVersions($olderThanDate);
            foreach($rowset as $row) {
                $dirName = $row->dirName;
                $row->delete();
                $this->_removeDir($this->_path . DIRECTORY_SEPARATOR . $dirName);
            }
        } catch(\Exception $e) {
            throw new OSDN_Exception('Unable to clean up version(s).\nError: ' . $e->getMessage());
        }

        return true;
    }

    /**
     * Create proposal version if it has any changes compared to the last version
     *
     * @param Proposal_Model_Proposal $proposalRow
     * @param array $data
     * @example <code>
     * array(
     *   'proposalId'       => 1,
     *   'proposalBlocks'   => serialized_data,
     *   'dirName'       => 'companyId-proposalId-dateCreated-uniqueId',
     *   'html' => array(
     *      array(
     *          'regionId'    => '1',
     *          'html'        => '<p>Some text</p>'
     *          // Hash to be used in comparison
     *          'hash'        => md5('<p>Some text</p>')
     *      )
     *   )
     * );
     * </code>
     * @return Proposal_Model_Proposal
     * @throws OSDN_Exception
     */
    public function create(\Proposal_Model_Proposal $proposalRow, array $data)
    {
        $this->_attachValidationRules('default', array(
            'proposalId'     => array('id', 'allowEmpty' => false, 'presence' => 'required'),
            'proposalBlocks' => array('allowEmpty' => false, 'presence' => 'required'),
            'dirName'        => array('allowEmpty' => false, 'presence' => 'required'),
            'html'           => array('allowEmpty' => false, 'presence' => 'required')
        ));

        $data['proposalId'] = $proposalRow->getId();
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $data['accountId'] = $accountRow->getId();

        $f = $this->_validate($data);

        $this->getAdapter()->beginTransaction();
        try {
            $proposalVersionRow = $this->_table->createRow($f->getData());
            $proposalVersionRow->save();

            foreach ($data['html'] as $region) {
                $region['proposalVersionId'] = $proposalVersionRow->getId();
                $proposalVersionHtmlRow = new \Proposal_Model_DbTable_ProposalVersionHtml();
                $proposalVersionHtmlRow->insert($region);
            }

            $this->getAdapter()->commit();
        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to create proposal', 0, $e);
        }

        return $proposalVersionRow;
    }

    /**
     * @param Proposal_Model_ProposalVersion $proposalVersionRow
     * @return bool
     * @throws OSDN_Exception
     */
    public function delete(\Proposal_Model_ProposalVersion $proposalVersionRow)
    {
        try {
            $proposalVersionRow->delete();
        } catch(\Exception $e) {
            throw new OSDN_Exception('Unable to create proposal');
        }

        return true;
    }

    public function getFiles(\Proposal_Model_ProposalVersion $versionRow)
    {
        $files = array();
        $versionDir = $this->_path . '/' . $versionRow->dirName;

        if (file_exists($versionDir)) {

        }

        return $files;
    }
}
