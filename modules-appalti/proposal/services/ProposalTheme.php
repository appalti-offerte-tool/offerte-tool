<?php

class Proposal_Service_ProposalTheme extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \Proposal_Model_DbTable_ProposalTheme
     */
    protected $_table;

    protected $_companyRow;

    protected $_cssPathRoot;

    public function __construct(\Company_Model_CompanyRow $companyRow)
    {
        $this->_companyRow = $companyRow;
        $this->_table = new \Proposal_Model_DbTable_ProposalTheme($this->getAdapter());
        $this->_attachValidationRules('default', array(
            'companyId' => array('id', 'allowEmpty' => false, 'presence' => 'required'),
        ));
        $this->_attachValidationRules('update', array());

        $this->_cssPathRoot = APPLICATION_PATH . '/modules-appalti/company/data/css-company-theme/' . $companyRow->getId() . '/';
    }

    /**
     * @return \Proposal_Model_ProposalTheme
     */
    public function getDefaultRow()
    {
        $row = $this->_table->fetchRow(array(
            'proposalId IS NULL',
            'companyId = ?' => $this->_companyRow->getId()
        ));

        if (null === $row) {
            $row = $this->create(array());
        }

        return $row;
    }



    /**
     * @param $id
     * @param bool $throwException
     * @throws OSDN_Exception
     * @return \Proposal_Model_ProposalTheme
     */
    public function find($id, $throwException = true)
    {
        $proposalThemeRow = $this->_table->findOne($id);
        if (null === $proposalThemeRow && true === $throwException) {
            throw new \OSDN_Exception('Unable to find proposal theme row: ' . (int) $id);
        }

        return $proposalThemeRow;
    }

    public function createThemeCss(\Proposal_Model_ProposalTheme $proposalTheme)
    {
        if (!file_exists($this->_cssPathRoot)) {
            mkdir($this->_cssPathRoot, 0777, true);
        }
        file_put_contents($this->_cssPathRoot . 'theme.'.$proposalTheme->getId().'.css', $proposalTheme->getPlainCss());
    }

    /**
     * @param array $data
     * @return \Proposal_Model_ProposalTheme
     */
    public function create(array $data)
    {
        $data['companyId'] = $this->_companyRow->getId();
        try {
            $f = $this->_validate($data);
            $proposalThemeRow = $this->_table->createRow($f->getData());
            $proposalThemeRow->save();
            $this->createThemeCss($proposalThemeRow);
            return $proposalThemeRow;
        }  catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to create proposal theme.');
        }
    }

    public function cloneThemeRow(\Proposal_Model_Proposal $proposalRow)
    {
        $defaultThemeRow = $this->getDefaultRow();
        try {
            /**
             * @todo Add checking permissions...
             */
            $themeRow = clone $defaultThemeRow;
            $themeRow->proposalId = $proposalRow->getId();
            $themeRow->save();
            return $themeRow;
        }  catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update');
        }
    }

    public function addThemeRow(\Proposal_Model_Proposal $proposalRow, \Proposal_Model_CustomTemplate $customTemplateRow)
    {
        try {
            $customProposalThemeRow = $customTemplateRow->findDependentRowset('Proposal_Model_DbTable_ProposalTheme', 'CustomTemplate')
                                                        ->current();
            // create new proposaltheme
            // - copy customTheme
            // - set proposaltheme id to zero
            // - set proposal id
            // - create proposaltheme row
            // - save proposaltheme row
            $data = array_merge(
                $customProposalThemeRow->toArray(),
                array(
                    'id'                => 0,
                    'proposalId'        => $proposalRow->getId(),
                )
            );
            $proposalThemeRow = $this->_table->createRow($data);
            $proposalThemeRow->save();
            
            $this->createThemeCss($proposalThemeRow);
            return $proposalThemeRow;
        }  catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to create proposal theme.');
        }

    }

    public function update(\Proposal_Model_ProposalTheme $row, array $data)
    {
        $f = $this->_validate($data, 'update');

        $this->getAdapter()->beginTransaction();
        try {
            $row->setFromArray($f->getData());
            $row->save();

            $proposalRow = $row->getProposalRow();
            if (null !== $proposalRow) {
                $poService = new \Proposal_Service_ProposalOffer();

                if (!$row->includeDiscountPerOffer) {
                    $poService->flushProposalOfferDiscounts($proposalRow);
                }

                if (!empty($data['offerPosition'])) {
                    $poService->setPositions($proposalRow->getId(), $data['offerPosition']);
                }
            }

            $this->createThemeCss($row);
            $this->getAdapter()->commit();
        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update');
        }

        return $row;
    }

    public function deleteByProposalRow(\Proposal_Model_Proposal $proposalRow)
    {
        $this->_table->deleteQuote(array('proposalId = ?' => $proposalRow->getId()));
    }
}