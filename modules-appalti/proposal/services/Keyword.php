<?php

class Proposal_Service_Keyword
{
    protected $_proposalThemeRow;

    protected static $_currCompany;

    protected static $_locationKeywords = array(
        // client location fields
        '{MijnBedrijfsLocatieNaam}' => array(
            'title' => 'Mijn bedrijfslocatie naam',
            'method' => array('_getLocationKwd', 'name')
        ),
        '{MijnBedrijfsLocatieNaamEnBedrijfsvorm}' => array(
            'title' => 'Mijn bedrijfslokatie naam en bedrijfsvorm',
            'method' => array('_getLocationKwd', 'fullname')
        ),
        '{MijnBedrijfsLocatieStraatnaamEnHuisnummer}' => array(
            'title' => 'Mijn bedrijfslocatie straatnaam en huisnummer',
            'method' => array('_getLocationKwd', 'address')
        ),
        '{MijnBedrijfsLocatiePostcode}' => array(
            'title' => 'Mijn bedrijfslocatie postcode',
            'method' => array('_getLocationKwd', 'zipcode')
        ),
        '{MijnBedrijfsLocatieVestigingsplaats}' => array(
            'title' => 'Mijn bedrijfslocatie vestigingsplaats',
            'method' => array('_getLocationKwd', 'city')
        ),
        '{MijnBedrijfsLocatiePostcodeEnVestigingsplaats}' => array(
            'title' => 'Mijn bedrijfslocatie postcode en vestigingsplaats',
            'method' => array('_getLocationKwd', 'zipcode_city')
        ),
        '{MijnBedrijfsLocatiePostbus}' => array(
            'title' => 'Mijn bedrijfslocatie postbus',
            'method' => array('_getLocationKwd', 'postaddress')
        ),
        '{MijnBedrijfsLocatieWebsite}' => array(
            'title' => 'Mijn bedrijfslocatie website',
            'method' => array('_getLocationKwd', 'website')
        ),
        '{MijnBedrijfsLocatieAlgemeneTelefoonnummer}' => array(
            'title' => 'Mijn bedrijfslocatie algemene telefoonnummer',
            'method' => array('_getLocationKwd', 'phone')
        ),
    );

    protected static $_myKeywords = array(
        // client fields
        '{MijnBedrijfsnaam}' => array(
            'title' => 'Mijn bedrijfsnaam',
            'method' => array('_getCompanyKwd', 'name')
        ),
        '{MijnBedrijfsnaamEnBedrijfsvorm}' => array(
            'title' => 'Mijn bedrijfsnaam en bedrijfsvorm',
            'method' => array('_getCompanyKwd', 'fullname')
        ),
        '{MijnStraatnaamEnHuisnummer}' => array(
            'title' => 'Mijn straatnaam en huisnummer',
            'method' => array('_getCompanyKwd', 'address')
        ),
        '{MijnPostcodeEnVestigingsplaats}' => array(
            'title' => 'Mijn postcode en vestigingsplaats',
            'method' => array('_getCompanyKwd', 'zipcode_city')
        ),
        '{MijnPostbus}' => array(
            'title' => 'Mijn postbus',
            'method' => array('_getCompanyKwd', 'postaddress')
        ),
        '{MijnWebsite}' => array(
            'title' => 'Mijn website',
            'method' => array('_getCompanyKwd', 'website')
        ),
        '{MijnAlgemeneTelefoonnummer}' => array(
            'title' => 'Mijn algemene telefoonnummer',
            'method' => array('_getCompanyKwd', 'phone')
        ),

        // client contactperson fields
        '{MijnVoornaam}' => array(
            'title' => 'Mijn voornaam',
            'method' => array('_getContactPersonKwd', 'firstname')
        ),
        '{MijnVoornaamEnAchternaam}' => array(
            'title' => 'Mijn voornaam en achternaam',
            'method' => array('_getContactPersonKwd', 'fullname')
        ),
        '{MijnFunctie}' => array(
            'title' => 'Mijn functie',
            'method' => array('_getContactPersonKwd', 'function')
        ),
        '{MijnDirecteTelefoonnummer}' => array(
            'title' => 'Mijn directe telefoonnummer',
            'method' => array('_getContactPersonKwd', 'phone')
        ),
        '{MijnEmailadres}' => array(
            'title' => 'Mijn emailadres',
            'method' => array('_getContactPersonKwd', 'email')
        ),
        '{MijnGeldigheidsduurVanDeOfferte}' => array(
            'title' => 'Mijn geldigheidsduur van de offerte',
            'method' => array('_getProposalKwd', 'daysValid')
        ),
        '{HandtekeningContactpersoon}' => array(
            'title' => 'Handtekening contactpersoon',
            'method' => array('_getContactPersonKwd', 'signature')
        ),
        '{OfferteDatum}' => array(
            'title' => 'Offerte datum',
            'method' => array('_getProposalKwd', 'createdDatetime')
        ),
        '{InitialenOndertekenaar}' => array(
            'title' => 'Initialen ondertekenaar',
            'method' => array('_getApprovedContactPersonKwd', 'initials')
        ),
        '{AchternaamOndertekenaar}' => array(
            'title' => 'Achternaam ondertekenaar',
            'method' => array('_getApprovedContactPersonKwd', 'lastname')
        ),
        '{HandtekeningOndertekenaar}' => array(
            'title' => 'Handtekening ondertekenaar',
            'method' => array('_getApprovedContactPersonKwd', 'signature')
        ),
        '{FunctieOndertekenaar}' => array(
            'title' => 'Functie ondertekenaar',
            'method' => array('_getApprovedContactPersonKwd', 'function')
        ),
        '{AanhefContactpersoon}' => array(
            'title' => 'Aanhef contactpersoon',
            'method' => array('_getContactPersonKwd', 'salutationFormal')
        ),
        '{AanschrijvenContactpersoon}' => array(
            'title' => 'Aanschrijven contactpersoon',
            'method' => array('_getContactPersonKwd', 'summon')
        ),
        '{mijnVestigingsplaats}' => array(
            'title' => 'Mijn vestigingsplaats',
            'method' => array('_getCompanyKwd', 'actualCity')
        ),
        '{mijnLogo}' => array(
            'title' => 'Mijn Logo',
            'method' => array('_getCompanyKwd', 'logo')
        ),
        '{offerteNummer}' => array(
            'title' => 'Offerte nummer',
            'method' => array('_getProposalKwd', 'luid')
        ),
    );

    protected static $_clientKeywords = array(
        '{BedrijfsnaamRelatie}' => array(
            'title' => 'Bedrijfsnaam relatie',
            'method' => array('_getClientKwd', 'name')
        ),
        '{BedrijfsnaamEnBedrijfsvormRelatie}' => array(
            'title' => 'Bedrijfsnaam en bedrijfsvorm relatie',
            'method' => array('_getClientKwd', 'fullname')
        ),
        '{StraatnaamEnHuisnummerRelatie}' => array(
            'title' => 'Straatnaam en huisnummer relatie',
            'method' => array('_getClientKwd', 'address')
        ),
        '{PostcodeEnVestigingsplaatsRelatie}' => array(
            'title' => 'Postcode en vestigingsplaats relatie',
            'method' => array('_getClientKwd', 'zipcode_city')
        ),
        '{PostbusRelatie}' => array(
            'title' => 'Postbus relatie',
            'method' => array('_getClientKwd', 'postaddress')
        ),
        '{WebsiteRelatie}' => array(
            'title' => 'Website relatie',
            'method' => array('_getClientKwd', 'website')
        ),
        '{TelefoonnummerRelatie}' => array(
            'title' => 'Telefoonnummer relatie',
            'method' => array('_getClientKwd', 'phone')
        ),
        '{VoornaamVanRelatie}' => array(
            'title' => 'Voornaam van relatie',
            'method' => array('_getClientContactPersonKwd', 'firstname')
        ),
        '{VoornaamEnAchternaamVanRelatie}' => array(
            'title' => 'Voornaam en achternaam van relatie',
            'method' => array('_getClientContactPersonKwd', 'fullname')
        ),
        '{FunctieRelatie}' => array(
            'title' => 'Functie relatie',
            'method' => array('_getClientContactPersonKwd', 'function')
        ),
        '{DirecteTelefoonnummerRelatie}' => array(
            'title' => 'Directe telefoonnummer relatie',
            'method' => array('_getClientContactPersonKwd', 'phone')
        ),
        '{AanhefContactpersoonRelatieFormeel}' => array(
            'title' => 'Aanhef contactpersoon relatie formeel',
            'method' => array('_getClientContactPersonKwd', 'salutationFormal')
        ),
        '{AanhefContactpersoonRelatieInformeel}' => array(
            'title' => 'Aanhef contactpersoon relatie informeel',
            'method' => array('_getClientContactPersonKwd', 'salutationInformal')
        ),
        '{AanschrijvenContactpersoonRelatie}' => array(
            'title' => 'Aanschrijven contactpersoon relatie',
            'method' => array('_getClientContactPersonKwd', 'summon')
        ),
        '{ContactpersoonEmail}' => array(
            'title' => 'Emailadres contactpersoon',
            'method' => array('_getClientContactPersonKwd', 'email')
        ),
        '{LogoRelatie}' => array(
            'title' => 'Logo relatie',
            'method' => array('_getClientKwd', 'logo')
        ),

        '{VestigingsplaatsRelatie}' => array(
            'title' => 'Vestigingsplaats relatie',
            'method' => array('_getClientKwd', 'actualCity')
        ),
    );

    protected static $_keywordsInactive = array(
        'Mijn gegevens' => array(
            '{Bedrijfsnaam}' => array(
                'title' => 'Bedrijfsnaam',
                'method' => array('_getCompanyKwd', 'name')
            ),
            '{Bedrijfsvorm}' => array(
                'title' => 'Bedrijfsvorm',
                'method' => array('_getCompanyKwd', 'form')
            ),
            '{BedrijfsnaamBedrijfsvorm}' => array(
                'title' => 'BedrijfsnaamBedrijfsvorm',
                'method' => array('_getCompanyKwd', 'fullname')
            ),
            '{StraatnaamHuisnummer}' => array(
                'title' => 'Straatnaam Huisnummer',
                'method' => array('_getCompanyKwd', 'address')
            ),
            '{Postcode}' => array(
                'title' => 'Postcode',
                'method' => array('_getCompanyKwd', 'zipcode')
            ),
            '{PostcodePlaats}' => array(
                'title' => 'Postcode Plaats',
                'method' => array('_getCompanyKwd', 'zipcode_city')
            ),
            '{MailStraatnaamHuisnummer}' => array(
                'title' => 'Postbus',
                'method' => array('_getCompanyKwd', 'postaddress')
            ),
            '{MailPostcodePlaatslaats}' => array(
                'title' => 'Postcode',
                'method' => array('_getCompanyKwd', 'post_zipcode_city')
            ),
            '{AlgemeenTelefoonnummer}' => array(
                'title' => 'Algemeen telefoonnummer',
                'method' => array('_getCompanyKwd', 'phone')
            ),
            '{Fax}' => array(
                'title' => 'Fax',
                'method' => array('_getCompanyKwd', 'fax')
            ),
            '{Email}' => array(
                'title' => 'Email',
                'method' => array('_getCompanyKwd', 'email')
            ),
            '{Website}' => array(
                'title' => 'Website',
                'method' => array('_getCompanyKwd', 'website')
            ),
            '{BankAccount}' => array(
                'title' => 'Bank account',
                'method' => array('_getCompanyKwd', 'bank_account')
            ),
            '{KvK}' => array(
                'title' => 'KvK',
                'method' => array('_getCompanyKwd', 'kvk')
            ),
            '{BTW}' => array(
                'title' => 'BTW',
                'method' => array('_getCompanyKwd', 'btw')
            ),
            '{Plaats}' => array(
                'title' => 'Plaats',
                'method' => array('_getCompanyKwd', 'city')
            ),

        ),

        'Contactpersoon offerte' => array(
            '{ContactpersoonAanhef}' => array(
                'title' => 'Aanhef',
                'method' => array('_getClientContactPersonKwd', 'salutation')
            ),
            '{ContactpersoonAanschrijven}' => array(
                'title' => 'Aanschrijven',
                'method' => array('_getClientContactPersonKwd', 'summon')
            ),
            '{ContactpersoonAchternaam}' => array(
                'title' => 'Achternaam',
                'method' => array('_getContactPersonKwd', 'lastname')
            ),
            '{ContactpersoonIntialen}' => array(
                'title' => 'Intialen',
                'method' => array('_getContactPersonKwd', 'initials')
            ),
            '{ContactpersoonTitel}' => array(
                'title' => 'Titel', //Contact person title
                'method' => array('_getContactPersonKwd', 'title')
            ),
//            '{ContactpersoonVoorvoegsel}' => array(
//                'title' => 'Voorvoegsel',
//                'method' => array('_getContactPersonKwd', 'prefix')
//            ),
            '{ContactpersoonVoornaam}' => array(
                'title' => 'Voornaam',
                'method' => array('_getContactPersonKwd', 'firstname')
            ),
            '{ContactpersoonFunctie}' => array(
                'title' => 'Functie',
                'method' => array('_getContactPersonKwd', 'function')
            ),
            '{ContactpersoonTelefoon}' => array(
                'title' => 'Telefoon',
                'method' => array('_getContactPersonKwd', 'phone')
            ),
            '{ContactpersoonMobiel}' => array(
                'title' => 'Mobiel',
                'method' => array('_getContactPersonKwd', 'mobile')
            ),
            '{ContactpersoonEmail}' => array(
                'title' => 'Emailadres contactpersoon',
                'method' => array('_getContactPersonKwd', 'email')
            ),
            '{ContactpersoonFoto}' => array(
                'title' => 'Contactpersoon foto',
                'method' => array('_getContactPersonKwd', 'photo')
            ),
//            '{ContactpersoonHandtekening}' => array(
//                'title' => 'Contactpersoon handtekening',
//                'method' => array('_getContactPersonKwd', 'signature')
//            ),
        ),

        'Ondertekenaar offerte' => array(
            '{OndertekenaarAanhef}' => array(
                'title' => 'Aanhef',
                'method' => array('_getApprovedContactPersonKwd', 'salutation')
            ),
            '{OndertekenaarAanschrijven}' => array(
                'title' => 'Aanschrijven',
                'method' => array('_getApprovedContactPersonKwd', 'summon')
            ),
            '{OndertekenaarAchternaam}' => array(
                'title' => 'Achternaam',
                'method' => array('_getApprovedContactPersonKwd', 'lastname')
            ),
            '{OndertekenaarIntialen}' => array(
                'title' => 'Intialen',
                'method' => array('_getApprovedContactPersonKwd', 'initials')
            ),
            '{OndertekenaarTitel}' => array(
                'title' => 'Titel', //Contact person title
                'method' => array('_getApprovedContactPersonKwd', 'title')
            ),
//            '{OndertekenaarVoorvoegsel}' => array(
//                'title' => 'Voorvoegsel',
//                'method' => array('_getApprovedContactPersonKwd', 'prefix')
//            ),
            '{OndertekenaarVoornaam}' => array(
                'title' => 'Voornaam',
                'method' => array('_getApprovedContactPersonKwd', 'firstname')
            ),
            '{OndertekenaarFunctie}' => array(
                'title' => 'Functie',
                'method' => array('_getApprovedContactPersonKwd', 'function')
            ),
            '{OndertekenaarTelefoon}' => array(
                'title' => 'Telefoon',
                'method' => array('_getApprovedContactPersonKwd', 'phone')
            ),
            '{OndertekenaarMobiel}' => array(
                'title' => 'Mobiel',
                'method' => array('_getApprovedContactPersonKwd', 'mobile')
            ),
            '{OndertekenaarEmail}' => array(
                'title' => 'Email',
                'method' => array('_getApprovedContactPersonKwd', 'email')
            ),
            '{OndertekenaarFoto}' => array(
                'title' => 'Contactpersoon foto',
                'method' => array('_getApprovedContactPersonKwd', 'photo')
            ),
            '{OndertekenaarHandtekening}' => array(
                'title' => 'Contactpersoon handtekening',
                'method' => array('_getApprovedContactPersonKwd', 'signature')
            ),
        ),

        'Klantgegevens' => array(
            '{KlantBedrijfsnaam}' => array(
                'title' => 'Bedrijfsnaam', //Company name
                'method' => array('_getClientKwd', 'name')
            ),
            '{KlantBedrijfsvorm}' => array(
                'title' => 'Bedrijfsvorm', //Company form
                'method' => array('_getClientKwd', 'form')
            ),
            '{KlantBedrijfsnaamBedrijfsvorm}' => array(
                'title' => 'BedrijfsnaamBedrijfsvorm', //Company fullname
                'method' => array('_getClientKwd', 'fullname')
            ),
            '{KlantStraatnaamHuisnummer}' => array(
                'title' => 'Straatnaam Huisnummer', //Company address
                'method' => array('_getClientKwd', 'address')
            ),
            '{KlantPostcode}' => array(
                'title' => 'Postcode',
                'method' => array('_getClientKwd', 'zipcode')
            ),
            '{KlantPostcodePlaats}' => array(
                'title' => 'Postcode Plaats',
                'method' => array('_getCompanyKwd', 'zipcode_city')
            ),
            '{KlantMailStraatnaamHuisnummer}' => array(
                'title' => 'Postbus', //Company post address
                'method' => array('_getClientKwd', 'postaddress')
            ),
            '{KlantMailPostcodePlaatslaats}' => array(
                'title' => 'Postcode',
                'method' => array('_getClientKwd', 'post_zipcode_city')
            ),
            '{KlantAlgemeenTelefoonnummer}' => array(
                'title' => 'Algemeen telefoonnummer',
                'method' => array('_getClientKwd', 'phone')
            ),
            '{KlantFax}' => array(
                'title' => 'Fax',
                'method' => array('_getClientKwd', 'fax')
            ),
            '{KlantEmail}' => array(
                'title' => 'Email',
                'method' => array('_getClientKwd', 'email')
            ),
            '{KlantWebsite}' => array(
                'title' => 'Website',
                'method' => array('_getClientKwd', 'website')
            ),
            '{KlantBankAccount}' => array(
                'title' => 'Bank account',
                'method' => array('_getClientKwd', 'bank_account')
            ),
            '{KlantKvK}' => array(
                'title' => 'KvK',
                'method' => array('_getClientKwd', 'kvk')
            ),
            '{KlantBTW}' => array(
                'title' => 'BTW',
                'method' => array('_getClientKwd', 'btw')
            ),
            '{KlantPlaats}' => array(
                'title' => 'Plaats',
                'method' => array('_getClientKwd', 'city')
            ),

        ),

        'Contactpersoon relatie' => array(
            '{KlantContactpersoonAanhef}' => array(
                'title' => 'Aanhef',
                'method' => array('_getClientContactPersonKwd', 'salutation')
            ),
            '{KlantContactpersoonAanschrijven}' => array(
                'title' => 'Aanschrijven',
                'method' => array('_getClientContactPersonKwd', 'summon')
            ),
            '{KlantContactpersoonAchternaam}' => array(
                'title' => 'Achternaam', //Contact person lastname
                'method' => array('_getClientContactPersonKwd', 'lastname')
            ),
            '{KlantContactpersoonIntialen}' => array(
                'title' => 'Intialen', //Contact person initials
                'method' => array('_getClientContactPersonKwd', 'initials')
            ),
            '{KlantContactpersoonTitel}' => array(
                'title' => 'Titel', //Contact person title
                'method' => array('_getClientContactPersonKwd', 'title')
            ),
//            '{KlantContactpersoonVoorvoegsel}' => array(
//                'title' => 'Voorvoegsel',
//                'method' => array('_getClientContactPersonKwd', 'prefix')
//            ),
            '{KlantContactpersoonVoornaam}' => array(
                'title' => 'Voornaam',
                'method' => array('_getClientContactPersonKwd', 'firstname')
            ),
            '{KlantContactpersoonFunctie}' => array(
                'title' => 'Functie',
                'method' => array('_getClientContactPersonKwd', 'function')
            ),
            '{KlantContactpersoonTelefoon}' => array(
                'title' => 'Telefoon',
                'method' => array('_getClientContactPersonKwd', 'phone')
            ),
            '{KlantContactpersoonMobiel}' => array(
                'title' => 'Mobiel',
                'method' => array('_getClientContactPersonKwd', 'mobile')
            ),
            '{KlantContactpersoonEmail}' => array(
                'title' => 'Email',
                'method' => array('_getClientContactPersonKwd', 'email')
            ),
            '{KlantContactpersoonFoto}' => array(
                'title' => 'Contactpersoon foto',
                'method' => array('_getClientContactPersonKwd', 'photo')
            )
        ),

        'Offerte' => array(
            '{OfferteDatum}' => array(
                'title' => 'Offerte datum', //date of proposal
                'method' => array('_getProposalKwd', 'createdDatetime')
            ),
            '{DagenGeldig}' => array(
                'title' => 'Dagen geldig', //Duration of proposal
                'method' => array('_getProposalKwd', 'daysValid')
            ),
            '{Bedrag}' => array(
                'title' => 'Bedrag', //Client logo
                'method' => array('_getProposalKwd', 'amount')
            ),
            '{Kans}' => array(
                'title' => 'Kans', //Client logo
                'method' => array('_getProposalKwd', 'chanceOfSuccess')
            ),
            '{ContactMoment}' => array(
                'title' => 'Contact moment', //proposal contact moment
                'method' => array('_getProposalKwd', 'contactMomentDate')
            ),
        ),
    );

    protected $_accountId;

    public function  __construct(\Proposal_Model_Proposal $proposalRow)
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if ($accountRow->isAdmin()) {
        } else {
            $this->_accountId = $accountRow->getId();
        }
        $this->_proposalRow = $proposalRow;
        $this->_proposalThemeRow = $proposalRow->getThemeRow();
    }
//
//    public function addKeyword($afterKey, array $keyword)
//    {
//        $this->addKeywordSet($afterKey, array($keyword));
//    }

    /**
     * insert an array of keywords
     *
     * @param string $afterKey name of key whereafter the new keywords should be inserted
     * @param array array of keywords
     */
    public static function addMyKeywordSet($afterKey, array $keywords)
    {
        if (!trim($afterKey) || !isset(self::$_myKeywords))
        {
            throw new Exception('The first argument should be a valid key.');
        }
        $keys = array_flip(array_keys(self::$_myKeywords));
        $offset = $keys[ $afterKey ]+1;
        self::$_myKeywords = array_merge(
            array_slice(self::$_myKeywords, 0, $offset, true),
            $keywords,
            array_slice(self::$_myKeywords, $offset, null, true)
        );
    }

    public static function fetchMyKeywords()
    {
        $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow(false);
        if($companyRow && count($companyRow->getLocations()) > 1)
        {
            self::addMyKeywordSet('{MijnAlgemeneTelefoonnummer}', self::$_locationKeywords);
        }
        $result = array(
            'Mijn bedrijfsgegevens' => array()
        );

        foreach(self::$_myKeywords as $k => $v) {
            $result['Mijn bedrijfsgegevens'][$k] = $v['title'];
        }

        return $result;
    }

    public static function fetchClientKeywords()
    {
        $result = array(
            'Mijn relatiegegevens' => array()
        );

        foreach(self::$_clientKeywords as $k => $v) {
            $result['Mijn relatiegegevens'][$k] = $v['title'];
        }
        return $result;
    }

    public static function fetchAll()
    {
        $result = array();

        foreach(self::$_keywordsInactive as $k => $v) {
            $result[$k] = array();
            foreach($v as $k1 => $v1) {
                $result[$k][$k1] = $v1['title'];
            }
        }
        return $result;
    }

    public function fetchOnlyKeywords()
    {
        return array_keys(self::_keywords);
    }

    public function replace($html)
    {
        $search = array();
        $replace = array();
//        $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow(false);
        $companyRow = $this->_getCurrCompany();
        if(count($companyRow->getLocations()) > 1)
        {
            self::addMyKeywordSet('{MijnAlgemeneTelefoonnummer}', self::$_locationKeywords);
        }
        foreach(self::$_myKeywords as $k => $v) {
            if (preg_match($k, $html)) {
                $search[] = $k;
                $fun = $v['method'][0];
                $par = $v['method'][1];
                $replace[] = $this->{$fun}($par);
            }

        }

        foreach(self::$_clientKeywords as $k => $v) {
            if (preg_match($k, $html)) {
                $search[] = $k;
                $fun = $v['method'][0];
                $par = $v['method'][1];
                $replace[] = $this->{$fun}($par);
            }
        }

        /* parse old keywords for backward compatibility */
        foreach(self::$_keywordsInactive as $k => $v) {
            foreach($v as $k1 => $v1) {
                if (preg_match($k1, $html)) {
                    $search[] = $k1;
                    $fun = $v1['method'][0];
                    $par = $v1['method'][1];
                    $replace[] = $this->{$fun}($par);
                }
            }
        }
        $html = str_replace($search, $replace, $html);
        return $html;
    }

    protected function _getCurrCompany()
    {
        if (isset($this->_currCompany)) {
            return $this->_currCompany;
        }

        $companySrv = new \Company_Service_Company();
        return self::$_currCompany = $companySrv->find($this->_proposalThemeRow->companyId, false);
    }

    protected function _getClientContactPerson()
    {
        if (isset($this->_clientContactPerson)) {
            return $this->_clientContactPerson;
        }

        return $this->_clientContactPerson = $this->_proposalRow->getClientContactPersonRow();
    }

    protected function _getContactPerson()
    {
        if (isset($this->_contactPerson)) {
            return $this->_contactPerson;
        }

        return $this->_contactPerson = $this->_proposalRow->getContactPersonRow();
    }

    protected function _getApprovedContactPerson()
    {
        if (isset($this->_approvedContactPerson)) {
            return $this->_approvedContactPerson;
        }

        return $this->_approvedContactPerson = $this->_proposalRow->getApprovedContactPersonRow();
    }

    /**
     * @return \Proposal_Model_Proposal
     */
    protected function _getCurrProposal()
    {
        if (isset($this->_currProposal)) {
            return $this->_currProposal;
        }
        $this->_currProposal = $this->_proposalThemeRow->getProposalRow();
        return $this->_currProposal;
    }

    /**
     * @return Company_Model_CompanyRow
     */
    protected function _getCurrClient()
    {
        if (isset($this->_currClient)) {
            return $this->_currClient;
        }
        return $this->_currClient = $this->_getCurrProposal()->getClientCompanyRow();
    }

    protected function _getCompanyFieldVal(\Company_Model_CompanyRow $company, $fieldname)
    {
        switch ($fieldname) {
            case 'address':
                return $company->actualStreet . ' ' . $company->actualHouseNumber;
            case 'postaddress':
                return $company->getPostAddress();
            case 'zipcode_city':
                return $company->actualPostCode . ' ' . $company->actualCity;
            case 'zipcode':
                return $company->actualPostCode;
            case 'post_zipcode_city':
                return $company->postPostCode . ' ' . $company->postCity;
            case 'email':
                return $company->email;
            case 'website':
                return $company->website;
            case 'phone':
                return $company->phone;
            case 'fax':
                return $company->fax;
            case 'kvk':
                return $company->kvk;
            case 'btw':
                return $company->btw;
            case 'city':
                return $company->actualCity;
            case 'fullname':
                return $company->getFullName();
            case 'bank_account':
                $bankAccountRow = $company->getCompanyDefaultBankAccountRow();
                if (!empty($bankAccountRow)) {
                    return $bankAccountRow->bankAccount;
                } else {
                    return '';
                }
            case 'logo':
                if (!empty($company->logo)) {
                    $clfs = new Company_Service_CompanyLogoFileStock($company);
                    return $this->_getBase64ImageHtml(APPLICATION_PATH . $clfs->toImageFileStockRelativePath() . $company->logo);

                } else {
                    return '';
                }
            default:
                return isset($company->{$fieldname}) ? $company->{$fieldname} : '';
        }
    }

    protected function _getContactPersonFieldVal(\Company_Model_ContactPersonRow $cp, $fieldname)
    {
        switch ($fieldname) {
            case 'photo':
            case 'signature':
                if (!empty($cp->$fieldname)) {
                    $cpfs = new Company_Service_ContactPersonPhotoFileStock($cp, $fieldname);
                    return $this->_getBase64ImageHtml(APPLICATION_PATH . $cpfs->toImageFileStockRelativePath() . $cp->{$fieldname});
                } else {
                    return '';
                }

            case 'salutationFormal':
                return 'Geachte ' . ($cp->sex == 'man' ? 'heer ' : 'mevrouw ') . $cp->lastname;
            case 'salutationInformal':
                return 'Beste ' . $cp->firstname;
            case 'fullname':
                return $cp->getFullname();
            case 'summon':
                if ($cp->sex == 'man') {
                    return 'De heer ' . $cp->getFullNameWithInitials();
                } else {
                    return  'Mevrouw ' . $cp->getFullNameWithInitials();
                }
            default:
                return isset($cp->{$fieldname}) ? $cp->{$fieldname} : '';
        }
    }

    protected function _getContactPersonKwd($fieldname)
    {
        $cp = $this->_getContactPerson();

        if (null === $cp) {
            return '[Contact person not found]';
        }

        return $this->_getContactPersonFieldVal($cp, $fieldname);
    }

    protected function _getClientContactPersonKwd($fieldname)
    {
        $cp = $this->_getClientContactPerson();

        if (null === $cp) {
            return '[Client contact person not found]';
        }

        return $this->_getContactPersonFieldVal($cp, $fieldname);
    }

    protected function _getApprovedContactPersonKwd($fieldname)
    {
        $cp = $this->_getApprovedContactPerson();

        if (null === $cp) {
            return '[Approved contact person not found]';
        }

        return $this->_getContactPersonFieldVal($cp, $fieldname);
    }

    protected function _getProposalKwd($fieldname)
    {
        $cp = $this->_getCurrProposal();

        if (null === $cp) {
            return '[Proposal not found]';
        }

        switch ($fieldname) {
            case 'createdDatetime':
                return date('d-m-Y', strtotime(null !== $cp->modifiedDatetime ? $cp->modifiedDatetime : $cp->createdDatetime));
            case 'contactMomentDate':
                return date('d-m-Y', strtotime($cp->$fieldname));
            case 'chanceOfSuccess':
                return $cp->chanceOfSuccess . '%';
            case 'daysValid':
                return $this->_proposalThemeRow->$fieldname . ' dagen';
            default:
                return isset($cp->{$fieldname}) ? $cp->{$fieldname} : '';
        }
    }

    protected function _getCompanyKwd($fieldname)
    {
        $company = $this->_getCurrCompany();
        if (null === $company) {
            return '[Company not found]';
        }
        return $this->_getCompanyFieldVal($company, $fieldname);
    }

    protected function _getLocationKwd($fieldname)
    {
        $_locations = new \Company_Service_Location();
        $locationRow = $_locations->find($this->_proposalThemeRow->getProposalRow()->locationId, false);
        if (null === $locationRow) {
            return '[Location not found]';
        }
        return $this->_getCompanyFieldVal($locationRow, $fieldname);
    }

    protected function _getClientKwd($fieldname)
    {
        $company = $this->_getCurrClient();

        if (null === $company) {
            return '[Client company not found]';
        }

        return $this->_getCompanyFieldVal($company, $fieldname);
    }

    protected function _getBase64ImageHtml($file)
    {
        $p = explode('?', $file);
        $file = $p[0];
        if (!file_exists($file)) {
            return '';
        }
        if($fp = fopen($file,"rb", 0)) {
            $picture = fread($fp,filesize($file));
            fclose($fp);
            // base64 encode the binary data, then break it
            // into chunks according to RFC 2045 semantics
            $base64 = chunk_split(base64_encode($picture));
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $ext = 'jpg' === $ext ? 'jpeg' : $ext;
            return sprintf('<img src="data:image/%s;base64,%s" />', $ext, $base64);
        }
        echo '';
    }
}
