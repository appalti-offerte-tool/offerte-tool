<?php

/**
 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
 * @version     SVN: $Id: Template.php 1045 2012-12-13 15:07:14Z vanya $
 * @changedby   $Author: vanya $
 */

class Proposal_Service_Template
{
    protected $_path = '/data/public-templates/';
    protected $_fontsDir = '/data/public-templates/common/fonts/';
    protected $_fontsCssDir = '/data/public-templates/common/css/fonts/';

    protected $_thumbName = 'thumb.png';
    protected $_previewThumbName = 'preview.png';

    /**
     * @var Proposal_Service_Section
     */
    protected $_section;

    protected function _pathToRelative($path)
    {

    }

    public function __construct()
    {
        $this->_section = new \Proposal_Service_Section();
    }

    /**
     * @param Proposal_Model_ProposalTheme $themeRow
     * @return array
     */
    public function fetchByTheme(Proposal_Model_ProposalTheme $themeRow)
    {
        $result = array();
        $sections = $this->_section->fetchAll();
        foreach ($sections as $sectionRow) {
            $luid = $sectionRow->getLuid();
            $themeName = 'template' . ucfirst($luid);

            if (empty($themeRow->$themeName)) {
                continue;
            }

            $themeDir = $this->_path . $themeRow->$themeName;
            $thumb = $themeDir . '/' .$sectionRow->getLuid() . '/' .$this->_thumbName;
            $thumbPreview = $themeDir . '/' .$sectionRow->getLuid() . '/' .$this->_previewThumbName;

            $thumbExists = file_exists(APPLICATION_PATH . $thumb);
            $thumbPreviewExists = file_exists(APPLICATION_PATH . $thumbPreview);

            $result[$sectionRow->getName()] = array(
                'section'   => $sectionRow,
                'thumb'     => $thumbExists ? $thumb : null,
                'preview'   => $thumbPreviewExists ? $thumbPreview : null
            );
        }

        return $result;
    }

    public function fetchAll()
    {
        $result = array();
        $sections = $this->_section->fetchAll();
        foreach (new \DirectoryIterator(APPLICATION_PATH . $this->_path) as $iterator) {
            if ($iterator->isDot() || $iterator->getFilename() === '.svn') {
                continue;
            }

            foreach ($sections as $sectionRow) {
                if ($sectionRow->isCalculatable()) {
                    continue;
                }

                $relativePath = '/' . $sectionRow->getLuid() . '/' . $this->_thumbName;
                $realPath = $iterator->getPathname() . $relativePath;

                if (file_exists($realPath)) {

                    if (empty($result[$sectionRow->getName()])) {
                        $result[$sectionRow->getName()] = array(
                            'section' => $sectionRow,
                            'thumbs' => array(),
                            'previewThumbs' => array()
                        );
                    }

                    $imgPath = $this->_path . $iterator->getFilename() . $relativePath;
                    $result[$sectionRow->getName()]['thumbs'][$iterator->getFilename()] = $imgPath;

                    $relativePath = '/' . $sectionRow->getLuid() . '/' . $this->_previewThumbName;
                    $realPath = $iterator->getPathname() . $relativePath;

                    if (file_exists($realPath)) {
                        $imgPath = $this->_path . $iterator->getFilename() . $relativePath;
                        $result[$sectionRow->getName()]['previewThumbs'][$iterator->getFilename()] = $imgPath;
                    }

                }
            }
        }

        return $result;
    }

    /**
     * @param Company_Model_CompanyRow $company
     * @param $section
     * @return array
     */
    protected function _customTplCss(\Company_Model_CompanyRow $company, $section)
    {
        return array();
    }

    public function getCssStyles(\Proposal_Model_Section $sectionRow, \Proposal_Model_ProposalTheme $themeRow)
    {
        $tpl = $themeRow->getTemplateThemeBySection($sectionRow);
        $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow(false);
        $luid = $sectionRow->getLuid();

        $css = array(
            '/data/public-templates/common/css/fonts/' . strtolower($themeRow->lettertype) . '.css',
            '/data/public-templates/common/css/reset.css'
        );
        $cssFile = sprintf('/data/public-templates/%s/%s/css/%s.css', $tpl, $luid, $luid);
        if (!file_exists(APPLICATION_PATH . $cssFile) || $sectionRow->useContentCss()) {
            $cssFile = sprintf('/data/public-templates/%s/css/content.css', $tpl);
        }
        $css[] = $cssFile;


        if (null !== $companyRow) {
            $themeFile = '/modules-appalti/company/data/css-company-theme/' . $companyRow->getId() . '/theme.'.$themeRow->getId().'.css';

            if (!file_exists(APPLICATION_PATH . $themeFile)) {
                $proposalThemeSrv = new \Proposal_Service_ProposalTheme($companyRow);
                $proposalThemeSrv->createThemeCss($themeRow);
            }

            $css[] = $themeFile;
        }

        return join(', ', $css);
    }

    public function fontScanningRequired()
    {
        return !file_exists(APPLICATION_PATH . $this->_fontsCssDir . 'all.css');
    }

    protected function _ttf2eot($fileName)
    {
        $filePath = APPLICATION_PATH . $this->_fontsDir . $fileName;

        if (!file_exists($filePath . 'eot')) {
            $exe = sprintf('ttf2eot%s', false !== stripos(PHP_OS, 'win') ? '.exe' : '');
            $cmd = sprintf('%s/library/ttf2eot/%s < %s.ttf > %s.eot', APPLICATION_PATH, $exe, $filePath, $filePath);
            try {
                exec(escapeshellcmd($cmd));
            } catch (\Exception $e) {
                throw new \Exception('Error creating eot from ttf. Message: ' . $e->getMessage());
            }
        }
    }

    protected function _createCssFontFile($fontFamily, $forceUpdate = false)
    {
        $cssFileName = APPLICATION_PATH . $this->_fontsCssDir . $fontFamily . '.css';

        if ($forceUpdate || !file_exists($cssFileName)) {

            if ($forceUpdate && file_exists($cssFileName)) {
                unlink($cssFileName);
            }

            $protocol = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https://' : 'http://';
            $url = $protocol . $_SERVER['SERVER_NAME'] . $this->_fontsDir;

            $this->_ttf2eot($fontFamily);

            $cssParams = array(
                'fontFamily' => $fontFamily,
                'normal' => array(
                    'fileName' => $url . $fontFamily,
                ),
                'bold' => array(),
                'italic' => array(),
                'boldItalic' => array(),
            );

            $fontStyles = array(
                'bold'          => '-bold',
                'italic'        => '-italic',
                'boldItalic'    => '-bold-italic'
            );

            $path = APPLICATION_PATH . $this->_fontsDir;
            foreach ($fontStyles as $k => $fontStyle) {
                $fName = $fontFamily . $fontStyle;

                if (file_exists($path . $fName . '.ttf')) {
                    $cssParams[$k] = array(
                        'fileName' => $url . $fName
                    );

                    $this->_ttf2eot($fName);
                }
            }

            ob_start();
            require APPLICATION_PATH . $this->_path. 'common/template.phtml';
            $cssContent = ob_get_clean();

            if (!file_put_contents($cssFileName, $cssContent)) {
                throw new \Exception('Error creating css font file.');
            }

            chmod($cssFileName, 0777);
        }
    }

    protected function _createAllCssFontFile()
    {
        $cssFileName = APPLICATION_PATH . $this->_fontsCssDir . 'all.css';
        $xmlFileName = APPLICATION_PATH . $this->_fontsDir . 'all.xml';

        if (file_exists($cssFileName)) {
            unlink($cssFileName);
        }

        if (file_exists($xmlFileName)) {
            unlink($xmlFileName);
        }

        $path = APPLICATION_PATH . $this->_fontsCssDir;
        $content = '';
        $xml = new \SimpleXMLElement('<root />');

        foreach (new \DirectoryIterator($path) as $f) {
            if (0 !== strcasecmp(pathinfo($f->getFileName(), PATHINFO_EXTENSION), 'css')) {
                continue;
            }

            $content .= file_get_contents($f->getPathname());

            $value = pathinfo($f->getFileName(), PATHINFO_FILENAME);
            $title = ucwords(str_replace('-', ' ', $value));
            $item = $xml->addChild('font');
            $item->addAttribute('title', $title);
            $item->addAttribute('value', $value);
        }

        if (!file_put_contents($cssFileName, $content)) {
            throw new \Exception('Error creating all fonts css file.');
        }
        chmod($cssFileName, 0777);

        if (!$xml->saveXML($xmlFileName)) {
            throw new \Exception('Error creating all fonts xml file.');
        }
        chmod($xmlFileName, 0777);
    }

    public function scanFonts($forceUpdate = false)
    {
        $fontFamilies = array();
        $currentFontFamily = '';

        foreach (new \DirectoryIterator(APPLICATION_PATH . $this->_fontsDir) as $f) {
            if (0 !== strcasecmp(pathinfo($f->getFileName(), PATHINFO_EXTENSION), 'ttf')) {
                continue;
            }

            $fName = pathinfo($f->getFileName(), PATHINFO_FILENAME);
            $fontFamily = preg_replace('/(-bold|-italic|-bold-italic)/i', '', $fName);

            if ($fontFamily !== $currentFontFamily && empty($fontFamilies[$fontFamily])) {
                $currentFontFamily = $fontFamily;
                $fontFamilies[$fontFamily] = $fontFamily;
                $this->_createCssFontFile($fontFamily, $forceUpdate);
            }
        }

        $this->_createAllCssFontFile();
    }

    protected function _sanitizeFileName($fileName)
    {
        $fileName = preg_replace('/[^\w\d\s\-]/i', '', $fileName);
        $fileName = preg_replace('/[\-|_]/i', ' ', $fileName);
        $fileName = preg_replace('/\s+/i', '-', $fileName);

        return $fileName;
    }

    public function addFont($fontName)
    {
        $transfer = new Zend_File_Transfer_Adapter_Http();
        $fontName = strtolower($fontName);
        $filesCount = 0;

        foreach ($transfer->getFileInfo() as $style => $value) {
            if ($transfer->isUploaded($style)) {
                $transfer->addValidator('MimeType', false, 'application/x-font-ttf', $style);
                if (!$transfer->isValid($style)) {
                    $e = new OSDN_Exception('Invalid font file type. Only TrueType fonts (ttf) are allowed.');
                    $e->assign($transfer->getMessages());
                    throw $e;
                }

                $fontStyle = 0 === strcasecmp($style, 'regular') ? '' : '-' . strtolower($style);
                $fileName = $this->_sanitizeFileName($fontName) . $fontStyle . '.ttf';
                $path = APPLICATION_PATH . $this->_fontsDir;
                $transfer->addFilter('Rename', array(
                    'target'    => $path . $fileName,
                    'overwrite' => true
                ), $style);

                $transfer->receive($style);
                $filesCount++;
            }
        }

        if (!$filesCount) {
            throw new \Exception('Font files are not supplied.');
        }

        $this->scanFonts();
    }

    public function deleteFont($fontName)
    {
        $possibleFileNames = array(
            $fontName,
            $fontName . '-bold',
            $fontName . '-italic',
            $fontName . '-bold-italic'
        );

        $fontsPath = APPLICATION_PATH . $this->_fontsDir;

        foreach (new \DirectoryIterator(APPLICATION_PATH . $this->_fontsDir) as $f) {
            if (0 !== strcasecmp(pathinfo($f->getFileName(), PATHINFO_EXTENSION), 'ttf')) {
                continue;
            }

            $fileName = pathinfo($f->getFileName(), PATHINFO_FILENAME);

            if (in_array($fileName, $possibleFileNames)) {
                $fontFile = $fontsPath . $f->getFileName();
                if (file_exists($fontFile)) {
                    unlink($fontFile);

                    $eotFile = $fontsPath . $fileName . '.eot';
                    file_exists($eotFile) && unlink($eotFile);
                }
            }
        }

        $cssFontFile = APPLICATION_PATH . $this->_fontsCssDir . $fontName . '.css';
        if (file_exists($cssFontFile)) {
            unlink($cssFontFile);
        }

        $this->_createAllCssFontFile();
    }
}
