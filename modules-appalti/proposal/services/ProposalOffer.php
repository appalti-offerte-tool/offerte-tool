<?php

class Proposal_Service_ProposalOffer extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \Proposal_Model_DbTable_ProposalOffer
     */
    protected $_table;

    public function __construct()
    {
        $this->_table = new Proposal_Model_DbTable_ProposalOffer($this->getAdapter());
        $this->_attachValidationRules('default', array(
            'companyOfferId'    => array('id', 'allowEmpty' => false, 'presence' => 'required'),
            'proposalId'        => array('id', 'allowEmpty' => false, 'presence' => 'required'),
        ));
        $this->_attachValidationRules('update', array(
            'proposalCompanyOfferId' => array('id', 'allowEmpty' => false, 'presence' => 'required'),
            'companyOfferId' => array('id', 'allowEmpty' => false),
            'proposalId' => array('id', 'allowEmpty' => false),
            'amount' => array(array('float', 'en_EN'), 'allowEmpty' => false),
            'discount' => array(array('float', 'en_EN'), 'allowEmpty' => false)
        ));

    }

    public function getTotalCost($proposalId)
    {
        $select = $this->_table->getAdapter()
            ->select()
            ->from(
                array('pco' => $this->_table->getTableName()),
                array('s' => new Zend_Db_Expr('SUM(co.`pricePerUnit` * pco.`amount` * (1 - IF(pco.`discount` IS NULL, 0, pco.`discount`) / 100))'))
            )
            ->join(
                array('co' => 'companyOffer'),
                'co.id = pco.companyOfferId',
                array()
            )
            ->where('pco.proposalId = ?', $proposalId);

        $data = $select->query()->fetch();
        return $data['s'];
    }

    public function fetchAll($proposalId, $connected = true)
    {
        if ($connected) {
            $select = $this->_table->getAdapter()
                ->select()
                ->from(
                    array('pco' => $this->_table->getTableName()),
                    array('companyOfferId', 'proposalId', 'amount', 'discount', 'proposalCompanyOfferId' => 'id')
                )
                ->join(
                    array('co' => 'companyOffer'),
                    'co.id = pco.companyOfferId',
                    array('id', 'name', 'kind', 'description', 'pricePerUnit', 'inStock')
                )
                ->where('proposalId = ?', $proposalId)
                ->order('position ASC');

            $rowset = $select->query()->fetchAll();

        } else {
            $accountRow = Zend_Auth::getInstance()->getIdentity();

            if ($accountRow->isAdmin()) {
                $proposalSrv = new \Proposal_Service_Proposal();
                $proposalRow = $proposalSrv->find($proposalId);
                $companyRow = $proposalRow->getCompanyRow();
            } else {
                $companyRow = $accountRow->getCompanyRow(false);
            }

            $subSelect = $this->_table->getAdapter()
                        ->select()
                        ->from($this->_table->getTableName(), array('companyOfferId'))
                        ->where('proposalId = ?', $proposalId);

            $companyOfferTable = new Company_Model_DbTable_Offer();
            $rowset = $companyOfferTable->fetchAll(array(
                'id ' . ($connected ? '' : 'NOT') . ' IN (?)' => new Zend_Db_Expr("($subSelect)"),
                'companyId = ?' => $companyRow->getId()
            ));
        }

        return $rowset;
    }

    public function create($params)
    {
        $row = $this->_table->fetchRow(array(
            'companyOfferId = ?' => $params['companyOfferId'],
            'proposalId = ?' => $params['proposalId']
        ));

        if (empty($row)) {
            $row = $this->_table->createRow($params);
            $row->save();
        }

        return true;
    }

    public function delete($proposalId, $offerId)
    {
        $this->_table->deleteQuote(array(
            'companyOfferId = ?' => $offerId,
            'proposalId = ?' => $proposalId
        ));
    }

    public function update($proposalCompanyOfferId, $params = array())
    {
        if (isset($params['amount'])) {
            $params['amount'] = \str_replace(',', '.', $params['amount']);
        }
        if (isset($params['discount'])) {
            $params['discount'] = \str_replace(',', '.', $params['discount']);
        }
        $f = $this->_validate($params, 'update');
        return $this->_table->updateByPk($f->getData(), $proposalCompanyOfferId);
    }

    public function setPositions($proposalId, array $positions = array())
    {
        foreach($positions as $offerId => $pos) {
            $this->_table->updateQuote(
                array('position' => $pos),
                array(
                    'proposalId = ?' => (int)$proposalId,
                    'companyOfferId = ?' => (int)$offerId,
                )
            );
        }
    }

    public function flushProposalOfferDiscounts(\Proposal_Model_Proposal $proposalRow)
    {
        return $this->_table->updateQuote(array(
                'discount' => 0
            ), array(
                'proposalId = ?'    => (int) $proposalRow->getId()
            ));
    }


}