<?php

final class Proposal_Service_Generate
{
    /**
     * @var \Proposal_Model_Proposal
     */
    protected $_proposalRow;

    /**
     * @var \Proposal_Service_Section
     */
    protected $_sectionService;

    protected $_path;

    protected $_templatesPath;

    protected $_outputPath;

    protected $_tplParams = array();

    protected $_globalParams = array(
        '--outline-depth' => 2,
        '--image-quality' => 72,
    );

    public function __construct(\Proposal_Model_Proposal $proposalRow)
    {
        $this->_proposalRow = $proposalRow;

        $this->_sectionService = new \Proposal_Service_Section();
        $this->_path = realpath(__DIR__ . '/../data/tmp');
        $this->_templatesPath = realpath(APPLICATION_PATH . '/data/public-templates');
        $name = uniqid($this->_proposalRow->getId() . '-');
        $this->_path = $this->_path . '/' . $name;
        mkdir($this->_path, 0777, true);

        /**
         * Sometimes on unix mkdir does not set right permissions
         */
        chmod($this->_path, 0777);
        $this->_path = realpath($this->_path);
        if (false === $this->_path) {
            throw new OSDN_Exception('Unable to create temporary environment');
        }

        $this->_outputPath = $this->_path . '/' . $this->_proposalRow->luid;
        mkdir($this->_outputPath, 0777, true);
        chmod($this->_outputPath, 0777);
    }

    protected function _paramsToStr($tplName, $section)
    {
        $params = '';
        $globals = '';

        if (!empty($this->_tplParams[$tplName][$section]['global'])) {
            $globals = trim(join(' ', $this->_tplParams[$tplName][$section]['global']));
        }

        foreach ($this->_globalParams as $k => $v) {
            $params .= $k . ' ' . $v . ' ';
        }

        $params .= $globals;
        return $params;
    }



    public function generate()
    {
        /**
         * @FIXME Add real company name
         */
        $clientCompanyRow = $this->_proposalRow->getClientCompanyRow();
        $generatedDate = date('Y-m-d');

        $system = false !== stripos(PHP_OS, 'win') ? 'win' : 'linux';
        $bin = '/library/WkHtmlToPdf/bin/' . $system . '/wkhtmltopdf %s %s -' ;

        $proposalThemeRow = $this->_proposalRow->getThemeRow();

        foreach($this->_sectionService->fetchAllCollapsedByRegion() as $id => $region) {

            $command = array();
            $hasRequired = false;

            $opath = $this->_path . '/' . $this->_proposalRow->luid;

            foreach($region as $sectionRow) {
                $templateName = $proposalThemeRow->getTemplateThemeBySection($sectionRow);

                if ($sectionRow->isRequired()) {
                    $hasRequired = true;
                }

                if (empty($this->_tplParams[$templateName])) {
                    $this->_tplParams[$templateName] = array();
                    $params = $this->_sectionService->getGeneratorParams($sectionRow, $proposalThemeRow);
                    $this->_tplParams[$templateName] = $params;
                }

                if ($sectionRow->isCalculatable()) {
                    $command[] = 'toc --xsl-style-sheet ' . APPLICATION_PATH . '/data/public-templates/'. $templateName . '/index/toc.xml';
                    continue;
                }

                $blockDefinitionRowset = $this->_proposalRow->fetchAllBlockDefinitionBySection($sectionRow);
                if ($blockDefinitionRowset->count() == 0) {
                    continue;
                }

                $page = new \Proposal_Service_Page($this->_proposalRow, $blockDefinitionRowset);
                $page->setOutputPath($this->_path);

                if (!empty($this->_tplParams[$templateName][$sectionRow->getLuid()])) {
                    $page->setHeaderFooterParams($this->_tplParams[$templateName][$sectionRow->getLuid()]);
                }

                $command[] = $page->getCommand();
            }

            $command = trim(join(' ', $command));
            if (empty($command)) {
                continue;
            }

            $globalParams = $this->_paramsToStr($templateName, $sectionRow->getLuid());
            $cmd = sprintf(APPLICATION_PATH . $bin, $globalParams, $command);

            file_put_contents($this->_path . '/command-' . $id . '.txt', $cmd);
            $cmd = escapeshellcmd($cmd);

            $result = $this->_pipeExec($cmd);

            $regionRow = $sectionRow->getRegionRow();
            $name = !empty($regionRow['name']) ? $regionRow['name'] : $sectionRow->luid;

            file_put_contents(sprintf(
                '%s/%s %s %s.pdf',
                $this->_outputPath, $name,
                $clientCompanyRow->getName(),
                $generatedDate
            ), $result['stdout']);
        }

        $zipname = $this->_proposalRow->luid . '.zip';
        $zippath = $this->_path . '/' . $zipname;

        $zip = new Zend_Filter_Compress_Zip(array(
            'target'    => $this->_outputPath,
            'archive'   => $zippath
        ));
        $zip->compress($this->_outputPath);

        header('Content-disposition: attachment; filename="' . $zipname . '"');
        header('Content-type: application/zip');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($zippath));
        header('Pragma: no-cache');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        readfile($zippath);
    }

    protected function _throw($result)
    {
        header("Content-Description: File Transfer");
        header("Cache-Control: public; must-revalidate, max-age=0");
        header("Pragme: public");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate('D, d m Y H:i:s') . " GMT");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream", false);
        header("Content-Type: application/download", false);
        header("Content-Type: application/pdf", false);
        header(sprintf('Content-Disposition: attachment; filename="%s";', $this->_proposalRow->luid));
        header("Content-Transfer-Encoding: binary");
        header("Content-Length:" . strlen($result));
        echo $result;
    }

    private static function _pipeExec($cmd, $input = '')
    {
        $pipes = array();
        $proc = proc_open($cmd, array(
            0 => array('pipe', 'r'),
            1 => array('pipe', 'w'),
            2 => array('pipe', 'w')),
            $pipes, null, null,
            array(
                'binary_pipes' => true
            )
        );

        fwrite($pipes[0], $input);
        fclose($pipes[0]);

        // From http://php.net/manual/en/function.proc-open.php#89338
        $buffer_len = $prev_buffer_len = 0;
        $ms = 10;
        $stdout = '';
        $read_output = true;
        $stderr = '';
        $read_error = true;
        stream_set_blocking($pipes[1], 0);
        stream_set_blocking($pipes[2], 0);

        // dual reading of STDOUT and STDERR stops one full pipe blocking the other, because the external script is waiting
        while ($read_error != false or $read_output != false) {
            if ($read_output != false) {
                if (feof($pipes[1])) {
                    fclose($pipes[1]);
                    $read_output = false;
                } else {
                    $str = fgets($pipes[1], 1024);
                    $len = strlen($str);
                    if ($len) {
                        $stdout .= $str;
                        $buffer_len += $len;
                    }
                }
            }

            if ($read_error != false) {
                if (feof($pipes[2])) {
                    fclose($pipes[2]);
                    $read_error = false;
                } else {
                    $str = fgets($pipes[2], 1024);
                    $len = strlen($str);
                    if ($len) {
                        $stderr .= $str;
                        $buffer_len += $len;
                    }
                }
            }

            if ($buffer_len > $prev_buffer_len) {
                $prev_buffer_len = $buffer_len;
                $ms = 10;
            } else {
                usleep($ms * 1000); // sleep for $ms milliseconds
                if ($ms < 160) {
                    $ms = $ms * 2;
                }
            }
        }

        $rtn = proc_close($proc);
        return array(
            'stdout' => $stdout,
            'stderr' => $stderr,
            'return' => $rtn
        );
    }


    public function __destruct()
    {
        if (APPLICATION_ENV === 'development' || false === $this->_path) {
            return;
        }

        $flush = function($path) use (& $flush) {
            $fp = opendir($path);
            if (!$fp) {
                return;
            }

            while ($f = readdir($fp)) {
                $file = $path . "/" . $f;
                if ($f == "." || $f == "..") {
                    continue;
                }
                else if (is_dir($file) && !is_link($file)) {
                    $flush($file);
                }
                else {
                    unlink($file);
                }
            }

            closedir($fp);
            rmdir($path);
        };

        $flush($this->_path);
    }
}