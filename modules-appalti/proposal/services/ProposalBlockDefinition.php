<?php

class Proposal_Service_ProposalBlockDefinition extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \Proposal_Model_DbTable_ProposalBlockDefinition
     */
    protected $_table;

    /**
     * @var \Proposal_Model_ProposalRow
     */
    protected $_proposalRow;

    /**
     * @var ProposalBlock_Service_ProposalBlock
     */
    protected $_proposalBlockService;

    public function __construct(\Proposal_Model_Proposal $proposalRow)
    {
        $this->_proposalRow = $proposalRow;

        parent::__construct();

        $this->_table = new Proposal_Model_DbTable_ProposalBlockDefinition($this->getAdapter());

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if ($accountRow->isAdmin()) {
            $companyRow = $this->_proposalRow->getCompanyRow();
        } else {
            $companyRow = $accountRow->getCompanyRow();
        }

        $this->_proposalBlockService = new ProposalBlock_Service_ProposalBlock($companyRow);

        $this->_attachValidationRules('default', array(
            'position'          => array('allowEmpty' => true, 'presence' => 'required'),
            'fitable'           => array('allowEmpty' => false, 'presence' => 'required')
        ));
    }


    public function createDefinitionRow(\ProposalBlock_Model_ProposalBlock $blockRow, $sectionId, array $data = array())
    {
        $this->getAdapter()->beginTransaction();
        try {
            $proposalBlockFileStockService = new \ProposalBlock_Service_ProposalBlockFileStock();
            $proposalBlockDefinitionFileStockService = new \Proposal_Service_ProposalBlockDefinitionFileStock();
            $storage = $proposalBlockDefinitionFileStockService->getStorage();

            $definitionRow = $this->_table->createRow($data);
            $definitionRow->proposalId = $this->_proposalRow->getId();
            $definitionRow->sectionId = $sectionId;

            $definitionRow->proposalBlockId = $blockRow->getId();
            $definitionRow->name = $blockRow->name;
            $definitionRow->title = $blockRow->title;
            $definitionRow->titleLevel = $blockRow->titleLevel;
            $definitionRow->kind = $blockRow->kind;


            if (0 === strcasecmp('file', $blockRow->getKind()) && $blockRow->isFileValid()) {
                $proposalBlockDefinitionFileStockService->copyProposalBlockImage(
                    $this->_proposalRow,
                    $blockRow
                );
            }
            $definitionRow->imageName = $blockRow->imageName;

            if (0 === strcasecmp('text', $blockRow->getKind())) {
                $files = $proposalBlockFileStockService->fetchAllFileStockByProposalBlockIdWithResponse($blockRow->getId());

                $relRows = array();
                foreach ($files->getRowset() as $file) {
                    $fileStockRow = $proposalBlockDefinitionFileStockService->create($this->_proposalRow->getId(), $file['id'], $sectionId, false);
                    $newLink = $storage->toFilePathHost($fileStockRow, true);
                    if (!empty($file['link']) && !empty($newLink)) {
                        $relRows[$file['link']] = $newLink;
                    }
                }

                $metadata = $blockRow->metadata;
                foreach ($relRows as $key => $value) {
                    $metadata = \str_replace($key, $value, $metadata);
                }
            }

            $definitionRow->metadata = !empty($metadata) ? $metadata : '';
            $definitionRow->inBetween = (int) $blockRow->getCategoryRow()->inBetween();

            $definitionRow->save();
            $this->getAdapter()->commit();
        } catch (\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception($e->getMessage());
        }

        return $definitionRow;
    }

    /**
     * Allow to store multiple block definitions at one moment
     * @deprecated
     * @FIXME We are using outside transaction for now
     * Zend_Db Should be adjusted to support multiple transaction via SAVEPOINT
     *
     * @param Proposal_Model_Section $sectionRow
     * @param array $data
     *
     * @throws Exception
     * @throws OSDN_Exception
     */
    public function bulk(\Proposal_Model_Section $sectionRow, array $data, $useInternalTransaction = true)
    {
        $useInternalTransaction && $this->getAdapter()->beginTransaction();
        try {
            foreach($data as $blockId => $bi) {
                $definitionRow = $this->fetchByParent($blockId);

                if (null === $definitionRow) {
                    $this->createDefinitionRow($blockId, $sectionRow->getId(), $bi);
                } else {
                    unset($bi['id']);
                    $this->update($definitionRow, $bi);
                }
            }

            $useInternalTransaction && $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $useInternalTransaction && $this->getAdapter()->rollBack();

            throw $e;
        } catch(\Exception $e) {
            $useInternalTransaction && $this->getAdapter()->rollBack();

            echo $e;
            throw new OSDN_Exception('Unable to save proposal block definition', null, $e);
        }
    }

    public function flush($useInternalTransaction = true)
    {

        $useInternalTransaction && $this->getAdapter()->beginTransaction();
        try {
            $proposalBlockDefinitionFileStockService = new \Proposal_Service_ProposalBlockDefinitionFileStock();
            $proposalBlockDefinitionFileStockService->deleteByProposal($this->_proposalRow, false);
            $this->_table->deleteQuote(array('proposalId = ?' => $this->_proposalRow->getId()));
            $useInternalTransaction && $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $useInternalTransaction && $this->getAdapter()->rollBack();

            throw $e;
        } catch(\Exception $e) {
            $useInternalTransaction && $this->getAdapter()->rollBack();

            echo $e;
            throw new OSDN_Exception('Unable to delete proposal block definitions', null, $e);
        }

    }

    public function fetchAll()
    {
        return $this->_table->fetchAll(array('proposalId = ?' => $this->_proposalRow->getId()));
    }

    public function fetchAllBySectionId($sectionId)
    {
        return $this->_table->fetchAll(array(
            'proposalId = ?' => $this->_proposalRow->getId(),
            'sectionId = ?' => $sectionId
        ), 'position ASC');
    }

    public function fetchBlocksCount($sectionId = null)
    {
        $params = array('proposalId = ?' => $this->_proposalRow->getId());

        if ($sectionId) {
            $params['sectionId = ? '] = $sectionId;
        }

        return $this->_table->count($params);
    }

    /**
     * @param $id
     * @param bool $throwException
     * @return null|Proposal_Model_ProposalBlockDefinition
     * @throws OSDN_Exception
     */
    public function find($id, $throwException = true)
    {
        $row = $this->_table->findOne($id);
        if (null === $row && true === $throwException) {
            throw new OSDN_Exception('Unable to find definition block #' . $id);
        }

        return $row;
    }

    public function delete($id) {

        $this->getAdapter()->beginTransaction();
        try {
            $row = $this->find($id);

            if (0 === strcasecmp('text', $row->kind)) {
                $proposalBlockDefinitionFileStockService = new \Proposal_Service_ProposalBlockDefinitionFileStock();
                $response = $proposalBlockDefinitionFileStockService->fetchAllFileStockByProposalBlockIdWithResponse($row->id);

                if ($response->count()) {
                    $rowset = $response->getRowset();
                    foreach ($rowset as $filestockRow) {
                        $proposalBlockDefinitionFileStockService->delete($row->proposalId, $filestockRow['id'], true, $row->id);
                    }
                }
            } else {
                if ($row->isFileValid()) {
                    $files = $row->toFilePaths(false);
                    foreach ($files as $file) {
                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }
            }

            $row->delete();
            $this->getAdapter()->commit();
        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to delete proposal block definition.', null, $e);
        }

    }

    public function fetch($id)
    {
        if (empty($id)) {
            throw new OSDN_Exception('Block id is not defined');
        }

        return $this->_table->findOne($id);
    }

    public function fetchByParent($id)
    {
        if (empty($id)) {
            throw new OSDN_Exception('Block definition parent id is not defined.');
        }

        $rowset = $this->_table->fetchAll(array(
            'proposalId = ?' => $this->_proposalRow->getId(),
            'proposalBlockId = ?' => $id
        ));

        return $rowset->current();
    }

    public function update(\Proposal_Model_ProposalBlockDefinition $row, array $data = array())
    {
        try {
            $data['fitable'] = isset($data['fitable']) ? $data['fitable'] : $row->fitable;
            $data['position'] = isset($data['position']) ? $data['position'] : $row->position;

            $f = $this->_validate($data);
            $row->setFromArray($f->getData());
            $row->save();
        } catch (\Exception $e) {
            throw new OSDN_Exception($e->getMessage(), null, $e);
        }
    }
}
