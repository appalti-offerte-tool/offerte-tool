<?php

class Proposal_Service_Proposal extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \Proposal_Model_DbTable_Proposal
     */
    protected $_table;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new Proposal_Model_DbTable_Proposal($this->getAdapter());

        parent::_init();
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \Proposal_Model_Proposal*/
    public function find($id, $throwException = true)
    {
        $proposalRow = $this->_table->findOne($id);
        if (null === $proposalRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find proposal #' . $id);
        }

        return $proposalRow;
    }

    public function findOneByTicketId($id, $throwException = true)
    {
        $params = array(
            'supportTicketId = ?' => $id
        );

        $row = $this->_table->fetchRow($params);
        if (null === $row && true === $throwException) {
            throw new \OSDN_Exception('Unable to find proposal by ticket id #' . $id);
        }

        return $row;
    }

    public function fetchAll(array $params = array())
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();

        $select = $this->_table //->getAdapter()
            ->select()
            ->setIntegrityCheck(false)
            ->from(array('p' => 'proposal'))
            ->joinLeft(array('c' => 'company'), 'p.clientCompanyId = c.id', array())
            ->join(array('pt' => 'proposalTheme'), 'pt.proposalId = p.id', array('pt.includePrice','pt.includePersonalIntroduction'))
            ->joinLeft(array('pco' => 'proposalCompanyOffer'), 'pco.proposalId = p.id', array('countOffers' => 'COUNT(pco.id)'))
            ->where('p.companyId = ?', $accountRow->getCompanyRow()->getId(), Zend_Db::INT_TYPE);

        if (!empty($params['statusFilter'])) {
            $select->where('p.status = ?', $params['statusFilter']);
        }

        if ( !$accountRow->acl('proposal', 'list')) {
            $select->where('p.accountId = ?', $accountRow->getId(), Zend_Db::INT_TYPE);
        }
        $select->group('p.id')
//                ->order('p.status ASC')
                ->order('ADDDATE(p.createdDatetime, pt.daysValid) DESC')
        ;
        if (!empty($params['contactMomentDate'])) {
            $params['contactMomentDate'] = implode('-', array_reverse(explode('-', $params['contactMomentDate'])));
        }

        if (!empty($params['amount'])) {
            $params['amount'] = str_replace('.', '', $params['amount']);
        }


        $fields = array(
            'p.name'    => 'name',
            'p.luid'    => 'luid',
            'p.amount'    => 'amount',
            'p.chanceOfSuccess'    => 'chanceOfSuccess',
            'p.contactMomentDate'    => 'contactMomentDate',
            'c.name'    => 'client',
            'pt.includePersonalIntroduction' => 'includePersonalIntroduction',
            'pt.includePrice' => 'includePrice',
        );

        $this->_initDbFilter($select, $this->_table, $fields)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        $table = $this->_table;
        $response->setRowsetCallback(function($row) use ($table) {
            $proposalRow = $table->createRow($row);
            $proposalRow->includePersonalIntroduction = $row['includePersonalIntroduction'];
            $proposalRow->includePrice = $row['includePrice'];
            $proposalRow->countOffers = $row['countOffers'];
            return $proposalRow;
        });
        return $response;
    }

    public function fetchAllForStatistics(array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(
                array('p' => 'proposal')
            );

        if (!empty($params['companyId'])) {
            $select->where('p.companyId = ?', $params['companyId'], Zend_Db::INT_TYPE);
        }

        if (!empty($params['startDate'])) {
            $select->where('p.createdDatetime >= ? ', $params['startDate']);
        }

        if (!empty($params['startDate'])) {
            $select->where('p.createdDatetime <= ? ', $params['endDate']);
        }

        if (!empty($params['clientCompanyId'])) {
            $select->where('p.clientCompanyId = ? ', $params['clientCompanyId'], Zend_Db::INT_TYPE);
        }

        if (!empty($params['priceslider_min'])) {
            $select->where('p.amount >= ? ', $params['priceslider_min'], Zend_Db::INT_TYPE);
        }
        if (!empty($params['priceslider_max'])) {
            $select->where('p.amount <= ? ', $params['priceslider_max'], Zend_Db::INT_TYPE);
        }

        if (!empty($params['locations']) && array_filter($params['locations'])) {
            $select->where('p.locationId IN(?) ', implode(',',$params['locations']), Zend_Db::INT_TYPE);
        }
        if (!empty($params['themes']) && array_filter($params['themes'])) {
            $select->where('p.proposalCustomTemplateId IN (?) ', implode($params['themes']), Zend_Db::INT_TYPE);
        }
        if (!empty($params['regions']) && array_filter($params['regions'])) {
            $select->where('p.locationId IN(SELECT companyId FROM companyRegion WHERE regionId IN (?)) ', implode(',',$params['regions']), Zend_Db::INT_TYPE);
        }

        if (!empty($params['contactpersons']) && array_filter($params['contactpersons'])) {
            $select->where('p.accountId IN (?) ', implode($params['contactpersons']), Zend_Db::INT_TYPE);
        }
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if (!$accountRow->acl('proposal','list')) {
            $select->where('p.accountId = ? ', $accountRow->getId());
        }

        $response = $this->getDecorator('response')->decorate($select);
        $table = $this->_table;
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });

        return $response;
    }

    public function fetchAllByCompanyId(array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(
                array('p' => 'proposal')
            )
            ->joinLeft(
                array('c' => 'company'),
                'p.clientCompanyId = c.id',
                array()
            )
            ->where('p.companyId = ?', $params['companyId'], Zend_Db::INT_TYPE);

        if (!empty($params['statusFilter'])) {
            $select->where('p.status = ?', $params['statusFilter']);
        }

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if (!$accountRow->acl('proposal','list')) {
            $select->where('p.accountId = ? ', $accountRow->getId());
        }

        $select->order('p.status');
        if (!empty($params['contactMomentDate'])) {
            $params['contactMomentDate'] = implode('-', array_reverse(explode('-', $params['contactMomentDate'])));
        }

        if (!empty($params['amount'])) {
            $params['amount'] = str_replace('.', '', $params['amount']);
        }

        $fields = array(
            'p.name'    => 'name',
            'p.luid'    => 'luid',
            'p.amount'    => 'amount',
            'p.chanceOfSuccess'    => 'chanceOfSuccess',
            'p.contactMomentDate'    => 'contactMomentDate',
            'c.name'    => 'client'
        );

        $this->_initDbFilter($select, $this->_table, $fields)->parse($params);

        $response = $this->getDecorator('response')->decorate($select);
        $table = $this->_table;
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });

        return $response;
    }

    public function fetchAllLast($limit = 10)
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('p' => 'proposal')
            )
            ->where('p.companyId = ?', $accountRow->getCompanyRow()->getId(), Zend_Db::INT_TYPE)
            ->order('createdDatetime DESC')
            ->limit($limit);

//        if (!$accountRow->isAdmin()) {
        if (!$accountRow->acl('proposal','list')) {
            $select->where('p.accountId = ?', $accountRow->getId(), Zend_Db::INT_TYPE);
        }

        $response = $this->getDecorator('response')->decorate($select);

        $table = $this->_table;
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });

        return $response;
    }

    public function createRow(array $data = array())
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $data['accountId'] = $accountRow->getId();
        $data['companyId'] = $accountRow->getCompanyRow()->getId();
        $data['clientCompanyId'] = isset($data['clientCompanyId'])? (int)$data['clientCompanyId']:null;
        $data['clientContactPersonId'] = isset($data['clientContactPersonId'])? (int)$data['clientContactPersonId']:null;

        $f = $this->_validate($data);

        return $this->_table->createRow($f->getData());
    }

    /**
     * Create the new proposal row
     *
     * @param array $data
     * @return Proposal_Model_Proposal
     */
    public function create(array $data)
    {
        $this->_attachValidationRules('default', array(
//            'accountId'             => array('id', 'allowEmpty' => false, 'presence' => 'required'),
            'companyId'             => array('id', 'allowEmpty' => false, 'presence' => 'required'),
//            'clientCompanyId'       => array('id', 'allowEmpty' => true, 'presence' => 'required'),
//            'clientContactPersonId' => array('id', 'allowEmpty' => true),
//            'approvedAccountId'     => array('id', 'allowEmpty' => false),
//            'luid'                  => array('allowEmpty' => false, 'presence' => 'required'),
            'locationId'                => array('id', 'allowEmpty' => false, 'presence' => 'required'),
            'proposalCustomTemplateId'  => array('id', 'allowEmpty' => false, 'presence' => 'required'),
       ));

        $this->_attachValidationRules('update', array(
            'accountId'             => array('id', 'allowEmpty' => false, 'presence' => 'required'),
            'companyId'             => array('id', 'allowEmpty' => false, 'presence' => 'required'),
//            'clientCompanyId'       => array('id', 'allowEmpty' => true, 'presence' => 'required'),
//            'clientContactPersonId' => array('id', 'allowEmpty' => true),
            'approvedAccountId'     => array('id', 'allowEmpty' => false),
            'luid'                  => array('allowEmpty' => false, 'presence' => 'required'),
            'locationId'                => array('id', 'allowEmpty' => false, 'presence' => 'required'),
            'proposalCustomTemplateId'  => array('id', 'allowEmpty' => false, 'presence' => 'required'),
       ));

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $data['accountId'] = $accountRow->getId();
        $data['companyId'] = $accountRow->getCompanyRow()->getId();
        $data['clientCompanyId'] = isset($data['clientCompanyId'])? (int)$data['clientCompanyId']:null;
        $data['clientContactPersonId'] = isset($data['clientContactPersonId'])? (int)$data['clientContactPersonId']:null;

        $f = $this->_validate($data);

        $this->getAdapter()->beginTransaction();
        try {

            $proposalRow = $this->_table->createRow($f->getData());
            $proposalRow->save();

            $_themes = new Proposal_Service_CustomTemplate();
            if ($proposalRow->proposalCustomTemplateId)
            {
                $customTemplateRow = $_themes->find($proposalRow->proposalCustomTemplateId);
            }
            else
            {
                $customTemplateRow = $this->getDefaultRow();
            }
            $companyRow = $proposalRow->getAccountRow()->getCompanyRow();
            $_proposalTheme = new Proposal_Service_ProposalTheme($companyRow);
            $theme = $_proposalTheme->addThemeRow($proposalRow, $customTemplateRow);
            if (!empty($data['theme']) && is_array($data['theme'])) {
                $this->_saveTheme($proposalRow, $data['theme']);
            }

            $this->getAdapter()->commit();
        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to create proposal', 0, $e);
        }

        return $proposalRow;
    }

    public function update(\Proposal_Model_Proposal $proposalRow, array $data)
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();

        if (!$proposalRow->isEditable()
            && !$accountRow->acl('proposal','update')) {
            throw new OSDN_Exception('You do not have permission to this proposal');
        }

        $this->_attachValidationRules('update', array(
//            'clientCompanyId'       => array('id', 'allowEmpty' => true, 'presence' => 'required'),
//            'clientContactPersonId' => array('id', 'allowEmpty' => true),
//            'locationId'                => array('id', 'allowEmpty' => false, 'presence' => 'required'),
//            'proposalCustomTemplateId'  => array('id', 'allowEmpty' => false, 'presence' => 'required'),
        ));

        $f = $this->_validate($data, 'update');

        $this->getAdapter()->beginTransaction();
        try {
            $proposalRow->setFromArray($f->getData());
            $proposalRow->save();
            if (!empty($data['theme']) && is_array($data['theme'])) {
                $this->_saveTheme($proposalRow, $data['theme']);
            }

            $this->getAdapter()->commit();
        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update proposal');
        }

        return $proposalRow;
    }

    public function getProposalSections()
    {
        $sectionSrv = new \Proposal_Service_Section();
        return $sectionSrv->fetchAll();
    }

    protected function _saveTheme(\Proposal_Model_Proposal $proposalRow, array $data)
    {
        $theme = $proposalRow->getThemeRow();
        $theme->setFromArray($data);
        $theme->save();
    }

    public function delete(\Proposal_Model_Proposal $proposalRow)
    {
        if (!$proposalRow->isEditable()) {
            throw new OSDN_Exception('You do not have permission to this proposal');
        }

        $this->getAdapter()->beginTransaction();
        try {

            $companyRow = $proposalRow->getCompanyRow();

            $proposalThemeService = new \Proposal_Service_ProposalTheme($companyRow);
            $proposalThemeService->deleteByProposalRow($proposalRow);

            $proposalBlockDefinitionService = new \Proposal_Service_ProposalBlockDefinition($proposalRow);
            $proposalBlockDefinitionService->flush(false);

            $proposalRow->delete();
            $this->getAdapter()->commit();

        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;

        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to create proposal');
        }
        return true;
    }

    protected function _prepareStatisticsData($rowset)
    {
        $total = count($rowset);
        $result = array(
            'total' => $total,
            'totalMoney' => 0,
            'chart' => array(
                'amount' => array(),
                'money'  => array(),
                'colors' => array(),
                'titles' => array(),
            ),
            'status' => array(
                'in-progress' => array(
                    'total' => 0,
                    'money' => 0,
                    'color' => '"blue"',
                    'title' => '"Lopende offertes"',
                    'icon' => 'icon-pending'
                ),
                'lost'      => array(
                    'total' => 0,
                    'money' => 0,
                    'color' => '"red"',
                    'title' => '"Verloren offertes"',
                    'icon' => 'icon-lost'
                ),
                'won'       => array(
                    'total' => 0,
                    'money' => 0,
                    'color' => '"green"',
                    'title' => '"Gewonnen offertes"',
                    'icon' => 'icon-won'
                ),
                'withdrawn' => array(
                    'total' => 0,
                    'money' => 0,
                    'color' => '"orange"',
                    'title' => '"Teruggetrokken offertes"',
                    'icon' => 'icon-withdrawn'
                )
            )
        );

        if ($total) {
            foreach ($rowset as $row) {
                $result['status'][$row->status ? $row->status : 'in-progress']['total']++;
                $result['status'][$row->status ? $row->status : 'in-progress']['money'] += $row->amount;
                $result['totalMoney'] += $row->amount;
            }

            foreach($result['status'] as $v) {
                $percent = round($v['total'] * 100 / $total);
                if ($percent) {
                    $result['chart']['amount'][] = $percent;
                    $result['chart']['money'][] = $v['money'];
                    $result['chart']['colors'][] = $v['color'];
                    $result['chart']['titles'][] = $v['title'];
                }
            }

            foreach($result['chart'] as $k => $v) {
                $result['chart'][$k] = implode(', ', $v) ;
            }

            if (0 == $result['total']) {
                $result['totalInclInProgress'] = number_format(0, 2, ',', '.');
                $result['totalExclInProgress'] = number_format(0, 2, ',', '.');
                $result['averagePrice'] = number_format(0, 2, ',', '.');
            } else {
                $result['totalInclInProgress'] = number_format(($result['status']['won']['total'] / $result['total']) * 100, 2, ',', '.');

                $exclInProgress = $result['total'] - $result['status']['in-progress']['total'];

                if ($exclInProgress) {
                    $totalExclInProgress = ($result['status']['won']['total'] / ($result['total'] - $result['status']['in-progress']['total'])) * 100;
                } else {
                    $totalExclInProgress = 0;
                }

                $result['totalExclInProgress'] = number_format($totalExclInProgress, 2, ',', '.');
                $result['averagePrice'] = number_format($result['totalMoney'] / $result['total'], 2, ',', '.');
            }

            $result['totalMoneyInclInProgress'] = number_format($result['status']['in-progress']['money'] + $result['status']['won']['money'], 2, ',', '.');
            $result['totalMoneyExclInProgress'] = number_format($result['status']['won']['money'], 2, ',', '.');
            $result['totalCount'] = number_format($result['status']['in-progress']['money']+$result['status']['lost']['money']+$result['status']['won']['money']+$result['status']['withdrawn']['money'], 2, ',', '.');
        } else {
            $result['chart']['amount'] = 0;
            $result['chart']['money'] = 0;
            $result['chart']['colors'] = '"gray"';
            $result['chart']['titles'] = '"Leeg"';
            $result['totalInclInProgress'] = 0;
            $result['totalExclInProgress'] = 0;
            $result['totalMoneyInclInProgress'] = 0;
            $result['totalMoneyExclInProgress'] = 0;
            $result['totalCount'] = 0;
            $result['averagePrice'] = 0;
        }


        return $result;
    }

    public function fetchStatistics(array $params = array())
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if (empty($params['companyId']) && !$accountRow->isAdmin()) {
            $companyRow = $accountRow->getCompanyRow(false);
            if (null === $companyRow) {
                return array();
            }

            $params['companyId'] = $companyRow->getId();
        }

        $response = $this->fetchAllForStatistics($params);

        $data = $this->_prepareStatisticsData($response->getRowset());
        return $data;
    }

    /**
     *
     * @param array $params
     * @return type
     */
    public function fetchAlmostExpired(array $params = array())
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();

        $select = $this->_table->getAdapter()->select()
            ->from(
                array('p' => 'proposal'),
                array('id', 'luid', 'contactMomentDate', 'ed' => new \Zend_Db_Expr('TO_DAYS(p.contactMomentDate) - TO_DAYS(NOW())'))
            )
            ->joinLeft(
                array ('c'=>'company'),
                'p.companyId = c.id',
                array(
                    'clientName'=>'c.name',
                    'clientId'=>'c.id',
                )
            )
            ->joinLeft(
                array ('cp'=>'contactPerson'),
                'p.contactPersonId = cp.id',
                array(
                    'contactPersonName'=>'CONCAT(cp.firstname," ", cp.prefix, " ", cp.lastname)',
                    'contactPersonEmail' => 'cp.email',
                    'contactPersonPhone' => 'cp.phone'
                    )
            )
//            ->where('p.companyId = ?', $accountRow->getCompanyRow()->getId(), Zend_Db::INT_TYPE)
            ->where('p.contactMomentDate IS NOT NULL')
            ->where(new \Zend_Db_Expr('TO_DAYS(p.contactMomentDate) - TO_DAYS(NOW())') . ' > 0');


            if (!empty($params['startDate'])) {
                $select->where('p.contactMomentDate >= ?', $params['startDate'] . ' 0:0:0');
}
            if (!empty($params['endDate'])) {
                $select->where('p.contactMomentDate <= ?', $params['endDate'] . ' 23:59:59');
            }
            if (!empty($params['expiredPeriod'])) {
                $select->where(new \Zend_Db_Expr('TO_DAYS(p.contactMomentDate) - TO_DAYS(NOW())') . ' < ' . \intval($params['expiredPeriod']));
            }

            $select->order('p.createdDatetime DESC');

        $this->_initDbFilter($select, $this->_table, array('name'))->parse($params);

        $response = $this->getDecorator('response')->decorate($select);

        return $response;
    }


    protected function getHitrateSelect(array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(
                array('p' => new \Zend_Db_Expr('(SELECT sum(IF(`status` like "won", 1, 0)) * 100 /count(`id`) as hitrate, companyId  FROM proposal group by companyId)')),
                array('hitrate', 'companyId')
            );

        if (isset($params['hitrate'])) {
            $select->where('p.hitrate <= ?', $params['hitrate'], Zend_Db::FLOAT_TYPE);
        }

        if (isset($params['direction']) && \in_array(\strtolower($params['direction']), array('asc','desc'))) {
            $select->order('p.hitrate ' . $params['direction']);
        } else {
            $select->order('p.hitrate');
        }

        if (isset($params['topLimit'])) {
            $select->limit(\intval($params['topLimit']));
        }

        return $select;
    }

    /**
     *
     * @param array $params
     * @return type
     */
    public function fetchTopHitrate(array $params = array())
    {
        $select = $this->getHitrateSelect($params);
        $response = $this->getDecorator('response')->decorate($select);

        $companyService = new \Company_Service_Company();
        $response->setRowsetCallback(function($row) use ($companyService) {
            $c = $companyService->find($row['companyId']);
            $row['companyName'] = $c->name;
            return $row;
        });

        return $response;
    }

    /**
     *
     * @param array $params
     * @return type
     */
    public function fetchHitrateUnder(array $params = array())
    {
        $select = $this->getHitrateSelect($params);
        $this->_initDbFilter($select, $this->_table, array('name'))->parse($params);
        $response = $this->getDecorator('response')->decorate($select);

        $companyService = new \Company_Service_Company();
        $response->setRowsetCallback(function($row) use ($companyService) {

            $c = $companyService->find($row['companyId']);
            $cp = $c->getCompanyDefaultContactPersonRow();

            $row['companyName'] = $c->name;
            $row['contactPersonName'] = $cp->getFullname();
            $row['contactPersonEmail'] = $cp->email;
            $row['contactPersonPhone'] = $cp->phone;

            return $row;
        });


        return $response;
    }

    /**
     *
     * @param array $params
     * @return type
     */
    public function fetchHitrateCount(array $params = array())
    {
        $select = $this->getHitrateSelect($params);
        $response = $this->getDecorator('response')->decorate($select);

        return $response;
    }

    public function fetchProposalRowWithMaxAmount()
    {
        $companyRow = Zend_Auth::getInstance()
                                ->getIdentity()
                                ->getCompanyRow()
        ;
        $select = $this->_table
                        ->select()
                        ->where('companyId = ?', $companyRow->getId())
                        ->order('amount DESC')
        ;
        return $this->_table->fetchRow($select);
    }
}




