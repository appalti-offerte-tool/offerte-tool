<?php

class Proposal_Service_CustomTemplate extends \OSDN_Application_Service_Dbable
{
    protected $_path = '/modules-appalti/company/data/templates/';

    protected $_tplPath = '/data/public-templates/';

    /**
     * @var \Proposal_Model_DbTable_CustomTemplate
     */
    protected $_table;

    /**
     * @var \Proposal_Model_DbTable_Theme
     */
    protected $_themeTable;

    /**
     * @var \Proposal_Model_DbTable_CustomTemplateSettings
     */
    protected $_settingsTable;


    protected function _toCompanyRow()
    {
        if (empty($this->_companyRow)) {
            $accountRow = Zend_Auth::getInstance()->getIdentity();
            $this->_companyRow = $accountRow->getCompanyRow(false);
        }
        return $this->_companyRow;

    }

    public function __construct(Company_Model_CompanyRow $companyRow = null)
    {
        $this->_companyRow = $companyRow;
        parent::__construct();
    }
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new Proposal_Model_DbTable_CustomTemplate($this->getAdapter());
        $this->_settingsTable = new Proposal_Model_DbTable_CustomTemplateSettings();
        $this->_themeTable = new Proposal_Model_DbTable_ProposalTheme();

        parent::_init();
    }

    protected function _recursiveCopy($src, $dst)
    {
        $dir = opendir($src);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' ) && ( $file != '.svn' )) {
                if ( is_dir($src . '/' . $file) ) {
                    if (!file_exists($dst . '/' . $file)) {
                        mkdir($dst . '/' . $file, 0777, true);
                    }

                    $this->_recursiveCopy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    protected function _copyEmpty(\Company_Model_CompanyRow $company)
    {
        $dst = APPLICATION_PATH . $this->_path . sprintf('%d/%s/', $company->getId(), 'tpl');

        try {
            if (!is_dir($dst)) {
                mkdir($dst, 0775, true);
                chmod($dst, 0775);
            }

            $src = APPLICATION_PATH . $this->_tplPath . '/empty';
            $this->_recursiveCopy($src, $dst);
        } catch (\Exception $e) {
            throw new OSDN_Exception('Error creating custom template file structure.',  0, $e);
        }
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \Proposal_Model_Proposal
     */
    public function find($id, $throwException = true)
    {
        $row = $this->_table->findOne($id);
        if (null === $row && true === $throwException) {
            throw new OSDN_Exception('Unable to find template #' . $id);
        }

        return $row;
    }

    public function fetchByCompany(\Company_Model_CompanyRow $companyRow)
    {
        return $this->_table->fetchRow(array(
            'companyId = ?' => $companyRow->getId()
        ));
    }

    public function fetchAll()
    {
        $select = $this->_table
                        ->select()
                        ->where('companyId = ?', $this->_toCompanyRow()->getId())
        ;
        return $this->_table->fetchAll($select);
    }

    public function fetchAllActive()
    {
        $select = $this->_table
                        ->select()
                        ->where('companyId = ?', $this->_toCompanyRow()->getId())
                        ->where('active = 1')
        ;
        return $this->_table->fetchAll($select);
    }

    public function fetchProposalsUsingTheme(Proposal_Model_CustomTemplate $customTemplateRow)
    {
        $this->_proposals = new Proposal_Model_DbTable_Proposal();
        $select = $this->_proposals->select()
                                    ->from(array('p'=>'proposal'),'p.*')
                                    ->join(array('pt'=>'proposalTheme'), 'pt.proposalId = p.id','')
                                    ->where('p.companyId = ?', $customTemplateRow->companyId)
                                    ->where('pt.useCustomTemplate = ?', $customTemplateRow->getId())
        ;
        return $this->_proposals->fetchAll($select);
    }

    /**
     * Saves a new row
     * @param Company_Model_CompanyRow $company
     * @param array $data
     * @return Proposal_Model_CustomTemplate
     * @throws OSDN_Exception
     */
    public function create(\Company_Model_CompanyRow $companyRow, array $data = array())
    {
        $data['companyId'] = $companyRow->getId();
        $this->getAdapter()->beginTransaction();
        try {
            $row = $this->_table->createRow($data);
            $row->save();

            /* create default settings */
            $scopes = Proposal_Model_CustomTemplate::getScopes();
            foreach($scopes as $k => $v) {
                $settingsRow = $this->_settingsTable->createRow(array(
                    'proposalCustomTemplateId' => $row->getId(),
                    'scope' => $k
                ));
                $settingsRow->save();
            }

            $themeRow = $row->getThemeRow();
            $themeRow->setFromArray(array( 'useCustomTemplate' => 1 ));
            $themeRow->save();

//            $this->_copyEmpty($company);

            $this->getAdapter()->commit();
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to create custom template.', 0, $e);
        }

        return $row;
    }

    /**
     * returns a new row without saving
     * @param array $data
     * @return Proposal_Model_CustomTemplate
     */
    public function createRow(array $data = array(), $defaultSource = null)
    {
        $data['companyId'] = $this->_toCompanyRow()->getId();
        return $this->_table->createRow($data, $defaultSource);
    }


    public function restoreTemplate(\Company_Model_CompanyRow $company)
    {
        if ($this->fetchByCompany($company)) {
            $this->_copyEmpty($company);
        } else {
            $this->create($company);
        }
    }

    /**
     * @param Proposal_Model_CustomTemplate $tpl
     * @param array $data
     */
    public function saveSettings(\Proposal_Model_CustomTemplate $tpl, array $data = array())
    {
        $scope = !empty($data['scope']) ? $data['scope'] : $tpl::COMMON_BG_SCOPE;
        try {
            $settingsRow = $tpl->getSettingsByScope($scope);

            if ($settingsRow) {
                $settingsRow->setFromArray($data);
            } else {
                $data['proposalCustomTemplateId'] = $tpl->getId();
                $settingsRow = $this->_settingsTable->createRow($data);
            }

            $settingsRow->save();
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to save template settings. ' . $e->getMessage());
        }

        return $settingsRow;
    }

    /**
     * @param Proposal_Model_CustomTemplate $template
     * @param array $data
     * @return Proposal_Model_CustomTemplate
     * @throws OSDN_Exception
     */
    public function update(\Proposal_Model_CustomTemplate $template, array $data)
    {
        try {
            $template->setFromArray($data);
            $template->save();
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to update proposal template');
        }

        return $template;
    }

    /**
     * @param Proposal_Model_CustomTemplate $tpl
     * @return bool
     * @throws OSDN_Exception
     */
    public function delete(\Proposal_Model_CustomTemplate $tpl)
    {
        try {
            $tpl->delete();
        } catch (\Exception $e) {
            throw new OSDN_Exception(sprintf('Error deleting template. %s', $e->getMessage()));
        }

        return true;
    }

     /**
     * @param Proposal_Model_CustomTemplate $tpl
     * @return bool
     * @throws OSDN_Exception
     */
    public function deleteTheme(\Proposal_Model_CustomTemplate $customTemplateRow)
    {
        // init
        $adapter = $this->_table->getAdapter()
                                    ->beginTransaction();
        try {
            // delete custom template settings
            foreach($customTemplateRow->findDependentRowset('Proposal_Model_DbTable_CustomTemplateSettings') as $customTemplateSettingsRow)
            {
                $customTemplateSettingsRow->delete();
            }

            // delete proposal theme
            $customTemplateRow->findParentRow('Proposal_Model_DbTable_ProposalTheme')
                                ->delete();

            // delete custom template
            $customTemplateRow->delete();

            $adapter->commit();
        } catch (\Exception $e) {
            $adapter->rollBack();
            throw new OSDN_Exception(sprintf('Error deleting template. %s', $e->getMessage()));
        }
        return true;
    }

    public function activateTheme(\Proposal_Model_CustomTemplate $customTemplateRow)
    {
        try {
            $customTemplateRow->activate();
        } catch (\Exception $e) {
            throw new OSDN_Exception(sprintf('Error activating template. %s', $e->getMessage()));
        }
        return true;
    }

    public function deActivateTheme(\Proposal_Model_CustomTemplate $customTemplateRow)
    {
        try {
            $customTemplateRow->deactivate();
        } catch (\Exception $e) {
            throw new OSDN_Exception(sprintf('Error deactivating template. %s', $e->getMessage()));
        }
        return true;
    }

    public function getCssStyles(\Proposal_Model_Section $sectionRow, \Proposal_Model_ProposalTheme $themeRow)
    {
        $tpl = $themeRow->getTemplateThemeBySection($sectionRow);
        $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow(false);
        $luid = $sectionRow->getLuid();

        $css = array('/data/public-templates/common/css/reset.css');

        $cssFile = sprintf('/data/public-templates/%s/%s/css/%s.css', $tpl, $luid, $luid);

        if (!file_exists(APPLICATION_PATH . $cssFile) || $sectionRow->useContentCss()) {
            $cssFile = sprintf('/data/public-templates/%s/css/content.css', $tpl);
        }

        $css[] = $cssFile;

        if (null !== $companyRow) {
            $css[] = '/modules-appalti/company/data/css-company-theme/' . $companyRow->getId() . '/theme.'.$themeRow->getId().'.css';
        }

        return join(', ', $css);
    }

    protected function _removePrevious($companyId, $customTemplateId, $scope)
    {
        $relPath = $this->_path . sprintf('%d/%d/%s/', $companyId, $customTemplateId, 'backgrounds');
        $path = APPLICATION_PATH . $relPath;

        if (file_exists($path)) {
            foreach(new \DirectoryIterator($path) as $f) {
                if ($f->isFile() && (pathinfo($f->getFilename(), PATHINFO_FILENAME) === $scope || pathinfo($f->getFilename(), PATHINFO_FILENAME) === $scope.'-thumb')) {
                    unlink($f->getPathname());
                }
            }
        }
    }

    protected function _base64EncodeBg($file)
    {
        if ($fp = fopen($file,"rb", 0)) {
            $picture = fread($fp, filesize($file));
            fclose($fp);
            $base64 = chunk_split(base64_encode($picture));

            $output = pathinfo($file, PATHINFO_DIRNAME)  . '/' . pathinfo($file, PATHINFO_FILENAME) . '.base64';
            if ($output = fopen($output, 'x+')) {
                $fileType = pathinfo($file, PATHINFO_EXTENSION);
                $fileType = 'jpg' === $fileType ? 'jpeg' : $fileType;
                fwrite($output, sprintf('data:image/%s;base64,%s', $fileType, $base64));
                fclose($output);
            }
        }
    }

    protected function _resize($file, $landscape, $thumb = false)
    {
        $width = $landscape ? 3508 : 2480;
        $height = $landscape ? 2480 : 3508;
        $thumbWidth = $landscape ? 595 : 420;
        $thumbHeight = $landscape ? 420 : 595;
        $thumbname = sprintf('/%s-thumb.jpg', pathinfo($file, PATHINFO_FILENAME));
        if (extension_loaded('imagick'))
        {
            $im = new Imagick();
            $im->readImage($file);
            $im->setImageCompression(Imagick::COMPRESSION_LOSSLESSJPEG);
            $im->setImageCompressionQuality(100);
            $im->resizeImage($thumb ? $thumbWidth : $width, $thumb ? $thumbHeight : $height, Imagick::FILTER_UNDEFINED, true);
            $im->stripImage();
            $im->writeImage($thumb ? pathinfo($file, PATHINFO_DIRNAME) . $thumbname : $file);
            $im->clear();
            $im->destroy();
        }
        else
        {
            $filename = $file;
            // Get new sizes
            list($filewidth, $fileheight) = getimagesize($filename);
            $percent = ($filewidth/$width);
            if ($percent > ($fileheight/$height) )
            {
                $percent = ($fileheight/$height);
            }
            $newwidth = ($thumb)? $thumbWidth : $width;
            $newheight = ($thumb)? $thumbHeight : $height;
            $destination = imagecreatetruecolor($newwidth, $newheight);
            $source = imagecreatefromjpeg($filename);
            imagecopyresized($destination, $source, 0, 0, 0, 0, $newwidth, $newheight, $filewidth, $fileheight);
            imagejpeg($destination, ($thumb)? pathinfo($file, PATHINFO_DIRNAME).$thumbname : $file, 100);
        }

        /* save base64 encoded file for later use in template */
        if (!$thumb) {
            $this->_base64EncodeBg($file);
        }

        return $thumb ? $thumbname : '';
    }

    protected function _processImage(
        \Zend_File_Transfer_Adapter_Abstract $transfer,
        $customTemplateId = 0,
        $scope = Proposal_Model_CustomTemplate::COMMON_BG_SCOPE,
        $landscape = false
    )
    {
        $companyRow = Zend_Auth::getInstance()
                                ->getInstance()
                                ->getIdentity()
                                ->getCompanyRow();

        $relPath = $this->_path . sprintf('%d/%d/%s/', $companyRow->getId(), $customTemplateId, 'backgrounds');
        $path = APPLICATION_PATH . $relPath;

        if (!is_dir($path)) {
            mkdir($path, 0775, true);
            chmod($path, 0775);
        }

        $transfer->setDestination($path);

        foreach ($transfer->getFileInfo() as $key => $value)
        {
            $isPdf = false;
            do {
                $transfer->addValidator('MimeType', false, 'application/pdf', $key);
                if ($transfer->isValid($key)) {
                    $isPdf = true;
                    break;
                }
                $transfer->removeValidator('MimeType');

                $transfer->addValidator('IsImage', true, null, $key);
                if ($transfer->isValid($key)) {
                    break;
                }

                $e = new OSDN_Exception('Mogelijke bestandsformaten zijn: jpeg, jpg, pdf.');
                $e->assign($transfer->getMessages());
                throw $e;

            } while(false);

            $this->_removePrevious($companyRow->getId(), $customTemplateId, $scope);

            $fi = $transfer->getFileInfo($key);
            $extension = pathinfo($fi[$key]['name'], PATHINFO_EXTENSION);

            $filename = sprintf('%s.%s', $scope, $extension);
            $transfer->addFilter('Rename', array(
                'target'    => $path . $filename,
                'overwrite' => true
            ), $key);

            $transfer->receive($key);

            if ($isPdf) {
                try {
                    /**
                     * set new max_execution_time cause on windows system
                     * large pdf conversion takes more time
                     */
                    $isWindows = false !== stripos(PHP_OS, 'win');
                    if ($isWindows) {
                        $timeLimit = ini_get('max_execution_time');
                        set_time_limit(120);
                    }

                    $im = new Imagick();
                    $im->setResolution(300, 300);
                    $im->readImage($path . sprintf('%s[%d]', $filename, 0));
                    $im->setImageFormat('jpg');
                    $im->setImageCompression(Imagick::COMPRESSION_LOSSLESSJPEG);
                    $im->setImageCompressionQuality(100);
                    $im->stripImage();
                    $filename = sprintf('%s.jpg', $scope);
                    $imgPath = $path . $filename;
                    $im->writeImage($imgPath);
                    $im->clear();
                    $im->destroy();

                    unlink($path . $scope . '.pdf');

                    $this->_resize($imgPath, $landscape);

                    /* reset max_execution_time to default */
                    if ($isWindows) {
                        set_time_limit((int)$timeLimit);
                    }

                } catch (\ImagickException $e) {
                    throw new OSDN_Exception('Unable to convert PDF to image. Imagick error: ' . $e->getMessage());
                } catch (\Exception $e) {
                    $err = new OSDN_Exception('Unable to convert PDF to image');
                    $err->assign($e->getMessages());
                    throw $err;
                }
            } else {
                $imgPath = $path . $filename;
                $this->_resize($imgPath, $landscape);
            }

            $thumbname = $this->_resize($imgPath, $landscape, true);
        }

        return $relPath . $thumbname;
    }

    public function saveBackground(array $data = array())
    {
        $transfer = new Zend_File_Transfer_Adapter_Http();
        $canProcessFile = true;
        foreach ($transfer->getFileInfo() as $key => $value) {
            $canProcessFile = $transfer->isUploaded($key);
        }
        if ($canProcessFile) {
            return $this->_processImage($transfer, $data['id'], $data['scope'], $data['landscape']);
        }
    }

    public function deleteBackground($customTemplateId, $scope = Proposal_Model_CustomTemplate::COMMON_BG_SCOPE)
    {
        $companyRow = Zend_Auth::getInstance()->getInstance()->getIdentity()->getCompanyRow();

        $relPath = $this->_path . sprintf('%d/%d/%s/', $companyRow->getId(), $customTemplateId, 'backgrounds');
        $path = APPLICATION_PATH . $relPath;

        if (!is_dir($path)) {
            mkdir($path, 0775, true);
            chmod($path, 0775);
        }
        $this->_removePrevious($companyRow->getId(), $customTemplateId, $scope);
    }

    public function copy(Proposal_Model_CustomTemplate $customTemplateRow, array $params = array())
    {
        $adapter = $this->_table->getAdapter()
                                ->beginTransaction()
        ;
        try {
            // clone customTemplateRow
            $data_ct = array_merge(
                $customTemplateRow->toArray(),
                array(
                    'id' => 0,
                    'companyId'     => $this->_toCompanyRow()->getId(),
                    'name'          => trim($params['name'])? trim($params['name']):'copy_of_'.$customTemplateRow->name,
                    'description'   => trim($params['description'])? trim($params['description']):$customTemplateRow->description,
            ));
            $customTemplateRowClone = $this->_table->createRow($data_ct);
            if (!$customTemplateRowClone->save())
            {
                throw new Exception('Template kon niet gekopiëerd worden.');
            }

            // get proposalThemeRow
            $select = $this->_themeTable->select()
                                        ->where('useCustomTemplate = ?', $customTemplateRow->getId())
                                        ->where('proposalId IS NULL')
            ;
            $proposalThemeRowset = $this->_themeTable->fetchAll($select);
            if (count($proposalThemeRowset) <> 1)
            {
                throw new OSDN_Db_Exception('Database is corrupt.');
            }

            // clone Proposal_Theme
            $data_pt = array_merge(
                    $proposalThemeRowset->current()->toArray(),
                    array(
                        'id' => 0,
                        'useCustomTemplate' => $customTemplateRowClone->getId(),
                    )
            );
            $proposalThemeRowClone = $this->_themeTable->createRow($data_pt);
            if (!$proposalThemeRowClone->save())
            {
                throw new Exception('Huisstijl kon niet gekopiëerd worden.');
            }

            // get customTemplateSettingsRows
            foreach($customTemplateRow->getSettings() as $customTemplateSettingRow)
            {
                // clone customTemplateSettingRow
                $data_cts = array_merge(
                    $customTemplateSettingRow->toArray(),
                    array(
                        'id' => 0,
                        'proposalCustomTemplateId' => $customTemplateRowClone->getId(),
                    )
                );
                $customTemplateSettingRowClone = $this->_settingsTable->createRow($data_cts);
                if (!$customTemplateSettingRowClone->save())
                {
                    throw new Exception('Pagina kon niet gekopiëerd worden.');
                }
            }

            // copy theme files
            $dirBase = str_replace('/', DIRECTORY_SEPARATOR ,APPLICATION_PATH . $this->_path . $customTemplateRow->companyId .'/');
            $dirFrom = $dirBase . $customTemplateRow->getId();
            if (is_dir($dirFrom))
            {
                $dirTo = $dirBase . $customTemplateRowClone->getId();
                Appalti_File::copy($dirFrom, $dirTo);
            }

            // copy link libraries
            $themeLibraries = $customTemplateRow->findDependentRowset('Company_Model_DbTable_LibraryProposalCustomTemplate');
            if($themeLibraries)
            {
                foreach($themeLibraries as $themeLibrary)
                {
                    $data = array_merge(
                        $themeLibrary->toArray(),
                        array(
                            'proposalcustomtemplateId' => $customTemplateRowClone->getId(),
                        )
                    );
                    $themeLibraries->getTable()->insert($data);
                }
            }

            // success
            $adapter->commit();
            return $customTemplateRowClone;
        } catch (OSDN_Exception $e) {
                $adapter->rollBack();
                throw $e;
        } catch(\Exception $e) {
                $adapter->rollBack();
                throw $e;
        }
    }


    public function getLibraries(Proposal_Model_CustomTemplate $customTemplate)
    {
        return $customTemplate->findManyToManyRowset('Company_Model_DbTable_Library', 'Company_Model_DbTable_LibraryProposalCustomTemplate');
    }

    public function setLibraries(Proposal_Model_CustomTemplate $customTemplateRow, array $libraryIds = array())
    {
        $adapter = $this->getAdapter();
        $count_rows_deleted = $adapter->delete('libraryProposalcustomtemplate', 'proposalcustomtemplateId = '.(int)$customTemplateRow->getId());
        foreach($libraryIds as $libraryId)
        {
            if ((int)$libraryId)
            {
                $data = array(
                    'proposalcustomtemplateId' => $customTemplateRow->getId(),
                    'libraryId'  => $libraryId,
                );
                $adapter->insert('libraryProposalcustomtemplate', $data);
            }
        }
    }


}
