<?php

class Proposal_Service_ProposalVersionHtml extends \OSDN_Application_Service_Dbable
{
    /**
     * @var \Proposal_Model_DbTable_ProposalVersionHtml
     */
    protected $_table;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new Proposal_Model_DbTable_ProposalVersionHtml($this->getAdapter());

        parent::_init();
    }


    /**
     * @param Proposal_Model_ProposalVersion $proposalVersionRow
     * @param array $params Filter params (limit, order, etc.)
     * @return OSDN_Db_Response
     */
    public function fetchAll(\Proposal_Model_ProposalVersion $proposalVersionRow, array $params = array())
    {
        $select = $this->_table->getAdapter()->select()
            ->from(array(
                'pvh' => 'proposalVersionHtml'
            ))
            ->where('pvh.proposalVersionId = ?', $proposalVersionRow->getId(), Zend_Db::INT_TYPE);


        $this->_initDbFilter($select, $this->_table)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        $table = $this->_table;
        $response->setRowsetCallback(function($row) use ($table) {
            return $table->createRow($row);
        });

        return $response;
    }

    /**
     * Create proposal version if it has any changes compared to the last version
     *
     * @param Proposal_Model_Proposal $proposalRow
     * @param array $data
     * @example <code>
     * array(
     *   'proposalId'       => 1,
     *   'proposalBlocks'   => serialized_data,
     *   'folderName'       => 'companyId-proposalId-dateCreated-uniqueId',
     *   'html' => array(
     *      array(
     *          '<regionId>'    => '1',
     *          '<html>'        => '<p>Some text</p>'
     *      )
     *   )
     * );
     * </code>
     * @return Proposal_Model_Proposal
     * @throws OSDN_Exception
     */
    public function create(\Proposal_Model_Proposal $proposalRow, array $data)
    {
        $this->_attachValidationRules('default', array(
            'folderName'     => array('allowEmpty' => false, 'presence' => 'required'),
            'html'           => array('allowEmpty' => false, 'presence' => 'required')
        ));

        $f = $this->_validate($data);

        $data['proposalId'] = $proposalRow->getId();
        $data['proposalBlocks'] = $this->_getSerializedBlocks($proposalRow);

        $proposalVersionRow = $this->_table->createRow($f->getData());
        $last = $this->fetchLast($proposalRow);

        if ($last) {
            $diff = $this->compare($last, $proposalVersionRow);
            if ($diff['total'] === 0) {
                return;
            }
        }

        $this->getAdapter()->beginTransaction();
        try {
            $proposalVersionRow->save();

            foreach ($data['html'] as $region) {
                $proposalVersionHtmlRow = new \Proposal_Model_ProposalVersionHtml();
                $proposalVersionHtmlRow->setFromArray($region);
                $proposalVersionHtmlRow->save();
            }

            $this->getAdapter()->commit();
        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new OSDN_Exception('Unable to create proposal', 0, $e);
        }

        return $proposalVersionRow;
    }

    /**
     * @param Proposal_Model_ProposalVersion $proposalVersionRow
     * @return bool
     * @throws OSDN_Exception
     */
    public function delete(\Proposal_Model_ProposalVersion $proposalVersionRow)
    {
        try {
            $proposalVersionRow->delete();
        } catch(\Exception $e) {
            throw new OSDN_Exception('Unable to create proposal');
        }

        return true;
    }
}
