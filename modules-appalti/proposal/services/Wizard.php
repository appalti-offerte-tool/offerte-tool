<?php

/**
 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
 * @version     SVN: $Id: Wizard.php 983 2012-10-16 14:57:34Z vanya $
 * @changedby   $Author: vanya $
 */

class Proposal_Service_Wizard
{
    const STEP_CREATE           = 0;
    const STEP_RELATION         = 1;
    const STEP_PERSONAL_INTRO   = 2;
    const STEP_PRICE            = 3;
    const STEP_BLOCKS           = 4;
    const STEP_SUMMARY          = 5;

    protected $_account;

    protected $_wizardSteps = array(
        self::STEP_CREATE => array(
            'enabled'   => true,
            'isActive'  => false,
            'title'     => 'Offerte informatie',
            'url'       => '/proposal/create/index',
            'params'    => ''
        ),
        self::STEP_RELATION => array(
            'enabled'   => true,
            'isActive'  => false,
            'title'     => 'Relatie gegevens',
            'url'       => '/proposal/create/relation',
            'params'    => ''
        ),
        self::STEP_PERSONAL_INTRO => array(
            'enabled'   => false,
            'isActive'  => false,
            'title'     => 'Persoonlijke inleiding',
            'url'       => '/proposal/create/personal-introduction',
            'params'    => ''
        ),
        self::STEP_PRICE => array(
            'enabled'   => true,
            'isActive'  => false,
            'title'     => 'Prijsopgave',
            'url'       => '/proposal/create/price',
            'class'     => 'pricePage',
            'params'    => ''
        ),
        self::STEP_BLOCKS => array(
            'enabled'   => true,
            'isActive'  => false,
            'title'     => 'Samenstellen offerte',
            'url'       => '/proposal/create/blocks',
            'params'    => ''
        ),
        self::STEP_SUMMARY => array(
            'enabled'   => true,
            'isActive'  => false,
            'title'     => 'Afronding',
            'url'       => '/proposal/create/summary',
            'params'    => '',
            'redirect'  => '/proposal/index/index'
        )
    );

    /**
     * @var Zend_Session_Namespace
     */
    protected $_session;

    public function __construct()
    {
        $this->_account = Zend_Auth::getInstance()->getIdentity();
        $uniqueSessionName = __CLASS__ . '_' . $this->_account->getId();
        $this->_session = new \Zend_Session_Namespace($uniqueSessionName);
        $this->getSteps();
    }

    protected function _setActive()
    {
        for($i = 0; $i < count($this->_wizardSteps); $i++)
        {
            $this->_wizardSteps[$i]['isActive'] = false;
        }
        $this->_wizardSteps[$this->_session->step]['isActive'] = true;
    }

    public function init()
    {
        $this->_session->step = self::STEP_CREATE;
        $this->_setActive();
    }

    public function setProposalId($id)
    {
        $this->_session->proposalId = $id;
        foreach($this->_wizardSteps as &$step) {
            $step['params'] = '/proposalId/' . $id;
        }
    }

    public function setCurrent($step)
    {
        $this->_session->step = $step;
        $this->_setActive();
    }

    public function getSteps()
    {
        if (!empty($this->_session->step)) {
            $this->_setActive();
        }

        if (!empty($this->_session->proposalId)) {
            try {
                $pr = new Proposal_Service_Proposal();
                $proposalRow = $pr->find($this->_session->proposalId);

                $this->_wizardSteps[self::STEP_PERSONAL_INTRO]['enabled'] = $proposalRow->getThemeRow()->includePersonalIntroduction == 1;
                $this->_wizardSteps[self::STEP_PRICE]['enabled'] = $proposalRow->getThemeRow()->includePrice == 1;
            } catch(Exception $e) {
                $proposalRow = $pr->createRow();

                $this->_wizardSteps[self::STEP_PERSONAL_INTRO]['enabled'] = false;
                $this->_wizardSteps[self::STEP_PRICE]['enabled'] = false;
            }
        } else {
            try {
                $companyRow = $this->_account->getCompanyRow();
                $companyProposalThemeRow = $companyRow->getProposalThemeRow();
                $this->_wizardSteps[self::STEP_PERSONAL_INTRO]['enabled'] = $companyProposalThemeRow->includePersonalIntroduction == 1;
                $this->_wizardSteps[self::STEP_PRICE]['enabled'] = $companyProposalThemeRow->includePrice == 1;
            } catch (\Exception $e) {

            }
        }
        return $this->_wizardSteps;
    }

    public function isLast()
    {
        return $this->_session->step == self::STEP_SUMMARY;
    }

    public function getNextStepUrl()
    {

        if ($this->isLast()) {
            $url = $this->_wizardSteps[self::STEP_SUMMARY]['redirect'] . $this->_wizardSteps[self::STEP_SUMMARY]['params'];
        } else {
            $next = $this->_session->step + 1;
            for ($i = $next; $i <= self::STEP_SUMMARY; $i++)
            {
                if ($this->_wizardSteps[$i]['enabled'])
                {
                    $url = $this->_wizardSteps[$i]['url'];
                    break;
                }
            }
        }
        return $url;
    }

    public function getPrevStepUrl()
    {
        $url = $this->_wizardSteps[self::STEP_CREATE]['url'];
        if (!empty($this->_session->step) && $this->_session->step - 1) {
            $prev = $this->_session->step - 1;
            for ($i = $prev; $i >= self::STEP_CREATE; $i--) {
                if ($this->_wizardSteps[$i]['enabled']) {
                    $url = $this->_wizardSteps[$i]['url'];
                    break;
                }
            }
        }

        return $url;
    }

    public function getFirstStepUrl()
    {
        $this->_session->step = self::STEP_CREATE;
        $this->_setActive();

        return $this->_wizardSteps[$this->_session->step]['url'];
    }

    public function getStepCreate()
    {
        return self::STEP_CREATE;
    }

    public function getStepRelation()
    {
        return self::STEP_RELATION;
    }

    public function getStepBlocks()
    {
        return self::STEP_BLOCKS;
    }

    public function getStepPersonalInto()
    {
        return self::STEP_PERSONAL_INTRO;
    }

    public function getStepPrice()
    {
        return self::STEP_PRICE;
    }

    public function getStepSummary()
    {
        return self::STEP_SUMMARY;
    }
}
