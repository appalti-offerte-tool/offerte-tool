<?php

// Prince - PHP interface
// Copyright 2005-2008 YesLogic Pty. Ltd.
// http://www.princexml.com

class Proposal_Service_Prince
{
    private $exePath;
    private $styleSheets = '';
    private $isHTML = true;
    private $baseURL = '';
    private $doXInclude = true;
    private $httpUser = '';
    private $httpPassword = '';
    private $logFile = '';
    private $embedFonts = true;
    private $compress = true;
    private $encrypt = false;
    private $encryptInfo = '';
    private $allowJS = true;
    private $debug = false;

    /**
     * @var Proposal_Service_Section
     */
    private $_sectionService;

    private $_path;
    private $_proposalDirName;
    private $_templatesPath;
    private $_outputPath;
    private $_msgs = array ();
    private $_errors = array ();
    private $_warnings = array ();

    /**
     * @var Proposal_Model_ProposalVersion
     */
    private $_version;

    public function __construct(\Proposal_Model_Proposal $proposalRow)
    {
        $system = false !== stripos(PHP_OS, 'win') ? 'win/engine/bin/' : '';
        if (empty($system)) {
            if (PHP_INT_MAX == 2147483647) {
                $system = '../linux-x32/bin/';
            }
        }

        $this->exePath = APPLICATION_PATH . '/library/prince/bin/' . $system . 'prince';

        $this->debug = strcasecmp(APPLICATION_ENV, 'development') === 0;

        $this->_proposalRow = $proposalRow;
        $this->_companyRow = $this->_proposalRow->getCompanyRow();

        $this->_sectionService = new \Proposal_Service_Section();
        $this->_path = realpath(__DIR__ .'/../data/tmp');
        $this->logFile = $this->_path . '/log.txt';
        $this->_templatesPath = realpath(APPLICATION_PATH . '/data/public-templates');
        $prefix = sprintf('%d-%d-%s-', $this->_companyRow->getId(), $this->_proposalRow->getId(), date('Ymd'));
        $this->_proposalDirName = uniqid($prefix);

        $this->_createProposalDir();
    }

    protected function _createProposalDir()
    {
        // create temp path
        $this->_path = $this->_path . DIRECTORY_SEPARATOR . $this->_proposalDirName;
        if (!mkdir($this->_path, 0777, true))
        {
            throw new OSDN_Exception("Creating temporary environment'".__DIR__.'/../data/tmp'."' failed.");
        }
        // Sometimes on unix mkdir does not set right permissions
        if (!chmod($this->_path, 0777))
        {
            throw new OSDN_Exception('Unable to set permissions for temporary environment');
        }
        $this->_path = realpath($this->_path);
        if (false === $this->_path) {
            throw new OSDN_Exception('Unable to create temporary environment');
        }

        // create output path
        $this->_outputPath = $this->_path . DIRECTORY_SEPARATOR . $this->_proposalRow->luid;
        if (!is_dir($this->_outputPath) && !mkdir($this->_outputPath, 0777, true))
        {
            throw new OSDN_Exception('Unable to create output folder.');
        }
        if (!chmod($this->_outputPath, 0777))
        {
            throw new OSDN_Exception('Unable to set permissions output folder.');
        }
        restore_error_handler();
    }

    protected function _moveToLastVersionDir()
    {
        $dir = dir($this->_path);
        while (false !== ($node = $dir->read())) {
            if (($node == '.') || ($node == '..')) {
                continue;
            }

            $filePath = $this->_path . DIRECTORY_SEPARATOR . $node;
            if (is_dir($filePath)) {
                rmdir($filePath);
            } else {
                unlink($filePath);
            }
        }

        rmdir($this->_path);

        $this->_proposalDirName = $this->_version->dirName;
        $this->_path = realpath(__DIR__ . '/../data/tmp') . DIRECTORY_SEPARATOR . $this->_version->dirName;
        $this->_outputPath = $this->_path . DIRECTORY_SEPARATOR . $this->_proposalRow->luid;
    }

    // Add a CSS style sheet that will be applied to each document.
    // cssPath: The filename of the CSS style sheet.
    public function addStyleSheet($cssPath)
    {
        $this->styleSheets .= '-s "' . $cssPath . '" ';
    }

    // Clear all of the CSS style sheets.
    public function clearStyleSheets()
    {
        $this->styleSheets = '';
    }

    // Specify whether documents should be parsed as HTML or XML/XHTML.
    // html: True if all documents should be treated as HTML.
    public function setHTML($html)
    {
        $this->isHTML = $html;
    }

    // Specify a file that Prince should use to log error/warning messages.
    // logFile: The filename that Prince should use to log error/warning
    //        messages, or '' to disable logging.
    public function setLog($logFile)
    {
        $this->logFile = $logFile;
    }

    // Specify the base URL of the input document.
    // baseURL: The base URL or path of the input document, or ''.
    public function setBaseURL($baseURL)
    {
        $this->baseURL = $baseURL;
    }

    // Specify whether XML Inclusions (XInclude) processing should be applied
    // to input documents. XInclude processing will be performed by default
    // unless explicitly disabled.
    // xinclude: False to disable XInclude processing.
    public function setXInclude($xinclude)
    {
        $this->doXInclude = $xinclude;
    }

    // Specify a username to use when fetching remote resources over HTTP.
    // user: The username to use for basic HTTP authentication.
    public function setHttpUser($user)
    {
        $this->httpUser = $user;
    }

    // Specify a password to use when fetching remote resources over HTTP.
    // password: The password to use for basic HTTP authentication.
    public function setHttpPassword($password)
    {
        $this->httpPassword = $password;
    }

    // Specify whether fonts should be embedded in the output PDF file. Fonts
    // will be embedded by default unless explicitly disabled.
    // embedFonts: False to disable PDF font embedding.
    public function setEmbedFonts($embedFonts)
    {
        $this->embedFonts = $embedFonts;
    }

    // Specify whether compression should be applied to the output PDF file.
    // Compression will be applied by default unless explicitly disabled.
    // compress: False to disable PDF compression.
    public function setCompress($compress)
    {
        $this->compress = $compress;
    }

    // Specify whether encryption should be applied to the output PDF file.
    // Encryption will not be applied by default unless explicitly enabled.
    // encrypt: True to enable PDF encryption.
    public function setEncrypt($encrypt)
    {
        $this->encrypt = $encrypt;
    }

    // Set the parameters used for PDF encryption. Calling this method will
    // also enable PDF encryption, equivalent to calling setEncrypt(true).
    // keyBits: The size of the encryption key in bits (must be 40 or 128).
    // userPassword: The user password for the PDF file.
    // ownerPassword: The owner password for the PDF file.
    // disallowPrint: True to disallow printing of the PDF file.
    // disallowModify: True to disallow modification of the PDF file.
    // disallowCopy: True to disallow copying from the PDF file.
    // disallowAnnotate: True to disallow annotation of the PDF file.
    public function setEncryptInfo($keyBits,
                                   $userPassword,
                                   $ownerPassword,
                                   $disallowPrint = false,
                                   $disallowModify = false,
                                   $disallowCopy = false,
                                   $disallowAnnotate = false)
    {
        if ($keyBits != 40 && $keyBits != 128) {
            throw new Exception("Invalid value for keyBits: $keyBits" .
                " (must be 40 or 128)");
        }

        $this->encrypt = true;

        $this->encryptInfo =
            ' --key-bits ' . $keyBits .
                ' --user-password="' . $userPassword .
                '" --owner-password="' . $ownerPassword . '" ';

        if ($disallowPrint) {
            $this->encryptInfo .= '--disallow-print ';
        }

        if ($disallowModify) {
            $this->encryptInfo .= '--disallow-modify ';
        }

        if ($disallowCopy) {
            $this->encryptInfo .= '--disallow-copy ';
        }

        if ($disallowAnnotate) {
            $this->encryptInfo .= '--disallow-annotate ';
        }
    }

    // Convert an XML or HTML file to a PDF file.
    // The name of the output PDF file will be the same as the name of the
    // input file but with an extension of ".pdf".
    // xmlPath: The filename of the input XML or HTML document.
    // msgs: An optional array in which to return error and warning messages.
    // Returns true if a PDF file was generated successfully.
    public function convert_file($xmlPath, &$msgs = array ())
    {
        $pathAndArgs = $this->getCommandLine();
        $pathAndArgs .= '"' . $xmlPath . '"';

        return $this->convert_internal_file_to_file($pathAndArgs, $msgs);
    }

    protected function _messages2html($messages = array (), $title = 'Errors')
    {
        $html = array ();

        if (!empty($messages)) {
            $html = array (
                '<h3>' . $title . '</h3>',
                '<ol>'
            );
            foreach ($messages as $msg) {
                $html[] = "<li style='margin-bottom: 3px'>$msg[2]<br />$msg[1]</li>";
            }
            $html[] = '</ol>';
        }

        return $html;
    }

    protected function _sendErrors()
    {
        if (!empty($this->_errors) || !empty($this->_warnings)) {

            $mail = new Zend_Mail('UTF-8');
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
            $mailConfig = $config->resources->mail;
            $mail->setFrom($mailConfig->defaultFrom->email);
            $mail->addTo($mailConfig->errorTo->email, $mailConfig->errorTo->name);
            $mail->setSubject('Proposal tool princexml problem report');

            $account = Zend_Auth::getInstance()->getIdentity();
            $head = array (
                '<b>Host:</b> ' . $_SERVER['HTTP_HOST'] . '<br />',
                '<b>Company name:</b> ' . $this->_companyRow->getFullname() . '<br />',
                '<b>Company ID:</b> ' . $this->_companyRow->getId() . '<br />',
                '<b>Proposal ID:</b> ' . $this->_proposalRow->getId() . '<br />',
                '<b>Generated at:</b> ' . date('H:i m-d-Y') . '<br />',
                '<b>Generated by:</b> ' . $account->getFullname() . ' (' . $account->getRoleName() . ')<br />',
            );

            $errors = $this->_messages2html($this->_errors);
            $warnings = $this->_messages2html($this->_warnings, 'Warnings');
            $mail->setBodyHtml(implode('', array_merge($head, $errors, $warnings)));

            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $bootstrap->bootstrap('mail');
            $transport = $bootstrap->getResource('mail');

            $mail->send($transport);
        }
    }

    protected function _collectErrors()
    {
        if ($this->debug && !empty($this->_msgs)) {
            foreach ($this->_msgs as $msg) {
                // $msg[0] = 'err' | 'wrn' | 'inf' | 'dbg'
                // $msg[1] = filename / line number
                // $msg[2] = message text, trailing newline stripped
                switch ($msg[0]) {
                    case 'err':
                        $this->_errors[] = $msg;
                    case 'wrn':
                        $this->_warnings[] = $msg;
                    default:
                        continue;
                }
            }
        }
    }

    protected function _html2pdf($files)
    {
        if (!is_array($files)) {
            $files = array ($files);
        }

        $pdfFiles = array ();

        foreach ($files as $fileName) {

            if (!empty($fileName)) {
                $pdfName = $fileName['pdf'];
                $pdfPath = $this->_outputPath . DIRECTORY_SEPARATOR . $pdfName;
                if ($this->convert_file_to_file($fileName['html'], $pdfPath, $this->_msgs)) {
                    /**
                     * Check if escapeshellcmd changed pdf file name.
                     * If so then rename it to original.
                     * Example: "Offerte inhoud Turien & Co.pdf" is changed to "Offerte inhoud Turien \& Co.pdf"
                     */
                    $escapedPath = escapeshellcmd($pdfPath);
                    if ($pdfPath !== $escapedPath) {
                        rename($escapedPath, $pdfPath);
                    }

                    $pdfFiles[] = $pdfPath;
                }

                $this->_collectErrors();
            }
        }

        $this->_sendErrors();

        return $pdfFiles;
    }

    public function generate($regionId = NULL, $preview = true)
    {
        $result = $this->_prepareHtml($regionId);

        if ($result['regenerate']) {
            $files = $this->_html2pdf($result['files']);
        } else {
            $this->_moveToLastVersionDir();
            $files = array ();
            foreach ($result['files'] as $fileNames) {
                $path = sprintf('%s/%s/%s', $this->_version->dirName, $this->_proposalRow->luid, $fileNames['pdf']);
                $files[] = $path;
            }
        }

        if ($preview && !empty($files)) {
            $this->preview($files);
        }

        return !empty($files) ? $this->_outputPath : false;
    }

    protected function _zip($name)
    {
        $zip = new ZipArchive();
        $zipPath = $this->_path . DIRECTORY_SEPARATOR . $name;
        $res = $zip->open($zipPath, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        $source = $this->_outputPath . DIRECTORY_SEPARATOR;

        if ($res !== true) {
            require_once 'Zend/Filter/Exception.php';
            throw new Zend_Filter_Exception($this->_errorString($res));
        }

        $dir = dir($source);
        while (false !== ($node = $dir->read())) {
            if (($node == '.') || ($node == '..')) {
                continue;
            }
            $filePath = $source . $node;
            if (is_file($filePath) && 'pdf' === strtolower(pathinfo($filePath, PATHINFO_EXTENSION))) {
                $files[] = $node;
            }
        }

        foreach ($files as $file) {
            $zip->addFile($source . $file, $file);
            if ($res !== true) {
                require_once 'Zend/Filter/Exception.php';
                throw new Zend_Filter_Exception($this->_errorString($res));
            }
        }

        return $zip->close();
    }

    protected function _getValidFileName($name)
    {
        $pattern = '/[\\/?%*:|"<>\.]/';
        return preg_replace($pattern, '_', $name);
    }

    public function preview($files)
    {

        /*TODO: CREATE ERROR PAGE*/
        setcookie('fileDownload', 'true', NULL, '/');
        header('Content-Transfer-Encoding: binary');
        header('Pragma: no-cache');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        if (count($files) > 1) {
            $clientCompanyRow = $this->_proposalRow->getClientCompanyRow();
            $companyName = $this->_getValidFileName($clientCompanyRow->getFullName());
            $zipname = sprintf('%s-%s.%s', $companyName, $this->_proposalRow->luid, 'zip');
            $fileToRead = $this->_path . DIRECTORY_SEPARATOR . $zipname;
            $this->_zip($zipname);

            header('Content-disposition: attachment; filename="' . $zipname . '"');
            header('Content-type: application/zip');
        } else {


            $fileToRead = $files[0];
            $fileName = pathinfo($fileToRead, PATHINFO_FILENAME);
            $fileToRead = explode("/", $fileToRead);


            $fileToRead = $this->_path . DIRECTORY_SEPARATOR . $fileToRead [count($fileToRead) - 2] . DIRECTORY_SEPARATOR . $fileToRead [count($fileToRead) - 1];
            header('Content-disposition: attachment; filename="' . $fileName . '"');
            header('Content-type: application/pdf');
        }

        if (file_exists($fileToRead)) {
            header('Content-Length: ' . filesize($fileToRead));
            readfile($fileToRead);
        } else {
            throw new OSDN_Exception('Error generating pdf.');
        }
    }

    private function _prepareHtml($regionId = NULL)
    {
        $files = array ();
        $versionData = array (
            'dirName'           => $this->_proposalDirName,
            'proposalBlocks'    => $this->_proposalRow->fetchAllBlockDefinitionsSerialized(),
            'html'              => array ()
        );

        foreach ($this->_sectionService->fetchAllCollapsedByRegion($regionId) as $k => $region) {
            $page = new \Proposal_Service_PrincePage($this->_proposalRow, $region);
            $page->setOutputPath($this->_path);
            $result = $page->getFile();
            $files[] = $result['fileNames'];

            if (!empty($result['fileContent'])) {
                $versionData['html'][$k] = array (
                    'regionId'  => $k,
                    'html'      => $result['fileContent'],
                    'hash'      => md5($result['fileContent'])
                );
            }
        }

        $proposalVersionSrv = new \Proposal_Service_ProposalVersion();
        $this->_version = $this->_proposalRow->fetchLastVersion();

        if (NULL !== $this->_version) {
            $diff = $proposalVersionSrv->compare($this->_version, $versionData);
            $createVersion = $diff['total'] > 0;
        } else {
            $createVersion = true;
        }

        if ($createVersion) {
            $this->_version = $proposalVersionSrv->create($this->_proposalRow, $versionData);
        }

        return array (
            'files'      => $files,
            'regenerate' => $createVersion
        );
    }

    // Convert an XML or HTML file to a PDF file.
    // xmlPath: The filename of the input XML or HTML document.
    // pdfPath: The filename of the output PDF file.
    // msgs: An optional array in which to return error and warning messages.
    // Returns true if a PDF file was generated successfully.
    public function convert_file_to_file($xmlPath, $pdfPath, &$msgs = array ())
    {
        $pathAndArgs = $this->getCommandLine();
        $pathAndArgs .= '"' . $xmlPath . '" "' . $pdfPath . '"';

        return $this->convert_internal_file_to_file($pathAndArgs, $msgs);
    }

    // Convert an XML or HTML string to a PDF file, which will be passed
    // through to the output of the current PHP page.
    // xmlString: A string containing an XML or HTML document.
    // Returns true if a PDF file was generated successfully.
    public function convert_string_to_passthru($xmlString)
    {
        $pathAndArgs = $this->getCommandLine();
        $pathAndArgs .= '--silent -';

        return $this->convert_internal_string_to_passthru($pathAndArgs, $xmlString);
    }

    // Convert an XML or HTML string to a PDF file.
    // xmlString: A string containing an XML or HTML document.
    // pdfPath: The filename of the output PDF file.
    // msgs: An optional array in which to return error and warning messages.
    // Returns true if a PDF file was generated successfully.
    public function convert_string_to_file($xmlString, $pdfPath, &$msgs = array ())
    {
        $pathAndArgs = $this->getCommandLine();
        $pathAndArgs .= ' - -o "' . $pdfPath . '"';

        return $this->convert_internal_string_to_file($pathAndArgs, $xmlString, $msgs);
    }

    // Old name for backwards compatibility
    public function convert1($xmlPath, &$msgs = array ())
    {
        return $this->convert_file($xmlPath, $msgs);
    }

    // Old name for backwards compatibility
    public function convert2($xmlPath, $pdfPath, &$msgs = array ())
    {
        return $this->convert_file_to_file($xmlPath, $pdfPath, $msgs);
    }

    // Old name for backwards compatibility
    public function convert3($xmlString)
    {
        return $this->convert_string_to_passthru($xmlString);
    }

    private function getCommandLine()
    {
        $cmdline = $this->exePath . ' --server ' . $this->styleSheets;

        if ($this->isHTML) {
            $cmdline .= '--input=html ';
        }

        if ($this->allowJS) {
            $cmdline .= '--javascript ';
        }

        if ($this->baseURL != '') {
            $cmdline .= '--baseurl="' . $this->baseURL . '" ';
        }

        if ($this->doXInclude == false) {
            $cmdline .= '--no-xinclude ';
        }

        if ($this->httpUser != '') {
            $cmdline .= '--http-user="' . $this->httpUser . '" ';
        }

        if ($this->httpPassword != '') {
            $cmdline .= '--http-password="' . $this->httpPassword . '" ';
        }

        if ($this->logFile != '') {
            $cmdline .= '--log="' . $this->logFile . '" ';
        }

        if ($this->embedFonts == false) {
            $cmdline .= '--no-embed-fonts ';
        }

        if ($this->compress == false) {
            $cmdline .= '--no-compress ';
        }

        if ($this->encrypt) {
            $cmdline .= '--encrypt ' . $this->encryptInfo;
        }

        if ($this->debug) {
            $cmdline .= '--debug ';
        }

        return $cmdline;
    }

    private function convert_internal_file_to_file($pathAndArgs, &$msgs)
    {
        $descriptorspec = array (
            0 => array ("pipe", "r"),
            1 => array ("pipe", "w"),
            2 => array ("pipe", "w")
        );

        $process = proc_open(escapeshellcmd($pathAndArgs), $descriptorspec, $pipes);

        if (is_resource($process)) {
            $result = $this->readMessages($pipes[2], $msgs);

            fclose($pipes[0]);
            fclose($pipes[1]);
            fclose($pipes[2]);

            proc_close($process);

            return ($result == 'success');
        } else {
            throw new Exception("Failed to execute $pathAndArgs");
        }
    }

    private function convert_internal_string_to_file($pathAndArgs, $xmlString, &$msgs)
    {
        $descriptorspec = array (
            0 => array ("pipe", "r"),
            1 => array ("pipe", "w"),
            2 => array ("pipe", "w")
        );

        $process = proc_open(escapeshellcmd($pathAndArgs), $descriptorspec, $pipes);

        if (is_resource($process)) {
            fwrite($pipes[0], $xmlString);
            fclose($pipes[0]);
            fclose($pipes[1]);

            $result = $this->readMessages($pipes[2], $msgs);

            fclose($pipes[2]);

            proc_close($process);

            return ($result == 'success');
        } else {
            throw new Exception("Failed to execute $pathAndArgs");
        }
    }

    private function convert_internal_string_to_passthru($pathAndArgs, $xmlString)
    {
        $descriptorspec = array (
            0 => array ("pipe", "r"),
            1 => array ("pipe", "w"),
            2 => array ("pipe", "w")
        );

        $process = proc_open(escapeshellcmd($pathAndArgs), $descriptorspec, $pipes);

        if (is_resource($process)) {
            fwrite($pipes[0], $xmlString);
            fclose($pipes[0]);
            fpassthru($pipes[1]);
            fclose($pipes[1]);

            $result = $this->readMessages($pipes[2], $msgs);

            fclose($pipes[2]);

            proc_close($process);

            return ($result == 'success');
        } else {
            throw new Exception("Failed to execute $pathAndArgs");
        }
    }

    private function readMessages($pipe, &$msgs)
    {
        while (!feof($pipe)) {
            $line = fgets($pipe);

            if ($line != false) {
                $msgtag = substr($line, 0, 4);
                $msgbody = rtrim(substr($line, 4));

                if ($msgtag == 'fin|') {
                    return $msgbody;
                } else {
                    if ($msgtag == 'msg|') {
                        $msg = explode('|', $msgbody, 4);

                        // $msg[0] = 'err' | 'wrn' | 'inf'
                        // $msg[1] = filename / line number
                        // $msg[2] = message text, trailing newline stripped

                        $msgs[] = $msg;
                    } else {
                        // ignore other messages
                    }
                }
            }
        }

        return '';
    }
}

