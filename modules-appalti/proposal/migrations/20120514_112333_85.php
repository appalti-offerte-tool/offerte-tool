<?php

class Proposal_Migration_20120514_112333_85 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->dropColumn('proposalTheme', 'showBTW');

        $this->createColumn('proposalTheme', 'templateIntroduction', self::TYPE_VARCHAR, 50, null, false);
        $this->createColumn('proposalTheme', 'templateCover', self::TYPE_VARCHAR, 50, null, false);
        $this->createColumn('proposalTheme', 'templateContent', self::TYPE_VARCHAR, 50, null, false);
        $this->createColumn('proposalTheme', 'templateAttachment', self::TYPE_VARCHAR, 50, null, false);
        $this->createColumn('proposalTheme', 'templateInBetween', self::TYPE_VARCHAR, 50, null, false);
        $this->createColumn('proposalTheme', 'showBtw', self::TYPE_INT, 1, '1', true);
    }

    public function down()
    {
        $this->dropColumn('proposalTheme', 'templateIntroduction');
        $this->dropColumn('proposalTheme', 'templateCover');
        $this->dropColumn('proposalTheme', 'templateContent');
        $this->dropColumn('proposalTheme', 'templateAttachment');
        $this->dropColumn('proposalTheme', 'templateInBetween');
        $this->dropColumn('proposalTheme', 'numeration');
        $this->dropColumn('proposalTheme', 'daysValid');
        $this->dropColumn('proposalTheme', 'showBtw');
    }
}