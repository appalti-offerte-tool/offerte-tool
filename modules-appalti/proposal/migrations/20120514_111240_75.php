<?php

class Proposal_Migration_20120514_111240_75 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createColumn('proposalTheme', 'companyId', self::TYPE_INT, 11, null, true);
        $this->createIndex('proposalTheme', array('companyId'), 'IX_companyId');
        $this->createForeignKey('proposalTheme', array('companyId'), 'company', array('id'), 'FK_companyId');
    }

    public function down()
    {
        $this->dropColumn('proposalTheme', 'companyId');
    }
}