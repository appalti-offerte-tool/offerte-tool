<?php

class Proposal_Migration_20120515_110907_41 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createColumn('proposalTheme', 'btw', self::TYPE_INT, 11, null, false);
        $this->createColumn('proposalTheme', 'includeTimeCharge', self::TYPE_INT, 1, '0', false);
        $this->createColumn('proposalTheme', 'timeChargeText', self::TYPE_TEXT);
        $this->createColumn('proposalTheme', 'includeShippingCost', self::TYPE_INT, 1, '0', false);
        $this->createColumn('proposalTheme', 'shippingCostText', self::TYPE_TEXT);
        $this->createColumn('proposalTheme', 'includeTravelingExpenses', self::TYPE_INT, 1, '0', false);
        $this->createColumn('proposalTheme', 'includeDiscount', self::TYPE_INT, 1, '0', false);
        $this->createColumn('proposalTheme', 'discountText', self::TYPE_TEXT);
        $this->createColumn('proposalTheme', 'includeDelivery', self::TYPE_INT, 1, '0', false);
        $this->createColumn('proposalTheme', 'deliveryText', self::TYPE_TEXT);
        $this->createColumn('proposalTheme', 'delayedPayment', self::TYPE_INT, 1, '0', false);
        $this->createColumn('proposalTheme', 'includeOtherDetails', self::TYPE_INT, 1, '0', false);
        $this->createColumn('proposalTheme', 'includeDiscountPerOffer', self::TYPE_INT, 1, '0', false);
    }

    public function down()
    {
        $this->dropColumn('proposalTheme', 'btw');
        $this->dropColumn('proposalTheme', 'includeTimeCharge');
        $this->dropColumn('proposalTheme', 'timeChargeText');
        $this->dropColumn('proposalTheme', 'includeShippingCost');
        $this->dropColumn('proposalTheme', 'shippingCostText');
        $this->dropColumn('proposalTheme', 'includeTravelingExpenses');
        $this->dropColumn('proposalTheme', 'includeDiscount');
        $this->dropColumn('proposalTheme', 'discountText');
        $this->dropColumn('proposalTheme', 'includeDelivery');
        $this->dropColumn('proposalTheme', 'deliveryText');
        $this->dropColumn('proposalTheme', 'delayedPayment');
        $this->dropColumn('proposalTheme', 'includeOtherDetails');
        $this->dropColumn('proposalTheme', 'includeDiscountPerOfferText');
    }
}
