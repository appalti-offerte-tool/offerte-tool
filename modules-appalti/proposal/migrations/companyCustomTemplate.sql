CREATE TABLE `proposalCustomTemplateSettings` (
	`id` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`proposalCustomTemplateId` INT( 11 ) UNSIGNED NOT NULL ,
	`marginTop` INT( 11 ) UNSIGNED NOT NULL DEFAULT '10',
	`marginRight` INT( 11 ) UNSIGNED NOT NULL DEFAULT '10',
	`marginBottom` INT( 11 ) UNSIGNED NOT NULL DEFAULT '10',
	`marginLeft` INT( 11 ) UNSIGNED NOT NULL DEFAULT '10',
	`clientLogoHeader` INT( 1 ) UNSIGNED NOT NULL DEFAULT '0',
	`clientLogoHeaderTop` INT( 11 ) NOT NULL DEFAULT '0' ,
	`clientLogoHeaderLeft` INT( 11 ) NOT NULL DEFAULT '0' ,
	`clientLogoHeaderHeight` INT( 11 ) NOT NULL DEFAULT '0' ,
	`clientLogoHeaderWidth` INT( 11 ) NOT NULL DEFAULT '0' ,
	`clientLogoFooter` INT( 1 ) UNSIGNED NOT NULL DEFAULT '0',
	`clientLogoFooterTop` INT( 11 ) NOT NULL  DEFAULT '0',
	`clientLogoFooterLeft` INT( 11 ) NOT NULL  DEFAULT '0',
	`clientLogoFooterHeight` INT( 11 ) NOT NULL DEFAULT '0' ,
	`clientLogoFooterWidth` INT( 11 ) NOT NULL DEFAULT '0' ,
	`scope` ENUM( 'common', 'introduction', 'content', 'attachment' ) NOT NULL DEFAULT 'common',
	INDEX ( `proposalCustomTemplateId` )
) ENGINE = InnoDB;

ALTER TABLE `proposalCustomTemplateSettings` ADD FOREIGN KEY ( `proposalCustomTemplateId` ) REFERENCES `proposalCustomTemplate` (
`id`
) ON DELETE CASCADE ;

CREATE TABLE `proposalCustomTemplate` (
	`id` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`companyId` INT( 11 ) UNSIGNED NOT NULL ,
	`useDifferentBackgrounds` INT( 1 ) UNSIGNED NOT NULL DEFAULT '0',
	`landscape` INT( 1 ) UNSIGNED NOT NULL DEFAULT '0',
INDEX ( `companyId` )
) ENGINE = InnoDB;

ALTER TABLE `proposalCustomTemplate` ADD FOREIGN KEY ( `companyId` ) REFERENCES `company` ( `id` ) ON DELETE CASCADE ;

ALTER TABLE `proposalTheme` ADD `useCustomTemplate` INT( 1 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `tableHeadTextColor` ;