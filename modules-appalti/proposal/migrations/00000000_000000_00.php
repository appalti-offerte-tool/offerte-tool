<?php

class Proposal_Migration_00000000_000000_00 extends \Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('proposal');
        $this->createColumn('proposal', 'accountId', self::TYPE_INT, 11, null, false);
        $this->createIndex('proposal', 'accountId');
        $this->createForeignKey('proposal', array('accountId'), 'account', array('id'), 'FK_accountId');

        /**
         * ParentId is using for versioning
         */
        $this->createColumn('proposal', 'parentId', self::TYPE_INT, 11, null, false);
        $this->createIndex('proposal', array('parentId'), 'IX_parentId');
        $this->createForeignKey('proposal', array('parentId'), 'proposal', array('id'), 'FK_parentId');
        $this->createColumn('proposal', 'luid', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposal', 'approvedAccountId', self::TYPE_INT, 11, null, false);
        $this->createIndex('proposal', 'approvedAccountId');
        $this->createForeignKey('proposal', array('approvedAccountId'), 'account', array('id'), 'FK_approvedAccountId');
        $this->createColumn('proposal', 'amount', self::TYPE_FLOAT, 9, '0', true);
        $this->createColumn('proposal', 'chanceOfSuccess', self::TYPE_INT, 11);
        $this->createColumn('proposal', 'contactMomentDatetime', self::TYPE_DATETIME);
        $this->createColumn('proposal', 'discount', self::TYPE_FLOAT, 9, '0', true);
        $this->createColumn('proposal', 'version', self::TYPE_INT, 11, '1');
        $this->createColumn('proposal', 'createdDatetime', self::TYPE_DATETIME, null, null, true);
        $this->createColumn('proposal', 'modifiedDatetime', self::TYPE_DATETIME);

        $service = new Proposal_Service_Proposal();
        $service->create(array(
            'accountId'         => 1,
            'luid'              => date('YmdHis'),
            'approvedAccountId' => 1,
            'amount'            => 1001,
            'chanceOfSuccess'   => 90,
            'discount'          => 5
        ));

        $this->createTable('proposalCompany');
        $this->createColumn('proposalCompany', 'proposalId', self::TYPE_INT, 11, null, false);
        $this->createIndex('proposalCompany', 'proposalId');
        $this->createForeignKey('proposalCompany', array('proposalId'), 'proposal', array('id'), 'FK_proposalId');
        $this->createColumn('proposalCompany', 'companyId', self::TYPE_INT, 11, null, false);
        $this->createIndex('proposalCompany', 'companyId');
        $this->createForeignKey('proposalCompany', array('companyId'), 'company', array('id'), 'FK_companyId');
        $this->createColumn('proposalCompany', 'contactPersonId', self::TYPE_INT, 11, null, false);
        /**
         * @todo Not clear how to connect contact person...
         * Contact person is allowed to login?
         */

        $this->createTable('proposalTheme');
        $this->createColumn('proposalTheme', 'proposalId', self::TYPE_INT, 11, null, false);

        /**
         * @FIXME We should think how to define selection custom template (like index, content) from
         * different theme. So, on each section should be two fields?
         */
//        $this->createColumn('proposalTheme', 'templateIntroLetter', self::TYPE_VARCHAR, 50);
//        $this->createColumn('proposalTheme', 'templateFirstPage', self::TYPE_VARCHAR, 50);
//        $this->createColumn('proposalTheme', 'templateProposal', self::TYPE_VARCHAR, 50);
//        $this->createColumn('proposalTheme', 'templateAppendix', self::TYPE_VARCHAR, 50);
//        $this->createColumn('proposalTheme', 'templateInbetweenPage', self::TYPE_VARCHAR, 50);

        $this->createColumn('proposalTheme', 'introductionLetter', self::TYPE_TEXT);
        $this->createColumn('proposalTheme', 'includePrice', self::TYPE_INT, 2);
        $this->createColumn('proposalTheme', 'includePersonalIntroduction', self::TYPE_INT, 2);
        $this->createColumn('proposalTheme', 'personalIntroduction', self::TYPE_TEXT);
        $this->createColumn('proposalTheme', 'numeration', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'daysValid', self::TYPE_INT, 11);
        $this->createColumn('proposalTheme', 'showBTW', self::TYPE_INT, 11);
        $this->createColumn('proposalTheme', 'lettertype', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'fontSize', self::TYPE_INT, 11);
        $this->createColumn('proposalTheme', 'textColor', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'h1Color', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'h2Color', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'h3Color', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'h4Color', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'h5Color', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'listColor', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'linkColor', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'footerBgColor', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'footerAccentColor', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'tableBorderColor', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'tableHeadBgColor', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalTheme', 'tableHeadTextColor', self::TYPE_VARCHAR, 50);
        $this->createIndex('proposalTheme', 'proposalId');
        $this->createForeignKey('proposalTheme', array('proposalId'), 'proposal', array('id'), 'FK_proposalId');

        $this->createTable('proposalBlockDefinition');
        $this->createColumn('proposalBlockDefinition', 'proposalId', self::TYPE_INT, 11, null, false);
        $this->createIndex('proposalBlockDefinition', 'proposalId', 'IX_proposalId');
        $this->createForeignKey('proposalBlockDefinition', array('proposalId'), 'proposal', array('id'), 'FK_proposalId');

        /**
         * At the moment we create a little mock, and define
         * that library contains only in the `libraryProposalBlock` table
         */
        $this->createColumn('proposalBlockDefinition', 'libraryProposalBlockId', self::TYPE_INT, 11, null, false);
        $this->createIndex('proposalBlockDefinition', 'libraryProposalBlockId', 'IX_libraryProposalBlockId');
        $this->createForeignKey('proposalBlockDefinition', array('libraryProposalBlockId'), 'libraryProposalBlock', array('id'), 'FK_libraryProposalBlockId');

        $this->createColumn('proposalBlockDefinition', 'sectionId', self::TYPE_INT, 11, null, false);
        $this->createColumn('proposalBlockDefinition', 'name', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalBlockDefinition', 'title', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalBlockDefinition', 'position', self::TYPE_INT, 11, null, false);
        $this->createColumn('proposalBlockDefinition', 'fitable', self::TYPE_INT, 1, '0', false);
        $this->createColumn('proposalBlockDefinition', 'kind', self::TYPE_VARCHAR, 50);
        $this->createColumn('proposalBlockDefinition', 'metadata', self::TYPE_LONGTEXT);
        $this->createColumn('proposalBlockDefinition', 'imageName', self::TYPE_VARCHAR, 50);

        $this->createTable('proposalCompanyOffer');
        $this->createColumn('proposalCompanyOffer', 'companyOfferId', self::TYPE_INT, 11, null, false);
        $this->createIndex('proposalCompanyOffer', 'companyOfferId');
        $this->createForeignKey('proposalCompanyOffer', array('companyOfferId'), 'companyOffer', array('id'), 'FK_companyOfferId');
        $this->createColumn('proposalCompanyOffer', 'proposalId', self::TYPE_INT, 11, null, false);
        $this->createIndex('proposalCompanyOffer', 'proposalId');
        $this->createForeignKey('proposalCompanyOffer', array('proposalId'), 'proposal', array('id'), 'FK_proposalId');
    }

    public function down()
    {
        $this->dropTable('proposalCompanyOffer');
        $this->dropTable('proposalBlockDefinition');
        $this->dropTable('proposalTheme');
        $this->dropTable('proposalCompany');
        $this->dropTable('proposal');
    }
}