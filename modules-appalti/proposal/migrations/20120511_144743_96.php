<?php

class Proposal_Migration_20120511_144743_96 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createColumn('proposalBlockDefinition', 'inBetween', self::TYPE_INT, 11, '0', false);
    }

    public function down()
    {
        $this->dropColumn('proposalBlockDefinition', 'inBetween');
    }
}