<?php

final class Proposal_Misc_Navigation_ProposalDetailPageMvc extends Zend_Navigation_Page_Mvc
{
    protected $_isCompleted = false;

    /**
     * (non-PHPdoc)
     * @see Zend_Navigation_Page::getLabel()
     */
    public function getLabel()
    {
        if (true !== $this->_isCompleted) {
            $this->_label = 'Detailinformatie offerte';

            $ticketId = (int) Zend_Controller_Front::getInstance()->getRequest()->getParam('ticketId');
            if (!empty($ticketId)) {
                $supportTicketService = new SupportTicket_Service_SupportTicket();
                $supportTicketRow = $supportTicketService->find($ticketId);

                $this->_label .= ' ' . $supportTicketRow->getCompanyRow()->getName();
                $this->_label .= ' #' . $supportTicketRow->getId();
            }

            $this->_isCompleted = true;
        }

        return parent::getLabel();
    }
}