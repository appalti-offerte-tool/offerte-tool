<?php

final class Proposal_Misc_Email_Proposal extends Configuration_Service_ConfigurationAbstract implements Notification_Instance_NotifiableInterface
{
    protected $_identity = 'proposal.proposal';

    protected $_fields = array(
        'receipt',
        'subject', 'body'
    );

    protected $_mui = true;

    protected $_keywords = array(
        'subject'  => 'E-mail subject',
        'comment'  => 'E-mail body',
        'name'     => 'Sender name',
        'email'    => 'Sender e-mail',

        'timestamp'=> 'Timestamp'
    );

    protected $_serialized = array('receipt', 'replyto');

    public function __construct()
    {
        parent::__construct();
        $defaultFrom = Zend_Mail::getDefaultFrom();
        if (is_array($defaultFrom)) {
            $this->_fields['receipt']['default'] = $this->_toValue($defaultFrom, 'receipt');
        }
    }

    public function notificate(array $data)
    {


        try {
            $proposalRow = $data['proposalRow'];

            if ( ! $proposalRow instanceof \Proposal_Model_Proposal ) {
                throw new OSDN_Exception('Unable to send. Proposal failure');
            }

            $clientCompanyRow = $proposalRow->getClientCompanyRow();
            $clientContactPersonRow = $proposalRow->getClientContactPersonRow();
            $authAccountRow =  Zend_Auth::getInstance()->getIdentity();
            $dt = new \DateTime();
            $params = array(
                'timestamp' => $dt->format('Y-m-d H:i:s'),
                'email'     => $clientContactPersonRow->email,
                'name'      => $clientContactPersonRow->getFullname(),
                'subject'   => $proposalRow->luid,
                'comment'   => $proposalRow->luid
            );

            $instance = $this->toInstance();

            $mail = new Zend_Mail('UTF-8');
            $mail->setFrom($authAccountRow->getEmail(), $authAccountRow->getFullname());
            $mail->addTo($clientContactPersonRow->email, $clientContactPersonRow->lastname);

            $mail->setSubject($instance->getSubject($params));
            $mail->setBodyHtml($instance->getBody($params));

            if (!empty($data['path']) && $handle = opendir($data['path'])) {
                while (false !== ($destination = readdir($handle))) {
                    $filePath = $data['path'] . '/' . $destination;
                    if (\file_exists($filePath) && \is_file($filePath)) {
                        $at = $mail->createAttachment(file_get_contents($filePath, false));
                        $at->filename = $destination;
                    }
                }
                closedir($handle);
            }
            $result = $mail->send();
        } catch(\Zend_Mail_Transport_Exception $e) {
            throw new OSDN_Exception('Unable to send. Transport error.', null, $e);
        } catch(\Exception $e) {
            throw new OSDN_Exception('Unable to send message', null, $e);
        }
        return (boolean) $result;
    }


    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_toValue()
     */
    protected function _toValue($value, $field)
    {
        if (in_array($field, $this->_serialized)) {
            return serialize($value);
        }

        return parent::_toValue($value, $field);
    }

    /**
     * (non-PHPdoc)
     * @see Configuration_Service_ConfigurationAbstract::_fromValue()
     */
    protected function _fromValue($value, $field)
    {
        if (in_array($field, $this->_serialized)) {
            return unserialize($value);
        }

        return parent::_fromValue($value, $field);
    }
}