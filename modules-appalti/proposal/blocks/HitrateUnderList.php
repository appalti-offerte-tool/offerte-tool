<?php

final class Proposal_Block_HitrateUnderList extends Application_Block_Abstract
{
    protected $_itemsPerPage = 10;

    protected $_hitrate = 30;

    protected function _toTitle()
    {
        return $this->view->translate('Almost expired proposals');
    }

    protected function _toHtml()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage($this->_itemsPerPage);

        $proposalSrv = new \Proposal_Service_Proposal();

        $params = $this->getRequest()->getParams();

        if (empty($params['hitrate'])) {
            $params['hitrate'] = $this->_hitrate;
        }

        $response = $proposalSrv->fetchHitrateCount($params);
        $count = count($response->getRowset());
        $response = $proposalSrv->fetchHitrateUnder($params);

        $this->view->rowset = $response->getRowset();

        $pagination->setTotalCount($count);
        $this->view->pagination = $pagination;
        $this->blockId = $this->getId();
        $this->view->listOnly = $this->_getParam('listOnly');
        $this->view->submittedData = $params;
    }
}