<?php

final class Proposal_Block_ProposalList extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return 'Alle uitgebrachte offertes';
    }

    protected function _toHtml()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage($this->_getParam('itemsPerPage', 10));

        $args = $this->getInvokeArgs();

        $params = $this->getRequest()->getParams();
        $params['filter'] = $this->getFilterParams();
        $cookie = isset($_COOKIE["proposalStatusFilter"]) ? $_COOKIE["proposalStatusFilter"] : null;
        if (!empty($params['statusFilter']) || isset($params['statusFilter'])) {
            setcookie('proposalStatusFilter', $params['statusFilter'], time()+60*60*24*3, '/');
        } elseif($cookie) {
            $params['statusFilter'] = $cookie;
        }
        $this->view->statusFilter = !empty($params['statusFilter']) ? $params['statusFilter'] : null;

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $proposalSrv = new \Proposal_Service_Proposal();
        if ($accountRow->isAdmin()) {
            $response = $proposalSrv->fetchAllByCompanyId($params);
            $this->view->return = $args['return'];
        }
        else {
            $response = $proposalSrv->fetchAll($params);
            $companyRow = $accountRow->getCompanyRow();
            $this->view->company    = $companyRow;
        }
        $rowset = $response->getRowset();
        $proposalId = $this->_getParam('proposalId');

        if ($proposalId) {
            $r = null;
            foreach ($rowset as $k => &$row) {
                if ($row->getId() === $proposalId) {
                    $r = $row;
                    unset($rowset[$k]);
                    $rowset = array_merge(array($r), $rowset);
                    break;
                }
            }
        }

        $this->view->accountRow = $accountRow;
        $this->view->proposalId = $proposalId;
        $this->view->listOnly   = $this->_getParam('listOnly');
        $this->view->blockId    = $this->getId();
        $this->view->proposals  = $rowset;

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }
}