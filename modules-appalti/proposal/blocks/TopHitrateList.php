<?php

final class Proposal_Block_TopHitrateList extends Application_Block_Abstract
{
    protected $_topLimit = 5;

    /**
     *
     * @var type $_top = true - direction for selection $_topLimit greatest values of hitrate
     */
    protected $_top = false;

    public function init()
    {
        parent::init();

        $this->_top = $this->_getParam('top', $this->_top);
        $this->_topLimit = $this->_getParam('topLimit', $this->_topLimit);
    }

    protected function _toTitle()
    {
        return $this->view->translate('Almost expired proposals');
    }

    protected function _toHtml()
    {
        $proposalSrv = new \Proposal_Service_Proposal();
        $response = $proposalSrv->fetchTopHitrate(array(
             'direction' => $this->_top ? 'desc' : 'asc',
             'topLimit' => $this->_topLimit
        ));

        $this->view->rowset = $response->getRowset();
    }
}