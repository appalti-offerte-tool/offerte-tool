<?php

final class Proposal_Block_RecentProposalsList extends Application_Block_Abstract
{
    const LIMIT = 5;

    protected function _toTitle()
    {
        return 'Laatste offertes';
    }

    protected function _toHtml()
    {
        $proposalSrv = new \Proposal_Service_Proposal();
        $response = $proposalSrv->fetchAllLast(self::LIMIT);
        $this->view->limit = self::LIMIT;
        $this->view->proposals = $response->getRowset();
    }
}