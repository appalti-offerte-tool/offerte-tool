<?php

final class Proposal_Block_AlmostExpiredList extends Application_Block_Abstract
{
    protected $_itemsPerPage = 10;

    protected function _toTitle()
    {
        return $this->view->translate('Almost expired proposals');
    }

    protected function _toHtml()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage($this->_itemsPerPage);

        $proposalSrv = new \Proposal_Service_Proposal();
        $response = $proposalSrv->fetchAlmostExpired($this->getRequest()->getParams());
        $this->view->rowset = $response->getRowset();

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
        $this->view->blockId = $this->getId();
        $this->view->listOnly = $this->_getParam('listOnly');
        $this->view->submittedData = $this->getRequest()->getParams();

    }
}