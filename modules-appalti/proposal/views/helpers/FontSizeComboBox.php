<?php

class Proposal_View_Helper_FontSizeComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    /**
     * The constructor
     *
     * Initialize options for combo field
     */
    public function __construct()
    {
        $this->_options = array(
            'Selecteer' => array()
        );
        for ($i = 9; $i < 41; $i++) {
           $this->_options['Selecteer'][$i] = $i;
        }
    }

    public function fontSizeComboBox()
    {
        return $this;
    }

    public function toField($name, $value = 11, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }

    public function toLabel($value)
    {
        return $this->_options[$value];
    }

    public function toArray()
    {
        return $this->_options;
    }
}