<?php

class Proposal_View_Helper_FontFamilyComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_defaultOptions = array(
        'Arial'             => 'Arial',
        'Comic Sans MS'     => 'Comic Sans MS',
        'Courier New'       => 'Courier New',
        'Georgia'           => 'Georgia',
        'Impact'            => 'Impact',
        'Times New Roman'   => 'Times New Roman',
        'Verdana'           => 'Verdana'
    );

    protected $_options = array();

    protected $_fontsXML = '/data/public-templates/common/fonts/all.xml';

    /**
     * The constructor
     *
     * Initialize options for combo field
     */
    public function __construct()
    {
        if (file_exists(APPLICATION_PATH . $this->_fontsXML)) {
            $root = simplexml_load_file(APPLICATION_PATH . $this->_fontsXML);

            foreach ($root->font as $font) {
                $this->_options[(string)$font['value']] = (string)$font['title'];
            }
            asort($this->_options);
        } else {
            $this->_options = $this->_defaultOptions;
        }
    }

    public function fontFamilyComboBox()
    {
        return $this;
    }

    public function toField($name, $value = 'Arial', $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }

    public function toLabel($value)
    {
        return $this->_options[$value];
    }

    public function toArray()
    {
        return $this->_options;
    }
}