Price = function (conf) {
    $.extend(this, conf || {});
    this.init();
}

Price.prototype = function () {

    var protect = {
            subtotalPrice: null,
            subTotalPrice1: null
        },
        zeroVal = '0.00',
        visibleInp = {},
        hiddenInp = {},
        api = {
            cacheInputs: function (f) {
                var vi = visibleInp, hi = hiddenInp;

                hi.timeChargesPercentage = $('input[name=timeChargesPercentage]', f);
                vi.timeChargesPercentage = $('#timeChargesPercentage');

                hi.timeChargesValue = $('input[name=timeChargesValue]', f);
                vi.timeChargesValue = $('#timeChargesValue');

                hi.extraCostType = $('input[name=extraCostType]', f);
                vi.extraCostType = $('#extraCostType');

                hi.extraCostPercentageOrValue = $('input[name=extraCostPercentageOrValue]', f);
                vi.extraCostPercentageOrValue = $('#extraCostPercentageOrValue');

                hi.extraCostValue = $('input[name=extraCostValue]', f);
                vi.extraCostValue = $('#extraCostValue');

                hi.shippingValue = $('input[name=shippingValue]', f);
                vi.shippingValue = $('#shippingValue');

                hi.shippingValueDisplay = $('input[name=shippingValueDisplay]', f);
                vi.shippingValueDisplay = $('#shippingValueDisplay');

                hi.travelingExpensesValue = $('input[name=travelingExpensesValue]', f);
                vi.travelingExpensesValue = $('#travelingExpensesValue');

                hi.travelingExpensesValueDisplay = $('input[name=travelingExpensesValueDisplay]', f);
                vi.travelingExpensesValueDisplay = $('#travelingExpensesValueDisplay');

                hi.includeDiscount = $('input[name=includeDiscount]', f);
                vi.includeDiscount = $('#includeDiscount');

                hi.discountPercentage = $('input[name=discountPercentage]', f);
                vi.discountPercentage = $('#discountPercentage');

                hi.discountValue = $('input[name=discountValue]', f);
                vi.discountValue = $('#discountValue');

                hi.subtotalPrice = $('input[name=subtotalPrice]', f);
                vi.subtotalPrice = $('#subtotalPrice');

                hi.totalPrice = $('input[name=totalPrice]', f);
                vi.totalPrice = $('#totalPrice');
            },

            fl: function(sel) {
                var v = $(sel).val() ? $(sel).val() : 0;
                v = parseFloat(v);
                return isNaN(v) ? 0 : v;
            }

        }

    return {

        saving: false,

        totalLoadUrl: '/proposal/offer/get-total-cost',

        submitUrl: '/proposal/create/save-price/format/json',

        proposalId: null,

        proposalOffer: null,

        init: function () {
            var me = this, vi = visibleInp, hi = hiddenInp;
            me.form = $('#proposal-price');
            api.cacheInputs(me.form);

            vi.timeChargesPercentage.val(hi.timeChargesPercentage.val());
            vi.timeChargesValue.val(number_format(hi.timeChargesValue.val()));
            vi.extraCostType.val(hi.extraCostType.val());

            var isPercentage = hi.extraCostType.val() == 'percentage';
            $('#extraCostEuro').toggleClass('invisible', isPercentage);
            $('#extraCostPercentage').toggle(isPercentage);

            vi.extraCostPercentageOrValue.val(hi.extraCostPercentageOrValue.val());
            vi.extraCostValue.val(hi.extraCostValue.val());
            vi.shippingValue.val(hi.shippingValue.val());
            vi.shippingValueDisplay.html(number_format(hi.shippingValueDisplay.val()));
            vi.travelingExpensesValue.val(hi.travelingExpensesValue.val());
            vi.travelingExpensesValueDisplay.html(number_format(hi.travelingExpensesValueDisplay.val()));

            if (hi.includeDiscount.val() == 1) {
                vi.includeDiscount.attr('checked', 'checked');
            }

            vi.discountPercentage.val(api.fl(hi.discountPercentage).toFixed(2));
            vi.discountValue.html(number_format(api.fl(hi.discountValue).toFixed(2)));

            me.form
            .on('change', 'input:text, input:checkbox, select', function () {
                if (   !$('input[name="proposal:grant-discount"]')
                    || !parseInt($('input[name="proposal:grant-discount"]').val()) ){
                    if ($(this).is(':checkbox')) {
                        $(this).attr('checked', false);
                    }
                    return;
                }
                if (   !$('input[name="proposal:price-sheet"]')
                    || !parseInt($('input[name="proposal:price-sheet"]').val())){
                    if ($(this).is(':checkbox')) {
                        $(this).attr('checked', !$(this).attr('checked'));
                    }
                    return;
                }
                var el = $(this);
                if (el.is(':checkbox')) {
                    el.data('show') && $(el.data('show')).toggle(this.checked);
                    el.data('hide') && $(el.data('hide')).toggle(!this.checked);
                }
                me.calculateAll.call(me);
            })
            .on('keypress', 'input:text', function (e) {
                return AppaltiLayout.filterDigitsOnly(e)
            })
            .on('keyup', 'input[data-max]', function () {
                var el = $(this), maxVal = el.data('max');
                if (maxVal && parseInt(el.val()) > maxVal) {
                    el.val(100).trigger('change');
                }
            })
            .on('change', '#includeDiscountPerOffer', function (e) {
                if (   !$('input[name="proposal:grant-discount"]')
                    || !parseInt($('input[name="proposal:grant-discount"]').val()) ){
                    if ($(this).is(':checkbox')) {
                        $(this).attr('checked', false);
                    }
                    return;
                }
//                $(this).attr('checked', !$(this).attr('checked'));
                $.when(me.submitForm(true)).done(function () {
                    me.proposalOffer.doLoadList();
                });
//                return false;
            });
        },

        submitForm: function (mask) {
            var me = this;
            return $.ajax({
                url: me.submitUrl,
                type: 'post',
                data: me.form.serialize(),
                dataType: 'json',
                mask:!!mask,
                maskMsg: 'Opnieuw te berekenen en opslaan van gegevens. Een ogenblik geduld.',
                beforeSend: function () {
                    if (!me.saving) {
                        me.saving = true;
                    } else {
                        return false;
                    }
                },
                success: function (data) {
                    if (data.success) {
//                    console.log(data);
                    }
                },
                complete: function () {
                    me.saving = false;
                }
            });
        },

        updateSubtotal: function () {
            $.ajax({
                url: this.totalLoadUrl,
                data: {
                    proposalId: this.proposalId,
                    format: 'json'
                },
                dataType: 'json',
                success:$.proxy(function (response) {
                    if (response.success) {
                        this.changeSubtotal(parseFloat(response.price));
                    }
                }, this)
            });
        },

        changeSubtotal: function (v) {
            protect.subtotalPrice = v;
            visibleInp.subtotalPrice.html(number_format(protect.subtotalPrice));
            hiddenInp.subtotalPrice.val(protect.subtotalPrice.toFixed(2));
            this.calculateAll.call(this);
        },

        timeChargesCalculate: function () {
            var show = $('#includeTimeCharge').prop('checked');
            $('#timeChargeRow').toggle(show);

            if (show) {
                var timeChargesPercentage = api.fl(visibleInp.timeChargesPercentage),
                    timeChargesValue = timeChargesPercentage * protect.subtotalPrice / 100;
            }

            hiddenInp.timeChargesPercentage.val(show ? timeChargesPercentage.toFixed(2) : zeroVal);
            hiddenInp.timeChargesValue.val(show ? timeChargesValue.toFixed(2) : zeroVal);
            visibleInp.timeChargesValue.html(show ? number_format(timeChargesValue) : zeroVal);
        },

        extraCostCalculate: function () {
            var show = $('#includeExtraCost').prop('checked');
            $('#extraCostRow').toggle(show);

            hiddenInp.extraCostType.val(show ? visibleInp.extraCostType.val() : zeroVal);
            hiddenInp.extraCostPercentageOrValue.val(show ? api.fl(visibleInp.extraCostPercentageOrValue).toFixed(2) : zeroVal);

            if (show) {
                var isPercentage = hiddenInp.extraCostType.val() == 'percentage';
                $('#extraCostEuro').toggleClass('invisible', isPercentage);
                $('#extraCostPercentage').toggle(isPercentage);

                var value = api.fl(visibleInp.extraCostPercentageOrValue);
                value *= isPercentage ? protect.subtotalPrice / 100 : 1;
                visibleInp.extraCostValue.html(number_format(value));
            }

            hiddenInp.extraCostValue.val(show ? value.toFixed(2) : zeroVal);
        },

        shippingCalculate: function () {
            var show = $('#includeShippingCost').prop('checked');
            $('#shippingCostRow').toggle(show);

            if (show) {
                var value = api.fl(visibleInp.shippingValue);
                visibleInp.shippingValueDisplay.html(number_format(value));
            }

            hiddenInp.shippingValue.val(show ? value.toFixed(2) : zeroVal);
        },

        travelingExpensesCalculate: function () {
            var show = $('#includeTravelingExpenses').prop('checked');
            $('#travelingExpensesRow').toggle(show);

            if (show) {
                var value = api.fl(visibleInp.travelingExpensesValue);
                visibleInp.travelingExpensesValueDisplay.html(number_format(value));
            }

            hiddenInp.travelingExpensesValue.val(show ? value.toFixed(2) : zeroVal);
        },

        discountCalculate: function () {
            var show = visibleInp.includeDiscount.prop('checked'), discountVal = 0;
            $('#discountContainer').toggle(show);

            if (show) {
                var subTotal = this.getSubTotalWithoutDiscount();
                discountVal = api.fl(visibleInp.discountPercentage) / 100 * subTotal;
            }

            hiddenInp.includeDiscount.val(+show);
            visibleInp.discountValue.html(number_format(discountVal));
            hiddenInp.discountPercentage.val(api.fl(visibleInp.discountPercentage).toFixed(2));
            hiddenInp.discountValue.val(discountVal.toFixed(2));
        },

        totalCalculate: function () {
            var val = protect.subTotalPrice1 - api.fl(hiddenInp.discountValue);
            visibleInp.totalPrice.html(number_format(val));
            hiddenInp.totalPrice.val(val);
        },

        getSubTotalWithoutDiscount: function () {
            return api.fl(hiddenInp.subtotalPrice)
                + api.fl(hiddenInp.timeChargesValue)
                + api.fl(hiddenInp.extraCostValue)
                + api.fl(hiddenInp.shippingValue)
                + api.fl(hiddenInp.travelingExpensesValue);
        },

        calculateAll: function () {
            this.timeChargesCalculate();
            this.extraCostCalculate();
            this.shippingCalculate();
            this.travelingExpensesCalculate();
            protect.subTotalPrice1 = this.getSubTotalWithoutDiscount();
            $('#subtotalPrice1').html(number_format(protect.subTotalPrice1));
            this.discountCalculate();
            this.totalCalculate();
            this.submitForm();
        }
    }
}();