;(function($, win) {
    if (!win.AppaltiWizard) {
        win.AppaltiWizard = {};
    }

    var StepOne = function() {
        this.init();
    };

    function updateOptions(el, data) {
        var $options = $();

        for (var i in data) {
            $options = $options.add($('<option/>', {
                value: data[i]['id'],
                text: data[i]['firstname'] + ' ' + data[i]['lastname']
            }));
        }

        el.empty().append($options).prop('disabled', false);

        if (el[0].name === 'clientContactPersonId') {
            step.addClientContactPerson.removeClass('disabled');
        }
    }

    function initMarkup() {
        step.addContactPerson = $('#newContactPerson');
        step.contactPersons = $('select[name="contactPersonId"]');
        step.canApproveContactPersons = $('select[name="approvedAccountId"]');
        step.clientCompany = $('select[name="clientCompanyId"]');
        step.addClientContactPerson = $('#newClientContactPerson');
        step.clientContactPersons = $('select[name="clientContactPersonId"]');
        step.saveStepState = $('#save-step-state');
        step.nextStep = $('#next-step');
        step.form = $('form[name="step-one"]');
        step.selfApproved = $('input:radio[name="self-approved"]');
        step.approveContactPerson = $('#approveContactPerson');
    }

    function getContactPerson(companyId) {
        var url = link('company', 'index', 'fetch-with-contact-person', {format: 'json', companyId: companyId});

        return $.getJSON(url);
    }

    function bindFormSubmitEvent($form, submitCallback, scope) {
        var preloader = $('.ajax-preloader', $form);

        $form.ajaxForm({
            data: win.isIE ? {returnHtml: true} : {},
            beforeSubmit: function() {
                if (!$form.valid()) {
                    return false;
                }
                preloader.addClass('show');
            },
            success: function(response) {
                preloader.removeClass('show');
                response = $.parseJSON(response);
                $.isFunction(submitCallback) && submitCallback.call(scope || this, response);
            }
        });
    }

    function updateContactPersonItems(rowset, createdContactPersonId, updateEl) {
        var options = $(),
            optionParams,
            canApproveOptions = options,
            selectedVal = step.canApproveContactPersons.find(':selected').val();

        for (var i in rowset) {
            optionParams = {
                value: rowset[i].id,
                text: rowset[i].firstname + ' ' + rowset[i].lastname
            };

            options = options.add($('<option />', optionParams));

            if (!updateEl && rowset[i].canApproveProposal) {
                optionParams.value = rowset[i].accountId;
                if (selectedVal === optionParams.value) {
                    optionParams.selected = 'selected';
                }
                canApproveOptions = canApproveOptions.add($('<option/>', optionParams).data('contactPersonId', rowset[i].id));
            }
        }


        if (!updateEl) {
            step.contactPersons.empty().append(options);
            step.canApproveContactPersons.empty().append(canApproveOptions);
        } else {
            options.filter('[value=' + createdContactPersonId + ']').attr('selected', 'selected');
            updateEl.empty().append(options);
        }
    }

    function markErrorFields($form, errors) {
        for (var i in errors) {
            $('input[name="' + errors[i].field + '"]', $form)
            .closest('tr')
            .find('.info-cnt')
            .append('<a class="icon-info-error-small" href="#" title="' + errors[i].message + '">&nbsp;</a>');
        }

        $('.icon-info-error-small').tooltip();
    }

    function getCreateContactPersonForm(url, companyId, updateEl) {
        $.when(getModalForm(url)).done(function() {
            var frm = $('form', step.modalCnt);
            $(frm).attr('action', $(frm).attr('action').replace('/format/json') +'/format/json');
            bindFormSubmitEvent(frm, function(response) {
                if (response.success) {
                    var createdContactPersonId = response.contactPersonId;
                    $.when(getContactPerson(companyId)).done(function(response) {
                        if (response.success) {
                            updateContactPersonItems(response.contactPersonRowset, createdContactPersonId, updateEl);
                            step.clientContactPersons.triggerHandler('change');
                            $('form', step.modalCnt).observable().refresh();
                            step.modalCnt.modal('hide');
                        }
                    });
                } else {
                    markErrorFields(frm, response.messages);
                }
            }, step);

            step.modalCnt.modal('show');
        });
    }

    function initEvents() {
        step.addContactPerson.click(function() {
            getCreateContactPersonForm(this.href, $('input[name="companyId"]').val());
            return false;
        });

        var addrSourceInputs = $('input:text', '.actualAddr'),
            addrTargetInputs = $('input:text', '.postAddr');

        var actualEqualsPostAddr = $('#post-same-as-actual').change(function() {
            if (this.checked) {
                addrSourceInputs.each(function() {
                    $(this).triggerHandler('blur');
                });
                addrTargetInputs.attr('readonly', 'readonly');
            } else {
                addrTargetInputs.removeAttr('readonly');
            }
        });

        addrSourceInputs.blur(function() {
            if (!actualEqualsPostAddr[0].checked) return;

            var name = this.name.replace('actual', 'post');
            step.form.validate().element(addrTargetInputs.filter('[name="' + name + '"]').val(this.value));
        });

        step.addClientContactPerson.click(function() {
            if ($(this).hasClass('btn-disabled')) {
                return false;
            }

            var companyId = step.clientCompany.val(),
                url = this.href + companyId;

            getCreateContactPersonForm(url, companyId, step.clientContactPersons);
            return false;
        });

        $('input[name="chanceOfSuccess"], input[name="amount"]').keypress(function(e) {
            return AppaltiLayout.filterDigitsOnly(e);
        });

        $('input[name="chanceOfSuccess"]').change(function() {
            if (this.value > 100) {
                this.value = 100;
            }
        });

        $('select[data-syncurl]').on('change', function() {
            var el = $(this), url = el.data('syncurl'), selected = $(':selected', el);

                url += this.name === 'approvedAccountId' ? selected.data('contactPersonId') : selected.val();

                if (this.name === 'clientCompanyId') {
                    onChangeFormCombo(el, url);

                    if (el.val() === 'newItem') {
                        step.clientContactPersons
                            .empty()
                            .attr('disabled', 'disabled')
                            .append('<option value="newItem">Nieuwe contactpersoon</option>')
                            .val('newItem');
                            step.clientContactPersons.triggerHandler('change');
                    } else {
                        step.clientContactPersons.removeAttr('disabled').val('');
                    }

                    step.addClientContactPerson.toggleClass('btn-disabled', !el.val() || el.val() === 'newItem');

                    return;
                }

                if (this.name === 'clientContactPersonId') {
                    onChangeFormCombo(el, url);
                    return;
                }

                $.getJSON(url).done(function(data) {
                    var prefix = el.data('prefix'), target, row = data.row;
                    for (var k in row) {
                        target = $(prefix + k);
                        target.is('img') ? target.attr('src', row[k]).removeClass('hidden') : target.html(row[k]);
                    }

                    if (undefined !== data.contactPersonRowset) {
                        updateOptions(step.clientContactPersons, data.contactPersonRowset);
                    }
                });
        });

        step.selfApproved.change(function() {
            var cnt = step.approveContactPerson,
                val = parseInt(this.value);

            if (val) {
                var select = cnt.find('select');
                $('option[value=' + select.data('self') + ']', select).attr('selected', 'selected');
            }

            cnt[val ? 'hide' : 'show']();
        });

        $('.icon-info-small').tooltip( );
    }

    function getModalForm(url) {
        return $.get(url).done(function(data) {
            if (!step.modalCnt) {
                step.modalCnt = $('<div class="modal" data-backdrop="static"></div>').appendTo('body')
                    .on('hide', function() {
                        return !step.modalCnt.find('form').observable().isDirty() ||
                               !confirm('Contactpersoon gegevens wordt niet opgeslagen.\nKeer terug om op te slaan?');
                    })
                    .on('hidden', function() {
                        step.modalCnt.empty();
                    });
            }

            step.modalCnt.html(data);
            AppaltiLayout.initTooltip(step.modalCnt);
        });
    }

    function onChangeFormCombo(combo, url) {
        var prefix = combo.data('prefix');

        if (combo.val() === 'newItem' || !combo.val()) {
            var container = $(prefix + 'input-fields'),
                inputs = $(':input', container).not(combo),
                inp;

            $('img', container).attr('src', '').addClass('hidden');

            inputs.toggleClass('ignore', !combo.val()).each(function() {
                inp = $(this);

                !inp.is(':radio') ? inp.val('') : inp.removeAttr('checked');

                if (combo.val() === 'newItem') {
                    inp.removeAttr('readonly');

                    if (inp.is(':file')) {
                        inp.removeClass('hidden');
                    }

                    if (inp.is(':radio')) {
                        inp.removeAttr('disabled');
                    }

                    $('.post-same-as-actual').show();

                    if (inp.is('select')) {
                        inp
                        .removeAttr('disabled')
                        .find(':selected')
                        .removeAttr('selected');
                    }
                } else {
                    if (inp.is(':file')) {
                        inp.addClass('hidden');
                    }

                    if (inp.is(':radio')) {
                        inp.attr('disabled', 'disabled');
                    }

                    inp.attr('readonly', 'readonly');
                }
            });

            combo[0].name === 'clientCompanyId' && step.clientContactPersons.empty().triggerHandler('change');

        } else {
            var maskEl = combo[0].name === 'clientCompanyId' ? '.companyClientRegion' : '.clientContactPersonRegion';

            $.ajax({
                url: url,
                dataType: 'json',
                mask: true,
                maskTarget: maskEl,
                success: function(data) {
                    var containerEl, el,
                        form = combo.closest('form'),
                        row = data.row;

                    $('.post-same-as-actual').hide();

                    for (var k in row) {
                        containerEl = $(prefix + k);
                        el = $(':input', containerEl);

                        var img = $(prefix + k + ' img');
                        img.length && img.attr('src', row[k]).removeClass('hidden').removeAttr('hidden');

                        if (el.length) {
                            el.addClass('ignore').attr('readonly', 'readonly');
                        } else {
                            continue;
                        }

                        el.is('select') && el.attr('disabled', 'disabled').find('[value="' + row[k] + '"').attr('selected', 'selected');

                        if (el.is(':file')) {
                            el.addClass('hidden');
                            continue;
                        }

                        if (el.is(':radio') || el.is(':checkbox')) {
                            el.attr('disabled', 'disabled')
                               .removeAttr('checked')
                               .filter('[value="' + row[k] + '"]')
                               .attr('checked', 'checked');

                            continue;
                        }

                        el.val(row[k]);
                        !el.hasClass('ignore') && form.validate().element(el);
                    }

                    if (undefined !== data.contactPersonRowset) {
                        updateOptions(step.clientContactPersons, data.contactPersonRowset);
                        step.clientContactPersons.triggerHandler('change');
                    }

                    form.validate().element(combo);
                }
            });
        }
    }

    StepOne.prototype = {
        constructor: StepOne,

        cache: [],

        init: function() {
            $(function() {
                initMarkup();
                initEvents();
            });
        }
    };

    win.AppaltiWizard.StepOne = new StepOne();

    var step = win.AppaltiWizard.StepOne;

})(jQuery, window);