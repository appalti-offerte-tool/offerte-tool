$(function () {
    var $form = $('#proposalSummaryForm');

    function getSupportTicketForm(proposalId) {
        AppaltiLayout.mask('Ophalen support ticket vorm. Een ogenblik geduld.');
        var stf = new SupportTicketForm();

        stf.showForm(true, '/support-ticket/client/proposal', { proposalId: proposalId })
           .done(AppaltiLayout.unmask);
    }

    $form.submit(function() {
        if (parseInt($('[name="createSupportTicket"]:checked', $form).val())) {
            $.ajax({
                url: $form[0].action + '/format/json',
                data: $form.serialize(),
                type: 'post',
                dataType: 'json',
                mask: true,
                maskMsg: 'Opslaan van gegevens. Een ogenblik geduld.',
                success: function(response) {
                    if(response.success) {
                        getSupportTicketForm(response.proposalId);
                    } else {
                        alert(response.messages[0].message);
                    }
                }
            });

            return false;
        } else {
            return true;
        }
    });
});