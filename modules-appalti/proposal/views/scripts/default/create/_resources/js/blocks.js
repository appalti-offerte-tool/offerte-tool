;(function($, win) {
    if (!win.AppaltiWizard) {
        win.AppaltiWizard = {};
    }

    var ProposalBlocks = function() {
        $(function() {
            step.lib = $('.library');
            step.sections = $('ol.proposal-section-items');

            refreshPosArrowsState();
            step.defaultSectionId = step.sections.filter('.proposal-section-default').data('section');
            step.previewPDF = $('a.preview-pdf');
            step.previewMenuToggle = $('[data-toggle="dropdown"]');
            step.save = $('#save-step-state');
            step.nextStep = $('#save-step-state-and-go-next');
            step.priceBlock = $('#priceBlock');
            step.extraParamElements = $('[name="includeTOC"], #priceBlock');
            getAvailableBlocks();
            initEvents();
        });
    };

    ProposalBlocks.prototype = {
        constructor: ProposalBlocks,

        url: {
            createDefinition:  '/proposal/create/create-definition/',
            updateDefinition:  '/proposal/create/update-definition/' ,
            deleteDefinition:  '/proposal/create/delete-definition/',
            previewDefinition: '/proposal/create/preview-definition/',
            extraParams:       '/proposal/create/save-state/'
        }
    };

    function refreshPosArrowsState(section) {
        var upCls = 'icon-move-up',
            downCls = 'icon-move-down',
            upDisabledCls = 'icon-move-up-disabled',
            downDisabledCls = 'icon-move-down-disabled';

        !section && (section = step.sections);

        section.each(function() {
            $('.' + upDisabledCls + ',.' + downDisabledCls, this).removeClass(upDisabledCls + ' ' + downDisabledCls);
            $('li.first, li.last', this).removeClass('first last');

            $('li:first', this)
                .addClass('first')
                .find('.' + upCls)
                .addClass(upDisabledCls);
            $('li:last', this)
                .addClass('last')
                .find('.' + downCls)
                .addClass(downDisabledCls);
        });
    }

    function createProposalItem($el) {
        var el = $el[0],
            $li = $('<li />', {
                "text": $.trim($el.parent('label').text()),
                "id": el.value
            }),
            actions = ['<i class="sortable-item-actions">'],
            title = $el.data('kind') === 'file' ? 'Schermvullend' : 'Start op nieuwe pagina',
            selected = $el.data('fitable') ? 'checked="checked"' : '';

        actions.push(
            $el.data('inbetween') ? '<i class="icon-info-small" title="Schermvullend">&nbsp;</i>' :
                                    '<input type="checkbox" class="setFullscreen" title="' + title + '"' + selected + '/>'
        );

        actions.push('<a class="icon-zoom" data-kind="' + $el.data('kind') + '" href="' + step.url.previewDefinition + 'format/' + $el.data('kind') + '/" title="Voorbeeld"></a>');

        if ($('input[name="proposal:template"]') && parseInt($('input[name="proposal:template"]').val()))
        {
            actions.push(
                "text" === $el.data('kind') ? '<a class="icon-edit" data-kind="text" href="' + step.url.updateDefinition + '" title="Veranderen"></a>':
                                              '<i class="icon-empty"></i>'
            );
        }

        actions.push('<i class="icon-delete" title="Verwijderen"></i>');
        actions.push('</i>');
        actions.push('<i class="sortable-pos">');
        actions.push('<i class="icon-move-down"/>');
        actions.push('<i class="icon-move-up"/>');
        actions.push('</i>');

        $li.append(actions.join('\r\n')).find('.sortable-item-actions').children().tooltip();
        return $li;
    }

    function addBlock(blockId, li, checkbox) {
        var section = li.parent();
        return $.ajax({
            url: step.url.createDefinition,
            data: {
                blockId: blockId,
                sectionId: section.data('section'),
                position: li.index(),
                format: 'json'
            },
            type: 'post',
            beforeSend: function() {
                toggleSection(section);
                checkbox.attr('disabled', 'disabled');
            },
            success: function(response) {
                if (response.success) {
                    var id = parseInt(response.row['id']);
                    li.attr('id', 'definition-' + id)
                      .data('blockId', blockId)
                      .data('blockDefinitionId', id);

                    $('.icon-zoom, .icon-edit', li).each(function() {
                        this.href += 'blockDefinitionId/' + id;
                    });

                    checkbox.data('blockDefinitionId', id);
                } else {
                    li.remove();
                    checkbox.removeAttr('checked');
                    alert(response.error);
                }
            },
            complete: function() {
                toggleSection(section);
                checkbox.removeAttr('disabled');
            }
        });
    }

    function addSelectedToProposal(el) {
        var sectionId = el.data('section'),
            li = createProposalItem(el),
            section = $('#section-' + sectionId).append(li);

        addBlock(el.val(), li, el);
        refreshPosArrowsState(section);
    }

    function getModalForm(action) {
        return $.ajax({
            url: action.href,
            mask: true,
            maskMsg: 'Het laden...',
            success: function(data) {
                step.modalCnt = $('<div class="modal"></div>').appendTo('body');

                step.modalCnt
                .html(data)
                .on('hidden', function() {
                    step.modalCnt.remove();
                });
            }
        });
    }

    function getAvailableBlocks() {
        step.availableBlocks = $();
        var blocks;
        $('.root-cat', step.lib).each(function() {
            var section = step.sections.filter('[data-related-section=' + this.id + ']'),
                id = section.data('section') || step.defaultSectionId;

            blocks = $('[type=checkbox]', this).data('section', id);

            step.availableBlocks = step.availableBlocks.add(blocks);
        });
    }

    function removePrice() {
        if (!confirm('Verwijder prijs van voorstel?')) return;

        step.priceBlock.find('.icon-delete').tooltip('hide').end().remove();
        step.priceBlock = null;
        step.removePrice = true;
        saveExtraParams(function() {
            var price = $('.wizard-progress .pricePage'),
                prevStep = price.prev(),
                href = (prevStep.hasClass('wizard-step-active') ? price.next() : prevStep).find('a')[0].href;

            step.nextStep[0].href = href;
            price.remove();
        });
    }

    function removeBlock(id, li, checkbox) {
        var section = li.parent();
        return $.ajax({
            url: step.url.deleteDefinition,
            data: {
                blockDefinitionId: id,
                format: 'json'
            },
            type: 'post',
            beforeSend: function() {
                toggleSection(section);
                li.hide().children().tooltip('hide');
                checkbox.length && checkbox.removeAttr('checked').attr('disabled', 'disabled');
            },
            success: function(response) {
                if (response.success) {
                    checkbox.length && checkbox.removeData('blockDefinitionId').removeAttr('data-block-definition-id');
                    li.remove();
                    refreshPosArrowsState(section);
                } else {
                    li.show();
                    checkbox.length && checkbox.attr('checked', 'checked');
                    alert(response.messages[0].message);
                }
            },
            complete: function() {
                toggleSection(section);
                checkbox.length && checkbox.removeAttr('disabled');
            }
        });
    }

    function removeBlockFromProposal(trigger) {
        if (trigger.closest('li').data('isPrice')) {
            removePrice();
            return;
        }

        if (!confirm('Verwijder blok van het voorstel?')) {
            trigger.is(':checkbox') && (trigger.attr('checked', 'checked'));
            return;
        }

        var id = trigger.is(':checkbox') ? trigger.data('blockDefinitionId') : trigger.closest('li').data('blockDefinitionId'),
            li = $('#definition-' + id),
            checkbox = trigger.is(':checkbox') ? trigger : step.availableBlocks.filter('[value=' + li.data('blockId') + ']');

        removeBlock(id, li, checkbox);
    }

    function updateDefinition(wrap, params) {
        var section = wrap.parent(),
            saveExtra = false,
            data = { format: 'json' };

        if ('positions' === params) {
            data.blockDefinitions = {};
            var li;
            wrap.each(function() {
                li = $(this);

                if (li.data('isPrice')) {
                    saveExtra = true;
                } else {
                    data.blockDefinitions[li.data('blockDefinitionId')] = {
                        sectionId: section.data('section'),
                        position: li.index()
                    };
                }
            });

            if (wrap.length === 1 && saveExtra) {
                saveExtraParams(null, true);
            }
        } else {
            $.extend(data, { blockDefinitionId: wrap.data('blockDefinitionId') }, params);
        }

        return $.ajax({
            url: step.url.updateDefinition,
            data: data,
            type: 'post',
            beforeSend: function() {
                toggleSection(section);
            },
            success: function(response) {
                !response.success && alert(response.messages[0].message);
                saveExtra && saveExtraParams(null, true);
            },
            complete: function() {
                toggleSection(section);
            }
        });
    }

    function collectData() {
        var data = { format: 'json' };

        data.includeTOC = +!step.extraParamElements.filter('[name=includeTOC]').prop('checked');
        data.companyId = document.getElementById('companyId').value;
        data.proposalId = document.getElementById('proposalId').value;

        if (step.priceBlock && step.priceBlock.length) {
            data.pricePosition = step.priceBlock.index();
        } else if (step.removePrice) {
            data.removePrice = true;
            step.removePrice = false;
        }

        return data;
    }

    function saveExtraParams(callback, noMask) {
        return $.ajax({
            url: step.url.extraParams,
            type: 'post',
            data: collectData(),
            dataType: 'json',
            mask: !noMask,
            maskMsg: 'Opslaan blokken en hun posities. Een ogenblik geduld.',
            beforeSend: function() {
                step.extraParamElements.attr('disabled', 'disabled').addClass('disabled');
            },
            success: function(response) {
                if (response.success) {
                    $.isFunction(callback) && callback(response);
                } else {
                    alert(response.messages[0].message);
                }
            },
            complete: function() {
                step.extraParamElements.removeAttr('disabled').removeClass('disabled');
            }
        });
    }

    function toggleSection(section) {
        if (section.hasClass('disabled')) {
            $('.' + section[0].id + '-overlay').remove();
        } else {
            var offset = section.offset();
            $('<div />')
                .addClass(section[0].id + '-overlay')
                .css({
                    background: '#fff',
                    position: 'absolute',
                    filter: 'alpha(opacity=50)',
                    opacity: '.5',
                    top: offset.top,
                    left: offset.left,
                    width: section.width(),
                    height: section.height()
                })
                .appendTo('body');
        }
        section.toggleClass('disabled');
    }

    function updateDefinitionPositions(section) {
        $.when(updateDefinition(section.children(), 'positions')).done(function() {
            refreshPosArrowsState(section);
        });
    }

    function initEvents() {
        step.lib.on('change', '[type=checkbox]', function() {
            var el = $(this);
            el[0].checked ? addSelectedToProposal(el) : removeBlockFromProposal(el);
        });

        step.sections.sortable({
            connectWith: step.sections,
            cursor: 'move',
            placeholder: 'sortable-highlight',
            forcePlaceholderSize: true,
            forceHelperSize: true,
            tolerance: 'pointer',
            opacity: 0.6,
            stop: function(e, ui) {
                ui.item.css('zoom', 0);
                updateDefinitionPositions(ui.item.parent());
            },
            receive: function(e, ui) {
                if (ui.item.hasClass('listSpecific')) {
                    $(ui.sender).sortable('cancel');
                    return;
                }
                ui.sender.children().length && updateDefinitionPositions(ui.sender);
            }

        }).disableSelection();

        $('.proposal-structure').on('click', '[class^="icon-move-"]', function() {

            if (/-disabled/ig.test(this.className)) return;

            var li = $(this).closest('li');

            /-up/ig.test(this.className) ? li.insertBefore(li.prev()) : li.insertAfter(li.next());

            updateDefinitionPositions(li.parent());
        })
        .on('click', '.icon-delete', function() {
            var el = $(this);
            !el.closest('li').hasClass('disabled') && removeBlockFromProposal(el);
        })
        .on('change', '.setFullscreen', function() {
            updateDefinition($(this).closest('li'), { fitable: +this.checked });
        })
        .on('change', '[name=includeTOC]', function() {
            var el = $(this);

            el.attr('disabled', 'disabled').tooltip('hide');

            $.when(saveExtraParams(null, true)).done(function() {
                el.removeAttr('disabled');
            });
        })
        .on('click', '.icon-zoom', function() {
            var el = $(this);
            $.when(getModalForm(this)).done(function() {
                step.modalCnt.toggleClass('fullscreen', el.data('kind') === 'text').modal('show');
            });
            return false;
        })
        .on('click', '.icon-edit', function() {
            $.when(getModalForm(this)).done(function() {
                step.modalCnt.modal('show');
            });
            return false;
        });

        step.previewPDF.click(function() {
            var el = $(this),
                regionId = parseInt(el.attr('href').match(/regionId\/(\d+)/)[1]);

            if (regionId) {
                var section = step.sections.filter('.region-' + regionId);
                if (!section.children().length) {
                    alert('Deze sectie is leeg');
                    return false;
                }
            }

            step.previewMenuToggle.dropdown('toggle');
            win.location.href = this.href;
            return false;
        });

        step.save.click(function() {
            saveExtraParams();
            return false;
        });

        step.nextStep.click(function () {
            saveExtraParams(function(response) {
                if (response.success) {
                    win.location = step.nextStep.attr('href');
                } else {
                    alert(response.messages[0].message);
                }
            });
            return false;
        });
    }

    win.AppaltiWizard.ProposalBlocks = new ProposalBlocks();

    var step = win.AppaltiWizard.ProposalBlocks;
})(jQuery, window);