!function($) {

    var fontTargets = null;

    $(function() {
        $('.icon-colorpicker').ColorPicker({
            onBeforeShow: function () {
                var el = $(this);
                if (!el.data('source')) {
                    el.data('source', el.parent().find('input:text'))
                      .data('target', $(el.data('target')))
                }
                el.ColorPickerSetColor(el.data('source')[0].value);
            },
            onChange: function(hsb, hex, rgb) {
                var el = $(this.data('colorpicker').el);
                el.data('source').val(hex);

                var target = el.data('target'),
                    attr = !target.data('attr') ? 'color' : target.data('attr');

                target.css(attr, '#' + hex);

            },
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).ColorPickerHide();
            }
        });

        var $fontSelect = $('#lettertype').change(function() {
            !fontTargets && (fontTargets = $('.font-target'));
            fontTargets.css('fontFamily', this.value);
        });

        $fontSelect.closest('form').submit(function() {
            $('input:text', this).removeAttr('disabled');
        });

        $('select.font-size')
            .each(function() {
                var el = $(this),
                    optGroup = $('<optgroup label="Standaard" />'),
                    val = Math.ceil(parseInt($(el.data('target')).css('font-size')) * 0.75),
                    $option = el.find('option[value=' + val + ']');

                optGroup.append($option).prependTo(el);
                this.value = val;
            })
            .change(function() {
                $($.data(this, 'target')).css('font-size', this.value + 'pt');
            });

        $('.font-style').click(function() {
            var el = $(this),
                target = $(el.data('target')),
                attr = el.data('attr'),
                inputTarget = $('#' + attr + '-' + target.attr('id'));

            el.toggleClass('checked');
            target.css(attr, el.hasClass('checked') ? el.data('val') : '');
            inputTarget.val(el.hasClass('checked') ? el.data('val') : inputTarget.data('default'));
        });
    });
}(window.jQuery);