;(function($, win) {
    var TemplatesPage = function() {
        this.init();
    }

    function initMarkup() {
        tplPage.$form = $('#templatesForm');
        tplPage.$templates = $('.tpl-thumb');
        initEvents();
    }

    function initEvents() {
        var value = $('input[name=useCustom]:checked').val();
        if (value == 0) {
            $.ajax({
                url: '/proposal/template/use-custom-template',
                type: 'post',
                data: {
                    format: 'json',
                    useCustom: 1,
                    returnUrl: /\/proposal\/template\/(.+)$/i.exec(win.location.pathname)[1] + win.location.hash
                },
                success: function(response) {
                    if (response.success) {
                        undefined !== response.redirect ?
                            win.location = response.redirect :
                            $('.useDefaultTpl, .useCustomTpl', parent).toggleClass('hidden', me[0].value);
                    } else {
                        alert(response.error);
                    }
                }
            });
        }

        $('body')
        .on('click', '.tpl-thumb', function() {
            var $el = $(this);
            !$el.data('selected') ? select($el) : deselect($el);
        })
        .on('click', '.icon-preview-tpl', preview)
        .on('change', 'input[name=useCustom]', function() {
            var me = $(this), parent = me.closest('.accordion-section');

            !parent.length && (parent = me.closest('article'));

            $.ajax({
                url: '/proposal/template/use-custom-template',
                type: 'post',
                data: {
                    format: 'json',
                    useCustom: this.value,
                    returnUrl: /\/proposal\/template\/(.+)$/i.exec(win.location.pathname)[1] + win.location.hash
                },
                mask: true,
                maskTarget: parent,
                success: function(response) {
                    if (response.success) {
                        undefined !== response.redirect ?
                            win.location = response.redirect :
                            $('.useDefaultTpl, .useCustomTpl', parent).toggleClass('hidden', me[0].value);
                    } else {
                        alert(response.error);
                    }
                }
            });
        });


    }

    function deselect($el) {
        $el.parent('li')
            .removeClass('tpl-selected')
            .find('input:hidden')
            .remove();

        $el.data('selected', false);
    }

    function select($el) {
        deselect($el.parents('.tpl-row').find('li.tpl-selected > img'));

        $el.parent('li')
            .addClass('tpl-selected')
            .append($('<input/>', {
                type: 'hidden',
                name: 'template' + $el.data('section').ucFirst(),
                value: $el.data('tpl')
            }));

        $el.data('selected', true);
    }

    function preview() {
        if (!tplPage.$modalCnt) {
            tplPage.$modalCnt = $('<div id="modal-container" class="tpl-preview modal"/>').appendTo('body');
            tplPage.$previewImg = $('<img/>').appendTo(tplPage.$modalCnt);
        }

        tplPage.$previewImg.attr('src', this.href);
        tplPage.$modalCnt.modal('toggle');

        return false;
    }

    TemplatesPage.prototype = {

        constructor: TemplatesPage,

        init: function() {
            $(initMarkup);
        }
    }

    window.TemplatesPage = new TemplatesPage();
    var tplPage = window.TemplatesPage;

})(jQuery, window);