;(function($) {
    var dpi = 96, mmInPx = 25.4 / dpi;

    function _mm2px(val) {
        return Math.round((val / 2) / mmInPx);
    }

    function _px2mm(val) {
        return Math.round((val * 2) * mmInPx);
    }

    var PageSettings = function(page) {
        this.page = page;
        this.el = $('.tpl-settings', page.wrap);

        var el = this.el;
        this.bgForm = $('form.tpl-bg', el);
        this.margins = $('input[name*=tpl-margin-]', el);
        this.pagingParams = $('.pagingParams', el);
        var p = this.pagingParams;
        this.pagingColor = $('input[name=pagingColor]', p);
        this.pagingFontWeight = $('.pagingFontWeight', p);
        this.pagingFontStyle = $('.pagingFontStyle', p);
        this.pagingTextDecoration = $('.pagingTextDecoration', p);
        this.pagingPos = $('input[name=pagingTop], input[name=pagingLeft]', el);
        this.addClientLogo = $('input.addClientLogo', el);
        this.addPaging = $('input.addPaging', el);
        this.syncMarginChange = $('[name=syncMarginChange]', el);
        this.resetMargins = $('.resetMargins', el);
        this.inputs = $(':input', el);


        $.validator.addMethod("imgOrPdf", function(value, element) {
            var pattern = /(\.jpe?g|\.pdf)$/i;
            return !value || pattern.test(value);
        }, "Mogelijke bestandsformaten zijn: jpeg, jpg, pdf.");

        this.initEvents();
        this.restore();
    }

    PageSettings.prototype = {

        restore: function() {
            var me = this;

            me.addClientLogo.filter(':checked').trigger('change', [true]);
            me.addPaging.is(':checked') && me.addPaging.trigger('change', [true]);
            me.margins.each(function() {
                $(this).change();
            });
        },

        toggleDisable: function(disable) {
            disable ? this.inputs.attr('disabled', 'disabled') : this.inputs.removeAttr('disabled');
            $('[type=submit]', this.bgForm).add(this.resetMargins).toggleClass('btn-disabled', disable);
            this.page.el.trigger('disabled', [disable]);
        },

        updatePagingPos: function() {
            var me = this;
            me.pagingPos.each(function() {
                $(this).val($('[name=' + this.name + ']', me.page.form).val());
            })
        },

        setMaxMarginSize: function(margin) {
            var me = this, prop, target = margin ? $(margin) : me.page.margins;

            target.each(function() {
                prop = /(tpl-margin-(t|b))/ig.test(this.className) ? 'Height' : 'Width';
                me.margins.data('max', _px2mm(Math.floor($(this).resizable('option', 'max' + prop))));
            });
        },

        getPagingMaxPropVal: function(el) {
            var method = /(top)$/i.test(el.name) ? 'height' : 'width',
                parentProp = this.page.margins.filter('.tpl-margin-b').find('.tpl-content')[method](),
                pagingProp = this.page.paging['outer' + method.ucFirst()]();

            return _px2mm(parentProp - pagingProp);
        },

        initEvents: function() {
            var me = this,
                KEY_UP = 38,
                KEY_ENTER = 13,
                KEY_DOWN = 40;

            $('.icon-colorpicker', me.el).ColorPicker({
                onBeforeShow: function () {
                    var el = $(this);
                    el.ColorPickerSetColor(me.pagingColor[0].value);
                },
                onChange: function(hsb, hex, rgb) {
                    var el = $(this.data('colorpicker').el);
                    me.pagingColor.val(hex);
                    me.page.el.trigger('pagingParamsChanged');

                },
                onSubmit: function(hsb, hex, rgb, el) {
                    $(el).ColorPickerHide();
                }
            });

            me.page.el
                .on('marginChange', function(e, margin) {
                    var cls, matches, prop, target = margin || me.page.margins;

                    target.each(function() {
                        matches = this.className.match(/(tpl-margin-(t|r|b|l))/ig);
                        if (null !== matches) {
                            cls = matches[0];
                            prop = /(tpl-margin-(t|b))/ig.test(cls) ? 'height' : 'width';
                            me.margins.filter('[name="' + cls + '"]').val(_px2mm($(this)[prop]()));
                        }
                    });
                })
                .on('pagingPosChange', function() {
                    me.updatePagingPos.call(me);
                })
                .on('orientationChange', function() {
                    me.setMaxMarginSize.call(me);
                });

            me.el
                .on('change', 'input[name=syncMarginChange]', function() {
                    if (this.value) {
                        var margins = me.page.margins;

                        switch (this.value.toLowerCase()) {
                            case 'lr':
                                if (this.checked) {
                                    margins
                                        .filter('.tpl-margin-r').data('alsoResize', margins.filter('.tpl-margin-l'))
                                        .end()
                                        .filter('.tpl-margin-l').data('alsoResize', margins.filter('.tpl-margin-r'));
                                } else {
                                    margins.filter('.tpl-margin-r, .tpl-margin-l').removeData('alsoResize');
                                }

                                break;

                            case 'tb':
                                if (this.checked) {
                                    margins
                                        .filter('.tpl-margin-t').data('alsoResize', margins.filter('.tpl-margin-b'))
                                        .end()
                                        .filter('.tpl-margin-b').data('alsoResize', margins.filter('.tpl-margin-t'));
                                } else {
                                    margins.filter('.tpl-margin-t, .tpl-margin-b').removeData('alsoResize');
                                }

                                break;
                        }
                    }
                })
                .on('click', '.font-style', function() {
                    $(this).toggleClass('checked');
                    me.page.el.trigger('pagingParamsChanged');
                })
                .on('click', '.resetMargins', function() {
                    me.page.margins
                        .filter('.tpl-margin-r, .tpl-margin-l').css('width', '')
                        .end()
                        .filter('.tpl-margin-t, .tpl-margin-b').css('height', '')
                        .end()
                        .eq(0)
                        .trigger('resizestop');
                })
                .on('keydown', 'input[name*=tpl-margin-], input[name=pagingTop], input[name=pagingLeft]', function(e) {
                    if (!AppaltiLayout.filterDigitsOnly(e)) {
                        return false;
                    }

                    var el = $(this);
                    switch (e.which) {
                        case KEY_UP:
                            var max = /(top|left)$/i.test(this.name) ?
                                                    me.getPagingMaxPropVal.call(me, this) :
                                                    el.data('max');

                            if (this.value < max) {
                                this.value++;
                                el.change();
                            }

                            break;
                        case KEY_DOWN:
                            if (this.value > 0) {
                                this.value--;
                                el.change();
                            }
                            break;
                        case KEY_ENTER:
                            el.change();
                            break;
                    }
                })
                .on('change', 'input[name*=tpl-margin-]', function() {
                    var margin = me.page.margins.filter('.' + this.name);

                    margin.add(margin.data('alsoResize')).css(
                        /(tpl-margin-(t|b))/ig.test(this.name) ? 'height' : 'width',
                        _mm2px(this.value)
                    ).triggerHandler('resizestop');
                })
                .on('change', 'input[name=pagingTop], input[name=pagingLeft]', function() {
                    var prop = this.name.match(/(top|left)$/i)[0].toLowerCase(),
                        max = me.getPagingMaxPropVal(this),
                        val = this.value > max ? max : this.value;

                    me.page.paging.css(prop, _mm2px(val) + 'px');
                    me.page.onPagingDrag();
                })
                .on('change', '.addClientLogo', function(e, restore) {
                    me.page.toggleLogo(this.value, this.checked, restore);
                })
                .on('change', '.addPaging', function(e, restore) {
                    me.pagingParams.toggleClass('hidden', !this.checked);
                    !this.checked && me.pagingPos.val(0);
                    me.page.togglePaging(this.checked, restore);
                })
                .on('pageBuildComplete', function() {
                    /* fix resizable events conflict
                     * should be triggered after resize event are bind
                     */
                    me.syncMarginChange.filter(':checked').change();
                });

            me.bgForm.validate();
            me.bgForm.on('submit', function() {
                var page = me.page.el;

                if (me.page.disabled) return false;

                me.bgForm.ajaxSubmit({
                    data: {
                        format: 'json',
                        landscape: +me.page.isLandscape()
                    },
                    type: 'post',
                    mask: true,
                    maskTarget: page,
                    beforeSubmit: function() {
                        if (!me.bgForm.valid()) {
                            alert('Mogelijke bestandsformaten zijn: jpeg, jpg, gif, png, pdf.')
                            return false;
                        }

                        me.toggleDisable(true);
                    },
                    success: function(response) {
                        response = $.parseJSON(response);
                        if (response.success) {

                            var img = $('img.bg', page);
                            !img.length && (img = $('<img class="bg" />').prependTo(page));
                            img.attr('src', response.path + '?#dc_' +  (new Date().getTime()));
                        } else {
                            alert(response.error);
                        }
                    },
                    complete: function() {
                        me.toggleDisable(false);
                    }
                });

                return false;
            });



        }
    }

    var TplPage = function(wrap) {
        var me = this;

        me.wrap = $(wrap)
        me.el = $('.tpl-page', wrap);
        me.margins = $('.resizable', me.el);
        me.form = me.el.closest('form');
        me.scope = $('[name="scope"]', me.form).val();
        me.customTemplateId = $('[name="id"]', me.form).val();
        me.tab = $('.nav-tabs').find('a[href=#' + me.wrap[0].id + ']');


        me.deletebkg = $('.deleteBkg', wrap);

//        console.log(me.form);
        me.saved = true;
        me.disabled = false;
        me.paging = null;

        me.settings = new PageSettings(me);
        me.initEvents();
        me.updateMargins();
        me.setMaxMarginSize();
        me.settings.el.trigger('pageBuildComplete');
        me.markSaved(true);
    }

    TplPage.prototype = {
        logoTpl: '<div class="tpl-logo"><i>Logo</i></div>',
        pagingTpl: '<div class="tpl-paging" title="Pagina 1 van N">Pagina 1 van N</div>',

        setMaxMarginSize: function() {
            var me = this, prop;
            me.margins.each(function() {
                prop = /(tpl-margin-(t|b))/ig.test(this.className) ? 'height' : 'width';
                $(this).resizable('option', 'max' + prop.ucFirst(), me.el[prop]() / 2 - 1 );
                me.settings.setMaxMarginSize(this);
            });

            me.markSaved(false);
        },

        markSaved: function(saved) {
            this.saved = saved;
            this.tab && this.tab.css('fontWeight', saved ? 400 : 700);
        },

        getLogoInputByParam: function(logo, param) {
            var selector = [
                '[name=clientLogo',
                /tpl-margin-t/.test(logo.closest('.resizable')[0].className) ? 'Header' : 'Footer',
                param ? param.ucFirst() : '',
                ']'
            ].join('');

            return $(selector, this.form);
        },

        changeOrientation: function() {
            this.el.toggleClass('landscape');
            this.setMaxMarginSize();
            this.fixLogoPos();
            this.el.trigger('orientationChange');
            this.markSaved(false);
        },

        initResizableLogo: function(logo) {
            var me = this;
            logo.resizable({handles: "all"}).on('resizestop', function(e, ui) {
                me.onLogoResize.call(me, ui.element);
            });
        },

        isLandscape: function() {
            return this.el.hasClass('landscape');
        },

        fixLogoPos: function() {
            var parentWidth, offsetLeft,
                el, elWidth,
                me = this;
            $('.tpl-logo', this.el).add(me.paging && me.paging.is(':visible') ? me.paging : null).each(function() {
                el = $(this);

                offsetLeft = el.is(':visible') ?
                                        el.position().left :
                                        _mm2px(me.getLogoInputByParam(el, 'left').val());

                elWidth = el.width() + 12;
                parentWidth = el.parent().width();

                if (parentWidth > 0 && offsetLeft + elWidth > parentWidth) {
                    offsetLeft -= elWidth - (parentWidth - offsetLeft) + 6;
                } else if (offsetLeft < 0) {
                    offsetLeft = 6;
                }

                el.css('left', offsetLeft + 'px');
                me.onLogoDrag.call(me, el);
                me.onPagingDrag.call(me);
            });
        },

        onLogoResize: function(logo) {
            if (logo.is(':visible')) {
                this.getLogoInputByParam(logo, 'height').val(_px2mm(logo.height()));
                this.getLogoInputByParam(logo, 'width').val(_px2mm(logo.width()));
                this.markSaved(false);
            }
        },

        onLogoDrag: function(logo) {
            if (logo.is(':visible')) {
                var pos = logo.position();
                this.getLogoInputByParam(logo, 'left').val(_px2mm(pos.left));
                this.getLogoInputByParam(logo, 'top').val(_px2mm(pos.top));
                this.markSaved(false);
            }
        },

        updatePagingParams: function() {
            if (this.paging) {
                var f = this.form,
                    s = this.settings,
                    css = {
                        color: '#' + s.pagingColor.val(),
                        fontWeight: s.pagingFontWeight.hasClass('checked') ? 'bold' : 'normal',
                        fontStyle: s.pagingFontStyle.hasClass('checked') ? 'italic' : 'normal',
                        textDecoration: s.pagingTextDecoration.hasClass('checked') ? 'underline' : 'none'
                    };

                $('[name=pagingColor]', f).val(s.pagingColor.val());
                $('[name=pagingFontWeight]', f).val(css.fontWeight);
                $('[name=pagingFontStyle]', f).val(css.fontStyle);
                $('[name=pagingTextDecoration]', f).val(css.textDecoration);

                this.paging.css(css);
                this.markSaved(false);
            }
        },

        onPagingDrag: function() {
            if (this.paging && this.paging.is(':visible')) {
                var pos = this.paging.position();
                $('[name=pagingTop]', this.form).val(_px2mm(pos.top));
                $('[name=pagingLeft]', this.form).val(_px2mm(pos.left));
                this.markSaved(false);
                this.el.trigger('pagingPosChange');
            }
        },

        togglePaging: function(addOrRemove, restore) {
            var me = this, paging,
                target = me.margins.filter('.tpl-margin-b').find('.tpl-content');

            if (addOrRemove) {
                me.paging = $(me.pagingTpl).prependTo(target);
                me.paging
                    .draggable({
                        containment: target,
                        start: function() {
                            me.paging.tooltip('hide');
                        },
                        stop: function() {
                            me.onPagingDrag.call(me);
                        }
                    })
                    .tooltip();

                if (restore) {
                    me.paging.css({
                        top:  _mm2px($('[name=pagingTop]', me.form).val()) + 'px',
                        left: _mm2px($('[name=pagingLeft]', me.form).val()) + 'px'
                    });
                } else {
                    me.onPagingDrag();
                }
            } else {
                $.each(['pagingLeft', 'pagingTop'], function(i, v) {
                    $('[name=' + v + ']', me.form).val(0);
                });
                me.paging.remove();
                me.paging = null;
            }

            $('[name=paging]', me.form).val(+addOrRemove);
            me.markSaved.call(me, false);
        },

        toggleLogo: function(target, addOrRemove, restore) {
            var me = this, logo,
                target = me.margins
                    .filter('.tpl-margin-' + ('header' == target ? 't' : 'b'))
                    .find('.tpl-content');

            if (addOrRemove) {
                logo = $(me.logoTpl).prependTo(target)
                    .draggable({
                        containment: target,
                        stop: function() {
                            me.onLogoDrag.call(me, $(this));
                        }
                    });

                if (restore) {
                    logo.css({
                        top:    _mm2px(me.getLogoInputByParam(logo, 'top').val()) + 'px',
                        left:   _mm2px(me.getLogoInputByParam(logo, 'left').val()) + 'px',
                        height: _mm2px(me.getLogoInputByParam(logo, 'height').val()) + 'px',
                        width:  _mm2px(me.getLogoInputByParam(logo, 'width').val()) + 'px'
                    });
                } else {
                    me.initResizableLogo(logo);
                    me.onLogoResize(logo);
                    me.onLogoDrag(logo);
                }
            } else {
                logo = target.find('.tpl-logo');
                $.each(['Left', 'Top', 'Height', 'Width'], function(i, v) {
                    me.getLogoInputByParam(logo, v).val(0);
                });

            }

            me.getLogoInputByParam(logo).val(+addOrRemove);
            !addOrRemove && logo.remove();
            me.markSaved.call(me, false);
        },

        updateMargins: function() {
            var me = this, m = me.margins,
                mt = m.filter('.tpl-margin-t'),
                mr = m.filter('.tpl-margin-r'),
                mb = m.filter('.tpl-margin-b'),
                ml = m.filter('.tpl-margin-l');

            mt.add(mb).find('.tpl-content').css({
                'marginRight': mr.width(),
                'marginLeft': ml.width()
            });

            me.fixLogoPos();

            $('[name=marginTop]', me.form).val(_px2mm(mt.height()));
            $('[name=marginRight]', me.form).val(_px2mm(mr.width()));
            $('[name=marginBottom]', me.form).val(_px2mm(mb.height()));
            $('[name=marginLeft]', me.form).val(_px2mm(ml.width()));

            this.markSaved(false);
        },

        initEvents: function() {
            var me = this;


            me.deletebkg.on('click', function() {

                $.ajax({
                    url: '/proposal/template/delete-background',
                    type: 'post',
                    data: {
                        format: 'json',
                        scope: me.scope,
                        customTemplateId: me.customTemplateId
                    },
                    success: function(response) {
                        if (response.success) {
                            $('.bg', me.el).remove();
                        } else {
                            alert(response.error);
                        }
                    }
                });
            });


            me.margins
                .each(function() {
                    var el = $(this);
                    el.resizable($.extend({
                        minHeight: 1,
                        minWidth: 1
                    }, el.data()));

                    var logo = $('.tpl-logo', el);
                    if (logo.length) {
                        me.initResizableLogo(logo);
                    }
                })
                .on('resize', function(e, ui) {
                    if (ui.element) {
                        var alsoResize = ui.element.data('alsoResize');
                        if (alsoResize) {
                            /(tpl-margin-(t|b))/ig.test(this.className) ?
                                alsoResize.css('height', ui.element.height()) :
                                alsoResize.css('width', ui.element.width());
                        }

                        me.el.trigger('marginChange', [ui.element.add(alsoResize)]);
                    }
                })
                .on('resizestop', function(e, ui) {
                    me.updateMargins.call(me);
                    var params = ui ? [ui.element.add(ui.element.data('alsoResize'))] : [];
                    me.el.trigger('marginChange', params);
                });

            me.form.submit(function() {
                if (me.disabled) return false;

                me.form.ajaxSubmit({
                    data: { format: 'json' },
                    mask: true,
                    maskTarget: me.el,
                    beforeSubmit: function() {
                        me.settings.toggleDisable(true);
                    },
                    success: function(response) {
                        if (response.success) {
                            me.markSaved.call(me, true);
                        } else {
                            alert(response.error);
                        }
                    },
                    complete: function() {
                        me.settings.toggleDisable(false);
                    }
                });

                return false;
            });

            var submitBtn = $('[type=submit]', me.form);
            me.el
                .on('disabled', function(e, disabled) {
                   me.disabled = disabled;
                   submitBtn.toggleClass('btn-disabled', disabled);
                   me.wrap.trigger('disabled', [disabled]);
                })
                .on('pagingParamsChanged', function() {
                    me.paging && me.updatePagingParams.call(me);
                });
        }
    }

    var CustomTplBuilder = function() {
        this.el = $('#customTplWrap');
        this.orientation = $('input[name=orientation]');
        this.separateBackgrounds = $('#separateBackgrounds');
        this.returnBtn = $('a.returnBtn');

        this.initPages();
        this.initEvents();
    }

    CustomTplBuilder.prototype = {
        disabled: false,

        initPages: function() {
            var me = this;
            me.pages = [];
            $('.tpl-wrap', this.el).each(function() {
                me.pages.push(new TplPage(this));
            });
        },

        initEvents: function() {
            var me = this;

            me.separateBackgrounds.change(function() {
                var el = $(this);
                $.ajax({
                    url: '/proposal/template/different-bg',
                    data: {
                        id: me.separateBackgrounds.attr('theme_id'),
                        different : +this.checked
                    },
                    type: 'post',
                    mask: true,
                    maskTarget: me.el,
                    beforeSend: function() {
                        el.add(me.orientation).attr('disabled', 'disabled');
                    },
                    success: function(html) {
                        me.el.html(html);
                        me.initPages();
                    },
                    complete: function() {
                        el.add(me.orientation).removeAttr('disabled');
                        me.orientation
                            .filter('[value="' + (me.pages[0].isLandscape() ? 'landscape' : 'portrait')  + '"]')
                            .val('checked', 'checked');
                    }
                });
            });

            me.orientation.change(function() {
                $.ajax({
                    url: '/proposal/template/orientation',
                    data: {
                        id: me.orientation.attr('theme_id'),
                        format: 'json',
                        landscape : +('landscape' == me.orientation.filter('[checked]').val())
                    },
                    type: 'post',
                    mask: true,
                    maskTarget: me.el,
                    beforeSend: function() {
                        me.orientation
                            .add(me.separateBackgrounds)
                            .attr('disabled', 'disabled');
                    },
                    success: function(response) {
                        !response.success && alert(response.error);
                    },
                    complete: function() {
                        me.orientation
                            .add(me.separateBackgrounds)
                            .removeAttr('disabled');
                    }
                });

                for (var i = 0, len = me.pages.length; i < len; i++) {
                    me.pages[i].changeOrientation.call(me.pages[i]);
                }

            });

            var inputs = me.separateBackgrounds.add(me.orientation);
            me.el.on('disabled', '.tpl-wrap', function(e, disabled) {
               me.disabled = disabled;
               disabled ? inputs.attr('disabled', 'disabled'):
                          inputs.removeAttr('disabled');

               me.returnBtn.toggleClass('btn-disabled', disabled);
            });

            me.returnBtn.click(function() {
                if (me.disabled) return false;

                for (var i = 0, len = me.pages.length; i < len; i++) {
                    if (!me.pages[i].saved) {
                        alert('De instellingen worden niet opgeslagen.\nGelieve op te slaan voordat het verlaten van deze pagina.');
                        return false;
                    }
                }

                return true;
            });
        }
    }

    $(function() {
        window.CustomTplBuilder = new CustomTplBuilder();
    })

})(window.jQuery);