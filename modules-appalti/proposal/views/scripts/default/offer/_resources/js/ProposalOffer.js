ProposalOffer = function (conf) {
    $.extend(this, conf || {});
    this.init();
}

ProposalOffer.prototype = function() {
    var protect = {
        modalWnd: null,

        getModalWindow: function() {
            if (!this.modalWnd) {
                this.modalWnd = $('<div id="modal-container" class="modal"></div>').appendTo('body');
            }

            return this.modalWnd;
        }
    };

    return {
        loadUrl: '/proposal/offer/index',

        addUrl: '/proposal/offer/create',

        updateUrl: '/proposal/offer/update',

        deleteUrl: '/proposal/offer/delete',

        containerId: null,

        proposalId: null,

        onOfferLoad: $.noop,

        maskMsg: 'Opnieuw te berekenen en opslaan van gegevens. Een ogenblik geduld.',

        init: function() {
            this._container = $('#' + this.containerId)
                .on('click', '.add-offer', this.addOffer())
                .on('click', '.delete-offer', this.deleteOffer())
                .on('change', '.update-offer', this.updateOffer());

            this.form = $('#proposal-price');

            this.doLoadList();
        },

        addOffer: function() {
            return $.proxy(this.showAddForm, this);
        },

        deleteOffer: function() {
            return $.proxy(this.doDelete, this);
        },

        updateOffer: function() {
            return $.proxy(this.doUpdate, this);
        },

        refreshArrowState: function(event, ui) {
            var parent = arguments.length > 1 ?  ui.item.closest('tbody') : arguments[0];

            $('.icon-move-down', parent)
                .removeClass('icon-move-down-disabled')
                .filter(':last')
                .addClass('icon-move-down-disabled');

            $('.icon-move-up', parent)
                .removeClass('icon-move-up-disabled')
                .filter(':first')
                .addClass('icon-move-up-disabled');

            $('input:hidden', parent).each(function() {
                var el = $(this);
                el.val(el.closest('tr').index());
            });

            $.post('/proposal/offer/save-positions/format/json/', this.form.serialize());
        },

        initSortable: function() {
            var el = $('.sortable'), me = this,
                params = {
                    update: function(event, ui) {
                        me.refreshArrowState.call(me, event, ui);
                    }
                }

            el.sortable($.extend(params, el.data() || {}));

            el.on('click', '[class*=icon-move-]', function(e) {
                var target = $(this);

                if (/disabled/.test(target[0].className)) return;

                var parent = $(target).closest('tr');

                if (/up/.test(target[0].className)) {
                    parent.index() && parent.insertBefore(parent.prev());
                } else {
                    parent.next().length && parent.insertAfter(parent.next())
                }

                me.refreshArrowState.call(me, el);
                el.sortable('refresh');
            });
        },

        doLoadList: function () {
            var me = this;
            return $.get(me.loadUrl, { proposalId: me.proposalId }, function(data) {
                me._container.html(data);
                me.onOfferLoad();
                me.initSortable();
                var $discountPerOffer = $('#includeDiscountPerOffer');
                if ($('#offer-list-empty').length) {
                    $discountPerOffer.attr('disabled', 'disabled');
                } else if( jQuery('input[name="proposal:grant-discount"]').val() == 1) {
                    $discountPerOffer.removeAttr('disabled');
                }
            })
        },

        doDelete: function(e) {
            var $el = $(e.currentTarget ? e.currentTarget : e.srcElement),
                me = this;

            $.ajax({
                url: this.deleteUrl,
                data: {
                    proposalId: this.proposalId,
                    offerId: $el.data('id'),
                    format: 'json'
                },
                type: 'post',
                mask: true,
                maskMsg: me.maskMsg,
                success: $.proxy(this.doLoadList, this)
            });

            return false;
        },

        doUpdate: function(e) {
            var input = $(e.currentTarget ? e.currentTarget : e.srcElement),
                fieldName = input.attr('name');

            var params = {
                proposalCompanyOfferId: input.data('id')
            };

            params[fieldName] = input.val();
            params['format'] = 'json';

            $.ajax({
                url: this.updateUrl,
                data: params,
                type: 'post',
                mask: true,
                maskMsg: this.maskMsg,
                success: $.proxy(this.doLoadList, this)
            });

            return false;
        },

        doAdd: function(params) {
            params['format'] = 'json';
            $.post(this.addUrl, params, $.proxy(function() {
                protect.getModalWindow().modal('hide');
                this.doLoadList();
            }, this));
        },

        showAddForm: function (e) {
            if ($(e.target).attr('disabled') !== undefined) {
                alert('U mag geen diensten of producten toevoegen.');
                event.preventDefault();
                return false;
            }
            $.get(this.addUrl, {
                proposalId: this.proposalId
            }, $.proxy(function(data) {
                var mw = protect.getModalWindow().addClass('offer-modal-container').html(data).modal('show'),
                    form = $('form', mw),
                    select = $('select[name=companyOfferId]', mw);

                //auto fill
                select.off('change').change(function() {
                    var option = $('option:selected', this), val;

                    $.each(['name', 'description', 'amount'], function(index, value) {
                        form.find('[name=' + value + ']').val(option.attr('data-' + value));
                    });


                    $.each(['pricePerUnit', 'discount'], function(index, value) {
                        val = parseFloat(option.attr('data-' + value));
                        form.find('[name=' + value + ']').val(val ? number_format(val, 2, ',', '.') : val);
                    });

                    form.find('[name=kind]').each(function (index, el) {
                        var el = $(el);
                        option.attr('data-kind') == el.attr('value')
                            ? el.attr('checked', 'checked')
                            : el.removeAttr('checked');
                    });

                    if (form.data('validator')) {
                        form.data('validator').form();
                    }
                });

                //adding offer
                form.submit($.proxy(function() {
                    if (!form.valid()) {
                        return false;
                    }

                    this.doAdd(form.serialize());
                    return false;
                }, this));

            }, this));

            return false;
        }
    }
}();
