Ext.define('Module.Proposal.List', {
    extend:'Ext.ux.grid.GridPanel',
    alias:'widget.module.proposal.list',
    filterRequestParam:null,

    features:[ {
        ftype:'filters'
    } ],

    companyId: null,
    clientCompanyId: null,
    initComponent:function() {

        this.store = new Ext.data.Store({
            model:'Module.Proposal.Model.Proposal',
            proxy:{
                type:'ajax',
                url:link('proposal', 'main', 'fetch-all', { format:'json' }),
                reader:{
                    type:'json',
                    root:'rowset'
                }
            }
        });

        this.columns = [ {
            header:lang('name'),
            dataIndex:'name',
            flex:1
        }, {
            header:lang('Luid'),
            dataIndex:'luid',
            flex:1
        },  {
            header:lang('Amount'),
            dataIndex:'amount',
            flex:1,
            renderer: function(value) {
                return '€'+value;
            }

        },{
            header:lang('Chance of success'),
            dataIndex:'chanceOfSuccess',
            flex:1,
            renderer: function(value) {
                return value+'%';
            }
        },{
            header:lang('Contact moment date'),
            dataIndex:'contactMomentDate',
            flex:1
        },{
            header:lang('Download'),
            dataIndex:'actions',
            flex: 0.4,
            renderer: function(v, m,record  ){
//                var send = '<a href=/proposal/main/send/proposalId/'+record.raw.id+'><img src="/modules-appalti/proposal/views/scripts/default/main/_resources/images/notification.png"></a>';
                var download = '<a href=/proposal/main/download-pdf/proposalId/'+record.raw.id+'><img src="/modules-appalti/proposal/views/scripts/default/main/_resources/images/pdf.png"></a>';
                return download;
            }
        },{
            xtype:'actioncolumn',
            header:lang('Actions'),
            width:50,
            fixed:true,
            items:[
                {
                tooltip:lang('Send by e-mail'),
                iconCls:'icon-notificate-16 icon-16',
                handler:function(g, rowIndex) {
                    Ext.Msg.confirm(
                        lang('Confirmation'),
                        lang('Are you sure to send this by e-mail?'),
                        function(b) {
                            b == 'yes' && this.onSendLetter(g,g.getStore().getAt(rowIndex));
                        }, this);
                },
                scope:this
            },
                {
                tooltip:lang('Delete'),
                iconCls:'icon-delete-16 icon-16',
                handler:this.onDeleteCategory,
                scope:this
            } ]
        } ];

        this.tbar = [ '-' ];

        this.plugins = [ new Ext.ux.grid.Search({
            minChars:2,
            stringFree:true,
            showSelectAll:false,
            align:2
        }) ].concat(Ext.isArray(this.plugins) ? this.plugins : []);
        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store:this.store,
            plugins:'pagesize'
        });
        ;
        this.callParent();

        this.getView().on('itemdblclick', function(w, record) {
            this.onEditCategory(this, record);
        }, this);
    },

    onDeleteCategory:function(g, rowIndex) {
        var record = g.getStore().getAt(rowIndex);
        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'),
        function(b) {
            if (b != 'yes') {
                return;
            }
            Ext.Ajax.request({
                url:link('proposal', 'main', 'delete', { format:'json' }),
                method:'POST',
                params:{
                    proposalId:record.get('id')
                },
                success:function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);
                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope:this
            });
        }, this);
    },

    onSendLetter : function (g,record)
    {
            Ext.Ajax.request({
            url:link('proposal', 'main', 'send'),
            method:'POST',
            params:{
                proposalId:record.get('id')
            },
            success:function(response, options) {
                var decResponse = Ext
                    .decode(response.responseText);
                Application
                    .notificate(decResponse.messages);
                if (true == decResponse.success) {
                    g.getStore().load();
                }
            },
            scope:this
        });
    },

    onEditCategory:function(g, record) {
        Ext.Ajax.request({
            url:link('proposal', 'index', 'download-pdf'),
            params:{
                proposalId:record.get('id')
            },
            success:function(response, options) {
                var decResponse = Ext
                    .decode(response.responseText);
                Application
                    .notificate(decResponse.messages);
                if (true == decResponse.success) {
                    g.getStore().load();
                }
            },
            scope:this
        });
    },

    setBaseParams : function(entityId ,clientCompanyId, forceReload) {

        this.companyId = entityId;
        this.clientCompanyId = clientCompanyId;
        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.companyId = entityId;
        p.extraParams.clientCompanyId = clientCompanyId;

        this.getStore().load();


        return this;

    }
});