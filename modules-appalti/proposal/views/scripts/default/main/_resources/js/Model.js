Ext.define('Module.Proposal.Model.Proposal', {
    extend:'Ext.data.Model',
    fields:[ 'id', 'name','luid','amount','chanceOfSuccess', 'contactMomentDate',]
});