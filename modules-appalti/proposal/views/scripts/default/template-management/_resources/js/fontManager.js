!function($) {
    var FontsManager = function() {
        this.fontForm = $('form[name="addFontForm"]');
        this.fonts = $('#templateFonts');
        this.errMsg = $('.error', this.fontForm);
        this.previewArea = $('.preview-area');
        this.initEvents();
    }

    FontsManager.prototype = {
        initEvents: function() {
            var me = this;

            me.fonts.change(function() {
                me.previewArea.css('fontFamily', this.value);
            });

            me.fontForm.submit(function() {
                var noFontFile = false;

                $('input.font', this).each(function() {
                    if (!!this.value) {
                        noFontFile = false;
                        return false;
                    }
                });

                if (me.fontForm.valid()) {
                    if (noFontFile) {
                        me.errMsg.slideDown();
                        return false;
                    }

                    return true;
                }
            });
        }
    }

    $(function() {
       window.FontsManager = new FontsManager();
    });
}(window.jQuery);
