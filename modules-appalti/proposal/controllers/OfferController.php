<?php

class Proposal_OfferController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Proposal_Service_ProposalOffer
     */
    protected $_service;

    /**
     * @var \Proposal_Model_Proposal
     */
    protected $_proposalRow;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_helper->ajaxContext()
            ->addActionContext('get-total-cost', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('save-positions', 'json')
            ->initContext();


        if ($this->_getParam('proposalId')) {
            $ps = new Proposal_Service_Proposal();
            $this->_proposalRow = $ps->find($this->_getParam('proposalId'));

            $accountRow = Zend_Auth::getInstance()->getIdentity();
            if ($accountRow->isAdmin()) {
                $companyRow = $this->_proposalRow->getCompanyRow();
            } else {
                $companyRow = $accountRow->getCompanyRow();
            }

            if ($this->_proposalRow->companyId != $companyRow->getId()) {
                throw new Exception('Access denied');
            }
        }

        parent::init();

        $this->_service = new \Proposal_Service_ProposalOffer();
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'proposal:create';
    }

    public function getTotalCostAction()
    {
        $price = $this->_service->getTotalCost($this->_getParam('proposalId'));
        $this->view->price = $price;
        $this->view->success = true;
    }

    public function indexAction()
    {
        $this->view->includeDiscountPerOffer = (1 === (int)$this->_proposalRow->getThemeRow()->includeDiscountPerOffer);
        $this->view->rowset = $this->_service->fetchAll($this->_getParam('proposalId'));
        $this->view->proposalRow = $this->_proposalRow;
        $this->view->accountRow = Zend_Auth::getInstance()->getIdentity();
    }

    public function createAction()
    {
        if ($this->getRequest()->isPost()) {
            try {
                $this->_service->create($this->_getAllParams());
                $this->view->success = true;
                $this->_helper->information('Proposal offer created successfully.', true, E_USER_NOTICE);
            } catch (\Exception $e) {
                return $this->_helper->information('Unable to create proposal offer');
                $this->view->success = false;
            }
        }

        $this->view->offerRowset = $this->_service->fetchAll($this->_getParam('proposalId'), false);
        $this->view->proposalId = $this->_getParam('proposalId');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->isPost()) {
            try {
                $this->_service->delete($this->_getParam('proposalId'), $this->_getParam('offerId'));
                $this->view->success = true;
                $this->_helper->information('Proposal offer deleted successfully.', true, E_USER_NOTICE);
            } catch (\Exception $e) {
                return $this->_helper->information('Unable to delete proposal offer');
                $this->view->success = false;
            }
        }
    }

    public function savePositionsAction()
    {
        if ($this->getRequest()->isPost()) {
            try {
                $this->_service->setPositions($this->_proposalRow->getId(), $this->_getParam('offerPosition'));
                $this->view->success = true;
            } catch (\Exception $e) {
                $this->view->success = false;
                return $this->_helper->information('Unable to save proposal offer positions');
            }
        }
    }

    public function updateAction()
    {
        if ($this->getRequest()->isPost()) {
            try {
                $proposalCompanyOfferId = $this->_getParam('proposalCompanyOfferId');
                $this->_service->update($proposalCompanyOfferId, $this->_getAllParams());
                $this->view->success = true;
                $this->_helper->information('Proposal offer saved successfully.', true, E_USER_NOTICE);
            } catch (\Exception $e) {
                $this->view->success = false;
                return $this->_helper->information('Unable to save proposal offer');
            }
        }
    }

}