<?php
class Proposal_SettingsController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'proposal';
    }
    
    public function indexAction()
    {
    }

}