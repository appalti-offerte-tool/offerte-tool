<?php

class Proposal_TemplateManagementController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Proposal_Service_Template
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_service = new \Proposal_Service_Template();

        $this->_helper->ContextSwitch()
            ->addActionContext('add-font', 'json')
            ->addActionContext('scan-fonts', 'json')
            ->addActionContext('use-custom-template', 'json')
            ->addActionContext('settings', 'json')
            ->addActionContext('orientation', 'json')
            ->initContext();


        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'template';
    }

    public function indexAction()
    {
        $this->view->requireFontScanning = $this->_service->fontScanningRequired();
    }

    public function addFontAction()
    {
        if ($this->_request->isPost() && $this->_getParam('fontname')) {
            try {
                $this->view->fontFamily = $this->_service->addFont($this->_getParam('fontname'));
            } catch (\Exception $e) {
                $this->_helper->information($e->getMessage());
            }
        } else {
            $this->_helper->information('Wrong request or font name is not supplied.');
        }

        $this->_redirect('/proposal/template-management/index');
    }

    public function deleteFontAction()
    {
        if ($this->_request->isPost() && $this->_getParam('templateFonts')) {
            try {
                $fonts = $this->_getParam('templateFonts');
                foreach ($fonts as $font) {
                    $this->_service->deleteFont($font);
                }
            } catch (\Exception $e) {
                $this->_helper->information($e->getMessage());
            }
        } else {
            $this->_helper->information('Wrong request or font is not selected.');
        }

        $this->_redirect('/proposal/template-management/index');
    }

    public function scanFontsAction()
    {
        try {
            $this->_service->scanFonts(true);
            $this->view->success = true;
            $this->_helper->information('Fonts scanning finished successfully.', true, E_USER_NOTICE);
        } catch (\Exception $e) {
            $this->view->success = false;
            $this->_helper->information(array('Error scanning fonts. Error: %s', array($e->getMessage())), true);
        }

        $this->_redirect('/proposal/template-management/index');
    }
}