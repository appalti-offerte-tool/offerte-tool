<?php

class Proposal_ProposalBlockDefinitionFileStockController extends \Zend_Controller_Action implements \FileStock_Instance_EntityFileStockControllerInterface
{
    /**
     * @var \Proposal_Service_ProposalBlockDefinitionFileStock
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'proposal';
    }

    public function init()
    {
        $this->_service = new \Proposal_Service_ProposalBlockDefinitionFileStock($this->getEntityType());

        $this->_helper->contextSwitch()
           ->addActionContext('fetch-all', 'json')
           ->addActionContext('fetch', 'json')
           ->addActionContext('fetch-thumbnail', 'json')
           ->addActionContext('create', 'json')
           ->addActionContext('update', 'json')
           ->addActionContext('delete', 'json')
           ->addActionContext('set-default-thumbnail', 'json')
           ->initContext();
    }

    public function getEntityType()
    {
        return 'proposal-block';
    }

    public function indexAction()
    {
        try {
            $blockId = $this->_getParam('blockDefinitionId');
            if (!empty($blockId)) {
                $response = $this->_service->fetchAllFileStockByProposalBlockIdWithResponse($this->_getParam('blockDefinitionId'));
                $this->view->assign($response->toArray());
            }

            $accountRow = Zend_Auth::getInstance()->getIdentity();

            $this->view->companyId = $accountRow->getCompanyRow()->getId();
            $this->view->accountId = $accountRow->getId();
            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function downloadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $fileStockRow = $this->_service->find($this->_getParam('blockId'), $this->_getParam('id'));

        $fileStockService = new FileStock_Service_FileStock();
        $fileStockService->download($fileStockRow, $this->getResponse(), true);
    }

    public function fetchAllAction()
    {
        try {
            $response = $this->_service->fetchAllFileStockByProposalBlockIdWithResponse($this->_getParam('blockId'));
            $this->view->assign($response->toArray());
            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAction()
    {
        $fileStockRow = $this->_service->find($this->_getParam('blockId'), $this->_getParam('fileStockId'));

        $this->view->data = $fileStockRow->toArray();
        $this->view->success = true;
    }

    public function fetchThumbnailAction()
    {
        try {
            $fileStockRow = $this->_service->fetchFileStockThumbnailRowByProposal($this->_getParam('blockDefinitionId'), $this->_getParam('fileStockId'));
            $this->view->assign($fileStockRow);
            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function createAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');

        try {

            list($articleRow, $fileStockRow) = $this->_service->createNew($this->_getAllParams());

            $this->view->success = true;
            $this->view->entityId = $articleRow->getId();
            $this->view->fileStockId = $fileStockRow->getId();
            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);
        } catch(OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function quickUploadAction()
    {
        try {
            list($blockDefinitionRow, $fileStockRow) = $this->_service->createNew($this->_getAllParams());

            $this->view->success = true;
            $this->view->blockdefinitionId = $blockDefinitionRow->getId();
            $this->view->blockId = $blockDefinitionRow->proposalBlockId;
            $folder = 1000 + floor($fileStockRow->getId() / 1000) * 1000;
            $this->view->src = sprintf('/modules-appalti/proposal/data/block-file/%d/%d.jpg', $folder, $fileStockRow->getId());
        } catch(OSDN_Exception $e) {
            $this->view->success = false;
            $this->view->error = $e->getMessage();
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');

        try {

            $this->_service->update($this->_getAllParams());

            $this->view->success = true;
            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);

        } catch(OSDN_Exception $e) {

            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->_service->delete($this->_getParam('proposalId'), $this->_getParam('fileStockId'), true, $this->_getParam('blockDefinitionId'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }

    public function setDefaultThumbnailAction()
    {
        try {
            $this->_service->setDefaultThumbnail($this->_getParam('blockId'), $this->_getParam('fileStockId'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }
}