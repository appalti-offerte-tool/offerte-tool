<?php

class Proposal_TemplateController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Proposal_Service_ProposalTheme
     */
    protected $_service;

    /**
     * @var \Company_Service_Wizard
     */
    protected $_wizard;

    /**
     * @var \Proposal_Model_ProposalTheme
     */
    protected $_proposalThemeRow;

    /**
     * @var \Company_Model_CompanyRow
     */
    protected $_companyRow;

    /**
     * @param $throwException
     *
     * @return \Proposal_Model_ProposalTheme
     */
    protected function _toProposalThemeRow($throwException = true)
    {
        $proposalThemeId = $this->_getParam('proposalThemeId');
        $customTemplateId = $this->_getParam('id');
        if (!empty($proposalThemeId)) {
            $proposalThemeRow = $this->_service->find($proposalThemeId, $throwException);
            $this->_customTemplateRow = $this->_themes->find($proposalThemeRow->useCustomTemplate);
        } elseif(!empty($customTemplateId)){
            $this->_customTemplateRow = $this->_themes->find($customTemplateId);
            $proposalThemeRow = $this->_customTemplateRow->findParentRow('Proposal_Model_DbTable_ProposalTheme');
        } else {
            $proposalThemeRow = $this->_service->getDefaultRow();
            $this->_customTemplateRow = $this->_themes->find($proposalThemeRow->useCustomTemplate);
        }
        return $proposalThemeRow;
    }

    protected function _toCompanyRow()
    {
        if (empty($this->_companyRow)) {
            $accountRow = Zend_Auth::getInstance()->getIdentity();

            if ($accountRow->isAdmin() && $this->_getParam('companyId')) {
                $companySrv = new \Company_Service_Company();
                $this->_companyRow = $companySrv->find($this->_getParam('companyId'));
            } else {
                $this->_companyRow = $accountRow->getCompanyRow(false);
            }
        }
        return $this->_companyRow;

    }

    protected function _toCustomTplRow()
    {
        $tplSrv = new \Proposal_Service_CustomTemplate();
        $companyRow = $this->_toCompanyRow();
        $row = $tplSrv->fetchByCompany($companyRow);
        $this->_toProposalThemeRow();
        $row = $this->_customTemplateRow;
        if (!$row) {
            $row = $tplSrv->create($companyRow);
        }
        return $row;
    }

    /**
     * (non-PHPdoc)
     *
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_wizard = new \Company_Service_Wizard();
        $this->view->wizard = $this->_wizard;
        if ($this->_helper->acl->isAllowed($this->getResourceId())) {
            $companyRow = $this->_toCompanyRow();
            if ($companyRow) {
                $this->_themes = new Proposal_Service_CustomTemplate();
                $this->_service = new \Proposal_Service_ProposalTheme($companyRow);
                $this->_proposalThemeRow = $this->_toProposalThemeRow();
                $this->view->proposalThemeRow = $this->_proposalThemeRow;
                $this->view->customTemplateRow = $this->_customTemplateRow;
            }
        }
        $this->_helper->ContextSwitch()
            ->addActionContext('background', 'json')
            ->addActionContext('delete-background', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('use-custom-template', 'json')
            ->addActionContext('settings', 'json')
            ->addActionContext('orientation', 'json')
            ->initContext();
        parent::init();
    }

    /**
     * (non-PHPdoc)
     *
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'proposal:template';
    }

    public function createAction()
    {
        $this->view->customTpl = $this->_toCustomTplRow();
        $this->view->return = $this->_getParam('return');
        unset($this->view->wizard);
    }

    public function getImageAction()
    {
        $this->_helper->layout->disableLayout();

        $customTemplateId = $this->_getParam('id', 0);
        $scope = $this->_getParam('scope');
        $_path = '/modules-appalti/company/data/templates/';
        $dir = sprintf('%s%d/%d/backgrounds/', $_path, $this->_getParam('companyId'), $customTemplateId);
        if (file_exists(APPLICATION_PATH . $dir)) {
            $fileName = $scope . '-thumb';
            foreach(new DirectoryIterator(APPLICATION_PATH . $dir) as $f) {
                if ($f->isFile() && pathinfo($f->getFilename(), PATHINFO_FILENAME) === $fileName) {
                    $result =  $_SERVER['DOCUMENT_ROOT']. $dir . $f->getFilename();
                    break;
                }
            }
            $this->_helper->layout->disableLayout();
            $this->_response->setHeader('Content-Type',  'image/jpeg', true);

            $img = imagecreatefromjpeg($result);
            imagejpeg($img, NULL ,75);
            imagedestroy($img);
            $this->_response->sendResponse();
            exit;
        }
    }

    public function useCustomTemplateAction()
    {
        $useCustom = (int)$this->_getParam('useCustom');
//        $useCustom = 1;
        if ($this->getRequest()->isPost()) {
            try {
                $this->_service->update($this->_proposalThemeRow, array (
                    'companyId'         => $this->_toCompanyRow()->getId(),
                    'useCustomTemplate' => $useCustom
                ));

                if ($useCustom) {
                    $returnUrl = base64_encode($this->_getParam('returnUrl'));
                    $this->view->redirect = sprintf('/proposal/template/create/companyId/%s/return/%s', $this->_toCompanyRow()->getId(), $returnUrl);
                }

                $this->view->success = true;
            } catch (\Exception $e) {
                $this->view->success = false;
                $this->view->error = 'Error setting value. ' . $e->getMessage();
            }
        }

        unset($this->view->wizard);
        unset($this->view->proposalThemeRow);
    }

    public function backgroundAction()
    {
        if ($this->getRequest()->isPost()) {
            try {
                $tplSrv = new \Proposal_Service_CustomTemplate();
                $bgPath = $tplSrv->saveBackground($this->_getAllParams());
                $this->view->path = $bgPath;
                $this->view->success = true;
            } catch (\Exception $e) {
                $this->view->success = false;
                $this->view->error = $e->getMessage();
            }
        } else {

        }

        unset($this->view->wizard);
        unset($this->view->proposalThemeRow);
        $this->getResponse()->setHeader('Content-Type', 'text/html', true);
    }

    public function deleteBackgroundAction()
    {
        try {
            $tplSrv = new \Proposal_Service_CustomTemplate();
            $tplSrv->deleteBackground($this->_getParam('customTemplateId'),$this->_getParam('scope'));
            $this->view->success = true;
        } catch (\Exception $e) {
            $this->view->success = false;
            $this->view->error = $e->getMessage();
        }
    }

    public function settingsAction()
    {
        if ($this->getRequest()->isPost()) {
            try {
                $tplSrv = new \Proposal_Service_CustomTemplate();
                $tpl = $this->_toCustomTplRow();
                $tplSrv->saveSettings($tpl, $this->_getAllParams());
                $this->view->success = true;
            } catch (\Exception $e) {
                $this->view->success = false;
                $this->view->error = $e->getMessage();
            }
        }

        unset($this->view->wizard);
        unset($this->view->proposalThemeRow);
    }

    public function orientationAction()
    {
        if ($this->getRequest()->isPost()) {
            $tplSrv = new \Proposal_Service_CustomTemplate();
            try {
                $tpl = $this->_toCustomTplRow();
                $tplSrv->update($tpl, array (
                    'landscape' => $this->_getParam('landscape', 0)
                ));
                $this->view->success = true;
            } catch (\Exception $e) {
                $this->view->success = false;
                $this->view->error = $e->getMessage();
            }
        }
        unset($this->view->wizard);
        unset($this->view->proposalThemeRow);
    }

    public function differentBgAction()
    {
        $tpl = $this->_toCustomTplRow();

        if ($this->getRequest()->isPost()) {
            $tplSrv = new \Proposal_Service_CustomTemplate();
            $useDifferentBackgrounds = $this->_getParam('different', 0);
            try {
                $tplSrv->update($tpl, array (
                    'useDifferentBackgrounds' => $useDifferentBackgrounds
                ));
                $this->view->success = true;
            } catch (\Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessage());
            }
        }

        $this->view->useDifferentBackgrounds = $useDifferentBackgrounds;
        $this->view->customTpl = $tpl;
        unset($this->view->wizard);
        unset($this->view->proposalThemeRow);
    }

    public function defaultAction()
    {
        // init
        $this->_wizard->setCurrent($this->_wizard->getStepProposalDefault());

        // validate
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $companyRow = $accountRow->getCompanyRow(false);
        if (empty($companyRow)) {
            $this->_helper->information('Voer eerst de bedrijfsgegevens in.', true, E_USER_WARNING);
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }
        $contactpersonRow = $accountRow->getContactPersonRow(false);
        if (empty($contactpersonRow)) {
            $this->_helper->information('Voer eerst uw gegevens in.', true, E_USER_WARNING);
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }

        // process
        if ($this->getRequest()->isPost())
        {
            $params = $this->_getAllParams();
            $this->view->formSubmittedPostData = $params;

            try {
                if (!empty($this->_proposalThemeRow)) {
                    $this->_service->update($this->_proposalThemeRow, $params);
                } else {
                    $this->_proposalThemeRow = $this->_service->create($params);
                }

                $this->view->proposalThemeRow = $this->_proposalThemeRow;
                $this->view->success = true;
                $this->_helper->information('Voorstel defaults succesvol opgeslagen', true, E_USER_NOTICE);
            } catch (\Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessage());
            }

            if (!empty($params['next-step'])) {
                $this->_redirect($this->_wizard->getNextStepUrl());
            }
        }
    }

    protected function _updateProposalTheme($section, array $params = array ())
    {
        $params = !empty($params) ? $params : $this->_getAllParams();
        $this->view->formSubmittedPostData = $params;

        try {
            $this->_service->update($this->_proposalThemeRow, $params);
            $this->view->success = true;
            $this->_helper->information(array ('Voorstel %s succesvol opgeslagen', array ($section)), true, E_USER_NOTICE);
//            $this->_redirect('/company/theme/update/id/'.$this->_proposalThemeRow->useCustomTemplate);
        } catch (\Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessage());
        }
    }

    public function templateAction()
    {
        // init
        $this->_wizard->setCurrent($this->_wizard->getStepTemplates());

        // validate
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $companyRow = $accountRow->getCompanyRow(false);
        if (empty($companyRow)) {
            $this->_helper->information('Voer eerst de bedrijfsgegevens in.', true, E_USER_WARNING);
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }
        $contactpersonRow = $accountRow->getContactPersonRow(false);
        if (empty($contactpersonRow)) {
            $this->_helper->information('Voer eerst uw gegevens in.', true, E_USER_WARNING);
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }


        $this->view->customTpl = $this->_toCustomTplRow();

        $templatesSrv = new \Proposal_Service_Template();
        $this->view->templates = $templatesSrv->fetchAll();

        if ($this->getRequest()->isPost()) {
            $this->_updateProposalTheme('templates');

            if ($this->_getParam('next-step')) {
                $this->_redirect($this->_wizard->getNextStepUrl());
            }
        }
    }

    public function typographyAction()
    {
        // init
        $this->_wizard->setCurrent($this->_wizard->getStepTemplateTypography());

        // validate
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $companyRow = $accountRow->getCompanyRow(false);
        if (empty($companyRow)) {
            $this->_helper->information('Voer eerst de bedrijfsgegevens in.', true, E_USER_WARNING);
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }
        $contactpersonRow = $accountRow->getContactPersonRow(false);
        if (empty($contactpersonRow)) {
            $this->_helper->information('Voer eerst uw gegevens in.', true, E_USER_WARNING);
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }

        if ($this->getRequest()->isPost())
        {
            $params = !empty($params) ? $params : $this->_getAllParams();
            $this->view->formSubmittedPostData = $params;
            try {
                $this->_service->update($this->_proposalThemeRow, $params);
                $this->view->success = true;
                $this->_helper->information(array ('Huisstijl succesvol opgeslagen'), true, E_USER_NOTICE);
                if ($this->_getParam('next-step')) {
                    $this->_redirect($this->_wizard->getNextStepUrl());
                }
            } catch (\Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessage());
            }
        }
    }

    public function companyDefaultsAction()
    {
        // get templates
        $templatesSrv = new \Proposal_Service_Template();
        $this->view->templates = $templatesSrv->fetchAll();

        // get libraries
        $_libraries = new Company_Service_Library();
        $this->view->libraries = $_libraries->fetchAllActive();
        $this->view->customTemplateLibraries = $this->_themes->getLibraries($this->_customTemplateRow);

        if ($this->getRequest()->isPost()) {
            $this->_updateProposalTheme('template and typography');
        }
    }

    public function companyDefaultFieldsAction()
    {
        $this->view->namespace = 'proposalTheme';
        if ($this->getRequest()->isPost()) {
            $params = $this->_getAllParams();
            $this->_updateProposalTheme('company defaults', $params);
        }
    }

    public function categoryCssAction()
    {
        $this->view->layout()->disableLayout();
        $css = '';
        $luid = $this->_getParam('luid');

        if (!empty($luid)) {
            $sectionSrv = new \Proposal_Service_Section();
            $sectionRow = $sectionSrv->findByLuid($luid);

            $templateSrv = new \Proposal_Service_Template();
            $css = $templateSrv->getCssStyles($sectionRow, $this->_proposalThemeRow);
        }

        echo $css;
        die;
    }
}