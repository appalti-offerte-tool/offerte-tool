<?php

class Proposal_CreateController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Proposal_Service_Proposal
     */
    protected $_service;

    /**
     * @var Zend_Session_Namespace
     */
    protected $_session;

    /**
     * @var Proposal_Service_Wizard
     */
    protected $_wizard;

    /**
     * @var Company_Model_CompanyRow
     */
    protected $_company;

    /**
     * @var Account_Model_DbTable_AccountRow
     */
    protected $_account;


    protected $_logFile;

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'proposal:create';
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_logFile = APPLICATION_PATH . '/modules-appalti/proposal/data/log.txt';
        $this->_service = new \Proposal_Service_Proposal();
        $this->_wizard = new \Proposal_Service_Wizard();
        $this->view->wizard = $this->_wizard;

        $this->_helper->ContextSwitch()
            ->addActionContext('index', 'json')
            ->addActionContext('save-state', 'json')
            ->addActionContext('save-price', 'json')
            ->addActionContext('summary', 'json')
            ->addActionContext('price', 'json')
            ->addActionContext('create-definition', 'json')
            ->addActionContext('update-definition', 'json')
            ->addActionContext('delete-definition', 'json')
            ->addActionContext('get-themes', 'json')
            ->addContext('file', array(
                'suffix' => 'file'
            ))
            ->addContext('text', array(
                'suffix' => 'text'
            ))
            ->addActionContext('preview-definition', array('file', 'text'))
            ->initContext();

        $this->_account = Zend_Auth::getInstance()->getIdentity();
        $this->view->accountRow = $this->_account;
        $this->_session = new \Zend_Session_Namespace(__CLASS__ . $this->_account->getId());

        parent::init();
    }

    /**
     * @return Proposal_Model_Proposal
     */
    public function _toProposalRow($throwException = true)
    {
        $proposalId = $this->_session->proposalId;
        if (empty($proposalId)) {
            $proposalId = $this->_getParam('proposalId', null);
        }
        if (empty($proposalId)) {
            $this->_wizard->setProposalId(null);
            return null;
        }
        $row = $this->_service->find($proposalId, $throwException);
        if (!empty($row)) {
            $this->_wizard->setProposalId($row->getId());
        }
        return $row;
    }

    public function indexAction()
    {
        // start logging
        if (!file_exists($this->_logFile)) {
            fopen($this->_logFile,"w");
        }
        $file = fopen($this->_logFile, "a");
//        fwrite($file, "TEST MESSAGE\n");
        $date = new DateTime();
        $message = $date->format("Y-m-d H:i:s")."...Proposal ID=".$this->_getParam('proposalId')."...";

        // main
        $this->_session->proposalId = $this->_getParam('proposalId');

        $proposalRow = $this->_toProposalRow(false);
        $this->_wizard->init();

        $accountRow = $account = Zend_Auth::getInstance()->getIdentity();
        if ($this->getRequest()->isPost())
        {
            $this->view->formSubmittedPostData = $this->_getAllParams();
            try {
                //init
                $proposalRow = $this->_toProposalRow(false);
                $allParams = $this->_getAllParams();
                if (!isset($allParams['contactPersonId']) || !$allParams['contactPersonId'])
                {
                    $contactPersonRow = $accountRow->getContactPersonRow();
                    $allParams['contactPersonId'] = $contactPersonRow->getId();
                }

                // log postdata
                $parameters = "";
                foreach($allParams as $key => $value) {
                    if (is_array($value)) {
                        foreach ($value as $k=>$v) {
                          $parameters .=   "\t\t\t".$k.":".$v."\n";
                        }
                    } else {
                        $parameters .= "\t\t".$key.":".$value."\n";
                    }
                }
                $message .= "\n\t"."======================== PARAMS =========. "
                           ."\n\t".$parameters ."\n";

                if ($accountRow->isProposalBuilder() && $accountRow->getId() != $clientCompanyRow->createdAccountId) {
                    throw new OSDN_Exception('Beperkte toegang tot de relatie-onderneming');
                }

                $this->view->formSubmittedPostData = $allParams;
                $message .=  "\n\t"."======================== SUBMITTED DATA =========. "
                            ."\n\t". $allParams ."\n";


                $this->view->formSubmittedPostData = $allParams;
                if (null === $proposalRow) {
                    $proposalRow = $this->_service->create($allParams);
                    $this->_session->proposalId = $proposalRow->getId();
                    $this->_helper->information(array('Offerte "%s" aangemaakt.', array($proposalRow->getName())), true, E_USER_NOTICE);
                } else {
                    $proposalRow = $this->_service->update($proposalRow, $allParams);
                    $this->_session->proposalId = $proposalRow->getId();
                    $this->_helper->information(array('Offerte "%s" bijgewerkt.', array($proposalRow->getName())), true, E_USER_NOTICE);
                }
                $this->view->success = true;
                if (empty($this->view->formSubmittedPostData['next-step'])) {
                    $message .=  "\n\t"."====ERROR==. "."\n\t'Offerte bijgewerkt.".$proposalRow->getName()."\n";
                }
            } catch (\OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            }
            if (null !== $proposalRow) {
                $this->_session->proposalId = $proposalRow->getId();
            }
            if (!empty($this->view->formSubmittedPostData['next-step'])) {
                $this->_redirect($this->_wizard->getNextStepUrl());
            }

            // save log
            fwrite($file,$message);
        }

        if ($accountRow->isAdmin()) {
            if (null === $proposalRow) {
                $this->_redirect('/ap-default');
            }

            $companyRow = $proposalRow->getCompanyRow();
        } else {
            $companyRow = $accountRow->getCompanyRow();
        }
        $this->view->companyRow = $companyRow;

        if (null !== $proposalRow) {
            $this->view->proposalRow = $proposalRow;
            try {
                $themeRow = $proposalRow->getThemeRow();
            } catch (Exception $e) {
                $themeRow = $companyRow->getProposalThemeRow();
            }
        } else {
            $themeRow = $companyRow->getProposalThemeRow();
        }
        $this->view->themeRow = $themeRow;

        $customTemplateRow = $this->view->themeRow->findDependentRowset('Proposal_Model_DbTable_CustomTemplate')->current();
        $this->view->customTemplateRow = $customTemplateRow;

        // get user locations
        $locations = $this->_account->getContactPersonRow()->getLocations(true);
        $this->locations = array();
        foreach($locations as $locationRow)
        {
            $this->locations[] = array(
                'location' => $locationRow,
                'themes' => $this->_account->getContactPersonRow()->getLocationThemes($locationRow),
            );
        }
        $this->view->locations = $this->locations;
    }

    public function relationAction()
    {
        // validate proposal
        $this->_wizard->setCurrent($this->_wizard->getStepRelation());
        if (null === ($proposalRow = $this->_toProposalRow(false))) {
            $this->_helper->information('Offerte details dienen eerst ingevuld te worden.', true, E_USER_ERROR);
            $this->_redirect($this->_wizard->getFirstStepUrl());
        }
        if (!$proposalRow->contactMomentDate)
        {
            $this->_helper->information('Voeg eerst een contactmoment toe.', true, E_USER_ERROR);
            $this->_redirect($this->_wizard->getPrevStepUrl().'/proposalId/'.(int)$proposalRow->getId());
        }
        if (!$proposalRow->contactPersonId)
        {
            $this->_helper->information('Voeg eerst een contactpersoon toe.', true, E_USER_ERROR);
            $this->_redirect($this->_wizard->getPrevStepUrl().'/proposalId/'.(int)$proposalRow->getId());
        }
        if (!$proposalRow->approvedAccountId)
        {
            $this->_helper->information('Voeg eerst een ondertekenaar toe.', true, E_USER_ERROR);
            $this->_redirect($this->_wizard->getPrevStepUrl().'/proposalId/'.(int)$proposalRow->getId());
        }

        // start
        if (!file_exists($this->_logFile)) {
            fopen($this->_logFile,"w");
        }
        $file = fopen($this->_logFile, "a");
        $date = new DateTime();
        $message = $date->format("Y-m-d H:i:s")."...Proposal ID=".$this->_getParam('proposalId')."...";

        $accountRow = $account = Zend_Auth::getInstance()->getIdentity();
        if ($this->getRequest()->isPost())
        {
            $this->view->formSubmittedPostData = $this->_getAllParams();
            $proposalRow = $this->_toProposalRow(false);
            $allParams = $this->_getAllParams();

            // log parameters
            $parameters = "";
            foreach($allParams as $key => $value) {
                if (is_array($value)) {
                    foreach ($value as $k=>$v) {
                      $parameters .=   "\t\t\t".$k.":".$v."\n";
                    }
                } else {
                    $parameters .= "\t\t".$key.":".$value."\n";
                }
            }
            $message .= "\n\t"."========================PARAMS =========. "
                       ."\n\t". $parameters ."\n";

            try {
                // create or find clientCompany (relation)
                $companyService = new \Company_Service_Company();
                $clientCompanyId = $this->_getParam('clientCompanyId');
                if (empty($clientCompanyId)) {
                    $e = new OSDN_Validate_Exception();
                    $e->addMessage('Client should be selected', array(), 'clientCompanyId');
                    throw $e;
                }
                if (    $clientCompanyId === 'newItem'
                    &&  isset($allParams['newClient'])) {
                    $allParams['accountId'] = $accountRow->getId();
                    $cParams = $allParams['newClient'];

                    $companyRow = $accountRow->getCompanyRow();

                    $cParams['parentId'] = $companyRow->getId();
                    $cParams['companyId'] = $companyRow->getId();
                    $cParams['accountId'] = $accountRow->getId();

                    $clientCompanyRow = $companyService->create($cParams);
                } else {
                    $clientCompanyRow = $companyService->find($clientCompanyId);
                }
                if ($accountRow->isProposalBuilder() && $accountRow->getId() != $clientCompanyRow->createdAccountId) {
                    throw new OSDN_Exception('Beperkte toegang tot de relatie-onderneming');
                }

                $clientCompanyId = (int) $clientCompanyRow->getId();
                $allParams['clientCompanyId'] = $clientCompanyId;
                $proposalRow = $this->_service->update($proposalRow, $allParams);
                $this->view->success = true;
                if (empty($this->view->formSubmittedPostData['next-step'])) {
                    $this->_helper->information('Relatie toegevoegd', true, E_USER_NOTICE);
                }
            } catch(Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessage());
            }
            try {
                if (!isset($clientCompanyRow) || !$clientCompanyRow)
                {
                    throw new OSDN_Exception("Het bedrijf is niet aangemaakt.");
                }
                // create or find relation contactperson
                if ((empty($allParams['clientContactPersonId']) || 'newItem' == $allParams['clientContactPersonId'])
                    && isset($allParams['newClientContactPerson'])
                    && is_array($allParams['newClientContactPerson']))
                {
                    $cParams['contactPerson']['create0'] = $allParams['newClientContactPerson'];
                    $bulkOperation = new Company_Service_CompanyBulkOperation($clientCompanyRow);
                    $bulkOperation->bulk($cParams);

                    $companyContactPersonRow = $clientCompanyRow->getCompanyContactPersonRow();
                    if (!empty($companyContactPersonRow)) {
                        $allParams['clientContactPersonId'] = $companyContactPersonRow->getId();
                    }
                }
                $message .=  "\n\t"."========================SUBMITTED DATA =========. "
                            ."\n\t". $allParams ."\n";
                $this->view->formSubmittedPostData = $allParams;

                // update proposal
                $proposalRow = $this->_service->update($proposalRow, $allParams);
                $this->view->success = true;
                if (empty($this->view->formSubmittedPostData['next-step'])) {
                    $this->_helper->information('Relatie contactpersoon bijgewerkt', true, E_USER_NOTICE);
                }
            } catch (\OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            }

            if (null !== $proposalRow)
            {
                $this->_session->proposalId = $proposalRow->getId();
            }
            if (    !empty($this->view->formSubmittedPostData['next-step'])
                &&  $proposalRow->clientCompanyId
                &&  $proposalRow->clientContactPersonId)
            {
                // redirect to next page
                $this->_redirect($this->_wizard->getNextStepUrl());
            }
            fwrite($file,$message);
//            $this->_redirect($this->view->url());
        }

        if ($accountRow->isAdmin()) {
            if (null === $proposalRow) {
               $this->_redirect('/ap-default');
            }
            $companyRow = $proposalRow->getCompanyRow();
        } else {
            $companyRow = $accountRow->getCompanyRow();
        }
        $this->view->companyRow = $companyRow;

        if (null !== $proposalRow) {
            $this->view->proposalRow = $proposalRow;
        }
    }

    public function personalIntroductionAction()
    {
        // validate proposal
        $this->_wizard->setCurrent($this->_wizard->getStepPersonalInto());
        if (null === ($proposalRow = $this->_toProposalRow(false)) || null === $proposalRow->getThemeRow()) {
            $this->_helper->information('Offerte details dienen eerst ingevuld te worden.', true, E_USER_ERROR);
            $this->_redirect($this->view->wizard->getFirstStepUrl());
        }
        if (!$proposalRow->clientCompanyId)
        {
            $this->_helper->information('Voeg eerst een relatie toe.');
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }
        if (!$proposalRow->clientContactPersonId)
        {
            $this->_helper->information('Voeg eerst een contactpersoon toe.');
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }

        if ($proposalRow->getThemeRow()->includePersonalIntroduction == 0) {
            $this->_redirect($this->_wizard->getNextStepUrl());
        }

        // start
        $section = new Proposal_Service_Section();
        $this->view->regions = $section->fetchRegions();

        $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
        if ($this->getRequest()->isPost()) {
            $this->view->formSubmittedPostData = $this->_getAllParams();
            if ($this->_account->acl('proposal','introduction'))
            {

                $ptService = new Proposal_Service_ProposalTheme($companyRow);
                $ptService->update($proposalRow->getThemeRow(), $this->_getParam('theme'));
                $this->_helper->information(array('Offerte "%s" bijgewerkt.', array($proposalRow->getName())), true, E_USER_NOTICE);
            }
            if (!empty($this->view->formSubmittedPostData['next-step']))
            {
                $this->_redirect($this->_wizard->getNextStepUrl());
            }
        }

        if (null !== $proposalRow) {
            $this->view->proposalRow = $proposalRow;
            try {
                $this->view->themeRow = $proposalRow->getThemeRow();
            } catch (Exception $e) {
                $this->view->themeRow = $companyRow->getProposalThemeRow();
            }
        } else {
            $this->view->themeRow = $companyRow->getProposalThemeRow();
        }

        $sectionSrv = new \Proposal_Service_Section();
        $sectionRow = $sectionSrv->findByLuid('content');

        $templateSrv = new \Proposal_Service_Template();
        $css = $templateSrv->getCssStyles($sectionRow, $this->view->themeRow);

        $this->view->css = $css;
        $this->view->accountRow = Zend_Auth::getInstance()->getIdentity();
    }

    public function priceAction()
    {
        // validate proposal
        $this->_wizard->setCurrent($this->_wizard->getStepPrice());
        if (null === ($proposalRow = $this->_toProposalRow(false)) || null === $proposalRow->getThemeRow()) {
            $this->_helper->information('Offerte details dienen eerst ingevuld te worden.', true, E_USER_ERROR);
            $this->_redirect($this->view->wizard->getFirstStepUrl());
        }
        if (!$proposalRow->clientCompanyId)
        {
            $this->_helper->information('Voeg eerst een relatie toe.');
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }
        if (!$proposalRow->clientContactPersonId)
        {
            $this->_helper->information('Voeg eerst een contactpersoon toe.');
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }

        if ($proposalRow->getThemeRow()->includePrice == 0) {
            $this->_redirect($this->_wizard->getNextStepUrl());
        }

        // start
        $section = new Proposal_Service_Section();
        $this->view->regions = $section->fetchRegions();

        $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
        if ($this->getRequest()->isPost()) {
            $this->view->formSubmittedPostData = $this->_getAllParams();

            $ptService = new Proposal_Service_ProposalTheme($companyRow);
            $ptService->update($proposalRow->getThemeRow(), $this->_getAllParams());
            $this->_helper->information(array('Offerte "%s" bijgewerkt.', array($proposalRow->getName())), true, E_USER_NOTICE);

            if (!empty($this->view->formSubmittedPostData['next-step'])) {
                $this->_redirect($this->_wizard->getNextStepUrl());
            }
        }


        if (null !== $proposalRow) {
            $this->view->proposalRow = $proposalRow;
            try {
                $themeRow = $proposalRow->getThemeRow();
            } catch (Exception $e) {
                $themeRow = $companyRow->getProposalThemeRow();
            }
        } else {
            $themeRow = $companyRow->getProposalThemeRow();
        }
        $this->view->themeRow = $themeRow;
    }

    public function savePriceAction()
    {

        if (null === ($proposalRow = $this->_toProposalRow(false)) || null === $proposalRow->getThemeRow()) {
            return $this->_helper->information('Proposal details and client information should be filled in first.', true, E_USER_ERROR);
        }

        if ($this->getRequest()->isPost()) {
            try {
                $companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
                $ptService = new Proposal_Service_ProposalTheme($companyRow);
                $ptService->update($proposalRow->getThemeRow(), $this->_getAllParams());
                $this->view->success = true;
                $this->_helper->information(array('Offerte "%s" bijgewerkt.', array($proposalRow->getName())), true, E_USER_NOTICE);
            } catch (\Exception $e) {
                $this->view->success = false;
                return $this->_helper->information('Fout bij het opslaan van het offerte prijsblad.');
            }
        }

    }

    public function blocksAction()
    {
        // get proposal

        // validate proposal
        $this->_wizard->setCurrent($this->_wizard->getStepBlocks());
        if (null === ($proposalRow = $this->_toProposalRow(false))) {
            $this->_helper->information('Offerte details dienen eerst ingevuld te worden.', true, E_USER_ERROR);
            $this->_redirect($this->_wizard->getFirstStepUrl());
        }
        if (!$proposalRow->clientCompanyId)
        {
            $this->_helper->information('Voeg eerst een relatie toe.');
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }
        if (!$proposalRow->clientContactPersonId)
        {
            $this->_helper->information('Voeg eerst een contactpersoon toe.');
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }
        $_proposalOffers = new Proposal_Service_ProposalOffer();
        $offers = $_proposalOffers->fetchAll($proposalRow->getId());
        if ($proposalRow->getThemeRow()->includePrice && !count($offers))
        {
            $this->_helper->information('Voeg eerst een product of dienst toe.');
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }

        // start
        $this->view->proposalRow = $proposalRow;
        $this->view->companyRow = $proposalRow->getCompanyRow();

        // get theme and library
        $themeRow = $proposalRow->getThemeRow();
        $this->view->includePrice = (bool) $themeRow->includePrice;
        $this->view->libraryRow = $themeRow->getLibrariesRowset()->current();

        // get categories
        $categoryService = new \ProposalBlock_Service_Category();
        $this->view->categoryRowset = $categoryService->fetchAllRootCategoriesWithResponse();
        $section = new Proposal_Service_Section();
        $this->view->sectionCollapsedByRegionRowset = $section->fetchAllCollapsedByRegion();
        $this->view->regions = $section->fetchRegions();
        if (($proposalRow = $this->_toProposalRow()) != null) {
            $psp = new Proposal_Service_ProposalBlockDefinition($proposalRow);
            $rowset = $psp->fetchAll();
            $result = array();
            if (!empty($rowset)) {
                foreach ($rowset as $row) {
                    $result[$row->proposalBlockId] = $row;
                }
            }
            $this->view->definitions = $result;
        } else {
            $this->view->definitions = array();
        }
    }

    public function summaryAction()
    {

        // validate proposal
        $this->_wizard->setCurrent($this->_wizard->getStepSummary());
        if (null === ($proposalRow = $this->_toProposalRow(false))) {
            $this->_helper->information('Offerte details dienen eerst ingevuld te worden.', true, E_USER_ERROR);
            $this->_redirect($this->_wizard->getFirstStepUrl());
        }
        if (!$proposalRow->clientCompanyId)
        {
            $this->_helper->information('Voeg eerst een relatie toe.');
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }
        if (!$proposalRow->clientContactPersonId)
        {
            $this->_helper->information('Voeg eerst een contactpersoon toe.');
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }
        $_proposalOffers = new Proposal_Service_ProposalOffer();
        $offers = $_proposalOffers->fetchAll($proposalRow->getId());
        if ($proposalRow->getThemeRow()->includePrice && !count($offers))
        {
            $this->_helper->information('Voeg eerst een product of dienst toe.');
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }
        $proposalBlockDefSrv = new \Proposal_Service_ProposalBlockDefinition($proposalRow);
        if ($proposalBlockDefSrv->fetchBlocksCount() == 0)
        {
            $this->_helper->information('Stel eerst uw offerte samen.');
            $this->_redirect($this->_wizard->getPrevStepUrl());
        }

        $this->view->proposalRow = $proposalRow;

        $supportTicketSrv = new SupportTicket_Service_SupportTicketService();
        $checkServiceRow = $supportTicketSrv->findByLuid('checkProposal');
        $checkServicePrice = null;
        if (null !== $checkServiceRow) {
            $checkServicePrice = $supportTicketSrv->fetchPrice(array(
               'serviceId' => $checkServiceRow['id']
            ));
        }

        $this->view->createTicket = (null !== $checkServiceRow && null !== $checkServicePrice);

        if ($this->getRequest()->isPost()) {
            $this->view->formSubmittedPostData = $this->_getAllParams();

            try {
                $proposalRow = $this->_service->update($proposalRow, $this->_getAllParams());
                $this->view->proposalId = $proposalRow->id;
                $this->view->success = true;
                if (empty($this->view->formSubmittedPostData['next-step'])) {
                    $this->_helper->information(array('Offerte "%s" bijgewerkt.', array($proposalRow->getName())), true, E_USER_NOTICE);
                } else {
                    $this->_redirect($this->view->wizard->getNextStepUrl());
                }
            } catch (\Exception $e) {
                $this->view->success = false;
                $this->_helper->information(array('Fout bij het opslaan van het offerte "%s".', array($proposalRow->getName())), true);
            }
        }

        $section = new Proposal_Service_Section();
        $this->view->regions = $section->fetchRegions();
    }

    public function saveStateAction()
    {
        $this->view->success = false;
        $proposalRow = $this->_toProposalRow();

        try {
            $params = $this->_getAllParams();

            $themeRow = $proposalRow->getThemeRow();
            $themeParams = array(
                'includeTOC' => isset($params['includeTOC']) ? $params['includeTOC'] : 1,
                'pricePosition' => isset($params['pricePosition']) ? $params['pricePosition'] : null,
            );

            if (!empty($params['removePrice'])) {
                $themeParams['includePrice'] = false;
            }

            $themeRow->setFromArray($themeParams);
            $themeRow->save();

            $this->view->success = true;
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        } catch(\Exception $e) {
            $this->_helper->information('Kan met het voorstel van blokken op te slaan.');
        }
    }

    public function downloadPdfAction()
    {
        $proposalRow = $this->_toProposalRow(false);
        $proposalTheme = $proposalRow->getThemeRow();
        $totalCost = 0;

        if ($proposalTheme->includePrice) {
            $proposalOfferSrv = new \Proposal_Service_ProposalOffer();
            $totalCost = $proposalOfferSrv->getTotalCost($proposalRow->getId());
        }

        if (null === $proposalRow) {
            return $this->_redirect('/proposal/create/index');
        }

        $proposalBlockDefSrv = new \Proposal_Service_ProposalBlockDefinition($proposalRow);

        if ($this->_getParam('regionId')) {
            $sectionSrv = new \Proposal_Service_Section();
            $sections = $sectionSrv->findByRegion($this->_getParam('regionId'));
        }

        if (!empty($sections)) {
            $blocksCount = 0;
            foreach ($sections as $section) {
                $blocksCount += $proposalBlockDefSrv->fetchBlocksCount($section->getId());
            }
        } else {
            $blocksCount = $proposalBlockDefSrv->fetchBlocksCount();
        }

        if ( $blocksCount > 0 || (Proposal_Service_Section::REGION_PROPOSAL === $this->_getParam('regionId') && $proposalTheme->includePrice && $totalCost)) {
            $this->view->layout()->disableLayout();
            $service = new Proposal_Service_Prince($proposalRow);
            $service->generate($this->_getParam('regionId'));
            die;
        } else {
            if (!empty($sections)) {
                $this->_helper->information(array('Sectie "%s" is leeg.', array($sections[0]->getName())));
            } else {
                $this->_helper->information(array('Offerte "%s" is leeg.', array($proposalRow->getName())));
            }
            $this->_redirect('/proposal/create/' . $this->_getParam('return', 'index') . '/proposalId/' . $proposalRow->getId());
        }

    }

    protected function _createDefinition()
    {
        $proposalRow = $this->_toProposalRow(false);
        $proposalBlockSrv = new \ProposalBlock_Service_ProposalBlock($proposalRow->getCompanyRow());
        $blockRow = $proposalBlockSrv->find($this->_getParam('blockId'));
        $definitionSrv = new \Proposal_Service_ProposalBlockDefinition($proposalRow);

        $sectionId = $this->_getParam('sectionId');
        if (!$sectionId) {
            $sectionSrv = new \Proposal_Service_Section();
            $sectionRow = $sectionSrv->findByRelatedCategoryId($blockRow->getCategoryId(true));
            $sectionId = $sectionRow->getId();
        }

        $params = $blockRow->toArray();
        $params['position'] = $this->_getParam('position');

        return $definitionSrv->createDefinitionRow($blockRow, $sectionId, $params);
    }

    public function updateDefinitionAction()
    {
        $proposalRow = $this->_toProposalRow(false);
        $definitionSrv = new \Proposal_Service_ProposalBlockDefinition($proposalRow);

        // get themeRow
        $themeRow = $proposalRow->getThemeRow();
        $this->view->themeRow = $themeRow;

        // get libraryRow
        $libraryRow = $themeRow->getLibrariesRowset()->current();
        $this->view->libraryRow = $libraryRow;

        try {
            $params = $this->_getAllParams();

            if (!empty($params['blockDefinitions']) && is_array($params['blockDefinitions'])) {
                if ($this->getRequest()->isPost()) {
                    foreach ($params['blockDefinitions'] as $blockId => $p) {
                        $blockDefinitionRow = $definitionSrv->find($blockId);
                        $definitionSrv->update($blockDefinitionRow, $p);
                    }
                }
            } else {
                $blockDefinitionRow = $definitionSrv->find($params['blockDefinitionId']);

                if ($this->getRequest()->isPost()) {
                    $definitionSrv->update($blockDefinitionRow, $params);
                }
            }


            $this->view->success = true;
        } catch (\Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }

        $this->view->parentBlock = $blockDefinitionRow->getParentBlock();
        $this->view->blockDefinitionRow = $blockDefinitionRow;
    }

    public function createDefinitionAction()
    {
        if ($this->getRequest()->isPost()) {
            try {
                $row = $this->_createDefinition();
                $this->view->row = $row->toArray();
                $this->view->success = true;
            } catch (\Exception $e) {
                $this->view->success = false;
                $this->view->error = $e->getMessage();
            }
        }
    }

    public function previewDefinitionAction()
    {
        $proposalRow = $this->_toProposalRow();
        $definitionSrv = new \Proposal_Service_ProposalBlockDefinition($proposalRow);
        $company = $proposalRow->getCompanyRow();
        $this->view->company = $company;

        if ($this->_getParam('iframe')) {
            $this->view->blockRow = $definitionSrv->find($this->_getParam('blockId'));

            $sectionSrv = new \Proposal_Service_Section();
            $sectionRow = $sectionSrv->find($this->_getParam('sectionId'));

            $templateSrv = new \Proposal_Service_Template();

            $themeSrv = new \Proposal_Service_ProposalTheme($company);
            $themeRow = $themeSrv->getDefaultRow();

            $cssArr = $templateSrv->getCssStyles($sectionRow, $themeRow);
            $this->view->css = !empty($cssArr) ? explode(',', $cssArr) : array();

            $this->view->layout()->disableLayout();

            return $this->render('preview-text-iframe');
        } else {
            $blockDefinitionRow = $definitionSrv->find($this->_getParam('blockDefinitionId'));

            if (null === $blockDefinitionRow) {
                $blockDefinitionRow = $this->_createDefinition();
            }

            $this->view->blockRow = $blockDefinitionRow;
        }

    }

    public function deleteDefinitionAction()
    {
        if ($this->getRequest()->isPost()) {
            $proposalRow = $this->_toProposalRow(false);
            $definitionSrv = new \Proposal_Service_ProposalBlockDefinition($proposalRow);

            try {
                $definitionSrv->delete($this->_getParam('blockDefinitionId'));
                $this->view->success = true;
            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            }
        }
    }

    public function getThemes()
    {
        try {
            $this->themes = array();
            $this->view->success = true;
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

}