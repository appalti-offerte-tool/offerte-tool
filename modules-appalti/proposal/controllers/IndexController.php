<?php

class Proposal_IndexController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Proposal_Service_Proposal
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_service = new \Proposal_Service_Proposal();
        $this->_helper->ContextSwitch()
            ->addActionContext('update', 'json')
            ->initContext();

        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'proposal';
    }

    public function indexAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(20);

        $params = $this->_getAllParams();
        $cookie = isset($_COOKIE["proposalStatusFilter"]) ? $_COOKIE["proposalStatusFilter"] : null;

        if (!empty($params['statusFilter']) || (isset($params['statusFilter']) && empty($params['statusFilter']))) {
            setcookie('proposalStatusFilter', $params['statusFilter'], time()+60*60*24*3, '/');
        } elseif($cookie) {
            $params['statusFilter'] = $cookie;
        }

        $response = $this->_service->fetchAll($params);
        $this->view->rowset = $response->getRowset();
        $this->view->proposalId = $this->_getParam('proposalId');
        $this->view->statusFilter = !empty($params['statusFilter']) ? $params['statusFilter'] : null;

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }

    public function clientProposalsAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(20);
        $response = $this->_service->fetchAllByCompanyId($this->_getAllParams());

        $this->view->companyId = $this->getRequest()->getParam('companyId');
        $this->view->rowset = $response->getRowset();
        $this->view->return = $this->getRequest()->getParam('return',null);

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }

    public function viewAction()
    {
        $id = $this->_getParam('proposalId');

        if (!$id) {
            $this->_helper->information('Offerte niet is opgegeven. Selecteer offerte van onderstaande lijst.', true, E_USER_WARNING);
            $this->_redirect('/proposal/index/index');
        }

        try {
            $row = $this->_service->find($id);
            $this->view->proposalRow = $row;
            $this->view->themeRow = $row->getThemeRow();

            $offerSrv = new \Proposal_Service_ProposalOffer();
            $this->view->offers = $offerSrv->fetchAll($row->getId());

            $sections = $this->_service->getProposalSections();
            $this->view->sections = $sections;

        } catch (\Exception $e) {
            $this->_helper->information($e->getMessage());
        }
    }

    public function personalIntroAction()
    {
        $id = $this->_getParam('proposalId');

        if (!$id) {
            $this->_helper->information('Offerte niet is opgegeven. Selecteer offerte van onderstaande lijst.', true, E_USER_WARNING);
            $this->_redirect('/proposal/index/index');
        }

        $sectionSrv = new \Proposal_Service_Section();
        $sectionRow = $sectionSrv->findByLuid('content');

        $proposalRow = $this->_service->find($id);
        $themeRow = $proposalRow->getThemeRow();
        $this->view->themeRow = $themeRow;

        $templateSrv = new \Proposal_Service_Template();
        $cssArr = $templateSrv->getCssStyles($sectionRow, $themeRow);
        $this->view->css = !empty($cssArr) ? explode(',', $cssArr) : array();

        $this->view->layout()->disableLayout();

        return $this->render('personal-intro-iframe');
    }

    public function changeStatusAction()
    {
        if ($this->_getParam('proposalId')) {
            try {
                $row = $this->_service->find($this->_getParam('proposalId'));
                $this->view->success = true;
                $this->view->row = $row;
            } catch (\Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessage());
            }
        } else {
            $this->view->success = false;
            $this->_helper->information('Wrong input data. Proposal id is not specified.');
        }
    }

    public function updateAction()
    {
        $proposalId = $this->_getParam('proposalId');

        if (!$proposalId) {
            $this->_helper->information('Offerte niet is opgegeven. Selecteer offerte van onderstaande lijst.', true, E_USER_WARNING);
            $this->_redirect('/proposal/index/index');
        }

        try {
            $row = $this->_service->find($proposalId);
            $this->_service->update($row, $this->_getAllParams());
            $this->view->proposalRow = $row;
            $this->view->success = true;
            $this->_helper->information(array('Offerte "%s" bijgewerkt.', array($row->getName())), true, E_USER_NOTICE);
        } catch (\Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessage());
        }

        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_redirect('/proposal/index/view/proposalId/' . $row->getId());
        }
    }

    public function getPdfAction()
    {
        $path = base64_decode($this->_getParam('path'));
        $fileInfo = pathinfo($path);
        header('Content-disposition: attachment; filename="'.$fileInfo['filename'].'.'. $fileInfo['extension'].'"');
        header('Content-type: application/pdf');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($path));
        header('Pragma: no-cache');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        readfile($path);
        die();
    }

    public function downloadPdfAction()
    {
        $this->view->layout()->disableLayout();

        $proposalId = $this->_getParam('proposalId');

        if (empty($proposalId)) {
            return ;
        }

        $proposalRow = $this->_service->find($proposalId, false);

        if (null === $proposalRow) {
            return ;
        }

        $proposalBlockDefSrv = new \Proposal_Service_ProposalBlockDefinition($proposalRow);
        $blocksCount = $proposalBlockDefSrv->fetchBlocksCount();

        if ($blocksCount > 0) {
            $this->view->layout()->disableLayout();
            $service = new Proposal_Service_Prince($proposalRow);
            $service->generate();
            die;
        } else {
            $this->_helper->information(array('Proposal "%s" is empty.', array($proposalRow->getName())), true, E_USER_NOTICE);
            $this->_redirect(base64_decode($this->_getParam('return')));
        }

    }

    public function deleteAction()
    {
       $proposalId = $this->_getParam('proposalId');

        try {
            $proposalRow = $this->_service->find($proposalId);
            $name = $proposalRow->getName();
            $this->_service->delete($proposalRow);
            $this->_helper->information(array('Offerte "%s" is verwijderd', array($name)), true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
        $this->_redirect('/proposal/index/index');
    }

    public function sendAction()
    {
        $this->view->layout()->disableLayout();

        $proposalId = $this->_getParam('proposalId');

        if (empty($proposalId)) {
            $this->_helper->information('Offerte niet is opgegeven. Selecteer offerte van onderstaande lijst.', true, E_USER_WARNING);
        } else {
            $proposalRow = $this->_service->find($proposalId, false);
            if (null === $proposalRow) {
                $this->_helper->information('Ongeldige offerte. Selecteer offerte van onderstaande lijst.', true, E_USER_WARNING);
            } else {
                try {
                    $service = new Proposal_Service_Prince($proposalRow);
                    $path = $service->generate(null, false);
                    $createNotify = new Proposal_Misc_Email_Proposal();
                    $this->view->success = (boolean) $createNotify->notificate(array(
                        'proposalRow' => $proposalRow,
                        'path' => $path
                    ));
                    $this->_helper->information(array('Offerte "%s" succesvol verzonden.', array($proposalRow->getName())), true, E_USER_NOTICE);
                } catch (\Exception $e) {
                    $this->_helper->information(array('Fout bij het verzenden van het offerte "%s". Error: %s', array($proposalRow->getName(), $e->getMessage())), true);
                }
            }
        }

        $this->_redirect('/proposal/index/');
    }

}