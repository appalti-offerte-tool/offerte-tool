<?php

class Proposal_MainController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Proposal_Service_ProposalMain
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_helper->ajaxContext()
            ->addActionContext('delete', 'json')
            ->addActionContext('send', 'json')
            ->addActionContext('fetch-all', 'json')
            ->initContext();

        $this->_service = new \Proposal_Service_ProposalMain();
        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'proposal';
    }

    public function fetchAllAction()
    {

        $response = $this->_service->fetchAll($this->_getAllParams());
        $this->view->assign($response->toArray());

    }





    public function downloadPdfAction()
    {
        $this->view->layout()->disableLayout();

        $proposalId = $this->_getParam('proposalId');

        if (empty($proposalId)) {
            return ;
        }

        $proposalRow = $this->_service->find($proposalId, false);

        if (null === $proposalRow) {
            return ;
        }

        $service = new Proposal_Service_Prince($proposalRow);
        $service->generate();
        die;
    }

    public function deleteAction()
    {
       $proposalId = $this->_getParam('proposalId');

        try {
            $proposalRow = $this->_service->find($proposalId);
            $proposalName = $proposalRow->luid;
            $this->_service->delete($proposalRow);
            $this->view->success = true;
            $this->_helper->information(array('"%s" proposal deleted', array($proposalName)), true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }

    }

    public function sendAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $proposalId = $this->_getParam('proposalId');

        if (empty($proposalId)) {
            $this->_helper->information('Proposal is not defined. Select a proposal to be sent from list.', true, E_USER_WARNING);
        } else {
            $proposalRow = $this->_service->find($proposalId, false);
            if (null === $proposalRow) {
                $this->_helper->information('Invalid proposal. Select a proposal to be sent from list.', true, E_USER_WARNING);
            } else {
                try {

                    $service = new Proposal_Service_Prince($proposalRow);
                    $path = $service->generate(null, false);
                    $createNotify = new Proposal_Misc_Email_Proposal();
                    $this->view->success = (boolean) $createNotify->notificate(array(
                        'proposalRow' => $proposalRow,
                        'path' => $path
                    ));
                    $this->_helper->information(array('Proposal #%s sent successfully.', array($proposalRow->luid)), true, E_USER_NOTICE);
                } catch (\Exception $e) {
                    $this->view->success = false;
                    $this->_helper->information(array('Error sending proposal #%s. Error: %s', array($proposalRow->luid, $e->getMessage())), true);
                }
            }
        }

    }

}