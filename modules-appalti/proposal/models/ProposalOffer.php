<?php

class Proposal_Model_ProposalOffer extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    private $__stringValueRepresentation;

    /**
     * @var Proposal_Model_Proposal
     */
    protected $_proposalRow;

    /**
     * @var Company_Model_OfferRow
     */
    protected $_companyOffer;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    public function getProposalRow()
    {

        if (empty($this->proposalId)) {
            return null;
        }

        if (null === $this->_proposalRow) {
            $this->_proposalRow = $this->findParentRow('Proposal_Model_DbTable_Proposal', 'Proposal');
        }

        return $this->_proposalRow;
    }


    public function getOfferRow()
    {

        if (empty($this->proposalId)) {
            return null;
        }

        if (null === $this->_proposalRow) {
            $this->_proposalRow = $this->findParentRow('Company_Model_DbTable_Offer', 'CompanyOffer');
        }

        return $this->_proposalRow;
    }


    public function __clone()
    {
        $this->id = null;
        $this->_cleanData = array();
        $this->_modifiedFields = array_combine(array_keys($this->_data), array_fill(0, count($this->_data), true));
    }
}