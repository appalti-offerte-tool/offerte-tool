<?php

class Proposal_Model_Section implements \OSDN_Application_Model_Interface
{
    const SECTION_INTRODUCTION  = 'introduction';
    const SECTION_COVER         = 'cover';
    const SECTION_CONTENT       = 'content';
    const SECTION_INBETWEEN     = 'in-between';
    const SECTION_TOC           = 'toc';
    const SECTION_ATTACHMENT    = 'attachment';

    protected static $_properties = array(
        'id',
        'databaseId',
        'position', 'isRequired',
        'name', 'regionId', 'related', 'luid'
    );

    protected $_data;

    public function __construct(array $data)
    {
        $diff = array_diff(self::$_properties, array_keys($data));
        if (count($diff) > 0) {
            throw new InvalidArgumentException('The arguments does not match');
        }

        $this->_data = $data;
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->_data['id'];
    }

    public function getDatabaseId()
    {
        return $this->_data['databaseId'];
    }

    public function getPosition()
    {
        return $this->_data['position'];
    }

    /**
     * Should contain minimum one block
     */
    public function isRequired()
    {
        return $this->_data['isRequired'];
    }

    public function getName()
    {
        return $this->_data['name'];
    }

    public function getRegionId()
    {
        return $this->_data['regionId'];
    }

    public function getRegionRow()
    {
        return Proposal_Service_Section::findRegion($this->getRegionId());
    }

    public function getDescription()
    {
        return !empty($this->_data['description']) ? $this->_data['description'] : '';
    }

    public function isCalculatable()
    {
        return !empty($this->_data['calculatable']);
    }

    public function useContentCss()
    {
        return !empty($this->_data['calculatable']) || self::SECTION_INBETWEEN === $this->getLuid();
    }

    public function getRelatedId()
    {
        return $this->_data['related'];
    }

    public function getLuid()
    {
        return $this->_data['luid'];
    }

    public function isDefault()
    {
        return !empty($this->_data['isDefault']);
    }

    public function isFitable()
    {
        return in_array($this->getLuid(), array(self::SECTION_INBETWEEN, self::SECTION_INTRODUCTION, self::SECTION_ATTACHMENT));
    }

    /**
     * @param Proposal_Model_Proposal $proposalRow
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getBlockDefinitionsRowset(\Proposal_Model_Proposal $proposalRow)
    {
        $blockDefinitionSrv = new Proposal_Service_ProposalBlockDefinition($proposalRow);
        return $blockDefinitionSrv->fetchAllBySectionId($this->getId());
    }
}