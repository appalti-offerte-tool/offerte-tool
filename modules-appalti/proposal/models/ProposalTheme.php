<?php

class Proposal_Model_ProposalTheme extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    private $__stringValueRepresentation;

    /**
     * @var Proposal_Model_Proposal
     */
    protected $_proposalRow;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    public function isDefault()
    {
        return empty($this->proposalId);
    }

    public function getProposalRow()
    {

        if (empty($this->proposalId)) {
            return null;
        }

        if (null === $this->_proposalRow) {
            $this->_proposalRow = $this->findParentRow('Proposal_Model_DbTable_Proposal', 'Proposal');
        }

        return $this->_proposalRow;
    }

    public function getTemplateThemeBySection(Proposal_Model_Section $sectionRow)
    {
        $name = 'template';

        if ($sectionRow->useContentCss()) {
            $name .= 'Content';
        } else {
            $name .= ucfirst($sectionRow->getLuid());
        }

        return $this->$name ?: 'default';
    }

    public function isBtwVisible()
    {
        return (boolean) $this->showBtw;
    }

    public function includePersonalIntro()
    {
        return (boolean) $this->includePersonalIntroduction && !empty($this->personalIntroduction);
    }

    public function __clone()
    {
        $this->id = null;
        $this->_cleanData = array();
        $this->_modifiedFields = array_combine(array_keys($this->_data), array_fill(0, count($this->_data), true));
    }

    public function getPlainCss()
    {
        ob_start();
        $plain = true;
        require __DIR__ . '/../views/proposal-theme-inline-stylesheet.phtml';
        return ob_get_clean();
    }

    public function getLibrariesRowset()
    {
        // get custom template
        $customTemplateRow = $this->findDependentRowset('Proposal_Model_DbTable_CustomTemplate')
                                    ->current();
        return $customTemplateRow->findManyToManyRowset('Company_Model_DbTable_Library','Company_Model_DbTable_LibraryProposalCustomTemplate');
    }

    public function __toString()
    {
        if (null === $this->__stringValueRepresentation) {
            ob_start();
            require __DIR__ . '/../views/proposal-theme-inline-stylesheet.phtml';
            $this->__stringValueRepresentation = ob_get_clean();
        }

        return $this->__stringValueRepresentation;
    }
}