<?php

class Proposal_Model_CustomTemplate extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    const COMMON_BG_SCOPE       = 'common';
    const INTRO_BG_SCOPE        = 'introduction';
    const CONTENT_BG_SCOPE      = 'content';
    const ATTACHMENT_BG_SCOPE   = 'attachment';
    const DEFAULT_MARGIN        = 10;

    protected $_path = '/modules-appalti/company/data/templates/';

    /**
     * @var Company_Model_CompanyRow
     */
    protected $_companyRow;

    /**
     * @var Proposal_Model_ProposalTheme
     */
    protected $_companyDefaultThemeRow;

    /**
     * @var Proposal_Model_CustomTemplateSettings
     */
    protected $_settingsRow;

    /**
     * @var Proposal_Model_CustomTemplateSettings
     */
    protected $_settingsRowset;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Company_Model_CompanyRow|null
     */
    public function getCompanyRow()
    {
        if (empty($this->companyId)) {
            return null;
        }

        if (null === $this->_companyRow) {
            $this->_companyRow = $this->findParentRow('Company_Model_DbTable_Company', 'Company');
        }

        return $this->_companyRow;
    }

    /**
     * @return Proposal_Model_ProposalTheme
     */
    public function getThemeRow()
    {
        if (null === $this->_companyDefaultThemeRow) {
            $company = $this->getCompanyRow();
            $this->_companyDefaultThemeRow = $company->getProposalThemeRow();
        }

        return $this->_companyDefaultThemeRow;
    }

    public function getSettings()
    {
        if (null === $this->_settingsRowset) {
            $this->_settingsRowset = $this->findDependentRowset('Proposal_Model_DbTable_CustomTemplateSettings', 'Settings');
        }
        return $this->_settingsRowset;
    }

    public function getSettingsByScope($scope = self::COMMON_BG_SCOPE)
    {
        $select = $this->select()->where('scope = ?', $scope);
        return $this->findDependentRowset('Proposal_Model_DbTable_CustomTemplateSettings', 'Settings', $select)->current();
    }

    protected function _base64EncodeBg($file)
    {
        $output = pathinfo($file, PATHINFO_DIRNAME)  . '/' . pathinfo($file, PATHINFO_FILENAME) . '.base64';
        return file_exists($output) ? file_get_contents($output) : '';
    }

    public function getBgByScope($scope, $thumb = true, $base64encode = false)
    {
        $result = '';
        $dir = sprintf('%s%d/%d/backgrounds/', $this->_path, $this->companyId, $this->getId());

        $helper = new Zend_View_Helper_ServerUrl();
        $url = $helper->serverUrl();

        if (file_exists(APPLICATION_PATH . $dir)) {
            $fileName = $thumb ? $scope . '-thumb' : $scope;
            foreach(new DirectoryIterator(APPLICATION_PATH . $dir) as $f) {
                if ($f->isFile() && pathinfo($f->getFilename(), PATHINFO_FILENAME) === $fileName) {
                    $result = $base64encode ?
                                $this->_base64EncodeBg($f->getPathname()) :
                                sprintf('<img class="bg" src="%s" />', $url.'/proposal/template/get-image/companyId/'.$this->companyId.'/id/'.$this->getId().'/scope/'.$scope);
//                                sprintf('<img class="bg" src="%s" />', $dir . $f->getFilename() . '?#dc_' . time());
                    break;
                }
            }
        }

        return $result;
    }

    public static function getScopes()
    {
        return array(
            self::COMMON_BG_SCOPE       => 'Common',
            self::INTRO_BG_SCOPE        => 'Introduction letter',
            self::CONTENT_BG_SCOPE      => 'Proposal',
            self::ATTACHMENT_BG_SCOPE   => 'Attachments'
        );
    }

    public function activate()
    {
        try {
            $this->active = 1;
            $this->save();
        } catch(Exception $e) {
            throw $e;
        }
    }
    public function deactivate()
    {
        try {
            $this->active = 0;
            $this->save();
        } catch(Exception $e) {
            throw $e;
        }
    }
}