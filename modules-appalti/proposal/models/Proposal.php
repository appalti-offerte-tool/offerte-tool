<?php

class Proposal_Model_Proposal extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    const STATUS_IN_PROGRESS = 'in-progress';
    const STATUS_WON         = 'won';
    const STATUS_LOST        = 'lost';
    const STATUS_WITHDRAWN   = 'withdrawn';

    protected $_accountRow;

    protected $_companyRow;

    protected $_themeRow;


    public $includePersonalIntroduction;
    public $includePrice;
    public $countOffers;
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name ?: '#' . $this->luid;
    }

    public function getContactMomentDate()
    {
        if (!empty($this->contactMomentDate)) {
            $date = new DateTime($this->contactMomentDate);
            return $date->format('d-m-Y');
        }

        return '<span class="text-gray">Niet ingesteld</span>';
    }

    public function isEditable()
    {
        $accountRow = Zend_Auth::getInstance()->getInstance()->getIdentity();
        return $accountRow->isAdmin()
               || $accountRow->isCompanyOwner()
               || $accountRow->acl('proposal', 'update')
               || $this->accountId === $accountRow->getId();
    }

    /**
     * returns locationRow
     * @return Company_Model_Company $locationRow
     */
    public function getLocation()
    {
        return $this->findDependentRowset('Company_Model_DbTable_Location', 'Proposal')->current();
    }

    /**
     * returns contactpersonRow
     * @return Company_Model_Company $location
     */
    public function getContactperson()
    {
        return $this->findDependentRowset('Company_Model_DbTable_ContactPerson', 'Proposal')->current();
    }

    /**
     * @return Proposal_Model_ProposalTheme
     */
    public function getThemeRow()
    {
        if (null === $this->_themeRow) {
            $proposalThemeRowset = $this->findDependentRowset('Proposal_Model_DbTable_ProposalTheme', 'Proposal');
            if ($proposalThemeRowset->count() > 1) {
                throw new OSDN_Exception('Data integrity corrupted');
            }

            if ($proposalThemeRowset->count() == 0) {
                throw new OSDN_Exception('No theme is connected to proposal');
            }

            $this->_themeRow = $proposalThemeRowset->current();
        }

        return $this->_themeRow;
    }

    /**
     * @param Proposal_Model_Section $sectionRow
     * @return Zend_Db_Table_Rowset_Abstract     The array of Proposal_Model_ProposalBlockDefinition
     */
    public function fetchAllBlockDefinitionBySection(Proposal_Model_Section $sectionRow)
    {
        $statement = $this->select()
            ->where('sectionId = ?', $sectionRow->getId(), Zend_Db::INT_TYPE)
            ->order('position ASC');

        return $this->findDependentRowset('Proposal_Model_DbTable_ProposalBlockDefinition', 'Proposal', $statement);
    }

    public function fetchAllBlockDefinitionsSerialized()
    {
        $statement = $this->select()->order('position ASC');
        $blocksRowset = $this->findDependentRowset('Proposal_Model_DbTable_ProposalBlockDefinition', 'Proposal', $statement);

        $blocks = array();
        foreach ($blocksRowset as $row) {
            $blockArr = $row->toArray();
            $blockArr['metadataHash'] = md5($row->metadata);
            $blocks[$row->getId()] = $blockArr;
        }

        return serialize($blocks);
    }

    /**
     * @return Proposal_Model_ProposalVersion | null
     */
    public function fetchLastVersion()
    {
        $statement = $this->select()
            ->where('proposalId = ?', $this->getId(), Zend_Db::INT_TYPE)
            ->order('id DESC')
            ->limit(1);

        $row = $this->findDependentRowset('Proposal_Model_DbTable_ProposalVersion', 'Proposal', $statement)->current();

        if ($row) {
            if ($row->isValid()) {
                return $row;
            } else {
                $row->delete();
                return null;
            }
        } else {
            return null;
        }

    }

    /**
     * @param string $olderThan Date in "Y-m-d" format
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function fetchAllVersions($olderThan = null)
    {
        $where = null;

        if (!empty($olderThen) && preg_match('/^(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)$/', $olderThan)) {
            $where = $this->select()
                ->where('createdDateTime < ?', $olderThan);
        }

        return $this->findDependentRowset('Proposal_Model_DbTable_ProposalVersion', 'Proposal', $where);
    }

    /**
     * @return Company_Model_CompanyRow
     */
    public function getClientCompanyRow()
    {
        return $this->findParentRow('Company_Model_DbTable_Company', 'Client');
    }

    /**
     * @return Company_Model_ContactPersonRow
     */
    public function getClientContactPersonRow()
    {
        return $this->findParentRow('Company_Model_DbTable_ContactPerson', 'ClientContactPerson');
    }

    /**
     * @return Company_Model_ContactPersonRow
     */
    public function getContactPersonRow()
    {
        return $this->findParentRow('Company_Model_DbTable_ContactPerson', 'ContactPerson');
    }

    public function getApprovedContactPersonRow()
    {
        $accountRow = $this->findParentRow('Account_Model_DbTable_Account', 'ApprovedAccount');
        $approvedContactPersonRow = $accountRow ? $accountRow->getContactPersonRow() : null;
        return $approvedContactPersonRow;
    }

    /**
     * @return Account_Model_DbTable_AccountRow
     */
    public function getAccountRow()
    {
        if (null === $this->_accountRow) {
            $this->_accountRow = $this->findParentRow('Account_Model_DbTable_Account', 'Account');
        }

        return $this->_accountRow;
    }

    /**
     * @return Company_Model_CompanyRow
     */
    public function getCompanyRow()
    {
        if (null === $this->_companyRow) {
            $this->_companyRow = $this->findParentRow('Company_Model_DbTable_Company', 'Company');
        }

        return $this->_companyRow;
    }

    public function getStatus()
    {
        $created = new DateTime($this->createdDatetime);
        $theme = $this->getThemeRow();
        $validPeriod = new DateInterval('P' . $theme->daysValid . 'D');

        $expires = new DateTime($this->createdDatetime);
        $expires->add($validPeriod);

        $statusOptions = $this->getStatusOptions();
        $val = $this->status ? $this->status : self::STATUS_IN_PROGRESS;
        $status = array(
            'val'       => $val,
            'progress'  => 100,
            'created'   => $created->format('d-m-Y'),
            'expires'   => $expires->format('d-m-Y'),
            'title'     => $statusOptions[$val]
        );

        if ($val == self::STATUS_IN_PROGRESS) {
            $status['title'] = 'Verloopt op: ' . $status['expires'];
            $now = new DateTime();
            if ($now->format('Y-m-d') < $expires->format('Y-m-d') && $theme->daysValid) {
                $diff = $now->diff($expires);
                $status['progress'] = floor((100 * ($theme->daysValid - $diff->days)) / $theme->daysValid);
            } else {
                $status['val'] = 'expired';
                $status['title'] = 'Verlopen op: ' . $status['val'];
            }
        }

        return $status;
    }

    public static function getStatusOptions()
    {
        return array(
            self::STATUS_IN_PROGRESS    => 'Lopend',
            self::STATUS_WON            => 'Gewonnen',
            self::STATUS_LOST           => 'Verloren',
            self::STATUS_WITHDRAWN      => 'Teruggetrokken',
        );
    }
}