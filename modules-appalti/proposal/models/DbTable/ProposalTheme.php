<?php

class Proposal_Model_DbTable_ProposalTheme extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'proposalTheme';

    protected $_rowClass = 'Proposal_Model_ProposalTheme';

    protected $_referenceMap = array(
        'Proposal' => array(
            'columns'           => 'proposalId',
            'refTableClass'     => 'Proposal_Model_DbTable_Proposal',
            'refColumns'        => 'id'
        ),
        'Theme'    => array(
            'columns'       => 'companyId',
            'refTableClass' => 'Company_Model_DbTable_Company',
            'refColumns'    => 'id'
        ),
        'CustomTemplate'    => array(
            'columns'       => 'useCustomTemplate',
            'refTableClass' => 'Proposal_Model_DbTable_CustomTemplate',
            'refColumns'    => 'id'
        )
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
        unset($data['companyId'], $data['proposalId']);

        return parent::update($data, $where);
    }
}