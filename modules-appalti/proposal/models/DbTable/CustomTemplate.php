<?php

class Proposal_Model_DbTable_CustomTemplate extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'proposalCustomTemplate';

    protected $_rowClass = 'Proposal_Model_CustomTemplate';

    protected $_referenceMap = array(
        'Company' => array(
            'columns'       => 'companyId',
            'refTableClass' => 'Company_Model_DbTable_Company',
            'refColumns'    => 'id'
        ),
        'Theme' => array(
            'columns'       => 'id',
            'refTableClass' => 'Proposal_Model_DbTable_ProposalTheme',
            'refColumns'    => 'useCustomTemplate'
        ),
    );
}