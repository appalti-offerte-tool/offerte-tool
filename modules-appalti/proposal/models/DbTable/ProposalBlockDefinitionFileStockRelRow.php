<?php

class Proposal_Model_DbTable_ProposalBlockDefinitionFileStockRelRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    public function getId()
    {
        return $this->id;
    }

    public function getFileStockRow()
    {
        return $this->findParentRow('FileStock_Model_DbTable_FileStock', 'FileStock');
    }

    public function getProposalBlockRow()
    {
        return $this->findParentRow('Proposal_Model_DbTable_Proposal', 'Proposal');
    }

    public function getParentRow()
    {
        return $this->findParentRow('Proposal_Model_DbTable_ProposalBlockDefinitionFileStockRel', 'Parent');
    }

    public function getChildrenRowset()
    {
        return $this->findDependentRowset('Proposal_Model_DbTable_ProposalBlockDefinitionFileStockRel', 'Parent');
    }
}