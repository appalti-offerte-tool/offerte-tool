<?php

class Proposal_Model_DbTable_Proposal extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'proposal';

    protected $_rowClass = '\\Proposal_Model_Proposal';

    protected $_referenceMap = array(
        'Account' => array(
            'columns'           => 'accountId',
            'refTableClass'     => 'Account_Model_DbTable_Account',
            'refColumns'        => 'id'
        ),

        'ApprovedAccount' => array(
            'columns'           => 'approvedAccountId',
            'refTableClass'     => 'Account_Model_DbTable_Account',
            'refColumns'        => 'id'
        ),

        /**
         * @todo Not nice dependency relation
         */
        'Theme' => array(
            'columns'           => 'id',
            'refTableClass'     => 'Proposal_Model_DbTable_ProposalTheme',
            'refColumns'        => 'proposalId'
        ),

        'Client' => array(
            'columns'           => 'clientCompanyId',
            'refTableClass'     => 'Company_Model_DbTable_Company',
            'refColumns'        => 'id'
        ),

        'ClientContactPerson' => array(
            'columns'           => 'clientContactPersonId',
            'refTableClass'     => 'Company_Model_DbTable_ContactPerson',
            'refColumns'        => 'id'
        ),

        'ContactPerson' => array(
            'columns'           => 'contactPersonId',
            'refTableClass'     => 'Company_Model_DbTable_ContactPerson',
            'refColumns'        => 'id'
        ),

        'Company' => array(
            'columns'           => 'companyId',
            'refTableClass'     => 'Company_Model_DbTable_Company',
            'refColumns'        => 'id'
        ),
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        if (empty($data['createdDatetime'])) {
            $dt = new \DateTime();
            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        return parent::insert($data);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
        if (empty($data['modifiedDatetime'])) {
            $dt = new \DateTime();
            $data['modifiedDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        unset(
            $data['accountId'], $data['companyId'], $data['luid']
//            ,
            /**
             * Not possible to update client
             */
//            $data['clientCompanyId']
        );

        return parent::update($data, $where);
    }
}