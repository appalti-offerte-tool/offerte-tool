<?php

class Proposal_Model_DbTable_ProposalVersionHtml extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'proposalVersionHtml';

    protected $_rowClass = 'Proposal_Model_ProposalVersionHtml';

    protected $_referenceMap = array(
        'proposalVersion' => array(
            'columns'       => 'proposalVersionId',
            'refTableClass' => 'Proposal_Model_DbTable_ProposalVersion',
            'refColumns'    => 'id'
        )
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        if (empty($data['hash'])) {
            $data['hash'] = md5($data['html']);
        }

        return parent::insert($data);
    }
}