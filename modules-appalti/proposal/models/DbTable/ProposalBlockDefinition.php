<?php

class Proposal_Model_DbTable_ProposalBlockDefinition extends OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'proposalBlockDefinition';

    protected $_rowClass = '\\Proposal_Model_ProposalBlockDefinition';

    protected $_referenceMap = array(
        'Proposal' => array(
            'columns'           => 'proposalId',
            'refTableClass'     => 'Proposal_Model_DbTable_Proposal',
            'refColumns'        => 'id'
        ),
        'Block' => array(
            'columns'           => 'proposalBlockId',
            'refTableClass'     => 'ProposalBlock_Model_DbTable_ProposalBlock',
            'refColumns'        => 'id'
        )
    );

    /**
     * Casting data values on before insert or update action
     *
     * @param array $data
     * @return array
     */
    protected function _onBeforeDbAction($data)
    {
        foreach($data as $k => $value) {
            if (in_array($k, array('fitable', 'position'))) {
                $data[$k] = (int) $value;
            }
        }

        return $data;
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        $data = $this->_onBeforeDbAction($data);

        return parent::insert($data);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
        $data = $this->_onBeforeDbAction($data);

        return parent::update($data, $where);
    }
}