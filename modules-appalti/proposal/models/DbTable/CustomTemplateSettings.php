<?php

class Proposal_Model_DbTable_CustomTemplateSettings extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'proposalCustomTemplateSettings';

    protected $_rowClass = 'Proposal_Model_CustomTemplateSettings';

    protected $_referenceMap = array(
        'Settings'    => array(
            'columns'       => 'proposalCustomTemplateId',
            'refTableClass' => 'Proposal_Model_DbTable_CustomTemplate',
            'refColumns'    => 'id'
        )
    );
}