<?php

class Proposal_Model_DbTable_ProposalVersion extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'proposalVersion';

    protected $_rowClass = '\\Proposal_Model_ProposalVersion';

    protected $_referenceMap = array(
        'Proposal' => array(
            'columns'       => 'proposalId',
            'refTableClass' => 'Proposal_Model_DbTable_Proposal',
            'refColumns'    => 'id'
        )
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        if (empty($data['createdDatetime'])) {
            $dt = new \DateTime();
            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        return parent::insert($data);
    }
}