<?php

class Proposal_Model_DbTable_ProposalOffer extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'proposalCompanyOffer';

    protected $_rowClass = 'Proposal_Model_ProposalOffer';

    protected $_referenceMap = array(
        'Proposal' => array(
            'columns'           => 'proposalId',
            'refTableClass'     => 'Proposal_Model_DbTable_Proposal',
            'refColumns'        => 'id'
        ),
        'CompanyOffer'    => array(
            'columns'       => 'companyOfferId',
            'refTableClass' => 'Company_Model_DbTable_Offer',
            'refColumns'    => 'id'
        )
    );
}