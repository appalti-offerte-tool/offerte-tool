<?php

class Proposal_Model_DbTable_ProposalBlockDefinitionFileStockRel extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'proposalBlockDefinitionFileStockRel';

    protected $_nullableFields = array(
        'isThumbnail', 'parentId'
    );

    protected $_referenceMap    = array(
        'FileStock' => array(
            'columns'       => 'fileStockId',
            'refTableClass' => 'FileStock_Model_DbTable_FileStock',
            'refColumns'    => 'id'
        ),
        'Parent'    => array(
            'columns'       => 'parentId',
            'refTableClass' => __CLASS__,
            'refColumns'    => 'id'
        ),
        'Proposal'    => array(
            'columns'       => 'proposalId',
            'refTableClass' => 'Proposal_Model_DbTable_Proposal',
            'refColumns'    => 'id'
        )
    );

    protected $_rowClass = '\\Proposal_Model_DbTable_ProposalBlockDefinitionFileStockRelRow';
}