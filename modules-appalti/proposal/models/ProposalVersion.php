<?php

class Proposal_Model_ProposalVersion extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    protected $_htmlRowset;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Zend_Db_Table_Rowset_Abstract | array
     * @throws OSDN_Exception
     */
    public function getHtmlRowset()
    {
        if (null === $this->_htmlRowset) {
            $this->_htmlRowset = $this->findDependentRowset('Proposal_Model_DbTable_ProposalVersionHtml', 'proposalVersion');
            if ($this->_htmlRowset->count() > 1) {
                throw new OSDN_Exception('Data integrity corrupted');
            }

            if ($this->_htmlRowset->count() == 0) {
                throw new OSDN_Exception('No html is connected to this proposal version.');
            }
        }

        return $this->_htmlRowset;
    }

    /**
     * @param int $id
     * @return Zend_Db_Table_Row_Abstract | null
     */
    public function getHtmlByRegionId($id)
    {
        $statement = $this->_table->select()->where('regionId = ?', $id, Zend_Db::INT_TYPE);
        return $this->findDependentRowset('Proposal_Model_DbTable_ProposalVersionHtml', 'proposalVersion', $statement)->current();
    }

    /**
     * Get date part from timestamp
     *
     * @param string $format
     * @return string
     */
    public function getDate($format = 'Y-m-d')
    {
        $date = new \DateTime($this->createdDateTime);
        return $date->format($format);
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        $path = realpath(__DIR__ . '/../data/tmp') . DIRECTORY_SEPARATOR . $this->dirName;
        return file_exists($path);
    }
}