<?php

class Proposal_Model_ProposalBlockDefinition extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    /**
     * @var Proposal_Model_Section
     */
    protected $_sectionRow;

    protected static $_section;
    protected static $_cache;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getTitle()
    {
        return $this->title ?: $this->getName();
    }

    public function getTitleWithTag()
    {
        $sectionRow = $this->getSectionRow();

        if (Proposal_Model_Section::SECTION_INTRODUCTION == $sectionRow->getLuid()) {
            return '';
        }

        $notoc = '';
        if (Proposal_Model_Section::SECTION_COVER == $sectionRow->getLuid()) {
            $notoc = 'notoc="notoc"';
        }
        return sprintf('<h%d %s>%s</h%d>', $this->titleLevel, $notoc, htmlspecialchars($this->title ?: $this->getName()), $this->titleLevel);
    }

    /**
     * @return \Proposal_Model_Section
     */
    public function getSectionRow()
    {
        if (null === $this->_sectionRow) {

            if (!isset(self::$_cache[$this->sectionId])) {
                if (null === self::$_section) {
                    self::$_section = new Proposal_Service_Section();
                }

                self::$_cache[$this->sectionId] = self::$_section->find($this->sectionId);
            }

            $this->_sectionRow = self::$_cache[$this->sectionId];
        }

        return $this->_sectionRow;
    }

    public function hasPageBreak()
    {
        return !$this->inBetween() && $this->fitable;
    }

    public function isFitable()
    {
        return $this->fitable && 0 !== strcasecmp('text', $this->kind);
    }

    public function inBetween()
    {
        return (boolean) $this->inBetween;
    }

    public function getParentBlock()
    {
        $row = $this->findParentRow('ProposalBlock_Model_DbTable_ProposalBlock', 'Block');
        return $row;
    }

    protected function _base64Encode($imgPath)
    {
        $data = '';
        $fileName = APPLICATION_PATH . $imgPath;

        if (file_exists($fileName)) {
            $imgType = array('jpeg', 'jpg', 'gif', 'png');
            $fileName = htmlentities($fileName);
            $fileType = pathinfo($fileName, PATHINFO_EXTENSION);

            if (in_array($fileType, $imgType)){
                $imgBinary = fread(fopen($fileName, "r"), filesize($fileName));
                $base64 = chunk_split(base64_encode($imgBinary));
                $fileType = 'jpg' === $fileType ? 'jpeg' : $fileType;
                $data = sprintf('data:image/%s;base64,%s', $fileType, $base64);
            }
        }

        return $data;
    }

    public function getMetadata()
    {
        if ('file' == $this->kind) {

            $o = array();
            foreach(explode(';', $this->imageName) as $filepath) {
                $path = sprintf(
                    '/modules-appalti/proposal/data/definition-block-kind-file/%d/%d/%s',
                    $this->proposalId,
                    $this->proposalBlockId,
                    $filepath
                );

                $o[] = '<img src="' . $this->_base64Encode($path) . '" />';
            }

            return join('', $o);
        }

        return $this->metadata;
    }

    public function toFilePaths($isRelative = true)
    {
        $o = array();
        /**
         * @FIXME Make path more abstract
         */

        foreach(explode(';', $this->imageName) as $image) {
            $path = '/modules-appalti/proposal/data/definition-block-kind-file/';
            $path = $path . sprintf('%d/%d/%s', $this->proposalId, $this->proposalBlockId, $image);
            if (true !== $isRelative) {
                $path =realpath(APPLICATION_PATH . $path);
            }

            $o[] = $path;
        }

        return $o;
    }

    public function isFileValid()
    {
        $collection = $this->toFilePaths(false);

        $died = array();

        foreach($collection as $k => $filepath) {
            if (!file_exists($filepath)) {
                $died[] = $filepath;
                unset($collection[$k]);
            }
        }

        if (count($died) > 0) {
            $this->imageName = join(';', $collection) ?: null;
            $this->save();
        }

        return count($collection) > 0;
    }
}