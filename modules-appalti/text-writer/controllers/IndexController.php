<?php

class TextWriter_IndexController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Account_Service_Account
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {

        $this->_helper->ajaxContext()
            ->addActionContext('edit', 'json')
            ->addActionContext('create', 'json')
            ->initContext();

        $this->_service = new \Account_Service_Account();
        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */

    public function getResourceId()
    {
        return 'text-writer';
    }

    public function indexAction()
    {
        try {
            $pagination = new OSDN_Pagination();
            $pagination->setItemsCountPerPage(20);

            $data = $this->_getAllParams();
            $response = $this->_service->fetchAllWriters($data);
            $this->view->rowset = $response->getRowset();

            $pagination->setTotalCount($response->count());
            $this->view->pagination = $pagination;
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to fetch writers!');
        }
    }

    public function detailsAction()
    {
        try {
            $response = $this->_service->find($this->_getParam('writerId'), true);
            $this->view->writer = $response->toArray();
            $service = new SupportTicket_Service_SupportTicketModerator();
            $tickets = $service->fetchAssignedByStatus($this->_getAllParams(), 'new');
            $this->view->rowset = $tickets->getRowset();
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to fetch writer details!');
        }
    }

    public function fetchByStatusAction()
    {
        try {
            $params = $this->_getAllParams();
            $service = new SupportTicket_Service_SupportTicketModerator();
            $tickets = $service->fetchAssignedByStatus($params, $params['status']);
            $this->view->rowset = $tickets->getRowset();

            $this->render('details-tickets');
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to fetch tickets!');
        }
    }

    public function editAction()
    {
        $this->getResponse()->setHeader('Content-Type', 'text/html', true);
        if (!$this->getRequest()->isPost()) {
            try {
                $response = $this->_service->find($this->_getParam('writerId'), true);
                $this->view->writerRow = $response;
            } catch (\Exception $e) {
                throw new \OSDN_Exception('Unable to update writer');
            }
        } else {
            try {
                $this->_service->updateFields($this->_getAllParams());
                $writerRow = $this->_service->find($this->_getParam('writerId'));
                $this->view->writer = $writerRow->toArray();
                $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
                $this->view->success = true;
            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
            }
        }
    }

    public function updateAction()
    {
        try {
            $params = $this->_getAllParams();
            $this->_service->updateFields($params);
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to update writer');
        }
        $this->_redirect('/text-writer/index/details/writerId/' . $params['writerId']);
    }

    public function deleteAction()
    {
        try {
            $this->_service->delete($this->_getParam('writerId'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to delete writer');
        }
        $this->_redirect('/text-writer/index');
    }

    public function createAction()
    {
        $this->getResponse()->setHeader('Content-Type', 'text/html', true);

        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $data = $this->_getAllParams();
            $role = new Account_Service_AclRole();
            $role = $role->fetchByLuid('textWriter');
            $data['roleId'] = $role->id;
            $data['isActive'] = 1;
            $result = $this->_service->create($data);
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
            $this->view->writer = $result->toArray();
            $this->view->success = true;
        } catch (\Exception $e) {
            $this->view->success = false;
            throw new \OSDN_Exception('Unable to create account');
        }
    }

}