<?php

final class TextWriter_Misc_Navigation_TextWriterDetailPageMvc extends Zend_Navigation_Page_Mvc
{
    protected $_isCompleted = false;

    protected $_writerId;

    protected function _init()
    {
        $this->_writerId = (int) Zend_Controller_Front::getInstance()->getRequest()->getParam('writerId');
        if (!empty($this->_writerId)) {
            $params = $this->getParams();
            $params['writerId'] = $this->_writerId;
            $this->setParams($params);
        }

        parent::_init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Navigation_Page::getLabel()
     */
    public function getLabel()
    {
        if (true !== $this->_isCompleted) {
            $this->_label = 'Detailinformatie tekstschrijver';

            if (!empty($this->_writerId)) {
                $service = new \Account_Service_Account();
                $row = $service->find($this->_writerId);

                $this->_label = $row->getFullname();
            }

            $this->_isCompleted = true;
        }

        return parent::getLabel();
    }

}