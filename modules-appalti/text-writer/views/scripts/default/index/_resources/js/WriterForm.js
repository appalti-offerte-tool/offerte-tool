;(function($, win) {
    var Writer = function() {
        this.init();
    }

    Writer.prototype = {

        constructor: Writer,

        init: function() {
            $(function() {
                $('#writer-list')
                    .on('click', '.icon-edit', mcp.getForm)
                    .on('click', '.icon-delete', function() {
                        return alert("Weet je zeker dat?")
                    });
                $('#writer-create').click(mcp.getForm);
            })
        },

        bindFormSubmitEvent: function ($form, submitCallback, scope) {
            var $preloader = $('.ajax-preloader', $form);

            $form.ajaxForm({
                beforeSubmit: function() {
                    if (!$form.valid()) {
                        return false;
                    }
                    $preloader.addClass('show');
                },
                success: function(data) {
                    $preloader.removeClass('show');
                    data = $.parseJSON(data);
                    $.isFunction(submitCallback) && submitCallback.call(scope || mcp, data);
                }
            });
        },

        updateList: function(data, remove) {

            var wr = data.writer,
                $row = $('#writer-' + wr.id);

            if ($row.length) {
                $('.wr-department', $row).html(wr.department);
                $('.wr-name', $row).html(wr.fullname);
                $('.wr-phone', $row).html(wr.phone);
                $('.wr-email', $row).attr('href', 'mailto:' + wr.email).html(wr.email);
                $('.wr-count', $row).html('0');
                $('.wr-resolved', $row).html('0/0');
            } else {
                var row = [
                    '<tr id=writer-' + wr.id + '>',
                        '<td class="wr-department">' + wr.department + '</td>',
                         '<td class="wr-name">' + wr.fullname + '</td>',
                        '<td class="wr-phone">' + wr.phone + '</td>',
                        '<td>',
                            '<a class="wr-email" href="mailto:' + wr.email + '">' + wr.email + '</a>',
                        '</td>',
                        '<td class="wr-count"> 0 </td>',
                        '<td class="wr-resolved">(0/0)</td>',
                        '<td class="actions">',
                            '<a href="/text-writer/index/details/writerId/' + wr.id + '" class="icon-zoom">&nbsp;</a>',
                            '<a href="/text-writer/index/edit/writerId/' + wr.id + '" class="icon-edit">&nbsp;</a>',
                            '<a href="/text-writer/index/delete/writerId/' + wr.id + '" class="icon-delete">&nbsp;</a>',
                        '</td>',
                    '</tr>'
                ].join('');

                $('#writer-list tbody').append(row);
            }
        },

        getForm: function () {
            $.get(this.href).done(function(data) {
                if (!mcp.modalCnt) {
                    mcp.modalCnt = $('<div id="modal-container" class="modal"></div>').appendTo('body');
                }
                mcp.modalCnt.html(data);
                var $form = $('form', mcp.modalCnt);
                mcp.bindFormSubmitEvent($form, function(data) {
                    if (data.success) {
                        alert(data.messages[0]['message']);
                        mcp.updateList(data);
                        mcp.modalCnt.modal('toggle');
                    } else {
                        alert(data.messages[0]);
                    }
                }, mcp);
                mcp.modalCnt.modal('toggle');
            });

            return false;
        }
    }

    win.ModuleWriter = new Writer();
    var mcp = win.ModuleWriter;

})(jQuery, window);
