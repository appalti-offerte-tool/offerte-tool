<?php

class Membership_Model_DbTable_Membership extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'membership';

    protected $_rowClass = '\\Membership_Model_Membership';
}