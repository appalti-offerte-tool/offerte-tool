<?php

class Membership_Model_DbTable_MembershipCompany extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'membershipCompany';

    protected $_rowClass = '\\Membership_Model_MembershipCompany';
}