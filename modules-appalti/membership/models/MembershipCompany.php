<?php

class Membership_Model_MembershipCompany extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Check if membership period is valid
     *
     * @return boolean
     */
    public function isValid()
    {
        $beginDt = new \DateTime($this->startDatetime);
        $endDt = new \DateTime($this->endDatetime);

        $dt = new \DateTime();
        return $beginDt->getTimestamp() <= $dt->getTimestamp() && $dt->getTimestamp() <= $endDt->getTimestamp();
    }
}