<?php

class Membership_Model_Membership extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }
}