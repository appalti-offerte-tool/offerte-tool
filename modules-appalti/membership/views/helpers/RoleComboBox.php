<?php

class Membership_View_Helper_RoleComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    /**
     * @var \Membership_Service_AclRole
     */
    protected $_service;

    /**
     * The constructor
     *
     * Initialize options for combo field
     */
    public function __construct()
    {
        $this->_service = new \Membership_Service_AclRole();

        $response = $this->_service->fetchAllWithResponse();
        $options = array();

        $response->getRowset(function($row) use (& $options) {
            $options[$row['id']] = $row['name'];
            return false;
        });

        $this->_options = $options;
    }

    public function roleComboBox()
    {
        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }

    public function toLabel($value)
    {
        return $this->_options[$value];
    }

}