<?php

class Membership_View_Helper_MembershipComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    protected $_prices = array();

    public function __construct()
    {
        $service = new  Membership_Service_Membership();
        $response = $service->fetchAllWithResponse(array(), false);

        $options = array('','');
        $prices = array();

        $response->getRowset(function($row) use (& $options, & $prices) {
            $row['name'] = $row['name'].' month';
            $options[$row['id']] = $row['name'];
            $prices[$row['id']] = $row['price'];
            return false;
        });
        $this->_options = $options;
        $this->_prices = $prices;
    }

    public function membershipComboBox()
    {
        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }

    public function getPrices()
    {
        return $this->_prices;
    }

    public function getOptions()
    {
        return $this->_options;
    }

}