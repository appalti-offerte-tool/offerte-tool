Ext.define('Module.Membership.CompanyMembership.List', {
    extend: 'Ext.ux.grid.GridPanel',
    alias: 'widget.module.membership.company-membership.list',

    filterRequestParam: null,

    features: [{
        ftype: 'filters'
    }],

    iconCls: 'm-membership-icon-16',

    modeReadOnly: false,

    companyId: null,

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.Membership.Model.MembershipCompany',
            proxy: {
                type: 'ajax',
                url: link('membership', 'company', 'fetch-all-by-company', {format: 'json'}),
                extraParams: {
                    companyId: this.companyId
                },
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.columns = [{
            header: lang('Membership'),
            dataIndex: 'membershipName',
            flex: 1
        }, {
            xtype: 'datecolumn',
            header: lang('Start datetime'),
            dataIndex: 'startDatetime',
            format: 'd-m-Y H:i:s',
            width: 150
        }, {
            xtype: 'datecolumn',
            header: lang('End datetime'),
            dataIndex: 'endDatetime',
            format: 'd-m-Y H:i:s',
            width: 150
        }];

        this.columns.push({
            xtype: 'actioncolumn',
            header: lang('Actions'),
            width: 50,
            fixed: true,
            items: [{
                tooltip: lang('Edit'),
                iconCls: 'icon-edit-16 icon-16',
                handler: function(g, rowIndex) {
                    this.onEditCompanyMembership(g, g.getStore().getAt(rowIndex));
                },
                scope: this
            }, {
                text: lang('Delete'),
                iconCls: 'icon-delete-16 icon-16',
                handler: this.onDeleteCompanyMembership,
                scope: this
            }]
        });

        this.tbar = [{
            text: lang('Create'),
            iconCls: 'icon-create-16',
            handler: this.onCreateCompanyMembership,
            scope: this
        }, '-'];

        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 2
        })];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent();

        this.getView().on('itemdblclick', function(w, record) {
            this.onEditCompanyMembership(this, record);
        }, this);
    },

    onCreateCompanyMembership: function() {
        Application.require([
            'membership/company-membership/form'
        ], function() {
            var f = new Module.Membership.CompanyMembership.Form({
                companyId: this.companyId
            });

            f.on('complete', function(form, membershipCompanyId) {
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditCompanyMembership: function(g, record) {
        Application.require([
            'membership/company-membership/form'
        ], function() {
            var f = new Module.Membership.CompanyMembership.Form({
                membershipCompanyId: record.get('membershipCompanyId'),
                companyId: this.companyId
            });
            f.doLoad();
            f.on('complete', function(form, membershipCompanyId) {
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onDeleteCompanyMembership: function(g, rowIndex) {

        var record = g.getStore().getAt(rowIndex);

        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
            if (b != 'yes') {
                return;
            }

            Ext.Ajax.request({
                url: link('membership', 'main', 'delete', {format: 'json'}),
                method: 'POST',
                params: {
                    membershipCompanyId: record.get('id')
                },
                success: function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);

                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope: this
            });
        }, this);
    },

    setCompanyId: function(companyId, forceReload) {

        this.companyId = companyId;

        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.companyId = companyId;
        if (true === forceReload) {
            this.getStore().load();
        }

        return this;
    }
});