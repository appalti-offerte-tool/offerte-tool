Ext.define('Module.Application.Model.Error', {
    extend: 'Ext.data.Model',
    fields: [
        'message', 'type', 'field',
        {name: 'id', mapping: 'field'},
        {name: 'msg', mapping: 'message'}
    ]
});


Ext.define('Module.Membership.CompanyMembership.Form', {
    extend: 'Ext.form.Panel',

    bodyPadding: 5,

    wnd: null,
    membershipCompanyId: null,
    companyId: null,

    model: 'Module.Membership.Model.MembershipCompany',

    fileStockImage: null,

    initComponent: function() {

        this.initialConfig.trackResetOnLoad = true;
        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.Membership.Model.MembershipCompany',
            type: 'json',
            root: 'row'
        });

        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
            model: 'Module.Application.Model.Error',
            type: 'json',
            root: 'messages',
            successProperty: 'success'
        });

        this.items = [{
            xtype: 'module.membership.combo-box',
            fieldLabel: lang('Month count'),
            name: 'membershipId',
            hiddenName: 'membershipId',
            anchor: '100%',
            allowBlank: false
        },{
            xtype: 'datefield',
            format:'d-m-Y',
            fieldLabel:lang('Start Date'),
            name:'startDatetime',
            anchor: '100%',
            value : new Date()
        }
        ];

        this.callParent();

        this.addEvents('complete');
    },

    onSubmit: function() {

        if (!this.form.isValid()) {
            return false;
        }

        var params = {};
        params['companyId']= this.companyId

        if (this.membershipCompanyId) {
            params['membershipCompanyId'] = this.membershipCompanyId;
        }
        this.form.submit({
            url:link('membership', 'company', 'update-by-date', {format:'json'}),
            method:'post',
            params:params,
            waitMsg:Ext.LoadMask.prototype.msg,
            failure: function(options, action) {
                Application.notificate(action);
                this.wnd && this.wnd.close();
            },
            success: function(form, action) {
                var decResponse = Ext.decode(action.response.responseText);
                Application.notificate(decResponse.messages);
                if (action.result.success) {
                    this.fireEvent('completed', this, decResponse.id);
                    this.wnd && this.wnd.close();
                }
                this.wnd && this.wnd.close();
            },
            scope: this
        });
    },

    showInWindow: function() {
        this.border = false;
        var w = this.wnd = new Ext.Window({
            title: 'Assign membership',
            resizable: false,
            layout: 'fit',
            iconCls: 'm-membership-icon-16',
            width: 350,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Save'),
                handler: this.onSubmit,
                scope: this
            }, {
                text: lang('Close'),
                handler: function() {
                    w.close();
                    this.wnd = null;
                },
                scope: this
            }]
        });

        w.show();
        return w;
    }
});