Ext.define('Module.Application.Model.Error', {
    extend: 'Ext.data.Model',
    fields: [
        'message', 'type', 'field',
        {name: 'id', mapping: 'field'},
        {name: 'msg', mapping: 'message'}
    ]
});


Ext.define('Module.Membership.Form', {
    extend: 'Ext.form.Panel',

    bodyPadding: 5,

    wnd: null,
    membershipId: null,

    model: 'Module.Membership.Model.Membership',

    fileStockImage: null,

    initComponent: function() {

        this.initialConfig.trackResetOnLoad = true;
        this.initialConfig.reader = new Ext.data.reader.Json({
            model: 'Module.Membership.Model.Membership',
            type: 'json',
            root: 'row'
        });

        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json({
            model: 'Module.Application.Model.Error',
            type: 'json',
            root: 'messages',
            successProperty: 'success'
        });

        this.items = [{
            xtype: 'textfield',
            fieldLabel: lang('Month count'),
            name: 'name',
            anchor: '100%'
        }, {
            xtype: 'textfield',
            name: 'price',
            fieldLabel: lang('Price'),
            anchor: '100%'
        }];

        this.callParent();

        this.addEvents('complete');
    },

    doLoad: function() {

        if (!this.membershipId) {
            return;
        }

        this.form.load({
            url: link('membership', 'main', 'fetch', {
                membershipId: this.membershipId,
                format: 'json'
            }),
            scope: this
        });
    },

    onSubmit: function() {

        if (!this.form.isValid()) {
            return false;
        }

        var action = this.membershipId ? 'update' : 'create';
        var o = {};
        if (this.membershipId) {
            o.membershipId = this.membershipId;
        }

        this.form.submit({
            url: link('membership', 'main', action, {format: 'json'}),
            params: o,
            success: function(options, action) {

                var decResponse = Ext.decode(action.response.responseText);
                Application.notificate(decResponse.messages);

                if (decResponse.success) {
                    this.fireEvent('complete', this, decResponse.membershipId);
                    this.wnd && this.wnd.close();
                }
            },
            scope: this
        });
    },

    showInWindow: function() {
        this.border = false;
        var w = this.wnd = new Ext.Window({
            title: this.membershipId ? lang('Update membership') : lang('Create membership'),
            resizable: false,
            layout: 'fit',
            iconCls: 'm-membership-icon-16',
            width: 350,
            border: false,
            modal: true,
            items: [this],
            buttons: [{
                text: lang('Save'),
                handler: this.onSubmit,
                scope: this
            }, {
                text: lang('Close'),
                handler: function() {
                    w.close();
                    this.wnd = null;
                },
                scope: this
            }]
        });

        w.show();
        return w;
    }
});