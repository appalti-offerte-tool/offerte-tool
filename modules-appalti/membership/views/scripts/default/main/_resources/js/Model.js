Ext.define('Module.Membership.Model.Membership', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'roleId', 'name', 'price'
    ]
});