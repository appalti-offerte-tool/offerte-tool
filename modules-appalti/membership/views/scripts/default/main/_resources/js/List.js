Ext.define('Module.Membership.List', {
    extend: 'Ext.ux.grid.GridPanel',



    features: [{
        ftype: 'filters'
    }],

    iconCls: 'm-membership-icon-16',

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.Membership.Model.Membership',
            proxy: {
                type: 'ajax',
                url: link('membership', 'main', 'fetch-all', {format: 'json'}),
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.columns = [{
            header: lang('Name'),
            dataIndex: 'name',
            flex: 1,
            renderer: function(value){
                if (value ==null) {
                    return '';
                } else {
                    return value+' month';
                }
            }
        }, {
            header: lang('Price'),
            dataIndex: 'price',
            width: 150,
            renderer: function(value){
            if (value ==null) {
                return '€0';
            } else {
                return '€'+value;
            }
        }
        }];

        this.columns.push({
            xtype: 'actioncolumn',
            header: lang('Actions'),
            width: 50,
            fixed: true,
            items: [{
                tooltip: lang('Edit'),
                iconCls: 'icon-edit-16 icon-16',
                handler: function(g, rowIndex) {
                    this.onEditMembership(g, g.getStore().getAt(rowIndex));
                },
                scope: this
            }, {
                text: lang('Delete'),
                iconCls: 'icon-delete-16 icon-16',
                handler: this.onDeleteMembership,
                scope: this
            }]
        });

        this.tbar = [{
            text: lang('Create'),
            iconCls: 'icon-create-16',
            qtip: lang('Create new membership'),
            handler: this.onCreateMembership,
            scope: this
        }, '-'];

        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 2
        })];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent();

        this.getView().on('itemdblclick', function(w, record) {
            this.onEditMembership(this, record);
        }, this);
    },

    onCreateMembership: function() {
        Application.require([
            'membership/./form'
        ], function() {
            var f = new Module.Membership.Form({
                membershipId: this.membershipId
            });

            f.on('complete', function(form, membershipId) {
                this.setLastInsertedId(membershipId);
                this.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onEditMembership: function(g, record) {
        Application.require([
            'membership/./form'
        ], function() {
            var f = new Module.Membership.Form({
                membershipId: record.get('id'),
            });
            f.doLoad();
            f.on('complete', function(form, membershipId) {
                this.setLastInsertedId(membershipId);
                g.getStore().load();
            }, this);
            f.showInWindow();
        }, this);
    },

    onDeleteMembership: function(g, rowIndex) {

        var record = g.getStore().getAt(rowIndex);

        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
            if (b != 'yes') {
                return;
            }

            Ext.Ajax.request({
                url: link('membership', 'main', 'delete', {format: 'json'}),
                method: 'POST',
                params: {
                    membershipId: record.get('id')
                },
                success: function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);

                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope: this
            });
        }, this);
    }
});