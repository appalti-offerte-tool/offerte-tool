Ext.define('Module.Membership.MembershipCompany.List', {
    extend: 'Ext.ux.grid.GridPanel',
    alias: 'widget.module.membership.membership-company.list',

    filterRequestParam: null,

    features: [{
        ftype: 'filters'
    }],

    iconCls: 'm-membership-icon-16',

    modeReadOnly: false,

    membershipId: null,

    initComponent: function() {

        this.store = new Ext.data.Store({
            model: 'Module.Membership.Model.MembershipCompany',
            proxy: {
                type: 'ajax',
                url: link('membership', 'main', 'fetch-all-company-by-membership-id', {format: 'json'}),
                extraParams: {
                    membershipId: this.membershipId
                },
                reader: {
                    type: 'json',
                    root: 'rowset'
                }
            }
        });

        this.columns = [{
            header: lang('Name'),
            dataIndex: 'fullname',
            flex: 1
        }, {
            xtype: 'datecolumn',
            header: lang('Start datetime'),
            dataIndex: 'startDatetime',
            format: 'd-m-Y',
            width: 150
        }, {
            xtype: 'datecolumn',
            header: lang('End datetime'),
            dataIndex: 'endDatetime',
            format: 'd-m-Y',
            width: 150
        }];

        this.columns.push({
            xtype: 'actioncolumn',
            header: lang('Actions'),
            width: 50,
            fixed: true,
            items: [
                {
                text: lang('Delete'),
                iconCls: 'icon-delete-16 icon-16',
                handler: this.onDeleteMembershipCompany,
                scope: this
            }]
        });

        this.tbar = ['-'];

        this.plugins = [new Ext.ux.grid.Search({
            minChars: 2,
            stringFree: true,
            align: 2
        })];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            plugins: 'pagesize'
        });

        this.callParent();

    },


    onDeleteMembershipCompany: function(g, rowIndex) {

        var record = g.getStore().getAt(rowIndex);
        Ext.Msg.confirm(lang('Confirmation'), lang('Are you sure?'), function(b) {
            if (b != 'yes') {
                return;
            }

            Ext.Ajax.request({
                url: link('membership', 'company', 'delete', {format: 'json'}),
                method: 'POST',
                params: {
                    membershipCompanyId: record.get('id'),
                    companyId: record.get('companyId')
                },
                success: function(response, options) {
                    var decResponse = Ext.decode(response.responseText);
                    Application.notificate(decResponse.messages);

                    if (true == decResponse.success) {
                        g.getStore().load();
                    }
                },
                scope: this
            });
        }, this);
    },

    setMembershipId: function(membershipId, forceReload) {

        this.membershipId = membershipId;

        var p = this.getStore().getProxy();
        p.extraParams = p.extraParams || {};
        p.extraParams.membershipId = membershipId;
        if (true === forceReload) {
            this.getStore().load();
        }

        return this;
    }
});