Ext.define('Module.Membership.Model.MembershipCompany', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'companyId', 'fullname',
        'membershipId', 'membershipName', 'price',
        'membershipCompanyId',
        {name: 'startDatetime', type: 'date', dateFormat: 'Y-m-d H:i:s'},
        {name: 'endDatetime', type: 'date', dateFormat: 'Y-m-d H:i:s'}
    ]
});