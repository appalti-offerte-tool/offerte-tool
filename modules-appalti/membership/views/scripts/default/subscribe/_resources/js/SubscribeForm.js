SubscribeForm = function (conf) {
    $.extend(this, conf || {});
    this.init();
}

SubscribeForm.prototype = function() {

    var protect = {
        getModalWindow: function() {
            if ($('#modal-container').length > 0) {
                return $('#modal-container');
            }
            return $('<div id="modal-container"></div>').appendTo('body').addClass('modal');
        },
        maxFileId: 0
    };

    var fileTpl = [
        '<p>',
            '<span class="file-container">',
                '<em class="file-name">',
                    '<span class="text-gray">Selecteer een bestand</span>',
                    '<input type="file" name="files-[s]">',
                    '<a href="#" class="file-remove icon-delete-cross">&nbsp;</a>',
                '</em>',
            '</span>',
        '</p>'
    ].join('');

return {
    loadUrl: '/membership/subscribe/create',

    formUrl: '/membership/subscribe/form',

    addUrl: '/membership/subscribe/create',


    containerId: null,


    init: function() {
        $('body')
            .off('SubscribeForm')
            .on('change.SubscribeForm', '#serviceId, #priority', $.proxy(this.getPrice, this));
    },

        initAddingFilesOnFly: function (form) {
        var me = this;
        form.find('a.add_file').click($.proxy(function() {
            protect.maxFileId++;
            form.find('#extra-files-container').append(fileTpl.replace('[s]', protect.maxFileId));
        }, this));

    },

    showForm: function () {
        $.get(this.formUrl, {
        }, $.proxy(function(data) {
            var mw = protect.getModalWindow().html(data).modal('show');
            var form = mw.find('form');

            form.submit($.proxy(function() {
                if (form.valid()) {
                    this.doAdd(form);
                }
                return false;
            }, this));

            this.initAddingFilesOnFly(form);
        }, this));
    },

    doAdd: function(form) {
        form.ajaxSubmit({
            url: this.addUrl,
            dataType: 'json',
            data: {
                format: 'json'
            },
            type: 'post',
            success: $.proxy(function(response) {
                if (response && response.success) {
                    protect.getModalWindow().modal('hide');
                    alert('Successfully saved')
                    this.onSuccessAdd();
                } else {
                    alert(response.messages);
                }
            }, this)
        });
    },

    onSuccessAdd: function () {}

}
}();
