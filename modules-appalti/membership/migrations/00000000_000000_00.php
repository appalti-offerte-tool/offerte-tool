<?php

class Membership_Migration_00000000_000000_00 extends Core_Migration_Abstract
{
    public function up()
    {
        $this->createTable('membership');
        $this->createColumn('membership', 'roleId', self::TYPE_INT, 11, null, true);
        $this->createIndex('membership', array('roleId'), 'roleId');
        $this->createForeignKey('membership', array('roleId'), 'aclRole', array('id'), 'FK_roleId');
        $this->createColumn('membership', 'name', self::TYPE_VARCHAR, 255, null, true);
        $this->createColumn('membership', 'price', self::TYPE_VARCHAR, 255, null, true);

        $this->createTable('membershipAccount');
        $this->createColumn('membershipAccount', 'accountId', self::TYPE_INT, 11, null, true);
        $this->createIndex('membershipAccount', array('accountId'), 'IX_accountId');
        $this->createForeignKey('membershipAccount', array('accountId'), 'account', array('id'), 'FK_accountId');
        $this->createColumn('membershipAccount', 'membershipId', self::TYPE_INT, 11, null, true);
        $this->createIndex('membershipAccount', array('membershipId'), 'IX_membershipId');
        $this->createForeignKey('membershipAccount', array('membershipId'), 'membership', array('id'), 'FK_membershipId');
        $this->createColumn('membershipAccount', 'startDatetime', self::TYPE_DATETIME, null, null, true);
        $this->createColumn('membershipAccount', 'endDatetime', self::TYPE_DATETIME, null, null, true);
    }

    public function down()
    {
        $this->dropTable('membership');
        $this->dropTable('membershipAccount');
    }
}