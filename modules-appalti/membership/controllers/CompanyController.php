<?php

class Membership_CompanyController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Membership_Service_MembershipCompany
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_helper->contextSwitch()
            ->addActionContext('index', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('update-by-date', 'json')
            ->addActionContext('update-by-id', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch-all-by-company', 'json')
            ->addActionContext('fetch', 'json')
            ->initContext();

        parent::init();

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        if ($accountRow->isAdmin()) {
            $companyService = new Company_Service_Company();
            $companyRow = $companyService->find($this->_getParam('companyId'));
        } else {
            $companyRow = $accountRow->getCompanyRow();
        }

        $this->_service = new \Membership_Service_MembershipCompany($companyRow);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'membership';
    }

    public function indexAction()
    {
        $response = $this->_service->fetchAllByCompanyId();

        $this->view->assign($response);
        $this->view->success = true;
    }

    public function fetchAllAction()
    {
        $response = $this->_service->fetchAllWithResponse($this->_getParam('membershipId'), $this->_getAllParams());
        $this->view->assign($response->toArray());
    }

    public function fetchAllByCompanyAction()
    {
        $response = $this->_service->fetchAllByCompanyId($this->_getParam('companyId'), $this->_getAllParams());
        $this->view->row =$response;

    }

    public function fetchAction()
    {
        $ratingRow = $this->_service->find($this->_getParam('membershipCompanyId'));
        $this->view->row = $ratingRow->toArray();
        $this->view->success = true;
    }

    public function createAction()
    {
        $serviceMembership = new Membership_Service_Membership();

        try {
            $membership = $serviceMembership->find($this->_getParam("membershipId"));
            $ratingRow = $this->_service->createSubscribe($this->_getAllParams(),$membership);
            $this->view->ratingOptionId = $ratingRow->getId();
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        try {
            $ratingRow = $this->_service->updateSubscribe($this->_getAllParams());
            $this->view->membershipCompanyId = $ratingRow->getId();
            $this->view->success = true;
            $this->_helper->information('Membership updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }

        if ($this->_getParam('return')) {
            $this->_redirect(base64_decode($this->_getParam('return')));
        }
    }

    public function updateByDateAction()
    {
         try {
             $row = $this->_service->fetch();
             if ($row == null) {
                 $row = $this->_service->createSubscribe($this->_getAllParams(), null);
             } else {
                 $row = $this->_service->updateSubscribe($this->_getAllParams());
             }
             $row = $row->toArray();
             $this->view->assign($row);
             $this->view->success = true;

             $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
         } catch(\OSDN_Exception $e) {
             $this->view->success = false;
             $this->_helper->information($e->getMessages());
         }
    }

    public function updateByIdAction()
    {
        $membershipId = $this->_getParam('membershipId');
        try{
           $row = $this->_service->fetch();
           if ($row == null) {
               $row = $this->_service->createSubscribe($this->_getAllParams(), null);

           } else {
               $row = $this->_service->updateSubscribe($this->_getAllParams());
           }

            $this->view->assign($row->toArray());
            $this->view->success =true;
        } catch(\OSDN_Exception $e) {
            $this->view->membershipId = $membershipId;
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }


    public function deleteAction()
    {
        try {
            $this->view->success = $this->_service->delete($this->_getParam('membershipCompanyId'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}