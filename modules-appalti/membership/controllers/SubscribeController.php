<?php

class Membership_SubscribeController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Membership_Service_MembershipCompany
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_accountRow = Zend_Auth::getInstance()->getIdentity();

        $this->_helper->contextSwitch()
            ->addActionContext('create', 'json')
            ->initContext();

        parent::init();

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $companyRow =  Zend_Auth::getInstance()->getIdentity()->getCompanyRow();

        if ($accountRow->isAdmin()) {
            $companyService = new Company_Service_Company();
            $companyRow = $companyService->find($companyRow->id);
        } else {
            $companyRow = $accountRow->getCompanyRow();
        }

        $this->_service = new \Membership_Service_MembershipCompany($companyRow);
    }

    public function getResourceId()
    {
        return 'membership:company';
    }


    public function formAction()
    {
        $this->view->companyRow = Zend_Auth::getInstance()->getIdentity()->getCompanyRow();
        $this->view->accountRow = $this->_accountRow;

    }

    public function createAction()
    {
        try {
            $row = $this->_service->fetch();
            $serviceMembership = new Membership_Service_Membership();
            $membership = $serviceMembership->find($this->_getParam("membershipId"));
            if ($row != null) {

                $this->_service->updateSubscribe($this->_getAllParams(), $membership->toArray());
            } else {
                $this->_service->createSubscribe($this->_getAllParams(), $membership);
            }
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }

    }
}