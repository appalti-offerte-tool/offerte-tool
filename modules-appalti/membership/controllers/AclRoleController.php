<?php

class Membership_AclRoleController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Membership_Service_AclRole
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_service = new \Membership_Service_AclRole();

        $this->_helper->contextSwitch()
            ->addActionContext('index', 'json')
            ->addActionContext('fetch-all', 'json')
            ->initContext();

        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'membership';
    }

    public function indexAction()
    {
        // prepare layout
    }

    public function fetchAllAction()
    {
        $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
        $this->view->assign($response->toArray());
    }

    public function comboBoxAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');

        $response = $this->_service->fetchAllWithResponse();
        $this->view->rowset = $response->getRowset();
    }
}