<?php

class Membership_MainController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var Membership_Service_Membership
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $this->_service = new \Membership_Service_Membership();

        $this->_helper->contextSwitch()
            ->addActionContext('index', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch-all-company-by-membership-id', 'json')
            ->addActionContext('fetch', 'json')
            ->initContext();

        parent::init();
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'membership:list';
    }

    public function indexAction()
    {
        // prepare layout
    }

    public function fetchAllAction()
    {
        $response = $this->_service->fetchAllWithResponse($this->_getAllParams(), false);
        $this->view->assign($response->toArray());
    }

    public function fetchAllCompanyByMembershipIdAction()
    {
        $response = $this->_service->fetchAllCompaniesByMembershipId($this->_getParam('membershipId'),$this->_getAllParams());
        $this->view->assign($response->toArray());
    }
    public function fetchAction()
    {
        $membershipRow = $this->_service->find($this->_getParam('membershipId'));
        $this->view->row = $membershipRow->toArray();
        $this->view->success = true;
    }

    public function comboBoxAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getResponse()->setHeader('Content-type', 'text/javascript');

        $response = $this->_service->fetchAllWithResponse(array(), false);
        $this->view->rowset = $response->getRowset();
    }

    public function createAction()
    {
        try {
            $membershipRow = $this->_service->create($this->_getAllParams());
            $this->view->membershipId = $membershipRow->getId();
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function updateAction()
    {
        try {
            $membershipRow = $this->_service->update($this->_getParam('membershipId'), $this->_getAllParams());
            $this->view->membershipId = $membershipRow->getId();
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->view->success = $this->_service->delete($this->_getParam('membershipId'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch(\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
    }
}