<?php

class Membership_IndexController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Membership_Service_Membership
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();

        $this->_helper->ContextSwitch()
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->initContext();


        $this->_service = new \Membership_Service_Membership();
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'membership:manage-list';
    }

    public function indexAction()
    {

        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(3);

        $accountRow = Zend_Auth::getInstance()->getIdentity();

        $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
        $this->view->assign($response->toArray());

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }

    public function createAction()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->render('form');
        }

        try {
            $this->view->formSubmittedPostData = $this->getRequest()->getParams();
            $membershipRow = $this->_service->create($this->_getAllParams());

            $this->view->membershipId = $membershipRow->getId();
            $this->view->success = true;
            $this->_helper->information(array('%s is gemaakt.', array($membershipRow->name)), true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch(\Exception $e) {
            $this->view->success = false;
            $this->_helper->information('Kan te maken support minutes.');
        }
    }

    public function updateAction()
    {
        $membershipRow = $this->_service->find($this->_getParam('membershipId'), false);
        $this->view->membershipRow = $membershipRow;

        if (null === $membershipRow) {
            $this->view->success = false;
            $this->_helper->information(array('%s is bijgewerkt.', array($membershipRow->name)), true, E_USER_NOTICE);
            return;
        }

        if (!$this->getRequest()->isPost()) {
            return $this->render('form');
        }

        try {
            $this->view->formSubmittedPostData = $this->getRequest()->getParams();
            $membershipRow = $this->_service->update($membershipRow->getId(), $this->_getAllParams());
            $this->view->membershipId = $membershipRow->getId();
            $this->view->success = true;
            $this->_helper->information(array('%s is bijgewerkt.', array($membershipRow->name)), array(), E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        } catch(\Exception $e) {
            $this->view->success = false;
            $this->_helper->information('Niet werken support minutes.');
        }
    }

   public function deleteAction()
    {
        $membershipId = $this->_getParam('membershipId');

        try {
            $membershipRow = $this->_service->find($membershipId);
            $name = $membershipRow->name;
            $this->_service->delete($membershipRow->getId());
            $this->_helper->information(array('%s is verwijderd.', array($name)), true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

}