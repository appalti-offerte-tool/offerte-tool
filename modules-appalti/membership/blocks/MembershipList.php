<?php

class Membership_Block_MembershipList extends \Application_Block_Abstract
{

    protected $_itemsPerPage = 10;

    protected function _toTitle()
    {
        return $this->view->translate('Membership list');

    }

    protected function _toHtml()
    {
        $service = new \Membership_Service_Membership();

        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage($this->_itemsPerPage);

        $params = $this->getRequest()->getParams();
        $params['filter'] = $this->getFilterParams();

        $response = $service->fetchAllWithResponse($params);

        $this->view->assign($response->toArray());

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
        $this->view->blockId = $this->getId();
        $this->view->listOnly = $this->_getParam('listOnly');

        
    }


}