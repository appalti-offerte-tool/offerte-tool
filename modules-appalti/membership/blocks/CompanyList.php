<?php

class Membership_Block_CompanyList extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Companies');
    }

    protected function _toHtml()
    {}
}