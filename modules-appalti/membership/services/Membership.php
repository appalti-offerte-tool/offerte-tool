<?php

class Membership_Service_Membership extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Membership_Model_DbTable_Membership
     */
    protected $_table;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new \Membership_Model_DbTable_Membership();

        parent::_init();
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \Membership_Model_Membership
     */
    public function find($id, $throwException = true)
    {
        $membershipRow = $this->_table->findOne($id);
        if (null === $membershipRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find membership #' . $id);
        }

        return $membershipRow;
    }

    /**
     * Create the new membership row
     *
     * @param array $data
     * @return Membership_Model_Membership
     */
    public function create(array $data)
    {
        $this->_attachValidationRules('default', array(
            'amount'   => array('id', 'presence' => 'required', 'allowEmpty' => false),
            'name'     => array(array('StringLength', 1, 255), 'presence' => 'required', 'allowEmpty' => false),
            'price'    => array('presence' => 'required', 'allowEmpty' => false)
        ));
        $f = $this->_validate($data);
        $membershipRow = $this->_table->createRow($f->getData());
        $membershipRow->save();

        return $membershipRow;
    }

    /**
     * @param array $params
     * @return OSDN_Db_Response
     */
    public function fetchAllWithResponse(array $params = array(), $returnModeRows = true)
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('m' => $this->_table->getTableName())
            );

        $this->_initDbFilter($select, $this->_table)->parse($params);
        $response = $this->getDecorator('response')->decorate($select);

        if (true === $returnModeRows) {
            $table = $this->_table;
            $response->setRowsetCallback(function($row) use ($table) {
                return $table->createRow($row);
            });
        }

        return $response;
    }

    public function fetchAllCompaniesByMembershipId ($membershipId, array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(
            array('ma' => 'membershipCompany'),
            array(
                '*',
                'membershipCompanyId' => 'id'
            )
        )
            ->join(
            array('m' => 'membership'),
            'm.id = ma.membershipId',
            array(
                'membershipName' => 'name',
                'price'
            )
        )
            ->join(
            array('c' => 'company'),
            'ma.companyId = c.id',
            array('fullname')
        )
            ->where('ma.membershipId = ?', $membershipId, Zend_Db::INT_TYPE);

        $this->_initDbFilter($select, $this->_table)->parse($params);
        return $this->getDecorator('response')->decorate($select);
    }


    public function update($id, array $data)
    {
        $this->_attachValidationRules('default', array(
            'amount'   => array('id', 'presence' => 'required', 'allowEmpty' => false),
            'name'     => array(array('StringLength', 1, 255), 'presence' => 'required', 'allowEmpty' => false),
            'price'    => array('presence' => 'required', 'allowEmpty' => false)
        ));
        $f = $this->_validate($data);
        $membershipRow = $this->find($id);
        $membershipRow->setFromArray($f->getData());
        $membershipRow->save();

        return $membershipRow;
    }

    /**
     * @todo should be done via event manager
     *
     * @FIXME Add main global transation for that
     */
    public function delete($id)
    {
         try {
            $membershipRow = $this->find($id);
            $membershipRow->delete();
            $servicePrise = new SupportTicket_Service_SupportTicketServicePrice();
            $servicePrise->deleteByMembershipId($id);
         } catch (\Exception $e) {
             throw new \OSDN_Exception('Unable to delete membership, because some relation don`t accept it!');
         }
    }
}