<?php

class Membership_Service_MembershipCompany extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Membership_Model_DbTable_MembershipCompany
     */
    protected $_table;

    protected $_companyRow;

    public function __construct(Company_Model_CompanyRow $companyRow)
    {
        $this->_companyRow = $companyRow;

        parent::__construct();

        $this->_table = new \Membership_Model_DbTable_MembershipCompany();
    }


    private function formatDate($date)
    {
        if (\is_string($date)) {
            if (($timestamp = strtotime($date)) === -1) {
                return false;
            } else {
                return date('Y-m-d', $timestamp);
            }
        } else {
            return $date;
        }
        return false;
    }


    /**
     * @param int     $id
     * @param boolean $throwException
     * @throws OSDN_Exception
     * @return \Membership_Model_MembershipCompany
     */
    public function find($id, $throwException = true)
    {
        $membershipCompanyRow = $this->_table->findOne($id);
        if (null === $membershipCompanyRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find rating #' . $id);
        }

        return $membershipCompanyRow;
    }
    /**
     * @param       $membershipId
     * @param array $params
     *
     * @return OSDN_Db_Response
     */
    public function fetchAllWithResponse($membershipId, array $params = array())
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('ma' => $this->_table->getTableName()),
                array(
                    '*',
                    'membershipCompanyId' => 'id'
                )
            )
            ->join(
                array('m' => 'membership'),
                'm.id = ma.membershipId',
                array(
                    'membershipName' => 'name',
                    'price'
                )
            )
            ->join(
                array('c' => 'company'),
                'ma.companyId = c.id',
                array('fullname')
            )
            ->where('ma.membershipId = ?', $membershipId, Zend_Db::INT_TYPE);

        $this->_initDbFilter($select, $this->_table)->parse($params);
        return $this->getDecorator('response')->decorate($select);
    }

    /**
     * @return Membership_Model_MembershipCompany
     */
    public function fetch()
    {

        return $this->_table->fetchRow(array('companyId = ?' => $this->_companyRow->getId()));
    }

    /**
     * @return array|mixed
     */

    public function fetchAllByCompanyId()
    {
        $select = $this->_table->getAdapter()->select()
            ->from(
            array('ma' => $this->_table->getTableName()),
            array(
                '*',
                'membershipCompanyId' => 'id'
            )
        )
            ->join(
            array('m' => 'membership'),
            'm.id = ma.membershipId',
            array(
                'membershipName' => 'name',
                'price'
            )
        )
            ->where('ma.companyId = ?', $this->_companyRow->getId(), Zend_Db::INT_TYPE);
        $response = $select->query()->fetch();
        if($response==false){
            return array();
        }
        $response['startDatetime'] = new DateTime($response['startDatetime']);
        $response['startDatetime']=$response['startDatetime']->format('d-m-Y');
        $response['endDatetime'] = new DateTime($response['endDatetime']);
        $response['endDatetime']=$response['endDatetime']->format('d-m-Y');

        return $response;
    }

    /**
     * Create the new rating row
     *
     * @param array $data
     * @return Membership_Model_Membership
     */

    public function createSubscribe( array $data)
    {
        $dt = new DateTime();
        $today = $dt->format('Y-m-d');


        if(!isset($data['membershipId'])) {
            throw new OSDN_Exception('Please select type membership for this company!');
            return ;
        }

        if (!isset($data['startDatetime'])) {
            $row['startDatetime'] = $today;
        } else {
             $startDate = new DateTime($data['startDatetime']);
             $data['startDatetime'] =$startDate->format('Y-m-d');
             $row['startDatetime'] = $data['startDatetime'];
        }

        if ($data['startDatetime'] < $dt->format('Y-m-d')) {
            throw new OSDN_Exception('Invalid start date!');
        }

        $serviceMembership = new Membership_Service_Membership();

        $membership = $serviceMembership->find($data['membershipId']);

        $interval = new DateInterval("P".$membership['name']."M");

        $row['companyId'] = $this->_companyRow->id;
        $row['membershipId'] = $data['membershipId'];
        $row['endDatetime'] = $dt->add($interval)->format('Y-m-d');

        $row = $this->_table->createRow($row);
        $row->save();


        return $row;
    }

    /**
     * @param $id
     * @param array $data
     * @return Membership_Model_MembershipCompany
     */

    public function updateSubscribe(array $data)
    {
        $dt = new DateTime();
        $today = $dt->format('Y-m-d');

        $membershipCompanyRow = $this->fetch();

        if ($membershipCompanyRow== null) {
            throw new OSDN_Exception('Please select type membership for this company!');
        }

        if (!isset($data['startDatetime'])) {
            $data['startDatetime'] = $today;
        }

        $row = $membershipCompanyRow->toArray();

        $serviceMembership = new Membership_Service_Membership();
        $membership = $serviceMembership->find($row['membershipId']);
        $interval = new DateInterval("P".$membership['amount']."M");

            $row['endDatetime'] =  $this->formatDate($row['endDatetime']);
            if (\strtotime($row['endDatetime'])>=\strtotime($today)) {
                throw new OSDN_Exception('This company has active membership!');
            } else {
                $startDate = new DateTime($data['startDatetime']);
                $data['startDatetime'] =$startDate->format('Y-m-d');
                $data['endDatetime'] = $startDate->add($interval)->format('Y-m-d');
            }

            if ($data['startDatetime'] < $dt->format('Y-m-d')) {
                throw new OSDN_Exception('Invalid start date!');
            }
        $membershipCompanyRow->setFromArray($data);
        $membershipCompanyRow->save();

        return $membershipCompanyRow;
    }

    /**
     * @todo  should be done via event manager
     *
     * @FIXME Add main global transation for that
     *
     * @param $id
     *
     * @return bool
     */
    public function delete($id)
    {
        $membershipCompanyRow = $this->find($id);
        return false !== $membershipCompanyRow->delete();
    }
}