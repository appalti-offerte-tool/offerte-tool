<?php

class Membership_Service_AclRole extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Account_Model_DbTable_AclRole
     */
    protected $_table;

    /**
     * @var Account_Model_DbTable_AclRoleRow
     */
    protected $_membershipRootRow;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Service_Abstract::_init()
     */
    protected function _init()
    {
        $this->_table = new \Account_Model_DbTable_AclRole();

        $this->_membershipRootRow = $this->_table->fetchRow(array('luid = ?' => 'membership'));

        if (null === $this->_membershipRootRow) {
            throw new OSDN_Exception('Unable to find membership row');
        }
        parent::_init();
    }

    /**
     * @param int $id
     * @param boolean $throwException
     *
     * @return \Membership_Model_AclRole
     */
    public function find($id, $throwException = true)
    {
        $aclRoleRow = $this->_table->findOne($id);
        if (null === $aclRoleRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find aclRole #' . $id);
        }

        if ($aclRoleRow->parentId != $this->_membershipRootRow->getId()) {
            throw new OSDN_Exception('Role should be behind membership root');
        }

        return $aclRoleRow;
    }

    /**
     * @param array $params
     * @return OSDN_Db_Response
     */
    public function fetchAllWithResponse(array $params = array())
    {
        $select = $this->_table->select(true)
            ->where('parentId = ?', $this->_membershipRootRow->getId(), Zend_Db::INT_TYPE);

        $this->_initDbFilter($select, $this->_table)->parse($params);
        return $this->getDecorator('response')->decorate($select);
    }
}