<?php

final class News_Misc_Navigation_NewsDetailPageMvc extends Zend_Navigation_Page_Mvc
{
    protected $_isCompleted = false;

    /**
     * (non-PHPdoc)
     * @see Zend_Navigation_Page::getLabel()
     */
    public function getLabel()
    {
        if (true !== $this->_isCompleted) {
            $newsId = (int) Zend_Controller_Front::getInstance()->getRequest()->getParam('newsId');
            if (!empty($newsId)) {
                $newsService = new News_Service_News();
                $newsRow = $newsService->find($newsId);

                $this->_label .= ' ' . $newsRow->title;
            }

            $this->_isCompleted = true;
        }

        return parent::getLabel();
    }
}