<?php

class News_Migration_00000000_000000_00 extends Core_Migration_Abstract
{

    public function up()
    {
        $this->createTable('newsCategory');
        $this->createColumn('newsCategory', 'name', Core_Migration_Abstract::TYPE_VARCHAR, 50);
       
        $this->createTable('news');
        $this->createColumn('news', 'newsCategoryId', Core_Migration_Abstract::TYPE_INT, 11, null, false);
        $this->createColumn('news', 'title', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('news', 'publish', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createColumn('news', 'publishDate', Core_Migration_Abstract::TYPE_TIMESTAMP);
        $this->createColumn('news', 'modifiedDate', Core_Migration_Abstract::TYPE_TIMESTAMP);
        $this->createColumn('news', 'desc', Core_Migration_Abstract::TYPE_TEXT);
        $this->createColumn('news', 'text', Core_Migration_Abstract::TYPE_TEXT);
        $this->createColumn('news', 'directShareLink', Core_Migration_Abstract::TYPE_VARCHAR, 50);
        $this->createIndex('news', 'newsCategoryId');

        $this->createForeignKey('news', array('newsCategoryId'), 'newsCategory', array('id'), 'FK_newsCategoryId');
    }

    public function down()
    {
        $this->dropTable('news');
        $this->dropTable('newsCategory');
    }


}

