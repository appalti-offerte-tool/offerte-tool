<?php

class News_Service_NewsSubscription extends \OSDN_Application_Service_Dbable
{
    protected $_table;

    protected function _init()
    {
        $this->_table = new News_Model_DbTable_NewsSubscription($this->getAdapter());

        parent::_init();
    }

    public function fetchAllWithSubscription($categories)
    {
        $accountId = Zend_Auth::getInstance()->getIdentity()->getId();
        $subscribeRow = $this->fetchAllByAccountId($accountId);
        for ($i = 0; $i < sizeof($categories); $i++) {
            $categories[$i]['checked'] = null;
            for ($j = 0; $j < sizeof($subscribeRow); $j++) {
                if ($categories[$i]['id'] == $subscribeRow[$j]['newsCategoryId']) {
                    $categories[$i]['checked'] = 'checked';
                }
            }
        }
        return $categories;

    }

    public function fetchAllByAccountId($id)
    {
        try {
            $select = $this->_table->getAdapter()->select()
                ->from($this->_table->getTableName())
                ->where('accountId = ?', $id)
                ->order('id ASC');
            $response = $this->getDecorator('response')->decorate($select);
            $response = $response->getRowset();
            return $response;
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to select subscription', 0, $e);
        }
    }

    public function delete($id)
    {
        try {
            $this->_table->deleteQuote(array('accountId = ?' => $id));
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to delete category');
            return false;
        }
        return true;
    }

    public function deleteByCategoryId($id)
    {
        try {
            $this->_table->deleteQuote(array('newsCategoryId = ?' => $id));
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to delete category');
            return false;
        }
        return true;
    }

    public function insert($data)
    {
        try {
            $row = $this->_table->createRow($data);
            $row->save();
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to create category', 0, $e);
        }
        return $row->toArray();
    }

    public function update($categories, $data)
    {
        $accountId = Zend_Auth::getInstance()->getIdentity()->getId();
        $response = $this->fetchAllByAccountId($accountId);
        if ($response != null) {
            $this->delete($accountId);
        }
        foreach ($categories as $category) {
            if (isset($data['categoryId_' . $category['id']])) {
                $this->insert(array(
                    'accountId'     => $accountId,
                    'newsCategoryId'=> $category['id']
                ));
            }
        }
    }

}
