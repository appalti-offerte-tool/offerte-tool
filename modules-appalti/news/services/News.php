<?php

class News_Service_News extends \OSDN_Application_Service_Dbable
{
    const LINK = '/news/index/preview/newsId/';
    const URL = 'http://';

    /**
     * @var News_Model_DbTable_News
     */
    protected $_table;

    protected function _init()
    {
        $this->_table = new News_Model_DbTable_News($this->getAdapter());

        $this->_attachValidationRules('default', array (
            'newsCategoryId' => array ( 'allowEmpty' => true, 'presence'   => 'required' ),
            'startPublish'   => array ( 'allowEmpty' => false, 'presence'   => 'required' ),
            'endPublish'     => array ( 'allowEmpty' => true, 'presence'   => 'required' ),
            'title'          => array ( 'allowEmpty' => false, 'presence'   => 'required' ),
            'desc'           => array ( 'allowEmpty' => false, 'presence'   => 'required' ),
            'text'           => array ( 'allowEmpty' => false, 'presence'   => 'required' ),
            'isActive'       => array ( 'allowEmpty' => true, 'presence'   => 'required' ),
        ));
        parent::_init();
    }

    /**
     * @param $id
     *
     * @return string Link to news
     */
    private function createLink($id)
    {
        return self::URL . $_SERVER['HTTP_HOST'] . self::LINK . $id;
    }

    /**
     * @param      $id
     * @param bool $throwException
     *
     * @return mixed
     * @throws OSDN_Exception
     */
    public function find($id, $throwException = true)
    {
        $newsRow = $this->_table->findOne($id);
        if (NULL === $newsRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find news #' . $id);
        }
        return $newsRow;
    }

    /**
     * @param $categoryId
     *
     * @return mixed
     * @throws OSDN_Exception
     */
    public function findWithEmptyFlag($categoryId = null , $throwException = true)
    {
        $params = array(
            'emptyFlag = 1',
            'accountId = ?' => Zend_Auth::getInstance()->getIdentity()->getId()
        );

        if ($categoryId != null) {
            $params['newsCategoryId'] =  $categoryId;
        }
        $newsRow = $this->_table->fetchRow($params);
        if (!$newsRow) {
            try {
                $newsRow = $this->_table->createRow(array(
                    'emptyFlag' => 1,
                    'accountId' => Zend_Auth::getInstance()->getIdentity()->getId()
                ));
                $newsRow->save();
            } catch (\Exception $e) {
                throw new \OSDN_Exception('Unable to create empty news row. Error: ' . $e->getMessage());
            }
        }

        return $newsRow;
    }

    /**
     * @param $data
     *
     * @return bool
     * @throws OSDN_Exception
     *
     * function archiving news
     */
    public function archiveNews($data)
    {
        try {
            $this->_table->updateByPk($data, $data['id']);
            return true;
        } catch (\Exception $e) {
            return false;
            throw new \OSDN_Exception('Unable to archive news', 0, $e);
        }
    }
    /**
    * @param array $params
    * @return mixed
    * @throws OSDN_Exception
    */
    public function fetchAllWithResponse(array $params = array ())
    {
        try {
            $select = $this->_table->getAdapter()->select()
                ->from($this->_table->getTableName())
                ->where('emptyFlag=0')
                ->where('isArchive=?', !empty($params['isArchive']) ? $params['isArchive'] : 0 , Zend_Db::INT_TYPE)
                ->order('startPublish DESC');

            $f = $this->_initDbFilter($select, $this->_table);
            $f->setStrategy(OSDN_Db_Select_Filter_Abstract::STRATEGY_COMBO);
            $f->parse($params);

            $response = $this->getDecorator('response')->decorate($select);
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to select news list', 0, $e);
        }
        return $response;
    }

    public function fetchAllArchive()
    {
        return $this->fetchAllWithResponse(array( 'isArchive' => 1 ));
    }

    public function fetchAllByPeriod(array $params = array ())
    {

        if (strtotime($params['startDate']) > strtotime($params['endDate'])) {
            throw new OSDN_Exception('Incorrect dates ');
        }
        $data = strtotime($params['startDate']);
        $params['startDate'] = date('Y-m-d', $data);

        $data = strtotime($params['endDate']);
        $params['endDate'] = date('Y-m-d', $data);

        try {
            $select = $this->_table->getAdapter()->select()
                ->from($this->_table->getTableName())
                ->where('emptyFlag=0')
                ->where('(endPublish >= ' . $params['startDate'] . ') or (endPublish is null)')
                ->where('startPublish <= ?', $params['endDate'])
                ->where('isArchive=?', !empty($params['isArchive']) ? $params['isArchive'] : 0 ,  Zend_Db::INT_TYPE)
                ->order('startPublish ASC');

            if (!empty($params['categoryId'])) {
                $select->where('newsCategoryId = ?', $params['categoryId'], Zend_Db::INT_TYPE);
            }
            $this->_initDbFilter($select, $this->_table)->parse($params);
            $response = $this->getDecorator('response')->decorate($select);
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to select news list', 0, $e);
        }
        return $response;
    }



    public function fetchLast($archive = 0 ,$count = 1)
    {
        try {
            $select = $this->_table->getAdapter()->select()
                ->from($this->_table->getTableName())
                ->where('emptyFlag=0')
                ->where('isArchive=?', $archive)
                ->order('startPublish DESC')
                ->limit($count);
            $response = $this->getDecorator('response')->decorate($select);
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to select news list', 0, $e);
        }
        return $response;
    }

    public function create($data)
    {
        try {
            if (!empty($data['endPublish'])){
                if (strtotime($data['startPublish']) > strtotime($data['endPublish'])) {
                    throw new OSDN_Exception('Incorrect dates ');
                }
            }
            $f = $this->_validate($data);
            $row = $this->_table->createRow($f->getData());
            $row->save();
            $row = $row->toArray();
            $row['directShareLink'] = $this->createLink($row['id']);
            $this->_table->updateByPk($row, $row['id']);
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to create news', 0, $e);
        }
        return $row;
    }

    public function delete($id)
    {
        $newsFileStockService = new News_Service_NewsFileStock();
        try {
            $newsRow = $this->find($id);
            $id = $newsRow->getId();
            $newsFileStockService->deleteByNewsId($id);
            $newsRow->delete();
            return true;
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to delete news', 0, $e);
        }

    }

    public function deleteAllByCategoryId($id)
    {
        $newsFileStockService = new News_Service_NewsFileStock();
        $newsRow = $this->_table->fetchAll(array ('newsCategoryId = ?' => $id));
        $newsRow = $newsRow->toArray();
        try {
            foreach ($newsRow as $row) {
                $newsFileStockService->deleteByNewsId($row['id']);
            }
            $this->_table->deleteQuote(array ('newsCategoryId = ?' => $id));
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to delete news', 0, $e);
        }
        return true;


    }

    public function update($id, array $data)
    {
        $newsRow = $this->find($id);
        if ($newsRow['emptyFlag'] == '1') {
            $data['emptyFlag'] = 0;
            $data['directShareLink'] = $this->createLink($id);
        } else {
            $data['modifiedDate'] = 'allow';
        }
        try {

            if (!empty($data['endPublish'])){
                if (strtotime($data['startPublish']) > strtotime($data['endPublish'])) {
                    throw new OSDN_Exception('Incorrect dates ');
                }
            }

            $f = $this->_validate($data);
            $newsRow->setFromArray($f->getData());
            $newsRow->save();
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to update news', 0, $e);
        }
        return $newsRow;
    }

}

