<?php

class News_Service_NewsFileStock extends \OSDN_Application_Service_Dbable
{
    /**
     * @var News_Model_DbTable_News
     */
    protected $_table;
    /**
     * @var $_fileStock FileStock_Service_FileStock
     */
    protected $_fileStock;

    public function _init()
    {
        $this->_table = new \News_Model_DbTable_News($this->getAdapter());
        $this->_fileStock = new \FileStock_Service_FileStock(array(
            'baseFilePath'       => APPLICATION_PATH . '/modules-appalti/news/data/news-file/',
            'useCustomExtension' => false,
            'identity'           => 'news'
        ));
    }

    public function fetchAllFileStockByNewsIdWithResponse($newsId)
    {
        $newsRow = $this->_table->findOne($newsId);
        if (empty($newsRow)) {
            throw new \OSDN_Exception('Unable to find #' . $newsId);
        }
        $fileStockTable = new \FileStock_Model_DbTable_FileStock($this->getAdapter());
        $select = $this->_table->getAdapter()->select()
            ->from(
                array('df' => 'newsFileStockRel'), array()
            )
            ->join(
                array('f' => 'fileStock'), 'f.id = df.fileStockId', $fileStockTable->getAllowedColumns()
            )
            ->join(
                array(
                    'dfThumbnail' => 'newsFileStockRel'), 'dfThumbnail.parentId = df.id AND dfThumbnail.isThumbnail = 1',
                    array(
                        'thumbnailFileStockId' => 'fileStockId',
                        'isThumbnail',
                        'isDefaultThumbnail'
                     )
             )
            ->where('df.newsId = ?', $newsRow->getId(), Zend_Db::INT_TYPE)
            ->where('df.parentId IS NULL');
        $response = $this->getDecorator('response')->decorate($select);
        $storage = $this->_fileStock->factory();
        $response->setScope($this);
        $response->setRowsetCallback(function($row, $rowset, $scope) use ($storage, $newsRow)
        {
            $thumbnailFileStockRow = $scope->find($newsRow->getId(), $row['thumbnailFileStockId']);
            $row['thumbnailLink'] = $storage->toFilePathHost($thumbnailFileStockRow);
            $fileStockRow = $scope->find($newsRow->getId(), $row['id']);
            $row['link'] = $storage->toFilePathHost($fileStockRow);
            return $row;
        });

        return $response;
    }

    public function fetchFileStockThumbnailRowByNews($newsId, $fileStockId = null)
    {
        $select = $this->getAdapter()->select()
            ->from(
                array('df' => 'newsFileStockRel'),
                array('parentId')
            )
            ->join(
                array('f' => 'fileStock'),
                'f.id = df.fileStockId',
                array('id')
             )
            ->join(
                array('dfp'    => 'newsFileStockRel'),
                'df.parentId = dfp.id',
                array()
             )
            ->where('df.newsId = ?', $newsId, Zend_Db::INT_TYPE)
            ->where('df.isThumbnail = 1')
            ->limit(1);

        if (empty($fileStockId)) {
            $select->where('dfp.fileStockId = ?', $fileStockId, Zend_Db::INT_TYPE);
        }

        $foundRow = $select->query()->fetch();
        if (empty($foundRow)) {
            return null;
        }

        $fileStockId = $foundRow['id'];

        if (null == ($fileStockRow = $this->_fileStock->find($fileStockId))) {
            return null;
        }

        $row = $fileStockRow->toArray();

        $storage = $this->_fileStock->factory();

        if (!empty($foundRow['parentId'])) {
            $newsFileStockRelation = new News_Model_DbTable_NewsFileStockRel();
            $newsFileStockRelationRow = $newsFileStockRelation->findOne($foundRow['parentId']) ;
            $row['parentLink'] = $storage->toFilePathHost($newsFileStockRelationRow->getFileStockRow());

        }

        $row['link'] = $storage->toFilePathHost($fileStockRow);

        return $row;
    }

    public function find($newsId, $fileStockId)
    {
        return $this->_fileStock->find($fileStockId);
    }

    public function create(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {

        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }
        if (null === ($newsRow = $this->_table->findOne($data['newsId']))) {
            $data['emptyFlag'] = '1';
            $data['title'] = null;
            $data['text'] = null;
            $newsRow = $this->_table->createRow($data);
            $newsRow->save();
        }
        $newsFileStockRelation = new News_Model_DbTable_NewsFileStockRel();
        $newsFileStockRelationRow = null;
        $fileStockRow = $this->_fileStock
            ->create($transfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
        use ($newsRow, $newsFileStockRelation, & $newsFileStockRelationRow)
        {
            $newsFileStockRelationRow = $newsFileStockRelation->createRow(array(
                'fileStockId' => $fileStockRow->getId(),
                'newsId'      => $newsRow->getId()
            ));
            $newsFileStockRelationRow->save();
        });
        $rawFileTransfer = new FileStock_Service_Transfer_Raw();
        $storage = $this->_fileStock->factory();
        $link = $storage->toFilePath($fileStockRow, true);
        $rawFileTransfer->addFile($link);
        require_once APPLICATION_PATH . '/modules/file-stock/views/scripts/filters/File/Duplicate.php';
        $rawFileTransfer->addFilter(new FileStock_View_Filter_File_Duplicate($link . '.tmp', null, true));

        $isf = new Zend_Filter_ImageSize();
        $isf->setHeight(100);
        $isf->setWidth(100);
        $isf->setThumnailDirectory(dirname($link));
        $rawFileTransfer->addFilter($isf);

        $fileStockThumRow = $this->_fileStock->create($rawFileTransfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
        use ($newsRow, $newsFileStockRelationRow, $newsFileStockRelation)
        {
            $hasPredefinedThumbnail = $newsFileStockRelation->count(array(
                'newsId = ?' => $newsRow->getId(),
                'isThumbnail = 1'
            )) > 0;
            $newsRow->getTable()->getAdapter()->insert('newsFileStockRel', array(
                'fileStockId'        => $fileStockRow->getId(),
                'newsId'             => $newsRow->getId(),
                'isThumbnail'        => 1,
                'isDefaultThumbnail' => !$hasPredefinedThumbnail,
                'parentId'           => $newsFileStockRelationRow->id
            ));
            $newsRow->save();
        });
        return array($newsRow, $fileStockRow);
    }

    public function update(array $data, \FileStock_Service_Transfer_Http $transfer = null)
    {
        $newsRow = $this->_table->findOne($data['newsId']);
        if (empty($newsRow)) {
            throw new OSDN_Exception('Unable to find news #' . $data['newsId']);
        }
        if (null === $transfer) {
            $transfer = new \FileStock_Service_Transfer_Http();
        }
        $this->getAdapter()->beginTransaction();
        try {
            $fileStockRow = $this->_fileStock->update($transfer, $data);
            $newsFileStockRelation = new News_Model_DbTable_NewsFileStockRel();
            $newsFileStockRelationRow = $newsFileStockRelation->fetchRow(array(
                'newsId = ?'      => $newsRow->getId(),
                'fileStockId = ?' => $fileStockRow->getId(),
                'parentId IS NULL'
            ));
            if (null !== $newsFileStockRelationRow) {
                foreach ($newsFileStockRelationRow->getChildrenRowset() as $row) {

                    $this->_fileStock->delete($row->fileStockId, function($fileStockRow) use ($row) {
                        $row->delete();
                    });
                }
            }
            $rawFileTransfer = new FileStock_Service_Transfer_Raw();
            $storage = $this->_fileStock->factory();
            $link = $storage->toFilePath($fileStockRow, true);
            $rawFileTransfer->addFile($link);
            require_once APPLICATION_PATH . '/modules/file-stock/views/scripts/filters/File/Duplicate.php';
            $rawFileTransfer->addFilter(new FileStock_View_Filter_File_Duplicate($link . '.tmp', null, true));

            $isf = new Zend_Filter_ImageSize();
            $isf->setHeight(100);
            $isf->setWidth(100);
            $isf->setThumnailDirectory(dirname($link));

            $rawFileTransfer->addFilter($isf);

            $fileStockThumbRow = $this->_fileStock->create($rawFileTransfer, $data, function(\FileStock_Model_DbTable_FileStockRow $fileStockRow) use ($newsRow, $newsFileStockRelationRow) {
                $newsRow->getTable()->getAdapter()->insert('newsFileStockRel', array(
                    'fileStockId' => $fileStockRow->getId(),
                    'newsId'      => $newsRow->getId(),
                    'isThumbnail' => 1,
                    'parentId'    => $newsFileStockRelationRow->id
                ));
            });
            $this->getAdapter()->commit();
        } catch (\Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        }
        return $fileStockRow;
    }

    public function delete($newsId, $fileStockId)
    {

        $newsRow = $this->_table->findOne($newsId);
        if (empty($newsRow)) {
            throw new OSDN_Exception('Unable to find news #' . $newsId);
        }
//        $this->getAdapter()->beginTransaction();
        try {


            $newsFileStockRelation = new \News_Model_DbTable_NewsFileStockRel();
            $scope = $this->_fileStock;

            $callbackFn = function(\FileStock_Model_DbTable_FileStockRow $fileStockRow)
            use ($newsId, $newsFileStockRelation, $scope, & $callbackFn)
            {
                $newsFileStockRelationRow = $newsFileStockRelation->fetchRow(array(
                    'newsId = ?'      => $newsId,
                    'fileStockId = ?' => $fileStockRow->getId()
                ));
                if (null !== $newsFileStockRelationRow) {
                    foreach ($newsFileStockRelationRow->getChildrenRowset() as $row) {
                        $scope->delete($row->fileStockId, $callbackFn);
                    }
                    $newsFileStockRelationRow->delete();
                }
            };
            $result = $this->_fileStock->delete($fileStockId, $callbackFn);
//            $this->getAdapter()->commit();
        } catch (\Exception $e) {
//            $this->getAdapter()->rollBack();
            throw $e;
        }
        return (boolean)$result;
    }

    public function deleteByNewsId($id)
    {
        $newsFileStockRelation = new \News_Model_DbTable_NewsFileStockRel();
        $rowset = $newsFileStockRelation->fetchAll(array(
            'newsId = ?' => $id
        ));
        $rowset = $rowset->toArray();
        foreach ($rowset as $row) {
            if ($row['parentId'] == null) {
                $this->delete($id, $row['fileStockId']);
            }
        }
        return true;
    }

    public function setDefaultThumbnail($newsId, $fileStockId)
    {
        $newsFileStockRelation = new \News_Model_DbTable_NewsFileStockRel();
        $this->getAdapter()->beginTransaction();
        try {
            $newsFileStockRelation->updateQuote(
                array(
                    'isDefaultThumbnail' => 0
                ), array(
                    'newsId = ?' => (int)$newsId,
                    'isThumbnail = 1'
                )
            );
            $newsFileStockRelationRow = $newsFileStockRelation->fetchRow(array(
                    'newsId = ?'      => (int)$newsId,
                    'fileStockId = ?' => (int)$fileStockId
                )
            );
            if (null == $newsFileStockRelationRow) {
                throw new OSDN_Exception('Unable to find row');
            }
            $affectedRows = $newsFileStockRelation->updateQuote(array(
                'isDefaultThumbnail' => 1
            ), array(
                'newsId = ?'   => (int)$newsId,
                'parentId = ?' => (int)$newsFileStockRelationRow->id,
                'isThumbnail = 1'
            ));
            if (0 == $affectedRows) {
                throw new \OSDN_Exception('Unable to change default thumbnail');
            }
            $this->getAdapter()->commit();
        } catch (\Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        }
    }

}