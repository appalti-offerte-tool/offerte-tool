<?php

class News_Service_NewsCategory extends \OSDN_Application_Service_Dbable
{
    protected $_table;

    protected function _init()
    {
        $this->_table = new News_Model_DbTable_NewsCategory($this->getAdapter());
        $this->_attachValidationRules('default', array(
            'name' => array('allowEmpty' => false, 'presence'   => 'required')
        ));

        parent::_init();
    }

    public function find($id, $throwException = true)
    {
        $categoryRow = $this->_table->findOne($id);
        if (null === $categoryRow && true === $throwException) {
            throw new OSDN_Exception('Unable to find category #' . $id);
        }
        return $categoryRow;
    }

    public function fetchAllWithResponse(array $params = array())
    {
        try {
            $select = $this->_table->getAdapter()->select()
                ->from($this->_table->getTableName(),
                array('id', 'name')
            );
            $this->_initDbFilter($select, $this->_table)->parse($params);
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to select category', 0, $e);
        }
        return $this->getDecorator('response')->decorate($select);
    }

    public function update($id, $data)
    {
        $categoryRow = $this->find($id);
        try {
            $categoryRow->setFromArray($data);
            $categoryRow->save();
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to update category', 0, $e);
        }
        return $categoryRow;
    }

    public function delete($id)
    {
        $newsService = new News_Service_News();
        $subscriptionService = new News_Service_NewsSubscription();
        try {
            $categoryRow = $this->find($id);
            $newsService->deleteAllByCategoryId($id);
            $subscriptionService->deleteByCategoryId($id);
            $categoryRow->delete();
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to delete category');
            return false;
        }
        return true;
    }

    public function create($data)
    {
        try {
            $f = $this->_validate($data);
            $row = $this->_table->createRow($f->getData());
            $row->save();
        } catch (\Exception $e) {
            throw new \OSDN_Exception('Unable to create category', 0, $e);
        }

        return $row->toArray();
    }
}
