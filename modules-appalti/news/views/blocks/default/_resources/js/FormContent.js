Ext.define('Module.Application.Model.Error', {
    extend:'Ext.data.Model',
    fields:[
        'message', 'type', 'field', {name:'id', mapping:'field'}, {name:'msg', mapping:'message'}
    ]
});
Ext.define('Module.News.Block.FormContent', {
    extend:'Ext.form.Panel',
    alias:'widget.news.block.form-content',
    bodyPadding:5,
    newsId:null,
    categoryId:null,
    initComponent:function() {
        this.initialConfig.trackResetOnLoad = true;
        this.items = [
            {
                xtype:'textfield',
                fieldLabel:lang('Title'),
                name:'title',
                labelWidth:85,
                allowBlank:false,
                anchor:'100%'
            },
//            {
//
//                xtype:'textfield',
//                fieldLabel:lang ('Publish'),
//                labelWidth:60,
//                width:420,
//                name:'publish'
//
//            },
            {
                xtype:'label',
                text:lang('Publish period'),
                style:{
                    fontWeight:'bold'
                }
            }, {
                xtype:'datefield',
                submitFormat: 'Y-m-d',
                fieldLabel:lang('start') + '<span class="asterisk">*</span>',
                name:'startPublish',
                format:'d-m-Y',
                labelWidth:85,
                margin:'5 0 0 0',
                allowBlank:false,
                width:180
            }, {
                xtype:'datefield',
                submitFormat: 'Y-m-d',
                fieldLabel:lang('end') + '<span class="asterisk">*</span>',
                name:'endPublish',
                format:'d-m-Y',
                margin:'-60 0 5 190',
                allowBlank:true,
                labelWidth:40,
                width:145
            }, {
                xtype:'htmleditor',
                fieldLabel:lang('Description'),
                name:'desc',
                height:100,
                anchor:'100%',
                labelWidth:85,
                allowBlank:true
            }
        ];
        var field = Ext.widget('tinymcefield', {
            border:false,
            labelAlign:'top',
            anchor:"100%",
            name:'text',
            allowBlank:0,
            margin:'0 0 12 0',
            height:445,
            tinymceConfig:{
                theme:"advanced",
                plugins:"pagebreak,table,advhr,advimage,advlink,iespell,insertdatetime,media,searchreplace,print,contextmenu,paste",
                theme_advanced_buttons1:"bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
                theme_advanced_buttons2:"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,|,forecolor,backcolor",
                theme_advanced_buttons3:"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,iespell,media,advhr,|,print,|pagebreak",
                theme_advanced_buttons4:"",
                theme_advanced_toolbar_location:"top",
                theme_advanced_toolbar_align:"left",
                theme_advanced_statusbar_location:"bottom",
                theme_advanced_resizing:false,
                extended_valid_elements:"a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
                file_browser_callback:Ext.bind(this.fileCallbackFn, this),
                baseURI:'',
                relative_urls:false,
                convert_urls:false,
                skin:'o2k7'
            }
        }, this);
        this.items.push(field);
        this.callParent(arguments);
    },

    fileCallbackFn:function(field_name, url, type, win) {
        var newsId = this.newsId;
        var me = this;
        Application.require([
            'file-stock/image/image-layout'
        ], function() {
            var field = win.document.forms[0].elements[field_name];
            var fi = new Module.FileStock.Image.Layout({
                border:false,
                entityId:newsId,
                extraParams:{
                    newsCategoryId:this.categoryId
                },
                enablePickerContextMenu:true,
                entityName:'newsId',
                entityType:'news',
                controller:'news-file-stock',
                module:'news'
            }, this);
            var w = new Ext.Window({
                modal:true,
                width:650,
                height:350,
                border:false,
                title:lang('Image dialogue'),
                layout:'fit',
                items:[fi]
            });
            fi.on('choose-image', function(p, record, link) {
                field.value = link;
                Ext.fly(field).blur();
                if (Ext.isFunction(win.ImageDialog.getImageData)) {
                    win.ImageDialog.getImageData();
                }
                if (Ext.isFunction(win.ImageDialog.showPreviewImage)) {
                    win.ImageDialog.showPreviewImage(link);
                }
                w.close();
            }, this);
            fi.on('entity-change', function(image, newsId) {
                this.newsId = newsId;
                me.newsId = newsId;
            }, this);
            w.show(Ext.get(field));
        }, this);
    },

    setNewsId:function(newsId) {
        this.newsId = newsId;
        return this;
    },

    setCategoryId:function(categoryId) {
        this.categoryId = categoryId;
        return this;
    }

});