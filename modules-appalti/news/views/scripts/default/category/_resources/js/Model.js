Ext.define('Module.News.Model.Category', {
    extend:'Ext.data.Model',
    fields:['id', 'name']
});