Ext.define ('Module.Application.Model.Error', {
    extend:'Ext.data.Model',
    fields:[ 'message', 'type', 'field', {
        name:'id',
        mapping:'field'
    }, {
        name:'msg',
        mapping:'message'
    } ]
});
Ext.define ('Module.News.Category.Form', {
    extend:'Ext.form.Panel',
    alias:'widget.module.news.category.form',
    bodyPadding:5,
    categoryId:null,
    trackResetOnLoad:true,
    waitMsgTarget:true,
    wnd:null,
    model:'Module.News.Model.Category',
    initComponent:function () {
        this.initialConfig.reader = new Ext.data.reader.Json ({
            model:'Module.News.Model.Category',
            type:'json',
            root:'row'
        });
        this.errorReader = this.initialConfig.errorReader = new Ext.data.reader.Json ({
            model:'Module.Application.Model.Error',
            type:'json',
            root:'messages',
            successProperty:'success'
        });
        this.initialConfig.trackResetOnLoad = true;
        this.items = [
            {
                xtype:'textfield',
                fieldLabel:lang ('Name'),
                labelWidth:50,
                width:400,
                name:'name'
            }
        ];
        this.callParent (arguments);
    },

    doLoad:function () {
        if (this.categoryId) {
            this.form.load ({
                url:link ('news', 'category', 'fetch-row', {
                    format:'json'
                }),
                method:'get',
                params:{
                    categoryId:this.categoryId
                },
                scope:this
            });
        }
    },

    showInWindow:function () {
        this.border = false;
        var w = this.wnd = new Ext.Window ({
            title:lang ('Category'),
            resizable:false,
            layout:'fit',
            border:false,
            modal:true,
            items:[ this ],
            buttons:[
                {
                    text:lang ('Save'),
                    handler:this.onSubmit,
                    scope:this
                },
                {
                    text:lang ('Close'),
                    handler:function () {
                        w.close ();
                        this.wnd = null;
                    },
                    scope:this
                }
            ]
        });
        this.doLoad ();
        w.show ();
        return w;
    },

    onSubmit:function (panel, w) {
        var params = {};
        if (this.categoryId) {
            var action = 'edit';
            params['categoryId'] = this.categoryId;
        }
        if (!this.categoryId) {
            var action = 'create';
        }
        this.form.submit ({
            url:link ('news', 'category', action, {
                format:'json'
            }),
            method:'post',
            params:params,
            waitMsg:Ext.LoadMask.prototype.msg,
            success:function (form, action) {
                var responseObj = Ext.decode (action.response.responseText);
                Application.notificate (action);
                if (action.result.success) {
                    this.fireEvent ('completed', this, responseObj.id, action.response);
                    this.wnd && this.wnd.close ();
                }
            },
            scope:this
        });
    }
});