Ext.define('Module.News.Model.News', {
    extend:'Ext.data.Model',
    fields:[
        'id', 'text','desc', 'title', 'isArchive', 'shortDesc',
        {name:'startPublish',   type:'date',    dateFormat:'Y-m-d'},
        {name:'endPublish',     type:'date',    dateFormat:'Y-m-d'},
        {name:'modifiedDate',   type:'date',    dateFormat:'Y-m-d'}
    ]
});