Ext.define ('Module.News.Form', {
    extend:'Ext.form.Panel',
    alias:'widget.module.news.news-form',
    model:'Module.News.Model.News',
    newsId:null,
    categoryId:null,
    waitMsgTarget:true,
    wnd:null,
    initComponent:function () {
        this.blocks = new Module.Application.BlockTabPanel ({
            border:false,
            invokeArgs:{
                categoryId:this.categoryId,
                newsId:this.newsId
            }
        });
        this.items = [ this.blocks ];
        this.callParent (arguments);
        this.blocks.on ('tabchange', function (tb, b) {
            b && b.reload ();
            b && b.doLayout ();
        });
        var params = {};
        params.keywords = 'form';
        params.module = 'news';
        this.on ('render', function () {
            var me = this;
            this.blocks.reload (params, function (tp) {
                var block = me.getFormPreviewBlock ();
                if (null !== block) {
                    if (!me.newsId) {
                        block.setDisabled (true);
                    }
                }
            });
        }, this);
        this.blocks.on ('backgroundinvokeargschanged', function (bp, ia) {
            if (ia.newsId) {
                this.newsId = ia.newsId;
            }
            return false;
        }, this);
    },

    getFormPreviewBlock:function () {
        var t = null;
        this.blocks.items.each (function (tab) {
            if (/form-preview/.test (tab.id)) {
                t = tab;
                return false;
            }
        });
        return t;
    },

    showInWindow:function () {
        this.border = false;
        var w = this.wnd = new Ext.Window ({
            title:this.newsId ? lang ('Update') : lang ('Create'),
            resizable:false,
            width:850,
            height:705,
            border:false,
            modal:true,
            items:[ this ],
            buttons:[
                {
                    text:this.newsId ? lang ('Update') : lang ('Create'),
                    handler:this.onSubmit,
                    scope:this
                },
                {
                    text:lang ('Close'),
                    handler:function () {
                        w.close ();
                        this.wnd = null;
                    },
                    scope:this
                }
            ]
        });
        w.show ();
        return w;
    },

    onSubmit:function (panel, w) {
        var params = {};
        var action = '';
        this.newsId = this.blocks.activeTab.newsId;
        if (this.newsId) {
            action = 'edit';
            params['newsId'] = this.newsId;
            params['newsCategoryId'] = this.categoryId;
        }
        if (!this.newsId) {
            this.newsId = this.blocks.activeTab.form.newsId;
            if (this.newsId) {
                action = 'edit';
                params['newsId'] = this.newsId;
                params['newsCategoryId'] = this.categoryId;
            } else {
                params['newsCategoryId'] = this.categoryId;
                action = 'create';
            }
        }
        var isValid = true;
        this.blocks.items.each (function (block) {
            if (!block.isValid ()) {
                isValid = false;
                return isValid = false;
            }
            Ext.applyIf (params, block.toValues ());
        });
        if (true !== isValid) {
            return;
        }
        this.form.submit ({
            url:link ('news', 'main', action, {
                format:'json'
            }),
            method:'post',
            params:params,
            waitMsg:Ext.LoadMask.prototype.msg,
            success:function (form, action) {
                var responseObj = Ext.decode(action.response.responseText);
                Application.notificate (responseObj.messages);
                if (action.result.success) {
                    this.fireEvent ('completed', this, responseObj.id, action.response);
                    if (!this.newsId) {
                        this.newsId = responseObj.id;
                    }
                    var block = this.getFormPreviewBlock ();
                    if (null !== block) {
                        block.setDisabled (false);
                        block.setInvokeArgs (Ext.apply (block.invokeArgs || {}, {
                            newsId:this.newsId,
                            categoryId:this.categoryId
                        }));
                    }
                    return;
                }
            },
            scope:this
        });
    }
});