;(function($, window) {
    var NewsText = function() {
        this.init();
    }

    NewsText.prototype = {

        constructor: NewsText,

        newsId: null,

        init: function() {

            $('body')
            .off('.NewsText')
            .on('click.NewsText', '.editorCommandTrigger', function() {
                var $el = $(this), cmd = $el.data('command');
                if (undefined != window.tinyMCE && cmd) {
                    window.tinyMCE.execCommand(cmd);
                }
            });

            this.$form = $('#create-news-form');
            this.$form.submit(function() {
                var editor = window.tinyMCE.getInstanceById($('.tinymce', nt.$form)[0].id);
                editor.save();
                return nt.$form.valid();
            });

            this.tinymce = new AppaltiTinyMCE({
                onAjaxSave: this.setUpdate
            });
        },

        setUpdate: function(data) {
            if (data.success) {
                if (!$('input[name="newsId"]', nt.$form).length) {
                    nt.$form
                        .attr('action', nt.$form[0].action.replace('/create/', '/update/'))
                        .prepend('<input type="hidden" name="newsId" value="' + data.id + '" />');
                }
            } else {
                alert(data.messages[0].message);
            }
        }
    }

    window.NewsText = new NewsText();
    var nt = window.NewsText;

})(jQuery, window);