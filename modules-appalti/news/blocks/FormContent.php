<?php

final class News_Block_FormContent extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Content');
    }

    protected function _toHtml()
    {
        $categoryId = $this->_getParam('categoryId', false);
        $newsId = $this->_getParam('newsId', false);
        $service = new \News_Service_News();
        $this->view->categoryId = $categoryId;
        if (!empty($newsId)) {
            $this->view->newsId = $newsId;
            $this->view->newsRow = $service->find($newsId);
        } else {
            $newsRow = $service->findWithEmptyFlag($categoryId, false);
            if (!empty($newsRow)) {
                $this->view->newsId = $newsRow->getId();
                $this->view->newsRow = $newsRow;
            }
        }


    }
}
