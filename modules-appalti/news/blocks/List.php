<?php

final class News_Block_List extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('News List');
    }

    protected function _toHtml()
    {
        $categoryId = $this->_getParam('categoryId');
        $this->view->assign('categoryId', $categoryId);
    }
}
