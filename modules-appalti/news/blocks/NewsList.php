<?php

final class News_Block_NewsList extends \Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('News List');
    }

    protected function _toHtml()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(20);

        $service = new News_Service_News();
        $params = $this->getRequest()->getParams();
        $params['isArchive'] = 0;

        $params['filter'] = $this->getFilterParams();

        $response = $service->fetchAllWithResponse($params);
        $this->view->assign('rowset', $response->getRowset());

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;

        $this->blockId = $this->getId();

        $this->view->listOnly = $this->_getParam('listOnly');
    }
}
