<?php

final class News_Block_FormPreview extends Application_Block_Abstract
{
    protected function _toTitle()
    {
        return $this->view->translate('Preview');
    }

    protected function _toHtml()
    {

        $newsId = $this->_getParam('newsId', false);
        if (!empty($newsId)) {
            $this->view->newsId = $newsId;
            $service = new \News_Service_News();
            $data = $service->find($newsId);
            $this->view->newsRow =$data->toArray();

        }
    }
}
