<?php

class News_SubscriptionController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var News_Service_NewsSubscription
     */
    protected $_service;

    public function init()
    {
        $this->_service = new News_Service_NewsSubscription();

        parent::init();
    }

    public function getResourceId()
    {
        return 'news';
    }

    public function indexAction()
    {
        try {
            $serviceNewsCategories = new News_Service_NewsCategory();
            $response = $serviceNewsCategories->fetchAllWithResponse($this->_getAllParams());
            $response = $response->getRowset();

            if ($this->getRequest()->isPost()) {
                $this->_service->update($response, $this->_getAllParams());
                $this->_helper->information('Completed successfully', array(), E_USER_NOTICE);
            }

            $response = $this->_service->fetchAllWithSubscription($response);
            $this->view->rowSet = $response;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }

    }

}