<?php

class News_NewsFileStockController extends \Zend_Controller_Action implements \FileStock_Instance_EntityFileStockControllerInterface
{
    /**
     * @var News_Service_NewsFileStock
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'news';
    }

    public function init()
    {
        $this->_service = new \News_Service_NewsFileStock($this->getEntityType());

        $this->_helper->contextSwitch()
            ->addActionContext('fetch-all', 'json')
            ->addActionContext('fetch', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->addActionContext('delete', 'json')
            ->addActionContext('set-default-thumbnail', 'json')
            ->addActionContext('fetch-thumbnail', 'json')
            ->initContext();
    }

    public function getEntityType()
    {
        return 'news';
    }


    public function indexAction()
    {
        try {
            $newsId = $this->_getParam('newsId');
            if (!empty($newsId)) {

                $response = $this->_service->fetchAllFileStockByNewsIdWithResponse($this->_getParam('newsId'));
                $this->view->assign($response->toArray());
                $this->view->newsId =  $newsId;
            }
            $this->view->companyId = Zend_Auth::getInstance()->getIdentity()->getCompanyRow()->getId();
            $this->view->accountId = Zend_Auth::getInstance()->getIdentity()->getId();

            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function downloadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $fileStockRow = $this->_service->find($this->_getParam('newsId'), $this->_getParam('id'));
        $fileStockService = new FileStock_Service_FileStock();
        $fileStockService->download($fileStockRow, $this->getResponse(), true);
    }

    public function fetchAllAction()
    {
        try {
            $response = $this->_service->fetchAllFileStockByNewsIdWithResponse($this->_getParam('newsId'));
            $this->view->assign($response->toArray());
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchAction()
    {
        $fileStockRow = $this->_service->find($this->_getParam('newsId'), $this->_getParam('fileStockId'));
        $this->view->data = $fileStockRow->toArray();
        $this->view->success = true;
    }


    public function fetchThumbnailAction()
    {
        try {
            $fileStockRow = $this->_service->fetchFileStockThumbnailRowByNews($this->_getParam('newsId'), $this->_getParam('fileStockId'));

            $this->view->assign($fileStockRow);
            $this->view->success = true;

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function createAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');
        try {

            list($newsRow, $fileStockRow) = $this->_service->create($this->_getAllParams());
            $this->view->success = true;
            $this->view->entityId = $newsRow->getId();
            $this->view->fileStockId = $fileStockRow->getId();
            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function quickUploadAction()
    {
        $this->_helper->layout->disableLayout();
        try {
            list($newsRow, $fileStockRow) = $this->_service->create($this->_getAllParams());
            $this->view->success = true;
            $this->view->id = $newsRow->getId();
            $folder = 1000 + floor($fileStockRow->getId() / 1000) * 1000;
            $this->view->src = sprintf('/modules-appalti/news/data/news-file/%d/%d.jpg', $folder, $fileStockRow->getId());
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->view->error = $e->getMessages();
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHeader('Content-type', 'text/html');
        try {
            $this->_service->update($this->_getAllParams());
            $this->view->success = true;
            $this->_helper->information('File has been uploaded successfully', true, E_USER_NOTICE);
        } catch (OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->_service->delete($this->_getParam('newsId'), $this->_getParam('fileStockId'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }

    public function setDefaultThumbnailAction()
    {
        try {
            $this->_service->setDefaultThumbnail($this->_getParam('newsId'), $this->_getParam('fileStockId'));
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->view->success = false;
        }
    }

}