<?php

class News_CategoryController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    /**
     * @var News_Service_NewsCategory
     */

    protected $_service;

    public function init()
    {
        $this->_helper->ajaxContext()
            ->addActionContext('delete', 'json')
            ->addActionContext('edit', 'json')
            ->addActionContext('create', 'json')
            ->addActionContext('fetch-row', 'json')
            ->addActionContext('fetch-all', 'json')
            ->initContext();

        $this->_service = new News_Service_NewsCategory();
        parent::init();
    }

    public function getResourceId()
    {
        return 'news';
    }

    public function fetchAllAction()
    {
        try {
            $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
            $this->view->assign($response->toArray());
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function fetchRowAction()
    {
        try {
            $response = $this->_service->find($this->_getParam('categoryId'));
            $this->view->row = $response->toArray();
            $this->view->success = true;
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function deleteAction()
    {
        try {
            $this->view->success = $this->_service->delete($this->_getParam('id'));
            $this->_helper->information('Deleted successfully', true, E_USER_NOTICE);
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function editAction()
    {
        try {
            $this->_service->update($this->_getParam('categoryId'), $this->_getAllParams());
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }

    public function createAction()
    {
        try {
            $response = $this->_service->create($this->_getAllParams());
            $this->view->id = $response['id'];
            $this->view->success = true;
            $this->_helper->information('Created successfully', true, E_USER_NOTICE);
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
        }
    }
}