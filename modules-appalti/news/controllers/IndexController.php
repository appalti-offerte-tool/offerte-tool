<?php

class News_IndexController extends Zend_Controller_Action implements Zend_Acl_Resource_Interface
{
    const PREV_PAGE = 1;
    const NEXT_PAGE = 0;

    /**
     * @var News_Service_News
     */
    protected $_service;

    protected function _getRedirectUrl($direction)
    {
        $params = $this->_getAllParams();
        $response = $this->_service->fetchAllWithResponse($params);
        $rowset =$response->getRowset();
        $len = count($rowset) - 1;
        $newsId = null;

        for($i = 0; $i <= $len; $i++) {
            if ($params['newsId'] == $rowset[$i]['id']) {
                switch ($direction) {
                    case self::PREV_PAGE :
                        $newsId = $rowset[($i == $len ? 0 : $i + 1)]['id'];
                        break;

                    case self::NEXT_PAGE :
                        $newsId = $rowset[(!$i ? $len : $i - 1)]['id'];
                        break;
                }

                break;
            }
        }

        $newsId = !empty($newsId) ? $newsId : $params['newsId'];

        return '/news/index/preview/newsId/' . $newsId;
    }

    public function init()
    {
        $this->_service = new News_Service_News();
        $this->_helper->ContextSwitch()
            ->addActionContext('create', 'json')
            ->addActionContext('update', 'json')
            ->initContext();

        parent::init();
    }

    public function getResourceId()
    {
        return 'news';
    }

    public function indexAction()
    {
        $pagination = new \OSDN_Pagination();
        $pagination->setItemsCountPerPage(5);

        try {
            $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
            $this->view->rowset = $response->getRowset();
            $pagination->setTotalCount($response->count());
            $this->view->pagination = $pagination;
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->_redirect('/');
        }
    }

    public function previewAction()
    {
        try {
            $newsId = $this->_getParam('newsId');
            $response = $this->_service->find($newsId);
            $row = $response->toArray();
            $this->view->row = $row;
            $this->view->first = $this->_getParam('first');
            $this->view->last = $this->_getParam('last');
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
            $this->_redirect('/ap-default/index/news-and-events');
        }
    }

    public function formAction()
    {
        if ($this->_getParam('newsId')) {
            $newsRow = $this->_service->find($this->_getParam('newsId'));
        } else {
            $newsRow = $this->_service->findWithEmptyFlag();
        }

        $this->view->row = $newsRow;
    }

    public function deleteAction()
    {
        try {
            $this->_service->delete($this->_getParam('newsId'));
            $this->_helper->information('News item deleted successfully!', true, E_USER_NOTICE);
        } catch (\OSDN_Exception $e) {
            $this->_helper->information($e->getMessages());
        }
        $this->_redirect('/ap-default/index/news-and-events');
    }

    public function createAction()
    {
        try {
            $params = $this->_getAllParams();
            $params['newsCategoryId'] = null;
            $response = $this->_service->create($params);
            $this->view->id = $response['id'];
            $this->view->success = true;
            $this->_helper->information('News item created successfully', true, E_USER_NOTICE);

            if (!$this->_request->isXmlHttpRequest()) {
                $this->_redirect('/ap-default/index/news-and-events');
            }
        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
            if (!$this->_request->isXmlHttpRequest()) {
                $this->_redirect('/news/index/form');
            }
        }
    }

    public function updateAction()
    {
        try {
            $params = $this->_getAllParams();
            $params['newsCategoryId'] = null;
            $this->_service->update($this->_getParam('newsId'), $params);
            $this->view->success = true;
            $this->_helper->information('Updated successfully', true, E_USER_NOTICE);
            if (!$this->_request->isXmlHttpRequest()) {
                if ($params['preview'] == 0) {
                    $this->_redirect('/ap-default/index/news-and-events');
                } else {
                    $this->_redirect('/news/index/preview/newsId/' . $params['newsId']);
                }
            }

        } catch (\OSDN_Exception $e) {
            $this->view->success = false;
            $this->_helper->information($e->getMessages());
            if (!$this->_request->isXmlHttpRequest()) {
                $this->_redirect('/news/index/form/newsId/' . $params['newsId']);
            }
        }
    }

    public function nextAction()
    {
        $this->_redirect($this->_getRedirectUrl(self::NEXT_PAGE));
    }

    public function previousAction()
    {
        $this->_redirect($this->_getRedirectUrl(self::PREV_PAGE));
    }
}