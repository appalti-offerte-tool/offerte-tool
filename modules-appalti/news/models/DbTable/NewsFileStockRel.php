<?php
class News_Model_DbTable_NewsFileStockRel extends OSDN_Db_Table_Abstract
{

    protected $id;

    protected $_name = 'newsFileStockRel';

    protected $_nullableFields = array(
        'isThumbnail', 'parentId'
    );

    protected $_referenceMap = array(
        'FileStock'     => array(
            'columns'       => 'fileStockId',
            'refTableClass' => 'FileStock_Model_DbTable_FileStock',
            'refColumns'    => 'id'
        ),
        'Parent'        => array(
            'columns'       => 'parentId',
            'refTableClass' => __CLASS__,
            'refColumns'    => 'id'
        ),
        'ProposalBlock' => array(
            'columns'       => 'newsId',
            'refTableClass' => 'News_Model_DbTable_News',
            'refColumns'    => 'id'
        )
    );

    protected $_rowClass = '\News_Model_NewsFileStockRelRow';

}