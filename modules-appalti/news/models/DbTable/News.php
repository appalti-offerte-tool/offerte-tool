<?php


class News_Model_DbTable_News extends OSDN_Db_Table_Abstract
{

    protected $_primary = 'id';

    protected $_name = 'news';

    protected $_rowClass = 'News_Model_NewsRow';

    protected $_nullableFields = array(
        'modifiedDate', 'text', 'publish', 'startPublish', 'endPublish'
    );

    public function insert(array $data)
    {
        $account = Zend_Auth::getInstance()->getIdentity();
        $account = $account->getId();
        $data['accountId'] = $account;
        return parent::insert($data);
    }

    public function update(array $data, $where)
    {
        if (!empty($data['modifiedDate'])) {
            $dt = new DateTime();
            $data['modifiedDate'] = $dt->format('Y-m-d');
        }
        return parent::update($data, $where);
    }
}
