<?php

class News_Model_NewsFileStockRelRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{

    public function getId()
    {
        return $this->id;
    }

    public function getFileStockRow()
    {
        return $this->findParentRow('FileStock_Model_DbTable_FileStock', 'FileStock');
    }

    public function getChildrenRowset()
    {
        return $this->findDependentRowset('News_Model_DbTable_NewsFileStockRel', 'Parent');
    }


}