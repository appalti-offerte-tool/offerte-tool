<?php

class Layout_View_Helper_WizardSteps extends Zend_View_Helper_Abstract
{
    public function wizardSteps($steps = array())
    {
        $this->view->assign('steps', $steps);
        return $this->view->render('wizard-steps.phtml');
    }

    public function setView(Zend_View_Interface $view)
    {
        parent::setView($view);
        $this->view->addScriptPath(__DIR__ . '/scripts/appalti');
        return $this;
    }
}