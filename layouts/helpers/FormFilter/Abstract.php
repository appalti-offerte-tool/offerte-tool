<?php

abstract class Layout_View_Helper_FormFilter_Abstract
{
    /**
     * @var Zend_View
     */
    public $view;

    protected $_session;

    protected $_identity;

    protected $_iCount = 0;

    public function init($id = null)
    {
        $this->_iCount = 0;

        if (null === $id) {
            $id = uniqid();
        }

        $namespace = OSDN_Controller_Plugin_ViewFilter::toId($id);
        $this->_session = new Zend_Session_Namespace($namespace);

        $this->view->id = $id;

        return $this;
    }

    protected function _toHidden($name, $type)
    {
        $o = array();
        $this->_iCount++;
        $o[] = $this->view->formHidden($this->_prefix('[data][field]'), $name, array('id' => ""));
        $o[] = $this->view->formHidden($this->_prefix('[data][type]'), $type, array('id' => ""));
        $o[] = $this->view->formHidden($this->_prefix('[field]'), $name, array('id' => ""));
        $o[] = $this->view->formHidden(sprintf('%s[id]', OSDN_Controller_Plugin_ViewFilter::IDENTITY), $this->view->id, array('id' => ""));

        return $o;
    }

    protected function _prefix($suffix = "")
    {
        $prefix = sprintf(
            '%s[%d]',
            OSDN_Controller_Plugin_ViewFilter::IDENTITY,
            $this->_iCount
        );

        return $prefix . $suffix;
    }

    public function field($field, $name, $type = 'string', $attribs = null)
    {
        $o = $this->_toHidden($name, $type);

        $args = func_get_args();
        $args = array_splice($args, 3);

        array_unshift($args, $this->getValue());
        array_unshift($args, $this->_prefix('[data][value]'));

        if (!isset($attribs['id'])) {
            $attribs['id'] = 'form-filter-' . $name;
        }

        if (is_string($field)) {
            $method = array($this->view, $field);
            $o[] = call_user_func_array($method, $args);
        } elseif (is_array($field)) {
            list($module, $helper) = $field;
            $o[] = $this->view->modules($module)->sandbox(function($view) use ($helper, $args) {
                return call_user_func_array(array($view, $helper), $args);
            });

        } else {
            throw new InvalidArgumentException(sprintf('Argument "%s" is not allowed.'));
        }

        return join(PHP_EOL, $o);
    }

    public function getValue()
    {
        $value = '';
        if (isset($this->_session->filter[$this->_iCount]['data']['value'])) {
            $value = $this->_session->filter[$this->_iCount]['data']['value'];
        }

        return $value;
    }

    /**
     * Apply view object
     *
     * @param Zend_View_Interface $view
     * @return
     */
    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
        $this->view->addScriptPath(__DIR__ . '/../scripts/default');
    }
}