<?php

class Layout_View_Helper_Version
{
    protected $_version;

    public function __construct()
    {
        $this->_version = OSDN_Version::getInstance(APPLICATION_PATH . '/configs/version.xml')->getVersion();
    }

    public function version($path = null)
    {
        if (null === $path) {
            return $this->_version;
        }

        if (!is_string($path)) {
            throw new OSDN_Exception('Path must be a string');
        }

        $separator = false === strpos($path, '?') ? '?' : '&';
        return $path . $separator . $version;
    }
}