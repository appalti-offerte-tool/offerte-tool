<?php

class Layout_View_Helper_FormPrefix extends Zend_View_Helper_Abstract
{
    protected $_prefix;

    public function formPrefix()
    {
        if (func_num_args() > 0) {
            return call_user_func_array(array($this, 'toName'), func_get_args());
        }

        return $this;
    }

    public function init($prefix)
    {
        $this->_prefix = $prefix;
        return $this;
    }

    public function __invoke()
    {
        return call_user_func_array(array($this, 'formPrefix'), func_get_args());
    }

    public function toName($field, $prefix = '', $useUnderscoreInstead = false)
    {
        if (empty($this->_prefix)) {
            return $field;
        }

        if (empty($prefix)) {
            $prefix = $this->_prefix;
        }

        if (false === $useUnderscoreInstead) {
            return sprintf('%s[%s]', $prefix, $field);
        }

        $prefix = str_replace(array(']', '['), '_', $prefix);
        $prefix = trim($prefix, '_');
        return sprintf('%s_%s', $prefix, $field);
    }

    public function getPrefix()
    {
        return $this->_prefix;
    }
}