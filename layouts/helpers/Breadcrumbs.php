<?php

class Layout_View_Helper_Breadcrumbs extends Zend_View_Helper_Abstract
{
    public function breadcrumbs()
    {
        return $this->view->render('breadcrumbs.phtml');
    }

    /**
     * (non-PHPdoc)
     * @see Zend_View_Helper_Abstract::setView()
     */
    public function setView(Zend_View_Interface $view)
    {
        parent::setView($view);
        $this->view->addScriptPath(__DIR__ . '/scripts/default');
        return $this;
    }
}