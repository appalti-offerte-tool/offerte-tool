;(function($, win) {
    window.tinyMceStyleSwitch = function(cssPath) {
        var editor, interval = window.setInterval(function() {

            if (!tinyMCE) { return; }

            editor = editor || tinyMCE.activeEditor;
            if (editor.initialized) {
                tinyMCE.execCommand('mceRemoveControl',false, editor.id);
                initialiseTinyMCE(cssPath);
                tinyMCE.execCommand('mceAddControl',false, editor.id);
                clearInterval(interval);
            }
        }, 100);
    }

    function initialiseTinyMCE (cssPath) {

        var params = win.AppaltiTinyMCEParams;

        undefined !== cssPath && (params.content_css = cssPath);

        params.setup = function(ed) {
            ed.onPostProcess.add(function(ed, o) {
                var div = $('<div />').append(o.content), tables = div.find('table');

                tables.filter(':not(:has("thead"))').each(function() {
                    var tr = $('tr:first', this);
                    tr.find('td').replaceWith(function() {
                        return '<th>' + this.innerHTML + '</th>';
                    });

                    $(this).prepend($('<thead/>').append(tr));
                });

                o.content = div[0].innerHTML;
            });

            ed.onInit.add(function(ed, evt) {
                $(ed.getElement()).addClass('visuallyhidden').css('display', 'block');
                tinymce.dom.Event.add(ed.dom.doc, 'blur', function(e) { ed.save(); });

                if (params.fullScreenBtnBig) {
                    if (!$('#mce_fullscreen_tbl').length) {
                        var btn = $('a.mce_fullscreen');

                        btn
                        .html(btn.html() + $('.mceVoiceLabel', btn).text())
                        .parent('td').css('position', 'static')
                        .closest('div')
                        .css('position', 'relative')
                        .addClass('fullScreenBtnBig');
                    }
                }
            });
        }

        $('textarea.tinymce').tinymce(params);
    };

    var FileBrowser = function(params) {
        var me = this;
        $.extend(me, params || {});
        me.modalWindow = $('<div />', {
            'id': 'image-modal-container',
            'class': 'modal',
            'style': 'z-index: 10000000'
        }).appendTo('body');

        var params = {
            isBlockDefinition: me.isBlockDefinition,
            _dc: Math.floor(Math.random()*11)
        }
        params[me.itemName] = me.itemId;

        $.post(link(me.module, me.controller, 'index'), params).done(function(html) {
            me.modalWindow.html(html).modal('toggle');

            me.modalWindow.on('hidden', function() {
                me.modalWindow.remove();
            });

            me.onAfterLoad(me.modalWindow);
            me.bindEvents.call(me);
        });

    }

    FileBrowser.prototype = {

        modalWindow: null,

        url: '',

        itemId: '',

        fileStockId: '',

        onAfterLoad: $.noop(),

        deleteImageCallback: $.noop(),

        bindEvents: function() {
            var me = this;

            $('body')
            .off('.AppaltiFileBrowser')
            .on("click.AppaltiFileBrowser", '.thumbnails-list .delete-image-item',  function() {
                if(confirm('Weet je zeker dat?')) {
                    var $el = $(this);
                    $.when(me.deleteImageCallback.call(me, this)).done(function() {
                        $el.parent('li').remove();
                    });
                }

                return false;
            })
            .on("click.AppaltiFileBrowser", '.thumbnails-list .icon-add', function() {
                me.addImg.call(me, this);
                return false;
            })
            .on("dblclick.AppaltiFileBrowser", '.thumbnails-list .image-item', function() {
                me.addImg.call(me, this);
                return false;
            })
            .on("click.AppaltiFileBrowser", '.thumbnails-list .image-item',  function() {
                return false;
            })
        },

        addImg: function(el) {
            this.win.document.forms[0].elements['src'].value = el.href;
            this.win.ImageDialog.showPreviewImage(el.href);
            this.modalWindow.modal('toggle');
        },

        fetchThumbs: function(fileStockId, itemId) {
            var me = this,
                params = {
                    fileStockId: fileStockId,
                    format: 'json',
                    _dc: Math.floor(Math.random()*11)
                }

            params[me.itemName] = itemId;

            $.get(link(me.module, me.controller, 'fetch-thumbnail'), params).done(function(data) {
                $('.images-ul-container').append([
                    '<li>',
                    '<a id="' + data.id + '" class="padding-all image-item" href="' + data.parentLink + '">',
                    '<img class="tpl-thumb" src="' + data.link + '" title="' + data.name + '" alt="' + data.description + '" />',
                    '</a>',
                    '<a class="icon-add" href="' + data.parentLink + '" title="Select">&nbsp;</a>',
                    '<a class="icon-delete delete-image-item"',
                        'href="/' + me.module + '/' + me.controller + '/delete/fileStockId/' + data.id + '/format/json/"',
                        'title="Delete">&nbsp;</a>',
                    '</li>'
                ].join(''));
            });
        }
    }

    win.AppaltiTinyMCE = function(params) {
        $.extend(this, params || {});
        $('body').trigger("ready.AppaltiTinyMCE");
    }

    win.AppaltiTinyMCE.prototype = {

        constructor: AppaltiTinyMCE,

        fileBrowser: null,

        editor: null,

        onAjaxSave: $.noop,

        createFileBrowser: function(params) {
            this.fileBrowser = new FileBrowser(params);
        },

        getEditor: function() {
            if (!this.editor) {
                this.editor = tinyMCE.activeEditor;
            }

            return this.editor;
        },

        ajaxSave: function() {
            var me = this, ed = me.getEditor(),
                blur = ed, fullscreen = false,
                form = ed.formElement;

            if ($(form).valid()) {

                for (var e in tinyMCE.editors) {
                    if ('mce_fullscreen' === e) {
                        blur = tinyMCE.get(e);
                        fullscreen = true;
                        break;
                    }
                }

                return $.ajax({
                    url: form.action + (/\/format\//i.test(form.action) ? '' : '/format/json/'),
                    type: form.method,
                    data: $(form).serialize(),
                    dataType: 'json',
                    beforeSend: function() {
                        blur.setProgressState(1);
                    },
                    success: function(data) {
                        me.onAjaxSave(data);
                        blur.setProgressState(0);
                    }
                });
            } else if (fullscreen) {
                return $.Deferred(function(dfd) {
                    alert('Niet alle benodigde velden ingevuld. Af te sluiten volledig scherm bewerken om fouten te zien.');
                    dfd.resolve();
                }).promise();
            }
        }
    }

    $(function() {
        initialiseTinyMCE();
    });
})(jQuery, window);