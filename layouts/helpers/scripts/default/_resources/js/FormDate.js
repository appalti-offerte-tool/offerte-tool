$(function() {

    $('.dateDMY').change(function() {
        if (this.value.length) {
            var d = this.value.split('-');
            if (d.length == 3) {
                $('#' + $(this).data('alt'))
                .val([d[2], d[1], d[0]].join('-'));
            }
        }
    });

    function addRule() {
        $.validator.addMethod(
            "dateDMY",
            function(value, element) {
                if (value == "") {
                    return true;
                }
                var re = /^\d{1,2}\-\d{1,2}\-\d{4}$/,
                    check = re.test(value);

                if (check) {
                    var adata = value.split('-'),
                        gg = parseInt(adata[0],10),
                        mm = parseInt(adata[1],10),
                        aaaa = parseInt(adata[2],10),
                        xdata = new Date(aaaa,mm-1,gg);

                    check = (xdata.getFullYear() == aaaa) &&
                            (xdata.getMonth() == mm - 1) &&
                            (xdata.getDate() == gg);
                }

                return this.optional(element) || check;
            },
            $('input.dateDMY').data('invalidMsg') || "Please enter a correct date dd-mm-yyyy"
        );
    }

    if ('function' != typeof $.validator) {
        EFIS.Core.getScript(EFIS.Core.SCRIPT_TYPE_JQUERY_PLUGIN, 'validate', addRule);
    } else {
        addRule();
    }

});

