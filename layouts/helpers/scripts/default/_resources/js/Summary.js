Ext.define('OSDN.layout.Summary', {
    extend: 'Ext.panel.Panel',
    alias: 'osdn.layout.summary',

    layout: 'border',

    border: false,

    height: 150,

    statsInColumn: 3,

    id: 'north-summary-container',

    bodyStyle: 'background: none;',

    dh: Ext.DomHelper,

    initComponent: function() {

        this.items = [{
            title: lang('Summary'),
            contentEl: 'summary-block',
            region: 'center',
            autoScroll: true
        }];

        this.listeners = {
            expand: this.saveVisibilityState,
            collapse: this.saveVisibilityState,
            render: function() {
                Ext.onReady(this.initSummary, this)
            },
            updatesummary: this.updateSummary,
            scope:  this
        };

        this.callParent(arguments);
    },

    initSummary: function() {
        this.summariesTbl = Ext.get(Ext.fly('summary-block').query('table.summary')[0]);
        this.collectSummaryGroups();
        this.bindEvents();
    },

    collectSummaryGroups: function() {
        this.availableStatisticListsCnt = Ext.get(this.dh.createDom({
            tag: 'div',
            cls: 'summary-statistics-lists'
        }));

        var availableStatLists = [];
        var colHeaders = this.summariesTbl.query('table.col-head');
        var cols = this.summariesTbl.query('td.col');
        this.groups = {};

        for (var groupId, i = 0; i < colHeaders.length; i++) {
            groupId = colHeaders[i].id;
            availableStatLists[i] = Ext.fly(colHeaders[i]).next('div.stats-available');
            availableStatLists[i].appendChild(this.dh.createDom({
                tag: 'input',
                type: 'hidden',
                name: groupId + '-summary-head-id',
                value: groupId
            }));

            this.groups[groupId] = {
                header: Ext.get(colHeaders[i]),
                trigger: Ext.get(Ext.fly(colHeaders[i]).query('td.stat-selector-trigger')),
                availableStatistics: availableStatLists[i],
                statisticsCol: Ext.get(cols[i])
            }
        }

        this.availableStatisticListsCnt.appendChild(availableStatLists);
        Ext.getBody().appendChild(this.availableStatisticListsCnt);
    },

    bindEvents: function() {
        this.summariesTbl.on({
            'click': {
                fn: function(e, t) {
                    this.toggleStatisticsList(t);
                },
                scope: this,
                delegate: '.stat-selector-trigger'
            }
        });

        this.availableStatisticListsCnt.on({
            click: function(e, t) {
                t.checked ? this.addStatistic(t) : this.removeStatistic(t);
            },
            scope: this,
            delegate: 'input[type="checkbox"]'
        });

        this.availableStatisticListsCnt.on({
            click: function(e, t) {
                e.stopEvent();
                this.toggleStatisticsList(t);
            },
            scope: this,
            delegate: 'a.done'
        });

//        this.availableStatisticListsCnt.on({
//            click: function(e, t) {
//                e.stopEvent();
//                this.toggleStatisticsList(t);
//            },
//            scope: this,
//            delegate: 'a.select-all'
//        });
    },

    updateSummary: function() {
        this.currentGroup ? this.updateGroup() : this.updateAll();
    },

    updateGroup: function() {

    },

    updateAll: function() {

    },

    showPreloader: function() {
        if (undefined == this.groups[this.currentGroup].preloader) {
            var preloader = this.dh.createDom({
                tag: 'span',
                cls: 'summary-preloader invisible'
            });

            var colHeader = this.groups[this.currentGroup].header;

            var link = colHeader.query('.col-title > a')[0];

            var preloaderParent = link ? Ext.fly(link).first() : Ext.get(colHeader.query('.col-title')[0]);

            preloaderParent.appendChild(preloader);

            this.groups[this.currentGroup].preloader = Ext.get(preloader);
        }

        this.groups[this.currentGroup].preloader.removeClass('invisible');
    },

    hidePreloader: function() {
        this.groups[this.currentGroup].preloader.addClass('invisible');
    },

    saveVisibilityState: function() {
        Ext.state.Manager.getProvider().set('north-summary-collapsed', this.isVisible());
    },

    setSummaryStatus: function(summaryName, enabled) {
        document.cookie = summaryName + '=' + (+enabled);
        return this;
    },

    addColumn: function(container) {
        var wrap = false;
        var subColTpl = {tag: 'td', cls: 'sub-col'};
        var colsTbl = container.query('table.cols')[0];

        if (wrap = undefined == colsTbl) {
            colsTbl =  this.dh.createDom({
                tag: 'table',
                cls: 'cols',
                children: [{tag: 'tbody', children: [{tag: 'tr'}]}]
            });
        }

        var row = Ext.get(Ext.query('tr', colsTbl));

        if (wrap) {
            var elsToWrap = Ext.get(container.query('a'));
            var td = Ext.get(this.dh.createDom(subColTpl));
            td.appendChild(elsToWrap)
            td.appendTo(row);
            container.update('').appendChild(colsTbl);
        }

        var lastCol = Ext.get(this.dh.createDom(subColTpl)).appendTo(row);

        Ext.get(colsTbl).clearOpacity();

        return lastCol;
    },

    getTargetColumn: function(container) {
        var columnsTbl = container.query('table.cols')[0];
        var rootEl = columnsTbl ? Ext.query('td:last', columnsTbl)[0] : container.dom;

        return Ext.query('a', rootEl).length < this.statsInColumn ? rootEl : this.addColumn(container);
    },

    changeDOM: function(checkbox, params) {
        var group = this.groups[this.currentGroup];
        var a = this.dh.createDom({
            tag: 'a',
            id: this.currentGroup + '-' + params.module + '-summary-' + params.instance,
            cls: 'stat invisible',
            children: [
                {tag: 'span', html: params.title},
                {tag: 'span', html: params.value}
            ]
        });

        this.getTargetColumn(group.statisticsCol).appendChild(a);

        this.toggleStatistic(a, checkbox.checked);

        Application.notificate(params.title + ' ' + lang('statistic activated'));
    },

    toggleStatistic: function(statEl, show) {
        if (statEl) {

            statEl = statEl instanceof Ext.Element ? statEl : Ext.get(statEl);

            statEl.toggleClass('invisible');

            var parentTd = statEl.parent('td.sub-col');
            if (parentTd) {
                var stats = parentTd.query('a');
                var showTd = false;

                for (var i = 0; i < stats.length; i++) {
                    showTd = showTd || Ext.get(stats[i]).isVisible();
                }

                showTd ? parentTd.removeClass('invisible') : parentTd.addClass('invisible');
            }

            this.setSummaryStatus(statEl.dom.id, show);
        }
    },

    addStatistic: function(el) {
        var a = Ext.get(el.value);
        if (!a) {
            this.currentGroup = Ext.fly(el).parent('.stats-available').query('input[type="hidden"]')[0].value;
            this.showPreloader();
            Ext.Ajax.request({
                url: link('module', 'summary', 'get', {'format': 'json'}),
                params: Ext.util.JSON.decode(Ext.get(el).getAttribute('data-summary')),
                success: function(response) {
                    var resp = Ext.util.JSON.decode(response.responseText);
                    if (resp.success) {
                        this.changeDOM(el, resp);
                    } else {
                        Application.notificate(resp.error, 'Error', Application.MSG_TYPE_ERROR);
                    }
                    this.hidePreloader();
                },
                failure: function() {
                    this.hidePreloader();
                    Application.notificate('Some error occured', 'Error', Application.MSG_TYPE_ERROR);
                },
                scope: this
            });
        } else {
            this.toggleStatistic(a, el.checked);
        }
    },

    setListPosition: function(statGroup) {
        var th = statGroup.header;
        statGroup.availableStatistics.alignTo(th, 'tl', [+th.getPadding('l'), +th.getHeight()]);
    },

    updateListDimensions: function(statGroup) {
        var th = statGroup.header;
        statGroup.availableStatistics.setStyle({
            width: th.getWidth() - th.getPadding('lr') - statGroup.availableStatistics.getPadding('lr') - 2 /* border */ + 'px'
        });
    },

    removeStatistic: function(el) {
        this.toggleStatistic(Ext.get(el.value), el.checked);
    },

    toggleStatisticsList: function(triggerEl) {
        var el = Ext.get(triggerEl);
        var groupId = el.is('a') ? el.getAttribute('data-group') :
                                   el.parent('table.col-head').dom.id;

        var list = this.groups[groupId].availableStatistics;

        if (list.isVisible()) {
            list.slideOut();
        } else {
            var group = this.groups[groupId];
            this.updateListDimensions(group);
            this.setListPosition(group);
            list.slideIn();
        }
    }
});