$(function() {
	var menu = $('ul.actions-menu');
    if (menu.length) {
        $('>li', menu).hover(function() {
            $(this).toggleClass('active');
        });

        var width;
        $('.sub', menu).each(function() {
            width = 0;
            var sub = $(this);
            $('ul', sub).each(function() {
                width += $(this).outerWidth();
            });
            
            if (width && width > 200) {
                $('>div', sub).width(
                    $.isIE7 ? width + 10 : width
                );
            }

            if (sub.parents('ul').hasClass('sub-right')) {
                sub.css(
                    'marginLeft',
                    (sub.width() - $(this).parent('li').width()) * -1
                );
            }
        });
    }
});