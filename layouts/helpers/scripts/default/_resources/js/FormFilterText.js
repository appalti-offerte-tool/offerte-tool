EFIS.FormFilter = function(searchForm) {
    this.els = {};
    this.init(searchForm);
};

EFIS.FormFilter.prototype = {
    els: null,
    
    widget: null,

    dataPropName: 'formfilter',

    init: function(searchForm) {
        this.widget = searchForm instanceof jQuery ? searchForm : $(searchForm);
        this.initMarkup();
    },

    initMarkup: function() {
        this.widget.data(this.dataPropName, {
            widgetElement: this.widget,
            formElement: $('>form', this.widget),
            extra: this.widget.find('.extra-filter-fields'),
            mainFields: $(':input:not(:button)', this.widget.find('table.main-filter-fields')),
            extraFields: $(':input:not(:button)', this.widget.find('div.extra-filter-fields')),
            buttons: {
                extra: $(':button[name="extra"]', this.widget),
                submit: $(':button[name="search"]', this.widget),
                reset: $(':button[name="reset"]', this.widget),
                closeExtra: $(':button[name="close-extra"]', this.widget)
            }
        });

        this.addEventHandlers().initEvents();
    },

    addEventHandlers: function() {
        var widgetData = this.widget.data(this.dataPropName);

        widgetData.events =  {
            onExtraShow: function() {
                widgetData.mainFields.each(function() {
                    widgetData.extraFields
                    .filter(this.tagName + '[name="' + this.name + '"]')
                    .val(this.value);
                });
            }
        };

        return this;
    },

    initEvents: function() {
        var widgetData = this.widget.data(this.dataPropName);

        widgetData.resetFields = function() {
            widgetData.mainFields.add(widgetData.extraFields)
            .filter('input:text').val('')
            .end()
            .filter('input:checked').removeAttr('checked')
            .end()
            .find('input:selected, option:selected')
            .removeAttr('selected');

            widgetData.formElement.submit();
        }

        widgetData.buttons.extra.click(function() {
            var show = widgetData.extra.is(':visible');
            
            if (show) {
                widgetData.events.onExtraShow();
            }

            widgetData.extra.toggle(0, function() {
                widgetData.buttons.submit.eq(0).parent()
                .add(widgetData.buttons.reset.eq(0).parent())
                .toggle(show);
            });
        });

        widgetData.buttons.closeExtra.click(function() {
            widgetData.buttons.extra.triggerHandler('click');
        });

        widgetData.buttons.submit.click(function() {
            
        });

        widgetData.buttons.reset.click(function() {
            widgetData.resetFields();
        });
    }
};

$(function() {
    $('.search-panel').each(function() {
        new EFIS.FormFilter(this);
    });
});

