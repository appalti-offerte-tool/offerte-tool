<?php

class Layout_View_Helper_FormatDate extends Zend_View_Helper_Abstract
{
    protected $_date = '';
    protected $_format = 'd-m-Y';

    public function formatDate($date, array $params = array())
    {
        if (is_string($date) && !empty($date)) {

            $validator = new Zend_Validate_Date(array('format' => 'yyyy-mm-dd'));
            if ($validator->isValid($date)) {
                if (!empty($params['locale'])) {
                    $locale = new Zend_Locale($params['locale']);
                    Zend_Date::setOptions(array('format_type' => 'php'));
                    $date = new Zend_Date($date, false, $locale);
                    $this->_date = $date->toString('j F Y');
                } else {
                    $date = new DateTime($date);
                    $this->_date = $date->format(!empty($params['format']) ? $params['format'] : $this->_format);
                }

            } else {
                throw new Exception("Value is not valid.");
            }
        }

        return $this;
    }

    public function __toString()
    {

        return $this->_date;
    }
}