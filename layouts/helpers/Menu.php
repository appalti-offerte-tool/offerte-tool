<?php

/**
 * Retrieve main menu content
 *
 * @category OSDN
 * @package OSDN_View_Helper
 *
 */
class OSDN_View_Helper_Menu extends \Zend_View_Helper_Abstract
{
    /**
     * Contain the collection of menu items
     *
     * @var array
     */
    protected $_menu = array();

    /**
     * Retrieve main menu
     */
    public function Menu()
    {
        /**
         * @FIXME Make independent module
         */
        $bootstrap = \Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $im = $bootstrap->getResource('ModulesManager');

        $aMenu = array();
        foreach($im->fetchAll() as $information) {
            $fMenu = $information->getRoot() . '/configs/menu.xml';
            if (!file_exists($fMenu)) {
                continue;
            }

            $xMenu = simplexml_load_file($fMenu);
            foreach($xMenu->xpath('administration/item') as $xMenuItem) {
                $this->_toMenu($xMenuItem);
            }
        }

        return $this;
    }

    public function toArrayFormattedAccordingExtjsToolbar($inJson = false)
    {
        $toComparisionFn = function($a, $b) {
            if ($a['position'] == $b['position']) {
                return 0;
            }
            return ($a['position'] < $b['position']) ? -1 : 1;
        };

        $toResetPosition = function(array & $mi) {
            foreach($mi as $k => $o) {
                unset($mi[$k]['position']);
            }

            return $mi;
        };

        $toMenu = function(array $menu) use (& $toMenu, $toComparisionFn, $toResetPosition) {

            $o = array();
            if (isset($menu['_attributes'])) {
                $o = $menu['_attributes'];
                unset($menu['_attributes']);
            } else {
                $o['text'] = 'Untitled';
                $o['position'] = 100;
            }

            foreach($menu as $k => $value) {
                if (false !== ($mi = $toMenu($value))) {
                    $o['children'][] = $mi;
                }
            }

            if (empty($o['children']) && empty($o['handler']) && empty($o['href'])) {
                return false;
            }

            if (!empty($o['children'])) {
                usort($o['children'], $toComparisionFn);
                $toResetPosition($o['children']);
            }
            return $o;
        };

        $result = array();
        foreach($this->_menu as $m) {
            if (false !== ($mResult = $toMenu($m))) {
                $result[] = $mResult;
            }
        }

        usort($result, $toComparisionFn);
        $toResetPosition($result);

        if (true === $inJson) {
            return Zend_Json::encode($result);
        }

        return $result;
    }

    protected function _toMenu(SimpleXMLElement $xItem, array $aParent = null)
    {
        if (empty($xItem['acl'])) {
            return;
        }
        $aclResource = trim($xItem['acl']);

        $aclResourceBreaks = explode(':', $aclResource);

        $resource = null;
        $privilege = null;

        if (3 == count($aclResourceBreaks)) {
            $privilege = array_pop($aclResourceBreaks);
            $resource = join(':', $aclResourceBreaks);
        } else {
            $resource = $aclResource;
        }

        /**
         * @FIXME
         *
         * Add some common functionality for ACL checking
         */
        $account = Zend_Auth::getInstance()->getIdentity();
        $acl = $account->getAcl();

        if (
            ! $acl->has($resource) ||
            ! $acl->isAllowed($account->getRoleId(), $resource, $privilege ?: 'default')
        ) {
            return;
        }

        $m = & $this->_menu;

        $path = array_filter(explode('/', null === $aParent ? trim($xItem['path']) : $aParent['_attributes']['path']));

        foreach($path as $pName) {
            if (!isset($m[$pName])) {
                $m[$pName] = array();
            }

            $m = & $m[$pName];
        }

        $name = trim($xItem['name']);
        $m[$name]['_attributes']['name'] = $name;

        $m = & $m[$name];
        $a = & $m['_attributes'];
        $a['position'] = (int) $xItem['position'];

        foreach(array(
            'text', 'href', 'target',
            'iconCls', 'qtip', 'handler', 'position'
        ) as $o) {
            if (isset($xItem->$o)) {
                $a[$o] = trim($xItem->$o);
            }
        }

        $a['path'] = '/' . join('/', $path);
        if (!empty($path)) {
            $a['path'] .= '/';
        }

        $a['path'] .= $a['name'];

        if (! isset($xItem->children)) {
            return;
        }

        foreach($xItem->children->xpath('item') as $xItemChild) {
            $this->_toMenu($xItemChild, $m);
        }
    }
}