<?php

require_once __DIR__ . '/FormFilter/Abstract.php';

class Layout_View_Helper_FormFilter extends Layout_View_Helper_FormFilter_Abstract
{
    protected $_isInitialized = false;

    protected $_decorator;

    public function FormFilter($id = null)
    {
        $this->init($id);

        return $this;
    }

    public function init($id = null)
    {
        if (false === $this->_isInitialized) {
            $this->_isInitialized = true;
            parent::init($id);
            ob_start();
        }

        return $this;
    }

    public function simple($name, $type = 'search', $attribs = null, $additionalCls = null)
    {
        if (empty($attribs['id'])) {
            $attribs['id'] = 'field-' . $this->view->id;
        }

        $f = $this->field('formText', $name, $type, $attribs);

        $this->view->content = $f;
        $this->view->value = $this->getValue();

        if (null !== $additionalCls) {
            $this->view->additionalCls = $additionalCls;
        }

        echo $this->view->render('form-filter-decorator-wrap.phtml');
        return $this;
    }

    public function helper($field, array $args = array(), $type = 'string')
    {
        if ( ! (is_string($field) || is_array($field))) {
            throw new OSDN_Exception(sprintf('Wrong helper type: %s', gettype($field)));
        }

//        elseif (!is_callable($field)) {
//            throw new OSDN_Exception('Unable to execute method. Must be callable.');
//        }

        list($name,) = $args;

        $f = $this->field($field, $name, $type);
        return $f;
    }

    public function __call($method, array $args)
    {
        array_unshift($args, $method);
        return call_user_func_array(array($this, 'field'), $args);
    }

    public function __toString()
    {
        $this->_isInitialized = false;
        $this->view->content = ob_get_clean();

        try {
            return $this->view->render('form-filter.phtml');
        } catch (Exception $e) {
            return sprintf('%s::%s - %s', __CLASS__, __METHOD__, $e->getMessage());
        }
    }
}