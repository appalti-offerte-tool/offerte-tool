<?php
class Layout_View_Helper_PageDescription extends Zend_View_Helper_Abstract
{
    /**
     * @var Zend_View
     */
    public $view;

    protected $_messages = array(
        'defaultText' => 'Deze pagina is nog niet beschreven.',
        'defaultTitle' => 'Over dit scherm',

        'company:index:index' => array(
            'text' => 'This page has not been described yet.'
        ),
        'company:create:company' => array(
            'text' => 'Vul hier de gegevens van uw bedrijf in.<br/>U kunt deze gegevens later aanpassen of aanvullen.'
        ),
        'company:create:contactperson' => array(
            'text' => 'Vul hier uw persoonsgegevens in.<br/>
                U kunt deze gegevens later aanpassen of aanvullen.<br/>
                Ook kunt u later nog andere gebruikers toevoegen.'
        ),
        'proposal:template:typography' => array(
            'text' => 'Kies hier het lettertype en de kleuren die u wilt gebruiken in offertes.<br/>
                Dit kunt u later nog aanpassen.'
        ),

        'company:create:summary' => array(
            'text' => 'Over dit scherm: Stap 5: Afronding'
        ),
        'company:client:summary' => array(
            'text' => 'Hier is alle detail informatie over klanten terug te vinden. Door te kiezen voor «gegevens wijzigen»,
                       is het simpel om klant informatie aan te passen. Kijk voor meer informatie en korte
                       instructiefilmpjes ook bij de <a href="/faq/index">FAQ’s</a>.'
        ),
        'proposal:index:view' => array(
            'text' => 'Hier is alle detail informatie over de gemaakte offerte terug te vinden. Door te kiezen voor «offerte wijzigen»,
                       is het simpel om de offerte inhoudelijk aan te passen. Kijk voor meer informatie en korte
                       instructiefilmpjes ook bij de <a href="/faq/index">FAQ’s</a>.'
        ),
        'proposal:create:index' => array(
            'text' => 'Vul hier de basisgegevens in voor de offerte. Deze gegevens komen terug in de offerte stukken en
                       in de management informatie.<br/>Klik onderaan op de button «volgende&nbsp;stap» om door te gaan in het
                       proces.'
        ),
        'proposal:create:relation' => array(
            'text' => 'Vul hier de gegevens in van de relatie voor wie de offerte gemaakt wordt of selecteer deze relatie.<br/>Klik onderaan op de button «volgende&nbsp;stap» om door te gaan in het
                       proces.'
        ),
        'proposal:create:blocks' => array(
            'text' => 'Stel in dit scherm de offerte (opbouw) samen. Selecteer in de bibliotheek de stukken die in de
                       offerte komen. Kies voor het paarse driehoekje om de geselecteerde stukken te verplaatsen naar de
                       «inhoud van deze offerte». Pas de volgorde eventueel aan door te slepen of door gebruik te maken
                       van de pijltjes. Voor meer opties klik op het tandwiel icoon.<br/>Klik onderaan op de button
                       «volgende&nbsp;stap» om door te gaan in het proces.'
        ),
        'proposal:create:personal-introduction' => array(
            'text' => 'Schrijf hier een inleiding die de offerte persoonlijk maakt. De inleiding wordt standaard als
                       hoofdstuk 1 in de offerte opgenomen.<br/>Klik onderaan op de button
                       «volgende&nbsp;stap» om door te gaan in het proces.'
        ),
        'proposal:create:summary' => array(
            'text' => 'De offerte is afgerond. Hulp van Appalti nodig? Kies ervoor om de offerte door Appalti te
                       laten nalezen. De offerte wordt automatisch opgeslagen onder «mijn offertes».'
        ),
        'proposal:create:price' => array(
            'text' => 'Kies in dit scherm welke diensten en producten in de offerte komen. Stel daarnaast simpel in
                       welke voorwaarden aan deze offertes verbonden zijn door de verschillende opties aan te vinken.<br/>Klik onderaan op de button
                       «volgende&nbsp;stap» om door te gaan in het proces.'
        ),

        'faq:index:index' => array(
            'text' => 'Hier is meer informatie terug te vinden over de offertetool. Type van toepassing zijnde zoekwoorden
                       in en automatisch komt er gerelateerde informatie tevoorschijn. Staat het onderwerp er niet tussen?
                       Neem gerust contact met ons op!'
        ),
        'proposal:template:company-defaults' => array(
            'text' => 'In mijn beheer kunnen (standaard) instellingen ingeven worden, zodat de offertes exact aan de wensen
                       voldoen. De  instellingen bestaan uit: (standaard) offerte gegevens, templates en kleuren.
                       Verandert er in de toekomst iets, dan wordt dit doorgevoerd voor enkel nieuwe offertes.'
        ),
    );

    protected $_viewParams = array(
        'showDesc' => true,
        'helpText' => 'Een vraag, suggestie of ondersteuning nodig?'
    );

    protected function _getDescription(array $params)
    {
        $this->_viewParams['showDesc'] = isset($params['showDesc']) ? $params['showDesc'] : true;

        if (empty($params['showDesc'])) {

            $r = Zend_Controller_Front::getInstance()->getRequest();
            $key = sprintf('%s:%s:%s', $r->getModuleName(), $r->getControllerName(), $r->getActionName());

            if (array_key_exists($key, $this->_messages) && !empty($this->_messages[$key])) {
                $this->_viewParams['title'] = !empty($this->_messages[$key]['title']) ? $this->_messages[$key]['title'] : $this->_messages['defaultTitle'];
                $this->_viewParams['desc'] = $this->_messages[$key]['text'];
            } else {
                $this->_viewParams['title'] = $this->_messages['defaultTitle'];
                $this->_viewParams['desc'] = $this->_messages['defaultText'];

            }
        }
    }

    public function pageDescription(array $params = array())
    {
        $this->_getDescription($params);

        $this->_viewParams['showHelp'] = (!empty($params) && array_key_exists('showHelp', $params)) ? $params['showHelp'] : true;
        $this->_viewParams['helpText'] = (!empty($params) && array_key_exists('helpText', $params)) ? $params['helpText'] : $this->_viewParams['helpText'];

        $this->view->assign($this->_viewParams);
        return $this->view->render('page-description.phtml');
    }

    /**
     * (non-PHPdoc)
     * @see Zend_View_Helper_Abstract::setView()
     */
    public function setView(Zend_View_Interface $view)
    {
        parent::setView($view);
        $this->view->addScriptPath(__DIR__ . '/scripts/appalti');
        return $this;
    }
}