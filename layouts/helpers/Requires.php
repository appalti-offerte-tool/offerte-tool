<?php

class Layout_View_Helper_Requires
{
    /**
     * @var Zend_View
     */
    public $view;

    protected $_baseUrl;

    /**
    * Helper for setting and retrieving stylesheets
    *
    * @var Zend_View_Helper_HeadStyle
    */
    protected $_headStyle;

    /**
    * Helper for setting and retrieving script elements for HTML head section
    *
     * @var Zend_View_Helper_HeadScript
     */
    protected $_headScript;

    /**
     * Zend_Layout_View_Helper_HeadLink
     *
     * @var Zend_View_Helper_HeadLink
     */
    protected $_headLink;

    public function requires($inc = null, array $packages = array(), array $ignores = array())
    {
        if (null === $inc) {
            return $this;
        }

        $assembler = new OSDN_View_Bind($inc, $packages, $ignores);
        $isAllowedMerge = 0 === strcasecmp('production', APPLICATION_ENV);
        $stylesheets = array();

        $includes = $assembler->getIncludes();
        $index = 10000;

        foreach($includes as $include) {

            $include->setRootDirectory(APPLICATION_PATH);
            $include->setBaseUrl($this->_baseUrl);

            switch($include->getType()) {
                case OSDN_View_Bind_Js::TYPE:
                case OSDN_View_Bind_Remote::TYPE:
                    if ($isAllowedMerge && $include->isCacheable()) {
                        continue;
                    }

                    $content = $this->_version($include->getHref());
                    $this->_headScript->appendFile($content);
                    break;

                case OSDN_View_Bind_Remote::TYPE:

                    if ($isAllowedMerge && $include->isCacheable()) {
                        continue;
                    }

                    $content = $include->toString();
                    $this->_headScript->appendScript($content);
                    break;

                case OSDN_View_Bind_Css::TYPE:

                    if ($include->isConditional()) {
                        $this->_headLink->offsetSetStylesheet($index, $include->getHref(), null, $include->getCondition());
                        $index++;
                        continue;
                    }

                    if ($isAllowedMerge) {
                        continue;
                    }

                    $stylesheets[] = $this->_version($include->getHref());
                    break;

                default:
                    throw new Exception('Unknown type "' . $include->getType() . '"');
            }
        }

        if ($isAllowedMerge) {

            $v = OSDN_Version::getInstance()->getVersion();

            $id = "";
            if (!empty($v)) {
                $id = '?v=' . $v;
            }

            $layout = $this->view->layout()->getLayout();

            $this->_headLink->appendStylesheet($this->_baseUrl . '/data/public-bind-cache/bind-' . $layout . '.css' . $id);
            $this->_headScript->appendFile($this->_baseUrl . '/data/public-bind-cache/bind-' . $layout . '.js' . $id);
        }

        /**
         * The maximum includes of external link is 31 in IE
         * and we desided simply split per 31 script and include it
         * via @import derective
         *
         * but in Zend library I cannot find the import directive for css script
         * so, I writed very simple splitter
         *
         * @var function
         */
        while($css = array_splice($stylesheets, 0, 31)) {
            $css = array_map(function($a) {
                return '@import "' . $a . '";';
            }, $css);
            $this->_headStyle->appendStyle(join(PHP_EOL, $css));
        }

        return $this;
    }

    public function package($module)
    {
        $m = explode('/', $module);
        $module = array_shift($m);

        $f = sprintf('%s/%s.xml', $this->view->modules($module)->getRoot() . '/views', $module);

        return $this->requires($f, $m);
    }

    /**
     * Append application version to filename
     *
     * @param string $file        The input filename
     * @return string            The parsed filename
     */
    protected function _version($file)
    {
        $version = OSDN_Version::getInstance(APPLICATION_PATH . '/configs/version.xml')->getVersion();
        if (!empty($version)) {
            $file = preg_replace('#\.(js|css)$#', '-v' . $version . '.$1', $file);
        }

        return $file;
    }

    /**
     * Apply view object
     *
     * @param Zend_View_Interface $view
     * @return
     */
    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;

        $this->_baseUrl = $view->baseUrl();
        $this->_headStyle = $view->headStyle();
        $this->_headLink = $view->headLink();
        $this->_headScript = $view->headScript();
    }

    public function __toString()
    {
        return $this->_headStyle . $this->_headLink . $this->_headScript;
    }
}