<?php

/**
 * @todo fix it
 * Enter description here ...
 * @author yaroslav
 *
 */
class Person_View_Helper_FormDate extends Zend_View_Helper_Abstract
{
    protected $_format = 'd-m-Y';

    protected $_hiddenFormat = 'Y-m-d';

    public function formDate($fieldName, $value = null, array $params = array(), array $attribs = array())
    {
        $displayValue = null;
        $hiddenValue = '';

        if (is_string($value) && !empty($value)) {

            $validator = new Zend_Validate_Date(array('format' => 'yyyy-mm-dd'));
            if ($validator->isValid($value)) {
                $hiddenValue = $value;
                $displayValue = date($this->_format, strtotime($value));
            } else {
                throw new Exception("The value of the field is not allowed.");
            }
        }

        if (is_object($value)) {
            if ($value instanceof DateTime) {
                $displayValue = $value->format($this->_format);
                $hiddenValue = $value->format($this->_hiddenFormat);
            } else {
                throw new Exception("The value of the field is not allowed.");
            }
        }

        $params = array(
            'dateFormat'        => 'dd-mm-yy',
            'showOn'            => 'button',

            /**
             * @FIXME
             */
            'buttonImage'       => '/layouts/scripts/default/_resources/images/icons-16/calendar.png',
            'buttonImageOnly'   => true,
            'showButtonPanel'   => true,
            'selectOtherMonths' => true,
            'changeMonth'       => true,
            'changeYear'        => true
        );

        $datePickerId = uniqid('datepicker-');
        $fieldId = uniqid($fieldName);

        $attribs['class'] = (isset($attribs['class']) ? $attribs['class'] : '') . ' dateDMY';
        $attribs['data-alt'] = $fieldId;
        $attribs['data-invalidMsg'] = $this->view->translate('Incorrect date value. Correct date format is "02-10-2010"');

        $this->view->headScript()->appendFile('/layouts/helpers/scripts/default/_resources/js/FormDate.js');

        return $this->view->datePicker($datePickerId, $displayValue, $params, $attribs)
               .$this->view->formHidden($fieldName, $hiddenValue, array('id' => $fieldId));
    }
}