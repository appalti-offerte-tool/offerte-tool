<?php

require_once __DIR__ . '/FormFilter/Abstract.php';

class Layout_View_Helper_FormFilterSearch extends Layout_View_Helper_FormFilter_Abstract
{
    public function formFilterSearch($name, $type = 'search', $attribs = null, $additionalCls = null)
    {
        $id = null;
        if (isset($attribs['id'])) {
            $id = $attribs['id'];
        }

        foreach(array('btnText', 'btnCls') as $o) {
            if (!empty($attribs[$o])) {
                $this->view->$o = $attribs[$o];
            }
        }

        if (null === $id) {
            $p = Zend_Controller_Front::getInstance()->getRequest()->getParam(OSDN_Controller_Plugin_ViewFilter::IDENTITY);
            if (!empty($p) && \is_array($p) && !empty($p['id'])) {
                $id = $p['id'];
            } else {
                $request = Zend_Controller_Front::getInstance()->getRequest();
                if (!Zend_Controller_Front::getInstance()->getRequest()->isXmlHttpRequest()) {
                    $id = join('-', array(
                        $request->getModuleName(),
                        $request->getControllerName(),
                        $request->getActionName()
                    ));
                } else {
                    $id = uniqid();
                }
            }
        }

        $this->init($id);
        $this->view->content = $this->field('formText', $name, $type, $attribs);
        $this->view->value = $this->getValue();

        if (null !== $additionalCls) {
            $this->view->additionalCls = $additionalCls;
        }

        $resetSuffics = '';
        if (!empty($id)) {
            $resetSuffics = '_' . $id;
        }

        $this->view->resetBtnName = sprintf('%s[reset%s]', OSDN_Controller_Plugin_ViewFilter::IDENTITY, $resetSuffics);

        return $this;
    }

    public function __toString()
    {
        return $this->view->render('form-filter-decorator-wrap.phtml');
    }
}