<?php

/**
 *                         $this->formValue('name', $row);

                        $this->formValue()->chain($row, array());

                        $this->formValue('contactTypeId', $row, 'emailType');
                        $this->formValue('contactTypeId', $row, array('emailType', 'contactType'));
 * Enter description here ...
 * @author yaroslav
 *
 */
class Layout_View_Helper_FormValue extends Zend_View_Helper_Abstract
{
    protected $_data = array();

    protected $_model;

    protected $_omitNullableModel = false;

    public function formValue()
    {
        if (func_num_args() > 0) {
            return call_user_func_array(array($this, 'value'), func_get_args());
        }

        return $this;
    }

    public function init()
    {
        $this->_model = null;
        $this->_data = array();
        $this->_omitNullableModel = false;

        return $this;
    }

    public function __invoke()
    {
        return call_user_func_array(array($this, 'formValue'), func_get_args());
    }

    public function omitNullableModel($flag)
    {
        $this->_omitNullableModel = (boolean) $flag;
        return $this;
    }

    public function setData(array $data)
    {
        $this->_data = $data;
        return $this;
    }

    public function setModel($model)
    {
        $this->_model = $model;
        return $this;
    }

    public function value($field, $model = null, $fields = null)
    {
        if (array_key_exists($field, $this->_data)) {
            return $this->_data[$field];
        }

        if (null === $fields) {
            $fields = array($field);
        }

        return $this->chain($model, $fields);
    }

    public function getData($field)
    {
        return array_key_exists($field, $this->_data) ? $this->_data[$field] : null;
    }

    public function chain($model, $fields)
    {
        if (null === $model && null === $this->_model) {

            if ($this->_omitNullableModel) {
                return null;
            }

            throw new OSDN_Exception('Unable to reach model');
        }

        if (!is_array($fields)) {
            $fields = array($fields);
        }

        if (null === $model) {
            $model = $this->_model;
        }

        if (!$model instanceof OSDN_Application_Model_Interface) {
            throw new OSDN_Exception(sprintf(
                'The interface "OSDN_Application_Model_Interface" does not supported yet in "%s". Please implement.',
                get_class($model)
            ));
        }

        $o = $model;
        $lastMethod = null;
        foreach($fields as $field) {

            if (is_array($field)) {

                $amethod = array_shift($field);
                if (!method_exists($o, $amethod)) {
                    throw new OSDN_Exception(sprintf('The method "%s" does not exists in "%s"', $amethod, get_class($o)));
                }

                $o = call_user_func_array(array($o, $amethod), $field);
                break;
            }

            if (isset($o->$field)) {
                $o = $o->$field;
                break;
            }

            $method = 'get' . ucfirst($field);
            $lastMethod = $method;
            if (!method_exists($o, $method)) {
                break;
            }

            if (null === ($o = $o->$method())) {
                break;
            }
        }

        if (is_object($o) && $o instanceof OSDN_Application_Model_Interface) {
            throw new OSDN_Exception(sprintf('Chain is not finished "%s::%s"', get_class($o), $lastMethod));
        }

        return $o;
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->init();

        $this->view = $view;

        /**
         * @todo repalce to get from view
         */
        if (!empty($view->formData)) {
            $formData = $view->formData;
            if (is_array($formData)) {
                $this->setData($formData);
            }
        }

        return $this;
    }
}