<?php

class Layout_View_Helper_Summary extends Zend_View_Helper_Abstract
{
    public function summary()
    {
        $f = Zend_Controller_Front::getInstance();
        $bootstrap = $f->getParam('bootstrap');
        $mm = $bootstrap->getResource('ModulesManager');

        $cacheManager = $bootstrap->getResource('Cachemanager');
        if (null !== $cacheManager && $cacheManager->hasCache('memcache')) {
            $memcache = $cacheManager->getCache('memcache');
            Application_Instance_Summary::setCache($memcache);
        }

        $summaries = array();
        $unmatched = array();
        foreach($mm->fetchAll() as $information) {
            $oGroup = $information->getSummary()->toArray();
            $summaries[] = $oGroup;
        }

        $mergedSummaries = count($summaries) > 1 ? call_user_func_array('array_merge', $summaries) : $summaries;
        foreach($mergedSummaries as $kGroup => $vGroup) {

            if (
                !is_int($kGroup) ||
                empty($vGroup['id']) ||
                !array_key_exists($vGroup['id'], $mergedSummaries)
            ) {
                continue;
            }

            $name = $vGroup['id'];
            $mergedSummaries[$name]['items'] = array_merge($mergedSummaries[$name]['items'], $vGroup['items']);
            unset($mergedSummaries[$kGroup]);
        }

        $this->view->summaries = $mergedSummaries;
        return $this->view->render('summary.phtml');
    }

    /**
     * (non-PHPdoc)
     * @see Zend_View_Helper_Abstract::setView()
     */
    public function setView(Zend_View_Interface $view)
    {
        parent::setView($view);
        $this->view->addScriptPath(__DIR__ . '/scripts/default');
        return $this;
    }
}