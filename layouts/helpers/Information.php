<?php

/**
 * Errors helper
 *
 * Allow display errors in different forms
 *
 * @category    OSDN
 * @package     OSDN_View_Helper
 * @author      Yaroslav Zenin <yaroslav.zenin@gmail.com>
 * @version     $Id: Information.php 2103 2012-06-09 07:05:40Z vasya $
 */
class Layout_View_Helper_Information
{
    /**
     * @var Zend_View
     */
    public $view;

    protected $_imgLocation;

    protected $_messages = array(
        'list'      => array(),
        'fields'    => array(
            E_USER_NOTICE    => array(),
            E_USER_WARNING   => array(),
            E_USER_ERROR     => array()
        )
    );

    protected $_isInitialized = false;

    protected $_cls = array(
        E_USER_ERROR    => 'error',
        E_USER_WARNING  => 'warning',
        E_USER_NOTICE   => 'notice'
    );

    public function toList()
    {
        return $this->view->render('information.phtml');
    }

    public function toCls($field, $cls = null)
    {
        $output = array();

        foreach($this->_cls as $msgType => $msgClass) {

            if (isset($this->_messages['fields'][$msgType][$field])) {
                $output[] = $msgClass;
            }

        }

        return join(' ', array_unique($output));
    }

    public function toIcon($field)
    {
        $o = array();

        foreach($this->_cls as $msgType => $msgClass) {

            if (empty($this->_messages['fields'][$msgType][$field])) {
                continue;
            }

            $icons = array();
            foreach($this->_messages['fields'][$msgType][$field] as $f) {
                $icons[] = sprintf(
                    '<span class="uiTooltip ui-valid-state-%1$s" title="%2$s">&nbsp;</span>',
                    $msgClass,
                    $f->getMessage()
                );
            }
            $o[] = join('', $icons);
        }

        return join('', $o);
    }

    public function information()
    {
        return $this;
    }

    public function init($omitFieldMessagesInList = true, array $messages = null)
    {
        
        if (null === $messages) {
            $fm = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
            $fm->setNamespace('message-information');

            if ($fm->hasCurrentMessages()) {
                $messages = $fm->getCurrentMessages();
            } elseif ($fm->hasMessages()) {
                $messages = $fm->getMessages();
            }

            $fm->clearCurrentMessages();
            $fm->clearMessages();
        }

        if (empty($messages)) {
            return $this;
        }

        foreach($messages as $messageId => $message) {

            if (
                !is_object($message)
                || ! $message instanceof OSDN_Exception_Message
            ) {
                $this->_messages['list'][E_USER_ERROR][] = $message;
                continue;
            }

            $field = $message->getField();
            if (!empty($field)) {
                $this->_messages['fields'][$message->getType()][$field][] = $message;

                if (false !== $omitFieldMessagesInList) {
                    continue;
                }
            }

            $this->_messages['list'][$message->getType()][] = $message;
        }

        $this->view->list = $this->_messages['list'];
        $this->view->fields = $this->_messages['fields'];
        $this->view->cls = $this->_cls;

        return $this;
    }

    public function __toString()
    {
        try {
            return $this->toList();
        } catch (Exception $e) {
            $msg = get_class($e) . ': ' . $e->getMessage();
            trigger_error($msg, E_USER_ERROR);
            return '';
        }
    }

    /**
     * Apply view object
     *
     * @param Zend_View_Interface $view
     * @return
     */
    public function setView(Zend_View_Interface $view)
    {
        $this->view = clone $view;
        $this->view->clearVars();
        $this->view->setScriptPath(__DIR__ . '/scripts/default/');
    }
}