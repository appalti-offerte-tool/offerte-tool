<?php

class Proposal_View_Helper_ButtonGroup extends \Zend_View_Helper_Abstract
{

    public function buttonGroup(array $params = array())
    {
        $this->view->assign($params);
        return $this;
    }

    public function setView(Zend_View_Interface $view)
    {
        parent::setView($view);
        $this->view->addScriptPath(__DIR__ . '/scripts/appalti');
        return $this;
    }

    public function __toString()
    {
        try {
            return $this->view->render('button-group.phtml');
        } catch (\Exception $e) {
            echo $e->getTraceAsString();
        }
    }
}