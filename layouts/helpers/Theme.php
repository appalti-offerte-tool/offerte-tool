<?php

class Layout_View_Helper_Theme extends Zend_View_Helper_Abstract
{
    protected $_theme;

    protected $_isDefaultTheme = false;

    protected $_baseUrl;

    public function theme($path, $directory = null)
    {
        if (null === $this->_theme) {

            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $viewResource = $bootstrap->getPluginResource('view');
            $viewOptions = $viewResource->getOptions();

            $this->_theme = 'default';
            if (empty($viewOptions['theme'])) {
                $this->_isDefaultTheme = true;
            } elseif (0 === strcasecmp('default', $viewOptions['theme'])) {
                $this->_isDefaultTheme = true;
            } else {
                $this->_theme = $viewOptions['theme'];
            }

            $this->_baseUrl = $this->view->baseUrl();
        }

        if (null !== $directory) {

            /**
             * @FIXME Pay attention for webserver(WAMP) compability
             */
            $tPath = $directory . $path;
            if (false !== ($tPath = realpath(sprintf($tPath, $this->_theme)))) {
                $tPath = str_replace($directory, '', $tPath);
                return str_replace('\\', '/', $tPath);
            }

        } else if (false !== ($tPath = realpath(sprintf($path, $this->_theme)))) {
            return str_replace('\\', '/', $tPath);
        }

        return sprintf($path, 'default');
    }

    public function setView(Zend_View_Interface $view)
    {
        parent::setView($view);
        return $this;
    }
}