<?php

/**
 * @todo Make module actions grabber...
 *
 * @version $Id: ActionMenu.php 333 2010-12-30 12:07:20Z vanya $
 */
class Layout_View_Helper_ActionMenu extends Zend_View_Helper_Abstract
{
    protected $_title;

    protected $_withMostUsed = false;

    protected $_items = array();

    public function actionMenu(array $items = array())
    {
        return $this;
    }

    public function withMostUsed()
    {
        $this->_withMostUsed = true;
        return $this;
    }

    public function setCategoryItems($categoryTitle, array $items = array())
    {
        $column = array(
            'title'    => $categoryTitle,
            'items'    => $items
        );

        $this->_items[] = $column;
        return $this;
    }

    /**
     * @deprecated Will be removed when module actions grabber implemented
     */
    public function fillWithDummyValues()
    {
                $o = array();
        for ($i = 0; $i < 2; $i++) {

            $column = array(
                'title'    => 'Category ' . $i,
                'items'    => array()
            );

            $jCount = rand(5, 8);
            for ($j = 0; $j < $jCount; $j++) {
                $column['items'][] = array(
                    'name'    => 'Some mega action ' . $j . str_repeat('q', rand(0, 15)),
                    'title'    => 'Some mega title action ' . $j
                );
            }

            $o[] = $column;
        }

        $this->_items = array_merge($this->_items, $o);
        return $this;
    }

    public function __toString()
    {
        $this->view->actions = $this->_items;

        $this->view->setScriptPath(__DIR__ . '/scripts/default');
        $this->view->title = $this->_title;
        $this->view->withMostUsed = $this->_withMostUsed;

        $this->_reset();

        try {
            return $this->view->render('action-menu.phtml');
        } catch(Exception $e) {
            return sprintf('%s::%s - %s', __CLASS__, __METHOD__, $e->getMessage());
        }
    }

    protected function _reset()
    {
        $this->_items = array();
        $this->_title = null;
        $this->_withMostUsed = false;

        return $this;
    }

    public function setTitle($title)
    {
        $this->_title = $title;
        return $this;
    }

    public function setView(Zend_View_Interface $view)
    {
        parent::setView(clone $view);
        $this->setTitle($this->view->translate('Actions'));

        return $this;
    }
}