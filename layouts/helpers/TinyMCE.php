<?php

class Layout_View_Helper_TinyMCE extends Zend_View_Helper_Abstract
{
    public function tinymce(array $params = array())
    {
        if (!empty($params['plugins_data']['mykwd'])) {
            $result = array();
            foreach ($params['plugins_data']['mykwd'] as $k => $v) {
                $result[] = array(
                    'value' => $k,
                    'name'  => $v,
                );
            }
            $params['plugins_data']['mykwd'] = json_encode($result);

        }

        if (!empty($params['plugins_data']['clientkwd'])) {
            $result = array();
            foreach ($params['plugins_data']['clientkwd'] as $k => $v) {
                $result[] = array(
                    'value' => $k,
                    'name'  => $v,
                );
            }
            $params['plugins_data']['clientkwd'] = json_encode($result);
        }

        $this->view->assign($params);
        return $this->view->render('tinymce.phtml');
    }

    /**
     * (non-PHPdoc)
     * @see Zend_View_Helper_Abstract::setView()
     */
    public function setView(Zend_View_Interface $view)
    {
        parent::setView($view);
        $this->view->addScriptPath(__DIR__ . '/scripts/appalti');
        return $this;
    }
}