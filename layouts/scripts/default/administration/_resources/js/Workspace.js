Ext.define('Application.Layout.Workspace', {
    extend: 'Ext.Base',

    blocks: null,
    blocksCfg: null,
    blocksFlexWidth: 0.6,

    workspace: null,
    workspaceCfg: null,

    invokeArgs: null,

    isClassic: true,

    mixins: {
        observable: 'Ext.util.Observable'
    },
    events: [],
    items: {
        center: null,
        west: null,
        east: null
    },

    constructor: function(items, wc) {

        if (arguments.length > 1) {
            Ext.apply(this, wc);
        }

        if (!Ext.isArray(items)) {
            this.items.center = items;
        } else Ext.each(items, function(i) {
            if (i.region && this.items.hasOwnProperty(i.region)) {
                this.items[i.region] = i;
            }
        }, this);

        this.callParent();
    },

    setWorkspace: function(p) {

        if (Ext.isObject(p) && p.hasOwnProperty('title')) {
            var w = this.workspace;
            if (w.header) {
                w.header.removeAllListeners();
                Ext.destroy(w.header);
            }
        }

        this.workspace.removeAll();

        this.workspace.add(p);
        this.workspace.doLayout();
    },

    getWorkspace: function(c) {

        if (this.workspace) {
            return this.workspace;
        }

        var w = this.items.center;
        w.region = 'center';
        w.flex = 1 - this.blocksFlexWidth;

        this.workspace = Ext.ComponentManager.create(w);

        return this.workspace;
    },

    setInvokeArgs: function(params) {

        var blocks = this.getBlocks();
        if (false !== blocks) {
            blocks.items.each(function(b) {
                b.setInvokeArgs(params);
            });

            var b = blocks.getActiveTab();
            b && b.reload();
        }

        return this;
    },

    getBlocks: function(blocksCfg) {

        if (this.blocks) {
            return this.blocks;
        }

        this.blocks = new Module.Application.BlockTabPanel(Ext.apply({
            ddGroupId: 'test',
            headerCssClass: 'x-osdn-wizard-block-header',
            bodyCls: 'x-osdn-wizard-block-body',
            deferredRender: false,
            region: 'east',
            activeTab: 0,
            stateful: false,
            stateId: false,
            split: true
        }, blocksCfg));

        this.blocks.on({
            render: function(p) {

                if (!p.contentEl) {

                    /**
                     * @todo Here is a very terrible situation with different kind
                     * of layouts. We need get access to block tabs and after that
                     * it will be converted to Ext.TabPanel via autoTabs property
                     */
                    var el = p.ownerCt.getEl();
                    var tabs = el.down('.block-tabs');
                    p.contentEl = tabs.id;
                }
            },
            afterrender: function(p) {

                var hash = location.hash;
                if (!hash) {
                    return;
                }
                hash = hash.replace(/^#/, '');

                p.items.each(function(block) {
                    if (block.name == hash) {
                        p.setActiveTab(block);
                        return false;
                    }
                });
            },
            resize: function(tb, b) {
                this.setCookie('split-layout-width', tb.getWidth());
            },
            tabchange: function(tb, b) {
                b && b.reload();
                b && b.doLayout();
            },
            scope: this
        });

        return this.blocks;
    },

    toArray: function(cfg) {

        var o = [];

        var b = this.getBlocks({
            region: 'east',
            flex: this.blocksFlexWidth
        });
        if (false !== b) {
            o.push(b);
        }

        o.push(this.getWorkspace());
        if (this.items.west) {
            this.items.west.split = true;
            o.push(Ext.ComponentManager.create(this.items.west));
        }

        return o;
    },

    getCookie: function(param) {
        return Ext.state.Manager.getProvider().get(param);
    },

    setCookie: function(param, value) {
        Ext.state.Manager.getProvider().set(param, value);
        return this;
    },

    setBlockWidth: function(w) {
        this.blocks && this.blocks.setWidth(w);
        return this;
    },

    onWorkspaceAfterRenderCallback: function(w) {

        w.dropZone = new Ext.dd.DropZone(w.getEl(), {

            ddGroup: 'test',

            getTargetFromEvent: function(e) {
                return w.getEl();
            },

            onNodeEnter : function(target, dd, e, data) {
                this.mask = w.container.createChild({cls:'ext-el-mask'}, w.el.dom);
                w.getEl().setStyle('z-index', 100);
            },

            onNodeOut : function(target, dd, e, data){
                this.mask.remove();
                w.getEl().setStyle('z-index', 'auto');
            },

            onNodeOver : function(target, dd, e, data){
                return Ext.dd.DropZone.prototype.dropAllowed;
            },

            onNodeDrop : function(target, dd, e, data){
                var item = Ext.getCmp(dd.el.id.split('__')[1]);
                Ext.getBody().mask(item.title + ': ' + Ext.LoadMask.prototype.msg, 'x-mask-loading');
                Ext.defer(function() {
                    window.location.href = '/' + item.module;
                }, 50);

                return true;
            }
        });
    }
});