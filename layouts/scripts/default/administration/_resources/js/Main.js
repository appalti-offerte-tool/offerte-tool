Ext.define('Application.Layout.Main', {
    extend: 'Ext.container.Viewport',
    alias: 'application.layout.main',

    layout: 'border',

    stateful: false,
    stateId: false,

    pSummary: null,

    constructor: function(c) {

        c.items = [];

        this.pHeader = new Ext.Panel({
            stateful: true,
            stateId: 'application-header',
            contentEl: 'header',
            stateEvents: ['hide', 'show'],
            getState: function() {
                return {hidden: this.hidden};
            },
            border: false
        });

        this.pSummary = new Ext.Panel({
            contentEl: 'summary-block',
            stateful: true,
            stateId: 'application-summary',
            stateEvents: ['hide', 'show'],
            getState: function() {
                return {hidden: this.hidden};
            },
            border: false
        });

        c.items.push({
            region: 'north',
            animCollapse: false,
            margins: '0 0 5 0',
            preventHeader: true,
            resizable: false,
            border: false,
            bodyCls: 'x-docked-noborder-left x-docked-noborder-top x-docked-noborder-right',
            items: [this.pHeader, this.pSummary, {
                xtype: 'toolbar',
                items: this.toMenuItems(Application.MENU)
            }]
        });

        c.items.push({
            region: 'south',
            contentEl: 'footer',
            split: true,
            collapsible: true,
            collapsed: true,
            collapseMode: 'mini',
            header: false,
            border: false
        });

        var w = c.workspace;
        if (w.isClassic) {
            c.items = c.items.concat(w.toArray());
        } else {
            w.region = 'center';
            c.items.push(w);
        }

        this.callParent([c]);
    },

    toMenuItems: function(mi) {

        var _toItem = function(o) {

            Ext.each(o, function(item) {

                if (item.handler) {
                    item.handler = new Function([], item.handler);
                }

                if (item.children) {
                    item.menu = _toItem(item.children);
                }

                delete item.children;
            });

            return o;
        };

        var data = _toItem(mi);

        data = data.concat(['->', '-', {
            iconCls: 'icon-16 icon-layout-toggle-header',
            qtip: lang('Toggle header'),
            handler: this.onToggleHeaderView,
            enableToggle: true,
            scope: this
        }, {
            iconCls: 'icon-16 icon-layout-toggle-summary',
            qtip: lang('Toggle summary and wizards'),
            handler: this.onToggleSummaryView,
            enableToggle: true,
            scope: this
        }, {
            iconCls: 'icon-16 icon-layout-50x50',
            qtip: lang('Divide workspace evenly')
        }, {
            iconCls: lang('icon-16 icon-layout-reset'),
            qtip: 'Reset layout'
        }, '-', {
            iconCls: 'icon-16 icon-logout-16',
            text: lang('Logout'),
            href: link('account', 'authenticate', 'logout'),
            target: '_self'
        }]);

        return data;
    },

    onToggleSummaryView: function() {
        var p = this.pSummary;
        p.setVisible(p.hidden);
    },

    onToggleHeaderView: function() {
        var p = this.pHeader;
        p.setVisible(p.hidden);
    }
});