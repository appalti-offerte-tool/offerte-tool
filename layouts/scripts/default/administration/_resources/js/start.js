Application.block = function(b, s, cb) {

    var bb = Ext.getCmp(b);
    if (bb) {
        Application.require(s, Ext.bind(cb || Ext.emptyFn, bb));
        return;
    }
    var args = arguments;

    Ext.ComponentMgr.onAvailable(b, function() {
        Application.block.apply(this, args);
    });
};

Ext.BLANK_IMAGE_URL = '/public/js/library/ext-4.0.7-gpl/resources/themes/images/access/tree/s.gif';

Ext.grid.DateColumn.prototype.format = 'd-m-Y';
Ext.form.DateField.prototype.format = 'd-m-Y';

Ext.Msg.minWidth = 350;

Ext.grid.GridPanel.prototype.stripeRows = true;
Ext.grid.GridPanel.prototype.autoExpandMin = 150;
Ext.grid.GridPanel.prototype.loadMask = Ext.LoadMask.prototype.msg;

//Ext.form.Field.prototype.msgTarget = 'side';
Ext.DatePicker.prototype.startDay = 1;

//Ext.override(Ext.form.field.HtmlEditor, {
//    getValue: function() {
//        var v = this.callOverridden();
//        return v.substring(0, v.length - 1);
//    }
//});

Ext.onReady(function(){

    Ext.QuickTips.init();

    try {
        var escp = new Ext.state.LocalStorageProvider();
    } catch(e) {

        /**
         * @FIXME
         */
        var location = window.location.pathname.split('/');
        var escp = new Ext.state.CookieProvider({
            path: (Ext.isIE) ? '/' : '/' + location.slice(1,3).join('/'),
            expires: new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 30))
        });
    }

    Ext.state.Manager.setProvider(escp);

    // disable stateful
    Ext.override(Ext.Component,{ stateful:false });

    Ext.Ajax.on('requestexception', function(conn, response, options) {
        switch (response.status) {
            case 401:
                alert("Authorization is expired! You'll be redirected to login screen.");
                window.location.reload();
                break;

            case 404:
                alert('Page is not found');
                break;

            case 405:

                alert("You don't have permission for that!");
                break;

            case 500:
                alert('Internal Server Error');
                break;
        }
    });
});

Application.notificate = function() {

    var a = arguments;

    Application.require([
        'notification/information/panel'
    ], function() {
        notification = new Module.Notification.Information.Panel();
        notification.notificate.apply(notification, a);

        Application.notificate = Ext.bind(notification.notificate, notification);
    });
};