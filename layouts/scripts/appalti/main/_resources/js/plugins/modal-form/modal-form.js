;
(function ($, window) {

    var ModalForm = function ($el) {
        if (null == this.modalCnt) {
            this.modalCnt = $('<div id="modal-container"></div>').appendTo('body');
        }

        var self = this;
        $el.on('click', function () {
            $.when(getModalForm.call(self, this)).done(function () {
                self.modalCnt.modal('toggle');
            });

            return false;
        });
    }

    ModalForm.prototype = {
        constructor: ModalForm,

        cache: [],

        modalCnt: null,

        show: function() {

        }
    }

    function getModalForm(action) {
        var $action = $(action), self = this,
            cacheData = $action.data('cache');

        if (cacheData && $action.data('cacheId')) {
            var dfd = $.Deferred();
            self.modalCnt.html(self.cache[$action.data('cacheId')]).addClass('modal');
            return dfd.resolve().promise();
        } else {
            return $.get(action.href).done(function (data) {
                self.modalCnt.html(data).addClass('modal');
                if (cacheData) {
                    var cacheId = (new Date()).getTime() + 1;
                    self.cache[cacheId] = data;
                    $action.data('cacheId', cacheId);
                }
            });
        }
    }

    function showForm() {

    }

    $.fn.modalForm = function () {
        return this.each(function () {
            var $this = $(this), data = $this.data('modal-form');

            if (!data) {
                $this.data('modal-form', new ModalForm($this));
            } else {

            }
        });
    }

    $.fn.modalForm.defaults = {
        onShow: $.noop(),
        onHide: $.noop()
    }

    $.fn.modalForm.Constructor = ModalForm;

    $(function () {
        $('body').on('click.modalForm.data-api', '.modal-form-trigger', function (e) {
            e.preventDefault()
            $(this).modalForm();
        });
    });
})(jQuery, window);