link = function(module, controller, action, params, router, absolute) {
    var bp = '/';

    var p = [];
    for (k in params) {
        p.push(k);
        p.push(params[k]);
    }

    var o = bp + [module, controller, action].concat(p).join('/');
    if (!/\/$/.test(o)) {
        o += '/';
    }

    return o;
};

String.prototype.ucFirst = function() {
    return this.substr(0,1).toUpperCase() + this.substr(1,this.length);
};

number_format = function( number, decimals, dec_point, thousands_sep ) {
    var i, j, kw, kd, km;

    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = 2;
    }
    if( dec_point == undefined ){
        dec_point = ",";
    }
    if( thousands_sep == undefined ){
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if( (j = i.length) > 3 ){
        j = j % 3;
    } else{
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");

    return km + kw + kd;
}

;(function($, win) {

    var AppaltiLayout = function() {
        this.init();
    }

    function handleErrors(status) {
        switch (status) {
            case 401:
                return "Authorization expired! You'll be redirected to login screen.";
            case 404:
                return "Page is not found.";
            case 405:
                return "You don't have permission for that!";
            case 500:
                return "Internal Server Error";
            default:
                return false;
        }
    }


    AppaltiLayout.prototype = {

        constructor: AppaltiLayout,

        blocks: {},

        init: function() {

            $.ajaxPrefilter(
                function( options, localOptions, jqXHR ){

                    if (options.mask) {
                        al.mask(options.maskMsg, options.maskTarget)
                    }

                    $.Deferred(function( defer ) {
                        jqXHR.done(function(data, textStatus, xhr) {
                            options.mask && al.unmask();
                            defer.resolveWith(this, arguments);
                        }).fail(function(xhr) {
                            options.mask && al.unmask();
                            defer.reject();
                            var msg = handleErrors(xhr.status);
                            if (msg) {
                                defer.reject();
                                alert(msg);
                                win.location.reload();
                            }
                        });
                    }).promise(jqXHR);

                    jqXHR.success = jqXHR.done;
                    jqXHR.error = jqXHR.fail;
                }
            );

            $.ajaxSetup({
                mask: false,
                maskMsg: 'Verwerking. Een ogenblik geduld.'
            });

            $(function() {
                al.initAccordion();
                al.initValidator();
                al.initFormValidation();
                al.initTableSorter();
                al.initPersonalDataEdit();
                al.initTooltip();
                al.initPlaceholder();
                al.initDatePicker();
                al.initInlineSearch();
            });
        },

        createMask: function() {
            return $('<div class="mask"><span class="mask-msg" style="display: none;" /></div>');
        },

        mask: function(message, target) {
            return $.Deferred(function(dfd) {
                if (target) {
                    var targetEl = $(target);
                    /(static)/i.test(targetEl.css('position')) && targetEl.css('position', 'relative');
                    al.targetMask = al.createMask().css('position', 'absolute').appendTo(targetEl);
                } else if (!al.bodyMask) {
                    al.bodyMask = al.createMask().appendTo('body');
                }

                var maskEl = al.targetMask || al.bodyMask,
                    maskMsgEl = $('.mask-msg', maskEl);

                maskMsgEl.html(message);
                maskEl.fadeIn('fast', function() {
                    maskMsgEl.css({
                        'marginTop': -(maskMsgEl.height() / 2),
                        'marginLeft': -(maskMsgEl.width() / 2)
                    }).show();

                    dfd.resolve();
                });
            }).promise();
        },

        unmask: function() {
            var maskEL = al.targetMask || al.bodyMask,
                maskMsgEl = $('.mask-msg', maskEL);

            maskMsgEl && maskMsgEl.fadeOut('fast', function() {
                maskEL.fadeOut();
                maskMsgEl.removeAttr('style').hide();
            });

            if (al.targetMask) {
                maskEL.remove();
                al.targetMask = null;
            }
        },

        downloadFile: function(url) {
            if ($.fileDownload) {
                al.mask('Voorbereiden bestand...');
                $.fileDownload(url, {
                    successCallback: function(url) {
                        al.unmask();
                    },
                    failCallback: function(responseHtml, url) {
                        alert(responseHtml);
                        al.unmask();
                    }
                });
            } else {
                win.location.href = url;
            }
        },

        initInlineSearch: function() {
            if ('function' === typeof $.search) {
                $('.inline-search > input:text').keyup(function() {
                    $(this).parent().parent().find('.searchable').search(this.value, 'by substring', { remove : 'parent' });
                })
            }
        },

        initPlaceholder: function() {
            if ('function' === typeof $.fn.placeholder) {
                $('input, textarea').placeholder();
            }
        },

        initAccordion: function($container) {
            function getSectionContent(header) {
                var link = $('a', header);
                if (link.data('url') && !link.data('loaded')) {
                    var target = $(link.data('target')).html('<p class="accordion-section-empty">Loading...</p>');
                    target.load(link.data('url'), function() {
                        link.data('loaded', true);
                        al.initTooltip(target);
                        $('body').triggerHandler('sectionLoaded', [link.data('target')]);
                    });
                }
            }

            if ('function' === typeof $.fn.accordion) {
                $('.accordion', $container || 'body')
                .accordion('destroy')
                .accordion({
                    active: false,
                    autoHeight:false,
                    header:' > .accordion-section-title',
                    navigation: true,
                    collapsible: true,
                    icons:{
                        'header':'icon-plus',
                        'headerSelected':'icon-minus'
                    },
                    create: function(event, ui) {
                        getSectionContent($(event.target).data('accordion').active);
                    },
                    change: function(event, ui) {
                        var header = ui.newHeader.length ? ui.newHeader : ui.oldHeader,
                            href = $('a', header )[0].href;

                        !(/#$/g.test(href)) && (win.location.href = href);
                        getSectionContent(header);
                    }
                });
            }
        },

        filterDigitsOnly: function(e) {
            var code = (e.which) ? e.which : e.keyCode;
            switch (code) {
                case 8 :  // backspace
                case 9 :  // tab
                case 13: // enter
                case 37: // left
                case 38: // up
                case 39: // right
                case 40: // down
                    return true;

                case 44  : // ,
                case 188 : // ,
                case 46  : // .
                case 190 : // .
                case 110 : // decimal point
                    var target = (e.currentTarget) ? e.currentTarget : e.srcElement;
                    return !/[,.]/.test(target.value);

                default:
                    return (code === 0 || (code >= 48 && code <= 57) || (code >= 96 && code <= 105));
            }
        },

        onUpdatePersonalData: function(data) {
            if (data.success) {
                $.get('/company/contact-person/get-photo-src', {
                    format: 'json',
                    contactPersonId: data.contactPersonRow['id']
                }, function(response) {
                    if (response.success) {
                        $('#currentUserPhoto').attr('src', response.src);
                    }

                    al.editCredentialsModal.off('hide').modal('hide');
                });
            } else {
                al.markErrorFields($('form', al.modalCnt), data.messages);
            }
        },

        markErrorFields: function ($form, errors) {
            for (var i in errors) {
                $('input[name="' + errors[i].field + '"]', $form)
                .closest('tr')
                .find('.info-cnt')
                .append('<a class="icon-info-error-small" href="#" title="' + errors[i].message + '">&nbsp;</a>');
            }

            $('.icon-info-error-small').tooltip();
        },

        initPersonalDataEdit: function() {
            $('#account-update').click(function() {
                $.get(this.href, function(html) {
                    if (!al.editCredentialsModal) {
                        al.editCredentialsModal = $('<div id="edit-credentials" data-backdrop="static" class="modal" />').appendTo('body');
                        al.editCredentialsModal.on('change', '#changeCredentials', function() {
                            this.checked ? $('.changeCredentials').removeAttr('disabled') :
                                           $('.changeCredentials').attr('disabled', 'disabled');

                        });
                    }

                    al.editCredentialsModal.html(html);
                    al.initTooltip(al.editCredentialsModal);
                    var form = $('form', al.editCredentialsModal),
                        preloader = $('.ajax-preloader', form);

                    form.ajaxForm({
                        beforeSubmit: function() {
                            if (!form.valid()) {
                                return false;
                            }

                            preloader.addClass('show');
                        },
                        success: function(data) {
                            preloader.removeClass('show');
                            data = $.parseJSON(data);
                            al.onUpdatePersonalData(data);
                        }
                    });

                    al.editCredentialsModal
                        .off('hide hidden')
                        .on('hide', function() {
                            return !form.observable().isDirty() ||
                                   confirm('Annuleren veranderingen?');
                        })
                        .on('hidden', function() { al.editCredentialsModal.empty() })
                        .modal('show');
                });

                return false;
            });
        },

        initValidator: function() {
            if (!$.validator) {
                return;
            }

            var appendErrorLabel = function(container, error) {
                if (container.hasClass('not-empty')) {
                    container.find('.icon-info-error-small').remove();
                }

                error.removeClass('hidden').tooltip();
                if (!container.children().length) {
                    container.empty();
                }
                container.append(error).addClass('not-empty');
                $('.icon-info-small', container).addClass('hidden');
            };

            $.validator.setDefaults({
                onkeyup: false,
                errorElement: 'a',
                errorClass: 'invalid',
                ignore: '.ignore',
                invalidHandler: function(form, validator) {
                    var els = validator.invalidElements().css('background', '#FAFF96');
                    setTimeout(function() { els.css({'background': '#fff'}); }, 400);
                    setTimeout(function() { els.css({'background': '#FAFF96'}); }, 800);
                    setTimeout(function() { els.css({'background': '#fff'}); }, 1200);
                },
                errorPlacement: function(error, element) {
                    error.attr('title', error.text())
                        .html('&nbsp;')
                        .addClass('icon-info-error-small')
                        .removeClass('invalid');

                    var container = element.closest('td').prev('.info-cnt');
                    appendErrorLabel(container, error);
                },
                success: function(label) {
                    var form = label.closest('form'),
                        el = $('[name="' + label.attr('for') + '"]', form);

                    if (!el.length) {
                        el = $('#' + label.attr('for'));
                    }

                    var container = el.closest('td').prev('.info-cnt');

                    container
                    .removeClass('not-empty')
                    .find('.icon-info-error-small').remove()
                    .end()
                    .children()
                    .removeClass('hidden');
                }
            });

            $.extend($.validator.messages, {
                required: "Dit veld vereist.",
                remote: "Corrigeer dit gebied.",
                email: "Vul een geldig e-mailadres.",
                url: "Voer een geldige URL in. Voorbeeld: www.sitename.nl",
                date: "Vul een geldige datum.",
                number: "Vul een geldig nummer.",
                digits: "Vul alleen cijfers.",
                creditcard: "Vul een geldig credit card nummer.",
                imageOnly: "Ongeldig bestandstype. Alleen jpg, png, gif zijn toegestaan.",
                equalTo: "Gelieve opnieuw in te voeren dezelfde waarde.",
                maxlength: $.validator.format("Geef niet meer dan {0} karakters."),
                minlength: $.validator.format("Geef aub minimaal {0} karakters."),
                rangelength: $.validator.format("Geef een waarde tussen {0} en {1} tekens lang zijn."),
                range: $.validator.format("Geef een waarde tussen {0} en {1}."),
                max: $.validator.format("Geef een waarde kleiner dan of gelijk aan {0}."),
                min: $.validator.format("Geef een waarde groter dan of gelijk is aan {0}.")
            });

        },

        initFormValidation: function($form) {

            if (!$.validator) {
                return;
            }

            var validatable = $form || $('form.validatable');
            validatable.observe().validate();

            $('[data-accept]', validatable).each(function() {
                var el = $(this);
                el.rules('add', { file: el.data('accept') })
            });

            $('[data-remoteValidation]', validatable).each(function() {
                var el = $(this);
                el.rules('add', {
                    remote: {
                        url: el.data('remotevalidation'),
                        type: "post",
                        data: {
                            format: 'json',
                            username: function() {
                                return el.val();
                            }
                        }
                    },
                    messages: {
                      remote: el.data('invalidmessage')
                    }
                 });
            }).change(function() {
                $(this).closest('form').data('validator').element(this);
            });
        },

        initDatePicker: function(el) {
            if ('function' === typeof $.fn.datepicker) {

                var setDateRange = function(selectedDate, ui) {
                    if (!!ui.settings.rangeTo) {
                        $(ui.settings.rangeTo).datepicker( "option", "minDate", selectedDate );
                    }

                    if (!!ui.settings.rangeFrom) {
                        $(ui.settings.rangeFrom).datepicker( "option", "maxDate", selectedDate );
                    }
                }

                var datePickers = $('.datepicker', el || 'body').each(function() {
                    var el = $(this),
                        $altField = $(el.data('altField')),
                        createAltField = !$altField.length;

                    if (createAltField) {
                        $altField = $('<input />',{
                            name: el.attr('name'),
                            type: 'hidden'
                        }).insertBefore(el);

                        el.removeAttr('name');
                    }

                    el.datepicker($.extend({}, {
                        dateFormat: 'dd-mm-yy',
                        altFormat: 'yy-mm-dd'
                    }, el.data()));


                    if (el.hasClass('dateRange')) {
                        el.datepicker('option', 'onSelect', setDateRange)
                    }

                    if (!!el.val()) {
                        var date = $.datepicker.formatDate('dd-mm-yy', new Date(el.val().split('-').reverse().join('/')));
                        el.datepicker("setDate", date);
                    }

                    el.change(function() {
                       el.datepicker("setDate", el.val());

                       if (el.hasClass('dateRange')) {
                           var dp = el.data('datepicker');
                           setDateRange(el.val(), dp);
                       }
                    });
                });

                datePickers.filter('.dateRange[value!=""]').each(function() {
                    var dp = $(this).data('datepicker');
                    setDateRange(this.value, dp);
                });
            }
        },

        initTooltip: function(container) {
            if ('function' === typeof $.fn.tooltip) {
                $('a[title], .tip[title]', container || 'body').tooltip();
            }
        },

        initTableSorter: function(container) {
            if ('function' === typeof $.fn.tablesorter) {
                var noSortHeaders = {}
                $('table.tablesorter', container || 'body').each(function() {

                    $('thead th:not(:has(">span"))', this).each(function() {
                        noSortHeaders[$(this).index()] = {
                            sorter: false
                        }
                    })

                    $(this).tablesorter({ headers: noSortHeaders });
                })

            }
        },

        notificate: function(title, msg, image, priority) {
            $.growl && $.growl(title || 'Informatie', msg, image, priority);
        }
    }

    win.AppaltiLayout = new AppaltiLayout();
    var al = win.AppaltiLayout;

})(jQuery, window);


window.AppaltiLayoutBlock = function(params) {
    $.extend(this, params || {});

    $($.proxy(this.init, this));
}

AppaltiLayoutBlock.prototype = {
    maskMsg: 'Herladen blok inhoud. Een ogenblik geduld.',
    action: '',
    defaultListUpdateParams : {
        listOnly: true,
        page: 1
    },
    listUpdateParams: {},
    onReloadList: $.noop,
    onBeforeInit: $.noop,
    onInit: $.noop,

    init: function() {

        this.onBeforeInit();

        this.$el = $('#' + this.updateElId);
        this.eventNS = 'Block' + (new Date().getTime())
        this.url = link('block', this.module, this.block);

        if (this.blockId) {
            this.listUpdateParams._bid = this.blockId;
        }

        this.initPagination();
        this.initSearchFilter();
        this.onInit();
    },

    initPagination: function() {
        var el, me = this;
        me.$el.on('click.' + this.eventNS, '.pagination-bar a', function() {
            el = $(this);

            if (el.hasClass('current-page')) {
                return false;
            }

            if (el.hasClass('next-page')) {
                if (el.data('page')) {
                    me.listUpdateParams.page = el.data('page');
                } else {
                    me.listUpdateParams.page++;
                }
                me.reloadList();
                return false;
            }

            if (el.hasClass('previous-page')) {

                if (me.listUpdateParams.page > 1) {
                    if (el.data('page')) {
                        me.listUpdateParams.page = el.data('page');
                    } else {
                        me.listUpdateParams.page--;
                    }
                }
                me.reloadList();
                return false;
            }

            me.listUpdateParams.page = parseInt(el.html());
            me.reloadList();
            return false;
        });
    },

    initSearchFilter: function() {
        var me = this,
            prepareParams = function($form, reset) {
                me.listUpdateParams.page = 1;
                jQuery.each(me.listUpdateParams, function(index) {
                    if (index.indexOf('93962CEE-FD0D-42A2-8CEB-91993234569C') >= 0) {
                        delete(me.listUpdateParams[index]);
                    }
                });
                var params = $form.serializeArray();

                for (var i = 0, len = params.length; i < len; i++) {
                    me.listUpdateParams[params[i].name] = params[i].value;
                }

                if (reset) {
                    var name = $form.find('button[value="reset"]')[0].name;
                    me.listUpdateParams[name] = 1;
                }
            }

        this.$el
            .on('submit', '.search-panel form', function(e) {
                prepareParams($(this));
                me.reloadList.call(me);
                return false;
            })
            .on('click', '.search-panel form button[value="reset"]', function() {
                var $form = $(this).closest('form')
                prepareParams($form, true);
                me.reloadList.call(me);
                return false;
            });
    },

    /**
     * @return $.Deferred
     */
    reloadList: function() {
        var me = this;
        return $.ajax({
            url: this.url,
            data: $.extend({}, me.defaultListUpdateParams, me.listUpdateParams),
            type: 'post',
            mask: true,
            maskTarget: me.$el,
            maskMsg: me.maskMsg,
            success: function(response) {
                me.$el.html(response);
                me.onReloadList.call(me);
            }
        });
    }
}