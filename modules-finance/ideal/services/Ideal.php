<?php

class Ideal_Service_Ideal extends Ideal_Service_IdealRequest
{

    /**
     * @var Ideal_Model_DbTable_IdealTransaction
     */
    protected $_table;

    const SUCCESS = 'SUCCESS';

    public function _init()
    {
        $this->_table = new \Ideal_Model_DbTable_IdealTransaction($this->getAdapter());

//                $params['transactionId'] = $oTransactionRequest->sTransactionId;
//                $params['merchantId'] = $oTransactionRequest->sMerchantId;
//                $params['description'] = $oTransactionRequest->sOrderDescription;
//                $params['cartId'] = $oTransactionRequest->sOrderId;
//                $params['amount'] = $oTransactionRequest->iOrderAmount;
//                $params['status'] = 'PENDING';
//                $params['entranceCode'] = $oTransactionRequest->sEntranceCode;
//                $params['redirectSuccess'] = $oTransactionRequest->sReturnUrl;
//                $params['redirectError'] = $oTransactionRequest->sReturnUrl;
//                $params['issuerAuthenticationUrl'] = $oTransactionRequest->sTransactionUrl;


        $this->_attachValidationRules('default', array(
            'transactionId' => array('allowEmpty' => false, 'presence' => 'required'),
            'merchantId' => array('allowEmpty' => false, 'presence' => 'required'),
            'description' => array('allowEmpty' => false, 'presence' => 'required'),
            'cartId' => array('allowEmpty' => false, 'presence' => 'required'),
            'amount' => array('allowEmpty' => false, 'presence' => 'required'),
            'status' => array('allowEmpty' => false, 'presence' => 'required'),
            'entranceCode' => array('allowEmpty' => false, 'presence' => 'required'),
            'redirectSuccess' => array('allowEmpty' => false, 'presence' => 'required'),
            'redirectError' => array('allowEmpty' => false, 'presence' => 'required'),
            'issuerAuthenticationUrl' => array('allowEmpty' => false, 'presence' => 'required')
        ));

        $this->_attachValidationRules('update', array(
            'transactionId' => array('allowEmpty' => false, 'presence' => null),
            'merchantId' => array('allowEmpty' => false, 'presence' => null),
            'description' => array('allowEmpty' => false, 'presence' => null),
            'cartId' => array('allowEmpty' => false, 'presence' => null),
            'amount' => array('allowEmpty' => false, 'presence' => null),
            'status' => array('allowEmpty' => false, 'presence' => null),
            'entranceCode' => array('allowEmpty' => false, 'presence' => null),
            'redirectSuccess' => array('allowEmpty' => false, 'presence' => null),
            'redirectError' => array('allowEmpty' => false, 'presence' => null),
            'issuerAuthenticationUrl' => array('allowEmpty' => false, 'presence' => null),
            'status' => array('allowEmpty' => false, 'presence' => 'required')
        ), 'default');

        parent::_init();
    }


    public function request(array $params = array())
    {

       // Set default timezone for DATE/TIME functions
        if(function_exists('date_default_timezone_set'))
        {
            date_default_timezone_set('Europe/Amsterdam');
        }

        // Opvragen van order informatie uit POST data.
        $sOrderId = $params['order_id']; // Unieke verwijzing naar de order
        $sOrderDescription = substr($params['order_description'], 0, 32); // Maximaal 32 karakters
        $fOrderAmount = $params['order_amount']; // Bedrag in EURO'S (2 decimalen)!
        $sIssuerId = $params['order_issuer']; // ID van de geselecteerde bank


        // Genereer een zelf op te geven EntranceCode (tot 40 karakters)
        $sEntranceCode = sha1($sOrderId . '_' . date('YmdHis'));

        $oTransactionRequest = new \Ideal_Service_TransactionRequest();

        // Order settings
        $oTransactionRequest->setOrderId($sOrderId);
        $oTransactionRequest->setOrderDescription($sOrderDescription);
        $oTransactionRequest->setOrderAmount($fOrderAmount);

        $oTransactionRequest->setIssuerId($sIssuerId);
        $oTransactionRequest->setEntranceCode($sEntranceCode);

        if(defined('IDEAL_RETURN_URL') && IDEAL_RETURN_URL)
        {
            // Gebruik URL uit configuratie bestand
        }
        else
        {
            // De URL waar de bezoeker naar toe wordt gestuurd nadat de ideal betaling is afgerond (of bij fouten)
            $sCurrentUrl = strtolower(substr($_SERVER['SERVER_PROTOCOL'], 0, strpos($_SERVER['SERVER_PROTOCOL'], '/')) . '://' . $_SERVER['SERVER_NAME'] . '/') . substr($_SERVER['SCRIPT_NAME'], 1);
            $sReturnUrl = substr($sCurrentUrl, 0, strrpos($sCurrentUrl, '/') + 1) . 'ideal/index/return';

            $oTransactionRequest->setReturnUrl($sReturnUrl);
        }

        $sUrl = $oTransactionRequest->sReturnUrl;

        if (!empty($params['redirectLink'])) {
            $oTransactionRequest->setReturnUrl($sUrl . '/redirect/' . $params['redirectLink']);
        }
        
        $sTransactionId = $oTransactionRequest->doRequest();

        if($oTransactionRequest->hasErrors())
        {
            throw new \OSDN_Exception($oTransactionRequest->errorsToString());
        }
        else
        {

            try {

                $params = array();
                $params['transactionId'] = $oTransactionRequest->sTransactionId;
                $params['merchantId'] = $oTransactionRequest->sMerchantId;
                $params['description'] = $oTransactionRequest->sOrderDescription;
                $params['cartId'] = $oTransactionRequest->sOrderId;
                $params['amount'] = $oTransactionRequest->iOrderAmount;
                $params['status'] = 'PENDING';
                $params['entranceCode'] = $oTransactionRequest->sEntranceCode;
                $params['redirectSuccess'] = $oTransactionRequest->sReturnUrl;
                $params['redirectError'] = $oTransactionRequest->sReturnUrl;
                $params['issuerAuthenticationUrl'] = $oTransactionRequest->sTransactionUrl;

                $transactionRow = $this->create($params);
                
                $oTransactionRequest->doTransaction();

            } catch (OSDN_Exception $e) {
                throw $e;
            } catch(\Exception $e) {
                throw new \OSDN_Exception('Unable to save transaction');
            }
            
        }
        
    }

    public function getIssuers()
    {
        // Set default timezone for DATE/TIME functions
        if(function_exists('date_default_timezone_set'))
        {
            date_default_timezone_set('Europe/Amsterdam');
        }

        $oIssuerRequest = new \Ideal_Service_IssuerRequest();


        $aIssuerList = $oIssuerRequest->doRequest();

        if($oIssuerRequest->hasErrors())
        {
            throw new \OSDN_Exception($oIssuerRequest->errorsToString());
        }
        else
        {
            return $aIssuerList;
        }
    }

    /**
     * @param array $data
     */
    public function create(array $data)
    {
        $f = $this->_validate($data);
        $row = $this->_table->createRow($f->getData());
        $row->save();

        return $row;
    }

    /**
     * @param $id
     * @param array $data
     * @return bool
     * @throws OSDN_Exception
     */
    public function update(\Ideal_Model_IdealTransactionRow $transactionRow, array $data)
    {

        $f = $this->_validate($data, 'update');
        $this->getAdapter()->beginTransaction();
        try {
            $transactionRow->setFromArray($f->getData());
            $transactionRow->save();

            $cartService = new \Cart_Service_Cart();
            $cartRow = $cartService->find($transactionRow->cartId);
            $cartRow = $cartService->update($cartRow, array(
                'status' => $transactionRow->status
            ));

            $this->getAdapter()->commit();

            if (\Cart_Model_CartRow::KIND_MEMBERSHIP === $cartRow->kind && self::SUCCESS === $cartRow->status ) {
                $companyService = new \Company_Service_Company();
                $companyRow = $companyService->find($cartRow->companyId);
                $membershipService = new \Membership_Service_MembershipCompany($companyRow);
                $membershipCompany = $membershipService->updateSubscribe(array(
                    'membershipId' => $cartRow->itemId
                ));
            }
            if (\Cart_Model_CartRow::KIND_SUPPORTMINUTES === $cartRow->kind && self::SUCCESS === $cartRow->status ) {
                $companyService = new \Company_Service_Company();
                $companyRow = $companyService->find($cartRow->companyId);

                $supportMinutesService = new \SupportTicket_Service_SupportMinutes();
                $supportMinutesRow = $supportMinutesService->find($cartRow->itemId);

                $companyService->addSupportMinutes($companyRow, $supportMinutesRow);
            }


        } catch(\OSDN_Exception $e) {
            $this->getAdapter()->rollBack();
            throw $e;
        } catch(\Exception $e) {
            $this->getAdapter()->rollBack();
            throw new \OSDN_Exception('Unable to update');
        }

        return $transactionRow;
    }

    /**
     * @param $id
     * @param bool $throwException
     * @throws OSDN_Exception
     */
    public function findByParams($sTransactionId, $sEntranceCode, $throwException = true)
    {
        $row = $this->_table->fetchRow(array(
            'transactionId = ?' => $sTransactionId,
            'entranceCode = ?' => $sEntranceCode
        ));
        if (null === $row && true === $throwException) {
            throw new \OSDN_Exception('Unable to find row #' . $id);
        }

        return $row;
    }



}

