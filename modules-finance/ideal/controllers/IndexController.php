<?php

class Ideal_IndexController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    protected $_service;

    public function init()
    {

        $this->_service = new \Ideal_Service_Ideal();

        parent::init();

        $this->_helper->contextSwitch()
             ->initContext();


    }

    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Resource_Interface::getResourceId()
     */
    public function getResourceId()
    {
        return 'ideal:payment';
    }

    public function indexAction()
    {}

    public function returnAction()
    {

        // Set default timezone for DATE/TIME functions
        if(function_exists('date_default_timezone_set'))
        {
            date_default_timezone_set('Europe/Amsterdam');
        }

        // Zoek het TransactieID en EntranceCode op in $_GET
        $sTransactionId = $_GET['trxid'];
        $sEntranceCode = $_GET['ec'];
        $returnUrl = $this->_getParam('redirect');

        // Controlleer of de TransactieID/EntranceCode combinatie bestaat in de database, en zoek de bijbehorende order informatie op
        if(true) // Transactie bestaat/geldig
        {
            $oStatusRequest = new \Ideal_Service_StatusRequest();

            // Het TransactionID van de transactie die je wilt controleren.
            $oStatusRequest->setTransactionId($sTransactionId);

            // Vraag de status van de transactie op (SUCCESS, CANCELLED, FAILURE, OPEN, EXPIRED)
            $sStatus = $oStatusRequest->doRequest();

            // Is er een fout opgetreden?
            if($oStatusRequest->hasErrors())
            {
                $this->view->success = false;
                $this->_helper->information($oStatusRequest->errorsToString());
            }
            else
            {

                try {

                    $params = array();
                    $params['name'] = \iconv('iso-8859-1', 'utf-8', $oStatusRequest->sAccountName);
                    $params['city'] = \iconv('iso-8859-1', 'utf-8', $oStatusRequest->sAccountCity);
                    $params['accountId'] = \iconv('iso-8859-1', 'utf-8', $oStatusRequest->sAccountNumber);
                    $params['status'] = \iconv('iso-8859-1', 'utf-8', $oStatusRequest->sTransactionStatus);

                    $transactionRow = $this->_service->findByParams($sTransactionId, $sEntranceCode);
                    $transactionRow = $this->_service->update($transactionRow, $params);

                    $params['returnUrl'] = \base64_decode($returnUrl);

                    $this->view->assign($params);

                } catch (OSDN_Exception $e) {
                    $this->view->success = false;
                    $this->_helper->information($e->getMessages());
                } catch(\Exception $e) {
                    $this->view->success = false;
                    $this->_helper->information('Unable to update transaction');
                }

            }
        }

        if (!empty($returnUrl)) {
            $this->_redirect(\base64_decode($returnUrl));
        }
    }

}