<?php

class Ideal_View_Helper_BankComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    public function bankComboBox()
    {

        $service = new \Ideal_Service_Ideal();
        $banks = $service->getIssuers();

        $options = array();
        if (!empty($banks) && \is_array($banks)) {
            foreach ($banks as $key => $value) {
                $this->_options[$key] = $value;
            }
        }
        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }
}