<?php

/**
 * Description
 *
 * @category
 * @author     Kudrinsky Vitaly <vkudrinsky@gmail.com>
 * @version    SVN: $Id: $
 */

/**
 * Generating Private key
 *
 * genrsa -des3 -out testapp.key -passout pass:TestAppKeyPass 1024
 * req -x509 -new -key testapp.key -passin pass:TestAppKeyPass -days 3650 -out testapp.cer
 */

// Use Rabobank, ABN Amro, ING Bank or Simulator
define('IDEAL_AQUIRER', 'ing');

// Use FALSE for NOCACHE, make sure PATH and inner files are writable (This folder should not be accessable for webusers)
define('IDEAL_CACHE_PATH', __DIR__ . '/cache/');

// Your unique iDEAL Merchant ID
define('IDEAL_MERCHANT_ID', '005039417');

// Name of your private certificate file (should be located in IDEAL_SECURE_PATH)
define('IDEAL_PRIVATE_CERTIFICATE_FILE', 'testapp.cer');

// Password used to generate private key file
define('IDEAL_PRIVATE_KEY', 'TestAppKeyPass');

// Name of your private certificate file (should be located in IDEAL_SECURE_PATH)
define('IDEAL_PRIVATE_KEY_FILE', 'testapp.key');

// Default return URL after transaction (Usualy overridden by script)
define('IDEAL_RETURN_URL', '');

// Path to your private key & certificate files (This folder should not be accessable for webusers)
define('IDEAL_SECURE_PATH', dirname(__FILE__) . '/ssl/');

// Your iDEAL Sub ID
define('IDEAL_SUB_ID', '0');

// Use TEST/LIVE mode; true=TEST, false=LIVE
define('IDEAL_TEST_MODE', true);

?>
