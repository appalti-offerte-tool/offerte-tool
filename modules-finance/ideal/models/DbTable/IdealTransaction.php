<?php

class Ideal_Model_DbTable_IdealTransaction extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'idealTransaction';

    protected $_rowClass = '\\Ideal_Model_IdealTransactionRow';

    protected $_nullableFields = array();

    protected $_referenceMap    = array(
        'Cart'    => array(
            'columns'       => 'cartId',
            'refTableClass' => 'Cart_Model_DbTable_Cart',
            'refColumns'    => 'id'
        ),
        'Account'    => array(
            'columns'       => 'accountId',
            'refTableClass' => 'Account_Model_DbTable_Account',
            'refColumns'    => 'id'
        )
    );

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {
        if (empty($data['createdDatetime'])) {
            $dt = new \DateTime();
            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        $data['accountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        return parent::insert($data);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {

        if (empty($data['updatedDatetime'])) {
            $dt = new \DateTime();
            $data['updatedDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        return parent::update($data, $where);
    }
}