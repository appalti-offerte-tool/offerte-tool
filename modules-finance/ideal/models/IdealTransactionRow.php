<?php

class Ideal_Model_IdealTransactionRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{
    /**
     * @var Account_Model_DbTable_AccountRow
     */
    protected $_accountRow;

    /**
     * @var Cart_Model_DbTable_CartRow
     */
    protected $_cartRow;

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Account_Model_DbTable_AccountRow
     */
    public function getAccountRow()
    {
        $this->_accountRow = $this->findParentRow('Account_Model_DbTable_Account', 'Account');

        return $this->_accountRow;
    }

    /**
     * @return Account_Model_DbTable_AccountRow
     */
    public function getCartRow()
    {
        $this->_cartRow = $this->findParentRow('Cart_Model_DbTable_Cart', 'Cart');

        return $this->_cartRow;
    }

}