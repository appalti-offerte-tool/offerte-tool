<?php

class Cart_Model_CartRow extends \Zend_Db_Table_Row_Abstract implements \OSDN_Application_Model_Interface
{

    const KIND_MEMBERSHIP = 'membership';
    const KIND_SUPPORTMINUTES = 'support-minutes';

    const STATUS_SUCCESS = 'SUCCESS';

    /**
     * (non-PHPdoc)
     * @see OSDN_Application_Model_Interface::getId()
     */
    public function getId()
    {
        return $this->id;
    }
}