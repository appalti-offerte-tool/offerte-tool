<?php

class Cart_Model_DbTable_PaymentMethod extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'paymentMethod';

    protected $_rowClass = '\\Cart_Model_PaymentMethodRow';

}