<?php

class Cart_Model_DbTable_Cart extends \OSDN_Db_Table_Abstract
{
    protected $_primary = 'id';

    protected $_name = 'cart';

    protected $_rowClass = '\\Cart_Model_CartRow';

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::insert()
     */
    public function insert(array $data)
    {

        if(function_exists('date_default_timezone_set'))
        {
            date_default_timezone_set('Europe/Amsterdam');
        }

        if (empty($data['createdDatetime'])) {
            $dt = new \DateTime();
            $data['createdDatetime'] = $dt->format('Y-m-d H:i:s');
        }

        $data['methodId'] = \Cart_Model_PaymentMethodRow::IDEAL;
        $data['accountId'] = Zend_Auth::getInstance()->getIdentity()->getId();

        return parent::insert($data);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_Db_Table_Abstract::update()
     */
    public function update(array $data, $where)
    {
        /**
         * Omit changing company owner
         */
        unset($data['accountId']);
        $dt = new \DateTime();
        $data['updatedDatetime'] = $dt->format('Y-m-d H:i:s');

        return parent::update($data, $where);
    }


}