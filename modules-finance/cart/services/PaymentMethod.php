<?php

class Cart_Service_PaymentMethod extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Cart_Model_DbTable_PaymentMethod
     */
    protected $_table;

    public function __construct()
    {
        $this->_table = new \Cart_Model_DbTable_PaymentMethod($this->getAdapter());

        parent::__construct();
    }

    public function fetchAll()
    {

        $select = $this->getAdapter()->select()
            ->from($this->_table->getTableName());

        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

    /**
     * @param $id
     * @param bool $throwException
     * @throws OSDN_Exception
     * @return Company_Model_ContactPersonRow
     */
    public function find($id, $throwException = true)
    {
        $paymentMethodRow = $this->_table->findOne($id);
        if (null === $paymentMethodRow && true === $throwException) {
            throw new \OSDN_Exception('Unable to find row #' . $id);
        }

        return $paymentMethodRow;
    }

}
