<?php

class Cart_Service_Cart extends \OSDN_Application_Service_Dbable
{
    /**
     * @var Company_Model_DbTable_Offer
     */
    protected $_table;

    protected function _init()
    {
        $this->_table = new \Cart_Model_DbTable_Cart($this->getAdapter());

        $this->_attachValidationRules('default', array(
            'companyId' => array('allowEmpty' => false, 'presence' => 'required'),
            'itemId' => array('allowEmpty' => false, 'presence' => 'required'),
            'kind' => array('allowEmpty' => false, 'presence' => 'required'),
            'price' => array('allowEmpty' => false, 'presence' => 'required'),
        ));

        $this->_attachValidationRules('update', array(
            'companyId' => array('allowEmpty' => false, 'presence' => null),
            'itemId' => array('allowEmpty' => false, 'presence' => null),
            'kind' => array('allowEmpty' => false, 'presence' => null),
            'price' => array('allowEmpty' => false, 'presence' => null),
            'status' => array('allowEmpty' => false, 'presence' => 'required'),
        ), 'default');

        parent::_init();
    }

    /**
     * @param $id
     * @param bool $throwException
     * @throws OSDN_Exception
     */
    public function find($id, $throwException = true)
    {
        $row = $this->_table->findOne($id);
        if (null === $row && true === $throwException) {
            throw new \OSDN_Exception('Unable to find row #' . $id);
        }

        return $row;
    }

    /**
     * @param array $data
     */
    public function create(array $data)
    {

        unset($data['price']);

        if (\Cart_Model_CartRow::KIND_MEMBERSHIP === $data['kind']) {
            $membershipService = new \Membership_Service_Membership();
            $membership = $membershipService->find($data['itemId']);
            $data['price'] = $membership->price;
        }
        if (\Cart_Model_CartRow::KIND_SUPPORTMINUTES === $data['kind']) {
            $supportMinutesService = new \SupportTicket_Service_SupportMinutes();
            $supportMinutes = $supportMinutesService->find($data['itemId']);
            $data['price'] = $supportMinutes->price;
        }

        $f = $this->_validate($data);
        $row = $this->_table->createRow($f->getData());
        $row->save();

        return $row;
    }

    /**
     * @param $id
     * @param array $data
     * @return bool
     * @throws OSDN_Exception
     */
    public function update(\Cart_Model_CartRow $cartRow, array $data)
    {

        $f = $this->_validate($data, 'update');
        try {
            $cartRow->setFromArray($f->getData());
            $cartRow->save();
        } catch(\OSDN_Exception $e) {
            throw $e;
        } catch(\Exception $e) {
            throw new \OSDN_Exception('Unable to update');
        }

        return $cartRow;
    }

    public function fetchAllByCompanyWithResponse(\Company_Model_CompanyRow $companyRow, array $params = array())
    {

        $select = $this->getAdapter()->select()
            ->from(
                array('cart' => 'cart')
            )
            ->joinLeft (
            array ('trans'=>'idealTransaction'),
            'trans.cartId = cart.id',
            array('accuierName'=>'name', 'accuierCity'=>'city', 'transDesc' => 'description')
            )

            ->where('companyId = ?', $companyRow->getId(), Zend_Db::INT_TYPE);

        $this->_initDbFilter($select, $this->_table, array('trans.description' => 'transDesc'))->parse($params);
        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

    public function fetchAllWithResponse(array $params = array())
    {

        $select = $this->getAdapter()->select()
            ->from(
                array('cart' => 'cart')
            )
            ->joinLeft(
                array ('trans'=>'idealTransaction'),
                'trans.cartId = cart.id',
                array('accuierName'=>'name', 'accuierCity'=>'city')
            )
            ->join(
                array ('c'=>'company'),
                'cart.companyId = c.id',
                array(
                    'companyId' => 'c.id',
                    'companyName'=> new Zend_Db_Expr('CONCAT(c.name, " ", c.form)')
                )
            );

        $this->_initDbFilter($select, $this->_table)->parse($params);

        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

    public function fetchLastOrdersByCompany(\Company_Model_CompanyRow $companyRow)
    {

        $select = $this->getAdapter()->select()
            ->from(
                array('cart' => 'cart'),
                array('updatedDatetime','description','price')
            )
            ->joinLeft (
                array ('trans'=>'idealTransaction'),
                'trans.cartId = cart.id',
                array('accuierName'=>'name')
            )

            ->where('cart.companyId = ?', $companyRow->getId(), Zend_Db::INT_TYPE)
            ->where('cart.status = ?', \Cart_Model_CartRow::STATUS_SUCCESS)
            ->order('cart.updatedDatetime DESC')
            ->limit(5);

        $response = $this->getDecorator('response')->decorate($select);
        return $response;
    }

}