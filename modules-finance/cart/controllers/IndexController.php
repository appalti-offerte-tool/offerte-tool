<?php

class Cart_IndexController extends \Zend_Controller_Action implements \Zend_Acl_Resource_Interface
{
    /**
     * @var \Cart_Service_Cart
     */
    protected $_service;

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        parent::init();

        /* Module temporary disabled */
        $this->_helper->information('Wrong request.');
        $this->_redirect('/');

        $this->_service = new \Cart_Service_Cart();
    }

    /**
    * (non-PHPdoc)
    * @see Zend_Acl_Resource_Interface::getResourceId()
    */
    public function getResourceId()
    {
        return 'cart:payment';
    }

    public function indexAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(20);

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $companyRow = $accountRow->getCompanyRow();

        if ($accountRow->isCompanyOwner() && !$companyRow->isMembershipValid()) {
            $this->_helper->information('Your membership expired. Please buy another one.', 0, E_USER_WARNING);
        }

        $response = $this->_service->fetchAllByCompanyWithResponse($companyRow, $this->_getAllParams());
        $this->view->assign($response->toArray());

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }

    public function summaryAction()
    {
        $pagination = new OSDN_Pagination();
        $pagination->setItemsCountPerPage(20);

        $response = $this->_service->fetchAllWithResponse($this->_getAllParams());
        $this->view->assign($response->toArray());

        $pagination->setTotalCount($response->count());
        $this->view->pagination = $pagination;
    }

    public function createAction()
    {
        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $companyRow = $accountRow->getCompanyRow();

        $redirectEncoded = $this->_getParam('redirect');
        $redirectLink = empty($redirectEncoded) ? '/cart/index' : \base64_decode($redirectEncoded);

        if (!$this->getRequest()->isPost()) {

            $this->view->kind = $this->_getParam('kind');
            $this->view->itemId = $this->_getParam('itemId');

            if ($this->view->kind === \Cart_Model_CartRow::KIND_MEMBERSHIP) {
                $this->view->validMembership = $companyRow->isMembershipValid();
            }

            $this->view->redirectEncoded = $redirectEncoded;
            $this->view->methodId = Cart_Model_PaymentMethodRow::IDEAL;
        } else {
            try {
                $this->view->formSubmittedPostData = $this->getRequest()->getParams();
                $params = $this->_getAllParams();
                $params['companyId'] = $companyRow->getId();

                $cartRow = $this->_service->create($params);

                $this->view->cartId = $cartRow->getId();
                $this->view->success = true;
                $idealService = new Ideal_Service_Ideal();

                $idealService->request(array(
                    'order_id' => $cartRow->getId(),
                    'order_description' => $cartRow->description,
                    'order_amount' => $cartRow->price,
                    'order_issuer' => $params['issuerId'],
                    'redirectLink' => $params['redirectEncoded']
                ));

                $this->_redirect('cart');
            } catch (OSDN_Exception $e) {
                $this->view->success = false;
                $this->_helper->information($e->getMessages());
                $this->_redirect($redirectLink);
            } catch(\Exception $e) {
                $this->view->success = false;
                $this->_helper->information('Unable to create');
                $this->_redirect($redirectLink);
            }
        }


        $this->render('form');
    }

}