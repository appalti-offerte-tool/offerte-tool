<?php

class Cart_View_Helper_PaymentMethodComboBox extends \Zend_View_Helper_Abstract
{
    /**
     * Contain value/pairs comment types
     *
     * @var $_options array
     */
    protected $_options = array();

    public function paymentMethodComboBox()
    {

        $service = new Cart_Service_PaymentMethod();
        $response = $service->fetchAll();

        $options = array();
        $response->getRowset(function($row) use (& $options) {
            $options[$row['id']] = $row['name'];
            return false;
        });
        $this->_options = $options;

        return $this;
    }

    public function toField($name, $value = null, $attribs = null)
    {
        return $this->view->formSelect($name, $value, $attribs, $this->_options);
    }
}