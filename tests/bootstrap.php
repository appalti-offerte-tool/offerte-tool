<?php

define('SILENT_MODE', true);

define('APPLICATION_ENV', 'testing');
$application = require_once __DIR__ . '/../index.php';
if (! $application instanceof Zend_Application) {
    throw new Zend_Application_Exception('The "$application" should be instance of Zend_Application');
}

$application->bootstrap();