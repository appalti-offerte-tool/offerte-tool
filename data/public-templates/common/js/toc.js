function LinkTo(Tgt) {
    var InA = document.createElement('a');
    InA.setAttribute('href', '#' + Tgt);

    return InA;
}

function TocItem(Tgt) {
    var InLI = document.createElement('li');
    InLI.appendChild(LinkTo(Tgt));

    return InLI;
}

function replaceHeader(target, newHeaderLevel, id) {
    var h = document.createElement('h' + newHeaderLevel);

    h.setAttribute('id', id);
    h.appendChild(target.firstChild);
    target.parentNode.replaceChild(h, target);
}

function normalizeHeaders(source) {

    var tocHeaders = [],
        currentLevel = null,
        rootLevel = 1,
        i, len, id;

    var collectHeaders = function(lSource) {
        switch (lSource.nodeName.toString().toUpperCase()) {
            case 'H1':
            case 'H2':
            case 'H3':
                tocHeaders.push(lSource);
                break;

            case 'H4':
            case 'H5':
            case 'H6':
                lSource.setAttribute('notoc', 'notoc');
                break;

            default  :
                break;
        }

        if (lSource.hasChildNodes()) {
            var tNode = lSource.firstChild;
            do {
                collectHeaders(tNode);
            } while (tNode = tNode.nextSibling);
        }
    }

    collectHeaders(source);

    for (i = 0, len = tocHeaders.length; i < len; i++) {
        if (tocHeaders[i].getAttribute('notoc')) {
            continue
        }

        currentLevel = parseInt(tocHeaders[i].nodeName.toString().toUpperCase().replace('H', ''));

        id = 'hh' + i;

        if (0 === i && currentLevel > 1) {
            replaceHeader(tocHeaders[i], 1, id);
        } else {
            tocHeaders[i].id = id;
            var gap = currentLevel - rootLevel;

            if (gap >= 1) {
                rootLevel++;
                replaceHeader(tocHeaders[i], rootLevel, id);
            }
        }
    }
}

function ToCwalk(Source) {

    normalizeHeaders(Source);

    var InOL = document.createElement('ol');
    var DStack = [];

    var Walker = function (lSource) {

        switch (lSource.nodeName.toString().toUpperCase()) {
            case 'H1':
                if (lSource.getAttribute('notoc') !== 'notoc') {
                    DStack.push({d:1, t:lSource.id});
                }
                break;
            case 'H2':
                if (lSource.getAttribute('notoc') !== 'notoc') {
                    DStack.push({d:2, t:lSource.id});
                }
                break;
            case 'H3':
                if (lSource.getAttribute('notoc') !== 'notoc') {
                    DStack.push({d:3, t:lSource.id});
                }
                break;
            case 'H4':
                if (lSource.getAttribute('notoc') !== 'notoc') {
                    DStack.push({d:4, t:lSource.id});
                }
                break;
            case 'H5':
                if (lSource.getAttribute('notoc') !== 'notoc') {
                    DStack.push({d:5, t:lSource.id});
                }
                break;
            case 'H6':
                if (lSource.getAttribute('notoc') !== 'notoc') {
                    DStack.push({d:6, t:lSource.id});
                }
                break;
            default  :
                break;
        }

        if (lSource.hasChildNodes()) {

            var tNode = lSource.firstChild;
            do {
                Walker(tNode);
            } while (tNode = tNode.nextSibling);

        }

    }

    Walker(Source);

    var curDepth = 1;
    var curStack = [InOL];
    var childStack = [];
    var lastChild = null;

    for (var i = 0, iC = DStack.length; i < iC; ++i) {

        var gap = DStack[i].d - curDepth;

        switch (gap) {

            case 0:
                lastChild = TocItem(DStack[i].t)
                curStack[curStack.length - 1].appendChild(lastChild);
                break;

            case 1:
                ++curDepth;
                childStack.push(lastChild);
                var newList = document.createElement('ol');
                curStack.push(newList);
                lastChild.appendChild(newList);
                lastChild = TocItem(DStack[i].t)
                curStack[curStack.length - 1].appendChild(lastChild);
                break;

            default:
                if (gap > 0) {
                    console && console.log("Header depth increased by more than one!");
                    var element = document.getElementById(DStack[i].t);
                    element && element.setAttribute('notoc', 'notoc');
                } else {
                    for (var z = 0; z > gap; --z) {
                        --curDepth;
                        childStack.pop();
                        curStack.pop();
                    }
                    lastChild = TocItem(DStack[i].t)
                    curStack[curStack.length - 1].appendChild(lastChild);
                }
                break;
        }
    }

    return InOL;
}

window.onload = function() {
    var ToC = ToCwalk(document.body);
    ToC.id = 'ToCol';
    document.getElementById('tocBlock').appendChild(ToC);
}
