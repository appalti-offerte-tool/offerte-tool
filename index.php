<?php

/**
 * Default entry point of the application
 *
 * !!! DO NOT CHANGE THIS FILE !!!
 */

$t = microtime(true);
$GLOBALS['T'] = $t;
error_reporting(E_ALL | E_STRICT);

defined('APPLICATION_PATH') || define('APPLICATION_PATH', __DIR__);
defined('APPLICATION_ENV') || define('APPLICATION_ENV', getenv('APPLICATION_ENV') ?: 'production');

set_include_path(join(PATH_SEPARATOR, array(
    __DIR__ . '/library',
    __DIR__ . '/data/cache',
    __DIR__ . '/data/proxies',
    get_include_path()
)));

umask(0);

require_once 'Zend/Application.php';
//echo APPLICATION_ENV .': '. APPLICATION_PATH.'<br/>';
//error_reporting(E_ALL|E_STRICT);
//ini_set('display_errors', 'on');
//require_once 'Zend/Loader/Autoloader.php';
//Zend_Loader_Autoloader::getInstance()->suppressNotFoundWarnings(false);

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);


if (!defined('SILENT_MODE') || true !== SILENT_MODE) {
	$bootstrap = $application->bootstrap();
	$bootstrap->run();
}

return $application;