/* toon alle klanten met thema's, beschikbaar thema's, bibliotheken en beschikbare bibliotheken */
SELECT
c.id,
c.name AS company,
'' as '-',
cp.name AS theme,
clp.name AS available,
pt.id AS style,
pcts.scope as template,
'' as '-',
l.name as library,
lib.name as available
FROM company c
/* company themes*/
LEFT JOIN proposalCustomTemplate cp ON cp.companyId = c.id
/* location available theme */
LEFT JOIN locationProposalcustomtemplate lp ON lp.locationId = c.id
LEFT JOIN proposalCustomTemplate clp ON clp.id = lp.proposalcustomtemplateId
LEFT JOIN proposalTheme pt ON pt.useCustomTemplate = clp.id
LEFT JOIN proposalCustomTemplateSettings pcts ON pcts.proposalCustomTemplateId=clp.id
/* company libraries */
LEFT JOIN library l ON l.companyId = c.id

/* company available libraries */
LEFT JOIN libraryProposalcustomtemplate libp ON libp.proposalcustomtemplateId = cp.id
LEFT JOIN library lib ON lib.id = libp.libraryId

WHERE TRUE
AND c.parentId IS NULL -- no relations
AND c.motherId IS NULL -- only main companie location


/* toon alle klanten met gebruikers en de aan de gebruikers toegewezen bedrijfslocaties en thema's */
SELECT
c.id,
c.name AS company,
a.username,
l.name,
p.name
FROM company c
/* company users */
LEFT JOIN contactPerson cp ON cp.companyId = c.id
LEFT JOIN account a ON a.id = cp.accountId
/* company user locations */
LEFT JOIN contactpersonLocation cpl ON cpl.contactpersonId = cp.id
LEFT JOIN company l ON l.id = cpl.locationId
/* company user location themes */
LEFT JOIN contactpersonProposalcustomtemplate cpp ON cpp.contactpersonId = cp.id
LEFT JOIN proposalCustomTemplate p ON p.id = cpp.proposalcustomtemplateId

WHERE TRUE
AND c.parentId IS NULL -- no relations
AND c.motherId IS NULL -- only main companie location


/* toon de offertes van gegeven bedrijf */
SELECT
c.id,
c.name as company,
a.username,
p.name as proposal,
pc.name as relation,
'' as '-',
pl.name as location,
pct.name as theme,
l.name as library
FROM proposal p
LEFT JOIN company c ON p.companyId = c.id
LEFT JOIN account a ON p.accountId = a.id
LEFT JOIN company pl ON pl.id = p.locationId
LEFT JOIN proposalTheme pt ON pt.proposalId = p.id
LEFT JOIN company pc ON pc.id = p.clientCompanyId
LEFT JOIN proposalCustomTemplate pct ON pct.id = pt.useCustomTemplate
LEFT JOIN libraryProposalcustomtemplate lpct ON lpct.proposalcustomtemplateId = pct.id
LEFT JOIN library l ON l.id = lpct.libraryId
WHERE TRUE
AND (c.id = 564) -- ArtDecco

/* toon company has company location has company theme has company library - user has user location, has user theme has library*/
SELECT
c.name as company,
lo.name as location,
t.name as theme,
li.name as library,
'' as '-',
cp.id,
CONCAT(cp.firstname,' ',cp.lastname),
cploc.name AS location,
cppct.name as theme
FROM company c
LEFT JOIN company lo ON (lo.motherId = c.id OR lo.id = c.id)
LEFT JOIN locationProposalcustomtemplate lot ON lot.locationId = lo.id
LEFT JOIN proposalCustomTemplate t ON t.id = lot.proposalcustomtemplateId
LEFT JOIN libraryProposalcustomtemplate lit ON lit.proposalcustomtemplateId = t.id
LEFT JOIN library li ON li.id = lit.libraryId

LEFT JOIN contactPerson cp ON cp.companyId = c.id
LEFT JOIN contactpersonLocation cplo ON (cplo.contactpersonId = cp.id AND cplo.locationId = lo.id)
LEFT JOIN company cploc ON cploc.id = cplo.locationId
LEFT JOIN contactpersonProposalcustomtemplate cpt ON (cpt.contactpersonId = cp.id AND cpt.proposalcustomtemplateId = t.id)
LEFT JOIN proposalCustomTemplate cppct ON cppct.id = cpt.proposalcustomtemplateId
WHERE TRUE
  AND c.parentId IS NULL
  AND c.motherId IS NULL
  AND c.id = 564
ORDER BY cp.id, cplo.locationId, cpt.proposalcustomtemplateId


/* toon klant gebruiker role function permissies */
SELECT
c.name as company,
CONCAT(cp.firstname,' ',cp.lastName) as Name,
a.username,
a.admin as beheerder,
ar.name as role,
ap.resource
FROM company c
LEFT JOIN contactPerson cp ON cp.companyId = c.id
LEFT JOIN account a ON a.id = cp.accountId
LEFT JOIN aclRole ar ON ar.id = a.roleId
LEFT JOIN aclPermission ap ON ap.roleId = ar.id
WHERE TRUE
  AND c.id = 564

  AND ap.resource IN (
    SELECT DISTINCT (resource)
    FROM `aclPermission`
    WHERE resource LIKE 'proposal:%'
  )

GROUP BY a.id,ar.id,ap.id
ORDER BY a.id


/* Toon alle active bedrijven met alle actieve contactpersonen en hun rol */
SELECT
  c.name as company,
  c.phone as telefoon,
  c.email as emailadres,
  '' as '-',
  a.username,
  TRIM(CONCAT(a.firstname,' ',a.prefix,' ',a.lastname)) as accountnaam,
  a.fullname as account_fullname,
  '' as '-',
  TRIM(CONCAT(cp.firstname,' ',cp.prefix,' ', cp.lastname)) as contactperson,
  cp.phone as telefoon,
  cp.mobile as mobiel,
  cp.email as emailadres,
  ar.name as rol
FROM company c
INNER JOIN contactPerson cp ON cp.companyId = c.id AND cp.isActive = 1
INNER JOIN account a ON a.id = cp.accountId AND a.isActive = 1
INNER JOIN aclRole ar ON ar.id = a.roleId
WHERE TRUE
  AND c.isActive = 1
  AND c.parentId IS NULL
ORDER BY c.id, a.id
LIMIT 1000

/* Toon user company, library, category, blocks */
SELECT
a.username as user,
c.name as company,
l.name as library,
pbc.name as category,
pb.name as block
FROM account a
LEFT JOIN contactPerson cp ON cp.accountId = a.id
LEFT JOIN company c ON c.id = cp.companyId
LEFT JOIN library l ON l.companyId = c.id
LEFT JOIN libraryProposalblock lpb ON lpb.libraryId = l.id
LEFT JOIN proposalBlock pb ON pb.id = lpb.proposalblockId
LEFT JOIN proposalBlockCategory pbc ON pbc.id = pb.categoryId
WHERE TRUE
AND a.username IN (
  'edward@appalti-software.nl',
  'edward@appalti.nl',
  'edward@lanenga.net'
)
ORDER BY c.id, l.id, pbc.id, pb.id
LIMIT 1000


// toont company library pcat ccat items
SELECT
	c.id,
	c.name as company,
	l.id,
	l.name as library,
	'',
	pbc.id,
	pbc.companyId,
	pbc.libraryId,
	IF (pbc.childName IS NULL, pbc.parentName,CONCAT(pbc.parentName,' | ',pbc.childName) ) as category,
	'',
	pb.id,
	lpb.libraryId,
	pb.name as item,
	'',
	(l.id = pbc.libraryId) AS lib_cat,
	(l.id = lpb.libraryId) AS lib_block,
	''
FROM company c
LEFT JOIN library l ON l.companyId = c.id
LEFT JOIN libraryProposalblock lpb ON lpb.libraryId = l.id
LEFT JOIN proposalBlock pb ON pb.id = lpb.proposalblockId
LEFT JOIN (
	SELECT
		pbc.id, 
		pbc.id as parentId,
		pbc.companyId,
		pbc.libraryId,
		pbc.name as parentName,
		NULL as childName
	FROM proposalBlockCategory pbc
	WHERE pbc.companyId IS NULL
	UNION
	SELECT
		pbc.id, 
		pbc.parentId,
		pbc.companyId,
		pbc.libraryId,
		ppbc.name as parentName,
		pbc.name as childName
	FROM proposalBlockCategory pbc
	LEFT JOIN proposalBlockCategory ppbc ON pbc.parentId = ppbc.id
	WHERE pbc.companyId =112
) pbc ON pbc.id = pb.categoryId
WHERE TRUE
AND c.id = 112
/**
AND (FALSE
  OR (l.id != pbc.libraryId)
  OR (l.id != lpb.libraryId)
)
/**/
ORDER BY c.id ASC, l.id ASC, pbc.parentId ASC, pbc.id ASC, pb.id ASC
LIMIT 0,100000

