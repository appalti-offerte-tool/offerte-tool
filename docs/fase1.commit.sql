/* ************************************************************************* */
/*  APPALTI SOFTWARE                                                         */
/*  OFFERTE TOOL AANPASSINGEN                                                */
/*  FASE 1 - MYSQL                                                           */
/*  MUTATIE SQL FILE                                                         */
/* ************************************************************************* */

--
-- CREATE NEW TABLES FIRST
--

CREATE TABLE IF NOT EXISTS `companyRegion` (
  `companyId` int(10) unsigned NOT NULL,
  `regionId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`companyId`,`regionId`),
  KEY `regionId` (`regionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `contactpersonProposalcustomtemplate` (
  `contactpersonId` int(10) unsigned NOT NULL,
  `proposalcustomtemplateId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`contactpersonId`,`proposalcustomtemplateId`),
  KEY `fk__proposalcustomtemplateId` (`proposalcustomtemplateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `contactpersonLocation` (
  `contactpersonId` int(10) unsigned NOT NULL,
  `locationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`contactpersonId`,`locationId`),
  KEY `fk_contactpersonlocation_locationId` (`locationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `library` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `companyId` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `isActive` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_library_companyId` (`companyId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

CREATE TABLE IF NOT EXISTS `libraryProposalblock` (
  `libraryId` int(10) unsigned NOT NULL,
  `proposalblockId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`libraryId`,`proposalblockId`),
  KEY `fk_libraryproposalblock_proposalblockId` (`proposalblockId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `libraryProposalcustomtemplate` (
  `libraryId` int(10) unsigned NOT NULL,
  `proposalcustomtemplateId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`libraryId`,`proposalcustomtemplateId`),
  KEY `fk_libraryproposalcustomtemplate_proposalcustomtemplateId` (`proposalcustomtemplateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `locationLibrary` (
  `locationId` int(10) unsigned NOT NULL,
  `libraryId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`locationId`,`libraryId`),
  KEY `fk_locationlibrary_libraryId` (`libraryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `locationProposalcustomtemplate` (
  `locationId` int(10) unsigned NOT NULL,
  `proposalcustomtemplateId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`locationId`,`proposalcustomtemplateId`),
  KEY `fk_locationproposalcustomtemplate_proposalcustomtemplateId` (`proposalcustomtemplateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `region` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `companyId` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;


--
-- ALTER EXISTING TABLES COLUMNS AND INDEXES
--

ALTER TABLE `account`
	ADD COLUMN `admin` int(1) unsigned NOT NULL DEFAULT '0',
    ADD COLUMN `signupData` MEDIUMTEXT NOT NULL;

ALTER TABLE `aclRole`
	ADD COLUMN `companyId` int(11) unsigned NOT NULL DEFAULT '3' AFTER `id`,
	DROP INDEX `name`,
	ADD UNIQUE KEY `u_companyId_name` (`companyId`,`name`);

ALTER TABLE `company`
	ADD COLUMN `motherId` int(11) unsigned DEFAULT NULL AFTER `id`;

ALTER TABLE `proposal`
	MODIFY COLUMN `clientCompanyId` int(11) unsigned DEFAULT NULL,
	MODIFY COLUMN `clientContactPersonId` int(11) unsigned DEFAULT NULL,
	ADD COLUMN `locationId` int(11) unsigned NOT NULL,
	ADD COLUMN `proposalCustomTemplateId` int(11) unsigned NOT NULL;

ALTER TABLE `proposalBlockCategory`
	ADD COLUMN `libraryId` int(10) unsigned DEFAULT NULL AFTER `parentId`,
	ADD KEY `i_companyId_libraryId` (`companyId`,`libraryId`);

ALTER TABLE `proposalCustomTemplate`
	ADD COLUMN `name` varchar(255) NOT NULL DEFAULT 'Standaard',
	ADD COLUMN `description` mediumtext NOT NULL,
	ADD COLUMN `active` int(1) unsigned NOT NULL DEFAULT '1';

ALTER TABLE `proposalTheme`
	MODIFY COLUMN `companyId` int(11) unsigned NOT NULL AFTER `id`,
	MODIFY COLUMN `useCustomTemplate` int(11) DEFAULT NULL AFTER `companyId`;


--
-- SET FOREIGN KEYS
--

ALTER TABLE `aclPermission` DROP FOREIGN KEY `aclPermission_ibfk_1` ;
ALTER TABLE `aclPermission` ADD FOREIGN KEY (`roleId`) REFERENCES `aclRole` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `aclRole`
--  ADD CONSTRAINT `aclRole_ibfk_1` FOREIGN KEY (`parentId`) REFERENCES `aclRole` (`id`),
  ADD CONSTRAINT `fk_companyId` FOREIGN KEY (`companyId`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `company`
	ADD CONSTRAINT `fk_company_motherId` FOREIGN KEY (`motherId`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `companyRegion`
  ADD CONSTRAINT `companyregion_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `companyregion_ibfk_2` FOREIGN KEY (`regionId`) REFERENCES `region` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `contactpersonLocation`
	ADD CONSTRAINT `fk_contactpersonlocation_contactpersonId` FOREIGN KEY (`contactpersonId`) REFERENCES `contactPerson` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `fk_contactpersonlocation_locationId` FOREIGN KEY (`locationId`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `contactpersonProposalcustomtemplate`
	ADD CONSTRAINT `fk__contactpersonId` FOREIGN KEY (`contactpersonId`) REFERENCES `contactPerson` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `contactpersonProposalcustomtemplate`
	ADD CONSTRAINT `fk__proposalcustomtemplateId` FOREIGN KEY (`proposalcustomtemplateId`) REFERENCES `proposalCustomTemplate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `library`
	ADD CONSTRAINT `fk_library_companyId` FOREIGN KEY (`companyId`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `libraryProposalblock`
	ADD CONSTRAINT `fk_libraryproposalblock_libraryId` FOREIGN KEY (`libraryId`) REFERENCES `library` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `fk_libraryproposalblock_proposalblockId` FOREIGN KEY (`proposalblockId`) REFERENCES `proposalBlock` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `libraryProposalcustomtemplate`
	ADD CONSTRAINT `fk_libraryproposalcustomtemplate_libraryId` FOREIGN KEY (`libraryId`) REFERENCES `library` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `fk_libraryproposalcustomtemplate_proposalcustomtemplateId` FOREIGN KEY (`proposalcustomtemplateId`) REFERENCES `proposalCustomTemplate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `locationLibrary`
	ADD CONSTRAINT `fk_locationlibrary_locationId` FOREIGN KEY (`locationId`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `fk_locationlibrary_libraryId` FOREIGN KEY (`libraryId`) REFERENCES `library` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `locationProposalcustomtemplate`
	ADD CONSTRAINT `fk_locationproposalcustomtemplate_companyId` FOREIGN KEY (`locationId`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `fk_locationproposalcustomtemplate_proposalcustomtemplateId` FOREIGN KEY (`proposalcustomtemplateId`) REFERENCES `proposalCustomTemplate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `proposalBlockCategory`
	ADD CONSTRAINT `fk_libraryId` FOREIGN KEY (`libraryId`) REFERENCES `library` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `region`
	ADD CONSTRAINT `region_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- insert translations for rights
--
INSERT INTO `translation` (`id`,`caption`,`en`,`nl`) VALUES
(NULL, 'dfaccf3c1f33f6ad112d7c164c466e7a','Access to all proposals','Toegang tot alle offertes'),
(NULL, 'd40d85a6c68229b5761c3b92fbf60763','Modify proposals','Wijzigen offertes'),
(NULL, 'e11160c745a1703c0685ca5d3141e937','Edit texts','Bewerken van teksten'),
(NULL, '78d3f64319704ddf85896f3184bfd78e','Edit introduction','Bewerken inleiding'),
(NULL, '79d00c2c72a6d8f63161a69a3e488164','Edit pricesheet','Bewerken prijsblad'),
(NULL, 'aaf8d823fcf7a62124e2cf315a7a264d','Grant discount','Korting geven'),
(NULL, '1b1f60644ac10b3c039493a8da505cbf','Approve proposal','Offerte goedkeuren'),
(NULL, '57b965f3b3bcb68424a6f021bb788e5d','Management information','Management informatie');

--
-- add role for Appalti
--
INSERT INTO `aclRole` (`id`,`companyId`,`name`,`parentId`,`luid`) VALUES (NULL, 319, 'Appalti', NULL, NULL);
