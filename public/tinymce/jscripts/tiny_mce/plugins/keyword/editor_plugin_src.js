/**
 * editor_plugin.js
 *
 * Copyright 2012,
 * Released under LGPL License.
 *
 * License: free
 * Contributing: free
 */

(function() {
	tinymce.create('tinymce.plugins.KeywordPlugin', {

        ed: null,

		init : function(ed, url) {
            this.ed = ed;
		},

        getListBox: function(cm, paramName) {
            var self = this,
                keywords = self.ed.getParam(paramName),
                mlb = cm.createListBox(paramName, {
                    title : keywords[0].value,
                    onselect : function(v) {
                        v && self.ed.execCommand("mceInsertContent", false, v);
                    }
                });

            for (var i = 0, len = keywords.length; i < len; i++) {
                $.each(keywords[i].name, function(i, v) {
                    mlb.add(v, i);
                });
            }

            return mlb;
        },

        createControl: function(n, cm) {
            var control = null;

            switch (n) {
                case 'mykwd':
                    control = this.getListBox(cm, 'mykwd');
                    break;
                case 'clientkwd':
                    control = this.getListBox(cm, 'clientkwd');
                    break
            }

            return control;
        },

		getInfo : function() {
			return {
				longname : 'Keyword',
				author : 'Vasya',
				authorurl : 'http://google.com',
				infourl : 'http://google.com',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('keyword', tinymce.plugins.KeywordPlugin);
})();