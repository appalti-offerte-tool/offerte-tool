tinyMCEPopup.requireLangPack();

var jbImagesDialog = {

	resized : false,
	iframeOpened : false,
	timeoutStore : false,
    params: {},

    initMarkup: function() {
        this.infoBar = document.getElementById("upload_infobar");
        this.info = document.getElementById("upload_additional_info");
        this.formContainer = document.getElementById("upload_form_container");
        this.uploadInProgress = document.getElementById("upload_in_progress");
        this.uploadForm = document.getElementById('upl');
        this.uploadTarget = document.getElementById("upload_target");
    },

	init : function() {
        this.initMarkup();
		this.uploadTarget.src += '/' + tinyMCEPopup.getLang('jbimages_dlg.lang_id', 'dutch');
        this.params = tinyMCEPopup.editor.getParam('quickupload');
        this.uploadForm.action = this.params.action;

        var win = tinyMCEPopup.getWin();

        if (undefined !== this.params.itemName) {
            switch (this.params.itemName.toLowerCase()) {
                case 'block':
                    this.block = win.AppaltiTextBlock;
                    this.addTextBlockFields(false);
                    break;

                case 'blockdefinition':
                    this.block = win.AppaltiTextBlock;
                    this.addTextBlockFields(true);
                    break;

                case 'event':
                    this.block = win.EventText;
                    this.addArticleBlockFields();
                    break;

                case 'news':
                    this.block = win.NewsText;
                    this.addArticleBlockFields();
                    break;
            }
        }

    },

    createInput: function(attributes) {
        var input = document.createElement('input');
        for (var name in attributes) {
            input.setAttribute(name, attributes[name]);
        }

        return input;
    },

    addArticleBlockFields: function() {

    },

    addTextBlockFields: function(definition) {
        var input = document.createElement('input');
        this.uploadForm.appendChild(this.createInput({
            type: 'hidden',
            name: definition ? 'blockDefinitionId' : 'blockId',
            value: this.params.itemId
        }));

        this.uploadForm.appendChild(
            this.createInput({
                type: 'hidden',
                name: 'libraryId',
                value: this.block.libraryId
            })
        );

        if (!definition) {
            var categoryId = this.block.getCategoryId();
            if (categoryId) {
                this.uploadForm.appendChild(this.createInput({
                    type: 'hidden',
                    name: 'categoryId',
                    value: categoryId
                }));
            }

            this.uploadForm.appendChild(this.createInput({
                type: 'hidden',
                name: 'kind',
                value: 'text'
            }));
        }
    },

	inProgress : function() {
		this.infoBar.style.display = 'none';
        this.info.innerHTML = '';
        this.formContainer.style.display = 'none';
        this.uploadInProgress.style.display = 'block';
        var me = this;
		this.timeoutStore = window.setTimeout(function(){
            me.info.innerHTML = [
                tinyMCEPopup.getLang('jbimages_dlg.longer_than_usual', 0),
                '<br />',
                tinyMCEPopup.getLang('jbimages_dlg.maybe_an_error', 0),
                '<br />',
                '<a href="#" onClick="jbImagesDialog.showIframe()">',
                tinyMCEPopup.getLang('jbimages_dlg.view_output', 0),
                '</a>'
            ].join('');

		}, 20000);
	},

	showIframe : function() {
		if (this.iframeOpened == false) {
            this.uploadTarget.className = 'upload_target_visible';
			tinyMCEPopup.editor.windowManager.resizeBy(0, 150, tinyMCEPopup.id);
			this.iframeOpened = true;
		}
	},

	uploadFinish : function(result) {
        if (result.success) {
            this.infoBar.innerHTML = tinyMCEPopup.getLang('jbimages_dlg.upload_complete', 0);
            tinyMCEPopup.editor.execCommand('mceInsertContent', false, '<img src="' + result.src +'" alt="" />');

            if ('function' === typeof this.block.setUpdate) {
                this.block.setUpdate.call(this.block, result);
            }

            tinyMCEPopup.close();
        } else {
            window.clearTimeout(this.timeoutStore);
            this.infoBar.innerHTML = result.error;
            this.formContainer.style.display = 'block';
        }

        this.uploadInProgress.style.display = 'none';
        this.infoBar.style.display = 'block';

//        this.showIframe(); //Disable close and enable this for DEBUG
    }

};

tinyMCEPopup.onInit.add(jbImagesDialog.init, jbImagesDialog);
