tinyMCE.addI18n('nl.jbimages_dlg',{
	title : 'Upload een afbeelding van de computer',
	select_an_image : 'Selecteer een afbeelding',
	upload_in_progress : 'Upload in progress',
	upload_complete : 'Upload Compleet',
	upload : 'Upload',
	longer_than_usual : 'Dit is langer dan normaal te nemen.',
	maybe_an_error : 'Er kan een fout zijn opgetreden.',
	view_output : 'View script\'s output',

	lang_id : 'dutch' /* php-side language files are in: ci/application/language/{lang_id}; and in ci/system/language/{lang_id} */
});
