;(function($) {
    var ObservableForm = function(element, params) {
        $.extend(this, params || {});
        this.init(element)
    }

    function _valuesEqual(el) {
        return el.data('originalValue') == (el.is(':radio') || el.is(':checkbox') ? el.is(':checked') : el.val());
    }

    function _checkDirty(field) {
        var me = this, dirty = me.dirty,
            toCheck = field ? $(field) : me.formFields;

        toCheck.each(function() {
            me.dirty = !_valuesEqual($(this));
            return !me.dirty;
        });

        if (dirty !== me.dirty) {
            me.onDirtyStateChange.call(me.scope || me, me.getEl());
        }
    }

    function _saveInitialFormState() {
        var el;

        this.formFields = $(this.inputSelector, this.el).each(function() {
            el = $(this);
            el.data('originalValue', el.is(':radio') || el.is(':checkbox') ? el.is(':checked') : this.value);
        });

        this.dirty = false;
    }

    function _initEvents() {
        var me = this;
        me.el
        .off('.observable')
        .on('change.observable', me.inputSelector, function() {
            _checkDirty.call(me, this);
        });
    }

    ObservableForm.prototype = {

        constructor: ObservableForm,

        dirty: false,

        inputSelector: ':input:not(:button)',

        onDirtyStateChange: $.noop,

        scope: null,

        init: function(element) {
            this.el = $(element);
            _saveInitialFormState.call(this);
            _initEvents.call(this);
        },

        refresh: function() {
            _saveInitialFormState.call(this);
            this.onDirtyStateChange.call(this.scope || this, this.getEl());
        },

        isDirty: function(flag) {
            if (undefined !== flag) {
                this.dirty = flag;
            }

            return this.dirty;
        },

        getEl: function() {
            return this.el;
        },

        getField: function(name) {
            return this.formFields.filter('name="' + name + '"');
        }
    }

    $.fn.observe = function(params) {
        return this.each(function() {
            var el = $(this), data = el.data('observable');

            if (!data) {
                el.data('observable', new ObservableForm(this, params));
            }
        });
    }

    $.fn.observable = function() {
        return $(this).observe().data('observable');
    }

    $.fn.observe.Constructor = ObservableForm;

})(window.jQuery);