/* =========================================================
 * bootstrap-modal.js v2.0.3
 * http://twitter.github.com/bootstrap/javascript.html#modals
 * =========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */


!function ($) {

    "use strict"; // jshint ;_;


    /* MODAL CLASS DEFINITION
     * ====================== */

    var Modal = function (content, options) {
        this.options = options
        this.$element = $(content)
            .delegate('[data-dismiss="modal"]', 'click.dismiss.modal', $.proxy(this.hide, this))
    }

    Modal.prototype = {

        constructor:Modal, toggle:function () {
            return this[!this.isShown ? 'show' : 'hide']()
        }, show:function () {
            var that = this
                , e = $.Event('show')

            if (this.isShown || e.isDefaultPrevented()) return

            this.isShown = true

            escape.call(this)
            backdrop.call(this, function () {
                var transition = $.support.transition && that.$element.hasClass('fade')

                if (!that.$element.parent().length) {
                    that.$element.appendTo(document.body) //don't move modals dom position
                }

                that.$element.show()

                if (transition) {
                    that.$element[0].offsetWidth // force reflow
                }

                that.$element.addClass('modal-open in')

                transition ?
                    that.$element.one($.support.transition.end, function () {
                        that.$element.trigger('shown')
                    }) :
                    that.$element.trigger('shown')

            })

            if (this.$element.hasClass('fullscreen')) {
                fullscreen.call(this);
            } else {
                center.call(this);
            }

            this.$backdrop.resize($.proxy(center, that));

            var $form = this.$element.find('form.validatable');
            if ($form.length && undefined !== AppaltiLayout) {
                AppaltiLayout.initFormValidation($form);
                preventReload.call(that);
            }
        }, hide:function (e) {
            e && e.preventDefault()

            e = $.Event('hide')

            this.$element.trigger(e)

            if (!this.isShown || e.isDefaultPrevented()) return

            this.isShown = false

            escape.call(this)

            this.$element.removeClass('modal-open in')

            $.support.transition && this.$element.hasClass('fade') ?
                hideWithTransition.call(this) :
                hideModal.call(this)
        }

    }


    /* MODAL PRIVATE METHODS
     * ===================== */

    function preventReload() {
        var eventNS = '.Modal' + (new Date().getTime()),
            me = this;

        this.$element.data('eventNS', eventNS);

        $(document).off(eventNS).on('keydown' + eventNS, function(e) {
            var charCode = (e.which) ? e.which : e.keyCode;
            if (charCode === 116) {
                e.preventDefault();
                e.returnValue = false;
                e.keyCode = 0;
                me.reloadOnHide = true;
                me.$element.modal('hide');
            }
        })
    }

    function fullscreen() {
        var $wnd = $(window);

        this.$element.css({
            'top': 0,
            'left': 0,
            'overflowY': 'auto',
            'width' : $wnd.width() - 60,
            'height' : $wnd.height() - 60
        });

        var headerH = 0, footerH = 0;
        $('.modal-head > *', this.$element).each(function() {
            headerH += $(this).outerHeight(true);
        });

        $('.modal-foot > *', this.$element).each(function() {
            footerH += $(this).outerHeight(true);
        });

        $('.modal-content', this.$element).css({
            'height': this.$element.height() - headerH - footerH
        })

        $wnd.off('resize').resize($.proxy(fullscreen, this));
    }

    function center() {
        var $wnd = $(window),
            top = ($wnd.height() / 2) - (this.$element.outerHeight() / 2);

        this.$element.css({
            'top':(top > 0 ? top : 0) + 'px',
            'left':(($wnd.width() / 2) - this.$element.outerWidth() / 2) + 'px',
            'position':top > 0 ? 'fixed' : 'absolute'
        })
        .off('resize')
        .resize($.proxy(center, this));
    }

    function hideWithTransition() {
        var that = this
            , timeout = setTimeout(function () {
                that.$element.off($.support.transition.end)
                hideModal.call(that)
            }, 500)

        this.$element.one($.support.transition.end, function () {
            clearTimeout(timeout)
            hideModal.call(that)
        })
    }

    function hideModal(that) {
        $(window).off('resize');

        this.$element.data('eventNS') && $(document).off(this.$element.data('eventNS'));

        this.$element
            .hide()
            .off('resize')
            .removeAttr('style')
            .removeClass('fullscreen')
            .trigger('hidden');

        backdrop.call(this)

        this.reloadOnHide && window.location.reload();
    }

    function backdrop(callback) {
        var animate = this.$element.hasClass('fade') ? 'fade' : ''

        if (this.isShown && this.options.backdrop) {
            var doAnimate = $.support.transition && animate

            this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
                .appendTo(document.body)

            if (this.$element.outerHeight() < $(window).height()) {
                $('body').addClass('no-overflow');
            }

            if (this.options.backdrop != 'static') {
                this.$backdrop.click($.proxy(this.hide, this))
            }

            if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

            this.$backdrop.addClass('in')

            doAnimate ?
                this.$backdrop.one($.support.transition.end, callback) :
                callback()

        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass('in')

            $.support.transition && this.$element.hasClass('fade') ?
                this.$backdrop.one($.support.transition.end, $.proxy(removeBackdrop, this)) :
                removeBackdrop.call(this)

        } else if (callback) {
            callback()
        }
    }

    function removeBackdrop() {
        this.$backdrop.remove()
        this.$backdrop = null
        $('body').removeClass('no-overflow');
    }

    function escape() {
        var that = this
        if (this.isShown && this.options.keyboard) {
            $(document).on('keyup.dismiss.modal', function (e) {
                e.which == 27 && that.hide()
            })
        } else if (!this.isShown) {
            $(document).off('keyup.dismiss.modal')
        }
    }


    /* MODAL PLUGIN DEFINITION
     * ======================= */

    $.fn.modal = function (option) {
        return this.each(function () {
            var $this = $(this)
                , data = $this.data('modal')
                , options = $.extend({}, $.fn.modal.defaults, $this.data(), typeof option == 'object' && option)
            if (!data) $this.data('modal', (data = new Modal(this, options)))
            if (typeof option == 'string') data[option]()
            else if (options.show) data.show()
        })
    }

    $.fn.modal.defaults = {
        backdrop:true, keyboard:true, show:true
    }

    $.fn.modal.Constructor = Modal


    /* MODAL DATA-API
     * ============== */

    $(function () {
        $('body').on('click.modal.data-api', '[data-toggle="modal"]', function (e) {
            var $this = $(this), href
                , $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
                , option = $target.data('modal') ? 'toggle' : $.extend({}, $target.data(), $this.data())

            e.preventDefault()
            $target.modal(option)
        })
    })

}(window.jQuery);