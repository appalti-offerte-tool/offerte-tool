
Ext.define('Ext.ux.grid.GridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'ext.ux.grid.gridpanel',

    selectFirstRow: true,

    autoLoadRecords: false,

    autoLoadRecordsCallbackFn: null,

    loadMask: true,

    recordChangeOnSelect: true,

    lastSelectedRow: null,

    refreshViewBeforeLoad: true,

    lastInsertedId: null,

    initComponent: function() {

        this.callParent(arguments);

        this.addEvents(
            /**
             * Fire when record change
             *
             * @param {Ext.ux.grid.GridPanel} grid
             * @param {Ext.data.Record} record
             */
            'recordchange'
        );

        this.on('viewready', this.onViewChangeCallback, this);
        if (this.recordChangeOnSelect) {
            var sm = this.getSelectionModel();
            sm.on('select', this.onViewChangeCallback, this);
            sm.singleSelect = true;
        } else {
            this.on('rowclick', this.onViewChangeCallback, this);
        }

        this.getView().on('refresh', this.onViewChangeCallback, this);

        true === this.autoLoadRecords && this.on('render', function() {
            var f = this.autoLoadRecordsCallbackFn;
            if (Ext.isFunction(f) || Ext.isFunction(f = this[this.autoLoadRecordsCallbackFn])) {
                f.call(this);
            } else {
                this.getStore().load();
            }
        }, this);

        if (true === this.refreshViewBeforeLoad) {
            var sm = this.getSelectionModel();
            this.getStore().on('beforeload', function(s) {
                s.removeAll();
            });
        }
    },

    onViewChangeCallback: function() {

        var sm = this.getSelectionModel();
        if (0 == this.getStore().getCount()) {
            return;
        }

        if (this.lastInsertedId) {

            var lastInsertedRecord = this.getStore().getById(this.lastInsertedId);
            this.lastInsertedId = null;

            if (lastInsertedRecord) {
                var rowIndex = this.getStore().indexOf(lastInsertedRecord);
                var silent = sm.silent;
                sm.silent = true;
                sm.select(rowIndex, false, true);
                sm.silent = silent;
            }
        }

        var hasSelection = sm.hasSelection();
        if (this.selectFirstRow && !hasSelection) {
            sm.select(0);
        } else if (!hasSelection) {
            return;
        }

        var record = sm.getSelection()[0];
        if (null !== this.lastSelectedRow && this.lastSelectedRow == record) {
            return;
        }

        this.lastSelectedRow = record;
        this.fireEvent('recordchange', this, record);
    },

    setLastInsertedId: function(id) {
        this.lastInsertedId = id;
        return this;
    }
});