Ext.define("Ext.ux.grid.feature.GroupingOverride", {
    extend : "Ext.grid.feature.Grouping",

    alias : "feature.grouping-override",

    getGroupRows: function(group, records, preppedRecords, fullWidth) {

        var me = this,
            children = group.children,
            rows = group.rows = [],
            view = me.view;
        group.viewId = view.id;

        group.rs = Ext.clone(children);

        Ext.Array.each(records, function(record, idx) {
            if (Ext.Array.indexOf(children, record) != -1) {
                rows.push(Ext.apply(preppedRecords[idx], {
                    depth: 1
                }));
            }
        });
        delete group.children;
        group.fullWidth = fullWidth;
        if (me.collapsedState[view.id + '-gp-' + group.name]) {
            group.collapsedCls = me.collapsedCls;
            group.hdCollapsedCls = me.hdCollapsedCls;
        }
        return group;
    }

});