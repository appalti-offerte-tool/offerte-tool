Ext.ns('Ext.ux.plugins.grid');

Ext.ux.plugins.grid.ContextMenuActions = function(config) {

    this.rowConfig = Ext.apply({

        actionConfig: null,

        /**
        * The type of actions
        *
        * @param {String}
        *         + "inline"    - Render at the right column of column model and allow inline editing
        *         + "context"    - Render at the menu items on `rowcontextmenu` or `cellcontextmenu`
         *      + "combine" - Works in inline and context mode
        */
        actionsType: 'context',

        /**
         * The type of context menu
         *
         * @param {String}
         *         + "row"     - Use "rowcontextmenu"
         *         + "cell"    - Use "cellcontextmenu"    if you want to use column index
         */
        contextmenu: 'row',

        /**
         * Select active row deselect all other
         *
         * @param {String} forceRowSelection
         */
        forceRowSelection: true

    }, config || {});

    Ext.ux.plugins.grid.ContextMenuActions.superclass.constructor.call(this);
};

Ext.extend(Ext.ux.plugins.grid.ContextMenuActions, Ext.util.Observable, {

    disabled: false,

    init: function(grid) {

        this.grid = grid;

        if (!this.rowConfig.items) {
            return;
        }

        var item = this.rowConfig.items;
        this.grid.rowActionsConfig = this.rowConfig.items;

        var type = this.rowConfig.actionsType;
        if (-1 != ['context', 'combine'].indexOf(type) && !Ext.isOpera) {
            var cmenu = this.rowConfig.contextmenu;
            this.grid.on(cmenu + 'contextmenu', this.onContextMenu, this);
            if ('row' == cmenu) {
                this.grid.on('cellcontextmenu', this.onCellContextMenu, this);
            }

            if ('context' == type) {
                return;
            }
        }

        var actions = [];
        var handlers = {};

        if (!Ext.isArray(this.rowConfig.items)) {
            this.rowConfig.items = [this.rowConfig.items];
        }

        Ext.each(this.rowConfig.items, function(item) {
            if (item.hidden || item.disabled || item.inlineHidden) {
                return;
            }

            var isSeparator = item instanceof Ext.menu.Separator || item == '-';
            if (item && !isSeparator) {

                if (Ext.isFunction(item)) {
                    actions.push(function(grid, rowIndex) {
                        var i = item.call(item.scope || grid, grid, rowIndex);
                        return {
                            iconCls: i.iconCls,
                            qtip: i.text,
                            cb: function(grid, record, action, rowIndex, col) {
                                if (Ext.isFunction(i.handler)) {
                                    i.handler.call(i.scope || this, grid, rowIndex);
                                }
                            },
                            scope: i.scope || grid
                        }
                    });
                } else {
                    actions.push({
                        iconCls: item.iconCls,
                        qtip: item.text,
                        cb: function(grid, record, action, rowIndex, col) {
                            if (Ext.isFunction(item.handler)) {
                                item.handler.call(item.scope || this, grid, rowIndex);
                            }
                        },
                        scope: item.scope || grid
                    });
                    handlers[item.iconCls] = item.handler.createDelegate(item.scope || this) || Ext.emptyFn;
                }

            }
        }, this);

        if (0 == actions.length) {
            return;
        }

        var rowAction = new Ext.ux.plugins.grid.RowActions(Ext.apply({
            header: this.rowConfig.header || '&nbsp;',
            width: actions.length * 22,
            fixed: true,
            hideable: false,
            actions: actions
        }, this.rowConfig.actionConfig || {}));

        var cm = grid.getColumnModel().config;
        cm = cm.concat(rowAction);
        this.grid.getColumnModel().setConfig(cm, true);

        this.grid.beforeMethod('reconfigure', function(store, columnModel) {
            cm = columnModel.config.concat(rowAction);
            columnModel.setConfig(cm, true);
        }, this);

        rowAction.init(this.grid);
    },

    setDisabled: function(flag){
        this.disabled = flag;
    },

    /**
     *
     * @param {Ext.grid.GridPanel} g
     * @param {Number} rowIndex
     */
    onContextMenu: function() {

        if(this.disabled){
            return;
        }

        var g = arguments[0];
        var rowIndex = arguments[1];

        var e = arguments[arguments.length - 1];
        e.stopEvent();

        if (false !== this.rowConfig.forceRowSelection) {
            var sm = g.getSelectionModel();
            var alen = arguments.length;
            if (sm instanceof Ext.grid.RowSelectionModel) {
                var sels = sm.getSelections();
                if (!sels || sels.length <=1) {
                    sm.selectRow(rowIndex);
                }
            } else if (4 == alen && sm instanceof Ext.grid.CellSelectionModel) {
                sm.select(rowIndex, arguments[2]);
            }
        }

        var menu = new Ext.menu.Menu({items: []});
        var count = 0;

        var args = arguments;
        Ext.each(g.rowActionsConfig, function(i) {

            var item = null;
            if (Ext.isFunction(i)) {
                item = i.apply(i.scope || g, args);
            } else {
                item = i;
            }

            if (!Ext.isBoolean(item)) {
                if (!Ext.isArray(item)) {
                    item = [item];
                }

                Ext.each(item, function(m) {
                    var isSeparator = m instanceof Ext.menu.Separator || m == '-';
                    if (!isSeparator && !(m.hidden || m.disabled)) {

                        m.xtype = m.xtype || 'menuitem';
                        m = Ext.ComponentMgr.create(m);

                        if (m.handler && Ext.isFunction(m.setHandler)) {
                            var h = m.handler;
                            m.setHandler(function(){
                                h.apply(m.scope || g, args);
                            }, m.scope || g);
                        }

                        count++;
                    }

                    menu.add(m);
                });
            }

        }, this);

        if (count > 0) {
            menu.showAt(e.getXY());
        }
    },

    onCellContextMenu: function(g, rowIndex, cellIndex, e) {
        if (false !== this.rowConfig.forceRowSelection) {
            var sm = g.getSelectionModel();
            if (sm instanceof Ext.grid.CellSelectionModel) {
                sm.select(rowIndex, cellIndex);
            }
        }
    }
});