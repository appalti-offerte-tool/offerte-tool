Ext.ns('Ext.ux.plugin.grid');

Ext.ux.plugin.grid.Filtering = Ext.extend(Ext.ux.plugin.grid.filtering.GridFilters, {

    searchable: false,

    searchField: null,

    stateful: true,

    isSearchable: function() {
        return this.searchable && String(this.getSearchField().getValue() || "").length > 0;
    },

    init: function(g) {

        Ext.ux.plugin.grid.Filtering.superclass.init.apply(this, arguments);

//        if (this.searchable && g instanceof Ext.grid.GridPanel) {
//            this.stateId = g.stateId;
        g.getColumnModel().on('hiddenchange', function(cm, colIndex, hidden) {
            this.reload();
        }, this);
//        }

        this.filters.each(function(f) {
            if (f.type != 'search') {
                return;
            }

            return false;
        }, this);

    },

    applyState: function(grid, state){
        if (true !== this.stateful) {
            return;
        }

        Ext.ux.plugin.grid.Filtering.superclass.applyState.apply(this, arguments);
    },

    saveState: function(grid, state){
        if (true !== this.stateful) {
            return;
        }

        Ext.ux.plugin.grid.Filtering.superclass.saveState.apply(this, arguments);
    },

    getStateId : function(){
        return this.stateId || ((this.id.indexOf('ext-comp-') == 0 || this.id.indexOf('ext-gen') == 0) ? null : this.id);
    },

    getFilterData: function() {

        var filters = [];
        var searchable = this.isSearchable();
        var searchableCollection = [];

        this.filters.each(function(f) {

            if (Ext.ux.OSDN.empty(f.getValue())) {
                if (f.getValue() != false) {
                    f.active = false;
                }
            }

            if (f.active) {
                var d = [].concat(f.serialize(this));
                for (var i = 0, len = d.length; i < len; i++) {
                    filters.push({
                        field: f.dataIndex,
                        data: d[i]
                    });
                }
            }

            if (searchable) {
                switch (f.type) {
                    case 'string':
                    case 'date':
                    case 'numeric':
                        if (false !== f.searchable) {
                            var cm = this.grid.getColumnModel();
                            var colIndex = cm.findColumnIndex(f.dataIndex);
                            if (-1 != colIndex && !cm.isHidden(colIndex)) {
                                searchableCollection.push(f.dataIndex);
                                f.active = true;
                            }
                        }
                }
            }

        }, this);

        if (searchable && searchableCollection.length > 0) {
            var diSearch = this.getFilter('di-search');
            filters.push({
                field: searchableCollection.join(','),
                data: {
                    type: diSearch.stype || 'search',
                    value: this.getSearchField().getValue(),
                    field: ''
                }
            });
        }

//        this.grid.getStore().setBaseParam('reset', +(filters.length == 0));

        return filters;
    },

    getRecordFilter: function(){

        var diValue = this.getSearchField().getValue();

        if (!this.isSearchable()) {
            return Ext.ux.plugin.grid.Filtering.superclass.getRecordFilter.apply(this, arguments);
        }

        var fd = this.getFilterData();
        var f = [];
        this.filters.each(function(filter){
            if (-1 != ['string', 'date', 'numeric'].indexOf(filter.type)) {
                f.push({
                    f: filter,
                    o: {
                        active: filter.active,
                        value: filter.getValue()
                    }
                });
                filter.setActive("" != diValue, true);
                filter.setValue(diValue || "", true);
            }
        }, this);

        var len = f.length;
        var fn = function(record) {
            for(var i = 0; i < len; i++) {
                return f[i].f.validateRecord(record);
            }

            return true;
        };

        this.store.on('datachanged', function() {
            for(var i = 0; i < len; i++) {
                var v = f[i].o.value || "";
                f[i].f.setActive(v != "", true);
                f[i].f.setValue(v, true);
            }
        }, this, {single: true});

        return fn;
    },

    getSearchField: function(config) {

        if (!this.searchField) {
            this.searchable = true;

            this.filters.each(function(f) {
                if (f.type != 'search') {
                    return;
                }

                this.searchField = f.getField();
                return false;

            }, this);
        }

        return this.searchField;
    }
});