Ext.ns('Ext.ux.plugins.panel');

Ext.define('Ext.ux.plugins.panel.Window', {

    extend: 'Ext.util.Observable',

    alias: 'ext.ux.plugins.panel.window',

    window: null,

    panel: null,

    config: null,

    constructor: function(config) {
//        this.config = Ext.apply(this.config || {}, config);
        this.config = config || {};
        // Call our superclass constructor to complete construction process.
        this.callParent(arguments);
    },

    init: function(p) {
        var $this = this;
        this.panel = p;
        p.showInWindow = Ext.bind(function(config) {

            var title = p.title;
            p.title = null;
//            p.elements = p.elements.replace(',header', '');
            
            var btns = [];
            var vConfig = Ext.clone($this.config);
            var pConfig = Ext.clone(config);

            config = Ext.apply({}, config || {});
            pConfig = Ext.apply(vConfig || {}, pConfig);

            if (false !== pConfig.submitBtnCaption) {
                btns.push({
                    text: pConfig.submitBtnCaption || lang('Save'),
                    handler: function() {
                        p.onSubmit();
                    },
                    scope: this
                });
            }

            btns.push({
                text: lang('Close'),
                handler: function() {
                    this.window.close();
                },
                scope: this
            });

            if (p.width) {
                pConfig.autoWidth = true;
                pConfig.width = null;
            } else {
                pConfig.width = pConfig.width || 800;
            }

            if (p.height) {
                pConfig.autoHeight = true;
                pConfig.height = null;
            } else {
                pConfig.height = pConfig.height || 500;
            }

            var w = new Ext.Window(Ext.apply({
                title: title,
                layout: 'fit',
                modal: true,
                stateful: false,
                autoScroll: true,
                bodyBorder: false,
                border: false,
                items: [p],
                tools: [{
                    id: 'close',
                    handler: function() {
                        this.window.close();
                    },
                    scope: this
                }],
                buttons: [btns]
            }, pConfig));

            $this.window = w;
            p.window = w;
            w.show();

            return w;
        }, this);
    },

    onSubmit: function() {
        this.panel.fireEvent('wndhandlerclick', this.panel, this.window);
    }
});
