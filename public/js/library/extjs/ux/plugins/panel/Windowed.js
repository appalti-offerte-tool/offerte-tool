Ext.ns('Ext.ux.plugins.panel');

Ext.ux.plugins.panel.Windowed = Ext.extend(Ext.util.Observable, {

    window: null,

    panel: null,

    config: null,

    constructor: function(config) {
        this.config = Ext.apply(this.config || {}, config);

        // Call our superclass constructor to complete construction process.
        Ext.ux.plugins.panel.Windowed.constructor.call(config);
    },

    init: function(p) {

        var $this = this;
        this.panel = p;
        p.showInWindow = function(config) {

            var title = p.title;
            p.title = null;
            p.elements = p.elements.replace(',header', '');

            var btns = [];
            config = Ext.apply({}, config || {});
            config = Ext.apply($this.config || {}, config);
            if (false !== config.submitBtnCaption) {
                btns.push({
                    text: config.submitBtnCaption || lang('Save'),
                    handler: function() {
                        this.onSubmit();
                    },
                    scope: this
                });
            }

            btns.push({
                text: lang('Close'),
                handler: function() {
                    this.window.close();
                },
                scope: this
            });

            var w = new Ext.Window(Ext.apply({
                title: title,
                layout: 'fit',
                modal: true,
                stateful: false,
                autoScroll: true,
                width: p.width || 800,
                height: p.height || 500,
                bodyBorder: false,
                border: false,
                items: [p],
                tools: [{
                    id: 'close',
                    handler: function() {
                        this.window.close();
                    },
                    scope: this
                }],
                buttons: [btns]
            }, config));

            $this.window = w;
            p.window = w;
            w.show();

            return w;
        }.createDelegate(this);
    },

    onSubmit: function() {
        this.panel.fireEvent('wndhandlerclick', this.panel, this.window);
    }
});

Ext.ComponentMgr.registerPlugin('ext.ux.plugins.panel.windowed', 'Ext.ux.plugins.panel.Windowed');