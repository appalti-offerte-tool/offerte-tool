Ext.ns('Ext.ux.plugins.TabPanel');

Ext.ux.plugins.TabPanel.Actionable = Ext.extend(Ext.util.Observable, {

    menu: null,

    menus: null,

    items: null,

    scrollMenu: null,

    alwaysVisible: true,

    closeOnHide: false,

    keyword: null,

    constructor: function(config) {

        Ext.apply(this, config || {});

        this.addEvents(
            'beforeinittab'
        );
    },

    init: function(tp) {

        this.tp = tp;
        Ext.apply(this, tp.actionable || {});
        this.menus = {};

        tp.on('render', function() {
            this.tp.header.applyStyles({
                position: 'relative'
            });

            if (this.alwaysVisible) {
                this.createScrollMenu();
            }

            var f = tp.createScrollers.createSequence(this.initMenu, this);
            tp.createScrollers = f;

            this.tp.on('destroy', this.onDestroy, this);
        }, this);

        tp.on('beforeunhide', this.onShowBlock, this);
        tp.on('afterrender', this.onAfterRender, this);
        tp.on('loadblocks', function(tpanel, blocks) {

            if (this.menus[this.keyword]) {
                this.menu = this.menus[this.keyword];
                return;
            }
            this.createMenu(blocks);
            this.onShowBlocks(blocks);

        }, this);


    },

    initMenu: function() {

        if (!this.scrollMenu) {
            this.createScrollMenu();
        }

        //move the right menu item to the left 18px
        var rtScrBtn = this.tp.header.dom.firstChild;
        Ext.fly(rtScrBtn).applyStyles({
            right : this.scrollMenu ? '18px' : '0px'
        });

        var stripWrap = Ext.get(this.tp.strip.dom.parentNode);
        stripWrap.applyStyles({
            'margin-right' : this.scrollMenu ? '36px' : '18px'
        });

        this.tp.scrollLeft.show = this.tp.scrollLeft.show.createSequence(function() {
            this.scrollMenu.show();
        }, this);

        this.tp.scrollLeft.hide = this.tp.scrollLeft.hide.createSequence(function() {
            this.scrollMenu.hide();
        }, this);
    },

    createScrollMenu: function() {
        // Add the new righthand menu
        var h = this.tp.stripWrap.dom.offsetHeight;
        this.scrollMenu = this.tp.header.insertFirst({
            cls:'x-tab-tabmenu-right'
        });
        this.scrollMenu.setHeight(h);
        this.scrollMenu.addClassOnOver('x-tab-tabmenu-over');
        this.scrollMenu.on('click', this.onShowMenu, this);
    },

    onShowMenu: function(e) {

        if (!this.menu && this.items) {

            this.menu = new Ext.menu.Menu({
                items: this.items
            });

            this.menu.on('click',  this.onMenuItemClick, this);
        }

        if (!this.menu) {
            this.createMenu(this.tp.items.items);
        }

        if (this.menu) {
            var target = Ext.get(e.getTarget());
            var xy = target.getXY();

            xy[1] += 24;

            this.menu.showAt(xy);
        }
    },

    createMenu: function(blocks) {

        var menuConfig = Ext.decode(this.getCookie('block-tab-panel-' + this.keyword));
        var items = [];
        var stateitems = {};

        for (var i in blocks) {

            if (!Ext.isObject(blocks[i])) {
                continue;
            }

            var chkd = true;
            if (menuConfig && !Ext.isEmpty(menuConfig[blocks[i].module + '-' + blocks[i].name])) {
                chkd = menuConfig[blocks[i].module + '-' + blocks[i].name] ;
            }

            items.push({
                text: blocks[i].title,
                checked: chkd,
                tab: blocks[i],
                module: blocks[i].module,
                name: blocks[i].name
            });

            stateitems[blocks[i].module + '-' + blocks[i].name] = chkd;
        }

        this.menus[this.keyword] = new Ext.menu.Menu({
            items: items
        });

        this.menus[this.keyword].on('click',  this.onMenuItemClick, this);
        this.menu = this.menus[this.keyword];

        this.setCookie('block-tab-panel-' + this.keyword, Ext.encode(stateitems));

    },

    onMenuItemClick: function(menu, item) {

        if (!this.items) {
            var stateitems = {};
            menu.items.each(function(m) {
                stateitems[m.module + '-' + m.name] = m.checked;
            });
            this.setCookie('block-tab-panel-' + this.keyword, Ext.encode(stateitems));
        }

        if (item.checked) {

            if (item.tab) {
                this.tp.unhideTabStripItem(item.tab);
                item.tab.setVisible(true)
                return;
            }

            var tab = this.onBeforeInitTabFn(item);
            item.tab = tab;

            this.tp.add(tab);
            this.tp.setActiveTab(tab);
            this.tp.doLayout();
            return;
        }

        var i = this.tp.get(item.tab);
        if (this.closeOnHide) {
            this.tp.remove(i);
            delete item.tab;
        } else {
            this.tp.hideTabStripItem(i);
            i.setVisible(false);
            if ( ! this.tp.getActiveTab().isVisible()) {
                this.tp.items.each(function(b) {
                    if (Ext.fly(this.tp.getTabEl(b)).isVisible()) {
                        this.tp.setActiveTab(b);
                        return false;
                    }
                }, this);
            }
        }
    },

    onBeforeInitTabFn: Ext.emptyFn,

    setOnBeforeInitTabCallback: function(fn) {
        if (!Ext.isFunction(fn)) {
            return this;
        }

        this.onBeforeInitTabFn = fn;
        return this;
    },

    setKeyword: function(keyword) {
        this.keyword = keyword;
        return this;
    },

    onDestroy: function() {
        Ext.destroy(this.menu);
    },

    getCookie: function(param) {
        return Ext.state.Manager.getProvider().get(param);
    },

    setCookie: function(param, value) {
        Ext.state.Manager.getProvider().set(param, value);
        return this;
    },

    onAfterRender: function(tpanel) {
        this.onShowBlocks(tpanel.items.items);
    },

    onShowBlocks: function(blocks) {

        for (var i in blocks) {
            if (!Ext.isObject(blocks[i])) {
                continue;
            }
            if (!this.onShowBlock(this.tp, blocks[i])) {
                this.tp.hideTabStripItem(blocks[i]);
                blocks[i].setVisible(false);
            }
        }

        if (this.tp.getActiveTab() &&  !this.tp.getActiveTab().isVisible()) {
            this.tp.items.each(function(b) {
                if (Ext.fly(this.tp.getTabEl(b)).isVisible()) {
                    this.tp.setActiveTab(b);
                    return false;
                }
            }, this);
        }
    },

    onShowBlock: function(tpanel, block) {
        var showFlag = true;
        if (!this.menus[this.keyword]) {
            var menuConfig = Ext.decode(this.getCookie('block-tab-panel-' + this.keyword));
            if(menuConfig && !Ext.isEmpty(menuConfig[block.module + '-' + block.name])) {
                showFlag = menuConfig[block.module + '-' + block.name];
            }
        } else {
            var m = this.menus[this.keyword].find('tab', block);
            if (!Ext.isEmpty(m)) {
                showFlag = m[0].checked;
            };
        }
        return showFlag;
    }


});


