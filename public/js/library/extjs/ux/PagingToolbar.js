Ext.ns('Ext.ux');

Ext.define('Ext.ux.PagingToolbar', {
    extend: 'Ext.toolbar.Paging',

    displayInfo: true,

    pageSize: 30,

    variations: null,

    ps: null,

    psConfig: null,

    forceLoad: true,

    initComponent: function() {

        if (false !== this.ps) {
            var o = Ext.applyIf(this.psConfig || {}, {
                beforeText: lang('Show'),
                afterText: lang('rows'),
                stateId: this.stateId,
                pagingToolbar: this,
                dynamic: false,
                variations: [5, 10, 20, 30, 50, 100]
            });

            if (this.variations) {
                o.variations = this.variations;
            }
            if (this.forceLoad != undefined) {
                o.forceLoad = this.forceLoad;
            }

            if (!Ext.isArray(this.plugins)) {
                this.plugins = [this.plugins];
            }

            this.ps = new Ext.ux.plugin.Andrie.pPageSize(o);
            this.plugins = this.plugins.concat(this.ps);
        }

        this.callParent(arguments);
    },

    beforeLoad: function(store, options) {
        this.callParent(arguments);
        options.params = options.params || {};
        Ext.applyIf(options.params, {
            start: 0,
            limit: this.pageSize
        });
        return true;
    },

    /**
     * Change the active page
     * @param {Integer} page The page to display
     */
    changePage: function(page, refreshOnly){

        if (true === refreshOnly) {
            var page = ((page-1) * this.pageSize).constrain(0, this.store.getTotalCount());
            var ps = {};
            ps[this.store.paramNames.start] = page;
            this.refreshOptions(ps);
        } else {
            this.callParent(arguments);
        }
    },

    /**
    * Refresh paging options
    *
    * @param {Object} o
    * @return {Ext.ux.PagingToolbar}
    */
    refreshOptions: function(o) {
        var args = [this.getStore(), [], {params: o}];
        this.onLoad.apply(this, args);
        return this;
    },

    /**
    * Retrieve store
    *
    * @return {Ext.data.Store}
    */
    getStore: function() {
        return this.store;
    }
});