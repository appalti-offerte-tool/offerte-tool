Ext.ns('Ext.ux.form');

Ext.ux.form.SearchField = Ext.extend(Ext.form.TwinTriggerField, {

    validationEvent: false,

    validateOnBlur: false,

    trigger1Class: 'x-form-clear-trigger',

    trigger2Class: 'x-form-search-trigger',

    hideTrigger1: true,

    hasSearch: false,

    paramName: 'query',

    minSearchLength: 2,

    initComponent: function() {

        Ext.ux.form.SearchField.superclass.initComponent.apply(this, arguments);

        var updateTask = new Ext.util.DelayedTask(this.onTrigger2Click, this);

        this.on('keyup', function(f, e) {

            var valLength = this.getValue().length;
            var k = e.getKey();
            if (0 != valLength && valLength < this.minSearchLength) {
                f.markInvalid();
                return;
            } else {
                f.clearInvalid();
            }

            if (k == e.ENTER) {
                updateTask.cancel();
                return this.onTrigger2Click();
            }

            if (k == e.BACKSPACE || k == e.DELETE) {
                updateTask.delay(1500, this.onTrigger2Click, this);
            }

            if (!e.isSpecialKey()) {
                updateTask.delay(1500, this.onTrigger2Click, this);
            }
        }, this);

        this.addEvents(

            /**
             * Fire and clear value
             *
             * @param {Ext.ux.form.SearchField}
             */
            'clear',

            /**
             * Fire on click
             *
             * @param {Ext.ux.form.SearchField}
             */
            'click'
        );
    },

    onTrigger1Click: function() {
        if (!this.hasSearch) {
            return;
        }

        this.hideClearBtn();
        this.fireEvent('clear', this);
        this.focus();
    },

    showClearBtn: function() {
        this.triggers[0].show();
        this.hasSearch = true;
    },

    hideClearBtn: function(quite) {
        this.el.dom.value = '';
        this.triggers[0].hide();
        this.hasSearch = false;
    },

    refreshTriggerActions: function()
    {
        var v = this.getRawValue();

        if (v.length < 1) {
            this.hideClearBtn();
        } else {
            this.showClearBtn();
        }
    },

    onTrigger2Click: function() {

        this.refreshTriggerActions();
        this.fireEvent('click', this);
        this.focus();
    }
});

Ext.reg('searchfield', 'Ext.ux.form.SearchField');