Ext.define('Ext.ux.form.field.DateTime', {
    extend:'Ext.form.FieldContainer',
    mixins: {
        field: 'Ext.form.field.Field'
    },

    alias: 'widget.datetimefield',
    layout: 'hbox',
    defaultMargins: {top: 0, right: 5, bottom: 0, left: 5},
    width: 200,
    height: 22,
    combineErrors: true,
    msgTarget :'side',

    hiddenName: null,
    hiddenField: null,

    dateCfg:{},
    timeCfg:{},

    initComponent: function() {

        this.hiddenName = this.hiddenName || this.name;

        var me = this;
        me.buildField();
        me.callParent();
        this.dateField = this.down('datefield')
        this.timeField = this.down('timefield')
        this.hiddenField = this.down('hiddenfield')
        me.initField();

        this.dateField.on('change', function() {
            this.updateHidden();
        }, this);
        this.timeField.on('change', function() {
            this.updateHidden();
        }, this);
    },

    //@private
    buildField: function(){

        this.items = [
            Ext.apply({
                xtype: 'datefield',
                format: 'd-m-Y',
                flex: 3,
                submitFormat: 'Y-m-d'
            },this.dateCfg), {
                name: this.hiddenName,
                xtype: 'hiddenfield',
                flex: 0
            },
            Ext.apply({
                xtype: 'timefield',
                format: 'H:i',
                submitFormat: 'H:i:00',
                flex: 2
            },this.timeCfg)
        ]
    },

    getValue: function() {
        var value,date = this.dateField.getSubmitValue(),time = this.timeField.getSubmitValue();
        if(date){
            if(time){
                var format = this.getFormat()
                value = Ext.Date.parse(date + ' ' + time,format)
            }else{
                value = this.dateField.getValue()
            }
        }
        return value
    },

    setValue: function(value){
        this.dateField.setValue(value)
        this.timeField.setValue(value)
    },

    getSubmitData: function(){
        var value = this.getValue()
        var format = this.getFormat()
        return value ? Ext.Date.format(value, format) : null;
    },

    getFormat: function(){
        return (this.dateField.submitFormat || this.dateField.format) + " " + (this.timeField.submitFormat || this.timeField.format)
    },

    updateHidden:function() {
        if (this.hiddenField) {
            var value = this.getSubmitData();
            this.hiddenField.setValue(value);
        }
    }
    
})
