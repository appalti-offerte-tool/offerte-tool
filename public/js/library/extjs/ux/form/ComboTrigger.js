Ext.define('Ext.ux.form.ComboTrigger', {
    extend: 'Ext.ux.form.ComboBox',
    alias: 'ext.ux.form.combotrigger',

    validationEvent:false,

    validateOnBlur:false,

    trigger1Class:'',

    trigger2Class: 'osdn-add',

    width: 180,

    hasSearch : false,

    paramName : 'query',

    triggerAction: 'all',

    permissions: false,

    triggerQtip: null,

    disabledClass: 'x-item-field-disabled',

    /**
    * The collection of triggerItems
    * <code>
    * [{
    *     cls: 'osdn-earth',
    *     disabledCls: 'osdn-disabled-earth',
    *     name: 'btn1',
    *     permissions: true,
    *     qtip: 'but1',
    *     handler: function (e, node) {
    *         alert('but1');
    *     }
    * }, {
    *     ...
    * }]
    * </code>
    *
    */
    triggerItems: null,

    initComponent: function() {

        this.addEvents(
            /**
             * Fires on custom trigger click
             *
             * @param {Ext.ux.form.ComboTrigger}    trigger
             * @param {Ext.Element}                 node
             * @param {Object}                      event object
             */
            'customtriggerclick'
        );

        var myCn = [{
            tag: "img",
            src: Ext.BLANK_IMAGE_URL,
            cls: "x-form-trigger " + this.trigger1Class
        }];

        this.triggerId = Ext.id();

        if (this.triggerItems && this.permissions) {

            var ind = 1;

            if (!Ext.isArray(this.triggerItems)) {
                throw 'Property triggerItems should be an array!';
            }

            Ext.each(this.triggerItems, function (trigger) {

                if (!trigger.permissions) {
                    return;
                }

                ind++;

                var fn = function(node, e) {

                    if (this.disabled) {
                        return;
                    }

                    if (Ext.isFunction(trigger.handler)) {
                        if (trigger.scope) {
                            trigger.handler.apply(trigger.scope, [this, node, e]);
                        } else {
                            trigger.handler(this, node, e);
                        }
                    }
                };

                if (this.disabled) {
                    if (trigger.disabledCls) {
                        trigger.cls = trigger.disabledCls;
                    } else {
                        trigger.hidden = true;
                    }
                }

                this['onTrigger' + ind + 'Click'] =  fn;
                myCn.push({
                    id: this.triggerId + '-empty-' + (trigger.name || ''),
                    tag: "img",
                    src: Ext.BLANK_IMAGE_URL,
                    style: 'width: 2px;' + (trigger.hidden ? 'display: none;' : '')
                });

                var p = {
                    id: this.triggerId + '-img-' + (trigger.name || ''),
                    tag: "img",
                    src: Ext.BLANK_IMAGE_URL,
                    cls: "x-form-trigger no-border-bottom x-form-osdn-trigger " + (trigger.iconCls || trigger.cls || this.trigger2Class),
                    style: (trigger.hidden ? 'display: none;' : '')
                }

                if (trigger.qtip) {
                    p.qtip = trigger.qtip;
                }
                myCn.push(p);
            }, this);
        } else {
            if (this.permissions) {
                myCn.push({
                    id: this.triggerId + '-empty',
                    tag: "img",
                    src: Ext.BLANK_IMAGE_URL,
                    style: 'width: 2px;'
                });

                var p = {
                    id: this.triggerId + '-img',
                    tag: "img",
                    src: Ext.BLANK_IMAGE_URL,
                    cls: "x-form-trigger no-border-bottom " + this.trigger2Class
                }

                if (this.triggerQtip) {
                    p.qtip = this.triggerQtip;
                }
                myCn.push(p);
            }
        }

        this.triggerConfig = {
            tag:'span', cls:'x-form-twin-triggers', cn: myCn
        };

        this.onTrigger1Click = this.onTriggerClick.createDelegate(this);

        Ext.ux.form.ComboTrigger.superclass.initComponent.apply(this, arguments);

        this.on('disable', function() {
            if (this.triggerItems && Ext.isArray(this.triggerItems)) {
                Ext.each(this.triggerItems, function (trigger) {
                    if (trigger.cls && trigger.disabledCls) {
                        var el = Ext.get(this.triggerId + '-img-' + trigger.name);
                        el.removeClass(trigger.cls);
                        el.addClass(trigger.disabledCls);
                    } else {
                        this.hideTriggerItem(trigger.name);
                    }
                }, this);
            }
        });

        this.on('enable', function() {
            if (this.triggerItems && Ext.isArray(this.triggerItems)) {
                Ext.each(this.triggerItems, function (trigger) {
                    if (trigger.cls && trigger.disabledCls) {
                        var el = Ext.get(this.triggerId + '-img-' + trigger.name);
                        el.removeClass(trigger.disabledCls);
                        el.addClass(trigger.cls);
                    } else {
                        this.showTriggerItem(trigger.name);
                    }
                }, this);
            }
        });
    },

    showTriggerItem: function(name) {
        if (this.triggerItems) {
            Ext.getDom(this.triggerId + '-empty-' + name).style.display = 'inline';
            Ext.getDom(this.triggerId + '-img-' + name).style.display = 'inline';
        } else {
            Ext.getDom(this.triggerId + '-empty').style.display = 'inline';
            Ext.getDom(this.triggerId + '-img').style.display = 'inline';
        }
    },

    hideTriggerItem: function(name) {
        if (this.triggerItems) {
            //var name = arguments[0];
            Ext.getDom(this.triggerId + '-empty-' + name).style.display = 'none';
            Ext.getDom(this.triggerId + '-img-' + name).style.display = 'none';
        } else {
            Ext.getDom(this.triggerId + '-empty').style.display = 'none';
            Ext.getDom(this.triggerId + '-img').style.display = 'none';
        }
    },

    // use TwinTriggerField method
    initTrigger: function() {
        Ext.form.TwinTriggerField.prototype.initTrigger.apply(this, arguments);
    },

    // private
    onTrigger2Click: function(e, node) {
        if (this.disabled) {
            return;
        }
        this.fireEvent('customtriggerclick', this, node, e);
        this.onCustomTriggerClick(this, node, e);
    },

    onCustomTriggerClick: Ext.emptyFn
});