/**
 * Combo extension
 * Allow local filtering, lazy set value, etc.
 *
 * @version $Id: ComboBox.js 18763 2010-09-15 13:07:01Z vasya $
 */
Ext.define('Ext.ux.form.ComboBox', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ext.ux.form.combobox',

    //defaults options
    resizable: true,

    forceSelection: true,

    triggerAction: 'all',

    selectFirst: false,

    monitorEventDirty: 'select',

    /**
     * Refresh default value after loading
     *
     * @param {String}
     */
    trackResetOnLoad: false,

    /**
     * Define if filtering mode will be executed locally
     * Can apply only "local" and "remote"
     * Param "local" by default
     *
     * @cfg {String}
     */
    filteringMode: 'local',

    /**
     * Set free mode of filtering
     * Can apply only "true" and "false"
     * @cfg {String}
     */
    filteringFree: false,

    /**
     * Fields for filtering splitited by comma (for multiple field search)
     *
     * @cfg {String}
     */
    filteringFields: null,

    /**
     * On every click on combo trigger store will be reloaded
     *
     * @param {Boolean}
     */
    reloadOnChange: false,

    /**
     * Set reload on chage event
     * Possible are:
     *  "change",
     *  "select"
     */
    reloadOnChangeEvent: 'change',

    /**
     * Set display raw field
     * <example>
     *      {name}: ({date})
     * </example>
     *
     * @param {String}
     */
    displayRawField: null,

    /**
     * Display field options
     * Can be used in XTemplate object
     *
     * @param {Object}
     */
    displayRawFieldOptions: {},

    stripeListItems: true,

    /**
     * Render the blank option in combo at first position
     *
     * @param {Boolean}
     */
    allowBlankOption: false,

    /**
     * Data of blank option to override default empty values
     *
     * @param {struct}
     */
    blankOptionData: null,

    /**
     * Position of blank option
     * Possible values: ['first', 'last']
     * Default value: 'first'
     *
     * @param {string}
     */
    blankOptionPosition: 'first',

    /**
     * Update value when store is loaded
     *
     * @param {Boolean}
     */
    preloadRecords: true,

    queryParam: 'filter[0][data][value]',

    minChars: 2,

    preloadOnNullValue: true,

    /**
     * Initialize component
     * Set handler for event "beforequery"
     */
    initComponent: function() {

        if (this.store && this.transform) {
            var data = null;
            data = this.getTransformationData();
            var oo = {};
            oo[this.store.reader.meta.root] = data;
            this.store.loadData(oo);
        }

        Ext.ux.form.ComboBox.superclass.initComponent.call(this);

        this.on('beforequery', this.onBeforeQuery);

        this.store.on('load', function () {
            this._storeLoaded = true;
            this.prepareDefaultValue(this.getStore());
        }, this, {single: true});

        if ('local' == this.mode && this.allowBlankOption) {
            this.insertBlankOption();
        }

        this.store.on('load', function(store, records) {
            if (this.allowBlankOption) {
                this.insertBlankOption();
            }

            if (Ext.ux.OSDN.empty(this.lastQuery) && this.getValue()) {
                this.setValue(this.getValue());
            }

            this.doSelectFirst();
        }, this);

        if (this.reloadOnChange) {
            this.on(this.reloadOnChangeEvent, function() {
                this.getStore().load();
            }, this);
        }


        this.addEvents(
            /**
             * Fire in situation when combo preload store before setting the value
             * And when value is assigned
             *
             * @param {Ext.ux.form.ComboBox}  combo
             * @param {String} value
             */
            'ready'
        );
    },

    initList: function() {

        if (!this.tpl) {

            var strippedRow = this.stripeListItems ? ' {[xindex % 2 === 0 ? "x-grid3-row-alt" : ""]}' : '';

            if (Ext.isArray(this.columns)) {
                this.title = [];
                this.tpl = [
                    '<table cellspacing="0" cellpadding="0" width="100%">', '<tpl for=".">',
                    '<tr class="x-grid3-row ', strippedRow, ' x-combo-list-item">'
                ];

                for (var i = 0; i < this.columns.length; i++) {
                    var o = this.columns[i];

                    var s = '';
                    if (o.width) {
                        s = 'width: ' + o.width + 'px';
                    }

                    this.title.push(
                        '<td class="x-grid3-hd x-grid3-cell x-grid3-hd-inner x-grid3-cell-first" style="' + s + '">'
                            + '<span class="x-grid3-hd-inner">' + (o.title || '') + '</span>'
                        + '</td>'
                    );

                    this.tpl.push(
                        '<td class="x-grid3-combo-cell x-grid3-col x-grid3-cell ' + (o.cls || '') + '" style="' + s + '">'
                            + '<div class="x-grid3-cell-inner">' + (o.value || '') + '</div>'
                        + '</td>'
                    );
                }

                this.tpl.push(
                    '</tr>'
                    + '</tpl>' + '</table>'
                );

                this.title =
                    '<table cellspacing="0" cellpadding="0" width="100%">'
                        + '<tr class="x-grid3-header x-grid3-hd-row">'
                            + this.title.join('')
                        + '</tr>'
                    + '</table>';
            } else {
                this.tpl = '<tpl for=".">'
                    + '<div class="x-combo-list-item {_cls}' + strippedRow + '" style="{_style}" {_attr}>'
                        + this.getDisplayField()
                    + '&nbsp;</div>'
                + '</tpl>';
            }
        }

        Ext.ux.form.ComboBox.superclass.initList.apply(this, arguments);
    },

    prepareDefaultValue: function(store) {
        this.selectByValue(this.hiddenValue, true);
    },

    /**
     * Executed before query
     * if filteringMode is local make only once request
     * and try filtering data locally
     *
     * @param {Object} queryConfig
     *      query       {String}    SQL query
     *      forceAll    {Boolean}
     *      combo       {Object}    mean that "this"
     *      cancel      {Boolean}
     */
    onBeforeQuery: function(queryConfig) {

        if (this.mode == 'remote' && this.filteringMode == 'local') {
            this.minChars = 1;

            var c = this.store.getCount();
            if (c || this.loadedRecords) {
                this.store.clearFilter();
            }

            if (c === 0) {
                this.onBeforeLoad();
                this.store.load({
                    callback: function() {
                        this.loadedRecords = true;
                        this.store.filter(this.displayField, queryConfig.query);
                    },
                    scope: this
                });
            }

            this.expand();
            queryConfig.cancel = true;
            if ((c || this.loadedRecords) && queryConfig.query.length > 0) {
                this.store.filter(this.displayField, queryConfig.query);
            }
            this.restrictHeight();
        }
    },

    getDisplayedValue: function () {
        var displayedValue, value = this.getValue();
        if (value) {
            this.getStore().each(function (rec) {
                if (rec.get(this.valueField) == value) {
                    displayedValue = rec.get(this.displayField);
                    return false;
                }
            }, this);
        }
    },

    /**
     * Set combo value
     * If combo is not loaded then loaded and try set value
     */
    setValue: function(v) {

        var s = this.getStore();
        if ('object' == Ext.type(s)) {
            var p = s.getProxy();
            p.extraParams = p.extraParams || {};
            p.extraParams.value = v;
        }

        if (this.preloadRecords && this.preloadOnNullValue && 'remote' === this.mode && !this.isLoaded()) {
            this.initList();
            var args = arguments;
            this.onBeforeLoad();

            var p = {};
            if (this.pageSize > 0) {
                p.start = 0;
                p.limit = parseInt(this.pageSize);
            }

            this.getStore().load({
                params: p,
                callback: function() {
                    this._storeLoaded = true;
                    this.setValue.apply(this, args);

                    // set original value to new after loading
                    // and prevent changing value for Ext.BasicForm.isDirty()
                    if (this.trackResetOnLoad) {
                        this.originalValue = this.getValue();
                    }

                    this.fireEvent('ready', this, this.getValue());
                },
                scope: this
            });
            return;
        }

        if (null === v) {
            v = '';
        }

        if (this.displayRawField) {
            var text = v;
            if (this.valueField) {
                var r = this.findRecord(this.valueField, v);
                if (r) {
                    var tpl = new Ext.XTemplate(this.getDisplayField(), this.displayRawFieldOptions || {});
                    text = tpl.apply(r.data);
                } else if (this.valueNotFoundText !== undefined) {
                    text = this.valueNotFoundText;
                }
            }
            this.lastSelectionText = text;
            if (this.hiddenField) {
                this.hiddenField.value = v;
            }
            Ext.form.ComboBox.superclass.setValue.call(this, text);
            this.value = v;
        } else {
            Ext.ux.form.ComboBox.superclass.setValue.call(this, v);
        }
    },

    getRecord: function() {
        return this.getStore().getById(this.getValue());
    },

    getDisplayField: function() {
        return this.displayRawField || ('{' + this.displayField + '}');
    },

    /**
     * Get value, displayed in combo
     * If not setted valueField or displayField - return combo value
     */
    getDisplayValue: function() {
        if (this.valueField && this.displayField) {
            var mxcol = this.getStore().query(this.valueField, this.getValue());
            if (mxcol && mxcol.getCount() > 0) {
                return mxcol.first().get(this.displayField);
            }
        }
        return this.getValue();
    },

    // is combo already loaded?
    isLoaded: function() {
        return this._storeLoaded;
    },

    insertBlankOption: function() {

        var s = this.getStore();
        if (!s) {
            return this;
        }

        var fields = [];
        var names = {};

        s.fields.each(function(i){
                fields.push({
                    name: i.name,
                    dateFormat: i.dateFormat,
                    defaultValue: i.defaultValue,
                    mapping: i.mapping,
                    sortDir: i.sortDir,
                    type: i.type
                });
                names[i.name] = '';
            });

        var rf = Ext.data.Record.create(fields);
        var record = new rf(this.blankOptionData || names);

        /**
         * Prevent collisions when using multiple combos with one store
         */
        if (-1 !== s.findExact(this.valueField, record.get(this.valueField))) {
            return this;
        }

        switch(this.blankOptionPosition) {
            case 'last':
                s.add([record]);
                break;

            case 'first':
            default:
                s.insert(0, [record]);
        }

        return this;
    },

    getParams: function(q) {
        var p = Ext.ux.form.ComboBox.superclass.getParams.apply(this, arguments);
        if (q) {
            var flt = this.filteringFree ? 'free' : '';
            if (this.filteringFields) {
                p['filter[0][field]'] = this.filteringFields;
                p['filter[0][data][type]'] = 'search' + flt ;
            } else {
                p['filter[0][field]'] = this.displayField;
                p['filter[0][data][type]'] = 'string' + flt;
            }
        }
        return p;
    },

    /**
     * Select first row
     *
     * @param {Boolean} quite   Do not fire event select
     */
    doSelectFirst: function(quite) {
        if (!this.selectFirst) {
            return;
        }

        if (this.getValue() !== null && this.getValue() !== '') {
            var v = this.getValue();
            var found = false;
            this.getStore().each(function (record, index) {
                if (record.get(this.valueField) == v) {
                    found = true;
                    if (true !== quite) {
                        this.fireEvent('select', this, record, index);
                    }
                    return false;
                }
            }, this);

            if (found) {
                return;
            }
        }

        var record = this.getStore().getAt(0);
        if (record) {
            this.setValue(record.get(this.valueField));
            if (true !== quite) {
                this.fireEvent('select', this, record, 0);
            }
        } else{
            this.setValue(null);
        }
    },

    getTransformationData: function() {

        var d = [], opts = Ext.getDom(this.transform).options;
        for(var i = 0, len = opts.length;i < len; i++){
            var o = opts[i],
                value = (o.hasAttribute ? o.hasAttribute('value') : o.getAttributeNode('value').specified) ? o.value : o.text;
            if(o.selected && Ext.isEmpty(this.value, true)) {
                this.value = value;
            }

            /**
             * @todo
             * Currently we use only Json store
             */
            var oo = {};
            oo[this.valueField] = value;
            oo[this.displayField] = o.text;
            d.push(oo);
        }
        return d;
    }
});