/**
 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
 * @version     SVN: $Id: DateRange.js 1783 2012-01-26 12:21:21Z yaroslav $
 * @changedby   $Author: yaroslav $
 */

Ext.ns('Ext.ux.form');

Ext.ux.form.DateRange = function() {
    var fromdate = new Ext.form.DateField({
        format: 'd-m-Y',
        fieldLabel: lang('Start'),
        id: 'startdt',
        name: 'startdt',
        allowBlank: true,
        vtype: 'daterange',
        endDateField: 'enddt'// id of the 'To' date field
    });

    var todate = new Ext.form.DateField({
        format: 'd-m-Y',
        fieldLabel: lang('End'),
        id: 'enddt',
        name: 'enddt',
        allowBlank: true,
        vtype: 'daterange',
        startDateField: 'startdt'// id of the 'From' date field
    });

    return [fromdate, ' ', ' ', todate];
};

Ext.reg('ext.ux.form.daterange', 'Ext.ux.form.DateRange');

