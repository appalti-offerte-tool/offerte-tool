Ext.ns('Ext.ux.form');

Ext.ux.form.SpinnerField = Ext.extend(Ext.form.NumberField, {

    actionMode: 'wrap',
    deferHeight: true,
    autoSize: Ext.emptyFn,
//    onBlur: Ext.emptyFn,
    zeroOnEmpty: true,
    adjustSize: Ext.BoxComponent.prototype.adjustSize,

    constructor: function(config) {
        var spinnerConfig = Ext.copyTo({}, config, 'incrementValue,alternateIncrementValue,accelerate,defaultValue,triggerClass,splitterClass');

        var spl = this.spinner = new Ext.ux.Spinner(spinnerConfig);

        var plugins = config.plugins
            ? (Ext.isArray(config.plugins)
                ? config.plugins.push(spl)
                : [config.plugins, spl])
            : spl;

        Ext.ux.form.SpinnerField.superclass.constructor.call(this, Ext.apply(config, {plugins: plugins}));
    },

    // private
    getResizeEl: function(){
        return this.wrap;
    },

    // private
    getPositionEl: function(){
        return this.wrap;
    },

    // private
    alignErrorIcon: function(){
        if (this.wrap) {
            this.errorIcon.alignTo(this.wrap, 'tl-tr', [2, 0]);
        }
    },

    validateBlur: function(){
        return true;
    },

    initComponent: function() {

        Ext.ux.form.SpinnerField.superclass.initComponent.apply(this, arguments);

        this.on('spin', this.onBlur, this);

        if (this.zeroOnEmpty) {
            this.on('focus', function() {
                if (this.getValue() == 0) {
                    this.setValue('');
                }
            }, this);
            this.on('blur', function() {
                if (!this.getValue()) {
                    this.setValue(0);
                    v = 0;
                }
            }, this);
        }
    },

    onBlur: function() {
        this._validate();
        var v = this.getValue();

        if(String(v) !== String(this.startValue)) {
            this.fireEvent('change', this, v, this.startValue);
            this.onChangeDirtyState();
            if (this.hiddenField) {
                this.setValue(v);
            }
        }
    },

    setValue: function(v) {
        if ('object' == typeof this.strategy) {
            switch (this.strategy.type) {
                case 'percent':
                case 'money':
                    this.value = v;
                    if (this.rendered) {
                        if (this.strategy.type == 'money') {
                            var v = parseFloat(v);
                        }
                        else {
                            var v = parseInt(v);
                        }

                        var b = isNaN(v) ||
                        (this.strategy.minValue !== null && v < this.strategy.minValue) ||
                        (this.strategy.maxValue !== null && v > this.strategy.maxValue) ||
                        v === null ||
                        v === undefined;

                        var rawValue = '';
                        if (!b) {
                            rawValue = this.strategy._format(v);
                        }

                        this.setRawValue(rawValue);
                        v = rawValue;
                    }
                    break;

                default:
            }

            this.updateHidden();
        }

        Ext.ux.form.SpinnerField.superclass.setValue.call(this, v);

        this._validate();
    },

    // private
    _validate: function() {
        if (this.validateOnBlur) {
            if (false === this.validate()) {
                this.markInvalid();
            } else {
                this.clearInvalid();
            }
        }
    }
});

Ext.reg('ext.ux.form.spinnerfield', 'Ext.ux.form.SpinnerField');