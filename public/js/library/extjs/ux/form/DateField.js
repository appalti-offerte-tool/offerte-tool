Ext.define('Ext.ux.form.DateField', {

    extend: 'Ext.form.field.Date',
    alias: 'widget.ext.ux.form.datefield',

    hiddenFormat: 'Y-m-d',

    hiddenName: null,

    hiddenId: null,

    hiddenValue: null,

    setValue : function(date) {

        this.callParent([this.formatDate(this.parseDate(date))]);
        this.updateHidden();
    },

    updateHidden: function() {
        if (!this.hiddenField) {
            return;
        }
        var v = this.getValue();
        var value = Ext.isDate(v) ? Ext.Date.dateFormat(v, this.hiddenFormat) : '';
        this.hiddenField.value = value;
    },

    onRender: function() {

        this.callParent(arguments);

        this.hiddenField = this.el.insertSibling({
            tag:'input',
            type:'hidden',
            name: this.hiddenName || this.name

                /**
                 * @FIXME
                 * Date picker does not work properly with markup when id present
                 */
//                ,
//                id: this.hiddenId || this.hiddenName
        }, 'before', true);

        var v = this.getValue();
        this.hiddenField.value = Ext.isDate(v) ? Ext.Date.dateFormat(v, this.hiddenFormat) : '';
        this.el.dom.removeAttribute('name');
    },

    onTriggerClick: function() {
        if(this.disabled){
            return;
        }

        this.callParent(arguments);
    },

    onBlur: function() {
        this.updateHidden();
    }
});