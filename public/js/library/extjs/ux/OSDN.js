Ext.ns('Ext.ux.OSDN');

Ext.apply(Ext.ux.OSDN,

/**
 * The singleton class that provide a common interface for assing the
 * startup initialization events in differerent ordering
 */
function() {

    /**
     * Event manager
     * @param {Ext.util.Event}
     */
    var event = new Ext.util.Event();

    var regDecode = /^("(\\.|[^"\\\n\r])*?"|[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t])+?$/;

    /**
     * Event listeners collection
     * @param {Array}
     */
    var eventListeners = [];

    var link = function(module, controller, action, params, router, absolute) {
        var bp = '';
        if (true === /\.js$/.test(module)) {
            return bp + module;
        }

        var p = [];
        for (k in params) {
            p.push(k);
            p.push(params[k]);
        }

        var link = [bp];
        if (!Ext.ux.OSDN.empty(router)) {
            link.push(router);
        }

        link = link.concat([module, controller || 'index', action || 'index']).concat(p).join('/');
        if (!/\/$/.test(link)) {
            link += '/';
        }

        return link;
    };

    // prevent errors on console calls
    if(typeof console == 'undefined') {
        console = {
            log: function() {
                var args = [];
                for (var i = 0, l = arguments.length; i < l; i++) {
                   args.push(arguments[i]);
                }

                if (Ext.debug) {
                    Ext.log(args);
                } else {
                    alert(args.join(" \n"));
                }
            }
        };
    }

    return {

        /**
         * Add callback functions in stack and call all it after
         * OSDN.applicationInitialization
         *
         * @param {Function} f        The callback function
         * @param {Int} index         The index of execution  OPTIONAL
         * @return void
         */
        onReady: function(f, index) {
            if (!index) {
                var index = 1;
                var max = 1;
                for(var i = 0; i < eventListeners.length; i++) {
                    if (eventListeners[i][0] > max) {
                        max = eventListeners[i][0];
                    }
                }
            }
            eventListeners.push([index, f]);
        },

        /**
         * Execute all callbacks and also start the dispatch of application
         *
         * @return void
         */
        applicationInitialization: function() {

            if (arguments.length > 0 && 'function' == Ext.type(arguments[0])) {
                this.onReady(arguments[0], arguments[1] || 1);
            }

            eventListeners.sort(function(a, b) {
                a = parseInt(a, 10);
                b = parseInt(b, 10);

                if (a > b) return 1;
                if (a < b) return -1;
                return 0;
            });

            Ext.each(eventListeners, function(i) {
                event.addListener(i[1]);
            });

            Ext.onReady(function() {
                event.fire();
            });
        },

        /**
         * Encode mixed vars
         *
         * @param mixed      value           The value for encode
         * @param {Boolean}  stringprops     The output result props will have a string representation
         */
        encode: function (value, stringprops) {
            if (!stringprops) {
                return Ext.encode(value);
            }
            var o = {};

            var encode = function (o, value, prefix) {
                switch(typeof value) {
                    case 'object':
                        for(var i in value) {
                           encode(o, value[i], prefix + '[' + i + ']');
                        }
                    break;
                    case 'function':
                    break;
                    default:
                        o[prefix] = value;
                }
                return o;
            };

            for(var i in value) {
               encode(o, value[i], i);
            }

            return o;
        },

        decodeProps: function(p) {

            var o = {};
            var f = function(o, props, value) {
                var prop = props.shift();
                if (props.length == 0) {
                    o = o[prop] = value;
                } else {
                    o = o[prop] = o[prop] || {};
                    o = f(o, props, value);
                }
            };

            for(var i in p) {
                var v = i.replace(/\]$/, '').split(/\]\[|\[|\]/g);
                f(o, v, p[i]);
            }

            return o;
        },

        /**
         * Decode json string
         * And catch exceptions when decoding process failed.
         *
         * @param {Object} value
         * @return string|false
         */
        decode: function(value) {
            try {
                return regDecode.test(value) && eval('(' + value + ')');
            } catch (e) {
                if (true !== OSDN.DEBUG) {
                    return false;
                }
                if (Ext.isGecko) {
                    if (window.console && 'function' == typeof console.log) {
                        console.log(value);
                    }
                } else {
                    alert("Error in json decode!\n" + value);
                }

                return false;
            }
        },

        /**
         * Clone object
         * @param {Object}
         *
         * @return {Object} The cloned object
         */
        clone: function(o, deep) {
            if(!o || 'object' !== typeof o) {
                return o;
            }
            var c = 'function' === typeof o.pop ? [] : {};
            var p, v;

            if (deep === undefined || deep > 0) {
                for(p in o) {
                    if(o.hasOwnProperty(p)) {
                        v = o[p];
                        if(v && 'object' === typeof v) {
                            c[p] = Ext.ux.OSDN.clone(v, deep === undefined ? (deep - 1) : undefined);
                        } else {
                            c[p] = v;
                        }
                    }
                }
            }


            return c;
        },

        /**
         * Check if variable is empty
         * @param {mixed} v
         * @return {Boolean}
         */
        empty: function(v) {
            var e = v === "" ||
                    v === 0  ||
                    v === "0" ||
                    v === null ||
                    v === false ||
                    v === undefined ||
                    (Ext.isArray(v) && v.length === 0);

            if ('object' == Ext.type(v)) {
                e = true;
                for(var i in v) {
                    e = false;
                    break;
                }
            }
            return e;
        },

        link: link,

        alink: function(module, controller, action, params, router) {
            return OSDN.link(module, controller, action, params, router, true);
        },

        getCollection: function() {
            return OSDN.data.Collection;
        }
    };
}());


// replace the Ext.decode native method
//Ext.decode = Ext.ux.OSDN.decode;

String.prototype.ucFirst = function() {
    return this.substr(0,1).toUpperCase() + this.substr(1,this.length);
};

//String.prototype.ellipsis = function(maxLength){
//    if(this.length > maxLength){
//        return this.substr(0, maxLength-3) + '...';
//    }
//    return this;
//};

link = Ext.ux.OSDN.link;

////var translation = new OSDN.Translation({
////    locale: OSDN.LOCALE,
////    autocreate: !OSDN.TRANSLATION_SKIP_AUTOCREATE
////});
////translation.parse(OSDN.getCollection().get('translation'));
////lang = function() {
////    return translation.translate.apply(translation, arguments);
////};
//
//String.prototype.wordwrap = function(int_width, str_break, cut) {
//
//    var m = ((arguments.length >= 1) ? arguments[0] : 75   );
//    var b = ((arguments.length >= 2) ? arguments[1] : "\n" );
//    var c = ((arguments.length >= 3) ? arguments[2] : false);
//
//    var i, j, l, s, r;
//
//    if (m < 1) {
//        return this;
//    }
//
//    for (i = -1, l = (r = this.split(/\r\n|\n|\r/)).length; ++i < l; r[i] += s) {
//        for (s = r[i], r[i] = ""; s.length > m; r[i] += s.slice(0, j) + ((s = s.slice(j)).length ? b : "")){
//            j = c == 2 || (j = s.slice(0, m + 1).match(/\S*(\s)?$/))[1] ? m : j.input.length - j[0].length || c == 1 && m || j.input.length + (j = s.slice(m).match(/^\S*/)).input.length;
//        }
//    }
//
//    return r.join("\n");
//};
//
//String.prototype.ord = function() {
//
//    // http://kevin.vanzonneveld.net
//    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
//    // +   bugfixed by: Onno Marsman
//    // +   improved by: Brett Zamir (http://brett-zamir.me)
//    // *     example 1: ord('K');
//    // *     returns 1: 75
//    // *     example 2: ord('\uD800\uDC00'); // surrogate pair to create a single Unicode character
//    // *     returns 2: 65536
//
//    var str = this + '';
//
//    var code = str.charCodeAt(0);
//    if (0xD800 <= code && code <= 0xDBFF) { // High surrogate (could change last hex to 0xDB7F to treat high private surrogates as single characters)
//        var hi = code;
//        if (str.length === 1) {
//            return code; // This is just a high surrogate with no following low surrogate, so we return its value;
//                                    // we could also throw an error as it is not a complete character, but someone may want to know
//        }
//        var low = str.charCodeAt(1);
//        if (!low) {
//
//        }
//        return ((hi - 0xD800) * 0x400) + (low - 0xDC00) + 0x10000;
//    }
//    if (0xDC00 <= code && code <= 0xDFFF) { // Low surrogate
//        return code; // This is just a low surrogate with no preceding high surrogate, so we return its value;
//                                // we could also throw an error as it is not a complete character, but someone may want to know
//    }
//    return code;
//}

Ext.util.Format.euroMoney = function(v) {
    return Ext.util.Format.usMoney(v).replace(/^\$/, '&euro; ');
};