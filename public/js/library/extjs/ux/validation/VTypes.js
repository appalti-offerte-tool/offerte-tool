/**
 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
 * @version     SVN: $Id: VTypes.js 1449 2011-07-19 14:17:47Z vkudrinsky $
 * @changedby   $Author: vkudrinsky $
 */

Ext.apply(Ext.form.VTypes, {
    number: function(val, field) {
        return /\d/.test(val);
    },
    numberText: 'Not a valid number. Must be 0-9.',
    numberMask: /\d/,


    phoneNumber: function(val, field) {
        return /^\+?[0-9-\s]{5,15}$/.test(val);
    },
    phoneNumberText: 'Not a valid phone number.',

    daterange : function(val, field) {
        var date = field.parseDate(val);

        if(!date) {
            return;
        }
        if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
            var start = Ext.getCmp(field.startDateField);
            start.setMaxValue(date);
            start.validate();
            this.dateRangeMax = date;
        }
        else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
            var end = Ext.getCmp(field.endDateField);
            end.setMinValue(date);
            end.validate();
            this.dateRangeMin = date;
        }
        /*
         * Always return true since we're only using this vtype to set the
         * min/max allowed values (these are tested for after the vtype test)
         */
        return true;
    },

    confirmPassword: function(val, field) {
        if (field.initialPassField) {
            var pwd = Ext.get(field.initialPassField);
            return (val == pwd.getValue());
        }
        return true;
    },

    confirmPasswordText: 'Passwords do not match'

});
