Ext.ns('OSDN.Accounts');

OSDN.Accounts.Personal = Ext.extend(OSDN.form.FormPanel, {
    
    title: lang('Personal'),
    
    iconCls: 'osdn-personal-icon-16x16',
    
    permissions: acl.isUpdate('accounts', 'personal'),
    
    /**
     * @param {Ext.TabPanel}
     */
    tp: null,
    
    trackResetOnLoad: true,
    
    defaultType: 'textfield',
    
    editable: true,
    
    initComponent: function() {
        
        this.reader = new Ext.data.JsonReader({
            root: 'rowset',
            id: 'id'
        }, ['fullname', 'phone', 'email', 'layout', 'locale']);
        
        this.initialConfig.reader = this.reader;
        
        this.items = [{
            fieldLabel: lang('Full name'),
            name: 'fullname',
            disabled: !this.editable
        }, {
            fieldLabel: lang('Phone'),
            name: 'phone',
            allowBlank: true,
            disabled: !this.editable
        }, {
            fieldLabel: lang('Email'),
            name: 'email',
            vtype: 'email',
            disabled: !this.editable
        }, {
            fieldLabel: lang('Language'),
            xtype: 'osdncombo',
            mode: 'local',
            store: [
                ['en', lang('English')], 
                ['nl', lang('Dutch')]
            ],
            allowBlank: false,
            allowBlankOption: true,
            name: 'locale',
            hiddenName: 'locale',
            valueField: 'id',
            displayField: 'value',
            disabled: !this.editable
        }, {
            xtype: 'radiogroup',
            name: 'layout',
            fieldLabel: lang('Menu style')  + ':<span style="color:red;">*</span>',
            items: [{
                name: 'layout',
                boxLabel: lang('Left menu'), 
                inputValue: 'tree'
            }, {
                name: 'layout',
                boxLabel: lang('Top menu'), 
                inputValue: 'menu'
            }]
        }, {
            xtype: 'panel',
            border: false,
            style: {
                color: '#FF0000',
                'text-align': 'right'
            },
            html: '* - ' + lang('Login anew to apply changes')
        }];
        
        this.resetBtnLayout = new Ext.Button({
            iconCls: 'osdn-reset-icon-16x16',
            text: lang('Reset layout to defaults') + '<span style="color:red;">*</span>',
            disabled: this.checkStates(),
            handler: this.onResetState
        });
        
        this.resetBtn = new Ext.Button({
            text: lang('Reset'),
            iconCls: 'osdn-reset-icon-16x16',
            handler: function() {
                this.getForm().reset();
            },
            scope: this
        });
        
        this.submitBtn = new Ext.Button({
            text: lang('Save'),
            iconCls: 'osdn-save-icon-16x16',
            handler: this.doSave,
            scope: this
        });
        
        this.bbar = [
            this.resetBtnLayout, '-', '->',
            this.resetBtn, this.submitBtn
        ];
        
        OSDN.Accounts.Personal.superclass.initComponent.apply(this, arguments);
        
        this.getForm().on({
            beforeaction: function(f, action) {
                var msg = "";
                switch(action.type) {
                    case 'load':
                        msg = 'Loading...';
                        break;
                        
                    case 'submit':
                        msg = 'Saving...';
                        break;
                }
                
                this.tp.getEl().mask(lang(msg), 'x-mask-loading');
            },
            
            actioncomplete: function() {
                this.tp.getEl().unmask();
            },
            
            actionfailed: function() {
                OSDN.Msg.error('Some errors occured');
                this.tp.getEl().unmask();
            },
            
            scope: this
        });
    },
    
    checkStates: function() {
        if ('function' == typeof Ext.state.Manager.getProvider().isEmpty) {
            return Ext.state.Manager.getProvider().isEmpty();
        }
        
        return true;
    },
    
    onResetState: function(b) {
        Ext.state.Manager.getProvider().reset();
        b.disable();
    },
    
    doLoad: function() {
        this.getForm().load({
            url: link('accounts', 'personal', 'fetch')
        });
        
        this.resetBtnLayout.setDisabled(this.checkStates());
    },
    
    doSave: function() {
        this.getForm().submit({
            url: link('accounts', 'personal', 'update')
        });
    }
});

Ext.reg('osdn.accounts.personal', OSDN.Accounts.Personal);