Ext.ns('OSDN.Accounts');

OSDN.Accounts.ChangePassword = Ext.extend(OSDN.form.FormPanel, {
    
    permissions: acl.isUpdate('accounts', 'personal'),
    
    initComponent: function() {
        Ext.applyIf(this, {
            reader: new Ext.data.JsonReader({
                root: 'rows',
                id: 'id'
            }, ['old_password', 'new_password1', 'new_password2'])
        });
        
        this.initialConfig.reader = this.reader;
        
        this.items = [{
            fieldLabel: lang('Old password'),
            name: 'old_password',
            xtype: 'textfield',
            inputType: 'password',
            allowBlank: false
        }, {
            fieldLabel: lang('New password'),
            name: 'new_password1',
            xtype: 'textfield',
            inputType: 'password',
            allowBlank: false
        }, {
            fieldLabel: lang('Re-enter password'),
            name: 'new_password2',
            xtype: 'textfield',
            inputType: 'password',
            allowBlank: false
        }];
        
        OSDN.Accounts.ChangePassword.superclass.initComponent.apply(this, arguments);
    },
    
    show: function() {
        var w = new OSDN.window.ModalContainer({
            title: lang('Change password'),
            iconCls: 'osdn-password',
            items: this,
            buttons: [{
                text: lang('Save'),
                iconCls: 'save',
                handler: function() {
                    if (this.getForm().isValid()) {
                        this.getForm().submit({
                            waitMsg: lang('Saving...'),
                            url: link('accounts', 'personal', 'change-password'),
                            success: function(response) {
                                w.close();
                            }
                        });
                    }
                },
                scope: this
            }],
            scope: this
        });
        
        w.show();
        return w;
    }
});