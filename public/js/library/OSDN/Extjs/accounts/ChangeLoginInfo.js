Ext.ns('OSDN.Accounts');

OSDN.Accounts.ChangeLoginInfo = Ext.extend(OSDN.form.FormPanel, {
    
    entity_id: null,
    
    saveUrl: null,
    
    loadUrl: null,
    
    checkEmailUrl: null,
    
    markFieldsDirty: false,
    
    rvHandler: null,
    
    defaultType: 'textfield',
    
    initComponent: function() {
        
        this.items = [{
            xtype: 'rvfield',
            fieldLabel: lang('Email'),
            name: 'email',
            allowBlank: false, 
            vtype: 'email',
            rvHandler: this.rvHandler,
            rvOptions: {
                url: this.checkEmailUrl,
                params: {
                    id: this.entity_id
                }
            }
        }, {
            fieldLabel: lang('Password'),
            inputType: 'password',
            allowBlank: false, 
            name: 'new_password1',
            vtype: 'password'
        }, {
            fieldLabel: lang('Confirm password'),
            inputType: 'password',
            allowBlank: false, 
            name: 'new_password2',
            vtype: 'password'
        }];
        
        OSDN.Accounts.ChangePassword.superclass.initComponent.apply(this, arguments);
    },
    
    loadData: function(callback) {
        this.getForm().load({
            url: this.loadUrl,
            params: {
                id: this.entity_id
            },
            waitMsg: lang('Loading ...'),
            success: function(form, options) {
                this.fireEvent('load', this);
                if ('function' == typeof callback) {
                    callback(this);
                }
            },
            failure: function () {
                alert('Error!');
            },
            scope: this
        });
    },
    
    show: function() {
        var w = new OSDN.window.ModalContainer({
            title: lang('Set authentication settings'),
            iconCls: 'osdn-password',
            items: this,
            buttons: [{
                text: lang('Save'),
                iconCls: 'save',
                handler: function() {
                    if (this.getForm().isValid()) {
                        this.getForm().submit({
                            waitMsg: lang('Saving...'),
                            url: this.saveUrl,
                            params: {
                                id: this.entity_id
                            },
                            success: function(response) {
                                w.close();
                            }
                        });
                    }
                },
                scope: this
            }],
            scope: this
        });
        w.show();
        return w;
    }
});