Ext.ns('OSDN.grid');

OSDN.grid.GroupViewer = Ext.extend(Ext.grid.GridPanel, {
	
	groupConfig: null,
	
	fieldConfig: null,
	
	sortInfo: null,
	
	viewConfig: null,
	
	data: null,
	
	border: false,
	
	height: 200,
	
	initComponent: function() {
		
		this.hideHeaders = true;
		this.groupDir = 'ASC';
		
		this.groupConfig = Ext.applyIf(this.groupConfig || {}, {
            id: 'groupId',
            title: 'groupTitle'
		});
		
		this.fieldConfig = Ext.applyIf(this.fieldConfig || {}, {
	        name: 'name',
	        label: 'label',
	        value: 'value'
	    });
		
        var dataReader = new Ext.data.ArrayReader({}, Ext.data.Record.create([
            this.groupConfig.id,
            {name: this.groupConfig.title, type: 'string'},
			this.fieldConfig.name,
            {name: this.fieldConfig.label, type: 'string'},
            {name: this.fieldConfig.value, type: 'string'}
        ]));
        
        this.store = new Ext.data.GroupingStore({
            reader: dataReader,
            groupField: this.groupConfig.id,
            sortInfo: Ext.apply({
				field: this.groupConfig.id
			}, this.sortInfo || {}),
            data: []
        });
        
        this.view = new Ext.grid.GroupingView(Ext.apply({
            showGroupName: false,
            enableNoGroups: false,
            enableGroupingMenu: false,
            hideGroupedColumn: true,
            forceFit: true,
            scrollOffset: 19,
			groupTextTpl: ['{[values.rs[0].get("', this.groupConfig.title, '")]}'].join('')
        }, this.viewConfig || {}));
        
        this.cm = new Ext.grid.ColumnModel({
            defaults: {
                menuDisabled: true, 
                hideable: false,
                resizable: false,
                sortable: false
            },
            columns: [{
                dataIndex: this.groupConfig.id, 
				hidden: true
            }, {
                dataIndex: this.fieldConfig.label,
				renderer: function (v) {
					return [
                        '<b>',
						v,
						'<b/>'
					].join('');
				},
                fixed: true, 
                width: 150
            }, {
				id: this.autoExpandColumn = Ext.id(),
                dataIndex: this.fieldConfig.value
            }]
        });
        
        OSDN.grid.GroupViewer.superclass.initComponent.apply(this, arguments);
		
		this.on('render', function () {
			if (!OSDN.empty(this.data)) {
				this.setProperties(this.data);
			}
		}, this);
	},
	
	setProperties: function(data) {
		if (data instanceof Ext.data.Record) {
			this.getStore().loadData(data.data);
			return;
		}
        this.getStore().loadData(data);
    },
    
    showInWindow: function(cfg) {
        var w = new Ext.Window(Ext.apply({
            width: 500,
            height: 400,
            layout: 'fit',
            resizable: false,
            items: [this],
            modal: true,
            buttons: [{
                text: lang('Close'),
                handler: function() {
                    w.close();
                }
            }] 
        }, cfg || {}));
        w.show().center();
        return w;
    }
});