Ext.ns('OSDN');

/**
 * @version $Id: $
 * @class OSDN.GMap
 * @extends Ext.ux.GMapPanel
 */
OSDN.GMap = Ext.extend(Ext.ux.GMapPanel, {
	
    mapConfOpts: [
    	'enableScrollWheelZoom',
    	'enableDoubleClickZoom',
    	'enableDragging', 
    	'enableGoogleBar'
	],
    
    mapControls: [
    	'GSmallMapControl',
    	'GMapTypeControl',
    	'NonExistantControl',
    	'GOverviewMapControl'
	],
	
	address: null,
	
	zoom: null, // set addres by object to auto calculate zoom
				//	{
				//		country: null,
				//		region: null,
				//		city: null,
				//		street: null,
				//		house: null
				//	}
	
	width:586,
	
	height:430,
	
	defaultAddress: 'Holland',
	
	defaultZoom: 6,
	
	latlng: null, // array
	
	readOnly: true,
	
	_addressObj: null,

    initComponent: function() {
        OSDN.GMap.superclass.initComponent.apply(this, arguments);
        if ('object' == typeof this.address) {
        	this._addressObj = this.address; 
        }
        var zoom = this.getZoom(this.address);
        if (!this.zoom) {
        	this.zoom = zoom;
        }
        if (this.address) {
        	this.setAddress(this.address);
        } else if (this.latlng) {
        	this.latlng = new GLatLng(this.latlng[0], this.latlng[1]);
        	this.on('render', function () {
	        	this._registerEvents2.call(this,this.latlng);
	        	this._registerEvents1.call(this);
        	}, this);
        } else {
        	this.setAddress(this.defaultAddress);
        }
    },
    
    buildAddress: function (address) {
    	var addressStr = [];
    	if (typeof address == 'object') {
			if (!OSDN.empty(address.country)) {
    			addressStr.push(address.country);
			}
			if (!OSDN.empty(address.region)) {
    			addressStr.push(address.region);
			}
			if (!OSDN.empty(address.city)) {
				addressStr.push(address.city)
			}
			if (!OSDN.empty(address.street)) {
				addressStr.push(address.street);
			}
			if (!OSDN.empty(address.house)) {
	    		addressStr.push(address.house);
			}
	    	addressStr = addressStr.reverse().join(', ');
    	}
    	return addressStr;
    },
    
    getZoom: function (address) {
    	var zoom = null;
    	var addressStr = [];
    	if (typeof address == 'object') {
			if (!OSDN.empty(address.country)) {
    			zoom = 6; 
    			addressStr.push(address.country);
			}
			if (!OSDN.empty(address.region)) {
				zoom = 8;
    			addressStr.push(address.region);
			}
			if (!OSDN.empty(address.city)) {
				zoom = 12;
				addressStr.push(address.city)
			}
			if (!OSDN.empty(address.street)) {
				zoom = 17;
				addressStr.push(address.street);
			}
			if (!OSDN.empty(address.house)) {
				zoom = 18;
	    		addressStr.push(address.house);
			}
	    	addressStr = addressStr.reverse().join(', ');
    		this.address = addressStr;
    	}
    	return zoom;
    },
    
    setAddress: function(address, notShow) {
    	this.address = address;
    	this.findLatLngByAddress(this.address, function (success, gmapPanel, point, address, resO) {
			if (!success) {
				this.fireEvent('addressChanged', address, this);
				OSDN.Msg.error(lang('{0} was not found!', address));
				if (this.defaultAddress && this.defaultAddress != this.address) {
					this.setAddress(this.defaultAddress, true);
					this.zoom = this.defaultZoom;
				}
			} else {
				if (this._addressObj && !this._addressObj.country) {
	    			if (resO && resO.locationDetails && resO.locationDetails.countryName) {
	    				this._addressObj.country = resO.locationDetails.countryName;
	    				this.address = this.buildAddress(this._addressObj);
	    			}
	    		}
	    		if (this._addressObj && !this._addressObj.city) {
	    			if (resO && resO.locationDetails && resO.locationDetails.city) {
	    				this._addressObj.city = resO.locationDetails.city;
	    				this.address = this.buildAddress(this._addressObj);
	    			}
	    		}
				this.getMap().setCenter(point, this.zoom || 13);
				if (!notShow) {
					this.showMarker(point, this.address);
				}
				this._registerEvents1.call(this);
			}
	    }.createDelegate(this), (
	    	this._addressObj && (
	    		!this._addressObj.country
	    		||
	    		(!this._addressObj.city && this._addressObj.street)
			)
	    ));
    }, 
    
	showMarker: function (latlng, address) {
		if (this.marker) {
			this.marker.setPoint(latlng);
		} else {
			this.marker = new GMarker(latlng, {draggable: !this.readOnly});
			
			GEvent.addListener(this.marker, "click", function(marker) {
				this.getMap().closeInfoWindow();
				this.getMap().addOverlay(this.marker);
				this.marker.openInfoWindowHtml(this.address);
			}.createDelegate(this));
			
			if (!this.readOnly) {
				GEvent.addListener(this.marker, "dragstart", function() {
					this.getMap().closeInfoWindow();
			    }.createDelegate(this));
			
			    GEvent.addListener(this.marker, "dragend", function() {
			    	this.findAddressByLatLng(this.marker.getPoint(), function (success, resO, response) {
			    		if (success) {
			    			this.addressO = resO;
							this.address = resO.address;
			    			this.marker.openInfoWindowHtml(resO.address);
			    		}
			    	}.createDelegate(this));
			    }.createDelegate(this));
			}
		}
		this.getMap().closeInfoWindow();
		if (address) {
			this.getMap().addOverlay(this.marker);
			this.marker.openInfoWindowHtml(address);
		}
		
	},
	
	doSave: function () {
		this.fireEvent('selectedAddress', this.addressO, this);
	}, 
    
	showInWindow: function(cfg) {
        var w = new Ext.Window(Ext.apply({
        	title: lang('Google map'),
        	iconCls: 'osdn-earth',
            items: [this],
            modal: true,
            width:600,
        	height:500,
        	layput: 'fit',
            buttons: [{
                text: lang('OK'),
                handler: this.doSave.createDelegate(this),
                hidden: this.readOnly 
            }, {
                text: this.readOnly? lang('OK'): lang('Cancel'),
                handler: function() {
                    w.close();
                }
            }]
        }, cfg || {}));
        w.show();
        return w;
    },
    
    _registerEvents1: function () {
    	if (!this.readOnly) {
	    	GEvent.addListener(this.getMap(), "click", function(overlay, latlng) {
				this._registerEvents2(latlng);
			}.createDelegate(this));
    	}
	},

	_registerEvents2: function (latlng) {
		this.findAddressByLatLng(latlng, function (success, addressO, response) {
			this.addressO = addressO;
			this.address = addressO.address;
			this.showMarker(latlng, this.address);
			this.fireEvent('changelocation', this, success, addressO, response);
		}.createDelegate(this));
	}
   
});

Ext.reg('osdngmap', OSDN.GMap);