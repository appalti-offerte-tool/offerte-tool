Ext.override(Ext.grid.EditorGridPanel, {

    /**
     * Define basic permissions on editable
     * @param {Boolean}  false to disable editing
     */
    permissions: null,

    onCellDblClick : function(g, row, col) {
        if (true === OSDN.DEBUG && null == this.permissions) {
            throw 'The permissions property is not set in editor grid' + "\n"  + this.xtype;
        }

        if (true == this.permissions) {
            this.startEditing(row, col);
        }
    }
});

Ext.applyIf(String, {

    htmlspecialchars: function(val, quote_style, charset, double_encode) {
        var string = String(val);

        // Convert special characters to HTML entities
        //
        // version: 1003.2411
        // discuss at: http://phpjs.org/functions/htmlspecialchars
        // +   original by: Mirek Slugen
        // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   bugfixed by: Nathan
        // +   bugfixed by: Arno
        // +    revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +    bugfixed by: Brett Zamir (http://brett-zamir.me)
        // +      input by: Ratheous
        // +      input by: Mailfaker (http://www.weedem.fr/)
        // +      reimplemented by: Brett Zamir (http://brett-zamir.me)
        // +      input by: felix
        // +    bugfixed by: Brett Zamir (http://brett-zamir.me)
        // %        note 1: charset argument not supported
        // *     example 1: htmlspecialchars("<a href='test'>Test</a>", 'ENT_QUOTES');
        // *     returns 1: '&lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;'
        // *     example 2: htmlspecialchars("ab\"c'd", ['ENT_NOQUOTES', 'ENT_QUOTES']);
        // *     returns 2: 'ab"c&#039;d'
        // *     example 3: htmlspecialchars("my "&entity;" is still here", null, null, false);
        // *     returns 3: 'my &quot;&entity;&quot; is still here'
        var optTemp = 0, i = 0, noquotes= false;
        if (typeof quote_style === 'undefined' || quote_style === null) {
            quote_style = 2;
        }
        string = string.toString();
        if (double_encode !== false) { // Put this first to avoid double-encoding
            string = string.replace(/&/g, '&amp;');
        }
        string = string.replace(/</g, '&lt;').replace(/>/g, '&gt;');

        var OPTS = {
            'ENT_NOQUOTES': 0,
            'ENT_HTML_QUOTE_SINGLE' : 1,
            'ENT_HTML_QUOTE_DOUBLE' : 2,
            'ENT_COMPAT': 2,
            'ENT_QUOTES': 3,
            'ENT_IGNORE' : 4
        };
        if (quote_style === 0) {
            noquotes = true;
        }
        if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
            quote_style = [].concat(quote_style);
            for (i=0; i < quote_style.length; i++) {
                // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
                if (OPTS[quote_style[i]] === 0) {
                    noquotes = true;
                }
                else if (OPTS[quote_style[i]]) {
                    optTemp = optTemp | OPTS[quote_style[i]];
                }
            }
            quote_style = optTemp;
        }
        if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
            string = string.replace(/'/g, '&#039;');
        }
        if (!noquotes) {
            string = string.replace(/"/g, '&quot;');
        }

        return string;
    }
});

Ext.apply(Date, {

    /**
     * Check if day is saturday or sunday
     * @param {Number}  day The day number of week
     */
    isHoliday: function(day) {
        return 6 == day || 7 == day;
    },

    /**
     * Calculate working days between two dates
     *
     * @param {Object} startDate
     * @param {Object} endDate
     */
    workDaysCount: function(startDate, endDate) {
        startDate.clearTime();
        endDate.clearTime();

        if (endDate - startDate < 0) {
            return false;
        }

        var days = 0;
        do {
            var day = startDate.format('N');
            startDate = startDate.add(Date.DAY, 1);

            if (!this.isHoliday(day)) {
                days++;
            }
        } while (endDate - startDate >= 0);
        return days;
    }
});

Ext.form.TimeField.prototype.initDate = new Date();

Ext.override(Ext.Component, {

    initState: function(config) {
        if (Ext.state.Manager && this.stateId != undefined) {
            var state = Ext.state.Manager.get(this.stateId);
            if (state) {
                if (this.fireEvent('beforestaterestore', this, state) !== false) {
                    this.applyState(state);
                    this.fireEvent('staterestore', this, state);
                }
            }
        }
    },

    saveState: function() {
        if (Ext.state.Manager && this.stateId != undefined && this.stateful !== false) {
            var state = this.getState();
            if (this.fireEvent('beforestatesave', this, state) !== false) {
                Ext.state.Manager.set(this.stateId, state);
                this.fireEvent('statesave', this, state);
            }
        }
    }

});

Ext.override(Ext.form.BasicForm, {

    clear: function() {
        this.items.each(function(f){
            f.setValue(null);
        });
    },

    unDirty: function() {
        this.setValues(this.getValues());
    }

});

Ext.override(Ext.form.RadioGroup, {
    setValueForItem : function(val){
        val = String(val).split(',')[0];
        this.eachItem(function(item){
            item.setValue(val == item.inputValue);
        });
    }
});

Ext.override(Ext.grid.GridView, {
    handleHdMenuClick : function(item){
        var index = this.hdCtxIndex,
            cm = this.cm,
            ds = this.ds,
            id = item.getItemId();
        switch(id){
            case 'asc':
                ds.sort(cm.getDataIndex(index), 'ASC');
                break;
            case 'desc':
                ds.sort(cm.getDataIndex(index), 'DESC');
                break;
            default:
                index = cm.getIndexById(id.substr(4));
                if(index != -1){
                    if(item.checked && cm.getColumnsBy(this.isHideableColumn, this).length <= 1){
                        this.onDenyColumnHide();
                        return false;
                    }
                    cm.setHidden(index, item.checked);
                }
        }
        return true;
    }
});

/**
 * Prevent display extra symbols in mail messages, e.g. #8203
 * @link {http://www.extjs.com/forum/showthread.php?t=79190}
 */
Ext.form.HtmlEditor.prototype.defaultValue = (Ext.isOpera || Ext.isIE6) ? '&#160;' : '&nbsp;';

Ext.override(Ext.grid.RowNumberer, {

    _renderer: Ext.grid.RowNumberer.prototype.renderer,

    onBeforeRender: function(v, p, record, rowIndex, o) {},

    renderer: function(v, p, record, rowIndex){

        var o = {
            cls: "",
            qtip: "",
            bgcolor: ""
        };

        this.onBeforeRender.call(this, v, p, record, rowIndex, o);

        var cls = this.getClass.apply(this, arguments) || o.cls;
        var qtip = this.getQtip.apply(this, arguments) || o.qtip;

        var attrs = [];
        if (o.bgcolor) {
            p.cls = "";
            attrs.push('background-color:' + o.bgcolor);
            attrs.push('padding-bottom:3px !important');
        }

        p.attr += String.format('style="{0}"', attrs.join(';'));
        var i = this._renderer.apply(this, arguments);

        return String.format('<div class="{0}" qtip="{1}">{2}</div>', cls, qtip, i);
    },

    getClass: function(v, p, record, rowIndex) {
        return "";
    },

    getQtip: function() {
        return "";
    }
});