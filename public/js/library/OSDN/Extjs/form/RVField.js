Ext.ns('OSDN.form');

OSDN.form.RVField = Ext.extend(Ext.form.TextField,  {
	
    msgTarget: 'side',
    
    validClass: 'osdn-accept-icon-16x16',
    
    plugins: [Ext.ux.plugins.RemoteValidator],
    
    initComponent: function() {
        
        OSDN.form.RVField.superclass.initComponent.apply(this, arguments);
        
        this.on('invalid', function() {
            if (this.errorIcon) {
                this.setWidth(this.getWidth() - 20);
                this.errorIcon.setStyle({cursor: 'pointer'});
                this.errorIcon.on('click', function() {
                    if (this.conflictId) {
                        this.rvHandler(this.conflictId);
                    }
                }, this);
            }
        }, this, {single: true});
        
        this.on('invalid', function() {
            if (this.errorIcon) {
                this.errorIcon.removeClass(this.validClass);
            }
        }, this);
        
        this.on('valid', function() {
            if (this.errorIcon) {
                this.errorIcon.addClass(this.validClass);
                this.errorIcon.show();
            }
        }, this);
    }
});

Ext.reg('rvfield', OSDN.form.RVField);