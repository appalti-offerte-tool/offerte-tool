Ext.ns('OSDN.form');

OSDN.form.HtmlEditor = Ext.extend(Ext.form.HtmlEditor, {
    
    imageCfg: null,
    
    /**
     * @type Ext.ux.HtmlEditor.ButtonImage
     */
    imgBtn: null,
    
    allowHyperlink: true,
	
	editEl: null,
	
    initComponent: function() {
        
        if (!Ext.isArray(this.plugins)) {
            this.plugins = [];
        }
        
        if (Ext.isObject(this.imageCfg)) {
            this.imgBtn = new Ext.ux.HtmlEditor.ButtonImage(this.imageCfg);
            this.plugins.push(this.imgBtn);
        }
        
        if (this.allowHyperlink) {
            this.plugins.push(new Ext.ux.HtmlEditor.ButtonHyperlink());
        }
        
        OSDN.form.HtmlEditor.superclass.initComponent.apply(this, arguments);
        
        this.on('initialize', function(he) {
            Ext.EventManager.on(this.doc, {
                'dblclick': this.onImgDblClick,
                buffer: 100,
                scope: this
            });
        }, this);
    },
    
    getImageButton: function() {
        return this.imgBtn;
    },
    
    onImgDblClick: function(he) {

        if (!this.getImageButton()) {
            return;
        }
		var el = null;
        if (Ext.isIE) {
            var d = this.getDoc();
            var r = d.selection.createRange();
			if(d.selection.type == "Control"){
				var oControlRange = d.selection.createRange();
				for (i = 0; i < oControlRange.length; i++){
					if (oControlRange(i).tagName == "IMG"){
						this.editEl = oControlRange(i);
					}
				}
			}
        } else {
//            var w = this.getWin();
//            var selection = w.getSelection();
//            var r = selection.getRangeAt(0);
//			items = r.commonAncestorContainer.getElementsByTagName('img');
			var items = this.getImages();
			for (var i = 0; i < items.length; i++) {
				if(items[i].getAttribute('_moz_resizing') == 'true'){
					this.editEl = items[i];
					break;
				}
			}
        }
		if(!this.editEl)
			return;
		this.editEl.setAttribute('id', Ext.id());
        this.getImageButton().showImages(this.editEl);
    },
	
	getImages: function(){
        return this.getDoc().images;
        return this.doc.images;
	},

	/**
	 pass to this function object
	  {
		src: url_to_image,
		width: 100,
		height: 100,
		align: 'top',
		margin-x: 1,
		margin-y: 1,
		border: 3
	  }
	 * @param {Object} o
	 */
	insertImage: function(o){
		if(o.src.trim().length < 1){
			return;
		}
		var id = Ext.id();
		if (!o.id) {
			this.insertAtCursor('<img id="' + id + '">');
		}else{
			id = o.id;
		}
		var all_images = this.getImages();
		
		/***************/
		for (var i = 0; i < all_images.length; i++) {
		   if (all_images[i].id && all_images[i].id.indexOf(id) != -1) {
		        if (typeof(o.width) == 'string' && o.width.trim().length > 0) {
					all_images[i].setAttribute('width',o.width.trim())
				}
		        if(typeof(o.height) == 'string' && o.height.trim().length > 0) {
					all_images[i].setAttribute('height',o.height.trim())
				}
				var style = '';
				switch(o.align) {
			        case 'baseline':
			        case 'top':
			        case 'middle':
			        case 'bottom':
			        case 'text-top':
			        case 'text-bottom':
			            style="vertical-align:" + o.align + ';';
			            break;
			        case 'left':
			        case 'right':
			            style="float:" + o.align + ';';
		        }
				if(typeof(o['margin-x']) == 'number' && o['margin-x'] != 0) {
		            if(typeof(o['margin-y']) != 'number' || o['margin-y'] == 0) {
						style += 'margin:0px ' + o['margin-x'] + 'px;';
					} else {
						style += 'margin:' + o['margin-y'] + 'px ' + o['margin-x'] + 'px;';
					}
		                
		        } else {
		            if(typeof(o['margin-y']) != 'number' && o['margin-y'] != 0) {
						style += 'margin:' + o['margin-y'] + 'px 0px;';
					}
		        }
		
		        if(typeof(o.border) == 'number' && o['border'] != 0){
		            style += 'border:' + o.border + 'px;';
		        }
				if(style.trim().length > 0) {
					if (Ext.isIE) {
						all_images[i].removeAttribute('style');
						this.setElementStyle(all_images[i], style.trim())
					} else {
						all_images[i].setAttribute('style', style.trim())
					}
				}
				all_images[i].setAttribute('src',o.src);
				all_images[i].removeAttribute('id');
		   }
		}
		/***************/
		this.syncValue();
		this.pushValue();
	},
	
	// for ie
	rzCC: function (s){
		for(var exp=/-([a-z])/; 
		   exp.test(s); 
		   s=s.replace(exp,RegExp.$1.toUpperCase()));
		return s;
	},
	
	// for ie
	setElementStyle: function (element, declaration) {
		if (declaration.charAt(declaration.length-1)==';')
		 declaration = declaration.slice(0, -1);
		var k, v;
		var splitted = declaration.split(';');
		for (var i=0, len=splitted.length; i<len; i++) {
		  k = this.rzCC(splitted[i].split(':')[0]);
		  v = splitted[i].split(':')[1];
		  eval("element.style."+k+"='"+v+"'");
		
		}
	}
	
	
});

Ext.reg('osdn.form.htmleditor', OSDN.form.HtmlEditor);