Ext.ns('OSDN.form');

OSDN.form.SocialNumber = Ext.extend(Ext.form.TriggerField, {
    
    fieldLabel: lang('Socialnumber'),
    
    name:  'socialnumber',
    
    xtype: 'trigger',
    
    vtype: 'ssn',
    
    msgTarget: 'side',
    
    validClass: 'osdn-accept-icon-16x16',
    
    plugins: [Ext.ux.plugins.RemoteValidator],
    
    initComponent: function() {
        
        OSDN.form.SocialNumber.superclass.initComponent.apply(this, arguments);
        
        this.on('invalid', function() {
            if (this.errorIcon) {
                this.setWidth(this.getWidth() - 20);
                this.errorIcon.setStyle({cursor: 'pointer'});
                this.errorIcon.on('click', function() {
                    if (this.conflictId) {
                        this.rvHandler(this.conflictId);
                    }
                }, this);
            }
        }, this, {single: true});
        
        this.on('invalid', function() {
            if (this.errorIcon) {
                this.errorIcon.removeClass(this.validClass);
            }
        }, this);
        
        this.on('valid', function() {
            if (this.errorIcon) {
                this.errorIcon.addClass(this.validClass);
                this.errorIcon.show();
            }
        }, this);
    },
    
    triggerConfig: {
        tag: "img", 
        src: Ext.BLANK_IMAGE_URL, 
        cls: "x-form-trigger trigger-noborder osdn-attention",
        style: {
            borderWidth: 0
        },
        qtip: lang("If person does not have social number just type '*'!")
    },
    allowBlank: false
});

Ext.reg('osdn.form.socialnumber', OSDN.form.SocialNumber);
