OSDN.form.Spinner = Ext.extend(Ext.ux.form.Spinner, {
    
    zeroOnEmpty: true,

    initComponent: function() {
        OSDN.form.Spinner.superclass.initComponent.apply(this, arguments);
        this.on('spin', function() {
            this._validate();
        }, this);
        
        if (this.zeroOnEmpty) {
            this.on('focus', function() {
                if (this.getValue() == 0) {
                    this.setValue('');
                }
            }, this);
            this.on('blur', function() {
                if (!this.getValue()) {
                    this.setValue(0);
                    v = 0;
                }
            }, this);
        }
    },
     
    onBlur: function() {
        this._validate();
        var v = this.getValue();
        
        if(String(v) !== String(this.startValue)) {
            this.fireEvent('change', this, v, this.startValue);
            
            if (this.hiddenField) {
                this.setValue(v);
            }
        }
    },
    
    setValue: function(v) {
        if ('object' == typeof this.strategy) {
            switch (this.strategy.type) {
                case 'percent':
                case 'money':
                    this.value = v;
                    if (this.rendered) {
                        if (this.strategy.type == 'money') {
                            var v = parseFloat(v);
                        }
                        else {
                            var v = parseInt(v);
                        }
                        
                        var b = isNaN(v) ||
                        (this.strategy.minValue !== null && v < this.strategy.minValue) ||
                        (this.strategy.maxValue !== null && v > this.strategy.maxValue) ||
                        v === null ||
                        v === undefined;
                        
                        var rawValue = '';
                        if (!b) {
                            rawValue = this.strategy._format(v);
                        }
                        
                        this.setRawValue(rawValue);
                        v = rawValue
                    }
                    break;
                    
                default:
            }
            
            this.updateHidden();
        }    
        
        Ext.ux.form.Spinner.superclass.setValue.call(this, v);
        this._validate();
    },
    
    // private
    _validate: function() {
        if (this.validateOnBlur) {
            if (false === this.validate()) {
                this.markInvalid();
            } else {
                this.clearInvalid();
            }    
        }
    }
});

Ext.reg('osdnspinner', OSDN.form.Spinner);
Ext.reg('osdn.form.spinner', OSDN.form.Spinner);