Ext.ns('OSDN.form');

OSDN.form.TextField = Ext.extend(Ext.form.TextField,  {
	
	prefixSymbol: null,
	
	prefixSymbolWidth: 5,
	
	onRender : function(ct, position) {
		OSDN.form.TextField.superclass.onRender.call(this, ct, position);
		if (this.prefixSymbol !== null && this.prefixSymbol !== '' && this.prefixSymbol !== undefined) {
            this.el.dom.style.paddingLeft = (this.prefixSymbolWidth + 10).toString() + 'px';
            this.splitter = this.el.wrap().createChild({tag:'div', style:'line-height: 14px; position: relative;width:' + (this.prefixSymbolWidth + 5) + 'px;padding-left: 4px;', html: this.prefixSymbol});
			this.splitter.show().setStyle({
				paddingLeft: (Ext.isIE) ? '4px' : '5px',
				marginTop: (Ext.isIE) ? '-18px' : '-19px'
			});
        }
	}
});

Ext.reg('osdntextfield', OSDN.form.TextField);
