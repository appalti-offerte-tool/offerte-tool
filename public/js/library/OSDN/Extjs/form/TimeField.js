Ext.ns('OSDN.form');

/**
 * Time field extension
 * 
 * @version $Id: $
 */
OSDN.form.TimeField = Ext.extend(Ext.form.TimeField, {
	
	/**
     * Render the blank option in combo at first position
     * 
     * @param {Boolean}
     */
    allowBlankOption: false,
    
    /**
     * Data of blank option to override default empty values
     * 
     * @param {struct}
     */
    blankOptionData: {
		field1: '&nbsp;'
	},
    
    /**
     * Position of blank option
     * Possible values: ['first', 'last']
     * Default value: 'first'
     *  
     * @param {string}
     */
    blankOptionPosition: 'first',

	
	initComponent: function() {
		OSDN.form.TimeField.superclass.initComponent.call(this);
		if (this.allowBlankOption) {
            this.insertBlankOption();
        }
	},
	
	insertBlankOption: OSDN.form.ComboBox.prototype.insertBlankOption
}); 

Ext.reg('osdntimefield', OSDN.form.TimeField);