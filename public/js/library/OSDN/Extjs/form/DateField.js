OSDN.form.DateField = Ext.extend(Ext.form.DateField, {

    hiddenFormat: 'Y-m-d',
    
    hiddenName: null,
    
    hiddenId: null,

    hiddenValue: null,
        
    setValue : function(date) {
        Ext.form.DateField.superclass.setValue.call(this, this.formatDate(this.parseDate(date)));
        this.updateHidden();
    },
    
    updateHidden: function() {
        if (this.hiddenField) {
            var v = this.getValue();
            var value = Ext.isDate(v) ? v.format(this.hiddenFormat) : '';
            this.hiddenField.value = value;
        }
    },
    
    onRender: function() {
        OSDN.form.DateField.superclass.onRender.apply(this, arguments);
        
        if(this.hiddenName) {
            this.hiddenField = this.el.insertSibling({
                tag:'input', 
                type:'hidden', 
                name: this.hiddenName, 
                id: this.hiddenId || this.hiddenName
            }, 'before', true);
            
            var v = this.getValue();
            this.hiddenField.value = Ext.isDate(v) ? v.format(this.hiddenFormat) : '';
            this.el.dom.removeAttribute('name');
        }
    },
	
	onTriggerClick: function() {
		if(this.disabled){
            return;
        }
		OSDN.form.DateField.superclass.onTriggerClick.apply(this, arguments);
	},
    
    onBlur: function() {
        this.updateHidden();
    }
});

// deprecated
Ext.reg('osdndatefield', OSDN.form.DateField);

Ext.reg('osdn.form.datefield', OSDN.form.DateField);