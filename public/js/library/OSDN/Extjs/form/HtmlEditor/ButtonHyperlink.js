Ext.ns('Ext.ux.HtmlEditor.ButtonHyperlink');

Ext.ux.HtmlEditor.ButtonHyperlink = function(config){
	config = config || {};
	Ext.apply(this, config);
}

Ext.extend(Ext.ux.HtmlEditor.ButtonHyperlink, Ext.util.Observable, {
	
	init: function(cmp){
		
		this.cmp = cmp;
		
        this.btn = new Ext.Button({
            iconCls: 'x-edit-createlink',
            handler: this.showLinks,
            scope: this,
			tooltip: '<p><b>'+ lang('Hyperlink') + '</b></p>' + lang('Insert a hyperlink to other manual.')
        });
        this.btn.disable();

        this.cmp.on('render', this.onRender, this);
	},
	
	showLinks: function(){
		var w = new OSDN.Manuals.AddLinkWindow({
			width: 550,
			height: 260
		});
		w.on('onChose', function(v){
			this.cmp.insertAtCursor(v);
			w.hide();
		}, this)
		w.show();
	},
	
	onRender: function(){
		this.cmp.getToolbar().addButton([new Ext.Toolbar.Separator()]);
		this.cmp.getToolbar().addButton(this.btn);
    }
});