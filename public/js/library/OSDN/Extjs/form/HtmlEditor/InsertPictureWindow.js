Ext.ns('OSDN.form.HtmlEditor');

OSDN.form.HtmlEditor.InsertPictureWindow = Ext.extend(Ext.Window ,{
	
	loadUrl: null,
	
	modal: true,
    
    loadBaseParams: null,
    
    updateUrl: null,
    
    updateBaseParams : null,
    
    deleteUrl: null,
    
    deleteBaseParams: null,
	
    title: lang('Insert image'),
	
    width:420,
	
    height:243,
	
    layout:"fit",
	
	editElement: null,
	
	editElementId: null,
	
    initComponent: function(){
		
        this.onChange = {
            change:{
				fn: function() {
					this.generateCode();
                },
				scope: this
            }
        };
		
        this.buttons = [{
            xtype:'progress',
            width:220,
			hidden:true
        }, {
            text:'Insert',
            handler:function() {
                if(this.callback) {
					this.callback.call(this.scope || this, this.getFormRealValues());
                }
                this.hide();
            },
			scope:this
        }, {
            text:'Cancel',
			handler:
            function() {
                this.hide();
            },
			scope:this
        }];
		
		var evalErrors = function(res, responseHTML) {
            if (res && res.errors && res.errors[0]['id'] == 'file') {
                Ext.Msg.show({
                    title: lang('Error'),
                    msg: lang('File type is not allowed!'),
                    buttons: Ext.Msg.OK,
                    fn: function (b){},
                    icon: Ext.MessageBox.ERROR
                });
            } else {
                Ext.Msg.show({
                    title: lang('Error'),
                    msg: lang('Unknown error occured!'),
                    buttons: Ext.Msg.OK,
                    fn: function (b){},
                    icon: Ext.MessageBox.ERROR
                });
                
                if (OSDN.DEBUG) {
                    if (console && console.log) {
                        console.log(responseHTML);
                    }
                }
            }
        }
		
        this.items = [{
            xtype:'form',
			border:false,
			layout:'vbox',
            layoutConfig:{
                align:"stretch",
                padding:"0"
            },
            defaults:{
                layout: "hbox",
				border: false,
				xtype: 'panel',
                layoutConfig: {
                    padding:"5"
                },
                defaults:{
                    margins: '0 5 0 0',
                    xtype: "textfield"
                }
            },
            items: [{
                items:[{
                        xtype:"label",
                        text:"URL:",
                        style:"line-height:22px;",
                        width:70
                    }, {
                        width: 200,
						name: 'src',
                        listeners: {
				            change: function() {
                                this.generateCode();
				            },
							scope: this
				        },
						scope: this
                    }, {
                        xtype: "button",
                        text: lang('View'),
                        handler: function() {
                            var uIO = new OSDN.Images.List({
				                loadUrl: this.loadUrl,
				                loadBaseParams: this.loadBaseParams,
				                updateUrl: this.updateUrl,
				                updateBaseParams : this.updateBaseParams,
				                deleteUrl: this.deleteUrl,
				                deleteBaseParams: this.deleteBaseParams
				            });
				            var w = uIO.showInWindow();
				            uIO.on({
								selected: function (data, uIO) {
									this.setSrc(data.imagepath);
									uIO.getEl().mask(lang('Loading...'), 'x-mask-loading');
									(function () {
										w.close();
									}).defer(1000);
									
					            }, 
								scope: this
                            });
                        },
						scope:this
                    }, {
			            uploadUrl: this.updateUrl,
						xtype: 'OSDN.UploadButton',
			            text: lang('Upload'),
			            tooltip: lang('You can upload next formats: {0}!', ['jpg', 'jpeg', 'png', 'gif'].join(', ')),
			            parameters: function () {
							return this.updateBaseParams;
						}.createDelegate(this),
			            listeners: {
			                afterupload: function (self, response, options) {
			                    var res = Ext.decode(response.responseText);
			                    if (res.success) {
									this.setSrc(res.data.imagepath);
			                        return;
			                    }
			                    evalErrors(res, response.responseText);
			                },
			                beforeupload: function () {
//			                    if (!this.form.getForm().isValid()) {
//			                        return false;
//			                    }
			                },
			                failedupload: function (a,b,c) {
			                },
							scope: this
			            },
						scope: this
			        }
                ]
            }, {
                items:[{
                    xtype: "label",
                    text: "Width:",
                    style: "line-height:22px;",
                    width: 70
                }, {
                    width: 90,
					name: 'width',
                    listeners: this.onChange
                }, {
                    xtype:"label",
                    text:"Height:",
                    style:"line-height:22px;",
                    width:70
                }, {
                    width:90, 
					name:'height',
                    listeners: this.onChange
                }]
            }, {
                items:[{
                    xtype:"label",
                    text:"Alignment:",
					style:"line-height:22px;",
                    width:70
                }, {
                    xtype: "combo",
                    triggerAction: "all",
                    mode: 'local',
                    displayField: 'desc',
                    valueField: 'name',
					name:'align',
                    store: new Ext.data.ArrayStore({
                        fields: ['name', 'desc'],
                        data : [[null, '--none--'],['baseline', ' Baseline'],['top', 'Top'],['middle','Middle'],['bottom','Bottom'],['text-top','Text top'],['text-bototm','Text bottom'],['left','Left'],['right','Right']]
                    }),
                    width: 90,
					value: null,
                    listeners: this.onChange
                }, {
                    xtype: "label",
                    text: "Border:",
					style:"line-height:22px;",
                    width:70
                }, {
                    xtype: 'numberfield',
					width: 50,
					name: 'border',
                    listeners: this.onChange
                }, {
                    xtype:"label", 
					text:"px",
					style:"line-height:22px;"
                }]
            }, {
                items:[{
                    xtype:"label",
                    text:"Dimensions:",
                    style:"line-height:22px;",
                    width:70
                }, {
                    xtype: 'numberfield',
					width: 50,
					name: 'margin-x',
                    listeners: this.onChange
                }, {
                    xtype: "label",
                    text: "px",
                    style: "line-height:22px;"
                }, {
                    xtype: 'numberfield',
					width: 50,
					name: 'margin-y',
                    listeners: this.onChange
                }, {
                    xtype: "label",
                    text: "px",
                    style: "line-height:22px;"
                }]
            }, {
                items:[{
                    xtype: "label",
                    text: "Code:",
                    style: "line-height:22px;",
                    width: 70
                }, {
                    width: 320,
					name: 'code'
                }]
            }]
        }];
		
        OSDN.form.HtmlEditor.InsertPictureWindow.superclass.initComponent.apply(this, arguments);
        this.on('show',function(){
            var f = this.findByType('form')[0];
            f.form.reset();
			this.pharseObj();
            this.generateCode();
        }, this);
    },
	
	/////////////////////////  special sollution for IE to find style of element
	getStyle: function(oElm, strCssRule){
		var strValue = "";
		if(document.defaultView && document.defaultView.getComputedStyle){
			strValue = document.defaultView.getComputedStyle(oElm, "").getPropertyValue(strCssRule);
		}
		else if(oElm.currentStyle){
			strCssRule = strCssRule.replace(/\-(\w)/g, function (strMatch, p1){
				return p1.toUpperCase();
			});
			strValue = oElm.currentStyle[strCssRule];
		}
		return strValue;
	},
	/////////////////////////  special sollution for IE to find style of element
	
	pharseObj: function(){
		var o = this.getFormRealValues(true);
		if(this.editElement.type != 'button' && this.editElement.tagName.toLowerCase() == 'img'){
			
			this.editElementId = this.editElement.getAttribute('id');
			var src = this.editElement.getAttribute('src');
			if(src) o.src = src;
			var height = this.editElement.getAttribute('height');
			if(height) o.height = height;
			var width = this.editElement.getAttribute('width');
			if(width) o.width = width;
			
			var re = new RegExp('[1-9]');
			
			if (Ext.isIE) {
				var margin = this.getStyle(this.editElement, 'margin')
				var m = re.exec(margin);
				if (m) {
					o['margin-y'] = m[0];
					var m = re.exec(margin.substr(m['index'] + 1));
					if (m) {
						o['margin-x'] = m[0];
					}
				}
			
				var align = this.getStyle(this.editElement, 'vertical-align');
				if(align != 'auto'){
					o.align = align;
				}else{
					o.align = this.getStyle(this.editElement, 'styleFloat');
				}
				
//				possible choises of getting border in IE
//				borderRightWidth
//				borderLeftWidth
//				borderBottomWidth
//				borderTopWidth
				var border = this.getStyle(this.editElement, 'borderRightWidth')
				if(border){
					o.border = border;
				}
			}
			else {
				
				var style = this.editElement.getAttribute('style')
				if (style) {
					style = style.split(';');
					for (var i = 0; i < style.length; i++) {
					
						if (style[i].trim().toLowerCase().search('border') >= 0) {
						
							var m = re.exec(style[i]);
							if (m) 
								o.border = m[0];
							
						}
						else 
							if (style[i].trim().toLowerCase().search('margin') >= 0) {
							
								var m = re.exec(style[i]);
								if (m) {
								
									o['margin-y'] = m[0];
									var m = re.exec(style[i].substr(m['index'] + 1));
									if (m) {
										o['margin-x'] = m[0];
									}
									
								}
								
							}
							else 
								if (style[i].trim().toLowerCase().search('vertical-align') >= 0 ||
								style[i].trim().toLowerCase().search('float') >= 0) {
								
									var aling = style[i].trim().toLocaleString().split(': ');
									if (aling && aling[1]) 
										o.align = aling[1];
									
								}
					}
				}
			}
		}
		this.getForm().setValues(o);
	},
	
    generateCode: function() {
        var f = this.findByType('form')[0];
        var o = this.getFormRealValues(true);
        if (typeof(o.src) != 'string' || o.src.trim().length < 1) {
            this.buttons[1].setDisabled(true);return;
        }
        this.buttons[1].setDisabled(false);

        var str = '<img src="' + o.src + '"'
        if (typeof(o.width) == 'string' && o.width.trim().length > 0) {
			str += ' width="' + o.width.trim() + '"';
		}
        var style = '';
        if(typeof(o.height) == 'string' && o.height.trim().length > 0) {
			str+=' height="' + o.height.trim() + '"';
		}
            
        switch(o.align) {
	        case 'baseline':
	        case 'top':
	        case 'middle':
	        case 'bottom':
	        case 'text-top':
	        case 'text-bottom':
	            style="vertical-align:" + o.align + ';';
	            break;
	        case 'left':
	        case 'right':
	            style="float:" + o.align + ';';
        }
            
        if(typeof(o['margin-x']) == 'number' && o['margin-x'] != 0) {
            if(typeof(o['margin-y']) != 'number' || o['margin-y'] == 0) {
				style += 'margin:0px ' + o['margin-x'] + 'px;';
			} else {
				style += 'margin:' + o['margin-y'] + 'px ' + o['margin-x'] + 'px;';
			}
                
        } else {
            if(typeof(o['margin-y']) != 'number' && o['margin-y'] != 0) {
				style += 'margin:' + o['margin-y'] + 'px 0px;';
			}
        }

        if(typeof(o.border) == 'number' && o['border'] != 0){
            style += 'border:' + o.border + 'px;';
        }
		
        if(style.trim().length > 0) {
			str+=' style="' + style + '"';
		}

        str += ' />';

        f.form.setValues({
			code: str
        });
    },
	getForm: function(){
		return this.findByType('form')[0].form;
	},
	
    getFormRealValues: function(flag) {
        var form = this.findByType('form')[0].form;
        var values = {};
        form.items.each(function(f) {
            if('code' != f.getName()) {
				values[f.getName()] = f.getValue();
			}
        });
		if(this.editElementId)
			values.id = this.editElementId;
		if (!flag) {
			this.editElementId = null;
			this.editElement = null;
		}
        return values;
    },
	
	setSrc: function (value) {
		var form = this.findByType('form')[0].form;
		var srcField = form.findField('src');
		if (srcField) {
            srcField.setValue(value);			
		}
		this.generateCode();
	},
	
    getCodeValue: function() {
        var ret = null;
        var form = this.findByType('form')[0].form;
        form.items.each(function(f) {
            if('code' == f.getName()) {
                var v = f.getValue();
                if(typeof(v) == 'string' && v.trim().length > 0) {
					ret = v;
				}
            }
        });
        return ret;
    }
})