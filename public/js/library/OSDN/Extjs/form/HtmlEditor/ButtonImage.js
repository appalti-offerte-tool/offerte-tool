Ext.ns('Ext.ux.HtmlEditor.ButtonImage');

Ext.ux.HtmlEditor.ButtonImage = function(config){
	config = config || {};
	Ext.apply(this, config);
}

Ext.extend(Ext.ux.HtmlEditor.ButtonImage, Ext.util.Observable, {

    loadUrl: null,
	
	loadBaseParams: null,
	
	updateUrl: null,
	
	updateBaseParams : null,
	
	deleteUrl: null,
	
	deleteBaseParams: null,
	
	baseParams: null,
	
	directSelect: false,

    init: function(cmp){
		this.cmp = cmp;
		
		this.baseParams = Ext.applyIf(this.baseParams || {}, {
            width: 150,
            height: 150
        }); 
        
        this.loadBaseParams = Ext.applyIf(this.loadBaseParams || {}, this.baseParams);
        this.updateBaseParams = Ext.applyIf(this.updateBaseParams || {}, this.baseParams);
        this.deleteBaseParams = Ext.applyIf(this.deleteBaseParams || {}, this.baseParams);
		
        this.btn = new Ext.Button({
            iconCls       : 'x-edit-image',
            handler: this.showImages,//this.show.createDelegate(this)
            scope        : this,
            tooltip      : {
                title: lang('Insert image')
            },
            overflowText : lang('Insert image')
        });
        this.btn.disable();

        this.cmp.on('render', this.onRender, this);
    },
	
	setBaseParams: function (params) {
        this.baseParams = Ext.apply(this.baseParams, params || {});
        this.loadBaseParams = Ext.apply(this.loadBaseParams, params || {});
        this.updateBaseParams = Ext.apply(this.updateBaseParams, params || {});
        this.deleteBaseParams = Ext.apply(this.deleteBaseParams, params || {});
    },
	
	showImages: function (el) {
		if (this.directSelect) {
			var uIO = new OSDN.Images.List({
	            loadUrl: this.loadUrl,
	            loadBaseParams: this.loadBaseParams,
	            updateUrl: this.updateUrl,
	            updateBaseParams : this.updateBaseParams,
	            deleteUrl: this.deleteUrl,
	            deleteBaseParams: this.deleteBaseParams
	        });
	        var w = uIO.showInWindow();
	        uIO.on('selected', function (data, uIO) {
				this.cmp.insertAtCursor([
                    '<img src="', data.imagepath, '" />'
                ].join(''));
                w.close(200);
	            //this.cmp.relayCmd('insertimage', data.imagepath);
	            //this.cmp.append('<img src="' + data.imagepath + '" style="width: 250px; height: 190px;" />');
	        }, this);
		} else {
			var ipW = new OSDN.form.HtmlEditor.InsertPictureWindow({
				loadUrl: this.loadUrl,
                loadBaseParams: this.loadBaseParams,
                updateUrl: this.updateUrl,
                updateBaseParams : this.updateBaseParams,
                deleteUrl: this.deleteUrl,
                deleteBaseParams: this.deleteBaseParams,
				callback: function (o) {
					this.cmp.insertImage(o);
				},
				editElement: el,
				scope: this
			});
			ipW.show().center();
		}
	},

//    show: Ext.ux.HtmlEditor.ButtonImage.prototype.showImages,
    
    onRender: function(){
		this.cmp.getToolbar().addButton([new Ext.Toolbar.Separator()]);
		this.cmp.getToolbar().addButton(this.btn);
    }
});