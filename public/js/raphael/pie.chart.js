Raphael.fn.pieChart = function (cx, cy, r, chartData) {
    var paper = this,
        rad = Math.PI / 180,
        chart = this.set(),
        colorVal = {
            red: '#ca0000',
            green: '#469d2b',
            orange: '#f57920',
            blue: '#168bef'
        };

    function sector(cx, cy, r, startAngle, endAngle, params) {
        var x1 = cx + r * Math.cos(-startAngle * rad),
            x2 = cx + r * Math.cos(-endAngle * rad),
            y1 = cy + r * Math.sin(-startAngle * rad),
            y2 = cy + r * Math.sin(-endAngle * rad);

        var el = (!startAngle && endAngle == 360) ?
                paper.circle(cx, cy, r) :
                paper.path(["M", cx, cy, "L", x1, y1, "A", r, r, 0, +(endAngle - startAngle > 180), 0, x2, y2, "z"]);

        return el.attr(params);
    }

    var angle = 0,
        total = 0,
        start = 0,
        textParams = {
            fill: "#fff",
            "font-family": 'Tahoma',
            "font-size": "14px",
            "font-weight": "700"
        },
        process = function (j) {
            var value = chartData.values[j],
                angleplus = 360 * value / total,
                popangle = angle + (angleplus / 2),
                ms = 500,

                p = sector(cx, cy, r, angle, angle + angleplus, {
                    fill:  colorVal[chartData.colors[j]],
                    stroke: "#fff",
                    "stroke-width": 2,
                    title: chartData.titles ? chartData.titles[j] : ''
                });

            if (chartData.animate) {
                p.mouseover(function () {
                    p.stop().animate({transform: "s1.1 1.1 " + cx + " " + cy}, ms, "elastic");
                }).mouseout(function () {
                    p.stop().animate({transform: ""}, ms, "elastic");
                });
            }

            if (chartData.showValues) {
                var textOffset = angleplus !== 360 ? (r / 2) : 0,
                    txtShadow = paper.text(
                        cx + 1 + textOffset * Math.cos(-popangle * rad),
                        cy + textOffset * Math.sin(-popangle * rad),
                        chartData.values[j] + '%'
                    )
                        .attr($.extend({}, textParams, { fill: "#000" })),

                    txt = paper.text(
                        cx + textOffset * Math.cos(-popangle * rad),
                        cy + textOffset * Math.sin(-popangle * rad),
                        chartData.values[j] + '%'
                    )
                    .attr(textParams);
            }

            angle += angleplus;
            chart.push(p);

            if (chartData.showValues) {
                chart.push(txtShadow);
                chart.push(txt);
            }
            start += .1;
        };

    for (var i = 0, ii = chartData.values.length; i < ii; i++) {
        total += chartData.values[i];
    }

    for (var i = 0; i < ii; i++) {
        process(i);
    }

    return chart;
};

$(function() {
    $('.raphael').each(function() {
        var $el = $(this);

        Raphael(this, $el.width(), $el.height())
        .pieChart($el.width() / 2, $el.height() / 2, $el.height() / 2, $el.data());
    });
});