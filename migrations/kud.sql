
CREATE TABLE IF NOT EXISTS `proposalBlockDefinitionFileStockRel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `proposalId` int(11) unsigned NOT NULL,
  `fileStockId` int(11) unsigned NOT NULL,
  `isThumbnail` int(1) unsigned NOT NULL,
  `parentId` int(1) unsigned DEFAULT NULL,
  `isDefaultThumbnail` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UX_proposalId` (`proposalId`,`fileStockId`),
  KEY `IX_fileStockId` (`fileStockId`),
  KEY `IX_proposalId` (`proposalId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=334 ;

ALTER TABLE `proposalBlockDefinitionFileStockRel` ADD FOREIGN KEY ( `proposalId` ) REFERENCES `test_appalti_nl`.`proposal` (
`id`
);

ALTER TABLE `proposalBlockDefinitionFileStockRel` ADD FOREIGN KEY ( `fileStockId` ) REFERENCES `test_appalti_nl`.`fileStock` (
`id`
);

ALTER TABLE `proposalBlockDefinitionFileStockRel` ADD `sectionId` INT( 11 ) UNSIGNED NOT NULL AFTER `proposalId`;

ALTER TABLE `proposal` CHANGE `contactMomentDatetime` `contactMomentDate` DATE NULL DEFAULT NULL;

ALTER TABLE `proposalTheme` ADD `includeDiscountPerOffer` INT( 1 ) NOT NULL DEFAULT '0',
ADD `includeDiscountPerOfferText` TEXT NULL;

--test.appalti.nl last

CREATE TABLE IF NOT EXISTS `idealTransaction` (
`id` int( 11 ) unsigned NOT NULL AUTO_INCREMENT ,
`transactionId` varchar( 16 ) NOT NULL ,
`merchantId` int( 10 ) unsigned NOT NULL DEFAULT '0',
`description` varchar( 64 ) NOT NULL ,
`orderId` varchar( 64 ) NOT NULL ,
`name` varchar( 35 ) DEFAULT NULL ,
`city` varchar( 24 ) DEFAULT NULL ,
`accountId` varchar( 10 ) DEFAULT NULL ,
`amount` mediumint( 9 ) DEFAULT NULL ,
`status` varchar( 9 ) DEFAULT NULL ,
`created` int( 11 ) DEFAULT NULL ,
`updated` int( 11 ) DEFAULT NULL ,
`entranceCode` varchar( 40 ) DEFAULT NULL ,
`expirationPeriod` smallint( 6 ) DEFAULT NULL ,
`redirectSuccess` varchar( 255 ) DEFAULT NULL ,
`redirectError` varchar( 255 ) DEFAULT NULL ,
`issuerAuthenticationUrl` mediumtext,
PRIMARY KEY ( `id` ) ,
UNIQUE KEY `id` ( `id` )
) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT =114;

ALTER TABLE `idealTransaction` CHANGE `created` `createdDatetime` DATETIME NOT NULL;
ALTER TABLE `idealTransaction` CHANGE `updated` `updatedDatetime` DATETIME NOT NULL;

CREATE TABLE IF NOT EXISTS `supportMinutes` (
`id` int( 11 ) unsigned NOT NULL AUTO_INCREMENT ,
`name` varchar( 255 ) NOT NULL ,
`amount` int( 11 ) unsigned NOT NULL DEFAULT '0',
`price` float DEFAULT '0',
PRIMARY KEY ( `id` )
) ENGINE = InnoDB DEFAULT CHARSET = utf8;


CREATE TABLE IF NOT EXISTS `cart` (
`id` int( 11 ) unsigned NOT NULL AUTO_INCREMENT ,
`companyId` int( 11 ) unsigned NOT NULL ,
`itemId` int( 11 ) unsigned NOT NULL ,
`kind` enum( 'membership', 'support-minutes' ) NOT NULL ,
`description` text,
`price` float NOT NULL DEFAULT '0',
`methodId` int( 11 ) unsigned DEFAULT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = InnoDB DEFAULT CHARSET = utf8;


CREATE TABLE IF NOT EXISTS `paymentMethod` (
`id` int( 11 ) unsigned NOT NULL AUTO_INCREMENT ,
`name` varchar( 50 ) NULL ,
PRIMARY KEY ( `id` )
) ENGINE = InnoDB DEFAULT CHARSET = utf8;


ALTER TABLE `cart` ADD CONSTRAINT `cart_paymentMethod_ibfk_1` FOREIGN KEY ( `methodId` ) REFERENCES `paymentMethod` ( `id` ) ;

ALTER TABLE `cart` ADD CONSTRAINT `cart_company_ibfk_1` FOREIGN KEY ( `companyId` ) REFERENCES `company` ( `id` ) ;

ALTER TABLE `cart` ADD `status` ENUM( 'PENDING', 'SUCCESS', 'CANCELLED', 'FAILURE', 'OPEN', 'EXPIRED' ) NOT NULL ;

ALTER TABLE `cart` ADD `createdDatetime` DATETIME NOT NULL ,
ADD `updatedDatetime` DATETIME NOT NULL;

ALTER TABLE `cart` ADD `accountId` INT( 11 ) NOT NULL AFTER `companyId`;

ALTER TABLE `cart` ADD UNIQUE (
`accountId`
);

ALTER TABLE `cart` CHANGE `accountId` `accountId` INT( 11 ) UNSIGNED NOT NULL;

ALTER TABLE `cart` ADD FOREIGN KEY ( `accountId` ) REFERENCES `account` (
`id`
);

ALTER TABLE `cart` DROP INDEX `accountId` ,
ADD INDEX `accountId` ( `accountId` );

ALTER TABLE `idealTransaction` CHANGE `orderId` `cartId` INT( 11 ) UNSIGNED NOT NULL;
ALTER TABLE `idealTransaction` ADD INDEX ( `cartId` );

ALTER TABLE `idealTransaction` ADD FOREIGN KEY ( `cartId` ) REFERENCES `cart` (
`id`
);

ALTER TABLE `idealTransaction` ADD UNIQUE (
`transactionId`
);

ALTER TABLE `cart` CHANGE `updatedDatetime` `updatedDatetime` DATETIME NULL;

ALTER TABLE `servicePrice` ADD UNIQUE (
`membershipTypeId` ,
`serviceId`
);

ALTER TABLE `servicePricePriorityRel` ADD UNIQUE (
`serviceId` ,
`priority`
);

--test.appalti.nl

ALTER TABLE `contactPerson` ADD `isActive` INT( 1 ) UNSIGNED NOT NULL DEFAULT '1'


