<?php

/**
 * Main bootstrap class
 *
 * Initialize all resources and set additional options
 *
 * !!! ATTENTION !!!
 * When bootstrap runned methods executed in order how
 * it described in class
 * Do not move possition of method because you can
 * get critical possibility for errors
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initErrorDisplay(){
        $frontController = Zend_Controller_Front::getInstance();
        $frontController->throwExceptions(true);
    }

    public function _initDbable()
    {
        $this->bootstrap('db');
        $db = $this->getPluginResource('db');

        Zend_Db_Table_Abstract::setDefaultAdapter($db->getDbAdapter());
        OSDN_Application_Service_Dbable::setDefaultDbAdapter($db->getDbAdapter());
    }

    public function _initModulees()
    {
        require_once APPLICATION_PATH . '/modules/module/resources/Modulesmanager.php';

        $coreModulesPath = APPLICATION_PATH . '/modules';
        $this->getPluginLoader()->addPrefixPath(
            'Module_Resource_',
            $coreModulesPath . '/module/resources'
        );

        $mmPaths = array(
            $coreModulesPath,
            APPLICATION_PATH . '/modules-cms',
            APPLICATION_PATH . '/modules-appalti',
            APPLICATION_PATH . '/modules-components',
            APPLICATION_PATH . '/modules-finance'
        );

        $this->registerPluginResource('ModulesManager', array('paths' => $mmPaths));
        $this->bootstrap('ModulesManager');
    }

    public function _initAuth()
    {
        $auth = Zend_Auth::getInstance();
        $account = null;
        if ($auth->getStorage()->isEmpty()) {
            $account = Account_Model_DbTable_AccountRow::initWithGuest();
            $auth->getStorage()->write($account);
        } else {
            $account = $auth->getStorage()->read();
        }
        $account->refresh();

        $cm = $this->bootstrap('CacheManager')->getResource('CacheManager');
        if (null !== $cm && $cm->hasCache('filecache')) {
            Account_Service_Authenticate::setCache($cm->getCache('filecache'));
        }

        $authenticate = new Account_Service_Authenticate();
        $authenticate->initAclObject($account);
    }

    public function _initI18n()
    {
        $cm = $this->getResource('Cachemanager');
        if ($cm->hasCache('filecache')) {
            Zend_Translate::setCache($cm->getCache('filecache'));
            Zend_Locale::setCache($cm->getCache('filecache'));
        }

        $account = Zend_Auth::getInstance()->getIdentity();

        $locale = new Zend_Locale($account->getLanguage() ?: 'nl');
        Zend_Registry::set('Zend_Locale', $locale);
        Zend_Locale::setDefault($locale);


        $translate = new Zend_Translate(array(
            'adapter'     => 'OSDN_Translation_Adapter_Mysql',
            'content'     => true,
            'locale'      => $locale,
            'log'         => $this->bootstrap('log')->getResource('log')
        ));

        Zend_Registry::set('Zend_Translate', $translate);
    }

    public function _initBootstrapPlugins()
    {
        $this->bootstrap('frontcontroller');
        $f = $this->getResource('frontcontroller');

        $f->registerPlugin(new Zend_Controller_Plugin_ErrorHandler(array(
            'module' => 'default'
        )), 100);
        $f->registerPlugin(new Default_Controller_Plugin_Bootstrap());
        $f->registerPlugin(new OSDN_Controller_Plugin_ViewFilter());

        /**
         * @FIXME
         */
        Zend_Controller_Action_HelperBroker::addPath(
            APPLICATION_PATH . '/modules/application/controllers/helpers',
            'Application_Controller_Helper_'
        );
    }

    public function _initFireBugLogger()
    {
        $writer = new Zend_Log_Writer_Firebug();
        $logger = new Zend_Log( $writer );
        Zend_Registry::set( 'logger', $logger );
    }
}