<?php

/**
 * Flush APC cache in CLI mode
 */

error_reporting(E_ALL | E_STRICT);

define('SILENT_MODE', true);

/**
 * @var Zend_Application
 */
$application = require __DIR__ . '/../../index.php';

/**
 * Here we create the bootstrap for get
 * easy access for all configured caches
 *
 * @var Zend_Application_Bootstrap_Bootstrap
 */
$application->bootstrap();

$bootstrap = $application->getBootstrap();

$cm = $bootstrap->getResource('cachemanager');

foreach($cm->getCaches() as $cache) {
    $cache->clean();
}

if (empty($_SERVER['argc']) || 2 != $_SERVER['argc']) {
    throw new Exception('Wrong arguments count');
}

/**
 * Refresh proxy objects
 */
require_once __DIR__ . '/proxy.php';

if (extension_loaded('apc')) {
    $host = $_SERVER['argv'][1];
    $result = fopen('http://' . $host . '/installation/rsync/apc.php?hash=' . md5($host), 'r');
}