<?php

error_reporting(E_ALL | E_STRICT);

if (! defined('SILENT_MODE')) {
    define('SILENT_MODE', true);

    /**
     * @var Zend_Application
     */
    $application = require __DIR__ . '/../../index.php';
}

if (!$application instanceof Zend_Application) {
    throw new Exception('$application should be instance of Zend_Application');
}
$application->bootstrap();

$em = OSDN_Application_Service_Doctrine::getDefaultEntityManager();
$mm = $application->getBootstrap()->getResource('ModulesManager');

$paths = array();
foreach($mm->fetchAll() as $information) {
    $modelsPath = $information->getRoot() . '/models';
    if (file_exists($modelsPath)) {
        $paths[] = $modelsPath;
    }

    $doctrinePath = $modelsPath . '/Doctrine';
    if (file_exists($doctrinePath)) {
        $paths[] = $doctrinePath;
    }
}

$em->getConfiguration()->getMetadataDriverImpl()->addPaths($paths);

$em->getProxyFactory()->generateProxyClasses(
    $em->getMetadataFactory()->getAllMetadata(),
    $em->getConfiguration()->getProxyDir()
);