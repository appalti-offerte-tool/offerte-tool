<?php

error_reporting(E_ALL | E_STRICT);

/**
 * In EFIS application we use APC caching
 * When we deploy new version we also need to flush cache
 * in APC.
 *
 * According to @link: http://stackoverflow.com/questions/439262/how-can-i-get-php-to-use-the-same-apc-cache-when-invoked-on-the-cli-and-the-web
 * The CLI and Apache use different APC caches.
 * So, we need run this script in CGI or Apache request
 */
if (empty($_GET['hash']) || 0 !== strcasecmp($_GET['hash'], md5($_SERVER['HTTP_HOST']))) {
    throw new Exception('Secure hash mismatch');
}

if (extension_loaded('apc')) {
    apc_clear_cache();
    apc_clear_cache('user');
    apc_clear_cache('opcode');
}