<?php

error_reporting(E_ALL | E_STRICT);

if (empty($_GET['p']) || empty($_GET['v'])) {
    return;
}

$packageRequest = $_GET['p'];
$versionRequest = $_GET['v'];

require_once __DIR__ . '/../../library/jsmin-php/jsmin-1.1.1.php';

/**
 * Retrieve package and compress it
 */

define('SILENT_MODE', true);

/**
 * @var Zend_Application
 */
$application = require __DIR__ . '/../../index.php';

/**
 * Here we create the bootstrap for get
 * easy access for all configured caches
 *
 * @var Zend_Application_Bootstrap_Bootstrap
 */
$application->bootstrap();

$bootstrap = $application->getBootstrap();

$mm = $bootstrap->getResource('ModulesManager');

list($module, $package, $subpackage) = explode(':', $packageRequest);

$information = $mm->fetch($module);
$path = $information->getRoot() . '/configs/resource.xml';


$bind = new OSDN_View_Bind($path);
$includes = $bind->getIncludes(OSDN_View_Bind_Js::TYPE, function(OSDN_View_Bind_Abstract $bind) use ($package, $subpackage) {
    return 0 === strcasecmp($package, $bind->getPackage()) && 0 == strcasecmp($subpackage, $bind->getSubpackage());
});

ob_start();

$output = array();
foreach($includes as $include) {

    if (!$include->isCacheable()) {
        continue;
    }

    $include->setBaseUrl('/');
    $jsContent = $include->toString();

    if ($include->isMinifiable()) {
        $jsContent = JSMin::minify($jsContent);
    }

    echo $jsContent;
}


$token = APPLICATION_PATH . sprintf('/data/public-bind-cache/%s-v%s.js', basename($packageRequest), $versionRequest);
file_put_contents($token, ob_get_contents());

header('Content-type: text/javascript');

ob_flush();