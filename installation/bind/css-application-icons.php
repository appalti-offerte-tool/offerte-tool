<?php

error_reporting(E_ALL | E_STRICT);

if (empty($_GET['v'])) {
    return;
}

$versionRequest = $_GET['v'];

/**
 * Retrieve package and compress it
 */

define('SILENT_MODE', true);

/**
 * @var Zend_Application
 */
$application = require __DIR__ . '/../../index.php';

/**
 * Here we create the bootstrap for get
 * easy access for all configured caches
 *
 * @var Zend_Application_Bootstrap_Bootstrap
 */
$application->bootstrap();

$bootstrap = $application->getBootstrap();

$mm = $bootstrap->getResource('ModulesManager');

ob_start();

foreach($mm->fetchAll() as $information) {

    $path = dirname($information->getRoot());
    $moduleBasePath = '/' . basename($path) . '/' . $information->getName();

    printf('.m-%s-icon-16 {background: url(%s/views/themes/default/images/module-icon-16.png) no-repeat;}', $information->getName(), $moduleBasePath);
}

$token = APPLICATION_PATH . sprintf('/data/public-bind-cache/application-icons-v%s.css', basename($versionRequest));
file_put_contents($token, ob_get_contents());

header('Content-type: text/css');

ob_flush();