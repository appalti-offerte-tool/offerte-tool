<?php
class Appalti_File
{
    public static function copy($src, $dst)
    {
        if (!is_file($src) && !is_dir($src))
        {
            throw new Exception("Source path '".$src."' does not exist.", E_WARNING);
        }
        if (is_file($src) && !copy($src, $dst))
        {
            throw new Exception("File '".$src."' could not be copied.", E_WARNING);
        }
        if (is_dir($src))
        {
            if (!($dir = opendir($src)))
            {
                throw new Exception("Source directory '".$src."' could not be accessed.", E_WARNING);
            }
            if (!is_dir($dst) && !mkdir($dst))
            {
                throw new Exception("Destination directory '".$dst."' could not be created.", E_WARNING);
            }
            while(false !== ( $entry = readdir($dir)) ) {
                if (( $entry != '.' ) && ( $entry != '..' ))
                {
                    self::copy($src . DIRECTORY_SEPARATOR . $entry, $dst . DIRECTORY_SEPARATOR . $entry);
                }
            }
            closedir($dir);
        }
        return true;
    }

    public static function delete($path)
    {
        if (!is_file($path) && !is_dir($path))
        {
            throw new Exception("Path '".$path."' does not exist.", E_WARNING);
        }
        if (is_file($path))
        {
            if (!@unlink($path))
            {
                throw new Exception("File '".$path."' could not be deleted.", E_WARNING);
            }
            return true;
        }
        if (is_dir($path))
        {
            if (!@rmdir($path))
            {
                throw new Exception("Folder '".$path."' could not be deleted.", E_WARNING);
            }
            return true;
        }
        return false;
    }
}