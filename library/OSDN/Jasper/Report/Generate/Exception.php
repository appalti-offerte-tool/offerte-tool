<?php

/**
 * Generate exception class
 *
 * @category		OSDN
 * @package		OSDN_Jasper_Report_Generate
 * @version		$Id: Exception.php 6239 2009-01-16 14:24:53Z flash $
 */
class OSDN_Jasper_Report_Generate_Exception extends OSDN_Jasper_Exception
{}