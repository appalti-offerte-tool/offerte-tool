<?
class OSDN_Jasper_Report_Output_Xls extends OSDN_Jasper_Report_Output_Abstract   
{
    protected $_extension = 'xls';
    
	protected $_contentType = 'application/xls';
	
	protected $_outputFormat = OSDN_Jasper_Metadata_Argument::RUN_OUTPUT_FORMAT_XLS;
}