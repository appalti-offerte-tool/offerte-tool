<?
class OSDN_Jasper_Report_Output_Rtf extends OSDN_Jasper_Report_Output_Abstract  
{
    protected $_extension = 'rtf';
    
    protected $_contentType = 'application/rtf';
    
    protected $_outputFormat = OSDN_Jasper_Metadata_Argument::RUN_OUTPUT_FORMAT_RTF;
}