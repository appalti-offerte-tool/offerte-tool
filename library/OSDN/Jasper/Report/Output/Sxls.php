<?php

class OSDN_Jasper_Report_Output_Sxls extends OSDN_Jasper_Report_Output_Xls
{
	protected $_outputFormat = OSDN_Jasper_Metadata_Argument::RUN_OUTPUT_FORMAT_SXLS;
}