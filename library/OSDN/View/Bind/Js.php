<?php

/**
 * Javascript include implementation
 *
 * @author Yaroslav Zenin <yaroslavzenin@gmail.com>
 */
class OSDN_View_Bind_Js extends OSDN_View_Bind_Abstract
{
    const TYPE = 'js';

    public function getType()
    {
        return self::TYPE;
    }
}