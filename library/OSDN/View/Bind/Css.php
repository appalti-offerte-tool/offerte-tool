<?php

/**
 * Css include implementation
 *
 * @category     OSDN
 * @package     OSDN_Source_Merger
 * @version $Id: Css.php 19324 2011-01-04 17:02:33Z yaroslav $
 * @author Yaroslav Zenin <yaroslavzenin@gmail.com>
 */
class OSDN_View_Bind_Css extends OSDN_View_Bind_Abstract
{
    /**
    * Define include type
    *
    * @var string
    */
    const TYPE = 'css';

    /**
     * Define if CSS file is conditional
     *
     * @var boolelan
     */
    protected $_condition = false;

    /**
     * @see OSDN_View_Bind_Abstract::getType()
     */
    public function getType()
    {
        return self::TYPE;
    }

    /**
     * Set CSS file condition
     *
     * @param boolean $condition
     * @return OSDN_View_Bind_Css
     */
    public function setCondition($condition)
    {
        $this->_condition = $condition;
        return $this;
    }

    /**
     * Is CSS file conditional
     *
     * @return boolean
     */
    public function isConditional()
    {
        return !empty($this->_condition);
    }

    /**
     * Retrieve CSS condition
     *
     * @return string|boolean
     */
    public function getCondition()
    {
        return $this->_condition;
    }

    /**
     * @see OSDN_View_Bind_Abstract::toString()
     */
    public function toString($baseUrl = null)
    {
        $content = parent::toString();

        if (!preg_match_all('/url\([\'"]?(.+?)[\'"]?\)/i', $content, $matches)) {
            return $content;
        }

        $replacable = array();

        foreach($matches[1] as $url) {
            if (false !== realpath($url)) {
                continue;
            }

            $fn = realpath(dirname($this->getName()) . DIRECTORY_SEPARATOR . $url);
            $replacable[$url] = $this->_toHref($fn);
        }

        if (!empty($replacable)) {
            $content = str_replace(array_keys($replacable), array_values($replacable), $content);
        }

        return $content;
    }
}