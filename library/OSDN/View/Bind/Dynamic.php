<?php

class OSDN_View_Bind_Dynamic extends OSDN_View_Bind_Abstract
{
    const TYPE = 'dynamic';

    /**
     * When dynamic caching is not possible
     *
     * @var boolean
     */
    protected $_cacheable = false;

    /**
     * @see OSDN_View_Bind_Abstract::getType()
     */
    function getType()
    {
        return self::TYPE;
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_View_Bind_Abstract::_toHref($fn)
     */
    protected function _toHref($fn)
    {
        return $fn;
    }

    /**
     * @see OSDN_View_Bind_Abstract::toString()
     *
     * @FIXME make more clear structure
     */
    public function toString()
    {
        return $this->getHref();
    }

    /**
     * @see OSDN_View_Bind_Abstract::setCacheable()
     *
     * When remote caching is not possible
     */
    public function setCacheable($flag)
    {}
}