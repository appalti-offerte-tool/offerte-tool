<?php

/**
 * Abstract include entity
 *
 * @category    OSDN
 * @package     OSDN_View
 * @subpackage  OSDN_View_Bind
 * @version     $Id: Abstract.php 20536 2012-05-18 08:18:09Z yaroslav $
 *
 * @author Yaroslav Zenin <yaroslavzenin@gmail.com>
 */
abstract class OSDN_View_Bind_Abstract
{
    /**
     * The path to include entity
     *
     * @var string
     */
    protected $_name;

    /**
     * The directory of start point of application
     * The webserver looking in this directory
     *
     * @var string
     */
    protected $_rootDirectory;

    /**
     * Base url of web application
     *
     * @var string
     */
    protected $_baseUrl = '/';

    /**
     * Allow minify content
     *
     * @var boolean
     */
    protected $_minifiable = true;

    /**
     * Allow render sources inline (directly in html page)
     *
     * @var boolean
     */
    protected $_inline = false;

    /**
     * Allow caching sources
     *
     * @var boolean
     */
    protected $_cacheable = true;

    protected $_package = '.';

    protected $_subpackage = '.';

    protected $_dependences = array();

    protected $_theme = 'default';

    /**
     * The constructor
     *
     * Create new include entity
     *
     * @param string $name      The path to include entity file
     * @return void
     */
    public function __construct($name)
    {
        $this->_name = $name;
    }

    /**
     * Get name from include entity
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    public function setTheme($theme)
    {
        $this->_theme = $theme;
        return $this;
    }

    /**
     * Retrieve file href
     *
     * @return string
     */
    public function getHref()
    {
        return $this->_toHref($this->getName());
    }

    /**
     * Get type of include entity
     *
     * @return string
     */
    abstract function getType();

    /**
     * Is allowed to minify
     *
     * @return boolean
     */
    public function isMinifiable()
    {
        return $this->_minifiable;
    }

    /**
     * Is allowed to be cached
     *
     * @return boolean
     */
    public function isCacheable()
    {
        return $this->_cacheable;
    }

    /**
     * Is allowed to be rendered online
     *
     * @return boolean
     */
    public function isInline()
    {
        return $this->_inline;
    }

    /**
     * Set up the base directory (start point)
     *
     * @param string $rootDirectory
     * @return OSDN_View_Bind_Abstract
     */
    public function setRootDirectory($rootDirectory)
    {
        $rootDirectory = realpath($rootDirectory);
        if (false === $rootDirectory) {
            throw new Exception('The root directory path is not valid.');
        }

        $this->_rootDirectory = $rootDirectory;
        return $this;
    }

    /**
     * Set up the base url
     *
     * @param string $baseUrl
     * @return OSDN_View_Bind_Abstract
     */
    public function setBaseUrl($baseUrl)
    {
        $this->_baseUrl = $baseUrl;
        return $this;
    }

    /**
     * Allow minify content
     *
     * @param boolean $flag
     * @return OSDN_View_Bind_Abstract
     */
    public function setMinifiable($flag)
    {
        $this->_minifiable = (boolean) $flag;
        return $this;
    }

    /**
     * Allow render sources inline (directly in html page)
     *
     * @param boolean $flag
     * @return OSDN_View_Bind_Abstract
     */
    public function setInline($flag)
    {
        $this->_inline = (boolean) $flag;
        return $this;
    }

    /**
     * Allow caching sources
     *
     * @param boolean $flag
     * @return OSDN_View_Bind_Abstract
     */
    public function setCacheable($flag)
    {
        $this->_cacheable = (boolean) $flag;
        return $this;
    }

    public function getPackage()
    {
        return $this->_package;
    }

    public function setPackage($package)
    {
        $this->_package = $package;
        return $this;
    }

    public function getSubpackage()
    {
        return $this->_subpackage;
    }

    public function setSubpackage($subpackage)
    {
        $this->_subpackage = $subpackage;
        return $this;
    }

    public function setDependences(array $dependences)
    {
        $this->_dependences = $dependences;
        return $this;
    }

    public function getDependences()
    {
        return $this->_dependences;
    }

    public function hasDependences()
    {
        return !empty($this->_dependences);
    }

    /**
     * Convert absolute name to href
     *
     * Sometimes on "WINDOWS" machine can be used virtual drives
     * for managing virtual hosts. It this case we realpath function
     * will retrive the absolute path to file but from physical drive.
     * In this case we cannot use realpath but we can retrieve it from document root.
     * The document root and host name should be the SAME otherwise it will
     * work not correcty.
     *
     * @FIXME Make definition when host is virtual
     * @todo Think how to build path definition without identical doc root and host name
     *
     * @param string $fn        The absolute path to file
     * @return string           Href path
     */
    protected function _toHref($fn)
    {
        /**
         * On version 4.1.2 it looks like realpath retrieve correct path with same disk
         */
        static $_isVirtualDrive = false;

        if (null === $_isVirtualDrive) {
            $_isWindows = false !== stripos(PHP_OS, 'WIN');
            $_isVirtualDrive = $_isWindows;
        }

        /**
         * Adding theme support in the resource paths
         *
         * @FIXME Find more nicer solution
         */
        $themeTokenPos = strpos($fn, 'default');
        if (false !== $themeTokenPos) {
            $themableFilepath = substr_replace($fn, $this->_theme, $themeTokenPos, 7);
            if (realpath($themableFilepath)) {
                $fn = $themableFilepath;
            }
        }

        if (true !== $_isVirtualDrive) {
            $pos = strpos($fn, $this->_rootDirectory);
            $fn = substr_replace($fn, "", $pos, strlen($this->_rootDirectory));
        } else {
            $p1 = strstr($this->_rootDirectory, $_SERVER['SERVER_NAME']);
            $p2 = strpos($fn, $p1);
            $fn = substr($fn, $p2 + strlen($p1));
        }

        $fn = $this->_baseUrl . '/' . ltrim($fn, '/\\');
        return str_replace(array('\\', '/'), '/', $fn);
    }

    /**
     * Retrieve content from include entity
     *
     * @return string
     */
    public function toString()
    {
        return file_get_contents($this->getName());
    }
}