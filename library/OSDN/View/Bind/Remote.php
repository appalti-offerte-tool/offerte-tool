<?php

class OSDN_View_Bind_Remote extends OSDN_View_Bind_Abstract
{
    const TYPE = 'remote';

    protected $_module;

    protected $_controller;

    protected $_action;

    protected $_params = array();

    /**
     * When remote caching is not possible
     *
     * @var boolean
     */
    protected $_cacheable = false;

    /**
     * @see OSDN_View_Bind_Abstract::getType()
     */
    function getType()
    {
        return self::TYPE;
    }

    public function setModule($module)
    {
        $this->_module = $module;
        return $this;
    }

    public function setController($controller)
    {
        $this->_controller = $controller;
        return $this;
    }

    public function setAction($action)
    {
        $this->_action = $action;
        return $this;
    }

    public function setParams(array $params)
    {
        $this->_params = $params;
        return $this;
    }

    /**
     * @see OSDN_View_Bind_Abstract::toString()
     *
     * @FIXME make more clear structure
     */
    public function toString()
    {
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        $view = $viewRenderer->view;

        return $view->action($this->_action, $this->_controller, $this->_module, $this->_params);
    }

    /**
     * (non-PHPdoc)
     * @see OSDN_View_Bind_Abstract::_toHref()
     */
    protected function _toHref($fn)
    {
        return $this->_baseUrl . '/' . $this->getName() . '/_dc/' . uniqid('uniq-');
    }

    /**
     * @see OSDN_View_Bind_Abstract::getName()
     */
    public function getName()
    {
        $name = join('/', array($this->_module, $this->_controller, $this->_action));
        foreach($this->_params as $pKey => $pValue) {
            $name .= '/' . $pKey . '/' . $pValue;
        }

        return $name;
    }


    /**
     * @see OSDN_View_Bind_Abstract::setCacheable()
     *
     * When remote caching is not possible
     */
    public function setCacheable($flag)
    {}
}