<?php

class OSDN_View_Bind
{
    protected $_defaultPackageName = '.';

    protected $_name = "";

    protected $_directory;

    protected $_items = array();

    protected static $_incs = array();

    protected $_project;

    /**
     * Internal cache
     *
     * @var Zend_Cache_Core
     * @access private
     */
    private static $_cache = null;

    /**
     * Internal value to remember if cache supports tags
     *
     * @var boolean
     */
    private static $_cacheTags = false;

    /**
     * Internal option, cache disabled
     *
     * @var     boolean
     * @access  private
     */
    private static $_cacheDisabled = false;

    public function __construct($filename, array $packages = array(), array $ignores = array())
    {
        $filename = realpath($filename);
        if (false === $filename) {
            throw new OSDN_Exception(sprintf('Bad location of include file "%s"', $filename));
        }

        $cacheViewBindId = md5($filename);
        if (
            false === self::$_cacheDisabled && self::hasCache()
            && false !== ($result = self::getCache()->load($cacheViewBindId))
        ) {
            foreach($result as $field => $value) {
                $this->{$field} = $value;
            }

            return $this;
        }

        $xml = simplexml_load_file($filename);
        $this->_directory = dirname($filename);

        $name = (string) $xml['name'];
        if (empty($name)) {
            throw new Exception(sprint(
                'The library name cannot be empty in "%s"',
                $filename
            ));
        }
        $this->_name = trim($name);

        $this->_project = '';

        foreach($xml->package as $package) {

            $pkgName = (string) $package['name'];
            if (empty($pkgName)) {
                throw new Exception('The package name cannot be empty');
            }

            $project = (string) $package['project'];

            if (!empty($project) && false === stripos($this->_project, $project)) {
                continue;
            }

            if (
                $this->_defaultPackageName != $pkgName &&
                ((!empty($packages) && !in_array($pkgName, $packages)) ||
                (!empty($ignores) && in_array($pkgName, $ignores)))
            ) {
                continue;
            }

            if (!array_key_exists($name, self::$_incs) || !is_array(self::$_incs[$name])) {
                self::$_incs[$name] = array();
            }

            if (in_array($pkgName, self::$_incs[$name])) {
                continue;
            }

            self::$_incs[$name][] = $pkgName;

            foreach($package->subpackage as $subpackage) {
                $project = (string) $subpackage['project'];
                if (!empty($project) && false === stripos($this->_project, $project)) {
                    continue;
                }

                $dependences = array();
                if (!empty($subpackage['dependences'])) {
                    $dependences = preg_split('/\s*,\s*/', $subpackage['dependences'], null, PREG_SPLIT_NO_EMPTY);
                    array_map('trim', $dependences);
                    $dependences = array_filter($dependences);
                }

                foreach($subpackage->include as $include) {
                    $this->_include($include, $pkgName, trim((string) $subpackage['name']), $dependences);
                }
            }

            $dependences = array();
            if (!empty($package['dependences'])) {
                $dependences = preg_split('/\s*,\s*/', $package['dependences'], null, PREG_SPLIT_NO_EMPTY);
                array_map('trim', $dependences);
                $dependences = array_filter($dependences);
            }

            foreach($package->include as $include) {
                $this->_include($include, $pkgName, $this->_defaultPackageName, $dependences);
            }
        }

        if (false === self::$_cacheDisabled && self::hasCache()) {
            $o = array();
            foreach($this->__sleep() as $field) {
                $o[$field] = $this->{$field};
            }

            self::getCache()->save($o, $cacheViewBindId);
        }
    }

    /**
     * Using with cache implementation
     * When executed serialize method like
     * serialize($information) the method sleep return
     * the list of properties which will be stored in cache
     *
     * @return array
     */
    public function __sleep()
    {
        return array('_defaultPackageName', '_name', '_directory', '_items', '_project');
    }

    /**
     * @deprecated
     * @FIXME Change to one central point
     */
    public static function getDefaultTheme()
    {
        static $theme = null;
        if (null === $theme) {
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $viewResource = $bootstrap->getPluginResource('view');
            $viewOptions = $viewResource->getOptions();

            if (empty($viewOptions['theme']) || 0 === strcasecmp('default', $viewOptions['theme'])) {
                $theme = 'default';
            } else {
                $theme = $viewOptions['theme'];
            }
        }

        return $theme;
    }

    protected function _include(SimpleXMLElement $include, $package = '.', $subpackage = '.', array $dependences = array())
    {
        $extension = pathinfo($include, PATHINFO_EXTENSION);
        $filename = realpath($this->getDirectory() . DIRECTORY_SEPARATOR . $include);

        $accountRow = Zend_Auth::getInstance()->getIdentity();
        $locale = null != $accountRow ? $accountRow->getLanguage() : 'en';

//        $locale = explode('_', Zend_Locale::findLocale());
        if (!empty($include['locale']) && $include['locale'] != $locale) {
            return false;
        }

        if (!empty($include['debug']) && (string) $include['debug'] !== (APPLICATION_ENV == 'production' ? 'false' : 'true')) {
            return false;
        }

        $project = (string) $include['project'];
        if (!empty($project) && false === stripos($this->_project, $project)) {
            return false;
        }

        $inline = 'true' == $include['inline'];
        $cacheable = 'false' !== $include['cacheable'];
        $minifiable = 'false' !== $include['minifiable'];

        if (empty($extension) && empty($filename) && !$include['module']) {
            $extension = 'dynamic';
        }

        switch($extension) {
            case 'inc':
            case 'xml':

                if (false === $filename) {
                    throw new Exception('The include filename `' . $include . '` is not found.');
                }

                $pkgs = preg_split('/\s+/', $include['package'], null, PREG_SPLIT_NO_EMPTY);
                $pkgs = array_filter(array_map('trim', $pkgs));

                $ignores = preg_split('/\s+/', $include['ignore'], null, PREG_SPLIT_NO_EMPTY);
                $ignores = array_filter(array_map('trim', $ignores));

                $this->_items[] = new OSDN_View_Bind($filename, $pkgs, $ignores);
                break;

            case 'css':
                if (false === $filename) {
                    throw new Exception('The filename `' . $include . '` is not found.');
                }

                $css = new OSDN_View_Bind_Css($filename);
                $css->setMinifiable($minifiable);
                $css->setCacheable($cacheable);

                $css->setInline($inline);
                $css->setPackage($package);
                $css->setSubpackage($subpackage);
                $css->setDependences($dependences);

                $condition = (string) $include['condition'];
                if (!empty($condition)) {
                    $css->setCondition($condition);
                }
                $css->setTheme(self::getDefaultTheme());

                $this->_items[] = $css;
                break;

            case 'js':
                if (false === $filename) {
                    throw new Exception('The filename `' . $include . '` is not found.');
                }

                $js = new OSDN_View_Bind_Js($filename);
                $js->setMinifiable($minifiable);
                $js->setCacheable($cacheable);
                $js->setInline($inline);
                $js->setPackage($package);
                $js->setSubpackage($subpackage);
                $js->setDependences($dependences);

                $this->_items[] = $js;
                break;

            case 'dynamic':
                $js = new OSDN_View_Bind_Dynamic(trim((string) $include));
                $js->setMinifiable(false);
                $js->setInline(true);
                $js->setPackage($package);
                $js->setSubpackage($subpackage);
                $js->setDependences($dependences);

                $this->_items[] = $js;
//                foreach($include->attributes() as $pKey => $pValue) {
//
//                    $pKey = trim((string) $pKey);
//                    $pValue = trim((string) $pValue);
//
//                    if (in_array($pKey, array('module', 'controller', 'action', 'cacheable'))) {
//                        continue;
//                    }
//
//                    $params[$pKey] = $pValue;
//                }
//
//                $js->setParams($params);

                break;

            case 'remote':
            default:

                $module = (string) $include['module'];
                $controller = (string) $include['controller'];
                $action = (string) $include['action'];

                if (empty($module) || empty($include) || empty($action)) {
                    throw new Exception('Some required arguments are empty with remote include');
                }

                $js = new OSDN_View_Bind_Remote("");
                $js->setMinifiable($minifiable);
                $js->setCacheable($cacheable);
                $js->setInline($inline);

                $js->setPackage($package);
                $js->setSubpackage($subpackage);

                $js->setModule($module);
                $js->setController($controller);
                $js->setAction($action);
                $js->setDependences($dependences);

                $params = array();
                foreach($include->attributes() as $pKey => $pValue) {

                    $pKey = trim((string) $pKey);
                    $pValue = trim((string) $pValue);

                    if (in_array($pKey, array('module', 'controller', 'action', 'cacheable'))) {
                        continue;
                    }

                    $params[$pKey] = $pValue;
                }

                $js->setParams($params);

                $this->_items[] = $js;
                break;
        }
    }

    public function getDirectory()
    {
        return $this->_directory;
    }

    public function getIncludes($type = null, Closure $callbackFn = null)
    {
        $includes = array();

        foreach($this->_items as $include) {

            if (null !== $callbackFn && false === $callbackFn($include)) {
                continue;
            }

            switch($include->getType())
            {
                case OSDN_View_Bind_Js::TYPE:
                case OSDN_View_Bind_Remote::TYPE:

                    if (!is_null($type) && !in_array($type, array(
                        OSDN_View_Bind_Js::TYPE,
                        OSDN_View_Bind_Remote::TYPE
                    ))) {
                        continue 2;
                    }

                    break;

                case OSDN_View_Bind_Css::TYPE:
                    if (!is_null($type) && $type != OSDN_View_Bind_Css::TYPE) {
                        continue 2;
                    }

                    break;

                case OSDN_View_Bind_Dynamic::TYPE:
                    break;

                default:

                    if ($include instanceof self) {
                        $includes = array_merge($includes, $include->getIncludes($type));
                        continue 2;
                    }

                    throw new Exception('The include type is not allowed.');
            }

            $includes[] = $include;
        }

        return $includes;
    }

    public function toArray()
    {
        $output = array();
        $bu = Zend_Controller_Front::getInstance()->getBaseUrl();
        foreach($this->_items as $include) {

            $n = $this->_name;
            $p = $include->getPackage();
            $sp = $include->getSubpackage();

            if (!isset($output[$n][$p][$sp])) {
                $output[$n][$p][$sp] = array();
            }

            $collection =& $output[$n][$p][$sp];

            if ($include->hasDependences()) {
                if (!isset($collection['_dependences'])) {
                    $collection['_dependences'] = $include->getDependences();
                } else {
                    $collection['_dependences'] = array_merge($collection['_dependences'], $include->getDependences());
                }

                $collection['_dependences'] = array_unique($collection['_dependences']);
            }

            $include->setRootDirectory(APPLICATION_PATH);
            $include->setBaseUrl($bu);
            $collection[] = $include->getHref();
        }

        return $output;
    }

    /**
     * Uses for includes compability
     *
     * @return null
     */
    public function getType()
    {
        return null;
    }

    /**
     * Returns the set cache
     *
     * @return Zend_Cache_Core The set cache
     */
    public static function getCache()
    {
        return self::$_cache;
    }

    /**
     * Returns true when a cache is set
     *
     * @return boolean
     */
    public static function hasCache()
    {
        return null !== self::$_cache;
    }

    /**
     * Removes any set cache
     *
     * @return void
     */
    public static function removeCache()
    {
        self::$_cache = null;
    }

    /**
     * Disables the cache
     *
     * @param boolean $flag
     */
    public static function disableCache($flag)
    {
        self::$_cacheDisabled = (boolean) $flag;
    }

    /**
     * Set a cache
     *
     * @param Zend_Cache_Core $cache A cache frontend
     */
    public static function setCache(Zend_Cache_Core $cache)
    {
        self::$_cache = $cache;
        self::_getTagSupportForCache();
    }

    /**
     * Internal method to check if the given cache supports tags
     *
     * @param Zend_Cache $cache
     */
    private static function _getTagSupportForCache()
    {
        $backend = self::$_cache->getBackend();
        if ($backend instanceof Zend_Cache_Backend_ExtendedInterface) {
            $cacheOptions = $backend->getCapabilities();
            self::$_cacheTags = $cacheOptions['tags'];
        } else {
            self::$_cacheTags = false;
        }

        return self::$_cacheTags;
    }
}