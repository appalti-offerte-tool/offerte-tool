<?php

/**
 * Abstract class for OSDN_View_Engine_Interface implementation
 *
 * @category OSDN
 * @package OSDN_View
 * @subpackage OSDN_View_Engine
 */
abstract class OSDN_View_Engine_Abstract extends Zend_View_Abstract implements OSDN_View_Engine_Interface
{
    /**
     * Retrieve the data from collection
     *
     * @return array
     */
    public function getCollection()
    {
        $collection = get_object_vars($this);
        
        foreach($collection as $key => $value) {
            if ('_' == substr($key, 0, 1)) {
                unset($collection[$key]);
            }
        }

        return $collection;
    }
    
    protected function _run()
    {
        include func_get_arg(0);
    }
}