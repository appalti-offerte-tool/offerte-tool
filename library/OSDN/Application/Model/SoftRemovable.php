<?php

interface OSDN_Application_Model_SoftRemovable
{
    function setIsActive($isActive);

    function getIsActive();
}