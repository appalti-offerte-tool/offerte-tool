<?php

/**
 * Default interface used for model
 * It helps to identify model object in the future...
 *
 * @version $Id: Interface.php 20480 2012-04-17 15:19:30Z yaroslav $
 */
interface OSDN_Application_Model_Interface
{
    public function getId();
}