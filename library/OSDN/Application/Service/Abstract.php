<?php

/**
 * Implementation of basic functionality for service layer
 *
 * @category OSDN
 * @package OSDN_Application
 * @link http://martinfowler.com/eaaCatalog/serviceLayer.html
 * @version $Id$
 */
abstract class OSDN_Application_Service_Abstract
{
    /**
     * Define decorators type
     *
     * @var string
     */
    const DECORATOR = 'DECORATOR';

    /**
     * Defile rules type
     *
     * @var string
     */
    const RULES_TYPE_FILTERS = 'FILTERS';
    const RULES_TYPE_VALIDATORS = 'VALIDATORS';
    const RULES_TYPE_FIELDS = 'FIELDS';

    /**
     * Contain rules highstack
     *
     * @var array
     */
    protected $_rules = array(
        'fields'    => array(
            'validators'    => array(),
            'filters'       => array()
        ),
        self::RULES_TYPE_FILTERS     => array('default' => array()),
        self::RULES_TYPE_VALIDATORS  => array('default' => array())
    );

    /**
     * Collect decorators
     *
     * @var array
     */
    protected $_decorators = array();

    /**
     * Contain decorators loaders
     *
     * @var array
     */
    protected $_loaders = array();

    /**
     * The contructor
     *
     * Initialize default decorators
     */
    public function __construct()
    {
        $this->_init();

        $this->loadDefaultDecorators();
    }

    /**
     * Simple mock...
     * Used in child classes
     */
    protected function _init()
    {}

    protected function _throw($message)
    {
        if (func_num_args() > 1) {
            $message = func_get_args();
        }

        $e = new OSDN_Validate_Exception();
        $e->addMessage($message);

        throw $e;
    }

    /**
     * Validate method
     *
     * @param array $data        fields/values pairs
     * @param string $schema    Validation schema
     * @param array $fields        Validation fields
     * @param boolean $throwException
     * @throws OSDN_Exception
     */
    protected function _validate(array $data, $schema = null, array $fields = array(), $throwException = true)
    {
        if (null === $schema) {
            $schema = 'default';
        }

        if (!array_key_exists($schema, $this->_rules[self::RULES_TYPE_VALIDATORS])) {
            throw new OSDN_Exception(sprintf('Schema "%s" is not defined in validate rules', $schema));
        }

        $validators = $this->_rules[self::RULES_TYPE_VALIDATORS][$schema];
        $filters = array();

        if (array_key_exists($schema, $this->_rules[self::RULES_TYPE_FILTERS])) {
            $filters = $this->_rules[self::RULES_TYPE_FILTERS][$schema];
        }

        if (!empty($fields)) {
            $f = array_fill_keys($fields, null);
            $filters = array_intersect_key($filters, $f);
            $validators = array_intersect_key($validators, $f);
        }

        $f = new OSDN_Filter_Input($filters, $validators, $data);
        $f->validate($throwException);

        return $f;
    }

    /**
     * Validate one field
     *
     * @param string $name    The field name
     * @param mixed $value    The field value
     * @param string|null $schema    Schema name
     * @param boolean $throwException
     */
    public function _validateField($name, $value, $schema = null, $throwException = true)
    {
        return $this->_validate(array(
            $name => $value
        ), $schema, array($name), $throwException);
    }

    /**
     * Map validation rules on schema namespaces
     *
     * @param string $schema    Mapped schema
     * @param array $rules        List of validation rules
     * @param string|null $parentSchema
     * @param string $ruleType
     * @throws OSDN_Exception
     */
    private function _attachRule($schema, array $rules, $parentSchema, $ruleType)
    {
        if (null !== $parentSchema) {
            if (!isset($this->_rules[$ruleType][$parentSchema])) {
                throw new OSDN_Exception(sprintf('Schema "%s" is not defined in highstack', $parentSchema));
            }

            $rules = array_merge($this->_rules[$ruleType][$parentSchema], $rules);
        }

        $this->_rules[$ruleType][$schema] = $rules;
        return $this;
    }

    public function _attachFilteringRules($schema, array $rules, $parentSchema = null)
    {
        $this->_attachRule($schema, $rules, $parentSchema, self::RULES_TYPE_FILTERS);
        return $this;
    }

    public function _attachValidationRules($schema, array $rules, $parentSchema = null)
    {
        $this->_attachRule($schema, $rules, $parentSchema, self::RULES_TYPE_VALIDATORS);
        return $this;
    }

    /**
     * Load the default decorators
     *
     * @return void
     */
    public function loadDefaultDecorators()
    {
//        if ($this->loadDefaultDecoratorsIsDisabled()) {
//            return $this;
//        }

        $decorators = $this->getDecorators();
        return $this;
    }

    public function addDecorator($decorator, $options = array())
    {
        if (is_string($decorator)) {
            $name      = $decorator;
            $decorator = array(
                'decorator' => $name,
                'options'   => $options,
            );

        } elseif (is_array($decorator)) {

            foreach ($decorator as $name => $spec) {
                break;
            }

            if (is_numeric($name)) {
                throw new OSDN_Exception('Invalid alias provided to addDecorator; must be alphanumeric string');
            }

            if (is_string($spec)) {
                $decorator = array(
                    'decorator' => $spec,
                    'options'   => $options,
                );
            }

        } else {
            throw new OSDN_Exception('Invalid decorator provided to addDecorator');
        }

        $this->_decorators[$name] = $decorator;

        return $this;
    }

    /**
     * @todo implement me..
     */
    public function addDecorators(array $decorators)
    {}

    public function setDecorators(array $decorators)
    {
        $this->clearDecorators();
        $this->addDecorators($decorators);

        return $this;
    }

    public function clearDecorators()
    {
        $this->_decorators = array();
        return $this;
    }

    /**
     * Retrieve all decorators
     *
     * @return array
     */
    public function getDecorators()
    {
        foreach ($this->_decorators as $key => $value) {
            if (is_array($value)) {
                $this->_loadDecorator($value, $key);
            }
        }
        return $this->_decorators;
    }

    /**
     * Retrieve a registered decorator
     *
     * @param  string $name
     * @return false|Zend_Form_Decorator_Abstract
     */
    public function getDecorator($name)
    {
        if (!isset($this->_decorators[$name])) {
            $len = strlen($name);
            foreach ($this->_decorators as $localName => $decorator) {
                if ($len > strlen($localName)) {
                    continue;
                }

                if (0 === substr_compare($localName, $name, -$len, $len, true)) {
                    if (is_array($decorator)) {
                        return $this->_loadDecorator($decorator, $localName);
                    }
                    return $decorator;
                }
            }

            return false;
        }

        if (is_array($this->_decorators[$name])) {
            return $this->_loadDecorator($this->_decorators[$name], $name);
        }

        return $this->_decorators[$name];
    }

    /**
     * Lazy-load a decorator
     *
     * @param  array $decorator Decorator type and options
     * @param  mixed $name Decorator name or alias
     * @return Zend_Form_Decorator_Interface
     */
    protected function _loadDecorator(array $decorator, $name)
    {
        $sameName = false;
        if ($name == $decorator['decorator']) {
            $sameName = true;
        }

        $instance = $this->_getDecorator($decorator['decorator'], $decorator['options']);

        if ($sameName) {
            $newName            = get_class($instance);
            $decoratorNames     = array_keys($this->_decorators);
            $order              = array_flip($decoratorNames);

            $order[$newName]    = $order[$name];
            $decoratorsExchange = array();
            unset($order[$name]);
            asort($order);
            foreach ($order as $key => $index) {
                if ($key == $newName) {
                    $decoratorsExchange[$key] = $instance;
                    continue;
                }
                $decoratorsExchange[$key] = $this->_decorators[$key];
            }
            $this->_decorators = $decoratorsExchange;
        } else {
            $this->_decorators[$name] = $instance;
        }

        return $instance;
    }

    /**
     * Instantiate a decorator based on class name or class name fragment
     *
     * @param  string $name
     * @param  null|array $options
     * @return
     */
    protected function _getDecorator($name, $options)
    {
        $class = $this->getPluginLoader(self::DECORATOR)->load($name);
        if (null === $options) {
            $decorator = new $class;
        } else {
            $decorator = new $class($options);
        }

        return $decorator;
    }

    public function getPluginLoader($type = null)
    {
        $type = strtoupper($type);
        if (!isset($this->_loaders[$type])) {
            switch ($type) {
                case self::DECORATOR:
                    $prefixSegment = 'Application_Service_Decorator';
                    $pathSegment   = 'Application/Service/Decorator';
                    break;

                default:
                    throw new \OSDN_Exception(sprintf('Invalid type "%s" provided to getPluginLoader()', $type));
            }

            $this->_loaders[$type] = new \Zend_Loader_PluginLoader(
                array('\OSDN_' . $prefixSegment => 'OSDN/' . $pathSegment . '/')
            );
        }

        return $this->_loaders[$type];
    }

    /**
     * Adds support for magic finders.
     *
     * @return array|object The found entity/entities.
     * @throws BadMethodCallException  If the method called is an invalid find* method
     *                                 or no find* method at all and therefore an invalid
     *                                 method call.
     */
    public function __call($method, $arguments)
    {
        if (false !== ($pos = strripos($method, 'with'))) {

            $decoratorName = substr($method, $pos + 4);
            $protectedMethodName = '_' . substr($method, 0, $pos);

            if (method_exists($this, $protectedMethodName)) {
                $args = $arguments;
                $result = call_user_func_array(array($this, $protectedMethodName), $args);
            }

            $decoratorName = strtolower($decoratorName);
            if (false !== ($decoratorObject = $this->getDecorator($decoratorName))) {
                $output = $decoratorObject->decorate($result);

                return $output;
            }
        }

        throw new BadMethodCallException(sprintf('Method "%s" is undefined in "%s"', $method, get_class($this)));
    }

    public function getFilterParams($id = null)
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $params = $request->getParams();

        if (!empty($params['filter']) && \is_array($params['filter'])) {
            foreach ($params['filter'] as $key => $value) {
                if ($key === 'fid') {
                    $id = $value;
                    break;
                }
            }
        }

        if (empty($id)) {
            $id = join('-', array(
                $request->getModuleName(),
                $request->getControllerName(),
                $request->getActionName()
            ));
        }

        $namespace = OSDN_Controller_Plugin_ViewFilter::toId($id);
        $session = new Zend_Session_Namespace($namespace);

        return !empty($session->filter) ? $session->filter : array();
    }

}