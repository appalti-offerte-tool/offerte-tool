<?php

/**
 * Minimal service who provides db functionality
 *
 */
abstract class OSDN_Application_Service_Dbable extends OSDN_Application_Service_Abstract
{
    /**
     * Contain default adapter
     *
     * @var Zend_Db_Adapter_Abstract
     */
    private static $_defaultDb = null;

    /**
     * Callback
     * Executed before init defaultDb
     *
     * @var Closure
     */
    private static $_defaultDbCallback = null;

    public function loadDefaultDecorators()
    {
        parent::loadDefaultDecorators();
        $this->addDecorator('response');

        return $this;
    }

    /**
     * Callback
     * Executed before default db accessed
     *
     * @param Closure $onInitDefaultDbHandler
     * @throws OSDN_Exception
     */
    public static function setOnInitDefaultDb(Closure $onInitDefaultDbCallbackHandler)
    {
        self::$_defaultDbCallback = $onInitDefaultDbCallbackHandler;
    }

    public static function setDefaultDbAdapter(Zend_Db_Adapter_Abstract $adapter)
    {
        self::$_defaultDb = $adapter;
    }

    public static function getDefaultDbAdapter()
    {
        if (null === self::$_defaultDb) {
            if (!is_callable(self::$_defaultDbCallback)) {
                throw new OSDN_Exception('Init callback fn is not callable');
            }

            $defaultDb = call_user_func(self::$_defaultDbCallback);
            self::setDefaultDbAdapter($defaultDb);
        }

        return self::$_defaultDb;
    }

    /**
     * Retrieve defautl adapter
     *
     * @return Zend_Db_Adapter_Abstract
     */
    public function getAdapter()
    {
        return self::getDefaultDbAdapter();
    }

    /**
     * Init default filtering
     *
     * @param Zend_Db_Table_Abstract $table  the working table
     * @param Zend_Db_Select $statement      the working statement
     * @param array $fields                  available fields
     *
     * @return OSDN_Db_Select_Filter
     */
    protected function _initDbFilter(& $statement, $table = null, array $fields = array())
    {
        return new OSDN_Db_Select_Filter($statement, $table, $fields);
    }
}