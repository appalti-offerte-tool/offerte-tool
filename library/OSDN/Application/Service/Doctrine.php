<?php

/**
 * @TODO
 * In PHP 5.4 will be available trait possibility
 * Move $_defaultDb to trait and it can be
 * possible easy connection in other services
 */

use Doctrine\ORM\EntityManager;

abstract class OSDN_Application_Service_Doctrine extends OSDN_Application_Service_Dbable
{
    /**
     * Contain default entity manager instance
     *
     * @var Doctrine\ORM\EntityManager
     */
    private static $_defaultEm = null;

    /**
     * Contain entity manager instance
     *
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    /**
     * The constructor
     * If default entity manager register set up to local instance
     * of em
     *
     * @return void
     */
    public function __construct()
    {
        if (null != ($em = self::getDefaultEntityManager())) {
            $this->_em = $em;
        }

        parent::__construct();
    }

    /**
     * Setup default entity manager
     *
     * @param Doctrine\ORM\EntityManager $em
     */
    public static function setDefaultEntityManager(EntityManager $em = null)
    {
        self::$_defaultEm = $em;
    }

    /**
     * Retrieve default entity manager
     *
     * @return Doctrine\ORM\EntityManager
     */
    public static function getDefaultEntityManager()
    {
        return self::$_defaultEm;
    }

    /**
     * Setup entity manager
     *
     * @param Doctrine\ORM\EntityManager $em
     */
    public function setEntityManager(EntityManager $em)
    {
        $this->_em = $em;
        return $this;
    }

    /**
     * Retrieve entity mananger
     *
     * @return Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->_em;
    }

    public function loadDefaultDecorators()
    {
        parent::loadDefaultDecorators();

        $this->addDecorator('paginator', array('adapter' => 'doctrine'));
        return $this;
    }

    /**
     * Populate domain model with data from array
     *
     * @link http://groups.google.com/group/doctrine-user/browse_thread/thread/d848b693e155eaf1
     * @param array $data
     * @param OSDN_Application_Model_Interface $domainModel
     */
    public function populate(array $data, OSDN_Application_Model_Interface $domainModel)
    {

        $domainModelCls = get_class($domainModel);
        $md = $this->_em->getClassMetadata($domainModelCls);

        foreach($data as $field => $value) {

            if (0 === strcasecmp($field, 'id')) {
                continue;
            }

            if ($md->isVersioned && 0 === strcasecmp($field, $md->versionField)) {
                continue;
            }

            if ($md->hasField($field)) {

                $method = 'set' . ucfirst($field);
                if (!method_exists($domainModel, $method)) {
                    throw new OSDN_Exception(sprintf('Unable to find setter method for field "%s"', $field));
                }

                call_user_func(array($domainModel, $method), $this->_toPhpFormat($field, $value, $md));
                continue;
            }

//            if (empty($value)) {
//                continue;
//            }

            if (is_string($value)) {
                $value = trim($value);
                if ('' === $value) {
                    continue;
                }
            }

            if ($md->hasAssociation($field)) {

                $method = 'set' . ucfirst($field);
                if (!method_exists($domainModel, $method)) {

                    /**
                     * Sometimes we have deals with names like "entityId"
                     * but method called setEntity() we need to call method
                     * without id setEntity() instead of setEntityId()
                     */
                    if (0 === substr_compare($field, 'id', -2, 2, true)) {
                        $method = 'set' . ucfirst(substr($field, 0, -2));
                    }

                    if (!method_exists($domainModel, $method)) {
                        throw new OSDN_Exception(sprintf('Unable to find setter method for field "%s"', $field));
                    }
                }

                call_user_func(array($domainModel, $method), $this->findAssociation($domainModel, $field, $value, $md));
            }
        }

        return $domainModel;
    }

    public function findAssociation($model, $field, $value, \Doctrine\ORM\Mapping\ClassMetadata $md = null)
    {
        if (null == $md) {

            if ($model instanceof OSDN_Application_Model_Interface) {
                $model = get_class($model);
            }

            $md = $this->_em->getClassMetadata($model);
        }

        $am = $md->getAssociationMapping($field);
        $targetEntityRepository = $this->_em->getRepository($am['targetEntity']);
        $o = $targetEntityRepository->find($value);

        if (empty($o) && null !== $o) {
            $o = null;
        }

        return $o;
    }

    private function _toPhpFormat($field, $value, \Doctrine\ORM\Mapping\ClassMetadata $md)
    {
        /**
         * @todo make more smarter checking type
         *
         * When value is boolean like false or '0' does not work
         * because driver SQLSRV does not allow null value
         */
//        if (empty($value)) {
//            $value = null;
//        }

        $typeOfColumn = $md->getTypeOfColumn($field);
        if (! \Doctrine\DBAL\Types\Type::hasType($typeOfColumn)) {
            return $value;
        }

        $conversion = \Doctrine\DBAL\Types\Type::getType($typeOfColumn);

        $output = null;
        switch($conversion->getName()) {
            case 'datetime':
                /**
                 * @todo
                 *     Currently the format of MSSQL with ".u". And microtime is required.
                 *     Here we override it.
                 */
                $output = \DateTime::createFromFormat('Y-m-d H:i:s', $value);
                break;

            default:
                $output = $conversion->convertToPHPValue($value, $this->_em->getConnection()->getDatabasePlatform());
        }

        return $output;
    }

    /**
     * Init Doctrine2 filtering
     *
     * @param $model            the entity model
     * @param $queryBuilder     the working query builder (statement)
     * @param array $fields     available fields
     *
     * @return OSDN_Db_Doctrine_Filter
     */
    protected function _initDbFilter(& $statement, $model = null, array $fields = array())
    {
        if ($statement instanceof Doctrine\ORM\QueryBuilder) {
            return new OSDN_Db_Doctrine_Filter($model, $statement, $fields);
        }

        return parent::_initDbFilter($statement, $model, $fields);
    }
}