<?php

abstract class OSDN_Application_Service_Decorator_Abstract
{
    protected $_options = array();

    public function setOptions(array $options)
    {
        $this->_options = array_merge($this->_options, $options);
        return $this;
    }
}