<?php

use Doctrine\ORM;

class OSDN_Application_Service_Decorator_Response extends OSDN_Application_Service_Decorator_Abstract
{
    public function decorate($statement, $table = null)
    {
        if ($statement instanceof ORM\QueryBuilder) {
            return new OSDN_Db_Doctrine_Response($statement);
        } elseif ($statement instanceof Zend_Db_Select) {
            return new OSDN_Db_Response($statement, $table);
        }

        throw new OSDN_Exception('Unable to decorate statement');
    }
}