<?php

use Doctrine\ORM;

class OSDN_Application_Service_Decorator_Paginator extends OSDN_Application_Service_Decorator_Abstract
{
    public function decorate(ORM\QueryBuilder $queryBuilder)
    {
        $adapter = new OSDN_Paginator_Adapter_Doctrine($queryBuilder);
        $paginator = new \Zend_Paginator($adapter);

        $paginator->setCurrentPageNumber(1);
        $paginator->setItemCountPerPage(2);

        foreach($this->_options as $o => $v) {

            switch($o) {
                case 'page':
                    $o = 'currentPageNumber';
                    break;
            }

            $method = 'set' . ucfirst($o);
            if (method_exists($paginator, $method)) {
                call_user_func_array(array($paginator, $method), is_array($v) ? $v : array($v));
            }
        }

        return $paginator;
    }
}