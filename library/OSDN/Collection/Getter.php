<?php

/**
 * Getter collection
 * Allow get property from collection and prevent notices
 *
 * @category    OSDN
 * @package     OSDN_Collection
 * @version     $Id: Getter.php 18126 2010-05-14 14:58:02Z yaroslav $
 */
class OSDN_Collection_Getter
{
    /**
     * The collection of items
     *
     * @var array
     */
    protected $_items;
    
    /**
     * The constructor
     *
     * @param array $items [optional]       The items for collection
     * @return void
     */
    public function __construct(array $items = array())
    {
        $this->_items = $items;
    }
    
    /**
     * Get property from collection
     *
     * @param string $prop      The property name
     * @param mixed $default [optional]     The default value if property does not exist
     *
     * @return mixed
     */
    public function get($prop, $default = null)
    {
        if (isset($this->_items[$prop])) {
            return $this->_items[$prop];
        }
        
        return $default;
    }
    
    /**
     * Alias for get()
     *
     * @see get()
     */
    public function __get($prop)
    {
        return $this->get($prop);
    }
    
    /**
     * Assign properties collection to main collection
     *
     * @param array $items      The collection of items
     * @param boolean $merge [optional]     Allow to merge collection
     * @return OSDN_Collection_Getter
     */
    public function assign(array $items, $merge = false)
    {
        if (true === $merge) {
            $this->_items = array_merge($this->_items, $items);
        } else {
            $this->_items = $items;
        }
        
        return $this;
    }
    
    /**
     * Check if property isset in collection
     *
     * @param string $prop
     * @return boolean
     */
    public function __isset($prop)
    {
        if (!is_string($prop)) {
            return false;
        }
        
        return isset($this->_items[$prop]);
    }
    
    public function __unset($prop)
    {
        if (!$this->__isset($prop)) {
            return false;
        }
        
        unset($this->_items[$prop]);
        return true;
    }
    
    public function toArray()
    {
        return $this->_items;
    }
}