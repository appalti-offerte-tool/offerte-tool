<?php

/**
 * Interface of all Set objects.
 *
 * @category		OSDN
 * @package		OSDN_Collection
 * @version		$Id: Interface.php 5403 2008-11-14 15:17:44Z flash $
 */
interface OSDN_Collection_Map_Interface extends OSDN_Collection_Interface
{} 