<?php

/**
 * Manipulate with system languages
 *
 * @category OSDN
 * @package OSDN_Language
 */
class OSDN_Language
{
    /**
     * List of supported locales
     *
     * @var array
     */
    protected static $_locales = array('en', 'nl');
    
	/**
	 * The default locale
	 *
	 * @var string
	 */
	protected static $_defaultLocale;
	
	/**
	 * The default namespace for Zend_Session_Namespace
	 *
	 * @var string
	 */
	protected static $_namespace = 'OSDN_Language';
	
    /**
     * Set default locale
     *
     * @param string $language  Locale en|nl|ru|ua|en_US|nl_NL...
     * @return void
     */
    public static function setDefaultLocale($locale, $overwrite = false)
    {
        if (false !== ($l = strpos($locale, '_'))) {
            $locale = strtolower($l[0]) . '_' . strtoupper($l[1]);
        } elseif (2 == strlen($locale)) {
            $locale = strtolower($locale);
        }

        $ns = new Zend_Session_Namespace(self::$_namespace);
        if (!$ns->locale || true === $overwrite) {
            $ns->locale = $locale;
        }
        
        self::$_defaultLocale = $ns->locale;
        Zend_Locale::setDefault(self::$_defaultLocale);
    }
    
    /**
     * Get default locale
     *
     * @return string
     */
    public static function getDefaultLocale()
    {
        if (!is_null(self::$_defaultLocale)) {
            return self::$_defaultLocale;
        }
        
        $ns = new Zend_Session_Namespace(self::$_namespace);
        self::$_defaultLocale = $ns->locale;
        return self::$_defaultLocale;
    }
    
    /**
     * Check if locale present in system
     *
     * @return bool
     */
    public static function isAvailableLocale($locale)
    {
        return in_array($locale, self::$_locales);
    }
}