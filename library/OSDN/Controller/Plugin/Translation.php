<?php

class OSDN_Controller_Plugin_Translation extends Zend_Controller_Plugin_Abstract
{
    protected $_options;
    
    public function __construct(array $options)
    {
        $this->_options = $options;
    }
    
    /**
    * @param Zend_Controller_Request_Abstract $request
    */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if (OSDN_Translation::hasInstance()) {
            return;
        }
        
        if (!empty($this->_options['adapter'])) {
            OSDN_Translation::setDefaultAdapter($this->_options['adapter']);
            unset($this->_options['adapter']);
        }
        
        $data = false;
        if (isset($this->_options['data'])) {
            $data = sprintf($this->_options['data'], OSDN_Language::getDefaultLocale());
            unset($this->_options['data']);
        }
        
        $translator = OSDN_Translation::factory(null, $data, OSDN_Language::getDefaultLocale(), $this->_options);
        Zend_Registry::set('Zend_Translate', $translator);
    }
}

/**
 * @todo make view helper
 * @see Zend_View_Helper_Translate
 *
 * @param string $phrase
 * @deprecated
 * @return string
 */
//function lang($phrase)
//{
//    $args = func_get_args();
//
//    if (!empty($phrase)) {
//
//        $internal = Zend_Registry::get('config')->ui->language->internal;
//        $translationObject = OSDN_Translation::getInstance();
//        $translation = $translationObject->translate($phrase, OSDN_Language::getDefaultLocale(), $internal);
//    } else {
//        $translation = $phrase;
//    }
//
//    $len = count($args);
//    if (1 >= $len) {
//        return $translation;
//    }
//
//    for($i = 1; $i < $len; $i++) {
//        $translation = str_replace('{' . ($i - 1). '}', $args[$i], $translation);
//    }
//    return $translation;
//}