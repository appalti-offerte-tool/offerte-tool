<?php

/**
 * OSDN authorization plugin
 *
 * @uses        Zend_Controller_Plugin_Abstract
 * @category    OSDN
 * @package     OSDN_Controller
 * @subpackage  OSDN_Controller_Plugin
 * @version     $Id: Authorization.php 20324 2012-01-19 13:35:26Z yaroslav $
 */

class OSDN_Controller_Plugin_Authorization extends Zend_Controller_Plugin_Abstract
{
    /**
     * Allowed routers property name
     *
     * @var string
     */
    const ALLOWED_ROUTERS   = 'allowedRouters';

    /**
     * Public module property name
     *
     * @var string
     */
    const PUBLIC_MODULE     = 'publicModule';

    /**
     * Public module name
     *
     * @var string
     */
    protected $_public = 'public';

    /**
     * The list of allowed routers
     *
     * @example <code>
     * array(
     *
     *   // module
     *   'default'   => true,
     *
     *   // controller
     *   'accounts'  => array('auth' => true),
     *
     *   // action
     *   'student'   => array('index' => array('list' => true))
     * );
     * </code>
     * @var array
     */
    private $_allowRouters = array();

    /**
     * Set enabling of using acl
     *
     * @var boolean
     */
    protected $_enableAcl = true;

    /**
     * The constructor
     *
     * @param array $options [optional]     The basic property object
     * @return void
     */
    public function __construct(array $options = array())
    {
        foreach($options as $option => $value) {
            switch($option) {
                case self::ALLOWED_ROUTERS:
                    if (is_array($value)) {
                        $this->_allowRouters = array_merge_recursive($this->_allowRouters, $value);
                    }
                    break;

                case self::PUBLIC_MODULE:
                    if (is_string($value)) {
                        $this->_public = $value;
                    }
                    break;

                default:
                    throw new Exception('Unknown option "' . $option . '"');
            }
        }
    }

    /**
     * @see Zend_Controller_Plugin_Abstract::routeStartup()
     */
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        $module = strtolower($request->getModuleName());
        $controller = strtolower($request->getControllerName());
        $action = strtolower($request->getActionName());

        // account go to anonymous area
        // make this account anonymous
        if ($this->_isPublic($module)) {

            // if already is anonymous
            if (OSDN_Accounts_Prototype::isAnonymous()) {
                return;
            }

            $anonymous = $this->_fetchAnonymous();
            if (false !== $anonymous) {
                Zend_Auth::getInstance()->getStorage()->write((object) $anonymous);
                return;
            }
        }

        // if superadministrator then "more po koleno!"
        if (OSDN_Accounts_Prototype::isSuperAdministrator()) {
            return;
        }

        // if not public area but anonymous then clear and redirect to start page to login
        if (!$this->_isPublic($module) && OSDN_Accounts_Prototype::isAnonymous()) {
            Zend_Auth::getInstance()->clearIdentity();
            return;
        }

        // account is authenticated
        // add acl helper
        if (OSDN_Accounts_Prototype::isAuthenticated()) {
            if ($this->isAclEnabled()) {
                Zend_Controller_Action_HelperBroker::addHelper(new OSDN_Controller_Action_Helper_Acl());
            }
            return;
        }

        if (true === $this->_isAllowed($module, $controller, $action)) {

            if ($module == Zend_Controller_Front::getInstance()->getDefaultModule()) {
                return;
            }

            if (!Zend_Auth::getInstance()->hasIdentity()) {

                $auth = Zend_Auth::getInstance();
                $auth->setStorage(new Zend_Auth_Storage_NonPersistent());

                $anonymous = $this->_fetchAnonymous();
                if (false !== $anonymous) {
                    Zend_Auth::getInstance()->getStorage()->write((object) $anonymous);
                    return;
                }
            }


            // the request location is allowed
            return;
        }

        $frontController = Zend_Controller_Front::getInstance();
        $route = $frontController->getRouter();
        header($_SERVER['SERVER_PROTOCOL'] . ' 401 Unauthorized', false, 401);

        /**
         * Current request is not allowed
         * Try to execute the default request settings
         */
        if ('default' == $route->getCurrentRouteName()) {
            $dispatcher = $frontController->getDispatcher();

            $request
                ->setModuleName($dispatcher->getDefaultModule())
                ->setControllerName($dispatcher->getDefaultControllerName())
                ->setActionName($dispatcher->getDefaultAction())
                ->setParam('unauthorized', true);
            return;
        }

        // other routes
        exit;
    }

    /**
     * Retrieve the anonymous account
     *
     * @return array|false
     */
    protected function _fetchAnonymous()
    {
        $model = OSDN_Accounts_Prototype::model();
        $response = $model->fetchAnonymousAccount();
        if ($response->isSuccess()) {
            $anonymous = $response->row;
            if (empty($anonymous)) {
                throw new OSDN_Exception('The Anonymous account is not set.');
            }

            return $anonymous;
        }

        return false;
    }

    /**
     * Fetch allowed routers
     *
     * @return array
     */
    protected function getAllowedRouters()
    {
        return $this->_allowRouters += array(
            Zend_Controller_Front::getInstance()->getDefaultModule() => true,
            $this->_public  => true
        );
    }

    /**
     * Check if dispatched controller is public
     *
     * @param string $module        The module name
     * @return boolean
     */
    protected function _isPublic($module)
    {
        return $module == $this->_public;
    }

    /**
     * Check if module is allowed
     *
     * @param string $module        The module name
     * @param string $controller    The controller name
     * @return boolean
     */
    protected function _isAllowed($module, $controller, $action)
    {
        $access = false;
        $routers = $this->getAllowedRouters();

        do {
            if (!array_key_exists($module, $routers)) {
                break;
            }

            if (true === $routers[$module]) {
                $access = true;
                break;
            }

            if (!is_array($routers[$module])) {
                break;
            }

            if (!array_key_exists($controller, $routers[$module])) {
                break;
            }

            if (true === $routers[$module][$controller]) {
                $access = true;
                break;
            }

            if (!is_array($routers[$module][$controller])) {
                break;
            }

            if (!array_key_exists($action, $routers[$module][$controller])) {
                break;
            }

            if (true !== $routers[$module][$controller][$action]) {
                break;
            }

            $access = true;

        } while(false);

        return $access;
    }

    /**
     * Set enabling of acl
     *
     * @param object $enable
     *
     * @return void
     */
    public function enableAcl($enable)
    {
        $this->_enableAcl = $enable;
    }

    /**
     * Return whether acl is enabled
     *
     * @return boolean
     */
    public function isAclEnabled()
    {
        return $this->_enableAcl;
    }
}