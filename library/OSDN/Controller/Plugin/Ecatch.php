<?php

class OSDN_Controller_Plugin_Ecatch extends Zend_Controller_Plugin_Abstract
{
    protected $_session;

    protected $_last = null;

    protected $_messages = array();
    
    public function __construct()
    {
        $this->_session = new Zend_Session_Namespace('a' . md5(__FILE__));
        $this->_session->setExpirationHops(1);

        if (!is_array($this->_session->history)) {
            $this->_session->history = array();
        }
    }

    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        if (!empty($this->_session->messages)) {
            $this->_messages = $this->_session->messages;
        }

        if ($request->isGet()) {
            $this->_session->history[] = $request->getRequestUri();
        } elseif ($request->isPost()) {
            $this->_session->params = $request->getPost();
        }
    }

    public function dispatchLoopShutdown()
    {
        if ( ! $this->_response->isException()) {
            return;
        }

        if (!empty($this->_session->history)) {
            $history = $this->_session->history;
            $this->_last = current($history);
        }
        
        $response = $this->_response;
        if (false !== ($stack = $response->getExceptionByType('OSDN_Exception'))) {

            $e = current($stack);
            $this->_session->messages = $e->getMessageDetails();

            $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
            if (null != ($path = $this->_request->getParam('onExceptionBackPath'))) {
                $redirector->gotoSimpleAndExit($path['action'], $path['controller'], $path['module'], $path['params']);
            }

            $redirector->gotoUrlAndExit($this->_last);
        }

        $stack = $this->_response->getException();
        $e = current($stack);

        echo $e->getMessage();
    }
    
    public function getMessages()
    {
        return $this->_messages;
    }
    
    public function addMessage($message)
    {
        $this->_messages[] = $message;
        return $this;
    }
}