<?php

/**
 * HTTP digest authorization in administration
 *
 * @category    OSDN
 * @package     OSDN_Controller_Plugin
 * @version     $Id: Administration.php 18047 2010-04-22 10:01:34Z bvh $
 */
class OSDN_Controller_Plugin_Administration extends Zend_Controller_Plugin_Abstract
{
    /**
     * The pass to passwd file
     *
     * @var string
     */
    protected $_passwd = '';

    protected $_layoutPath;

    protected $_layout;

    protected $_domain = '/admin /json/admin';

    protected $_realm = 'Please login for changing site configuration';

    /**
     * The constructor
     *
     * @param $file The path to passwd file
     * @throws OSDN_Exception if empty file or file is not readable
     */
    public function __construct($file)
    {
        if (empty($file) || !Zend_Loader::isReadable($file)) {
            throw new OSDN_Exception("Password file `$file` is not readable.");
        }

        $this->_passwd = $file;
    }

    /**
     * Called after Zend_Controller_Router exits.
     *
     * Called after Zend_Controller_Front exits from the router.
     *
     * @param  Zend_Controller_Request_Abstract $request
     * @return void
     */
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        if ('admin' !== $request->getModuleName()) {
            return;
        }

        $config = array(
            'accept_schemes'    => 'digest',
            'realm'             => $this->_realm,
            'digest_domains'    => $this->_domain,
            'nonce_timeout'     =>  360*60
        );

        $adapter = new Zend_Auth_Adapter_Http($config);

        $digestResolver = new Zend_Auth_Adapter_Http_Resolver_File();
        $digestResolver->setFile($this->_passwd);

        $adapter->setDigestResolver($digestResolver);
        $adapter->setRequest(new Zend_Controller_Request_Http());
        $adapter->setResponse(new Zend_Controller_Response_Http());

        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();

        $auth->setStorage(new Zend_Auth_Storage_NonPersistent());
        $result = $auth->authenticate($adapter);

        if (true !== $result->isValid()) {
            $adapter->getResponse()->sendHeaders();
            echo join('<br/>', $result->getMessages());
            exit;
        } else {

            $storage = $auth->getStorage();
            $identity = $storage->read();
            $identity[OSDN_Accounts_Prototype::SUPERADMIN] = true;
            $identity[OSDN_Accounts_Prototype::ID] = -1;
            $storage->write((object) $identity);

            $mvcInstance = Zend_Layout::getMvcInstance();
            if (!empty($mvcInstance)) {
                $mvcInstance->setLayout(!empty($this->_layout) ? $this->_layout : 'admin');
                if (!empty($this->_layoutPath)) {
                    $mvcInstance->setLayoutPath($this->_layoutPath);
                    $mvcInstance->getView()->layoutPath = $this->_layoutPath;
                }
            }
        }
    }

    /**
     * Set up layout path
     *
     * @param string $layoutPath        The layout path
     * @return OSDN_Controller_Plugin_Administration
     */
    public function setLayoutPath($layoutPath)
    {
        $this->_layoutPath = $layoutPath;
        return $this;
    }

    public function setLayout($name)
    {
        $this->_layout = $name;
        return $this;
    }

    public function setRealm($realm)
    {
        $this->_realm = $realm;
        return $this;
    }

    public function setDigestDomain($domain)
    {
        $this->_domain = $domain;
    }
}