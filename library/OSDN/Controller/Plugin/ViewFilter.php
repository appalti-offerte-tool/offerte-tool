<?php

class OSDN_Controller_Plugin_ViewFilter extends Zend_Controller_Plugin_Abstract
{
    const IDENTITY = '93962CEE-FD0D-42A2-8CEB-91993234569C';

    protected static $_id;

    protected $_session;

    /**
     * Called before an action is dispatched by Zend_Controller_Dispatcher.
     *
     * This callback allows for proxy or filter behavior.  By altering the
     * request and resetting its dispatched flag (via
     * {@link Zend_Controller_Request_Abstract::setDispatched() setDispatched(false)}),
     * the current action may be skipped.
     *
     * @param  Zend_Controller_Request_Abstract $request
     * @return void
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $id = $request->getParam('_bid');

        if ($request->isPost() && is_array($params = $request->getParam(self::IDENTITY))) {

            if (isset($params['id'])) {
                $id = $params['id'];
                unset($params['id']);
            }
        }

        if (empty($id) && !$this->getRequest()->isXmlHttpRequest()) {
            if (is_null($request)) {
                $request = Zend_Controller_Front::getInstance()->getRequest();
            }

            $id = join('-', array(
                $request->getModuleName(),
                $request->getControllerName(),
                $request->getActionName()
            ));
        }

        $this->_session = new Zend_Session_Namespace(self::toId($id, $request));

        if (isset($params) && is_array($params)) {
            if (!empty($id) && isset($params['reset_' . $id])) {
                $params = array();
            } else if (isset($params['reset'])) {
                $params = array();
            } else if (!empty($id)) {
                $params['fid'] = $id;
            }

            $this->_session->filter = $params;
            $router = Zend_Controller_Front::getInstance()->getRouter();
            $u = $router->assemble(array());

            /*
             * Do not send "Location" header if it's an AJAX request
             * because browser send an extra GET request
             */
            if (!$this->getRequest()->isXmlHttpRequest() && empty($params)) {
                $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
                $redirector->gotoUrl($u);
            }
        }

        if (isset($this->_session->filter) && is_array($this->_session->filter)) {
            $request->setParam('filter', $this->_session->filter);
        }
    }

    public static function toId($id = null, Zend_Controller_Request_Abstract $request = null, $useRequest = false)
    {
        $ids = array('id');

        if (!empty($id)) {
            $ids[] = $id;
        }

        if (true === $useRequest) {
            if (is_null($request)) {
                $request = Zend_Controller_Front::getInstance()->getRequest();
            }

            $ids[] = $request->getModuleName();
            $ids[] = $request->getControllerName();
            $ids[] = $request->getActionName();
        }

        return join('-', $ids);
    }
}