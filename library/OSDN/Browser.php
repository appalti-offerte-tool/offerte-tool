<?php

/**
 * Identifies the user's Operating system and browser
 * by parsing the HTTP_USER_AGENT string sent to the server
 *
 * @category		OSDN
 * @package		OSDN_Browser
 * @version		$Id: Browser.php 18843 2010-09-24 06:59:15Z yaroslav $
 */
class OSDN_Browser
{
    /**
     * Is opera engine
     *
     * @return boolean
     */
    public static function isOpera()
    {
        return false !== stripos(self::_toAgent(), 'opera');
    }
    
    /**
     * Is Safari engine
     *
     * @return boolean
     */
    public static function isSafari()
    {
        return (boolean) preg_match('/webkit|khtml/i', self::_toAgent());
    }
    
    /**
     * Is Safari 3 engine
     *
     * @return boolean
     */
    public static function isSafari3()
    {
        return self::isSafari() && false !== stripos(self::_toAgent(), 'webkit/5');
    }
    
    /**
     * Is IE engine
     *
     * @return boolean
     */
    public static function isIE()
    {
        return !self::isOpera() && false !== stripos(self::_toAgent(), 'msie');
    }
    
    /**
     * Is IE 6
     *
     * @return boolean
     */
    public static function isIE6()
    {
    	return self::isIE() && !self::isIE7() && !self::isIE8();
    }
    
    /**
     * Is IE 7
     *
     * @return boolean
     */
    public static function isIE7()
    {
        return !self::isOpera() && false !== stripos(self::_toAgent(), 'msie 7');
    }
    
    /**
     * Is IE 8
     *
     * @return boolean
     */
	public static function isIE8()
    {
        return !self::isOpera() && false !== stripos(self::_toAgent(), 'msie 8');
    }
    
    /**
     * Is Gecko engine
     *
     * @return boolean
     */
    public static function isGecko()
    {
        return !self::isSafari() && false !== stripos(self::_toAgent(),'gecko');
    }
    
    /**
     * Is Gecko 3 engine
     *
     * @return boolean
     */
    public static function isGecko3()
    {
        return !self::isSafari() && false !== stripos(self::_toAgent(), 'rv:1.9');
    }
    
    /**
     * Is the abobe AIR agent
     *
     * @return boolean
     */
    public static function isAir()
    {
        return false !== stripos(self::_toAgent(), 'adobeair');
    }
    
    /**
     * Is secure connection used
     *
     * @return boolean
     */
    public static function isSecure()
    {
        return isset($_SERVER['HTTPS']) && 'on' == strtolower($_SERVER['HTTPS']);
    }
    
    /**
     * Is konqueror engine
     *
     * @return boolean
     */
    public static function isKonqueror()
    {
        return false !== stripos(self::_toAgent(), 'konqueror');
    }
    
    /**
     * Is linux platform
     *
     * @return boolean
     */
    public static function isLinux()
    {
        return false !== stripos(self::_toAgent(), 'linux');
    }
    
    /**
     * Is windows platform
     *
     * @return boolean
     */
    public static function isWindows()
    {
        return false !== stripos(self::_toAgent(), 'windows') ||
            false !== stripos(self::_toAgent(), 'win32');
    }
    
    /**
     * Is mac platform
     *
     * @return boolean
     */
    public static function isMac()
    {
        return false !== stripos(self::_toAgent(), 'macintosh') ||
            false !== stripos(self::_toAgent(), 'mac os x');
    }
    
    /**
     * Is iPhone platform
     *
     * @return boolean
     */
    public static function isIphone()
    {
        return false !== stripos(self::_toAgent(), 'iphone');
    }
    
    protected static function _toAgent()
    {
        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            return $_SERVER['HTTP_USER_AGENT'];
        }
        
        return "";
    }
}