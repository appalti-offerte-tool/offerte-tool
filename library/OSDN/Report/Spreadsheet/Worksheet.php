<?php

/**
 * Simplified version of PEAR worksheet writer
 * Of course if somebody needs very flexible class
 * plase use native PEAR worksheet writer
 *
 * @category	OSDN
 * @package	OSDN_Report_Spreadsheet
 * @author		Yaroslav Zenin <yaroslav.zenin@gmail.com>
 * @version	$Id: Worksheet.php 18844 2010-09-24 07:35:40Z yaroslav $
 *
 */
class OSDN_Report_Spreadsheet_Worksheet
{
    const METADATA_COLUMNS = 'columns';
    
    const METADATA_HEADLINES = 'headlines';
    
    protected $_metadata = array(
        self::METADATA_COLUMNS   => array(),
        self::METADATA_HEADLINES => array()
    );

    protected $_rowset = array();

    protected $_minWidth = 5;
    
    /**
     * Contains worksheet names
     *
     * And for every iteration looking on names
     * and increase index if name is duplicated
     *
     * @var array
     */
    protected static $_names = array();
    
    protected $_worksheet;
    
    /**
     * The PEAR workbook class is designed very stupid
     * and we cannot go futher without
     * containing internal workbook instance
     *
     * @var Spreadsheet_Excel_Writer_Workbook
     */
    protected $_workbook;

    /**
     * Contain callbacks collection for writing
     * and modifing columns
     *
     * @var array
     */
    protected $_callbacks = array(
    
        /**
         * Fires before write header method executed
         *
         * @param $column   string  The name of column
         * @param $value    mixed   The value of column
         * @param $format   Spreadsheet_Excel_Writer_Format     Basic column format
         * @param $options  The all options collection
         *
         * @return array    Will be merged with input options
         */
        'before_write_header'   => false,
    
        /**
         * Fires before write row method executed
         *
         * @param $record   array
         * @return
         *      false   The rendering will be skipped for this columns
         *      array   The result will be merged with input record
         */
        'before_write_row'      => false,
    
        /**
         * Fires before write column method executed
         *
         * @param $column   string  The name of column
         * @param $value    mixed   The value of column
         * @param $format   Spreadsheet_Excel_Writer_Format     Basic column format
         * @param $record   The all options collection
         *
         * @return
         */
        'before_write_row_column'   => false
    );
    
    public function __construct(Spreadsheet_Excel_Writer_Workbook $workbook, $name)
    {
        if (isset(self::$_names[$name])) {
            self::$_names[$name]++;
            $name = self::$_names[$name];
        } else {
            self::$_names[$name] = 0;
        }
        
        $this->_worksheet = $workbook->addWorksheet($name);
        $this->_workbook = $workbook;
    }
    
    public function setMetadata(array $metadata)
    {
        $this->_metadata = array(
            self::METADATA_COLUMNS   => array(),
            self::METADATA_HEADLINES => array()
        );
        
        
        foreach($metadata as $o => $value) {
            
            $o = strtolower($o);
            switch($o) {
                case self::METADATA_COLUMNS:
                    
                    $index = 0;
                    foreach($value as $column) {
                        
                        $name = $column['name'];
                        
                        $this->_metadata[self::METADATA_COLUMNS][$name] = array(
                            'index'     => $index,
                            'caption'   => $column['caption'],
                            'autowidth' => isset($column['autowidth']) && true === $column['autowidth'],
                            'width'     => 0,
                            'headlines' => $column['headlines']
                        );
                        
                        $index++;
                    }
                    
                    break;
                    
                case self::METADATA_HEADLINES:
                    $this->_metadata[self::METADATA_HEADLINES] = $value;
                    break;
            }
        }
        
        
    }
    
    public function columns()
    {
        return $this->_metadata[self::METADATA_COLUMNS];
    }
    
    public function setRowset(array $rowset)
    {
        $this->_rowset = $rowset;
        return $this;
    }

    public function setOnWriteHeadersCallback($callbackfn)
    {
        if (is_callable($callbackfn)) {
            throw new Exception('Cannot assign callback function');
        }
        
        $this->_callbacks['before_write_header'] = $callbackfn;
        return $this;
    }
    
    public function setOnWriteRecordCallback($callbackfn)
    {
        if (!is_callable($callbackfn)) {
            throw new Exception('Cannot assign callback function');
        }
        
        $this->_callbacks['before_write_row'] = $callbackfn;
        return $this;
    }
    
    public function setOnWriteRecordColumnCallback($callbackfn)
    {
        if (!is_callable($callbackfn)) {
            throw new Exception('Cannot assign callback function');
        }
        
        $this->_callbacks['before_write_row_column'] = $callbackfn;
        return $this;
    }
    
    public function getColumns()
    {
        return $this->_metadata[self::METADATA_COLUMNS];
    }
    
    public function getColumn($name, $prop = null)
    {
        $column = null;
        if (isset($this->_metadata[self::METADATA_COLUMNS][$name])) {
            $column = $this->_metadata[self::METADATA_COLUMNS][$name];
        }
        
        if (null != $prop && isset($column[$prop])) {
            return $column[$prop];
        }
        
        return $column;
    }
    
    protected function _setColumnValue($name, $prop, $value)
    {
        if (!isset($this->_metadata[self::METADATA_COLUMNS][$name][$prop])) {
            return false;
        }
        
        $this->_metadata[self::METADATA_COLUMNS][$name][$prop] = $value;
        return true;
    }
    
    protected function _doRenderRecord($index, $record)
    {
        
    }
    
    public function run()
    {
        $index = 0;
        $icolumn = 0;
        
        $maxHeadlineLevel = count($this->_metadata[self::METADATA_HEADLINES]);;
        $index += $maxHeadlineLevel;
        $columns = $this->columns();
        
        $isWriteHeaderCallable = is_callable($this->_callbacks['before_write_header']);
        $previousColumn = null;
        
        foreach($this->columns() as $column => $options) {
            $th = $this->getTh();
            
            if ($isWriteHeaderCallable) {
                $result = call_user_func_array(
                    $this->_callbacks['before_write_header'],
                    array($column, $options['caption'], $th, $options)
                );
                
                if (is_array($result)) {
                    $options += $result;
                }
            }
            
            $this->_worksheet->write($index, $icolumn, $options['caption'], $th);
            
            foreach($this->_metadata[self::METADATA_HEADLINES] as $hLevel => $headline) {
                
                foreach($options['headlines'] as $hId) {

                    $h = $this->_metadata[self::METADATA_HEADLINES][$hLevel];
                    
                    if (
                        isset($previousColumn['headlines'][$hLevel])
                        && $previousColumn['headlines'][$hLevel] == $hId
                    ) {
                        $this->_worksheet->setMerge($index - $maxHeadlineLevel + $hLevel, $icolumn - 1, $index - $maxHeadlineLevel + $hLevel, $icolumn);
                        continue;
                    }
                    
                    $this->_worksheet->write(
                        $index - $maxHeadlineLevel + $hLevel,
                        $icolumn,
                        $this->_metadata[self::METADATA_HEADLINES][$hLevel][$hId]['name'],
                        $th
                    );
                }
                
            }
            
            $icolumn++;
            $previousColumn = $options;
        }
        
        $isWriteRowCallable = is_callable($this->_callbacks['before_write_row']);
        $isWriteRowColumnCallable = is_callable($this->_callbacks['before_write_row_column']);
        
        $columns = array_keys($this->columns());
        foreach($this->_rowset as $row) {
            
            $index++;
            $icolumn = 0;
            
            $record = array_intersect_key($row, $this->columns());
            if ($isWriteHeaderCallable) {
                $result = call_user_func_array(
                    $this->_callbacks['before_write_row'],
                    array($record)
                );
                
                if (false === $result) {
                    continue;
                }
                
                if (is_array($result)) {
                    $record += $result;
                }
            }
            
            foreach($columns as $column) {
                
                $td = $this->getTd();
                $value = isset($record[$column]) ? $record[$column] : null;
                
                if ($isWriteRowColumnCallable) {
                    $result = call_user_func_array(
                        $this->_callbacks['before_write_row_column'],
                        array($column, $value, $td, $record)
                    );
                    
                    if (false !== $result) {
                        $value = $result;
                    }
                }
                
                if (false != ($autowidth = $this->getColumn($column, 'autowidth'))) {
                    
                    $len = strlen($value);
                    if ($len > $this->getColumn($column, 'width')) {
                        $this->_setColumnValue($column, 'width', $len);
                    }
                    
                }
                
                $this->_worksheet->write($index, $icolumn, $value, $td);
                $icolumn++;
            }
            
        }
        
        foreach($this->columns() as $column => $options) {

            $this->_worksheet->setColumn(
                $options['index'],
                $options['index'],
                max($this->_minWidth, !empty($options['width']) ? $options['width'] : 0)
            );

        }
    }

    public function setColumnsWidth(array $columns)
    {
        foreach($columns as $column => $value) {
            
            if (!array_key_exists($column, $this->columns())) {
                continue;
            }
            
            if (true === $value) {
                $this->_setColumnValue($column, 'autowidth', true);
            } elseif (is_numeric($value)) {
                $this->_setColumnValue($column, 'width', (int) $value);
            }
        }
    }
    
    public function setMinWidth($width)
    {
        $width = (int) $width;
        $this->_minWidth = max(5, $width);
        
        return $this;
    }
    
    protected function getTh(array $options = array())
    {
        $th = & $this->_workbook->addFormat($options);
        $th->setBold();
        $th->setSize(10);
        $th->setValign('vcenter');
        $th->setHalign('center');
        return $th;
    }
    
    protected function getTd(array $options = array())
    {
        $td = & $this->_workbook->addFormat($options);
        $td->setSize(10);
        $td->setValign('vcenter');
        return $td;
    }
}