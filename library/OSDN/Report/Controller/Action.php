<?php

class OSDN_Report_Controller_Action extends Zend_Controller_Action
{
    protected $_namespace;
    
    private $_hasCredentials = false;
    
    public function init()
    {
        $this->_namespace = 'h' . md5(get_class($this));
        parent::init();
    }
    
    public function permission(OSDN_Controller_Action_Helper_Acl $acl)
    {
        parent::permission($acl);
        
        $acl->isAllowed(OSDN_Acl_Privilege::VIEW, 'create-flash-session-data');
    }
    
    public function createFlashSessionDataAction()
    {
        $ns = new Zend_Session_Namespace($this->_namespace);
        $uniq = md5(uniqid());
        $ns->$uniq = $this->getRequest()->getPost();
        
        $this->view->success = true;
        $this->view->uniq = $uniq;
        return $uniq;
    }

    protected function _hasReportingCredentials($param = 'uniq')
    {
        $uniq = $this->_getParam($param);
        if (empty($uniq)) {
            return false;
        }
        
        $ns = new Zend_Session_Namespace($this->_namespace);
        
        $params = null;
        if (isset($ns->$uniq)) {
            $params = $ns->$uniq;
            unset($ns->$uniq);
        }
        
        if (is_array($params)) {
            $this->getRequest()->setParams($params);
            $this->_hasCredentials = true;
        }
        
        return $this->_hasCredentials;
    }
    
    public function preDispatch()
    {
        parent::preDispatch();

        if ($this->_hasReportingCredentials()) {
            
            $request = $this->getRequest();
            $request->setParam('start', null);
            $request->setParam('limit', null);
        }
    }
    
    public function toExcelReport(array $rowset, $callback = null)
    {
        $metadata = $this->_getParam('metadata');
        if (!is_array($metadata)) {
            return;
        }
        
        $spreadsheet = new OSDN_Report_Spreadsheet();
        $worksheet = $spreadsheet->attachWorksheet();
        
        if (is_array($metadata)) {
            $worksheet->setMetadata($metadata);
        }
        
        $worksheet->setRowset($rowset);
        
        $keys = array_keys($rowset[0]);
        $worksheet->setColumnsWidth(array_combine($keys, array_fill(0, count($keys), true)));
        
        if (is_callable($callback)) {
            call_user_func_array($callback, array($worksheet));
        }
        
        $spreadsheet->dump();
        
        die;
    }
}