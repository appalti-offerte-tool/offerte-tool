<?php

class OSDN_Report_Spreadsheet
{
    /**
     * @var Spreadsheet_Excel_Writer
     */
    protected $_workbook;
    
    protected $_worksheets = array();
    
    public function __construct()
    {
        /**
         * The old spreadsheet library works in PHP4
         * so here is optional if in error_reporting deprecated
         * functions will be disabled
         */
        $reporting = E_ALL ^ E_NOTICE;
        
        if (defined('E_DEPRECATED')) {
            $reporting ^= E_DEPRECATED;
        }
        error_reporting($reporting);
                
        $this->_workbook = new Spreadsheet_Excel_Writer();
    }
    
    /**
     *
     * @param $name string  The name of worksheet [OPTIONAL]
     * @return OSDN_Report_Spreadsheet_Worksheet
     */
    public function attachWorksheet($name = 'Untitled')
    {
        $worksheet = new OSDN_Report_Spreadsheet_Worksheet($this->_workbook, $name);
        $this->_worksheets[] = $worksheet;
        
        return $worksheet;
    }
    
    public function dump($filename = 'Untitled.xls')
    {
        $this->_workbook->send($filename);
        $this->toString();
    }
    
    public function __toString()
    {
        return $this->toString();
    }
    
    public function toString()
    {
        foreach($this->_worksheets as $worksheet) {
            $worksheet->run();
        }
        
        $this->_workbook->close();
    }
}