<?php

/**
 * The remote translation adapter
 * 
 * @category    OSDN
 * @package     OSDN_Translation
 * @version     $Id: $
 */
class OSDN_Translation_Adapter_Remote extends Zend_Translate_Adapter
{
	const CACHE_TAG = 'Zend_Translate';
	
    /**
     * Remote client
     * 
     * @var OSDN_Translation_Adapter_Remote_Abstract
     */
    protected $_client;
    
    protected $_cacheRepository;
    
    /**
     * The path to translation file
     * 
     * @var string
     */
    protected $_phrasesFile;
    
    /**
     * @var string
     */
    protected $_phrases = array();
    
    protected $_oldPhrases = array();
    
    public function __construct($data, $locale = null, array $options = array())
    {
        if (empty($options['client'])) {
            throw new Exception('The client is required for remote adapter.');
        }
        
        if (!$options['client'] instanceof OSDN_Translation_Adapter_Remote_Abstract) {
            throw new Exception('The client must be instance of OSDN_Translation_Adapter_Remote_Abstract');
        }
        
        $this->_client = $options['client'];
        $translationCache = Zend_Cache::factory('Core', 'File', array(
            'lifeTime' => 3600,
            'automatic_serialization' => true
        ), array(
            'cache_dir' => CACHE_DIR
        ));

        $this->_cacheRepository = $translationCache;
        $this->_phrasesFile = CACHE_DIR . '/translation_phrases.txt';
        
        parent::__construct($data, $locale, $options);
    }
    
    /**
     * The destructor
     * Write to file used translation if it present
     * @return void
     */
    public function __destruct()
    {
        if (!empty($this->_phrases)) {
            
            if (file_exists($this->_phrasesFile)) {
                $flag = FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES;
                $phrases = array_map('trim', file($this->_phrasesFile, $flag));
                $this->_phrases = array_unique(array_merge($phrases, $this->_phrases));
            }
            file_put_contents($this->_phrasesFile, join("\n", $this->_phrases));
        }
    }
    
    /**
     * @see Zend_Translate_Adapter::_loadTranslationData()
     */
    protected function _loadTranslationData($data, $locale, array $options = array())
    {
        if (is_array($data)) {
            if (empty($this->_translate[$locale]) || !is_array($this->_translate[$locale])) {
                $this->_translate[$locale] = array();
            }
            foreach ($data as $key => $translate) {
                if (!$this->isTranslated($key)) {
                    $this->_translate[$locale][$key] = $translate;
                }
            }
        } else {
        
        	$cacheId = 'translation_' . $locale;
            if (!$rowset = $this->_cacheRepository->load($cacheId)) {
                
                // retrieve new phrases
                if (file_exists($this->_phrasesFile)) {
                    $flag = FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES;
                    $phrases = array_map('trim', file($this->_phrasesFile, $flag));
                    $this->_client->setPhrases($phrases);
                }
                
                $rowset = $this->_client->fetchAll($locale);
                $this->_cacheRepository->save($rowset, $cacheId, array(self::CACHE_TAG));
                
                // clear phrases
                file_put_contents($this->_phrasesFile, "");
            }
            
            if (!is_array($rowset)) {
                throw new Zend_Translate_Exception("Error fetching data");
            }
            
            $options = $options + $this->_options;
            if (($options['clear'] == true) || !isset($this->_translate[$locale])) {
                $this->_translate[$locale] = array();
            }
            
            $this->_oldPhrases = array();
            foreach ($rowset as $row) {
                $this->_translate[$locale][$row['caption']] = $row['translation'];
                if ($row['status'] == OSDN_Translation_Table::STATUS_OLD) {
                    $this->_oldPhrases[] = $row['caption'];
                }
            }
        }
        
        if (empty($this->_translate[$locale])) {
            $this->_translate[$locale] = true;
        }
    }
    
    /**
     * @see Zend_Translate_Adapter::translate()
     */
    public function translate($messageId, $locale = null)
    {
        if (is_null($locale)) {
            $locale = $this->getLocale();
        }
        
        if (!$this->isTranslated($messageId)) {
            $translation = '_' . $messageId;
            $this->_phrases[] = $messageId;
            
            $this->addTranslation(array($messageId => ""), $locale);
            return $translation;
        }
    
        if (in_array($messageId, $this->_oldPhrases)) {
            $this->_phrases[] = $messageId;
        }
        
        $translation = parent::translate($messageId, $locale);
        return "" == $translation ? ('_' . $messageId) : $translation;
    }
    
    /**
     * @see Zend_Translate_Adapter::toString()
     */
    public function toString()
    {
        return 'Remote';
    }
}