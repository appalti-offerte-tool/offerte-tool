<?php

/**
 * Simple mysql adapter for translation
 *
 * @category OSDN
 * @package  OSDN_Translation
 * @version  $Id: Mysql.php 20708 2012-10-03 14:22:55Z vanya $
 */
class OSDN_Translation_Adapter_Mysql extends Zend_Translate_Adapter
{
    /**
     * Load translation data
     *
     * @param  mixed              $data
     * @param  string|Zend_Locale $locale
     * @param  array              $options (optional)
     * @return void
     */
    public function _loadTranslationData($data, $locale, array $options = array())
    {
        if (is_array($data)) {
            foreach ($data as $key => $translate) {
                if (!$this->isTranslated($key)) {
                    $this->_translate[$locale][$key] = $translate;
                }
            }
        } else {

            $response = $this->_model()->fetchAllByLocale($locale);
            $rowset = $response->getRowset();
            $options = $options + $this->_options;
            if (($options['clear'] == true) || !isset($this->_translate[$locale])) {
                $this->_translate[$locale] = array();
            }

            foreach ($rowset as $row) {
                $this->_translate[$locale][$row['caption']] = $row['translation'];
            }
        }

        if (empty($this->_translate[$locale])) {
            $this->_translate[$locale] = true;
        }

        return $this->_translate;
    }

    /**
     * Translates the given string
     * returns the translation
     *
     * @see Zend_Locale
     * @param  string             $messageId Translation string
     * @param  string|Zend_Locale $locale    (optional) Locale/Language to use, identical with
     *                                       locale identifier, @see Zend_Locale for more information
     * @param string              $internal If internal matched with locale then will translated now
     * @return string
     */
    public function translate($messageId, $locale = null, $internal = null)
    {
        if (is_null($locale)) {
            $locale = $this->getLocale();
        }

        $md5Message = md5($messageId);
        if (!$this->isTranslated($md5Message)) {
            $translation = '_' . $messageId;

            $this->_model()->createNewCaptions(array($messageId));
            return $translation;
        }

        $translation = parent::translate($md5Message, $locale);
        return "" == $translation ? ('_' . $messageId) : $translation;
    }

    /**
     * Create translation model
     *
     * @return OSDN_Translation_Model
     */
    protected function _model()
    {
        if (empty($this->_model)) {
            $this->_model = new I18n_Service_Translation();
        }

        return $this->_model;
    }

    /**
     * Returns the adapter name
     *
     * @return string
     */
    public function toString()
    {
        return 'Mysql';
    }
}