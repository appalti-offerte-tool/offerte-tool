<?php

abstract class OSDN_Translation_Adapter_Remote_Abstract
{
    protected $_client;
    
    protected $_phrases = array();

    protected $_type;
        
    abstract function fetchAll($locale);
    
    public function setPhrases(array $phrases)
    {
        $this->_phrases = array_unique($phrases);
        return $this;
    }
    
    public function hasClient()
    {
        return !is_null($this->_client);
    }
    
    public function setType($type)
    {
        $this->_type = $type;
        return $this;
    }
} 