<?php

class OSDN_Translation_Adapter_Remote_XmlRpc extends OSDN_Translation_Adapter_Remote_Abstract
{
    /**
     * Default client object
     *
     * @var Zend_XmlRpc_Client
     */
    protected $_client;
    
    /**
     * The constructor
     *
     * @param Zend_XmlRpc_Client $client [optional]
     * @return void
     */
    public function __construct($client = null)
    {
        if (!empty($client)) {
            $this->setClient($client);
        }
    }
    
    /**
     * Set up client
     *
     * @param Zend_XmlRpc_Client|array $client
     * @return OSDN_Translation_Adapter_Remote_XmlRpc
     */
    public function setClient($client)
    {
        if (is_array($client)) {
            $client = call_user_func_array($client, array());
        }
        
        if (empty($client) || !(
            $client instanceof OSDN_XmlRpc_Client_Proxy_Request ||
            $client instanceof Zend_XmlRpc_Client_ServerProxy
        )) {
            throw new Exception('The client must be instance of OSDN_XmlRpc_Client_Proxy_Request or Zend_XmlRpc_Client_ServerProxy');
            
        }
            
        $this->_client = $client;
        return $this;
    }
    
    /**
     * Retrieve the XML-RPC client
     *
     * @return Zend_XmlRpc_Client
     */
    protected function _client()
    {
        if (empty($this->_client)) {
            throw new Exception('The XML-RPC client is empty');
        }
        
        return $this->_client;
    }
    
    /**
     * @TODO rebuild to more abstract structure
     *
     * @return $response
     */
    public function fetchAll($locale)
    {
        $response = new OSDN_Response();
        $phrases = $this->_phrases;
        
        
        
        // @todo acorrding to bug in xmlrpc conversion empty arrays
        if (empty($phrases)) {
            $phrases = null;
        }
        
        /**
         * FIXME
         * @var unknown_type
         */
        $result = $this->_client()->translation->fetchAll($locale, $this->_type, empty($phrases) ? array() : $phrases);
        $response->import($result);
        $rowset = array();
        if ($response->isSuccess()) {
            $rowset = $response->getRowset();
        }
        
        return $rowset;
    }
}