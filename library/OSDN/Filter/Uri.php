<?php

/**
 * @author      Yaroslav Zenin <yaroslav.zenin@gmail.com>
 * @version     $Id: Uri.php 18715 2010-09-02 07:38:13Z yaroslav $
 */
class OSDN_Filter_Uri implements Zend_Filter_Interface
{
    /**
     * Defined by Zend_Filter_Interface
     *
     * @param  string $value
     * @return string
     */
    public function filter($value)
    {
        $u = parse_url($value);
        
        if (isset($u['query'])) {

            $q = $u['query'];
            $q = str_replace('&amp;', '&', $q);
            $query = array();

            foreach(explode('&', $q) as $qClause) {
                
                $parts = explode('=', $qClause);
                $value = !empty($parts[1]) ? $parts[1] : '';
                if (!$this->_isEncoded($value)) {
                    $value = urlencode($value);
                }
                
                $query[] = $parts[0] . '=' . $value;
            }
            
            $u['query'] = join('&amp;', $query);
        }
        
        $output = $u['scheme'] . '://';
        if (!empty($u['username'])) {
            $output .= $u['username'];
            if (!empty($u['password'])) {
                $output .= $u['password'];
            }
            
            $output .= '@';
        }
        
        $output .= $u['host'];
        
        if (!empty($u['port'])) {
            $output .= ':' . $u['port'];
        }
        
        if (!empty($u['path'])) {
            $output .= $u['path'];
        }
        
        if (!empty($u['query'])) {
            $output .= '?' . $u['query'];
        }
        
        if (!empty($u['fragment'])) {
            $output .= '#' . $u['fragment'];
        }
        
        return $output;
    }
    
    protected function _isEncoded($value)
    {
        $u = $value;
        while(urldecode($u) != $u){
            $u = urldecode($u);
        }
        
        return urlencode($u) == $value;
    }
}