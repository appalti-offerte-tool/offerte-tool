<?php

/**
 * OSDN_Filter_Input
 *
 * @category OSDN
 * @package OSDN_Filter
 */
class OSDN_Filter_Input extends Zend_Filter_Input
{
    /**
     * OSDN_Filter_Input constructor
     *
     * @param array $filterRules
     * @param array $validatorRules
     * @param array $data       OPTIONAL
     * @param array $options    OPTIONAL
     *
     * @todo remove self::INPUT_NAMESPACE because it's deprecated
     */
    public function __construct($filterRules, $validatorRules, array $data = null, array $options = null)
    {
        $this->setOptions(array(
            self::INPUT_NAMESPACE => 'OSDN_Validate',
            self::ESCAPE_FILTER   => 'StringTrim'
        ));
        parent::__construct($filterRules, $validatorRules, $data, $options);

        $this->addFilterPrefixPath('OSDN_Filter', dirname(__FILE__));
    }

    /**
     * Retrieve all data previously escaped
     *
     * @return array
     */
    public function getData()
    {
        $unescaped = $this->getEscaped();
        $unknown = $this->_escapeRecursive($this->getUnknown());

        /**
         * The function "array_merge" does not work when fields are numeric,
         * after execution we lost all keys, so we use "+" here
         */
        return $unescaped + $unknown;
    }

    /**
     * Get escaped field
     * If field not present in valid field then try get it from unknown
     * maybe validator is not set for this field
     *
     * @param string $fieldName     OPTIONAL
     * @return mixed
     */
    public function getEscaped($fieldName = null)
    {
        $value = parent::getEscaped($fieldName);
        if (null !== $fieldName && is_null($value)) {
            if (array_key_exists($fieldName, $this->_unknownFields)) {
                return $this->_escapeRecursive($this->_unknownFields[$fieldName]);
            }
        }
        return $value;
    }

    /**
     * Get unescaped field
     *
     * If field not present in valid field then try get it from unkown
     * maybe validator is not set for this field
     *
     * @param string $fieldName OPTIONAL
     * @return mixed
     */
    public function getUnescaped($fieldName = null)
    {
        $value = parent::getUnescaped($fieldName);
        if (null !== $fieldName && is_null($value)) {
            if (array_key_exists($fieldName, $this->_unknownFields)) {
                return $this->_unknownFields[$fieldName];
            }
        }
    }

    public function validate($throwException = true)
    {
        if ($this->isValid()) {
            return true;
        }

        if (false !== $throwException) {
            $e = new OSDN_Validate_Exception();
            $e->assign($this->getMessages());
            throw $e;
        }

        return false;
    }
}