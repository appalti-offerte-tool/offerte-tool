<?php

/**
 * OSDN_Exception
 *
 * @category OSDN
 * @package OSDN_Exception
 * @version $Id: Exception.php 20506 2012-05-07 11:39:28Z yaroslav $
 */
class OSDN_Exception extends Exception implements IteratorAggregate // ArrayAccess
{
    protected $_messages = array();

    public function __construct($message = '', $code = 0, Exception $previous = null)
    {
        if (!empty($message)) {
            $this->addMessage($message instanceof Exception ? $message->getMessage() : $message);
        }

        parent::__construct($message, $code, $previous);
    }

    /**
     * Return message collection
     *
     * @return array
     */
    public function getMessages()
    {
        return $this->_messages;
    }

    /**
     * getIterator() - complete the IteratorAggregate interface, for iterating
     *
     * @return ArrayObject
     */
    public function getIterator()
    {
        if ($this->hasMessages()) {
            return new ArrayObject($this->getMessages());
        }

        return new ArrayObject();
    }

    public function hasMessages()
    {
        return count($this->_messages) > 0;
    }

    /**
     * Attach new exception message to collection
     *
     * @param $message string|OSDN_Exception_Message
     * @param $args    array
     * @param $field   string
     * @param $e       int      message type
     *
     * @return OSDN_Exception_Message
     */
    public function addMessage($message, array $args = array(), $field = "", $e = E_USER_ERROR)
    {
        if (is_string($message)) {
            $message = new OSDN_Exception_Message($message, $args);
        } elseif ( ! $message instanceof OSDN_Exception_Message) {
            throw new OSDN_Exception(sprintf(
                'Unable to create exception message from type: "%s"', gettype($message)
            ));
        }

        if (!empty($field)) {
            $message->setField($field);
        }

        $message->setType($e);

        $this->_messages[] = $message;
        return $message;
    }

    /**
     * Assign multiple items to collection
     *
     * @param $messages  array
     *  <code>
     *      array('test');
     *      array('test', 'test1');
     *      array(
     *          array('test "%d"', array(1), 'field1', E_USER_NOTICE),
     *          array('test "%d"', array(2), 'field2', E_USER_WARNING),
     *          array('test "{name}"', array('name' => 'test name'), E_USER_NOTICE)
     *      );
     *      array(
     *          new OSDN_Exception_Message('message1'),
     *          new OSDN_Exception_Message('message2')
     *      );
     *  </code>
     * @param $field    string
     * @param $e        int
     *
     * @return array    The array of last added messages
     */
    public function assign(array $messages, $field = null, $e = E_USER_ERROR)
    {
        $self = $this;
        $f = function($msg) use($self, & $f) {

            $o = array();

            if (is_string($msg) || $msg instanceof OSDN_Exception_Message) {
                $o[] = $self->addMessage($msg);
                return $o;
            }

            if (!is_array($msg)) {
                return $o;
            }

            $argsc = count($msg);
            if (
                $argsc >= 2 &&
                array_keys($msg) === range(0, $argsc - 1)
            ) {

                $field = null;
                if ($argsc > 2) {
                    list($m, $a, $field) = $msg;
                } else {
                    list($m, $a) = $msg;
                }

                if (is_string($m) && is_array($a)) {

                    $o[] = $self->addMessage($m, $a, $field);
                    return $o;
                }
            }

            foreach($msg as $ms) {
                $o = array_merge($o, $f($ms));
            }

            return $o;
        };

        return $f($messages);
    }

    public function toArray(Closure $callbackFn = null)
    {
        $output = array();
        foreach($this->getMessages() as $message) {

            if (null !== $callbackFn) {
                $message = $callbackFn($message);
            }

            $output[] = $message->toArray();
        }

        return $output;
    }

    /**
     * Dump to string
     *
     * @return string
     */
    public function toString()
    {
        $output = array();
        foreach($this->getMessages() as $message) {
            $output[] = $message->getMessage();
        }

        return join(";\n", $output);
    }
}