<?php

/**
 * Class to store and retrieve the version of OSDN API.
 *
 * @category OSDN
 * @package OSDN
 * @version $Id: Version.php 19603 2011-05-30 10:14:25Z yaroslav $
 */
final class OSDN_Version
{
    /**
     * Current version
     *
     * @var string
     */
    protected $_version;

    /**
     * Last modified date
     *
     * @var string
     */
    protected $_date;

    /**
     * Contain instance
     *
     * @var OSDN_Version
     */
    protected static $_instance;

    /**
     * The constructor
     *
     * @param string $file      Path to version file
     * @return void
     */
    protected function __construct($file)
    {
        if (empty($file) && defined('APPLICATION_PATH')) {
            $file = APPLICATION_PATH . '/configs/version.xml';
        }

        if (!file_exists($file) || !Zend_Loader::isReadable($file)) {
            return;
        }

        $xml = simplexml_load_file($file);

        $revision = (int) $xml->logentry->attributes()->revision;
        if (!empty($revision)) {
            $this->_version = '0.2.' . $revision;
        }

        $date = date('d-m-Y H:i:s', strtotime((string) $xml->logentry->date));
        if (!empty($date)) {
            $this->_date = $date;
        }
    }

    /**
     * Create instance
     *
     * @param string $file [optional]       Path to file
     * @return OSDN_Version
     */
    public static function getInstance($file = null)
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self($file);
        }

        return self::$_instance;
    }

    /**
     * Retrieve date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->_date;
    }

    /**
     * Retrieve version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->_version;
    }
}