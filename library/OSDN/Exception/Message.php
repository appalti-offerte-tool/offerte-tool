<?php

/**
 * Default exception item
 *
 * Simply way to get lightweight, configurable, translateable items
 * of exception message
 *
 * @version $Id: Message.php 19605 2011-06-02 08:06:40Z yaroslav $
 */
class OSDN_Exception_Message
{
    protected $_message;

    protected $_args = array();

    protected $_code;

    protected $_field;

    protected $_type = E_USER_ERROR;

    protected $_isTranslated = false;

    protected $_translator;

    /**
     * Default translator
     *
     * @var Zend_Translate
     */
    protected static $_defaultTranslator;

    public function __construct($message, array $args = array(), $field = null)
    {
        $this->_message = $message;
        $this->_args = $args;
        $this->_field = $field;
    }

    public function setType($type)
    {
        if (!in_array($type, array(E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE))) {
            throw new OSDN_Exception('Unable to apply message type.');
        }

        $this->_type = $type;
        return $this;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function setField($field)
    {
        $this->_field = $field;
        return $this;
    }

    public function wrapFieldWithNamespace($namespace)
    {
        $field = $this->getField();
        if (empty($field)) {
            return $this;
        }

        if (']' == substr($field, -1)) {

            $leftBracketPosition = stripos($field, '[');
            $this->_field = sprintf(
                '%s[%s]%s',
                $namespace,
                substr($field, 0, $leftBracketPosition),
                substr($field, $leftBracketPosition)
            );
        } else {
            $this->_field = sprintf('%s[%s]', $namespace, $field);
        }

        return $this;
    }

    public function getField()
    {
        return $this->_field;
    }

    public function setMessageArgs(array $args)
    {
        $this->_args = $args;
        return $this;
    }

    public function markAsTranslated()
    {
        $this->_isTranslated = true;
    }

    public function getMessage()
    {
        $message = $this->_message;

        if ($this->_isTranslated) {
            $message = $this->getTranslator()->translate($message);
        }

        if (!empty($this->_args)) {

            $argsKeys = array_keys($this->_args);
            $isPlacementReplacement = $argsKeys === range(0, count($this->_args) - 1);
            if ($isPlacementReplacement) {
                $args = $this->_args;
                array_unshift($args, $message);
                $message = call_user_func_array('sprintf', $args);
            } else {

                $toReplace = array_map(function($v) {
                    return '{' . $v . '}';
                }, $argsKeys);

                $message = str_replace($toReplace, $this->_args, $message);
            }
        }

        return $message;
    }

    public static function setDefaultTranslator(Zend_Translate $translator)
    {
        self::$_defaultTranslator = $translator;
    }

    /**
     * Get the default translator
     *
     * @return Zend_Translate
     */
    public static function getDefaultTranslator()
    {
        return self::$_defaultTranslator;
    }

    public function setTranslator(Zend_Translate $translator)
    {
        $this->_translator = $translator;
    }

    /**
     * Get the translator
     *
     * @throws OSDN_Exception When no translator can be found
     * @return Zend_Translate
     */
    public function getTranslator()
    {
        if ($this->_translator !== null) {
            return $this->_translator;
        } if (null !== ($translator = self::getDefaultTranslator())) {
            return $translator;
        } else {
            try {
                $translator = Zend_Registry::get('Zend_Translate');
            } catch (Zend_Exception $e) {
                $translator = null;
            }

            if ($translator instanceof Zend_Translate) {
                $this->_translator = $translator;
                return $translator;
            }
        }

        throw new OSDN_Exception('Could not find a translator');
    }

    public function toArray()
    {
        return array(
            'message'    => $this->getMessage(),
            'code'       => $this->_code,
            'field'      => $this->getField(),
            'type'       => $this->getType()
        );
    }

    public function __toString()
    {
        try {
            return  $this->getMessage();
        } catch (Exception $e) {
            return sprintf('%s::%s - %s', __METHOD__, __LINE__, $e->getMessage());
        }
    }
}