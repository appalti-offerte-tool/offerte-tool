<?php

/**
 * OpenId provider storage
 *
 * @category	OSDN
 * @package	OSDN_OpenId
 * @author		Yaroslav Zenin <yaroslav.zenin@gmail.com>
 * @version	$Id: Db.php 17530 2010-03-14 20:46:11Z yaroslav $
 */
class OSDN_OpenId_Provider_Storage_Db extends Zend_OpenId_Provider_Storage
{
	/**
	 * @var OSDN_OpenId_Consumer_Storage_Db_Assiciation_Table
	 */
	private $_associationTable = null;
	
	/**
	 * @var OSDN_OpenId_Consumer_Storage_Db_Users_Table
	 */
	private $_usersTable = null;
	
	/**
	 * @var OSDN_OpenId_Consumer_Storage_Db_Sites_Table
	 */
	private $_sitesTable = null;
	
	/**
     * Stores information about session identified by $handle
     *
     * @param string $handle assiciation handle
     * @param string $macFunc HMAC function (sha1 or sha256)
     * @param string $secret shared secret
     * @param string $expires expiration UNIX time
     * @return void
     */
    public function addAssociation($handle, $macFunc, $secret, $expires)
    {
    	$this->_associationTable()->insert(array(
            'handle'  => $handle,
            'macFunc' => $macFunc,
            'secret'  => base64_encode($secret),
            'expires' => $expires,
    	));

    	return true;
    }
    
    /**
     * Gets information about association identified by $handle
     * Returns true if given association found and not expired and false
     * otherwise
     *
     * @param string $handle assiciation handle
     * @param string &$macFunc HMAC function (sha1 or sha256)
     * @param string &$secret shared secret
     * @param string &$expires expiration UNIX time
     * @return bool
     */
    public function getAssociation($handle, &$macFunc, &$secret, &$expires)
    {
    	$this->_associationTable()->deleteQuote(array(
			'expires < ?' => time()
    	));

    	$row = $this->_associationTable()->fetchRow(array(
    		'handle = ?' => $handle
    	));
    	
    	if (is_null($row)) {
    		return false;
    	}
    	
        $macFunc = $row->macFunc;
        $secret  = base64_decode($row->secret);
        $expires = $row->expires;
        
        return true;
    }
    
	/**
     * Removes information about association identified by $handle
     *
     * @param string $handle assiciation handle
     * @return bool
     */
    public function delAssociation($handle)
    {
        $this->_associationTable()->deleteQuote(array(
    		'handle = ?'	=> $handle
    	));
    	
        return true;
    }
    
    /**
     * Register new user with given $id and $password
     * Returns true in case of success and false if user with given $id already
     * exists
     *
     * @param string $id user identity URL
     * @param string $password encoded user password
     * @return int  The record id
     */
    public function addUser($id, $password)
    {
        $rowId = $this->_usersTable()->insert(array(
        	'identity'	=> $id,
        	'password'	=> $password
        ));
        
        return $rowId;
    }

    /**
     * Returns true if user with given $id exists and false otherwise
     *
     * @param string $id user identity URL
     * @return bool
     */
    public function hasUser($id)
    {
    	return $this->_usersTable()->count(array('identity = ?' => $id), 1) > 0;
    }

    public function isActive($id)
    {
    	return $this->_usersTable()->count(array(
    	   'identity = ?'  => $id,
    	   'status = ?'    => OSDN_OpenId_Provider_Storage_Db_Users_Table::STATUS_ACTIVE
    	), 1) > 0;
    }
    
    /**
     * Verify if user with given $id exists and has specified $password
     *
     * @param string $id user identity URL
     * @param string $password user password
     * @return bool
     */
    public function checkUser($id, $password)
    {
    	$count = $this->_usersTable()->count(array(
    		'identity = ?'	=> $id,
    		'password = ?'	=> $password,
    	    'status = ?'    => OSDN_OpenId_Provider_Storage_Db_Users_Table::STATUS_ACTIVE
    	), 1);
    	
    	return $count > 0;
    }

    /**
     * Returns array of all trusted/untrusted sites for given user identified
     * by $id
     *
     * @param string $id user identity URL
     * @return array
     */
    public function getTrustedSites($id)
    {
    	$row = $this->_usersTable()->fetchRow(array(
    		'identity = ?'	=> $id
    	));
    	
    	$output = array();
    	if (is_null($row)) {
    		return $output;
    	}
    	
    	$select = $this->_sitesTable()->select()
    		->where('user_id = ?', $row->id);
    		
    	$query = $select->query();
    	
    	/**
    	 * It is not complitely clear assign extension to storage
    	 * But I'm realy do not know how to do it in another way
    	 *
    	 * @todo  Remove extension from here...
    	 *         Make more abstration layer...
    	 */
    	$tableRecord = new OSDN_OpenId_Extension_Record_Table();
    	$recordRow = $tableRecord->fetchRow(array(
    	    'id = ?'  => $row->id
    	));
    	
    	if (empty($recordRow)) {
    		return $output;
    	}
    	$recordRow = $recordRow->toArray();
        
    	while($tsRow = $query->fetch()) {
    		if (!empty($tsRow['trusted'])) {
    			$trusted = unserialize($tsRow['trusted']);
    			if (false !== $trusted) {
                    $trusted['OSDN_OpenId_Extension_Record'] = $recordRow;
    			}
    			
    			$output[$tsRow['site']] = $trusted;
    		}
    	}

    	return $output;
    }

    /**
     * Stores information about trusted/untrusted site for given user
     *
     * @param string $id user identity URL
     * @param string $site site URL
     * @param mixed $trusted trust data from extensions or just a boolean value
     * @return bool
     */
    public function addSite($id, $site, $trusted)
    {
    	$row = $this->_usersTable()->fetchRow(array(
    		'identity = ?'	=> $id
    	));
    	
    	if (is_null($row)) {
    		return false;
    	}
    	
    	$table = $this->_sitesTable();
    	if ($table->count(array(
    		'user_id = ?' 	=> $row->id,
    		'site = ?'		=> $site
    	)) > 0) {
    		
    		$affectedRows = $table->updateQuote(array(
    			'trusted'	=> serialize($trusted)
    		), array(
    			'user_id = ?'	=> $row->id,
    			'site = ?'		=> $site,
    		));
    		
    		return false !== $affectedRows;
    	}
    	
    	$rowId = $table->insert(array(
    		'user_id'	=> $row->id,
    		'site'		=> $site,
    		'trusted'	=> serialize($trusted)
    	));
    	
    	return $rowId > 0;
    }
    
    /**
     * Convert identity to record id
     *
     * @return int|false
     */
    public function identity2id($identity)
    {
        $row = $this->_usersTable()->fetchRow(array(
            'identity = ?'  => $identity
        ));
        
        return !empty($row) ? $row->id : false;
    }
    
	/**
	 * Retrieve the association table object
	 *
	 * @return OSDN_OpenId_Consumer_Storage_Db_Association_Table
	 */
	protected function _associationTable()
	{
		if (is_null($this->_associationTable)) {
			$this->_associationTable = new OSDN_OpenId_Provider_Storage_Db_Association_Table();
		}
		
		return $this->_associationTable;
	}
	
	/**
	 * Retrieve the users table object
	 *
	 * @return OSDN_OpenId_Consumer_Storage_Db_Users_Table
	 */
	protected function _usersTable()
	{
		if (is_null($this->_usersTable)) {
			$this->_usersTable = new OSDN_OpenId_Provider_Storage_Db_Users_Table();
		}
		
		return $this->_usersTable;
	}
	
	/**
	 * Retrieve the users table object
	 *
	 * @return OSDN_OpenId_Consumer_Storage_Db_Sites_Table
	 */
	protected function _sitesTable()
	{
		if (is_null($this->_sitesTable)) {
			$this->_sitesTable = new OSDN_OpenId_Provider_Storage_Db_Sites_Table();
		}
		
		return $this->_sitesTable;
	}
}