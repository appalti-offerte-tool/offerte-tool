CREATE TABLE IF NOT EXISTS `openid_provider_association` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `handle` varchar(255) NOT NULL,
  `macFunc` char(16) NOT NULL,
  `secret` varchar(255) NOT NULL,
  `expires` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `handle` (`handle`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `openid_provider_sites` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `user_id` int(11) unsigned NOT NULL,
  `site` varchar(255) NOT NULL,
  `trusted` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_id` (`user_id`,`site`),
  KEY `site` (`site`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `openid_provider_users` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `identity` varchar(255) NOT NULL,
  `password` char(32) NOT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `identity` (`identity`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `openid_provider_sites`
  ADD CONSTRAINT `openid_provider_sites_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `openid_provider_users` (`id`) ON DELETE CASCADE;
