<?php

/**
 * OpenId database users storage
 *
 * @category	OSDN
 * @package	OSDN_OpenId
 * @author		Yaroslav Zenin <yaroslav.zenin@gmail.com>
 * @version	$Id: Table.php 17528 2010-03-14 16:01:41Z yaroslav $
 *
 */
class OSDN_OpenId_Provider_Storage_Db_Users_Table extends OSDN_Db_Table_Abstract
{
	const STATUS_ACTIVE = 'ACTIVE';
	
	const STATUS_INACTIVE = 'INACTIVE';
	
	protected $_name = 'provider_users';
	
    /**
     * Support method for fetching rows.
     *
     * @param  Zend_Db_Table_Select $select  query options.
     * @return array An array containing the row results in FETCH_ASSOC mode.
     */
    protected function _fetch(Zend_Db_Table_Select $select)
    {
        $stmt = $this->_db->query($select);
        $rowset = array();
        while($row = $stmt->fetch(Zend_Db::FETCH_ASSOC)) {
	        if (array_key_exists('password', $row)) {
	            unset($row['password']);
	        }
	        
	        $row['enabled'] = $row['status'] == self::STATUS_ACTIVE;
	        $rowset[] = $row;
        }
        
        return $rowset;
    }
}