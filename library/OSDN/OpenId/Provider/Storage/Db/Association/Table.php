<?php

/**
 * OpenId database association storage
 *
 * @category	OSDN
 * @package	OSDN_OpenId
 * @author		Yaroslav Zenin <yaroslav.zenin@gmail.com>
 * @version	$Id: Table.php 17442 2010-03-10 13:36:43Z yaroslav $
 *
 */
class OSDN_OpenId_Provider_Storage_Db_Association_Table extends OSDN_Db_Table_Abstract
{
	protected $_name = 'provider_association';
}