<?php

/**
 * OpenId database storage
 *
 * @category	OSDN
 * @package	OSDN_OpenId
 * @author		Yaroslav Zenin <yaroslav.zenin@gmail.com>
 * @version	$Id: Db.php 17421 2010-03-09 14:49:07Z yaroslav $
 */
class OSDN_OpenId_Consumer_Storage_Db extends Zend_OpenId_Consumer_Storage
{
	/**
	 * @var OSDN_OpenId_Consumer_Storage_Db_Assiciation_Table
	 */
	private $_associationTable = null;
	
	/**
	 * @var OSDN_OpenId_Consumer_Storage_Db_Discovery_Table
	 */
	private $_discoveryTable = null;
	
	/**
	 * @var OSDN_OpenId_Consumer_Storage_Db_Nonce_Table
	 */
	private $_nonceTable = null;
	
	/**
     * Stores information about association identified by $url/$handle
     *
     * @param string $url OpenID server URL
     * @param string $handle assiciation handle
     * @param string $macFunc HMAC function (sha1 or sha256)
     * @param string $secret shared secret
     * @param long $expires expiration UNIX time
     * @return void
     */
    public function addAssociation($url, $handle, $macFunc, $secret, $expires)
    {
    	$this->_associationTable()->insert(array(
    		'url'     => $url,
            'handle'  => $handle,
            'macFunc' => $macFunc,
            'secret'  => base64_encode($secret),
            'expires' => $expires,
    	));

    	return true;
    }

    /**
     * Gets information about association identified by $url
     * Returns true if given association found and not expired and false
     * otherwise
     *
     * @param string $url OpenID server URL
     * @param string &$handle assiciation handle
     * @param string &$macFunc HMAC function (sha1 or sha256)
     * @param string &$secret shared secret
     * @param long &$expires expiration UNIX time
     * @return bool
     */
    public function getAssociation($url, &$handle, &$macFunc, &$secret, &$expires)
    {
    	$this->_associationTable()->deleteQuote(array(
			'expires < ?' => time()
    	));

    	$row = $this->_associationTable()->fetchRow(array(
    		'url = ?' => $url
    	));
    	
    	if (is_null($row)) {
    		return false;
    	}
    	
    	$handle  = $row->handle;
        $macFunc = $row->macFunc;
        $secret  = base64_decode($row->secret);
        $expires = $row->expires;
        
        return true;
    }

    /**
     * Gets information about association identified by $handle
     * Returns true if given association found and not expired and false
     * othverwise
     *
     * @param string $handle assiciation handle
     * @param string &$url OpenID server URL
     * @param string &$macFunc HMAC function (sha1 or sha256)
     * @param string &$secret shared secret
     * @param long &$expires expiration UNIX time
     * @return bool
     */
    public function getAssociationByHandle($handle, &$url, &$macFunc, &$secret, &$expires)
    {
		$this->_associationTable()->deleteQuote(array(
			'expires < ?' => time()
    	));
    	
    	$row = $this->_associationTable()->fetchRow(array(
    		'handle = ?' => $handle
    	));
    	
    	if (is_null($row)) {
    		return false;
    	}
    	
    	$handle  = $row->handle;
        $macFunc = $row->macFunc;
        $secret  = base64_decode($row->secret);
        $expires = $row->expires;

        return true;
    }

    /**
     * Deletes association identified by $url
     *
     * @param string $url OpenID server URL
     * @return void
     */
    public function delAssociation($url)
    {
    	$this->_associationTable()->deleteQuote(array(
    		'url = ?'	=> $url
    	));
    	
        return true;
    }

    /**
     * Stores information discovered from identity $id
     *
     * @param string $id identity
     * @param string $realId discovered real identity URL
     * @param string $server discovered OpenID server URL
     * @param float $version discovered OpenID protocol version
     * @param long $expires expiration UNIX time
     * @return void
     */
    public function addDiscoveryInfo($id, $realId, $server, $version, $expires)
    {
    	$this->_discoveryTable()->insert(array(
    		'id'      => $id,
            'realId'  => $realId,
            'server'  => $server,
            'version' => $version,
            'expires' => $expires
    	));
    	
        return true;
    }

    /**
     * Gets information discovered from identity $id
     * Returns true if such information exists and false otherwise
     *
     * @param string $id identity
     * @param string &$realId discovered real identity URL
     * @param string &$server discovered OpenID server URL
     * @param float &$version discovered OpenID protocol version
     * @param long &$expires expiration UNIX time
     * @return bool
     */
    public function getDiscoveryInfo($id, &$realId, &$server, &$version, &$expires)
    {
		$this->_discoveryTable()->deleteQuote(array(
			'expires < ?' => time()
		));
		
		$row = $this->_discoveryTable()->findOne($id);
    	if (is_null($row)) {
    		return false;
    	}
    	
    	$realId  	= $row->realId;
        $server 	= $row->server;
        $version  	= $row->version;
        $expires 	= $row->expires;
        
        return true;
    }

    /**
     * Removes cached information discovered from identity $id
     *
     * @param string $id identity
     * @return bool
     */
    public function delDiscoveryInfo($id)
    {
		$this->_discoveryTable()->deleteByPk($id);
		
        return true;
    }

    /**
     * The function checks the uniqueness of openid.response_nonce
     *
     * @param string $provider openid.openid_op_endpoint field from authentication response
     * @param string $nonce openid.response_nonce field from authentication response
     * @return bool
     */
    public function isUniqueNonce($provider, $nonce)
    {
    	try {
        	$this->_nonceTable()->insert(array('nonce' => $nonce));
        } catch (Zend_Db_Statement_Exception $e) {
            return false;
        }
        
        return true;
    }
    

    /**
     * Removes data from the uniqueness database that is older then given date
     * @todo implement me...
     *
     * @param string $date Date of expired data
     */
    public function purgeNonces($date=null)
    {
    	
    }
	
	/**
	 * Retrieve the association table object
	 *
	 * @return OSDN_OpenId_Consumer_Storage_Db_Association_Table
	 */
	protected function _associationTable()
	{
		if (is_null($this->_associationTable)) {
			$this->_associationTable = new OSDN_OpenId_Consumer_Storage_Db_Association_Table();
		}
		
		return $this->_associationTable;
	}

	/**
	 * Retrieve the discovery table object
	 *
	 * @return OSDN_OpenId_Consumer_Storage_Db_Discovery_Table
	 */
	protected function _discoveryTable()
	{
		if (is_null($this->_discoveryTable)) {
			$this->_discoveryTable = new OSDN_OpenId_Consumer_Storage_Db_Discovery_Table();
		}
		
		return $this->_discoveryTable;
	}
	
	/**
	 * Retrieve the nonce table object
	 *
	 * @return OSDN_OpenId_Consumer_Storage_Db_Nonce_Table
	 */
	protected function _nonceTable()
	{
		if (is_null($this->_nonceTable)) {
			$this->_nonceTable = new OSDN_OpenId_Consumer_Storage_Db_Nonce_Table();
		}
		
		return $this->_nonceTable;
	}
}