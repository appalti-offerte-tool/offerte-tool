CREATE TABLE IF NOT EXISTS `openid_consumer_association` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `url` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `macFunc` char(16) NOT NULL,
  `secret` varchar(255) NOT NULL,
  `expires` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `openid_consumer_discovery` (
  `id` varchar(255) NOT NULL,
  `realId` varchar(255) NOT NULL,
  `server` varchar(255) NOT NULL,
  `version` float default NULL,
  `expires` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `openid_consumer_nonce` (
  `nonce` varchar(255) NOT NULL,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`nonce`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
