<?php

class OSDN_OpenId_Extension_Record_Table extends OSDN_Db_Table_Abstract
{
    protected $_name = 'provider_users_record';
    
    protected $_id = 'record_id';
    
    protected $_nullableFields = array(
        'fullname', 'dob', 'gender',
        'postcode', 'country', 'language', 'timezone'
    );
}