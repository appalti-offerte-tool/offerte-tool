CREATE TABLE IF NOT EXISTS `openid_provider_users_record` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `email` varchar(255) NOT NULL,
  `fullname` varchar(255) default NULL,
  `dob` date default NULL,
  `gender` enum('MALE','FEMALE') default NULL,
  `postcode` varchar(20) default NULL,
  `country` varchar(255) default NULL,
  `language` varchar(255) default NULL,
  `timezone` varchar(100) default NULL,
  UNIQUE KEY `user_id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='This table is using for store users personal detail' AUTO_INCREMENT=1;

ALTER TABLE `openid_provider_users_record`
  ADD CONSTRAINT `openid_provider_users_record_ibfk_1` FOREIGN KEY (`id`) REFERENCES `openid_provider_users` (`id`) ON DELETE CASCADE;
