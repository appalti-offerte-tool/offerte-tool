<?php

/**
 * Record model
 *
 * @category    OSDN
 * @package     OSDN_OpenId_Extension
 * @author      Yaroslav Zenin <yaroslav.zenin@gmail.com>
 * @version     $Id: Model.php 19509 2011-03-30 11:19:11Z yaroslav $
 */
class OSDN_OpenId_Extension_Record_Model
{
    /**
    * @var $_table OSDN_OpenId_Extension_Record_Table
    */
    protected $_table;

    /**
    * Container of errors collection
    *
    * @var array
    */
    protected $_errors = array();

    /**
    * The constructor
    *
    * Initialize table object
    */
    public function __construct()
    {
        $this->_table = new OSDN_OpenId_Extension_Record_Table();
    }

    /**
     * Create new record profile
     *
     * @param $data
     * @return boolean
     */
    public function insert(array $data)
    {
        if (true !== $this->_validate($data)) {
            return false;
        }

        $id = $this->_table->insert($data);
        return $id > 0;
    }

    /**
     * Update existing profile
     *
     * @param $id   The primary key
     * @param $data
     *
     * @return boolean
     */
    public function update($id, array $data = array())
    {
        if (true !== $this->_validate($data)) {
            return false;
        }

        $affected = $this->_table->updateQuote($data, array(
            'id = ?' => $id
        ));

        return false !== $affected;
    }

    /**
     * Fetch existing profile record
     *
     * @param $id       The user id
     * @return array|false
     */
    public function fetch($id)
    {
        $row = $this->_table->fetchRow(array(
            'id = ?'   => $id
        ));
        if (empty($row)) {
            return false;
        }

        return $row->toArray();
    }

    /**
     * Validate record properties
     *
     * @param array $data
     * @return boolean
     */
    protected function _validate($data)
    {
        $f = new OSDN_Filter_Input(array(), array(
            'email'     => array('EmailAddress', 'presence' => 'required'),
            'fullname'  => array('presence' => 'required'),
            'dob'       => array(),
            'sex'       => array(array('InArray', array('MALE', 'FEMALE'))),
            'postcode'  => array(),
            'country'   => array(),
            'language'  => array(array('InArray', array('en', 'nl')))
        ), $data);

        if ($f->isValid()) {
            return true;
        }

        $this->_errors = $f->getMessages();
        return false;
    }

    /**
     * Retrieve the errors collection
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->_errors;
    }
}