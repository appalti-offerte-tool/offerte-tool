<?php

/**
 * OpenId record extension
 *
 * This is more extended version of SReg extension
 * Unfortunately it's not possible to extend SReg extension because
 * static methods using
 * Some docs can be found
 * @link http://openid.net/specs/openid-simple-registration-extension-1_0.html
 *
 * @category	OSDN
 * @package	OSDN_OpenId
 * @author		Yaroslav Zenin <yaroslav.zenin@gmail.com>
 * @version	$Id: Record.php 17529 2010-03-14 18:40:08Z yaroslav $
 */
class OSDN_OpenId_Extension_Record extends Zend_OpenId_Extension
{
	protected $_id = null;
	
	protected $_props = array();
	
	protected static $_properties = array(
	    'id'        => true,
	    'nickname'  => true,
	    'email'     => false,
	    'fullname'  => false,
	    'dob'       => false,
        'gender'    => false,
        'postcode'  => false,
	    'country'   => false,
	    'language'  => false,
	    'timezone'  => false
	);
	
	public function __construct($id = null)
	{
        $this->_id = (int) $id;
	}
	
	public static function getDefinition()
	{
        return self::$_properties;
	}
	
	/**
     * Returns associative array of RECORD variables
     *
     * @return array
     */
    public function getProperties()
    {
        return is_array($this->_props) ? $this->_props : array();
    }
    
	/**
     * Method to add additional data to OpenId 'checkid_immediate' or
     * 'checkid_setup' request. This method addes nothing but inherited class
     * may add additional data into request.
     *
     * @param array &$params request's var/val pairs
     * @return bool
     */
    public function prepareRequest(&$params)
    {
        return true;
    }

    /**
     * Method to parse OpenId 'checkid_immediate' or 'checkid_setup' request
     * and initialize object with passed data. This method parses nothing but
     * inherited class may override this method to do somthing.
     *
     * @param array $params request's var/val pairs
     * @return bool
     */
    public function parseRequest($params)
    {
    	return true;
    }

    /**
     * Method to add additional data to OpenId 'id_res' response. This method
     * addes nothing but inherited class may add additional data into response.
     *
     * @param array &$params response's var/val pairs
     * @return bool
     */
    public function prepareResponse(&$params)
    {
    	foreach (self::$_properties as $prop => $required) {
        	if (!empty($this->_props[$prop])) {
            	$params['openid.profile.' . $prop] = $this->_props[$prop];
            }
        }
        
        return true;
    }

    /**
     * Method to parse OpenId 'id_res' response and initialize object with
     * passed data. This method parses nothing but inherited class may override
     * this method to do somthing.
     *
     * @param array $params response's var/val pairs
     * @return bool
     */
    public function parseResponse($params)
    {
        $props = array();
        foreach (self::$_properties as $prop => $required) {
            if (!empty($params['openid_profile_' . $prop])) {
                $props[$prop] = $params['openid_profile_' . $prop];
            }
        }
        
        if (isset($this->_props) && is_array($this->_props)) {
            foreach (self::$_properties as $prop => $required) {
                if (isset($this->_props[$prop]) &&
                    $this->_props[$prop] &&
                    !isset($props[$prop])) {
                    return false;
                }
            }
        }
        $this->_props = (count($props) > 0) ? $props : null;
    	
        return true;
    }

    /**
     * Method to prepare data to store it in trusted servers database.
     *
     * @param array &$data data to be stored in tusted servers database
     * @return bool
     */
    public function getTrustData(&$data)
    {
    	return true;
    }

    /**
     * Method to check if data from trusted servers database is enough to
     * sutisfy request.
     *
     * @param array $data data from tusted servers database
     * @return bool
     */
    public function checkTrustData($data)
    {
    	$name = get_class();
        $props = isset($data[$name]) ? $data[$name] : array();
        $props2 = array();
        
        foreach(self::$_properties as $p => $required) {
            if (empty($props[$p])) {
                if ($required) {
                    return false;
                }
            } else {
                $props2[$p] = $props[$p];
            }
        }

        $this->_props = count($props2) > 0 ? $props2 : null;
        return true;
    }
}