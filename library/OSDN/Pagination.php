<?php

/**
 * Simple pagination object
 *
 * @category	OSDN
 * @package	OSDN_Pagination
 * @author		Yaroslav Zenin <yaroslav.zenin@gmail.com>
 * @version	$Id: Pagination.php 19196 2010-11-26 09:49:25Z sergey $
 */
class OSDN_Pagination
{
    /**
     * Contain current page
     *
     * @var int
     */
    protected $_page = 1;
    
    /**
     * Contain page total count
     *
     * @var int
     */
    protected $_totalCount = 0;
    
    /**
     * Contain count items per page
     *
     * @var int
     */
    protected $_itemsCountPerPage = 10;
    
    /**
     * @var Zend_Controller_Request_Abstract
     */
    protected $_request;
    
    /**
     * The constructor
     * Initialize current page
     *
     * @return void
     */
    public function __construct()
    {
        $f = Zend_Controller_Front::getInstance();
        $this->_request = $f->getRequest();
        $this->setPage($this->_request->getParam('page'));
        $this->toRequest();

        $router = $f->getRouter();
        if ($router instanceof Zend_Controller_Router_Rewrite) {
            foreach($this->_request->getQuery() as $param => $value) {
                $router->setGlobalParam($param, $value);
            }
        }
    }
    
    /**
     * Retrieve current page
     *
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->_page;
    }
    
    /**
     * Retrieve start position
     *
     * @return int
     */
    public function getStart()
    {
        return ($this->_page - 1) * $this->_itemsCountPerPage;
    }
    
    /**
     * Retrieve offset limit
     *
     * @return int
     */
    public function getLimit()
    {
        return $this->_itemsCountPerPage;
    }
    
    /**
     * Retrieve total count of items
     *
     * @return int
     */
    public function getTotalCount()
    {
        return $this->_totalCount;
    }
    
    /**
     * Retrieve pages count
     *
     * @return int
     */
    public function getPagesCount()
    {
        return ceil($this->_totalCount / $this->_itemsCountPerPage);
    }
    
    /**
     * Set items count per page
     *
     * @param $itemsCount int
     * @return OSDN_Pagination
     */
    public function setItemsCountPerPage($itemsCount)
    {
        $this->_itemsCountPerPage = max((int) $itemsCount, 1);
        $this->toRequest();
        
        return $this;
    }
    
    /**
     * Set current page
     *
     * @param $page int
     * @return OSDN_Pagination
     */
    public function setPage($page)
    {
        $this->_page = max((int) $page, 1);
        return $this;
    }
    
    /**
     * Set total count
     *
     * @param $totalCount int
     * @return OSDN_Pagination
     */
    public function setTotalCount($totalCount)
    {
        $this->_totalCount = max((int) $totalCount, 0);
        return $this;
    }
    
    /**
     * Apply arguments to request object
     *
     * @return OSDN_Pagination
     */
    public function toRequest()
    {
        $this->_request->setParam('start', $this->getStart());
        $this->_request->setParam('limit', $this->getLimit());
        
        return $this;
    }
}