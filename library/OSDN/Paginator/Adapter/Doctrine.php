<?php

use Doctrine\ORM\QueryBuilder;

class OSDN_Paginator_Adapter_Doctrine implements \Zend_Paginator_Adapter_Interface
{
    /**
     * Name of the row count column
     *
     * @var string
     */
    const ROW_COUNT_COLUMN = 'zend_paginator_row_count';

    /**
     * Database query
     *
     * @var QueryBuilder
     */
    protected $_query = null;

    /**
     * Total item count
     *
     * @var integer
     */
    protected $_rowCount = null;

    /**
     * Constructor.
     *
     * @param QueryBuilder $query The select query
     */
    public function __construct(QueryBuilder $query)
    {
        $this->_query = $query;
    }

    /**
     * Sets the total row count, either directly or through a supplied query
     *
     * @param  QueryBuilder|integer $totalRowCount Total row count integer
     *                                               or query
     * @return OSDN_Paginator_Adapter_Doctrine $this
     * @throws Zend_Paginator_Exception
     */
    public function setRowCount($rowCount)
    {
        if ($rowCount instanceof QueryBuilder) {
            $sql = $rowCount->getSql();
            
            if (false === strpos($sql, self::ROW_COUNT_COLUMN)) {
                throw new \Zend_Paginator_Exception('Row count column not found');
            }
            
            $result = $rowCount->fetchOne()->toArray();
            
            $this->_rowCount = count($result) > 0 ? $result[self::ROW_COUNT_COLUMN] : 0;
        } else if (is_integer($rowCount)) {
            $this->_rowCount = $rowCount;
        } else {
            throw new \Zend_Paginator_Exception('Invalid row count');
        }

        return $this;
    }

    /**
     * Returns an array of items for a page.
     *
     * @param  integer $offset Page offset
     * @param  integer $itemCountPerPage Number of items per page
     * @return array
     */
    public function getItems($offset, $itemCountPerPage)
    {
        $this->_query
            ->setFirstResult($offset)
            ->setMaxResults($itemCountPerPage);
        
            $result = $this->_query->getQuery()->getResult();
        return $result;
    }

    /**
     * Returns the total number of rows in the result set.
     *
     * @return integer
     */
    public function count()
    {
        $query = clone $this->_query;

        if ($this->_rowCount === null) {
            
            $query->setFirstResult(null);
            $query->setMaxResults(null);
            $query->select($query->expr()->count($query->getRootAlias()));
            $query->resetDQLPart('orderBy');
            
            $rowCount = (int) $query->getQuery()->getSingleScalarResult();

            $this->setRowCount($rowCount);
        }

        return $this->_rowCount;
    }
}