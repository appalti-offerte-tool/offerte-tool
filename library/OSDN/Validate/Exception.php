<?php

/**
 * @category OSDN
 * @package OSDN_Validate
 * @version $Id: Exception.php 19130 2010-11-10 15:54:39Z yaroslav $
 */
class OSDN_Validate_Exception extends OSDN_Exception
{
    /**
     * (non-PHPdoc)
     * @see library/OSDN/OSDN_Exception::assign()
     */
    public function assign(array $messages, $field = null, $e = E_USER_ERROR)
    {
        $output = array();
        foreach($messages as $field => $msgs) {
            foreach($msgs as $m) {
                $output[] = $this->addMessage($m, array(), $field);
            }
        }
        
        return $output;
    }
}