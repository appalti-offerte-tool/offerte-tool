<?php

/**
 * The basic zipcode validator
 *
 * @category OSDN
 * @package OSDN_Validate
 * @version $Id: Zipcode.php 13516 2009-10-26 09:58:48Z bvh $
 */
class OSDN_Validate_Zipcode extends Zend_Validate_Abstract
{
    const INCORRECT = 'notCorrectZipcodeId';

    /**
     * Contain error messages
     * @var array
     */
    protected $_messageTemplates = array(
        self::INCORRECT => "'%value%' does not appear to be a zipcode"
    );

    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if $value is a valid secure id
     *
     * @param  string|int $value
     * @return boolean
     */
    public function isValid($value)
    {
        $result = (boolean) preg_match('/^[\d]{4,10}\s?[a-z]{0,5}$/i', $value);

        if (true !== $result) {
            $this->_error(self::INCORRECT, $value);
        }

        return $result;
    }
}
