<?php

/**
 * The basic phonenumber validator
 *
 * @category OSDN
 * @package OSDN_Validate
 */
class OSDN_Validate_Mobile extends Zend_Validate_Abstract
{

    const INCORRECT = 'notCorrectMobileNumber';
    
    /**
     * Contain error messages
     * @var array
     */
    protected $_messageTemplates = array(
        self::INCORRECT => "'%value%' does not appear to be a mobile number. Use example: `000-0000000`"
    );
    
    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if $value is a valid secure id
     *
     * @param  string|int $value
     * @return boolean
     */
    public function isValid($value)
    {
        $regex = new Zend_Validate_Regex('/^\+?\d{3}-\d{7}$/');
        if (!$regex->isValid($value)) {
            $this->_error(null, $value);
            return false;
        }

        return true;
    }
}
