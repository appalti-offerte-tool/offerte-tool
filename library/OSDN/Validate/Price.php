<?php

/**
 * The basic price validator
 *
 * @category OSDN
 * @package OSDN_Validate
 */
class OSDN_Validate_Price extends Zend_Validate_Abstract
{

    const INCORRECT = 'notCorrectPrice';
    
    const LESS_THAN_ZERO = 'lessThanZero';
    
    /**
     * Contain error messages
     * @var array
     */
    protected $_messageTemplates = array(
        self::INCORRECT => "'%value%' does not appear to be an valid price",
        self::LESS_THAN_ZERO => "'%value%' does not appear to be an less than '0'",
    );
    
    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if $value is a valid secure id
     *
     * @param  string $value
     * @return boolean
     */
    public function isValid($value)
    {
        $float = new Zend_Validate_Float(OSDN_Language::getDefaultLocale());
        if (!$float->isValid($value)) {
            $this->_error(self::INCORRECT, $value);
            return false;
        }
        
        if ($value < 0) {
            $this->_error(self::LESS_THAN_ZERO, $value);
            return false;
        }
        return true;
    }
}
