<?php

/**
 * Password confirmation validator
 *
 * @category    OSDN
 * @package     OSDN_Validate
 * @author      Yaroslav Zenin <yaroslav.zenin@gmail.com>
 * @version     $Id: PasswordConfirmation.php 19792 2011-09-26 14:10:00Z yaroslav $
 */
class OSDN_Validate_PasswordConfirmation extends Zend_Validate_Abstract
{
    const NOT_MATCH = 'notMatch';

    protected $_messageTemplates = array(
        self::NOT_MATCH => 'Password confirmation does not match'
    );

    protected $_context;

    protected $_contextKey;

    public function __construct($context = null, $key = 'passwordConfirm')
    {
        if (!is_null($context)) {
            $this->_context = $context;
        }

        $this->_contextKey = $key;
    }

    public function isValid($value, $context = null)
    {
        $value = (string) $value;
        $this->_setValue($value);

        if (is_null($context)) {
            $context = $this->_context;
        }

        if (is_array($context)) {
            if (isset($context[$this->_contextKey]) && $value == $context[$this->_contextKey]) {
                return true;
            }
        } elseif (is_string($context) && $value == $context) {
            return true;
        }

        $this->_error(self::NOT_MATCH);
        return false;
    }
}