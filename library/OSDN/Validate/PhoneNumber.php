<?php

/**
 * The basic phonenumber validator
 *
 * @category    OSDN
 * @package     OSDN_Validate
 * @validate     $Id: PhoneNumber.php 13516 2009-10-26 09:58:48Z bvh $
 */
class OSDN_Validate_PhoneNumber extends Zend_Validate_Abstract
{

    const INCORRECT = 'notCorrectPhoneNumberId';

    /**
     * Contain error messages
     * @var array
     */
    protected $_messageTemplates = array(
        self::INCORRECT => "'%value%' does not appear to be a phone number"
    );

    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if $value is a valid secure id
     *
     * @param  string|int $value
     * @return boolean
     */
    public function isValid($value)
    {
        $result = (boolean) preg_match('/^\+?[0-9-\s]{5,15}$/', $value);
        if (true !== $result) {
            $this->_error(self::INCORRECT, $value);
        }

        return $result;
    }
}
