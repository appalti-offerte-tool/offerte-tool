<?php

/**
 * OSDN Soap exception
 * 
 * @category		OSDN
 * @package		OSDN_Soap
 * @version		$Id: Exception.php 5516 2008-11-18 15:36:49Z flash $
 */
class OSDN_Soap_Exception extends OSDN_Exception 
{}