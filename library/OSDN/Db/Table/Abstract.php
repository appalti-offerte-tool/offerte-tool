<?php
/**
 * Class for SQL table interface
 *
 * @category        OSDN
 * @package         OSDN_Db
 * @subpackage      OSDN_Db_Table
 * @version         $Id: Abstract.php 20366 2012-02-09 09:12:48Z yaroslav $
 */
abstract class OSDN_Db_Table_Abstract extends \Zend_Db_Table_Abstract
{
    const OMIT_FETCH_ALL  = 1;
    const OMIT_FETCH_ROW  = 2;
    const OMIT_ALWAYS     = 3;    // OMIT_FETCH_ALL | OMIT_FETCH_ROW

    /**
     * Define the logic for new values in the primary key.
     * May be a string, boolean true, or boolean false.
     *
     * @var mixed
     */
    protected $_sequence = null;

    /**
     * The table prefix
     *
     * @var string
     */
    protected $_prefix = "";

    /**
     * Clear primary key on insert operation
     *
     * @var boolean
     */
    protected $_clearPkOnInsert = true;

    /**
     * Clear primary key on update operation
     *
     * @var boolean
     */
    protected $_clearPkOnUpdate = true;

    /**
     * Contain nullable fields
     * which will be removed on insert or set "NULL" in SQL query
     *
     * @var array
     */
    protected $_nullableFields = array();

    /**
     * The default table prefix using for each OSDN_Db_Table_Abstract object
     *
     * @var string
     */
    protected static $_defaultPrefix = "";

    /**
     * The default table sequense
     *
     * @var mixed
     */
    protected static $_defaultSequence = true;

    /**
     *
     * @var array
     */
    protected $_codes = array();

    protected $_onErrorCallbackFn = null;

    /**
     * List of columns which will be serialized in database
     *
     * @var array
     */
    protected $_serializableColumns = array();

    /**
     * Omit columns
     * List of value/pairs columns which will be omited from rowset while fetching
     *
     * <code>
     *  // Simple example
     *  array(
     *      password => 1,      // always omited
     *      state    => 2       // omited only while fetching collection of rows
     *      content  => 3       // omited when while fetching single row
     *  )
     * </code>
     * @var array
     */
    protected $_omitColumns = array();

    /**
     * Constructor
     *
     * @see Zend_Db_Table_Abstract::__construct() about params configuration
     *
     * @param array $config
     */
    public function __construct($config = array())
    {
        if (empty($this->_prefix) && !empty(self::$_defaultPrefix)) {
            $this->_prefix = self::getDefaultPrefix();
        }

        if (is_null($this->_sequence) && !is_null(self::getDefaultSequence())) {
            $this->_sequence = self::getDefaultSequence();
        }

        parent::__construct($config);
        $this->_init();
    }

    protected function _init()
    {}

    /**
     * Initialize table name with prefix if last is not empty
     *
     */
    protected function _setupTableName()
    {
        parent::_setupTableName();
        if (!empty($this->_prefix)) {
            $this->_name = (string) $this->_prefix . $this->_name;
        }
    }

    /**
     * Retrieve the table prefix
     *
     * @return string
     */
    public function getPrefix()
    {
        return $this->_prefix;
    }

    /**
     * Set the default prefix for all OSDN_Db_Table objects
     *
     * @param string $prefix
     * @return void
     */
    public static function setDefaultPrefix($prefix)
    {
        self::$_defaultPrefix = $prefix;
    }

    /**
     * Retrieve default table prefix
     *
     * @return string
     */
    public static function getDefaultPrefix()
    {
        return self::$_defaultPrefix;
    }

    /**
     * Set the default sequense for all OSDN_Db_Table objects
     * Sequense will be used only in insert operations
     * when response return from db object
     *
     * true allow use the standart db id
     * md5 for use char 32 length
     *
     * @param mixed $sequense
     */
    public static function setDefaultSequence($sequence)
    {
        self::$_defaultSequence = $sequence;
    }

    /**
     * Retrieve the default sequense
     *
     * @return string|bool
     */
    public static function getDefaultSequence()
    {
        return self::$_defaultSequence;
    }

    /**
     * Insert data into table
     *
     * All fields with values equals to null
     * or not present in column structure will be unset
     * If primary key is present in data and pk is autoincrement then drip pk
     *
     * @param array $data
     * @return  int|false         SQL last inserted id
     */
    public function insert(array $data)
    {
        foreach ($data as $key => $value) {
            if (
                is_null($value) ||
                !$this->hasColumn($key)

                /**
                 * @FIXME
                 */
//                 ||
//                (isset($this->_omitColumns[$key]) && self::OMIT_ALWAYS & $this->_omitColumns[$key])
            ) {
                unset($data[$key]);
                continue;
            }

            if (in_array($key, $this->_nullableFields) && empty($data[$key])) {
                unset($data[$key]);
                continue;
            }

            // if primary key then not needed and the sequense is not user defined unset it
            if (
                true === $this->_clearPkOnInsert &&
                isset($this->_metadata[$key]['PRIMARY']) && true === $this->_metadata[$key]['PRIMARY'] &&
                isset($this->_metadata[$key]['IDENTITY']) && true === $this->_metadata[$key]['IDENTITY'] &&
                !is_string($this->_sequence)
            ) {
                unset($data[$key]);
                continue;
            }

            if (in_array($key, $this->_serializableColumns)) {
                $value = serialize($value);
            }
        }

        try {
            return parent::insert($data);
        } catch (Exception $e) {
            $this->_catch($e, __METHOD__, $data);
            return false;
        }
    }

    /**
     * Allow multiple insert statement
     *
     * @param array $data
     * @return int
     */
    public function insertMultiple(array $data)
    {
         $collection = array();
         $columns = array();
         $bind = array();

         // Extract and quote col names from the array keys
         // of the first row
         $first = current($data);
         $columns = array();
         foreach (array_keys($first) as $column) {
             $columns[] = $this->getAdapter()->quoteIdentifier($column, true);
         }

         // Loop through data to extract values for binding
         foreach ($data as $rowdata) {
             if (count($rowdata) != count($columns)) {
                 /** @see Zend_Db_Adapter_Exception */
                 require_once 'Zend/Db/Adapter/Exception.php';
                 throw new Zend_Db_Adapter_Exception('Each row must'
                     . ' have the same number of columns.');
             }

             $values = array();

             foreach ($rowdata as $key => $value) {
                 if ($value instanceof Zend_Db_Expr) {
                     $values[] = $value->__toString();
                 } else {
                     $values[] = '?';
                     $bind[] = $value;
                 }
             }

             $collection[] = '(' . implode(', ', $values) . ')';
         }

         // Build the insert statement
         $sql = "INSERT INTO " . $this->getTableName() . "\n(" . join(', ', $columns)
              . ")\nVALUES\n" . implode(', ', $collection);

         // Execute the statement and return the number of affected rows
         $stmt = $this->getAdapter()->query($sql, $bind);
         $result = $stmt->rowCount();

         return $result;
     }

    /**
     * Update existings rows
     *
     * @param array $data         Columns-value pairs
     * @param string|array $where An SQL WHERE clause, or an array of SQL WHERE clauses.
     * @return      int           Number of rows updated
     *               false         If some exceptions throws
     */
    public function update(array $data, $where)
    {
        foreach ($data as $key => $value) {
            if (
                !$this->hasColumn($key) ||
                (
                    true === $this->_clearPkOnUpdate &&
                    isset($this->_metadata[$key]['PRIMARY']) && true === $this->_metadata[$key]['PRIMARY'] &&
                    isset($this->_metadata[$key]['PRIMARY']) && true === $this->_metadata[$key]['IDENTITY']
                )

                /**
                 * @FIXME
                 */
//                 ||
//                (isset($this->_omitColumns[$key]) && (self::OMIT_ALWAYS & $this->_omitColumns[$key]))

            ) {
                unset($data[$key]);
                continue;
            }

            if (in_array($key, $this->_nullableFields) && empty($data[$key])) {
                $data[$key] = new Zend_Db_Expr('NULL');
                continue;
            }

            if (in_array($key, $this->_serializableColumns)) {
                $value = serialize($value);
            }
        }

        try {
            return parent::update($data, $where);
        } catch (Exception $e) {
            $this->_catch($e, __METHOD__, $data);
            return false;
        }
    }

    /**
     * Deletes existing rows.
     *
     * @param  array|string $where SQL WHERE clause(s).
     * @return int          The number of rows deleted.
     */
    public function delete($where)
    {
        try {
            return parent::delete($where);
        } catch (Exception $e) {
            $this->_catch($e, __METHOD__, array());
            return false;
        }
    }

    /**
     * Update existing rows
     *
     * @param array $data       Columns-value pairs
     * @param array $clause     An array of SQL WHERE clauses
     *
     * <pre>For example: </pre><code>
     *    $data = array('datafield' => 1);
     *    $clause = array('clausefield = ?' => 2);
     *    $this->updateQuote($data, $clause);
     * </code>
     *
     * @see update()
     */
    public function updateQuote(array $data, array $clause)
    {
        $preparedClause = array();
        foreach ($clause as $cond => $value) {
            if (is_int($cond)) {
                $preparedClause[] = $value;
                continue;
            }
            $preparedClause[] = $this->_db->quoteInto($cond, $value);
        }
        return $this->update($data, $preparedClause);
    }

    /**
     * Delete existing rows
     *
     * @param array $clause    An array of SQL WHERE clauses
     *
     * <pre>For example: </pre><code>
     *      $this->deleteQuote(array(
     *          'testfield = ?' => 1
     *      ));
     * </code>
     *
     * @return affectedRows | false if error
     */
    public function deleteQuote(array $clause)
    {
        $preparedClause = array();
        foreach ($clause as $cond => $value) {
            $preparedClause[] = $this->_db->quoteInto($cond, $value);
        }

        return $this->delete($preparedClause);
    }

    /**
     * Delete records by primary key
     *
     * @param mixed $pkValue    The id | array of primary keys
     * @return int number of row deleted
     */
    public function deleteByPk($pkValue)
    {
        $this->_setupPrimaryKey();
        $keyNames = array_values((array) $this->_primary);
        if (count($keyNames) == 0) {
            throw new OSDN_Db_Exception('Primary key is not defined');
        }

        if (count($keyNames) > 1) {
            throw new OSDN_Db_Exception('Too many primary key are defined');
        }

        reset($keyNames);
        $key = current($keyNames);

        $collection = $this->_preparePkCollection($pkValue);
        if (0 === count($collection)) {
            return 0;
        }

        $clause = $this->_db->quoteInto($this->_db->quoteIdentifier($key) . ' IN(?)', $collection);
        return $this->delete($clause);
    }

    /**
     * Update records by primary key
     *
     * @param array $data  Column-value pairs.
     * <code>array(
     *      'count'  => 1,
     *      'date'   => new Zend_Db_Expr('NOW()')
     * )</code>
     * @param mixed $pkValue
     * $return int  number of affected rows
     */
    public function updateByPk(array $data, $pkValue)
    {
        $this->_setupPrimaryKey();
        $keyNames = array_values((array) $this->_primary);
        if (count($keyNames) == 0) {
            throw new OSDN_Db_Exception('Primary key is not defined');
        }

        if (count($keyNames) > 1) {
            throw new OSDN_Db_Exception('Too many primary key are defined');
        }

        reset($keyNames);
        $key = current($keyNames);

        $collection = $this->_preparePkCollection($pkValue);
        if (0 === count($collection)) {
            return 0;
        }

        $clause = $this->_db->quoteInto($this->_db->quoteIdentifier($key) . ' IN(?)', $collection);
        return $this->update($data, $clause);
    }

    /**
     * Prepare the collection values
     * @param mixed $pkValue
     * @return array        The collection of primary keys
     */
    private function _preparePkCollection($pkValue)
    {
        if (true === $this->_sequence) {
            $validate = new OSDN_Validate_Id();
        } else {
            $validate = new OSDN_Validate_SecureId();
        }

        if (!is_array($pkValue)) {
            $pkValue = (array) $pkValue;
        }

        $collection = array();
        foreach ($pkValue as $v) {
            if ($validate->isValid($v)) {
                array_push($collection, $v);
            }
        }
        return $collection;
    }

    /**
     * Return one row by primary key
     *
     * @param int $primaryId
     * @return Zend_Db_Table_Row | null if empty result
     *
     * @throws Zend_Db_Exception
     */
    public function findOne($primaryId)
    {
        $this->_setupPrimaryKey();

        $keyNames = array_values((array) $this->_primary);
        if (count($keyNames) == 0) {
            throw new OSDN_Db_Exception('Primary key is not defined');
        }

        if (count($keyNames) > 1) {
            throw new OSDN_Db_Exception('Too many primary key are defined');
        }

        reset($keyNames);
        $key = current($keyNames);

        $clause = $this->_db->quoteInto($this->_db->quoteIdentifier($key) . ' = ?', $primaryId, Zend_Db::INT_TYPE);
        return $this->fetchRow($clause);
    }

    /**
     * Retrieve "any" column by primary key
     *
     * Here we using the db query for retrieveing
     * because sometimes we wants to add in _fetch method
     * some omit columns
     *
     * @return string|false if column is not found in row model definition
     */
    public function findColumn($primaryId, $field)
    {
        if (!$this->hasColumn($field)) {
            return false;
        }

        $select = $this->_db->select()
            ->from($this->getTableName(), $field)
            ->where('id = ?', $primaryId);

        $value = $select->query()->fetchColumn();
        if (in_array($field, $this->_serializableColumns)) {
            $value = @unserialize($value);
        }

        return $value;
    }

    /**
     * Retrieve the table name
     *
     * @return String
     */
    public function getTableName()
    {
        return $this->_name;
    }

    public function getTableNameWithSchema()
    {
        return empty($this->_schema) ? $this->getTableName() : $this->_schema . '.' . $this->getTableName();
    }

    /**
     * Check if column is present in table metadata
     *
     * @param string $name
     * @return boolean
     */
    public function hasColumn($name)
    {
        return in_array($name, $this->_getCols());
    }

    public function getAllowedColumns($omit = null)
    {
        $cols = $this->_getCols();
        if (empty($this->_omitColumns)) {
            return $cols;
        }

        if (null === $omit) {
            $omit = self::OMIT_FETCH_ALL;
        }

        $omitColumns = array_filter($this->_omitColumns, function($o) use ($omit) {
            return $omit & $o;
        });

        return array_diff($cols, array_keys($omitColumns));
    }

    /**
     * Build the order query
     *
     * @param string|array $field   table field
     * @param string $orderKeys     ASC|DESC
     * @return array
     */
    public function createOrder($field, $dir = null)
    {
        if (!is_array($field)) {
            $field = array($field => $dir);
        }
        $orderCollection = array();
        foreach ($field as $fieldName => $key) {
            if (!empty($fieldName) && $this->hasColumn($fieldName)) {
                $key = strtoupper($key);
                if (!in_array($key, array('ASC', 'DESC'))) {
                    $key = 'ASC';
                }
                $orderCollection[] = $fieldName . ' ' . $key;
            }
        }
        return $orderCollection;
    }

    /**
     * Retrieve the count rows from table by some where condition.
     * If condition are empty then return count of all rows
     *
     * <pre>For example: </pre><code>
     *      $this->count(array(
     *          'testfield = ?' => 1,
     *          'anotherfield = ?' => new Zend_Db_Expr('NOW()'))
     *      ));
     *      $this->count($this->getAdapter()->quoteInto('testfield = ?', 1));
     * </code>
     *
     * @param string|array  $where
     * @param int           $limit
     * @return int
     * @throws Zend_Db_Exception
     */
    public function count($where = null, $limit = null)
    {
        $select = $this->select()->from($this, array('count' => new Zend_Db_Expr('COUNT(*)')));
        if (null !== $where) {
            $this->_where($select, $where);
        }

        if (!is_null($limit)) {
            $select->limit($limit);
        }

        try {
            return (int) $select->query()->fetchColumn();
        } catch (Exception $e) {
            $this->_catch($e, __METHOD__, array());
            return false;
        }
    }

    public function fetchAllColumns($where = null, $order = null, array $columns)
    {
        if (!($where instanceof Zend_Db_Table_Select)) {
            $select = $this->select(self::SELECT_WITHOUT_FROM_PART);

            $select->reset(OSDN_Db_Select::FROM);

            if ($where !== null) {
                $this->_where($select, $where);
            }

            if ($order !== null) {
                $this->_order($select, $order);
            }
        } else {
            $select = $where;
        }

        $select->from($this->getTableName(), array());
        $select->columns($columns);

        return $this->fetchAll($select);
    }

    /**
     * Clone the row by pkId and return pkId of new row
     *
     * @param int $pkId
     * @param array $exclude
     * @return  int|false         SQL last inserted id
     */
    public function cloneRow($pkId, array $exclude = array())
    {
        $this->_setupPrimaryKey();

        $primary = (array) $this->_primary;

        /**
         * @todo     Is it correct to change identity to int?
         *            Because when sequense is not autoincrement it will work uncorrect!!!
         */
        $pkIdentity = $primary[(int) $this->_identity];

        $row = $this->findOne($pkId)->toArray();
        if(isset($row[$pkIdentity])){
            unset($row[$pkIdentity]);
        }

        foreach ($exclude as $k => $v){
            if(isset($row[$v])){
                unset($row[$v]);
            }
        }

        return $this->insert($row);
    }

    /**
     * (non-PHPdoc)
     * @see Zend_Db_Table_Abstract::_fetch()
     */
    protected function _fetch(Zend_Db_Table_Select $select)
    {
        $stmt = $this->_db->query($select);

        $mode = self::OMIT_FETCH_ALL;
        if (1 == $select->getPart(Zend_Db_Table_Select::LIMIT_COUNT)) {
            $mode = self::OMIT_FETCH_ROW;
        }

        $data = array();
        while ($row = $stmt->fetch(Zend_Db::FETCH_ASSOC)) {
            $data[] = $this->filterRow($row, $mode);
        }

        return $data;
    }

    /**
     * Filter unallowed fields from row
     *
     * @param array $row        The fetched row
     * @return array
     */
    public function filterRow(array $row, $mode = self::OMIT_ALWAYS)
    {
        foreach($row as $field => & $value) {

            if (
                array_key_exists($field, $this->_omitColumns) &&
                $this->_omitColumns[$field] & $mode
            ) {
                unset($row[$field]);
                continue;
            }

            if (in_array($field, $this->_serializableColumns)) {
                $value = @ unserialize($value);
            }
        }

        return $row;
    }

    protected function _catch(Exception $e, $method, $args)
    {
        $codeConnection = $e->getCode();
        $code = false;
        if (false !== ($hasErrorInterface = $this->_db instanceof OSDN_Db_Errors_Interface)) {
            $codeConnection = $this->_db->toCodeConnection($codeConnection, $e);
            $code = $this->_db->toCodeAlias($codeConnection, $e);
        }

        $message = false;
        do {

            if (false !== $code && array_key_exists($code, $this->_codes)) {
                $message = $this->_codes[$code];
                break;
            }

            if (empty($message) && !empty($this->_onErrorCallbackFn)) {
                $message = $this->_onErrorCallbackFn;
                break;
            }

            if ($hasErrorInterface) {
                $message = $this->_db->toExplanation($codeConnection);
            }

        } while(false);

        if (empty($message)) {
            $message = '';
            $code = $e->getCode();
            if (!empty($code)) {
                $message = '#' . $code;
            } else {
                $message = 'Unknown';
            }

            $message .= ' database error occured : ' . $e->getMessage();
        }

        if (is_string($message)) {
            throw new \OSDN_Db_Exception($message, $code, $e);
        } elseif (is_callable($message)) {
            $m = call_user_func_array($message, array($e, $method, $args));

            $ee = new \OSDN_Db_Exception('', $code, $e);
            $ee->assign(array($m));
            throw $ee;
        } else {
            throw new \OSDN_Exception($e);
        }

        throw new \OSDN_Db_Exception('Unknown database error occured.', null, $e);
    }

    public function setOnErrorCallbackFn($callback, $code = null)
    {
        if (null !== $code) {
            $this->_codes[$code] = $callback;
            return $this;
        }

        if (is_callable($callback)) {
            $this->_onErrorCallbackFn = $callback;
            return $this;
        }

        if (is_string($callback)) {

            $this->_onErrorCallbackFn = function() use ($callback) {
                return $callback;
            };

            return $this;
        }

        throw new OSDN_Exception(sprintf('Unable to set callback of type "%s"', gettype($callback)));
    }
}