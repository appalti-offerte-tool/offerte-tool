<?php

class OSDN_Db_Errors implements OSDN_Db_Errors_Interface
{
    protected static $_codes = array(
        self::E_DUPLICATE_KEY   => 'Duplicate key voilation'
    );
    
    public static function toExplanationStatic($code)
    {
        if (array_key_exists($code, self::$_codes)) {
            return self::$_codes[$code];
        }
        
        return false;
    }
    
    public function toCodeConnection($code, Exception $e = null)
    {
        return $code;
    }
    
    public function toCodeAlias($nativeCode, Exception $e = null)
    {
        return null;
    }
    
    public function toExplanation($code)
    {
        return self::toExplanationStatic($code);
    }
}