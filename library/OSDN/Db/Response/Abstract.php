<?php

abstract class OSDN_Db_Response_Abstract implements Countable, IteratorAggregate
{
    /**
     * Contain row count
     *
     * @var int
     */
    protected $_rowCount;

    /**
     * The collection of Closure which will be applied
     * to the output rowset
     *
     * @var array of Closure
     */
    protected $_rowsetCallbackFns = array();

    /**
     * The instance of stdClass
     *
     * @var stdClass
     */
    protected $_scope = null;

    /**
     * (non-PHPdoc)
     * @see IteratorAggregate::getIterator()
     */
    public function getIterator()
    {
        return $this->getRowset();
    }

    abstract public function getRowset(Closure $callbackFn = null);

    protected function _toRowset($rowset, Closure $callbackFn = null)
    {
        if (null !== $callbackFn) {
            $this->addRowsetCallback($callbackFn);
        }

        if (count($this->_rowsetCallbackFns) > 0) {
            foreach($this->_rowsetCallbackFns as $callbackFn) {
                $output = array();
                foreach($rowset as $row) {
                    if (false !== ($result = call_user_func_array(
                        $callbackFn, array($row, & $output, $this->_scope))
                    )) {
                        $output[] = $result;
                    }
                }
                $rowset = $output;
            }
        }

        return $rowset;
    }

    public function addRowsetCallback(Closure $callbackFn)
    {
        $this->_rowsetCallbackFns[] = $callbackFn;
        return $this;
    }

    public function setRowsetCallback(Closure $callbackFn)
    {
        $this->_rowsetCallbackFns = array($callbackFn);
        return $this;
    }

    public function setRowCount($count)
    {
        $this->_rowCount = (int) $count;
        return $this;
    }

    public function setScope($scope)
    {
        $this->_scope = $scope;
        return $this;
    }

    public function toArray()
    {
        return array(
            'rowset'    => $this->getRowset(),
            'total'     => $this->count()
        );
    }
}