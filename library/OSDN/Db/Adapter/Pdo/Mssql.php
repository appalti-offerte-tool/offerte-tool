<?php

class OSDN_Db_Adapter_Pdo_Mssql extends Zend_Db_Adapter_Pdo_Mssql implements OSDN_Db_Errors_Interface
{
    protected $_codes = array(
        OSDN_Db_Errors_Interface::E_DUPLICATE_KEY   => 2627
    );

    public function toCodeConnection($code, Exception $e = null)
    {
        if (null !== $e and empty($code)) {

            $ee = $e;

            while(
                null != $ee->getPrevious()
                && 'PDOException' != get_class($ee)
            ) {
                $ee = $ee->getPrevious();
            }
            
            if (isset($ee->errorInfo)) {
                $code = $ee->errorInfo[1];
            }

            if (empty($code)) {
                $message = $e->getMessage();
                
                if (preg_match('/^SQLSTATE\[(\w+)\]:.*?\[(\d+)\].*$/', $message, $matches)) {
                    $code = (int) $matches[2];
                }
            }
        }
        
        return $code;
    }
    
    public function toCodeAlias($codeConnection, Exception $e = null)
    {
        if (empty($codeConnection)) {
            $codeConnection = $this->toCodeConnection($codeConnection, $e);
        }

        return array_search($codeConnection, $this->_codes) ?: -1;
    }
    
    public function toExplanation($codeConnection)
    {
        $message = OSDN_Db_Errors::toExplanationStatic($codeConnection);
        return $message;
    }
}