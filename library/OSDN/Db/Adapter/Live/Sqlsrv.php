<?php

final class OSDN_Db_Adapter_Live_Sqlsrv extends OSDN_Db_Adapter_Sqlsrv
{
    public function __construct($config, \PDO $connection)
    {
        parent::__construct($config);

        $this->_connection = $connection;
    }

    protected function _connect()
    {
        if ($this->_connection) {
            return;
        }

        throw new OSDN_Db_Exception('Connect forbidden');
    }
}