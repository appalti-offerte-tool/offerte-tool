<?php

class OSDN_Db_Adapter_Live_Pdo_Mssql extends Zend_Db_Adapter_Pdo_Mssql
{
    protected $_commitStack = array();

    public function __construct($config, \PDO $connection)
    {
        parent::__construct($config);

        $this->_connection = $connection;
    }

    protected function _connect()
    {
        if ($this->_connection) {
            return;
        }

        throw new OSDN_Db_Exception('Connect forbidden');
    }

    public function beginTransaction()
    {
        $count = count($this->_commitStack);
        array_push($this->_commitStack, 'ZFsavePoint'.$count);
        if($count == 0)
        {
            parent::beginTransaction();
        } else {
            $query = 'SAVE TRANSACTION ZFsavePoint'.$count;
            $q = $this->_profiler->queryStart($query, Zend_Db_Profiler::TRANSACTION);
            $this->query($query);
            $this->_profiler->queryEnd($q);
        }

        return $this;
    }

    protected function _beginTransaction()
    {
        $this->_connect();
        $this->getConnection()->beginTransaction();
        return true;
    }

    public function commit()
    {
        if(count($this->_commitStack) == 1) {
            parent::commit();
        }

        array_pop($this->_commitStack);
        return $this;
    }

    protected function _commit()
    {
        $this->_connect();
        $this->getConnection()->commit();
        return true;
    }

    public function rollback()
    {
        $lastTransaction = array_pop($this->_commitStack);
        if(count($this->_commitStack == 0)) {
            parent::rollback();
        } else {
            $query = 'ROLLBACK TRANSACTION '.$lastTransaction;
            $q = $this->_profiler->queryStart($query, Zend_Db_Profiler::TRANSACTION);
            $this->query($query);
            $this->_profiler->queryEnd($q);
        }

        return $this;
    }

    protected function _rollBack() {
        $this->_connect();
        $this->getConnection()->rollBack();
        return true;
    }

    /**
     * Adds an adapter-specific LIMIT clause to the SELECT statement.
     *
     * @param string $sql
     * @param integer $count
     * @param integer $offset OPTIONAL
     * @return string
     * @throws Zend_Db_Adapter_Sqlsrv_Exception
     */
     public function limit($sql, $count, $offset = 0)
     {
        $count = intval($count);
        if ($count <= 0) {
            require_once 'Zend/Db/Adapter/Exception.php';
            throw new Zend_Db_Adapter_Exception("LIMIT argument count=$count is not valid");
        }

        $offset = intval($offset);
        if ($offset < 0) {
            /** @see Zend_Db_Adapter_Exception */
            require_once 'Zend/Db/Adapter/Exception.php';
            throw new Zend_Db_Adapter_Exception("LIMIT argument offset=$offset is not valid");
        }

        if ($offset == 0) {
            $sql = preg_replace('/^SELECT\s/i', 'SELECT TOP ' . $count . ' ', $sql);
        } else {
            $orderby = stristr($sql, 'ORDER BY');

            if (!$orderby) {
                $over = 'ORDER BY (SELECT 0)';
            } else {
                $over = preg_replace('/\"[^,]*\".\"([^,]*)\"/i', '"inner_tbl"."$1"', $orderby);
            }

            // Remove ORDER BY clause from $sql
            $sql = preg_replace('/\s+ORDER BY(.*)/', '', $sql);

            // Add ORDER BY clause as an argument for ROW_NUMBER()
            $sql = "SELECT ROW_NUMBER() OVER ($over) AS \"ZEND_DB_ROWNUM\", * FROM ($sql) AS inner_tbl";

            $start = $offset + 1;
            $end = $offset + $count;

            $sql = "WITH outer_tbl AS ($sql) SELECT * FROM outer_tbl WHERE \"ZEND_DB_ROWNUM\" BETWEEN $start AND $end";
        }

        return $sql;
    }
}