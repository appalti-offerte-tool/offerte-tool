<?php

class OSDN_Db_Response extends OSDN_Db_Response_Abstract
{
    /**
     * @var Zend_Db_Select
     */
    protected $_select;

    /**
     * @var Zend_Db_Table_Abstract
     */
    protected $_table;

    public function __construct(Zend_Db_Select $select, Zend_Db_Table_Abstract $table = null)
    {
        $this->_select = $select;
        $this->_table = $table;
    }

    public function getRowset(Closure $callbackFn = null)
    {
        if (null === $this->_table) {
            $rowset = $this->_select->query()->fetchAll();
        } else {
            $rowset = $this->_table->fetchAll($this->_select);
        }

        return $this->_toRowset($rowset, $callbackFn);
    }

    /**
     * (non-PHPdoc)
     * @see Countable::count()
     */
    public function count()
    {
       if ($this->_rowCount === null) {
            $localClonedStatement = clone $this->_select;

            $this->_rowCount = 0;

            $onSelectResetCallback = function(Zend_Db_Select $s) {
                $s->reset(Zend_Db_Select::COLUMNS);                 // remove all columns
                $s->columns(array('row'=>"TRIM('row')"));   // but need at least 1 column
                $s->reset(Zend_Db_Select::LIMIT_OFFSET);
                $s->reset(Zend_Db_Select::LIMIT_COUNT);
                $s->reset(Zend_Db_Select::ORDER);
                // $s->reset(Zend_Db_Select::GROUP);

                $sc = new Zend_Db_Select($s->getAdapter());
                $sc->from(array('sq' => $s), array());
                $sc->columns(array('c' => new Zend_Db_Expr('COUNT(*)')));
                return $sc;

                $s->columns(array('c' => new Zend_Db_Expr('COUNT(*)')));
                return $s;
            };

            if (($union = $localClonedStatement->getPart(Zend_Db_Select::UNION)) && !empty($union))
            {
                foreach ($union as $u) {
                    if (! $u[0] instanceof Zend_Db_Select) {
                        throw new OSDN_Exception('Union must be an Zend_Db_Select instance');
                    }

                    $uselect = $onSelectResetCallback($u[0]);
                    $this->_rowCount += (int) $uselect->query()->fetchColumn(0);
                }
            }
            else
            {
                $uselect = $onSelectResetCallback($localClonedStatement);
                $this->_rowCount = (int) $uselect->query()->fetchColumn(0);
            }

        }
        return $this->_rowCount;
    }
}