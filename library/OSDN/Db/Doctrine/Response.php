<?php

class OSDN_Db_Doctrine_Response extends OSDN_Db_Response_Abstract
{
    /**
     * @var Doctrine\ORM\QueryBuilder
     */
    protected $_queryBuilder;

    public function __construct(\Doctrine\ORM\QueryBuilder $queryBuilder)
    {
        $this->_queryBuilder = $queryBuilder;
    }

    public function getRowset(Closure $callbackFn = null)
    {
        $rowset = $this->_queryBuilder->getQuery()->getResult();
        return $this->_toRowset($rowset, $callbackFn);
    }

    /**
     * (non-PHPdoc)
     * @see Countable::count()
     */
    public function count()
    {
        $query = clone $this->_queryBuilder;

        if ($this->_rowCount === null) {

            $query->setFirstResult(null);
            $query->setMaxResults(null);
            $query->select($query->expr()->count($query->getRootAlias()));
            $query->resetDQLPart('orderBy');

            $this->_rowCount = (int) $query->getQuery()->getSingleScalarResult();
        }

        return $this->_rowCount;
    }
}