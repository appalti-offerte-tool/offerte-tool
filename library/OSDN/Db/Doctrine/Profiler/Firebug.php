<?php

class OSDN_Db_Doctrine_Profiler_Firebug extends \Doctrine\DBAL\Logging\DebugStack
{
    /**
     * The original label for this profiler.
     * @var string
     */
    protected $_label = null;

    /**
     * The label template for this profiler
     * @var string
     */
    protected $_label_template = '%label% (%totalCount% @ %totalDuration% sec)';

    /**
     * The message envelope holding the profiling summary
     * @var Zend_Wildfire_Plugin_FirePhp_TableMessage
     */
    protected $_message = null;

    /**
     * The total time taken for all profiled queries.
     * @var float
     */
    protected $_totalElapsedTime = 0;

    public function  __construct($label = null)
    {
        $this->_label = $label;
        if(!$this->_label) {
            $this->_label = 'Database queries log via Doctrine ORM';
        }

        if (null === $this->_message) {
            $this->_message = new Zend_Wildfire_Plugin_FirePhp_TableMessage($this->_label);
            $this->_message->setBuffered(true);
            $this->_message->setHeader(array('Time','Event','Parameters'));
            $this->_message->setOption('includeLineNumbers', false);
            $this->_message->setDestroy(true);
            Zend_Wildfire_Plugin_FirePhp::getInstance()->send($this->_message);
        }
    }


    /**
     * Update the label of the message holding the profile info.
     *
     * @return void
     */
    protected function updateMessageLabel()
    {
        if (!$this->_message) {
            return;
        }
        $this->_message->setLabel(str_replace(array(
            '%label%',
            '%totalCount%',
            '%totalDuration%'
        ), array(
            $this->_label,
            $this->currentQuery,
            (string)round($this->_totalElapsedTime,5)
        ), $this->_label_template
       ));
    }

    /**
     * (non-PHPdoc)
     * @see Doctrine\DBAL\Logging.SQLLogger::stopQuery()
     */
    public function stopQuery()
    {
        parent::stopQuery();

        if (!$this->queries) {
            return;
        }

        $this->_message->setDestroy(false);
        $this->_totalElapsedTime += $this->queries[$this->currentQuery]['executionMS'];
        $this->_message->addRow(array(
            (string)round($this->queries[$this->currentQuery]['executionMS'], 5),
            $this->queries[$this->currentQuery]['sql'],
            $this->queries[$this->currentQuery]['params']
        ));
        $this->updateMessageLabel();
    }
}