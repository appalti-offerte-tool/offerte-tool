<?php

use Doctrine\DBAL\Platforms;
use Doctrine\DBAL;

class OSDN_Db_Doctrine_Dbal_Platform_Mssql extends Platforms\MsSqlPlatform
{
    /**
     * Adds an adapter-specific LIMIT clause to the SELECT statement.
     *
     *
     * @param string $sql
     * @param mixed $count
     * @param mixed $offset
     * @link http://lists.bestpractical.com/pipermail/rt-devel/2005-June/007339.html
     * @return string
     */
//    public function doModifyLimitQuery($sql, $count, $offset = null)
//    {
//        $count = intval($count);
//
//        if ($count <= 0) {
//            return $sql;
//        }
//
//        $offset = intval($offset);
//        if ($offset < 0) {
//            throw new DBALException("LIMIT argument offset=$offset is not valid");
//        }
//
//        if ($offset == 0) {
//            $sql = preg_replace('/^SELECT\s/i', 'SELECT TOP ' . $count . ' ', $sql);
//        } else {
//            $orderby = stristr($sql, 'ORDER BY');
//
//            if (!$orderby) {
//                $over = 'ORDER BY (SELECT 0)';
//            } else {
//                $over = preg_replace('/\"[^,]*\".\"([^,]*)\"/i', '"inner_tbl"."$1"', $orderby);
//            }
//
//            // Remove ORDER BY clause from $sql
//            $sql = preg_replace('/\s+ORDER BY(.*)/', '', $sql);
//
//            // Add ORDER BY clause as an argument for ROW_NUMBER()
//            $sql = "SELECT ROW_NUMBER() OVER ($over) AS \"ZEND_DB_ROWNUM\", * FROM ($sql) AS inner_tbl";
//
//            $start = $offset + 1;
//            $end = $offset + $count;
//
//            $sql = "WITH outer_tbl AS ($sql) SELECT * FROM outer_tbl WHERE \"ZEND_DB_ROWNUM\" BETWEEN $start AND $end";
//        }
//
//        return $sql;
//    }
}