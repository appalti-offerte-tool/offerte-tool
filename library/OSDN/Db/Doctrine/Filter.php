<?php
use Doctrine\ORM\Query;
use Doctrine\ORM;
class OSDN_Db_Doctrine_Filter extends OSDN_Db_Select_Filter_Abstract
{
    /**
     * @var Doctrine\ORM\QueryBuilder
     */
    protected $_queryBuilder;

    public function __construct(
        $model = null,
        ORM\QueryBuilder $queryBuilder = null,
        array $fields = array()
    ) {
        $this->_queryBuilder = $queryBuilder;
        $this->_fields = $fields;

        if (null !== $model) {
            $em = OSDN_Application_Service_Doctrine::getDefaultEntityManager();

            $modelCls = '';
            if (is_string($model)) {
                $modelCls = $model;
                if (!class_exists($modelCls)) {
                    throw new InvalidArgumentException(sprintf('Unable to find model object "%s"', $model));
                }
            } elseif ($model instanceof OSDN_Application_Model_Interface) {
                $modelCls = get_class($model);
            } else {
                throw new InvalidArgumentException(sprintf('Wrong model type "%s"', gettype($model)));
            }

            $metadata = $em->getClassMetadata($modelCls);
            $this->_fields = array_merge($metadata->fieldNames, $this->_fields);
        }
    }


    /**
     * @param string    $cond
     * @param string    $value
     * @param mixed     $type
     * @return OSDN_Db_Doctrine_Filter
     */
    protected function _addClause($cond, $value = null, $type = null)
    {
        if (null === $value) {
            $this->_queryBuilder->andWhere($cond);
            return $this;
        }

        $this->_queryBuilder->andWhere($cond, $value);
//        $this->_queryBuilder->andWhere($cond, $value);
//        $this->_queryBuilder->andWhere($this->quoteInto($cond, $value));
        return $this;
    }

    /**
     * @return Doctrine\ORM\QueryBuilder
     */
    protected function _resultSelectStatement()
    {
        return $this->_queryBuilder;
    }


    /**
     * @param string $value
     * @return string
     */
    public function quote($value)
    {
        return $this->_queryBuilder->getEntityManager()->getConnection()->quote($value);
    }


    /**
     * @param string $cond
     * @param mixed $value
     * @return string
     */
    public function quoteInto($cond, $value)
    {
        if  ($cond = str_replace('?', $this->quote($value), $cond)) {
            return $cond;
        }

        return "";
    }


    /**
     * @param string $value
     * @return string
     */
    public function quoteIdentifier($value)
    {
        return $this->_queryBuilder->getEntityManager()->getConnection()->quoteIdentifier($value);
    }

    /**
     *
     * @param integer|null $limit
     * @param integer|null $offset
     * @return OSDN_Db_Doctrine_Filter
     */
    protected function _addSelectLimit($limit, $offset)
    {
        $this->_queryBuilder->setFirstResult($offset);
        $this->_queryBuilder->setMaxResults($limit);
        return $this;
    }

    /**
     *
     * @param string $orderClause
     * @return OSDN_Db_Doctrine_Filter
     */
    protected function _addOrderClause($orderClause)
    {
        $this->_queryBuilder->add('orderBy', $orderClause);
        return $this;
    }


    /**
     *
     * @param string $field
     * @param boolean $escape
     * @return string
     */
    public function getAlias($field, $escape = true)
    {
        $index = array_search($field, $this->_fields);

        if (false !== strpos($index, '.')) {
            $field = $index;
        } else {
            $field = $this->_queryBuilder->getRootAlias() . '.' . $field;
        }

        return $field;
    }

    /**
     *
     * @return integer
     */
    public function getTotalCount()
    {
        $qb = clone $this->_queryBuilder;

        $qb->resetDQLParts(array('groupBy', 'orderBy'));
        $qb->setFirstResult(null);
        $qb->setMaxResults(null);

        $qb->select($qb->expr()->count($qb->getRootAlias()));

        return (int) $qb->getQuery()->getSingleScalarResult();
    }


    /**
     * @FIXME !!!!!
     *
     * (non-PHPdoc)
     * @see OSDN_Db_Select_Filter_Abstract::_parseComboBoxValue()
     */
    public function _parseComboBoxValue($params = null)
    {
        if (!isset($params)) {
            $params = $this->_params;
        }

        $field = $this->getAlias('value');
        $value = $params['value'];

//        $comboStatement = clone $this->_queryBuilder;

        /**
         * Ordering possible only with last select statement
         */
//        $this->_queryBuilder->resetDQLPart('orderBy');

//        $comboStatement->resetDQLParts(array('where', 'orderBy'));
//        $comboStatement->setFirstResult(null);
//        $comboStatement->setMaxResults(null);
//        $comboStatement->add('where', $field . $this->quoteInto(' = ?', $value));

//        $count = $this->_queryBuilder->getMaxResults();
//        $offset = $this->_queryBuilder->getFirstResult();
//
//        $offset = $this->_queryBuilder->setFirstResult($offset);
//        $offset = $this->_queryBuilder->setMaxResults($count - 1);

//        $this->_addClause($field . $this->quoteInto(' != ?', $value));

        $this->_queryBuilder->orWhere($field . $this->quoteInto(' = ?', $value));

//1        $q = $this->_queryBuilder->getEntityManager()->getConnection()->fetchAssoc(
//1                '  ( ' . $this->_queryBuilder->getQuery()->getSQL() . '  union all ( ' .
//1                $comboStatement->getQuery()->getSQL() .  ' )'
//1                );
//1                                ||
//1                                ||
//1                                ||
//1      $this->_selectStatement = $this->_adapter->select()->union(array($this->_selectStatement, $comboStatement));

        return $this->_queryBuilder;
    }

    protected function _doAttachParsedStringClause(array $fields, $value, $strict, $free)
    {
        $orx = null;
        foreach($fields as $field) {

            if (true === $strict) {

                $this->_queryBuilder->andWhere(
                    $this->_queryBuilder->expr()->in($field, $value)
                );
                continue;
            }

            $prefix = true === $free ? '%' : '';
            $ex = $this->_queryBuilder->expr();

            if (null === $orx) {
                $orx = $ex->orx();
            }

            $orx->add($ex->like($field, $ex->literal($prefix . $value . '%')));
        }

        if (null !== $orx) {
            $this->_queryBuilder->andWhere($orx);
        }

        return $this;
    }
}
