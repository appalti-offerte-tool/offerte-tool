<?php

/**
 * The plugin help to fill params as
 * "limit", "order", "sort", "filters"
 * into SQL query
 *
 * @category    OSDN
 * @package     OSDN_Db_Plugin
 * @version     $Id: Abstract.php 20592 2012-07-24 11:42:13Z yaroslav $
 */
abstract class OSDN_Db_Select_Filter_Abstract
{
    const STRATEGY_GRID        = 'grid';

    const STRATEGY_COMBO       = 'combo';

    const LIMIT                = 'limit';
    const START                = 'start';
    const SORT                 = 'sort';
    const DIR                  = 'dir';
    const FILTER               = 'filter';


    /**
     * The tables cols
     *
     * @var array
     */
    protected $_fields = array();

    protected $_sqlCalcFoundRows = false;

    /**
     * array of param names
     *
     * @var array
     */
    protected $_paramNames = array(
        self::LIMIT                 => self::LIMIT,
        self::START                 => self::START,
        self::SORT                  => self::SORT,
        self::DIR                   => self::DIR,
        self::FILTER                => self::FILTER
    );

    /**
     * The input params
     * @var array
     */
    protected $_params;

    /**
     * Define allowed limit
     *
     * @var boolean
     */
    protected $_allowLimit = true;

    protected $_strategy = self::STRATEGY_GRID;

    /**
     * Set param names
     *
     * @param array $data            param names
     * @return void
     */
    public function setParamNames($data)
    {
        foreach ($data as $k => $v) {
            if (array_key_exists($k, $this->_paramNames)) {
                $this->_paramNames[$k] = $v;
            }
        }
    }

    /**
     * Get param names
     *
     * @param array $name name of parameter
     * @return mixed
     */
    public function getParamNames($name = null)
    {
        if (array_key_exists($name, $this->_paramNames)) {
            return $this->_paramNames[$name];
        }

        return $this->_paramNames;
    }

    /**
     * Parse order, limit and filter params and add this to statement
     *
     * @param array $params            params
     * @return mixed                   the modified statement
     */
    public function parse(array $params)
    {
        $this->_params = $params;

        $this->parseFilters($this->_params);

        $this->parseOrders();
        $this->parseLimits();

//        if (true === $this->_sqlCalcFoundRows) {
//            if (method_exists($this->_resultSelectStatement(), 'setSqlCalcFoundRows')) {
//                $this->_resultSelectStatement()->setSqlCalcFoundRows($this->_sqlCalcFoundRows);
//            } else {
//                $this->_sqlCalcFoundRows = false;
//            }
//        }

        if ($this->_strategy == self::STRATEGY_COMBO && !empty($params['value'])) {
            $this->_parseComboBoxValue();
        }

        return $this->_resultSelectStatement();
    }

    /**
     * Parse order params and add this to statement
     *
     * @param array $params            params
     * @return mixed          the modified statement
     */
    public function parseOrders($params = null)
    {
        if (!isset($params)) {
            $params = $this->_params;
        }

        if (empty($params[self::SORT])) {
            return $this->_resultSelectStatement();
        }

        /**
         * @FIXME In the extjs 4+ added posibility to sort by multiple columns
         * The format on client side should be changed
         */
        $sort = Zend_Json::decode($params[self::SORT]);

        if (!is_array($sort)) {
            return $this->_resultSelectStatement();
        }

        foreach($sort as $clause) {
            if (!isset($clause['property']) || !isset($clause['direction'])) {
                continue;
            }

            if (!in_array($clause['property'], $this->_fields)) {
                continue;
            }

            if (!in_array(strtolower($clause['direction']), array('asc', 'desc'))) {
                $clause['direction'] = 'asc';
            }

            $orderClause = $this->getAlias($clause['property'], false) . " " . strtoupper($clause['direction']);
            $this->_addOrderClause($orderClause);
        }

        return $this->_resultSelectStatement();
    }

    /**
     * Parse limit params and add this to statement
     *
     * @param array $params            params
     * @return mixed          the modified statement
     */
    public function parseLimits($params = null)
    {
        if (true !== $this->_allowLimit) {
            return $this->_resultSelectStatement();
        }

        if (!isset($params)) {
            $params = $this->_params;
        }

        if (empty($params[self::LIMIT]) || !isset($params[self::START])) {
            return $this->_resultSelectStatement();
        }

        return $this->_addSelectLimit($params[self::LIMIT], $params[self::START]);
    }

    /**
     * Parse filter params and add this to statement
     *
     * @param array $params            params
     * @return mixed                   the modified statement
     */
    public function parseFilters(array $params = array())
    {
        if (empty($params)) {
            $params = $this->_params;
        }

        if (!array_key_exists(self::FILTER, $params) || !is_array($params[self::FILTER])) {
            return $this->_resultSelectStatement();
        }

        foreach ($params[self::FILTER] as $filter) {

            if (empty($filter['data'])) {
                continue;
            }

            $data = $filter['data'];
            if (empty($filter['field']) || empty($data['type']) || !isset($data['value'])) {
                continue;
            }

            $field = !empty($data['field']) ? $data['field'] : $filter['field'];
            $value = trim($data['value']);
            if (empty($value) && '0' !== $value) {
                continue;
            }

            /**
             * @todo
             */
            if (!in_array($data['type'], array('search', 'stringfree', 'searchfree', 'list', 'string', 'stringstrict'))) {
                if (!in_array($field, $this->_fields)) {
                    continue;
                }
                $field = $this->getAlias($field);
            }

            switch($data['type']) {

                case 'list':
                    $value = explode(',', $value);
                case 'string':
                case 'stringstrict':
                case 'search':
                case 'searchfree':
                case 'stringfree':

                    $fields = array();
                    foreach(explode(',', $field) as $field) {

                        $field = trim($field);
                        if (!in_array($field, $this->_fields)) {
                            continue;
                        }

                        $fields[] = $this->getAlias($field);
                    }

                    if (empty($fields)) {
                        continue;
                    }

                    if (is_string($value) && preg_match('!^(\d{2})[.\/-](\d{2})[.\/-](\d{2,4})$!', $value, $matches)) {
                        $value = $matches[3] . '-' . $matches[2] . '-' . $matches[1];
                    }

                    $this->_doAttachParsedStringClause(
                        $fields,
                        $value,
                        in_array($data['type'], array('list', 'stringstrict')),
                        in_array($data['type'], array('searchfree', 'stringfree'))
                    );
                    break;

                case 'numeric' :
                    if (empty($data['comparison'])) {
                        continue;
                    }

                    switch ($data['comparison']) {
                        case 'eq':
                            $this->_addClause($field . $this->quoteInto(' = ?', $value));
                            break;

                        case 'lt':
                            $this->_addClause($field . $this->quoteInto(' < ?', $value));
                            break;

                        case 'lteq':
                            $this->_addClause($field . $this->quoteInto(' <= ?', $value));
                            break;

                        case 'gt':
                            $this->_addClause($field . $this->quoteInto(' > ?', $value));
                            break;

                        case 'gteq':
                            $this->_addClause($field . $this->quoteInto(' >= ?', $value));
                            break;

                        case 'neq':
                            $this->_addClause($field . $this->quoteInto(' != ?', $value));
                            break;
                    }
                    break;

                case 'date':
                    if (empty($data['comparison'])) {
                        continue;
                    }

                    extract(date_parse($value));
                    $formattedValue = sprintf('%04d-%02d-%02d', $year, $month, $day);
                    switch ($data['comparison']) {
                        case 'eq':
                            $this->_addClause($field . $this->quoteInto(' = ?', $formattedValue));
                            break;

                        case 'lt':
                            $this->_addClause($field . $this->quoteInto(' < ?', $formattedValue));
                            break;

                        case 'lteq':
                            $this->_addClause($field . $this->quoteInto(' <= ?', $formattedValue));
                            break;

                        case 'gt':
                            $this->_addClause($field . $this->quoteInto(' > ?', $formattedValue));
                            break;

                        case 'gteq':
                            $this->_addClause($field . $this->quoteInto(' >= ?', $formattedValue));
                            break;
                    }
                    break;

                case 'boolean':
                    $this->_addClause($field . $this->quoteInto(' = ?', $value));
                    break;

                default:
                    break;

            }
        }

        return $this->_resultSelectStatement();
    }

    /**
     * Get column alias
     *
     * @param string $field     The field name
     * @param bool $escape      Escape flag
     *      If true then escape via adapter quote indentifier
     * @return string           The field name
     */
    public function getAlias($field, $escape = true)
    {
        $index = array_search($field, $this->_fields);
        if (!is_int($index)) {
            $field = $index;
        }

        if (true === $escape) {
            $field = $this->quoteIdentifier($field);
        }

        return $field;
    }

    /**
     * Set limit avialable
     *
     * @param bool $flag
     * @return OSDN_Db_Select_Filter_Abstract
     */
    public function allowLimit($flag)
    {
        $this->_allowLimit = (boolean) $flag;
        return $this;
    }

    /**
     * Change plugin strategy
     *
     * @param string $strategy
     * @return OSDN_Db_Select_Filter_Abstract
     */
    public function setStrategy($strategy)
    {
        if (!in_array($strategy, array(self::STRATEGY_COMBO, self::STRATEGY_GRID))) {
            throw new Exception("The type `$strategy` is not allowed.");
        }

        $this->_strategy = $strategy;
        return $this;
    }

    abstract protected function _doAttachParsedStringClause(array $fields, $value, $strict, $free);

    abstract public function getTotalCount();

    abstract protected function _addClause($cond, $value = null, $type = null);

    abstract protected function _resultSelectStatement();

    abstract public function quote($value);

    abstract public function quoteInto($cond, $value);

    abstract public function quoteIdentifier($value);

    abstract protected function _addSelectLimit($limit, $offset);

    abstract protected function _addOrderClause($orderClause);

    abstract protected function _parseComboBoxValue($params = null);
}