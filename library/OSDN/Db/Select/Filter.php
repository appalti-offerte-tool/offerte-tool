<?php

class OSDN_Db_Select_Filter extends OSDN_Db_Select_Filter_Abstract
{

    /**
     * Predefined table
     *
     * @var Zend_Db_Table_Abstract
     */
    protected $_table;

    /**
     * Detault adapter
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_adapter;

    /**
     * Predefined select statement
     *
     * @var OSDN_Db_Select
     */
    protected $_selectStatement;

    /**
     * Constructor
     *
     * @param Zend_Db_Table_Abstract $table  the working table
     * @param Zend_Db_Select $statement      the working statement
     * @param array $fields                  available fields
     */
    public function __construct(
        Zend_Db_Select & $statement,
        Zend_Db_Table_Abstract $table = null,
        array $fields = array()
    ) {
        $this->_adapter = $statement->getAdapter();
        $this->_table = $table;
        $this->_selectStatement = & $statement;

        if (empty($fields) && !is_null($table)) {
            $fields = $table->info(OSDN_Db_Table_Abstract::COLS);
        }

        $this->_fields = $fields;
    }

    protected function _addClause($cond, $value = null, $type = null)
    {
        $this->_selectStatement->where($cond, $value, $type);
        return $this;
    }


    protected function _resultSelectStatement()
    {
        return $this->_selectStatement;
    }


    public function quote($value)
    {
        return $this->_adapter->quote($value);
    }


    public function quoteInto($cond, $value)
    {
        return $this->_adapter->quoteInto($cond, $value);
    }


    public function quoteIdentifier($value)
    {
        return $this->_adapter->quoteIdentifier($value);
    }


    protected function _addSelectLimit($limit, $offset)
    {
        return $this->_selectStatement->limit($limit, $offset);
    }

    protected function _addOrderClause($orderClause)
    {
        return $this->_selectStatement->order($orderClause);
    }


    public function getTotalCount()
    {
        $s = clone $this->_selectStatement;
        $s->reset(Zend_Db_Select::COLUMNS);
        $s->reset(Zend_Db_Select::LIMIT_OFFSET);
        $s->reset(Zend_Db_Select::LIMIT_COUNT);
        $s->reset(Zend_Db_Select::ORDER);
        $s->reset(Zend_Db_Select::GROUP);

        $s->columns(array('c' => new Zend_Db_Expr('COUNT(*)')));

        try {
            $count = $s->query()->fetchColumn(0);
            return $count;
        } catch (Exception $e) {
            if (OSDN_DEBUG) {
                throw $e;
            }
            return false;
        }
    }


    /**
     * Parse order params and add this to statement
     *
     * @param array $params            params
     * @return Zend_Db_Select          the modified statement
     */
    public function _parseComboBoxValue($params = null)
    {
        if (!isset($params)) {
            $params = $this->_params;
        }

        $field = $this->getAlias('value');
        $value = $params['value'];

        if (empty($value)) {
            return $this->_selectStatement;
        }

        $comboStatement = clone $this->_selectStatement;

        /**
         * Ordering possible only with last select statement
         */
        $this->_selectStatement->reset(OSDN_Db_Select::ORDER);
        $comboStatement->reset(OSDN_Db_Select::WHERE);
        $comboStatement->reset(OSDN_Db_Select::LIMIT_OFFSET);
        $comboStatement->reset(OSDN_Db_Select::LIMIT_COUNT);
        $comboStatement->reset(OSDN_Db_Select::SQL_CALC_FOUND_ROWS);
        $comboStatement->where($field . ' = ?', $value);

        $count = $this->_selectStatement->getPart(OSDN_Db_Select::LIMIT_COUNT);
        $offset = $this->_selectStatement->getPart(OSDN_Db_Select::LIMIT_OFFSET);
        $this->_selectStatement->limit($count - 1, $offset);
        $this->_selectStatement->where($field . ' != ?', $value);

        if ($offset) {
            $cols = $comboStatement->getPart(OSDN_Db_Select::COLUMNS);
            $comboStatement->reset(OSDN_Db_Select::COLUMNS);

            $comboStatement->columns(array('ZEND_DB_ROWNUM' => new Zend_Db_Expr('1')));

            foreach ($cols as $col) {
                
                if (is_object($col[1])  &&  'Zend_Db_Expr' === get_class($col[1])) {
                    $valExpr = array(
                        (empty($col[2]) ? ('field' . uniqid()) : $col[2]) =>  $col[1]
                    );
                } else {
                    $valExpr = array(
                        (empty($col[2]) ? $col[1] : $col[2]) =>  $col[0] . '.' . $col[1]
                    );
                }

                $comboStatement->columns($valExpr);
            }
        }

        $this->_selectStatement = $this->_adapter->select()->union(array($this->_selectStatement, $comboStatement));

        return $this->_selectStatement;
    }

    protected function _doAttachParsedStringClause(array $fields, $value, $strict, $free)
    {
        foreach($fields as $field) {

            if (true === $strict) {
                $orx[] = $this->quoteInto($field . ' IN (?)', $value);
                continue;
            }

            $prefix = true === $free ? '%' : '';
            $orx[] = $this->quoteInto($field . ' LIKE ?', $prefix . $value . '%');
        }

        if (!empty($orx)) {
            $this->_selectStatement->where('(' . join(') OR (', $orx) . ')');
        }

        return $this;
    }
}