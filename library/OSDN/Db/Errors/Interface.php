<?php

/**
 * Provide a bridge between OSDN errors
 * and database adapter
 * Allow very easy track errors in the future
 *
 * @version $Id: Interface.php 19140 2010-11-12 16:10:10Z yaroslav $
 */
interface OSDN_Db_Errors_Interface
{
    const E_DUPLICATE_KEY = 1;

    /**
     * Retrieve explanation by code connection
     *
     * @param int $codeConnection
     * @return string
     */
    public function toExplanation($codeConnection);
    
    /**
     * Convert internal code to code connection
     *
     * @param $code int Internal code
     * @param $e    Exception   The exception object
     *      Sometimes db adapter return bad error code
     *      and we need to parse error message from exception
     *      to get right code back
     * @return int
     */
    public function toCodeConnection($code, Exception $e = null);
    
    /**
     * Convert code connection (internal code of driver) to
     * code alias (internal code like self::E_DUPLICATE_KEY)
     *
     * @param int $codeConnection
     * @param Exception $e
     *
     * @return int
     */
    public function toCodeAlias($codeConnection, Exception $e = null);
}