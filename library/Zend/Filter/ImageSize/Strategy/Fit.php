<?php

/**
 * @see Zend_Filter_ImageSize_Strategy_Interface 
 */
require_once 'Zend/Filter/ImageSize/Strategy/Interface.php';

/**
 * Strategy for resizing the image by fitting the content into the given 
 * dimensions.
 */
class Zend_Filter_Imagesize_Strategy_Fit 
    implements Zend_Filter_ImageSize_Strategy_Interface
{
    /**
     * Return canvas resized according to the given dimensions.
     * @param resource $image GD image resource
     * @param int $width Output width
     * @param int $height Output height
     * @return resource GD image resource
     */
    public function resize($image, $width, $height)
    {
        $origWidth = imagesx($image);
        $origHeight = imagesy($image);
        
        $rWidth = $origWidth / $width;
        $rHeight = $origHeight / $height;

        if($rWidth > $rHeight) {
            $ratio = $rWidth;
        } else {
            $ratio = $rHeight;
        }
        
        $newWidth = $origWidth / $ratio;
        $newHeight = $origHeight / $ratio;    

        $resized = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($resized, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);

        // added by vkudrinsky for fixing resizing transparent png. Not tested on another projects.
        imagesavealpha($resized, true);
        $trans_colour = imagecolorallocatealpha($resized, 0, 0, 0, 127);
        imagefill($resized, 0, 0, $trans_colour);
        // --------------------------------
        
        return $resized;
    }
}